let domain = '//static.pedulisehat.id/';
let webpackConfig = require("./common.config");

module.exports = function(paramObj) {
    return webpackConfig(paramObj, domain);
}