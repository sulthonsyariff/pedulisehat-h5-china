#!/bin/bash
rootPath=$PWD

#-----  首页 -----#
cd ${rootPath}/dev/src/home && ${rootPath}/node_modules/.bin/webpack
#-----  首页 -----#


#-----  专题页 -----#
cd ${rootPath}/dev/src/specialSubject && ${rootPath}/node_modules/.bin/webpack
#-----  专题页 -----#

#-----  covidCount -----#
cd ${rootPath}/dev/src/covidCount && ${rootPath}/node_modules/.bin/webpack
#-----  covidCount -----#

#-----  noCaptchaForApp -----#
cd ${rootPath}/dev/src/noCaptchaForApp && ${rootPath}/node_modules/.bin/webpack
#-----  noCaptchaForApp -----#
# 列表页
cd ${rootPath}/dev/src/campaignList && ${rootPath}/node_modules/.bin/webpack --config webpack.config.pro.campaignList.js
cd ${rootPath}/dev/src/campaignList && ${rootPath}/node_modules/.bin/webpack --config webpack.config.pro.donateList.js
cd ${rootPath}/dev/src/campaignList && ${rootPath}/node_modules/.bin/webpack --config webpack.config.pro.zakatList.js

cd ${rootPath}/dev/src/OVOXPeduliSehat && ${rootPath}/node_modules/.bin/webpack

# 搜索结果页
cd ${rootPath}/dev/src/searchResaults && ${rootPath}/node_modules/.bin/webpack

#-----  new landing page -----#
# cd ${rootPath}/dev/src/newHp2 && ${rootPath}/node_modules/.bin/webpack
#-----  new landing page -----#
#-----  首页:banner -----#
# cd ${rootPath}/dev/src/homeBanner/NGO && ${rootPath}/node_modules/.bin/webpack
#-----  首页:banner -----#

#----- 个人中心 -----#
# aboutUs
cd ${rootPath}/dev/src/personalCenter/aboutUs && ${rootPath}/node_modules/.bin/webpack
# faq
cd ${rootPath}/dev/src/personalCenter/faq && ${rootPath}/node_modules/.bin/webpack

# 1.我关注的列表
cd ${rootPath}/dev/src/personalCenter/followedCampaigns && ${rootPath}/node_modules/.bin/webpack
# 2.我支持的列表
# cd ${rootPath}/dev/src/personalCenter/supportedCampaigns && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/donationHistory && ${rootPath}/node_modules/.bin/webpack
# 3.我发起的列表
cd ${rootPath}/dev/src/personalCenter/myCampaigns && ${rootPath}/node_modules/.bin/webpack
# 勋章页
cd ${rootPath}/dev/src/personalCenter/myTrophies && ${rootPath}/node_modules/.bin/webpack

# bindEmail
cd ${rootPath}/dev/src/personalCenter/setting/bindEmail && ${rootPath}/node_modules/.bin/webpack

cd ${rootPath}/dev/src/personalCenter/setting/myAccount && ${rootPath}/node_modules/.bin/webpack
# 4.设置页--修改昵称
cd ${rootPath}/dev/src/personalCenter/setting/modifyName && ${rootPath}/node_modules/.bin/webpack
# 5.设置页--修改头像
cd ${rootPath}/dev/src/personalCenter/setting/setting && ${rootPath}/node_modules/.bin/webpack
# 6.设置页--绑定
# cd ${rootPath}/dev/src/personalCenter/setting/bindingAccount && ${rootPath}/node_modules/.bin/webpack
# 7.设置页--确认重新绑定手机号
# cd ${rootPath}/dev/src/personalCenter/setting/modifyPhone && ${rootPath}/node_modules/.bin/webpack
# 8.设置页--绑定手机号
# cd ${rootPath}/dev/src/personalCenter/setting/sendCode && ${rootPath}/node_modules/.bin/webpack
# 9.设置页--切换语言
cd ${rootPath}/dev/src/personalCenter/changeLanguage && ${rootPath}/node_modules/.bin/webpack
# 10.用户等级
cd ${rootPath}/dev/src/personalCenter/userLevel && ${rootPath}/node_modules/.bin/webpack

#----- 设置页--修改密码 -----#
# 1.找回密码
cd ${rootPath}/dev/src/personalCenter/setting/modifyPassword/findBackPassword && ${rootPath}/node_modules/.bin/webpack
# 2.修改密码
cd ${rootPath}/dev/src/personalCenter/setting/modifyPassword/modifyPassword && ${rootPath}/node_modules/.bin/webpack
# 3.短信验证
cd ${rootPath}/dev/src/personalCenter/setting/modifyPassword/phoneNumVerification && ${rootPath}/node_modules/.bin/webpack
# 4.重置密码
cd ${rootPath}/dev/src/personalCenter/setting/modifyPassword/resetPassword && ${rootPath}/node_modules/.bin/webpack
# 5.设置密码
cd ${rootPath}/dev/src/personalCenter/setting/modifyPassword/setPassword && ${rootPath}/node_modules/.bin/webpack
#----- 设置页--修改密码 -----#

#----- 消息 -----#
# 1.消息聚合列表
cd ${rootPath}/dev/src/personalCenter/messages/MessagesCategory && ${rootPath}/node_modules/.bin/webpack
# 2.消息设置
cd ${rootPath}/dev/src/personalCenter/messages/MessagesSetting && ${rootPath}/node_modules/.bin/webpack
# 3.通知类型消息详情
cd ${rootPath}/dev/src/personalCenter/messages/NotificationMsgDetail && ${rootPath}/node_modules/.bin/webpack
# 4.通知类型消息列表
cd ${rootPath}/dev/src/personalCenter/messages/NotificationsMsg && ${rootPath}/node_modules/.bin/webpack
# 5.动态更新消息详情
cd ${rootPath}/dev/src/personalCenter/messages/UpdatesMsg && ${rootPath}/node_modules/.bin/webpack
# 6.动态更新消息列表
cd ${rootPath}/dev/src/personalCenter/messages/UpdatesMsgDetail && ${rootPath}/node_modules/.bin/webpack
#----- 消息 -----#

#----- 个人中心 -----#


#-----  登陆 -----#
cd ${rootPath}/dev/src/login && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/loginDonate && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/loginPhone && ${rootPath}/node_modules/.bin/webpack
#-----  登陆 -----#


#-----  发起 -----#
# 发起页
cd ${rootPath}/dev/src/initiate/initiatePage && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/initiate/initiateChoose && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/initiate/initiateZakat && ${rootPath}/node_modules/.bin/webpack
# 发起成功页
cd ${rootPath}/dev/src/initiate/initiateSuccessPage && ${rootPath}/node_modules/.bin/webpack
#-----  发起 -----#


#-----  详情 -----#
# 详情页
cd ${rootPath}/dev/src/detail && ${rootPath}/node_modules/.bin/webpack
#-----  详情 -----#


#-----  详情页相关 -----#
# 1.修改金额
cd ${rootPath}/dev/src/detailRelated/modifyGoal && ${rootPath}/node_modules/.bin/webpack
# 2.修改故事
cd ${rootPath}/dev/src/detailRelated/modifyStory && ${rootPath}/node_modules/.bin/webpack
# 3.项目更新
cd ${rootPath}/dev/src/detailRelated/update && ${rootPath}/node_modules/.bin/webpack
# 4.项目更新列表
cd ${rootPath}/dev/src/detailRelated/updateList && ${rootPath}/node_modules/.bin/webpack
# 5.患者验证
cd ${rootPath}/dev/src/detailRelated/verification/patientVerification && ${rootPath}/node_modules/.bin/webpack
# 6.增加项目结束时间
cd ${rootPath}/dev/src/detailRelated/extension && ${rootPath}/node_modules/.bin/webpack
# 7.支持者排行
cd ${rootPath}/dev/src/detailRelated/supporterRanking && ${rootPath}/node_modules/.bin/webpack

#-- 提款人相关  --#
# 6.患者本人提款验证
cd ${rootPath}/dev/src/detailRelated/verification/payeeVerification/patientHimself && ${rootPath}/node_modules/.bin/webpack
# 7.直系亲属提款验证
cd ${rootPath}/dev/src/detailRelated/verification/payeeVerification/directRelatives && ${rootPath}/node_modules/.bin/webpack
# 8.夫妻关系验证
# cd ${rootPath}/dev/src/detailRelated/verification/payeeVerification/conjugalRelation && ${rootPath}/node_modules/.bin/webpack
# 9.医院验证
cd ${rootPath}/dev/src/detailRelated/verification/payeeVerification/hospital && ${rootPath}/node_modules/.bin/webpack
# 10.公益组织验证
cd ${rootPath}/dev/src/detailRelated/verification/payeeVerification/commonwealOrganization && ${rootPath}/node_modules/.bin/webpack
#-- 提款人相关  --#

# 11.验证成功
cd ${rootPath}/dev/src/detailRelated/verification/verificationSuccess && ${rootPath}/node_modules/.bin/webpack

#--  钱包相关  --#
# 12.我的钱包
cd ${rootPath}/dev/src/detailRelated/wallet/myWallet && ${rootPath}/node_modules/.bin/webpack
# 13.提现
cd ${rootPath}/dev/src/detailRelated/wallet/withdraw && ${rootPath}/node_modules/.bin/webpack
# 14.提现成功
cd ${rootPath}/dev/src/detailRelated/wallet/withdrawSuccess && ${rootPath}/node_modules/.bin/webpack
#--  钱包相关  --#

# 15.confirmation list
cd ${rootPath}/dev/src/detailRelated/confirmationList && ${rootPath}/node_modules/.bin/webpack
# 16.confirm page
cd ${rootPath}/dev/src/detailRelated/confirm/confirmPage && ${rootPath}/node_modules/.bin/webpack
# 17.confirma success page
cd ${rootPath}/dev/src/detailRelated/confirm/confirmSuccessPage && ${rootPath}/node_modules/.bin/webpack

#-----  详情页相关 -----#



#-----  捐赠 -----#
# 1.捐赠--捐赠页
cd ${rootPath}/dev/src/donate/donateMoney && webpack
# cd ${rootPath}/dev/src/donate/donateMoney2 && webpack
cd ${rootPath}/dev/src/donate/confirmOrder && webpack
# cd ${rootPath}/dev/src/donate/donateMoneyAB && webpack
cd ${rootPath}/dev/src/donate/donateMoneyTest && webpack
cd ${rootPath}/dev/src/donate/confirmOrderTest && webpack
# 2.捐赠--捐赠状态页
cd ${rootPath}/dev/src/donate/commonDonateStatus/commonDonateFailure && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/donate/commonDonateStatus/commonDonateSuccess && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/donate/donateSuccess && ${rootPath}/node_modules/.bin/webpack
# 3.捐赠--捐赠test
# cd ${rootPath}/dev/src/donate/donateTest && ${rootPath}/node_modules/.bin/webpack
# 4.捐赠--捐赠分享成功页
cd ${rootPath}/dev/src/donate/donateShareSuccess && ${rootPath}/node_modules/.bin/webpack
# 5.网上银行捐赠结果页
# cd ${rootPath}/dev/src/donate/onlineBankPayResults && ${rootPath}/node_modules/.bin/webpack
#----- GO PAY -----#
# 1.GO-PAY--go pay
# cd ${rootPath}/dev/src/donate/goPay/goPay && ${rootPath}/node_modules/.bin/webpack

# 2.GO-PAY--go pay error
# cd ${rootPath}/dev/src/donate/goPay/goPayError && ${rootPath}/node_modules/.bin/webpack
# 3.GO-PAY--go pay finish
# cd ${rootPath}/dev/src/donate/goPay/goPayFinish && ${rootPath}/node_modules/.bin/webpack
# 4.GO-PAY--go pay QR code
cd ${rootPath}/dev/src/donate/goPay/goPayQRcode && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/donate/shopeePay/QRcode && ${rootPath}/node_modules/.bin/webpack
# 5.GO-PAY--go pay unfinish
# cd ${rootPath}/dev/src/donate/goPay/goPayUnFinish && ${rootPath}/node_modules/.bin/webpack
#----- GO PAY -----#

#----- sinar mas -----#
cd ${rootPath}/dev/src/donate/VirtualAccount/OrderTransactionDetail && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/donate/VirtualAccount/VAFinish && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/donate/VirtualAccount/VAUnFinish && ${rootPath}/node_modules/.bin/webpack
#----- sinar mas -----#

#----- OVO -----#
cd ${rootPath}/dev/src/donate/OVO/OVO && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/donate/OVO/OVOTransaction && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/donate/OVO/OVOError && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/donate/OVO/OVOFinish && ${rootPath}/node_modules/.bin/webpack
#----- OVO -----#

#----- group donate -----#
# !!!!!!! 老版本安卓还需要，暂时留着 2020-8-5
# cd ${rootPath}/dev/src/donate/groupDonate && ${rootPath}/node_modules/.bin/webpack
#----- group donate -----#

#-----  捐赠 -----#

#-----  电话注册或绑定  -----#
cd ${rootPath}/dev/src/phoneBind && ${rootPath}/node_modules/.bin/webpack
#-----  电话注册或绑定  -----#

#-----  安卓页 -----#
cd ${rootPath}/dev/src/blankforapp && ${rootPath}/node_modules/.bin/webpack
#-----  安卓页 -----#

# privacyPolicy
cd ${rootPath}/dev/src/privacyPolicy && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/termCondition && ${rootPath}/node_modules/.bin/webpack

# 运营活动:复仇者联盟
# cd ${rootPath}/dev/src/activity/the-avengers/home && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/activity/the-avengers/info && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/activity/the-avengers/questions && ${rootPath}/node_modules/.bin/webpack

# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.1.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.2.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.3.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.4.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.5.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.6.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.7.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.8.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.9.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.10.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.11.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.12.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.13.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.14.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.15.js
# cd ${rootPath}/dev/src/activity/the-avengers/result && ${rootPath}/node_modules/.bin/webpack --config webpack.config.16.js
# 运营活动:复仇者联盟

# mediaPartner
cd ${rootPath}/dev/src/mediaPartner && ${rootPath}/node_modules/.bin/webpack

# kindnessReport
cd ${rootPath}/dev/src/kindnessReport && ${rootPath}/node_modules/.bin/webpack

# myProfile
cd ${rootPath}/dev/src/myProfile && ${rootPath}/node_modules/.bin/webpack

cd ${rootPath}/dev/src/zakatCalculator && ${rootPath}/node_modules/.bin/webpack