const path = require("path");

module.exports = {
    vue$: "vue/dist/vue.esm.js",

    'jquery': path.resolve(__dirname, '../dev/common/libs/jquery/jquery.min.js'),
    'lazyload': path.resolve(__dirname, '../dev/common/libs/jquery/jquery.lazyload.min.js'),
    'jq_cookie': path.resolve(__dirname, '../dev/common/libs/jquery.cookie-1.4.1.min.js'),
    'swiper': path.resolve(__dirname, '../dev/common/libs/swiper/swiper-4.3.3.min.js'),
    'store': path.resolve(__dirname, '../dev/common/libs/store/store.min.js'),
    'circleProgress': path.resolve(__dirname, '../dev/common/libs/circle-progress.js'), //环形进度条
    // 'clipboard': path.resolve(__dirname, '../dev/common/libs/clipboard.min.js'), //复制粘贴
    'fastclick': path.resolve(__dirname, '../dev/common/libs/fastclick.js'), //300毫秒延迟
    'fancybox': path.resolve(__dirname, '../dev/common/libs/fancybox/jquery.fancybox.js'),
    // 'html2canvas': path.resolve(__dirname, '../dev/common/libs/html2canvas/html2canvas.js'),
    // 'html2canvas': path.resolve(__dirname, '../dev/common/libs/html2canvas/html2canvas.min.js'),

    'cloudinary': path.resolve(__dirname, '../dev/common/libs/jquery/jquery.cloudinary'), //upload img 相关
    'fileupload': path.resolve(__dirname, '../dev/common/libs/jquery/jquery.fileupload'), //upload img 相关
    'jquery.ui.widget': path.resolve(__dirname, '../dev/common/libs/jquery/jquery.ui.widget'), //upload img 相关
    'sensorsActive': path.resolve(__dirname, '../dev/common/libs/sensors/sensorsActive.js'), // freshchat 获取 externalId,restoreID

    'freshchat.widget': path.resolve(__dirname, '../dev/common/widget/freshchat.widget'), //zendesk sdk 客服系统相关
    'google.analytics': path.resolve(__dirname, '../dev/common/widget/google.analytics'), //google analytics 谷歌埋点统计
    'google.analytics.gtry': path.resolve(__dirname, '../dev/common/widget/google.analytics.gtry'), //google analytics 谷歌埋点统计
    'facebook.jssdk': path.resolve(__dirname, '../dev/common/widget/facebook.jssdk'), //facebook.jssdk

    'createUploaderMore': path.resolve(__dirname, '../dev/common/qsc/js/upload/createUploaderMore'), //upload more imgs 相关
    'uploadCloudinaryMore': path.resolve(__dirname, '../dev/common/qsc/js/upload/uploadCloudinaryMore'), //upload more imgs 相关

    // 'createUploaderCover': path.resolve(__dirname, '../dev/common/qsc/js/upload/createUploaderCover'), //upload more imgs 相关
    // 'uploadCloudinaryCover': path.resolve(__dirname, '../dev/common/qsc/js/upload/uploadCloudinaryCover'), //upload more imgs 相关

    'createUploaderAvatar': path.resolve(__dirname, '../dev/common/qsc/js/upload/createUploaderAvatar'), //upload avatar 相关
    'uploadCloudinaryAvatar': path.resolve(__dirname, '../dev/common/qsc/js/upload/uploadCloudinaryAvatar'), //upload avatar 相关

    // Components
    'share': path.resolve(__dirname, '../dev/common/shareComponents/js/share'), // 分享组件
    'shareGtry': path.resolve(__dirname, '../dev/common/shareComponents/js/shareGtry'), // shareGtry分享组件
    'shareAsuransi': path.resolve(__dirname, '../dev/common/shareComponents/js/shareAsuransi'), // asuransi分享组件



    'appDownload': path.resolve(__dirname, '../dev/common/appDownload/main'),
    'paymentPopup': path.resolve(__dirname, '../dev/common/paymentPopup/main'),
    'campaignListComponents': path.resolve(__dirname, '../dev/common/campaignListComponents/main'),
    'urgentListComponents': path.resolve(__dirname, '../dev/common/urgentListComponents/main'),
    'FinishCampaignPopup': path.resolve(__dirname, '../dev/common/FinishCampaignPopup/main'),
    'saveH5Tips': path.resolve(__dirname, '../dev/common/saveH5Tips/main'),
    'promoPopup': path.resolve(__dirname, '../dev/common/promoPopup/js/main'),

    // Components

    //login relevant
    'turingVerification': path.resolve(__dirname, '../dev/common/turingVerification/js/turingVerification'), // shareGtry分享组件
    'noCaptchaJs': path.resolve(__dirname, '../dev/common/noCaptcha/js/noCaptcha'), // 阿里云滑动验证

    'loading': path.resolve(__dirname, '../dev/common/qsc/js/loading'), //loading 相关
    'locationChoose': path.resolve(__dirname, '../dev/common/qsc/js/locationChoose'), //三级联动
    'utils': path.resolve(__dirname, '../dev/common/qsc/js/utils'),
    'ajaxProxy': path.resolve(__dirname, '../dev/common/qsc/js/ajaxProxy.js'),
    'webSocket': path.resolve(__dirname, '../dev/common/qsc/js/webSocket.js'), //websocket

    'nav': path.resolve(__dirname, '../dev/common/nav/main.js'),
    'navBottom': path.resolve(__dirname, '../dev/common/navBottom/main.js'), //navBottom
    'commonNav': path.resolve(__dirname, '../dev/common/qsc/js/commonNav.js'),
    'commonNavDetail': path.resolve(__dirname, '../dev/common/qsc/js/commonNavDetail.js'),
    'commonFooter': path.resolve(__dirname, '../dev/common/qsc/js/commonFooter.js'),

    'commonShare': path.resolve(__dirname, '../dev/common/qsc/js/commonShare.js'),
    'commonTimeLimit': path.resolve(__dirname, '../dev/common/qsc/js/commonTimeLimit.js'),
    // 'shareTplDetail': path.resolve(__dirname, '../dev/common/qsc/js/shareTpl-detail.js'),
    'qscScroll_timestamp': path.resolve(__dirname, '../dev/common/qsc/js/qscScroll_timestamp.js'), //滑动加载
    'qscScroll_timestamp_donate_popup': path.resolve(__dirname, '../dev/common/qsc/js/qscScroll_timestamp_donate_popup'),
    'domainName': path.resolve(__dirname, '../dev/common/qsc/js/domainName.js'), //不同环境不同接口域名
    'mnsName': path.resolve(__dirname, '../dev/common/qsc/js/mnsName.js'), //不同环境不同接口域名
    'numberAnimate': path.resolve(__dirname, '../dev/common/qsc/js/numberAnimate.js'),
    'getProject': path.resolve(__dirname, '../dev/common/qsc/js/getProject.js'), //getProject
    'GTRY-bankList': path.resolve(__dirname, '../dev/common/qsc/js/GTRY-bankList.js'), //GTRY-bankList
    'GTRY-calculatePaymentFee': path.resolve(__dirname, '../dev/common/qsc/js/GTRY-calculatePaymentFee.js'), //GTRY-fee
    'search': path.resolve(__dirname, '../dev/common/qsc/js/search.js'), //搜索弹窗
    'select-relationship': path.resolve(__dirname, '../dev/common/qsc/js/select-relationship.js'), //验证页弹窗
    'bank-list-test': path.resolve(__dirname, '../dev/common/qsc/js/bank-list-test.js'), //银行列表弹窗 测试GO_PAY
    'bank-list': path.resolve(__dirname, '../dev/common/qsc/js/bank-list.js'), //银行列表弹窗
    'changeMoneyFormat': path.resolve(__dirname, '../dev/common/qsc/js/changeMoneyFormat.js'), //输入金额三位一个点
    'getFreshchat-restoreID': path.resolve(__dirname, '../dev/common/qsc/js/getFreshchat-restoreID.js'), // freshchat 获取 externalId,restoreID
    'commonDonateRecommendation': path.resolve(__dirname, '../dev/common/qsc/js/common_donate_recommendation.js'), //捐赠页联合支付
    // 'sensorsActive': path.resolve(__dirname, '../dev/common/qsc/js/sensorsActive.js'), // freshchat 获取 externalId,restoreID
    'shareAccessCount': path.resolve(__dirname, '../dev/common/qsc/js/shareAccessCount.js'), //分享数据上报
    'ramadanTrophiesAlertBox': path.resolve(__dirname, '../dev/common/qsc/js/ramadanTrophiesAlertBox.js'), //勋章

    'commonDonateSuccessTpl': path.resolve(__dirname, '../dev/common/qsc/tpl/common_donate_success.juicer'),
    'commonDonateFailureGopayTpl': path.resolve(__dirname, '../dev/common/qsc/tpl/common_donate_failure_gopay.juicer'),
    'commonDonateSuccessCalendarTpl': path.resolve(__dirname, '../dev/common/qsc/tpl/common_donate_success_calendar.juicer'),
    'commonDonateSuccess': path.resolve(__dirname, '../dev/common/qsc/js/commonDonateSuccess.js'),
    'commonDonateFailureGopay': path.resolve(__dirname, '../dev/common/qsc/js/commonDonateFailureGopay.js'),

    'model_getListData': path.resolve(__dirname, '../dev/common/campaignListComponents/model.js'),
    'getPayPlatform': path.resolve(__dirname, '../dev/common/pay/getPayPlatform.js'), //获取银行对应都渠道
    'paySubmitSuccessHandle': path.resolve(__dirname, '../dev/common/pay/submitSuccessHandle.js'), //支付提交成功后跳转逻辑
    'calculatePaymentFee': path.resolve(__dirname, '../dev/common/pay/calculatePaymentFee.js'), //费率计算
    'AppHasInstalled': path.resolve(__dirname, '../dev/common/qsc/js/AppHasInstalled.js'), //安卓内打开某个应用

    // ************ translations ************ //

    'qscLang': path.resolve(__dirname, '../dev/common/lang/qscLang.js'), //语言包
    // 'about': path.resolve(__dirname, '../dev/common/lang/translation/about.js'), //翻译包：about
    'aboutUs': path.resolve(__dirname, '../dev/common/lang/translation/aboutUs.js'), //翻译包：aboutUs
    'bindingAccount': path.resolve(__dirname, '../dev/common/lang/translation/bindingAccount.js'), //bindingAccount
    'campaignList': path.resolve(__dirname, '../dev/common/lang/translation/campaignList.js'), //campaignList
    'changeLanguage': path.resolve(__dirname, '../dev/common/lang/translation/changeLanguage.js'), //changeLanguage
    'common': path.resolve(__dirname, '../dev/common/lang/translation/common.js'), //翻译包：common
    'commonTitle': path.resolve(__dirname, '../dev/common/lang/translation/commonTitle.js'), //翻译包：commonTitle
    'commonwealOrganization': path.resolve(__dirname, '../dev/common/lang/translation/commonwealOrganization.js'), //翻译包：commonwealOrganization
    'confirmationList': path.resolve(__dirname, '../dev/common/lang/translation/confirmationList.js'), //翻译包：confirmationList
    'confirmPage': path.resolve(__dirname, '../dev/common/lang/translation/confirmPage.js'), //翻译包：confirmPage
    'confirmSuccessPage': path.resolve(__dirname, '../dev/common/lang/translation/confirmSuccessPage.js'), //翻译包：confirmSuccessPage
    'conjugalRelation': path.resolve(__dirname, '../dev/common/lang/translation/conjugalRelation.js'), //翻译包：conjugalRelation
    'covidData': path.resolve(__dirname, '../dev/common/lang/translation/covidData.js'), //翻译包：covidData
    'detail': path.resolve(__dirname, '../dev/common/lang/translation/detail.js'), //翻译包：page
    'directRelatives': path.resolve(__dirname, '../dev/common/lang/translation/directRelatives.js'), //翻译包：page
    'donateHistory': path.resolve(__dirname, '../dev/common/lang/translation/donateHistory.js'), //翻译包：donateHistory
    'donateMoney': path.resolve(__dirname, '../dev/common/lang/translation/donateMoney.js'), //翻译包：donateMoney
    'donateSuccess': path.resolve(__dirname, '../dev/common/lang/translation/donateSuccess.js'), //donateSuccess
    'donateShareSuccess': path.resolve(__dirname, '../dev/common/lang/translation/donateShareSuccess.js'), //donateShareSuccess
    'extension': path.resolve(__dirname, '../dev/common/lang/translation/extension.js'), //extension
    'followedCampaigns': path.resolve(__dirname, '../dev/common/lang/translation/followedCampaigns.js'), //翻译包：followedCampaigns
    'goPay': path.resolve(__dirname, '../dev/common/lang/translation/goPay.js'), //goPay
    'goPayError': path.resolve(__dirname, '../dev/common/lang/translation/goPayError.js'), //goPayError
    'goPayFinish': path.resolve(__dirname, '../dev/common/lang/translation/goPayFinish.js'), //goPayFinish
    'goPayQRcode': path.resolve(__dirname, '../dev/common/lang/translation/goPayQRcode.js'), //goPayQRcode
    'goPayUnFinish': path.resolve(__dirname, '../dev/common/lang/translation/goPayUnFinish.js'), //goPayUnFinish
    //'helpCenter': path.resolve(__dirname, '../dev/common/lang/translation/helpCenter.js'), //翻译包：helpCenter
    'faq': path.resolve(__dirname, '../dev/common/lang/translation/faq.js'), //翻译包：faq
    'home': path.resolve(__dirname, '../dev/common/lang/translation/home.js'), //翻译包：homePage
    'homePage': path.resolve(__dirname, '../dev/common/lang/translation/homePage.js'), //翻译包：homePage
    'hospital': path.resolve(__dirname, '../dev/common/lang/translation/hospital.js'), //翻译包：hospital
    'initiatePage': path.resolve(__dirname, '../dev/common/lang/translation/initiatePage.js'), //翻译包：initiatePage
    'initiatePage2': path.resolve(__dirname, '../dev/common/lang/translation/initiatePage2.js'), //翻译包：initiatePage
    'initiateChoose': path.resolve(__dirname, '../dev/common/lang/translation/initiateChoose.js'), //initiateChoose
    'initiateSuccessPage': path.resolve(__dirname, '../dev/common/lang/translation/initiateSuccessPage.js'), //翻译包：initiateSuccessPage
    'kindnessReport': path.resolve(__dirname, '../dev/common/lang/translation/kindnessReport.js'), //翻译包：login
    'login': path.resolve(__dirname, '../dev/common/lang/translation/login.js'), //翻译包：login
    'loginDonate': path.resolve(__dirname, '../dev/common/lang/translation/loginDonate.js'), //loginDonate
    'loginPhone': path.resolve(__dirname, '../dev/common/lang/translation/loginPhone.js'), //loginPhone
    'mediaPartner': path.resolve(__dirname, '../dev/common/lang/translation/mediaPartner.js'), //mediaPartner
    'messages': path.resolve(__dirname, '../dev/common/lang/translation/messages.js'), //翻译包：messages
    'messagesList': path.resolve(__dirname, '../dev/common/lang/translation/messagesList.js'), //翻译包：messagesList
    'modifyGoal': path.resolve(__dirname, '../dev/common/lang/translation/modifyGoal.js'), //翻译包：modifyGoal
    'modifyName': path.resolve(__dirname, '../dev/common/lang/translation/modifyName.js'), //翻译包：modifyName
    'modifyPassword': path.resolve(__dirname, '../dev/common/lang/translation/modifyPassword.js'), //翻译包：modifyPassword
    'modifyPhone': path.resolve(__dirname, '../dev/common/lang/translation/modifyPhone.js'), //翻译包：modifyPhone
    'modifyStory': path.resolve(__dirname, '../dev/common/lang/translation/modifyStory.js'), //翻译包：modifyStory
    'myCampaigns': path.resolve(__dirname, '../dev/common/lang/translation/myCampaigns.js'), //翻译包：myCampaigns
    'myTrophies': path.resolve(__dirname, '../dev/common/lang/translation/myTrophies.js'), //翻译包：myTrophies
    'myWallet': path.resolve(__dirname, '../dev/common/lang/translation/myWallet.js'), //翻译包：myWallet
    'newHp': path.resolve(__dirname, '../dev/common/lang/translation/newHp.js'), //翻译包：newHp
    'ngoACT': path.resolve(__dirname, '../dev/common/lang/translation/ngoACT.js'), //翻译包：ngoACT
    'noCaptchaLang': path.resolve(__dirname, '../dev/common/lang/translation/noCaptcha.js'), //翻译包：noCaptcha
    'ovo': path.resolve(__dirname, '../dev/common/lang/translation/ovo.js'), //翻译包：ngoACT
    'OVOTransaction': path.resolve(__dirname, '../dev/common/lang/translation/OVOTransaction.js'), //翻译包：OVOTransaction
    'patientHimself': path.resolve(__dirname, '../dev/common/lang/translation/patientHimself.js'), //翻译包：page
    'patientVerification': path.resolve(__dirname, '../dev/common/lang/translation/patientVerification.js'), //翻译包：patientVerification
    'paymentPopupLang': path.resolve(__dirname, '../dev/common/lang/translation/paymentPopup.js'), //paymentPopup
    'phoneBind': path.resolve(__dirname, '../dev/common/lang/translation/phoneBind.js'), //翻译包：phoneBind
    'privacyPolicy': path.resolve(__dirname, '../dev/common/lang/translation/privacyPolicy.js'), //privacyPolicy
    'ramadanTrophiesAlertBoxLang': path.resolve(__dirname, '../dev/common/lang/translation/ramadanTrophiesAlertBoxLang.js'), //ramadanTrophiesAlertBoxLang 勋章
    'searchResults': path.resolve(__dirname, '../dev/common/lang/translation/searchResults.js'), //翻译包：searchResults
    'selectRelationship': path.resolve(__dirname, '../dev/common/lang/translation/selectRelationship.js'), //翻译包：selectRelationship
    'sendCode': path.resolve(__dirname, '../dev/common/lang/translation/sendCode.js'), //翻译包：sendCode
    'setPassword': path.resolve(__dirname, '../dev/common/lang/translation/setPassword.js'), //翻译包：setPassword
    'setting': path.resolve(__dirname, '../dev/common/lang/translation/setting.js'), //翻译包：setting
    'sinarmas': path.resolve(__dirname, '../dev/common/lang/translation/sinarmas.js'), //翻译包：sinarmas
    'supportedCampaigns': path.resolve(__dirname, '../dev/common/lang/translation/supportedCampaigns.js'), //翻译包：supportedCampaigns
    'supporterRanking': path.resolve(__dirname, '../dev/common/lang/translation/supporterRanking.js'), //supporterRanking
    'system': path.resolve(__dirname, '../dev/common/lang/translation/system.js'), //翻译包：system
    'termCondition': path.resolve(__dirname, '../dev/common/lang/translation/termCondition.js'), //termCondition
    'turingVerify': path.resolve(__dirname, '../dev/common/lang/translation/turingVerify.js'), //termCondition
    'update': path.resolve(__dirname, '../dev/common/lang/translation/update.js'), //翻译包：update
    'updateList': path.resolve(__dirname, '../dev/common/lang/translation/updateList.js'), //翻译包：updateList
    'userLevel': path.resolve(__dirname, '../dev/common/lang/translation/userLevel.js'), //翻译包：userLevel
    'verificationSuccess': path.resolve(__dirname, '../dev/common/lang/translation/verificationSuccess.js'), //翻译包：verificationSuccess
    'withdraw': path.resolve(__dirname, '../dev/common/lang/translation/withdraw.js'), //翻译包：withdraw
    'withdrawSuccess': path.resolve(__dirname, '../dev/common/lang/translation/withdrawSuccess.js'), //翻译包：withdrawSuccess
    'zakatCalculator': path.resolve(__dirname, '../dev/common/lang/translation/zakatCalculator.js'), //翻译包：zakatCalculator





    'gtryPay': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/pay.js'), //语言包
    'gtryPayFailed': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/payFailed.js'), //语言包
    'gtryPaySucceed': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/paySucceed.js'), //语言包
    // 'gtryShareTpl': path.resolve(__dirname, '../dev/common/qsc/js/shareTpl-GTRY.js'),
    'gtryAccident': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/accident.js'),
    'gtryDetail': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/detail.js'),
    'gtryHome': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/home.js'),
    'gtryMembershipCard': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/membershipCard.js'),
    'gtryReferralBonus': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/referralBonus.js'),
    'gtryTransactions': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/transactions.js'),
    'gtryActiveMembership': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/activeMembership.js'),
    'gtryGuidence': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/guidence.js'),
    'gtryPatient': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/patientInfo.js'),
    'gtryPublicity': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/publicity.js'),
    'gtryDisease': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/diseaseInfo.js'),
    'gtryPayee': path.resolve(__dirname, '../dev/common/lang/translation/GTRY-Pedulisehat-Id/payeeInfo.js'),





    'asuransiDetail': path.resolve(__dirname, '../dev/common/lang/translation/Asuransi-Pedulisehat-Id/detail.js'),
    'asuransiPaymentStates': path.resolve(__dirname, '../dev/common/lang/translation/Asuransi-Pedulisehat-Id/paymentStates.js'),
    'asuransi-bankList': path.resolve(__dirname, '../dev/common/qsc/js/Asuransi-bankList.js'),
    'asuransi-policyData': path.resolve(__dirname, '../dev/common/lang/translation/Asuransi-Pedulisehat-Id/policyData.js'),
    'asuransi-myInsuranceList': path.resolve(__dirname, '../dev/common/lang/translation/Asuransi-Pedulisehat-Id/myInsuranceList.js'),
    'asuransiPolicy': path.resolve(__dirname, '../dev/common/lang/translation/Asuransi-Pedulisehat-Id/policyInfo.js'),

    // ************ translations ************ //



    //选择支付渠道
    'qurbanBankList': path.resolve(__dirname, '../dev/common/bankList/js/Qurban-bankList.js'), // shareGtry分享组件
    'commonBankList': path.resolve(__dirname, '../dev/common/bankList/js/common-bankList.js'), // shareGtry分享组件
    //选择支付渠道

}