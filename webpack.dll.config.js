const webpack = require('webpack')
const library = '[name]_lib'
const path = require('path')
let commonDir = path.resolve(__dirname);
let staticFileDir = path.resolve(commonDir, './release');

// libs:第三方依赖不会变更
let vendorsArr = [
    "babel-polyfill",
    'jquery',
    'lazyload',
    'jq_cookie',
    'store',
    'circleProgress',
    // 'clipboard',
    'swiper',
    'fancybox',
    'cloudinary',
    'fileupload',
    'jquery.ui.widget',
];

module.exports = {
    entry: {
        vendors: vendorsArr
    },

    output: {
        filename: '[name].dll_V20180820.js',
        path: path.join(staticFileDir, '/lib/libs'),

        library
    },

    plugins: [
        // 自动加载模块，而不必到处 import 或 require？
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),

        new webpack.DllPlugin({
            path: path.join(staticFileDir, '/lib/libs/[name]-manifest.json'),
            // This must match the output.library option above
            name: library
        }),
    ],

    resolve: {
        extensions: ['.js', '.juicer', '.json'],
        // resolve.alias 可以配置 webpack 模块解析的别名，对于比较深的解析路径，可以对其配置 alias. 可以提升 webpack 的构建速度。
        // 第三方依赖,alias别名配置:(也可以在给定对象的键后的末尾添加 $，以表示精准匹配)
        alias: {
            'jquery': path.resolve(__dirname, './dev/common/libs/jquery/jquery.min.js'),
            'lazyload': path.resolve(__dirname, './dev/common/libs/jquery/jquery.lazyload.min.js'),
            'cloudinary': path.resolve(__dirname, './dev/common/libs/jquery/jquery.cloudinary'), //upload img 相关
            'fileupload': path.resolve(__dirname, './dev/common/libs/jquery/jquery.fileupload'), //upload img 相关
            'jquery.ui.widget': path.resolve(__dirname, './dev/common/libs/jquery/jquery.ui.widget'), //upload img 相关
            'jq_cookie': path.resolve(__dirname, './dev/common/libs/jquery.cookie-1.4.1.min.js'),
            'swiper': path.resolve(__dirname, './dev/common/libs/swiper/swiper-4.3.3.min.js'),
            'store': path.resolve(__dirname, './dev/common/libs/store/store.min.js'),
            'fancybox': path.resolve(__dirname, './dev/common/libs/fancybox/jquery.fancybox.js'),
            'circleProgress': path.resolve(__dirname, './dev/common/libs/circle-progress.js'), //环形进度条
            // 'clipboard': path.resolve(__dirname, './dev/common/libs/clipboard.min.js'), //复制粘贴
            'fastclick': path.resolve(__dirname, './dev/common/libs/fastclick.js'), //300毫秒延迟
        }
    }
}