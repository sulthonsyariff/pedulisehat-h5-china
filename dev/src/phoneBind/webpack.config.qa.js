var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'phoneBind',
    htmlFileURL: 'html/phoneBind.html',
    appDir: 'js/phoneBind',
    uglify: true,
    hash: '',
    mode: 'development'
})