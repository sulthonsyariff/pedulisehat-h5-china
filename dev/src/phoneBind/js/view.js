// 公共库
import fastclick from 'fastclick';
import commonNav from 'commonNav'
import mainTpl from '../tpl/phoneBind.juicer'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import validate from '../js/_validate'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'
import turingVerification from 'turingVerification'

/* translation */
import phoneBind from 'phoneBind'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(phoneBind);
/* translation */

let reqObj = utils.getRequestParams();
// 返回地址
let qf_redirect = decodeURIComponent(reqObj["qf_redirect"]);

let [obj, $UI] = [{}, $('body')];
let isAgreePolicy = true; //是否同意协议
let CACHED_KEY = 'switchLanguage';
let Lang = store.get(CACHED_KEY);
obj.UI = $UI;

obj.init = function (rs) {
    rs.JumpName = titleLang.phoneBind;
    rs.commonNavGoToWhere = qf_redirect;
    rs.domainName = domainName;
    rs.lang = lang;
    // console.log('view:', rs);

    commonNav.init(rs);
    $UI.append(mainTpl(rs)); //主模版
    turingVerification.init(rs);

    fastclick.attach(document.body);
    clickHandle(rs);
    // watchKeyboard();
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

function clickHandle(rs) {
    //sumbit
    $('.nextBtn').on('click', function () {
        if (isAgreePolicy) {
            let sumbitData = getSumbitParam();
            if (validate.check(sumbitData, lang)) {
                $UI.trigger('sumbit', [sumbitData]);
                utils.showLoading(lang.lang9)
            }
        } else {
            utils.alertMessage(lang.lang10);
        }
    });

    //send code
    $('.senCode').on('click', function () {
        let sumbitData = getSumbitParam();
        let channel = 'WhatsApp';
        sumbitData.channel = channel;
        if ($('#mobile').val().length < 7) {
            utils.alertMessage(lang.lang1);
            return false;
        } else {
            // turingVerification.init(sumbitData);
            turingVerification.nc_cb(sumbitData);
        }
    });

    //send SMS code
    $('body').on('click', '.SMSCode', function () {
        let sumbitData = getSumbitParam();
        let channel = 'Sms';
        sumbitData.channel = channel;
        if ($('#mobile').val().length < 7) {
            utils.alertMessage(lang.lang15);
            return false;
        } else {
            turingVerification.nc_cb(sumbitData);
        }
    });


}

function getSumbitParam() {
    let mobile;
    mobile = $('#mobile').val().replace(/\b(0+)/gi, "")
    return {
        mobile: mobile,
        mobile_country_code: 62,
        captcha: $('#captcha').val(),
        authType: 100,
        platform: 'h5'
    }
}


export default obj;