import 'jq_cookie' //ajax cookie
import 'store'
import turingVerification from 'turingVerification'

import view from './view'
import model from './model'
import 'loading'
import '../less/phoneBind.less'
import utils from 'utils'
import store from 'store'

let obj = {};
let UI = view.UI;

let reqObj = utils.getRequestParams();
// 返回地址
let req_rul = decodeURIComponent(reqObj['qf_redirect']);
// 获取成功后回调地址
let jump_url = decodeURIComponent(req_rul.split('?qf_redirect=')[1]);

let user_id;
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

// console.log('==req_rul==', req_rul);
console.log('==jump_url==', jump_url);

utils.hideLoading(); // 隐藏loading
view.init({});

UI.on('sumbit', function(e, objData) {
    // 手机号绑定
    model.loginByMobile({
        param: {
            mobile: objData.mobile, //string
            mobile_country_code: objData.mobile_country_code, //number
            captcha: objData.captcha, //string
            authType: objData.authType,
            platform: objData.platform,
            uid: reqObj.uid,
            openId: reqObj.openId,
            statistics_link: statistics_link
        },
        success: function(res) {
            if (res.code == 0) {
                //存localstorage，并跳转
                $.cookie('passport', JSON.stringify({
                        accessToken: res.data.accessToken,
                        expiresIn: res.data.expiresIn,
                        refreshToken: res.data.refreshToken,
                        tokenType: res.data.tokenType,
                        uid: res.data.uid,
                    }), {
                        expires: 365,
                        path: '/',
                        domain: 'pedulisehat.id'
                    })
                    // console.log('accessToken=', JSON.parse($.cookie('passport')).accessToken);

                // judge sinarmas:金光登录输入邮箱流程需要
                if (store.get('from') == 'sinarmas') {
                    setTimeout(function() {
                        location.href = '/bindEmail.html';
                    }, 30)
                } else {
                    setTimeout(function() {
                        location.href = jump_url;
                    }, 30)
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    });
})

//sendCode
UI.on('sendCode', function(e, objData) {
    model.sendCode({
        param: {
            channel: objData.channel,
            mobile: parseInt(objData.mobile),
            mobile_country_code: parseInt(objData.mobile_country_code),
            token: objData.token,
            session_id: objData.session_id,
            sig: objData.sig,
        },
        success: function(rs) {
            if (rs.code == 0) {
                if (objData.channel == 'WhatsApp') {
                    turingVerification.invokeWhatsAppSettime();
                } else {
                    utils.alertMessage(rs.msg)
                    turingVerification.invokeSMSSettime();
                }
                utils.hideLoading();
            } else if (rs.code == 4151) {
                utils.alertMessage(rs.msg)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
})