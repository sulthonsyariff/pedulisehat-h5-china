// 公共库
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import model_getListData from 'model_getListData' //列表接口调用地址

// 引入依赖
import utils from 'utils'


let reqObj = utils.getRequestParams();
let search = reqObj['search']

let UI = view.UI;
utils.hideLoading();

// view.init({search});


model.getSearchHistory({
    success: function(rs) {
        if (rs.code == 0) {
            getPopularWord(rs);
            // console.log('insertData', data);
        } else {
            utils.alertMessage(data.msg)
        }
    },
    error: utils.handleFail
})

function getPopularWord(rs) {
    model.getPopularWord({
        success: function(res) {
            if (res.code == 0) {
                res.searchHistory = rs.data
                res.from = 'campaignList'
                view.init(res);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
}
UI.on('deleteSearchHistory', function() {
    model.deleteSearcnHistory({
        success: function(data) {
            if (data.code == 0) {
                //     view.insertData(data);
                console.log('deleteSuccess', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
})

UI.on('needload', function(e, page) {
    // console.log('needload');
    model_getListData.getListData_searchResaults({
        param: {
            page: page,
            search: search
        },
        success: function(rs) {
            if (rs.code == 0) {

                view.insertData(rs);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
});

//搜索结果为空 获取推荐列表
UI.on('needloadReconmmendation', function(e, page) {
    // console.log('needload');
    model_getListData.getListReconmmendData_searchResaults({
        param: {
            page: page,
            search: ''
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.recommend = true
                view.insertData(rs);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
});