import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getSearchHistory = function(o) {
    let url = domainName.project + '/v1/project_search_history';


    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};
obj.getPopularWord = function(o) {
    let url = domainName.project + '/v1/search_popular_word';
    // if (isLocal) {
    //     url = '/mock/project_lists_' + o.param.page + '.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};
obj.deleteSearcnHistory = function(o) {
    let url = domainName.project + '/v1/project_search_history';
    // if (isLocal) {
    //     url = '/mock/project_lists_' + o.param.page + '.json';
    // }

    ajaxProxy.ajax({
        type: 'delete',
        url: url
    }, o)
};

export default obj;