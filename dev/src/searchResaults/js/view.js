// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import listItemTpl from '../tpl/_listItem.juicer'
import store from 'store'
import domainName from 'domainName' // port domain name
import search from 'search'

import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat'
import qscScroll_timestamp from 'qscScroll_timestamp'
import googleAnalytics from 'google.analytics'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'
import campaignListComponents from 'campaignListComponents'

/* translation */
import campaignList from 'campaignList'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(campaignList);
/* translation */

let is_first = true;
let page = 0;
let scroll_list = new qscScroll_timestamp();

let reqObj = utils.getRequestParams();
let from = reqObj['from'];
// let search = reqObj['from'];

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;

obj.init = function(rs) {
    rs.lang = lang;
    rs.JumpName = '';
    // rs.from = titleLang.searchResaults
    rs.commonNavGoToWhere = '/';
    $('title').html(titleLang.searchResaults);
    // commonNav.init(rs);
    console.log('rs', rs)
    search.init(rs);

    $UI.append(mainTpl(rs)); //insert 主模版

    clickHandle(rs);
    initScorll();
    UIhandle(rs);
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

function clickHandle(res) {


    $('body').on('click', '#delete', function() {
        $('.search-input').val('');
        $('.search-bar').val('');
        store.remove('search');
        $('.search-wrap').css('display', 'block')
        $('.search-delete').css('display', 'none')
        $('.search-bar').attr('autofocus', "autofocus").trigger("click").focus();
    })

    // $('body').on('click', '.empty-icon', function () {
    //     // store.remove('search');
    //     $UI.trigger('deleteSearchHistory')
    // })
    $('body').on('click', '.return', function(res) {
        if (from == 'home') {
            location.href = '/'
        } else if (from == 'campaignList') {
            location.href = '/campaignList.html?composite=1'
        }
    })
    $('body').on('click', '.search-input', function() {
            $('.search-wrap').css('display', 'block')
            $('.search-bar').attr('autofocus', "autofocus").trigger("click").focus();
        })
        // $('.search-input').focus(function () {
        //     $('.search-wrap').css('display', 'block')
        //     $('.search-bar').attr('autofocus', "autofocus")
        // })
}

function UIhandle(rs) {
    $('.search-input').val(store.get('search'));
    $('.search-bar').val(store.get('search'));

    // if ($('.search-bar').val()) {
    //     $('.search-bar-delete').css('display', 'block');
    // } else {
    //     $('.search-bar-delete').css('display', 'none');
    // }

}

function initScorll() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });

    scroll_list.run();
};

function initScorllReconmmendation() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needloadReconmmendation', [++page])
        }
    });

    scroll_list.run();
};

obj.insertData = function(rs) {
    rs.domainName = domainName;
    rs.lang = lang;
    $('.loading').show();

    if (rs.data == null && page == 1) {
        getReconmmendList();
    }

    if (rs.data) {
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        $('.project-list').append(listItemTpl(rs));

        rs.fromWhichPage = 'searchResaults.html'
        rs.$dom = $('.Campaigns_List.page_' + rs._metadata.page)
        campaignListComponents.init(rs);

        scroll_list.run();
    } else if (is_first) {
        $('.loading').hide();
        $('.project-list').append(listItemTpl(rs));
    } else {
        $('.loading').hide();

    }

}

function getReconmmendList() {
    initScorllReconmmendation();
    var no_results = '<p class="no-results">' + lang.lang10 + '</p>'
    $('.project-list').append(no_results);
}

obj.raiseDay = function(created_at) {

    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}



export default obj;