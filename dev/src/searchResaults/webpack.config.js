var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'searchResaults',
    htmlFileURL: 'html/searchResaults.html',
    appDir: 'js/searchResaults',
    uglify: true,
    hash: '',
    mode: 'production'
})