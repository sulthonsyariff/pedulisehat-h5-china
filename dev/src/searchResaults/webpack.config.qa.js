var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'searchResaults',
    htmlFileURL: 'html/searchResaults.html',
    appDir: 'js/searchResaults',
    uglify: true,
    hash: '',
    mode: 'development'
})