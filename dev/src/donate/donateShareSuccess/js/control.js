import view from './view'
import model from './model'
import 'loading'
import '../less/donateShareSuccess.less'
import store from 'store'
import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'
import model_getListData from 'model_getListData' //列表接口调用地址

// 引入依赖
import utils from 'utils'

let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'];
let from = reqObj['from'];

// let trophiesParam = {}
// trophiesParam.isDonatedOrshare = 'share'
ramadanTrophiesAlertBox.init({
    isDonatedOrshare: 'share',
    fromWhichPage: 'donateShareSuccess.html' //google analytics need distinguish the page name
});


// 隐藏loading
utils.hideLoading();

if (from == 'OVOXPeduliSehat') {
    model_getListData.getListData_donateShareSuccess_OVO({
        param: {
            short_link: short_link
        },
        success: successHandle,
        error: utils.handleFail
    });
} else {
    model_getListData.getListData_donateShareSuccess({
        param: {
            short_link: short_link
        },
        success: successHandle,
        error: utils.handleFail
    });
}

function successHandle(res) {
    if (res.code == 0) {
        // conflicts

        res.share_info = store.get('share-info').data
            // conflicts

        // 隐藏loading
        utils.hideLoading();
        view.init(res)
        view.insertData(res);
    } else {
        utils.alertMessage(res.msg)
    }
    // console.log('insertData', res);
}

// view.init({});