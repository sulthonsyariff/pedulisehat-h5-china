import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// 获取分享trophies
obj.getShareMedal = function(o) {
    let url = domainName.psuic + '/v1/ramadan/medal/share';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

export default obj;