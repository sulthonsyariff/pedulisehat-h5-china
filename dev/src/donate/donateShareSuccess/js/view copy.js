import mainTpl from '../tpl/donateShareSuccess.juicer'
import listItemTpl from '../tpl/listItem.juicer'
import commonNav from 'commonNav'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'

/* translation */
import donateShareSuccess from 'donateShareSuccess'
import qscLang from 'qscLang'
let lang = qscLang.init(donateShareSuccess);
/* translation */

let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'];
let from = reqObj['from'];

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// var reqObj = utils.getRequestParams();
// var project_id = reqObj['project_id'];

obj.init = function(rs) {
    console.log('rs', rs)
    rs.lang = lang;
    rs.JumpName = '';
    commonNav.init(rs);
    rs.domainName = domainName;
    if (store.get('share-info').from && store.get('share-info').from == 'group') {
        rs.commonNavGoToWhere = '/group/' + rs.share_info.short_link
    } else {
        rs.commonNavGoToWhere = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.share_info.project_id + '&clear_top=true' :
            '/' + rs.share_info.short_link +
            (from == 'OVOXPeduliSehat' ? '?from=OVOXPeduliSehat' : '');
    }

    // rs.commonNavGoToWhere = '/' + rs.share_info.short_link;
    if (store.get('share-info').data.is_first_share) {
        rs.is_first_share = store.get('share-info').data.is_first_share
        rs.coop_name = store.get('share-info').data.coop_name
        rs.money = store.get('share-info').data.money
        rs.text = store.get('share-info').data.text
    }

    $UI.append(mainTpl(rs));
    fastclick.attach(document.body);

    utils.hideLoading();

    $('body').on('click', '.view-all-btn', function(e) {
        // location.href = '/campaignList.html'
        location.href = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/main?index=0' :
            (from == 'OVOXPeduliSehat' ? '/OVOXPeduliSehat.html' : '/campaignList.html');
    });

    $('body').on('click', '.review-this', function(e) {
        if (store.get('share-info').from && store.get('share-info').from == 'group') {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/special_projects?short_link=' + short_link : '/group/' + short_link
        } else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + res.project_id : '/' + short_link
        }
    });

    // $('body').on('click','.view-all-btn','#goBack',function(e){
    //     store.remove('un-login')
    //     store.remove('share-info')
    // })

    // close APP download
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '118px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '72px')

    });
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensorsActive.init();

};

obj.insertData = function(rs) {

    rs.domainName = domainName;
    let arr = [];

    if (rs.data.length != 0) {
        rs.hotCampaigns = rs.data
        rs._short_link = short_link;

        for (let i = 0; i < rs.data.length; i++) {
            if (!rs.data[i].stopped) {
                arr.push(rs.data[i])
            }
        }

        for (let i = 0; i < rs.data.length; i++) {
            rs.hotCampaigns = rs.data
            console.log('rs.hotCampaigns[i]', rs.hotCampaigns[i].created_at)
                //时间
                // rs.hotCampaigns[i].created_at = raiseDay(rs.hotCampaigns[i].created_at);
                //时间
            if (rs.hotCampaigns[i].closed_at) {
                rs.hotCampaigns[i].timeLeft = utils.timeLeft(rs.hotCampaigns[i].closed_at);
            }
            rs.hotCampaigns[i].detailHref = utils.browserVersion.android ?
                'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.hotCampaigns[i].project_id + '&clear_top=true' :
                '/' + rs.hotCampaigns[i].short_link + (from == 'OVOXPeduliSehat' ? '?from=OVOXPeduliSehat' : '');

            // 进度条：设置基准的高度为1%，
            rs.hotCampaigns[i].progress = rs.hotCampaigns[i].current_amount / rs.hotCampaigns[i].target_amount * 100 + 1;

            //   图片
            rs.hotCampaigns[i].cover = JSON.parse(rs.hotCampaigns[i].cover);

            // function imageChoose(souceImg) {
            //     var fauto = souceImg.image.indexOf(",f_auto")
            //     var imageReg = '/image/authenticated/s--[\\w\\-]{8}--/';
            //     var regResult = souceImg.image.match(imageReg)

            //     if (typeof String.prototype.endsWith != 'function') {
            //         String.prototype.endsWith = function (str) {
            //             return this.slice(-str.length) == str;
            //         };
            //     }
            //     var imageEndWithWebp = souceImg.image.endsWith(".webp")

            //     if (regResult && regResult[0]) {
            //         if (imageEndWithWebp && fauto == -1) {
            //             return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120,f_auto/' + souceImg.thumb
            //         }
            //         return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120/' + souceImg.thumb
            //     }
            //     return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120/' + souceImg.image
            // }

            rs.hotCampaigns[i].rightUrl = utils.imageChoose(rs.hotCampaigns[i].cover)
            rs.hotCampaigns[i].avatar = utils.imageChoose(rs.hotCampaigns[i].avatar)

            // 格式化金额
            rs.hotCampaigns[i].target_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].target_amount);
            rs.hotCampaigns[i].current_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].current_amount);

            // 企业用户
            rs.hotCampaigns[i].corner_mark = rs.hotCampaigns[i].corner_mark ? JSON.parse(rs.hotCampaigns[i].corner_mark) : '';
        }
        rs.lang = lang;

        $('.campaigns_list').append(listItemTpl(rs));

        // if row's number more than 2,show the overflow class
        for (let i = 0; i < rs.data.length; i++) {
            if ($(".inner_name" + '_' + i).height() > 32) {
                $('.campaign_name' + '_' + i).addClass('overflow')
            }
        }
        let windowWidth = $(window).width() / 2
        $(".user-name").each(function() {
            console.log('windowWidth',windowWidth)
            if ($(this).width() > windowWidth) {
                $(this).css({
                    'width': windowWidth,
                    'overflow': 'hidden',
                    'white-space': 'nowrap',
                    'text-overflow': 'ellipsis',
                })
            }
        });

        // 如果超过四个项目，则隐藏
        // if ($('.campaign_box').length > 3) {
        //     $($('.campaign_box')[3]).css('display', 'none');
        // }

    }
    rs.lang = lang;
}

function raiseDay(created_at) {

    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}
export default obj;