import mainTpl from '../tpl/donateShareSuccess.juicer'
// import listItemTpl from '../tpl/listItem.juicer'
import commonNav from 'commonNav'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'
import campaignListComponents from 'campaignListComponents'

/* translation */
import donateShareSuccess from 'donateShareSuccess'
import qscLang from 'qscLang'
let lang = qscLang.init(donateShareSuccess);
/* translation */

let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'];
let from = reqObj['from'];

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// var reqObj = utils.getRequestParams();
// var project_id = reqObj['project_id'];

obj.init = function(rs) {
    console.log('rs', rs)
    rs.lang = lang;
    rs.JumpName = '';
    commonNav.init(rs);
    rs.domainName = domainName;
    if (store.get('share-info').from && store.get('share-info').from == 'group') {
        rs.commonNavGoToWhere = '/group/' + rs.share_info.short_link
    } else {
        rs.commonNavGoToWhere = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.share_info.project_id + '&clear_top=true' :
            '/' + rs.share_info.short_link +
            (from == 'OVOXPeduliSehat' ? '?from=OVOXPeduliSehat' : '');
    }

    // rs.commonNavGoToWhere = '/' + rs.share_info.short_link;
    if (store.get('share-info').data.is_first_share) {
        rs.is_first_share = store.get('share-info').data.is_first_share
        rs.coop_name = store.get('share-info').data.coop_name
        rs.money = store.get('share-info').data.money
        rs.text = store.get('share-info').data.text
    }

    $UI.append(mainTpl(rs));
    fastclick.attach(document.body);

    utils.hideLoading();

    $('body').on('click', '.view-all-btn', function(e) {
        // location.href = '/campaignList.html'
        location.href = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/main?index=0' :
            (from == 'OVOXPeduliSehat' ? '/OVOXPeduliSehat.html' : '/campaignList.html');
    });

    $('body').on('click', '.review-this', function(e) {
        if (store.get('share-info').from && store.get('share-info').from == 'group') {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/special_projects?short_link=' + short_link : '/group/' + short_link
        } else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + res.project_id : '/' + short_link
        }
    });

    // $('body').on('click','.view-all-btn','#goBack',function(e){
    //     store.remove('un-login')
    //     store.remove('share-info')
    // })

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensorsActive.init();

};

obj.insertData = function(rs) {
    if (rs.data.length != 0) {
        rs.fromWhichPage = 'donateShareSuccess.html'
        campaignListComponents.init(rs);
    }
}

export default obj;