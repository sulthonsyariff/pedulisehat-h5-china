var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'donateShareSuccess',
    htmlFileURL: 'html/donateShareSuccess.html',
    appDir: 'js/donateShareSuccess',
    uglify: true,
    hash: '',
    mode: 'development'
})