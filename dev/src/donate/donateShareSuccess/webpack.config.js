var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'donateShareSuccess',
    htmlFileURL: 'html/donateShareSuccess.html',
    appDir: 'js/donateShareSuccess',
    uglify: true,
    hash: '',
    mode: 'production'
})