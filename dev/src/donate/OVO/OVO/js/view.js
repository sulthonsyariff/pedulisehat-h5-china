import mainTpl from '../tpl/main.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'
// import sensorsActive from 'sensorsActive'

/* translation */
// import ovo from 'ovo'
// import qscLang from 'qscLang'
// import commonTitle from 'commonTitle'
// let titleLang = qscLang.init(commonTitle);
// let lang = qscLang.init(ovo);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];
let fromCalculator = reqObj['fromCalculator'];
let category_id = reqObj['category_id'];

// let amount = reqObj['amount'];
// let order_id = reqObj['order_id'];

obj.init = function(rs) {
    // rs.lang = lang;
    // rs.JumpName = titleLang.OVO;
    if (fromCalculator == 'zakatCalculator') {
        rs.commonNavGoToWhere = '/zakatCalculator.html';

    } else if (rs.group_id) {
        rs.commonNavGoToWhere = '/donateMoney.html?short_link=' + short_link + '&group_id=' + rs.group_id
    } else {
        // if (store.get('test-AB')){
        //     rs.commonNavGoToWhere = '/donateMoneyAB.html?project_id=' + rs.project_id +
        //     '&short_link=' + short_link +
        //     (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
        // } else {
        // if (category_id == 15) {
        //     rs.commonNavGoToWhere = '/donateMoney2.html?project_id=' + rs.project_id +
        //         '&short_link=' + short_link +
        //         (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
        // } else {
        rs.commonNavGoToWhere = '/donateMoney.html?project_id=' + rs.project_id +
            '&short_link=' + short_link +
            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
        // }
        // }
    }

    console.log('commonNavGoToWhere', rs.commonNavGoToWhere)
    commonNav.init(rs);

    rs.domainName = domainName;
    rs.amount = changeMoneyFormat.moneyFormat(parseInt(rs.amount));
    // rs.order_id = order_id

    // 自动添加上0
    console.log('----', rs.phone)
    rs.phone = '0' + rs.phone.replace(/^0+/, "");

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);
    clickHandle();

    $('.number').bind('input propertychange', function() {
        console.log('numberChange')
        if ($('.number').val()) {
            $('.delete').css('display', 'block');
        } else {
            // console.log($('.number').val())
            $('.delete').css('display', 'none');
        }
    });

    fastclick.attach(document.body);
    utils.hideLoading();

    // payBtn
    $('body').on('click', '.payBtn', function(e) {
        $UI.trigger('check_pay');
    });

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensorsActive.init();

};

function clickHandle(res) {
    $('body').on('click', '.delete', function() {
        $('.number').val('');
        // $('.search-bar').val('');
        $('.delete').css('display', 'none')
            // store.remove('search');
    })

}




export default obj;