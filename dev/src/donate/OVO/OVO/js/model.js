import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

obj.ovoSupport = function(o) {
    var url = domainName.trade + '/v2/ovo_support';
    if (isLocal) {
        url = 'mock/v1_ovo_support.json'
    }

    ajaxProxy.ajax({
        type: 'POST',
        url: url,
        // data: o.param,
        data: JSON.stringify(o.param),
    }, o)
}

export default obj;