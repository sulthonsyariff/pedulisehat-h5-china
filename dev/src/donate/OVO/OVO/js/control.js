import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let group_id = reqObj['group_id'];
let amount = reqObj['amount'];
let order_id = reqObj['order_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];
let phone = reqObj['phone'];

/* translation */
import ovo from 'ovo'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(ovo);
/* translation */

let param = {};
param.lang = lang;
param.amount = amount;
param.order_id = order_id;
param.group_id = group_id;
param.project_id = project_id;
param.JumpName = titleLang.OVO;

model.getUserInfo({
    success: function(rs) {
        // 隐藏loading
        utils.hideLoading();

        param.phone = rs.data.mobile; //登录后有手机号
        view.init(param);
    },
    unauthorizeTodo: function(rs) {
        // 隐藏loading
        utils.hideLoading();

        param.phone = phone; //未登录从前一页面带入
        view.init(param);
    },
    error: utils.handleFail
});

// submit
$UI.on('check_pay', function(e) {
    // let mobile = $('#number').val().replace(/\b(0+)/gi, "");
    let mobile = $('#number').val();

    let postParam = {
        order_id: order_id,
        phone: mobile
    }

    if (check(postParam)) {

        console.log(postParam);

        model.ovoSupport({
            param: postParam,
            success: function(rs) {
                utils.showLoading(lang.lang8);

                if (rs.code == 0) {
                    console.log('rs = ', rs);
                    setTimeout(() => {
                        if (group_id) {
                            location.href = '/OVOTransaction.html?group_id=' + group_id +
                                '&order_id=' + order_id +
                                '&mobile=' + mobile +
                                '&amount=' + amount +
                                '&short_link=' + short_link +
                                (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                        } else {
                            location.href = '/OVOTransaction.html?project_id=' + project_id +
                                '&order_id=' + order_id +
                                '&mobile=' + mobile +
                                '&amount=' + amount +
                                '&short_link=' + short_link +
                                (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                        }
                    }, 300);
                } else {
                    utils.alertMessage(rs.msg)
                }
            },

            error: utils.handleFail
        });
    }
});

// check phone number
function check(params) {
    if (params.phone.length < 7) {
        utils.alertMessage(lang.lang1);
        return false;
    }

    return true;
}