var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'OVO',
    htmlFileURL: 'html/OVO.html',
    appDir: 'js/OVO',
    uglify: true,
    hash: '',
    mode: 'development'
})