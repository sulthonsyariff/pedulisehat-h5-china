var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'OVOTransaction',
    htmlFileURL: 'html/OVOTransaction.html',
    appDir: 'js/OVOTransaction',
    uglify: true,
    hash: '',
    mode: 'production'
})