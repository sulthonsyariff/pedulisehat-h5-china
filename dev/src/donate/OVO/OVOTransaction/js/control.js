import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let group_id = reqObj['group_id'];
let short_link = reqObj['short_link'];
let amount = reqObj['amount'];
let order_id = reqObj['order_id'];
let mobile = reqObj['mobile'];
let from = reqObj['from'];

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

/* translation */
import OVOTransaction from 'OVOTransaction'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(OVOTransaction);
/* translation */

let param = {};
param.lang = lang;
param.order_id = order_id;
param.project_id = project_id;
param.JumpName = titleLang.OVO;
param.mobile = mobile;

// 隐藏loading
utils.hideLoading();

view.init(param);

// 轮询结果
let timer = setInterval(() => {
    judgeOrderState();
}, 3000);

/*
 * judge order state
 1 未支付
 2 已支付
 3 提现
 4 退款
 5 返还
 10 手机号有问题
 100 取消订单
 */
function judgeOrderState() {
    model.getOrderState({
        param: order_id,
        success: function(res) {
            if (res.code == 0) {
                // console.log('res = ', res);
                // 2 已支付
                if (res.data.state == 2) {
                    //sensors
                    // sensors.track('PayOrder', {
                    //     from: document.referrer,
                    //     order_id: order_id,
                    //     order_amount: amount,
                    //     is_succeed: true
                    // })

                    // 安卓项目
                    if (appVersionCode > 128 && project_id) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' +
                            encodeURIComponent(project_id) +
                            '&order_id=' + encodeURIComponent(order_id);

                    }
                    // 安卓专题
                    else if (appVersionCode >= 202 && group_id && short_link) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?order_id=' +
                            encodeURIComponent(order_id) +
                            '&short_link=' + encodeURIComponent(short_link);

                    }
                    // H5项目+专题
                    else {
                        location.href = '/commonDonateSuccess.html?order_id=' + order_id +
                            '&short_link=' + short_link +
                            // '&terminal=' + res.data.terminal +
                            (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                            (group_id ? ('&group_id=' + group_id) :
                                ('&project_id=' + project_id)) +
                            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    }


                    // if (appVersionCode > 128) {
                    //     location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' + project_id + '&order_id=' + order_id;
                    // } else {
                    //     if (project_id) {
                    //         //统一捐赠成功页
                    //         location.href = '/commonDonateSuccess.html?project_id=' + project_id +
                    //             '&short_link=' + short_link +
                    //             '&order_id=' + order_id +
                    //             (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    //     } else {
                    //         //统一捐赠成功页
                    //         location.href = '/commonDonateSuccess.html?group_id=' + group_id +
                    //             '&short_link=' + short_link +
                    //             '&order_id=' + order_id +
                    //             (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    //     }
                    // }
                }
                // 0 手机号有问题 ？
                else if (res.data.state == 10) {
                    //sensors
                    // sensors.track('PayOrder', {
                    //     from: document.referrer,
                    //     order_id: order_id,
                    //     order_amount: amount,
                    //     is_succeed: false
                    // })

                    store.set('ovoErrorMessageStore', res.data.detail);
                    // console.log('fail!');
                    if (project_id) {
                        location.href = '/commonDonateFailure.html?project_id=' + project_id +
                            '&short_link=' + short_link +
                            '&order_id=' + order_id +
                            '&amount=' + amount +
                            '&errorMsg=true' +
                            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                        // location.href = '/OVOError.html?project_id=' + project_id +
                        //     '&short_link=' + short_link +
                        //     '&order_id=' + order_id +
                        //     '&amount=' + amount +
                        //     (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    } else {
                        location.href = '/commonDonateFailure.html?group_id=' + group_id +
                            '&short_link=' + short_link +
                            '&order_id=' + order_id +
                            '&amount=' + amount +
                            '&errorMsg=true' +
                            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                        // location.href = '/OVOError.html?group_id=' + group_id +
                        //     '&short_link=' + short_link +
                        //     '&order_id=' + order_id +
                        //     '&amount=' + amount +
                        //     (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    }

                    // utils.alertMessage(res.data.detail);
                    // clearInterval(timer);
                }
                // 支付失败
                else if (res.data.state != 1) {
                    //sensors
                    // sensors.track('PayOrder', {
                    //         from: document.referrer,
                    //         order_id: order_id,
                    //         order_amount: amount,
                    //         is_succeed: false
                    //     })
                        // console.log('fail!');
                    if (project_id) {
                        location.href = '/commonDonateFailure.html?project_id=' + project_id +
                            '&short_link=' + short_link +
                            '&order_id=' + order_id +
                            '&amount=' + amount +
                            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                        // location.href = '/OVOError.html?project_id=' + project_id +
                        //     '&short_link=' + short_link +
                        //     '&order_id=' + order_id +
                        //     '&amount=' + amount +
                        //     (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    } else {
                        location.href = '/commonDonateFailure.html?group_id=' + group_id +
                            '&short_link=' + short_link +
                            '&order_id=' + order_id +
                            '&amount=' + amount +
                            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                        // location.href = '/OVOError.html?group_id=' + group_id +
                        //     '&short_link=' + short_link +
                        //     '&order_id=' + order_id +
                        //     '&amount=' + amount +
                        //     (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    }
                }
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}