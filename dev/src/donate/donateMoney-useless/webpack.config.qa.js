var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'donateMoneyHide',
    htmlFileURL: 'html/donateMoneyHide.html',
    appDir: 'js/donateMoneyHide',
    uglify: true,
    hash: '',
    mode: 'development'
})