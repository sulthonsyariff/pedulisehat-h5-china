import mainTpl from '../tpl/main.juicer'
import _zakatIntentionTpl from '../tpl/_zakatIntention.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import bankList from './_bankList'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
import store from 'store'
import _validate from './_validate';
import _validateZakat from './_validateZakat'; //项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
import campaignCard from '../tpl/_campaignCard.juicer'
// import sensorsActive from 'sensorsActive'
import getSubmitParam from "./_getSubmitParam";
// import moneyInputChange from './_moneyInputChange'

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];
let fromLogin = reqObj['fromLogin'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0
// let appVersionName = utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName')


// let arr = [];
let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* translation */
import donateMoney from 'donateMoney'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateMoney);
/* translation */

let getLanguage = utils.getLanguage();
let submitData;
let category_id;
let anonymous = 'non-anonymous';
let is_comment;

obj.getProjInfo = function (rs) {
    category_id = rs.data.category_id;
    rs.lang = lang;
    rs.getLanguage = getLanguage;
}

obj.init = function (rs) {
    if (rs.getProjInfoData && rs.getProjInfoData.data && rs.getProjInfoData.data.category_id == 13) {
        rs.JumpName = 'Zakat';
        $('title').html('Zakat');
    } else {
        rs.JumpName = titleLang.donate;
        $('title').html(titleLang.donate);
    }

    // rs.commonNavGoToWhere = utils.browserVersion.android ?
    //     'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true' :
    //     '/' + short_link + (from ? ('?from=' + from) : '');
    rs.commonNavGoToWhere = commonNavGoToWhere();
    rs.lang = lang;
    // console.log('======B=======', rs.lang)

    commonNav.init(rs);

    for (let i = 0; i < rs.data.length; i++) {
        // handle image
        if (rs.data[i].image) {
            rs.data[i].image = JSON.parse(rs.data[i].image);
        }
    }

    // 关闭AB测试， 留下B版
    rs.detailDonateAB = "detailDonate-b";
    if (rs.user_id) {} else {
        rs.touristLoginAB = 'b'
    }
    // console.log('rs.touristLoginAB', rs.touristLoginAB)

    rs.from = from;
    console.log('from', from, typeof (from))
    $UI.append(mainTpl(rs));
    $('.campaignCard').append(campaignCard(rs.getProjInfoData)); //项目卡片
    $UI.append(_zakatIntentionTpl(rs)); //zakat项目捐款须知
    bankList.init(rs); // OVO合作项目只需要展示OVO
    commonFooter.init(rs);
    judgeCanRec(rs); //判断能否联合支付

    // sensorsActive.init();

    // 隐藏loading
    utils.hideLoading();

    fastclick.attach(document.body);
    obj.event(rs);
    changeMoneyFormat.init('#target_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

// 返回
function commonNavGoToWhere() {
    if (utils.browserVersion.android) {
        return 'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true';
    }
    // else if (document.referrer.indexOf('supporterRanking') != -1) {
    //     return document.referrer; // 上一个页面的URL: document.referrer
    // } 
    else {
        return '/' + short_link + (from ? ('?from=' + from) : '');
    }
}

/**
 * 判断能否联合支付:
 *
 * regular 项目、 bulk 项目捐款页， 增加给互助项目捐款的联合支付功能
 * category_id == 13 为zakat类项目
 */
function judgeCanRec(rs) {
    if (category_id == 12 || category_id == 14) {
        $UI.trigger('canRec', rs);
    }
}

obj.showRecommendationAmountList = function ($this) {
    if ($($this).hasClass('add')) {
        $('.recommendationAmountList').css('display', 'block');
    } else {
        $('.recommendationAmountList').css('display', 'none');
    }
}

obj.event = function (rs) {
    // moneyInputChange.init(rs);//监听input输入

    // submit
    $UI.on('click', '.payment_channel', function (rs) {
        $('.alert-bank').css('display', 'block')
    })

    // choose money card
    // $UI.on('click', '.amount-card', function() {
    //     if ($(this).hasClass('chosed')) {
    //         $('.amount-card').removeClass('chosed');
    //     } else {
    //         $('.amount-card').removeClass('chosed');
    //         $(this).addClass('chosed');
    //     }

    // });

    // open or close anonymous
    $UI.on('click', '.open-icon', function () {
        $(this).toggleClass('open');
    });

    $UI.on('click', '#target_amount', function () {
        $('#target_amount').val('');
        $('.amount-card.chosed').removeClass('chosed');
    })

    // Intention Before Zakat : read
    $UI.on('click', '.intention-before-zakat #readIntention', function () {
        $(this).parents().find('.content-inner').toggleClass('active');
    })

    // Intention Before Zakat : close
    $UI.on('click', '.intention-before-zakat .close', function (rs) {
        $('.intention-before-zakat').css('display', 'none')
    })

    // Intention Before Zakat:confirm
    $UI.on('click', '.intention-before-zakat .content-inner.active .confirm', function () {
        utils.showLoading(lang.lang12);
        $UI.trigger('submit-b', [submitData]);
    })
    // donate-B login
    $UI.on('click', '.login', function () {
        location.href = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/login?req_code=600' : '/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=loginB')
        // location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=loginB')
    })

    // 判断项目分类ID，如果为13说明是zakat项目，则想要弹出zakat捐款须知
    // submit
    $UI.on('click', '.nextBtn', function () {
        submitData = getSubmitParam.init();

        console.log('submitData = ', submitData)
        if (submitData.comment) {
            is_comment = true
        } else {
            is_comment = false
        }
        // sensors.track('DonateInfoFill', {
        //     from: document.referrer,
        //     payment_method: submitData.payment_channel,
        //     order_amount: submitData.money,
        //     is_anonim: anonymous,
        //     is_comment: is_comment,
        // })

        // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
        if ((category_id == 13 && _validateZakat.check(submitData, lang)) ||
            (category_id != 13 && _validate.check(submitData, lang))) {
            store.set('donateMoney', submitData);
            if (category_id == 13) {
                $('.intention-before-zakat').css('display', 'block');
                $('.intention-before-zakat .money span').html($('#target_amount').val());
            } else {
                utils.showLoading(lang.lang12);
                $UI.trigger('submit-b', [submitData]);
            }
        }

    });

};

/**
 * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 4
 */
// function getSubmitParam() {
// let terminal;
// let detail_page;

// if (store.get('test-AB') && store.get('test-AB') == 'a') {
//     detail_page = 'original'
// } else {
//     detail_page = 'rotate_play'
// }
// if (appVersionCode) {
//     terminal = 21

// } else {
//     terminal = 10
// }

// let pay_platform; //1.faspay 2. gopay 3 sinamas 4ovo

// let bank_code = $(".payment_channel .bank-name").attr('data-bankCode');
// let support_price = $('#target_amount').val().replace(/\./g, '') || $('.amount-card.chosed span').text().replace(/\./g, '');
// let privacy_policy = $('.open-icon').hasClass('open') ? 2 : 1

// // gopay
// if (bank_code == '1') {
//     pay_platform = 2;
// }
// // sinamas
// else if (bank_code == '2') {
//     pay_platform = 3;
// }
// // ovo
// else if (bank_code == '4') {
//     pay_platform = 4;
// }
// // dana
// else if (bank_code == '5') {
//     pay_platform = 5;
// }
// // faspay
// else {
//     pay_platform = 1;
// }


// return {
//     avatar: '',
//     user_name: $('.name-box').val() || '',
//     phone: $('#mobile').val() || '',
//     money: parseInt(support_price),
//     comment: $("textarea[name=commentArea]").val(), //备注：$('#story').val()不能获取到值？？？
//     project_id: project_id,
//     payment_channel: bank_code,
//     payment_channel_name: $(".payment_channel .bank-name").text(),
//     terminal: terminal,
//     pay_platform: pay_platform,
//     privacy_policy: privacy_policy,
//     ab_test: {
//         'detail_page': detail_page // AB 测试需要
//     },
//     reference: (from == 'OVOXPeduliSehat') ? 'OVO' : '', // OVO合作项目

//     version_code: appVersionCode,
//     version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
// }
// }





export default obj;