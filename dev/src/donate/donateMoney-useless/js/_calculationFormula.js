import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

let obj = {};
let $UI = $('body');
obj.UI = $UI;

/**
 * 修改计算公式：
 *
 * campaignMoney 捐款金额
 * recommendationAmount 互助产品
 *
 */
obj.init = function() {
    let total = 0;
    let recommendationAmount;
    let campaignMoney = parseInt($('.campaign-money').attr('data-amount') || 0);

    if ($('.recommendationAmountList').css('display') == 'block') {
        recommendationAmount = parseInt($('.recmdt_amount').attr('data-amount'));
    } else {
        recommendationAmount = 0;
    }

    total = campaignMoney + recommendationAmount;

    $('#price').attr('data-amount', total);
    $('#price').html(changeMoneyFormat.moneyFormat(total));

    console.log('*** 重新计算资金公式 ***', );
    console.log('campaignMoney:', campaignMoney);
    console.log('Gotongroyong Donation:', recommendationAmount);
    console.log('total:', total);
};

export default obj;