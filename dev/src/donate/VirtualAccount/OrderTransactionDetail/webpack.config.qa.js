var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'Order Transaction Detail',
    htmlFileURL: 'html/OrderTransactionDetail.html',
    appDir: 'js/OrderTransactionDetail',
    uglify: true,
    hash: '',
    mode: 'development'
})