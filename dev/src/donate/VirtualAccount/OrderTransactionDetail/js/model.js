import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getOrderState = function(o) {
    let url = domainName.trade + '/v1/support/' + o.param.order_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

export default obj;