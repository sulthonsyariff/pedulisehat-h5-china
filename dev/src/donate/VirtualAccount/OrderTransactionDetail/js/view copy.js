// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import howToPayTpl from '../tpl/howToPay.juicer'
import listItem from '../tpl/listItem.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils'
import store from 'store'
import changeMoneyFormat from 'changeMoneyFormat'
import html2canvas from 'html2canvas';
// import sensorsActive from 'sensorsActive'
import domainName from 'domainName' // port domain name

// 复制粘贴
import ClipboardJS from 'clipboard'
new ClipboardJS('.copy-btn');
/* translation */
import sinarmas from 'sinarmas'
import qscLang from 'qscLang'
let lang = qscLang.init(sinarmas);
/* translation */

let [obj, $UI] = [{}, $('body')];
let project_id = utils.getRequestParams().project_id;
let group_id = utils.getRequestParams().group_id;
var short_link = utils.getRequestParams().short_link;
let channel = utils.getRequestParams().channel;
let fromCalculator = utils.getRequestParams().fromCalculator;
// var category_id = utils.getRequestParams().category_id;

let CACHED_KEY = 'VA';
let VAorder = JSON.parse(sessionStorage.getItem(CACHED_KEY));
console.log('bill_no', bill_no)
let bill_no = VAorder.NoRef
let SourceID = VAorder.SourceID;
let IssueDate = VAorder.IssueDate;
let VirtualAccountNumber = VAorder.VirtualAccountNumber;
let Amount = VAorder.Amount;
let from = VAorder.from;



// let source = '$oldUA;language=${AppConstants.Http.getDeviceLan()};appVersionName=${AppUtil.getAppVersionName(view.getContext())};appVersionCode=126;asdsad'
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

// console.log('source', source)
console.log('appVersionCode', appVersionCode)

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (channel == '2') {
        res.JumpName = 'Sinarmas Virtual Account';
    } else if (channel == '707') {
        res.JumpName = 'Alfagroup Virtual Account'
    } else if (channel == '801') {
        res.JumpName = 'BNI Virtual Account'
    } else if (channel == '708') {
        res.JumpName = 'Danamon Virtual Account'
    } else if (channel == '802') {
        res.JumpName = 'Mandiri Virtual Account'
    } else if (channel == '408') {
        res.JumpName = 'MayBank Virtual Account'
    } else if (channel == '402') {
        res.JumpName = 'Permata Virtual Account'
    } else if (channel == '702') {
        res.JumpName = 'BCA Virtual Account'
    } else if (channel == '800') {
        res.JumpName = 'BRI Virtual Account'
    }



    commonNav.init(res);
    if (fromCalculator == 'zakatCalculator') {
        res.commonNavGoToWhere = '/zakatCalculator.html';

    } else if (project_id) {
        // if (category_id == 15) {
        //     res.commonNavGoToWhere = '/donateMoney2.html?project_id=' + project_id + '&short_link=' + short_link;
        // } else {
        res.commonNavGoToWhere = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link;
        // }

        // res.commonNavGoToWhere = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link;
    } else {
        res.commonNavGoToWhere = '/donateMoney.html?short_link=' + short_link + '&group_id=' + group_id
    }
    res.lang = lang;
    res.getLanguage = utils.getLanguage();
    res.SourceID = SourceID;
    // res.IssueDate = IssueDate;
    // res.NoRef = NoRef;
    res.VirtualAccountNumber = VirtualAccountNumber
    res._VirtualAccountNumber = VirtualAccountNumber.replace(/[^0-9]+/, '').replace(/^0+/, '').replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1 ");
    res.Amount = changeMoneyFormat.moneyFormat(parseInt(Amount));;
    res.project_id = project_id;
    res.channel = channel;

    console.log('====', res)

    let time = timeHandler(IssueDate);
    res.time = time;


    $UI.append(mainTpl(res)); //主模版
    $('.how-to-pay-wrap').append(howToPayTpl(res)); //添加tips

    if (appVersionCode <= 126) {
        console.log('hideBtn')
        $('.saveAsPhoto').css('display', 'none')
    }

    commonFooter.init(res);

    utils.hideLoading();
    fastclick.attach(document.body);

    $('body').on('click', '.copy-btn', function() {
        utils.alertMessage(lang.lang9);
    });
    // back-to-campaign
    $('body').on('click', '.back-to-campaign', function() {
        $UI.trigger('sinarmas', 'flag'); //区分轮询和点击
    });
    // view-all-campaign
    $('body').on('click', '.view-all-campaign', function() {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html'
    });

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();


    $("#captureBtn").click(function() {
        utils.showLoading(lang.lang10);

        $('.wrapper').addClass('photo'); // hide copy button

        // element in your code is a jQuery object, not an element
        html2canvas($('#capture')[0], {
            useCORS: true
        }).then(canvas => {

            // var context = canvas.getContext('2d');
            var url = canvas.toDataURL('png');

            $('.capture-img').attr('src', url);
            $('.canvas').css('display', 'block');

            setTimeout(() => {
                $('.capture-img').css({
                    'margin-left': -($('.capture-img').width() / 2),
                    'display': 'block',
                    'animation': 'screenshot 1.5s ease-in-out',
                    'animation-fill-mode': 'forwards',
                    // 'transform': 'scale(1,0.9)',
                });
                utils.hideLoading();
            }, 200);
        });
    });

    $('body').on('click', '.canvas', function() {
        $('.wrapper').removeClass('photo');
        $('.canvas').css('display', 'none');
        // $('.capture-img').css({
        //     'position': 'absolute',
        //     // 'transform': 'scale(1,0.9)',
        //     'height': '85vh !important',
        //     'left': '50%',
        //     'top': '0%',
        // });
    })

    $(".tab li").click(function() {
        //通过 .index()方法获取元素下标，从0开始，赋值给某个变量
        var _index = $(this).index();
        console.log('index', _index / 2)
            //让内容框的第 _index 个显示出来，其他的被隐藏
        $(".tab-box>div").eq(_index / 2).show().siblings().hide();
        // console.log('----',$(".tab-box>div").eq(_index))
        //改变选中时候的选项框的样式，移除其他几个选项的样式
        $(this).addClass("change").siblings().removeClass("change");
    });


    // store.remove(CACHED_KEY);
};

obj.insertData = function(rs) {
    console.log('insert')
    rs.domainName = domainName;
    let arr = [];

    if (rs.data.length != 0) {
        rs.hotCampaigns = rs.data
        rs._short_link = short_link;

        for (let i = 0; i < rs.data.length; i++) {
            if (!rs.data[i].stopped) {
                arr.push(rs.data[i])
            }
        }

        for (let i = 0; i < rs.data.length; i++) {
            rs.hotCampaigns = rs.data
            console.log('rs.hotCampaigns[i]', rs.hotCampaigns[i].created_at)
                //时间
                // rs.hotCampaigns[i].created_at = raiseDay(rs.hotCampaigns[i].created_at);

            //时间
            if (rs.hotCampaigns[i].closed_at) {
                rs.hotCampaigns[i].timeLeft = utils.timeLeft(rs.hotCampaigns[i].closed_at);
            }
            //时间
            if (rs.hotCampaigns[i].closed_at) {
                rs.hotCampaigns[i].timeLeft = utils.timeLeft(rs.hotCampaigns[i].closed_at);
            }

            rs.hotCampaigns[i].detailHref = utils.browserVersion.android ?
                'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.hotCampaigns[i].project_id + '&clear_top=true' :
                '/' + rs.hotCampaigns[i].short_link;

            // 进度条：设置基准的高度为1%，
            rs.hotCampaigns[i].progress = rs.hotCampaigns[i].current_amount / rs.hotCampaigns[i].target_amount * 100 + 1;

            //   图片
            rs.hotCampaigns[i].cover = JSON.parse(rs.hotCampaigns[i].cover);

            // function imageChoose(souceImg) {
            //     var fauto = souceImg.image.indexOf(",f_auto")
            //     var imageReg = '/image/authenticated/s--[\\w\\-]{8}--/';
            //     var regResult = souceImg.image.match(imageReg)

            //     if (typeof String.prototype.endsWith != 'function') {
            //         String.prototype.endsWith = function (str) {
            //             return this.slice(-str.length) == str;
            //         };
            //     }
            //     var imageEndWithWebp = souceImg.image.endsWith(".webp")

            //     if (regResult && regResult[0]) {
            //         if (imageEndWithWebp && fauto == -1) {
            //             return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120,f_auto/' + souceImg.thumb
            //         }
            //         return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120/' + souceImg.thumb
            //     }
            //     return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120/' + souceImg.image
            // }

            rs.hotCampaigns[i].rightUrl = utils.imageChoose(rs.hotCampaigns[i].cover)
            rs.hotCampaigns[i].avatar = utils.imageChoose(rs.hotCampaigns[i].avatar)

            // 格式化金额
            rs.hotCampaigns[i].target_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].target_amount);
            rs.hotCampaigns[i].current_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].current_amount);

            // 企业用户
            rs.hotCampaigns[i].corner_mark = rs.hotCampaigns[i].corner_mark ? JSON.parse(rs.hotCampaigns[i].corner_mark) : '';
        }
        rs.lang = lang;

        $('.campaigns_list').append(listItem(rs));

        // if row's number more than 2,show the overflow class
        for (let i = 0; i < rs.data.length; i++) {
            if ($(".inner_name" + '_' + i).height() > 32) {
                $('.campaign_name' + '_' + i).addClass('overflow')
            }
        }
        let windowWidth = $(window).width() / 2
        $(".user-name").each(function() {
            console.log('windowWidth', windowWidth)
            if ($(this).width() > windowWidth) {
                $(this).css({
                    'width': windowWidth,
                    'overflow': 'hidden',
                    'white-space': 'nowrap',
                    'text-overflow': 'ellipsis',
                })
            }
        });
        // 如果超过四个项目，则隐藏
        // if ($('.campaign_box').length > 3) {
        //     $($('.campaign_box')[3]).css('display', 'none');
        // }

    }
    rs.lang = lang;
}

function raiseDay(created_at) {

    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}

function timeHandler(IssueDate) {
    console.log('IssueDate', IssueDate)
        // IssueDateString = toString(IssueDate)
        // console.log('IssueDateString',IssueDateString)
    var theString = IssueDate;
    if (from == 'sinarmas') {
        arr = theString.split(" ");
        console.log('===sinarmasArr===', arr)

        var time1 = arr[1];
        hour1 = time1.split(":");
        var hour2 = hour1[0] + ":" + hour1[1]

        var time2 = arr[0]
        date1 = time2.split("/");

        var month = date1[0];
        var day = date1[1];
        var year = date1[2];

    } else if (from == 'faspay') {
        arr = theString.split(" ");
        console.log('===faspayArr===', arr)

        var time1 = arr[1];
        hour1 = time1.split(":");
        var hour2 = hour1[0] + ":" + hour1[1]

        var time2 = arr[0]
        date1 = time2.split("-");

        var day = date1[2];
        var month = date1[1];
        var year = date1[0];
    }

    if (month == 1) {
        month = "Jan";
    } else if (month == 2) {
        month = "Feb";
    } else if (month == 3) {
        month = "Mar";
    } else if (month == 4) {
        month = "Apr";
    } else if (month == 5) {
        month = "May";
    } else if (month == 6) {
        month = "June";
    } else if (month == 7) {
        month = "July";
    } else if (month == 8) {
        month = "Aug";
    } else if (month == 9) {
        month = "Sept";
    } else if (month == 10) {
        month = "Oct";
    } else if (month == 11) {
        month = "Nov";
    } else if (month == 12) {
        month = "Dec";
    }
    return hour2 + ' WIB, ' + day + " " + month + " " + year
}

export default obj;