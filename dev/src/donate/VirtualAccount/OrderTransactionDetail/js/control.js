import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import model_getListData from 'model_getListData' //列表接口调用地址

var $UI = view.UI;
let project_id = utils.getRequestParams().project_id;
let group_id = utils.getRequestParams().group_id;
let short_link = utils.getRequestParams().short_link;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
let CACHED_KEY = 'VA';
let VAorder = JSON.parse(sessionStorage.getItem(CACHED_KEY));

// bill_no字段统一为order_id
// let bill_no = VAorder.NoRef
let order_id = VAorder.NoRef

let Amount = VAorder.Amount;

judgeOrderState();
setInterval(() => {
    judgeOrderState();
}, 10000);

// 点击查询支付状态
$UI.on('sinarmas', function(e, flag) {
    judgeOrderState(flag);
})

function judgeOrderState(flag) {
    model.getOrderState({
        param: {
            project_id: project_id,
            order_id: order_id,
        },
        success: function(res) {
            console.log('getOrderState', res)
            if (res.code == 0) {
                if (res.data.state == 2) {

                    //sensors
                    // sensors.track('PayOrder', {
                    //     from: document.referrer,
                    //     order_id: encodeURIComponent(order_id),
                    //     order_amount: Amount,
                    //     is_succeed: true
                    // })

                    // 安卓项目
                    if (appVersionCode > 128 && project_id) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' +
                            encodeURIComponent(project_id) +
                            '&order_id=' + encodeURIComponent(order_id);

                    }
                    // 安卓专题
                    else if (appVersionCode >= 202 && group_id && short_link) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?order_id=' +
                            encodeURIComponent(order_id) +
                            '&short_link=' + encodeURIComponent(short_link);

                    }
                    // H5项目+专题
                    else {
                        location.href = '/commonDonateSuccess.html?order_id=' + order_id +
                            '&short_link=' + short_link +
                            // '&terminal=' + res.data.terminal +
                            (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                            (group_id ? ('&group_id=' + group_id) :
                                ('&project_id=' + project_id))
                    }

                    // if (project_id) {
                    //     //项目
                    //     location.href = '/commonDonateSuccess.html?project_id=' + project_id +
                    //         '&short_link=' + short_link +
                    //         '&order_id=' + order_id;
                    // } else {
                    //     //专题
                    //     location.href = '/commonDonateSuccess.html?group_id=' + group_id +
                    //         '&short_link=' + short_link +
                    //         '&order_id=' + order_id;
                    // }

                } else if (flag) {
                    //sensors
                    // sensors.track('PayOrder', {
                    //     from: document.referrer,
                    //     order_id: encodeURIComponent(order_id),
                    //     order_amount: Amount,
                    //     is_succeed: false
                    // })


                    if (project_id) {
                        location.href = '/commonDonateFailure.html?project_id=' + project_id + '&short_link=' + short_link;
                        // location.href = '/VAUnFinish.html?project_id=' + project_id + '&short_link=' + short_link;
                    } else {
                        location.href = '/commonDonateFailure.html?group_id=' + group_id + '&short_link=' + short_link;
                        // location.href = '/VAUnFinish.html?group_id=' + group_id + '&short_link=' + short_link;
                    }
                }
            } else {
                utils.alertMessage(res.msg)
            }
        }
    })
}

model_getListData.getListData_OrderTransactionDetail({
    param: {
        project_id: project_id
    }, //过滤project_id项目
    success: function(res) {
        if (res.code == 0) {
            view.insertData(res)
        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
});

view.init({});