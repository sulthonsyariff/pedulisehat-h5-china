var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'Order Transaction Detail',
    htmlFileURL: 'html/OrderTransactionDetail.html',
    appDir: 'js/OrderTransactionDetail',
    uglify: true,
    hash: '',
    mode: 'production'
})