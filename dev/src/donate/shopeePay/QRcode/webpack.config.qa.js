let baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'shopeePayQRcode',
    htmlFileURL: 'html/shopeePayQRcode.html',
    appDir: 'js/shopeePayQRcode',
    uglify: true,
    hash: '',
    mode: 'development'
})