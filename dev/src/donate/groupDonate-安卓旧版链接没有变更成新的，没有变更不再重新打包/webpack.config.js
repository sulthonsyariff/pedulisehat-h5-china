var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'groupDonate',
    htmlFileURL: 'html/groupDonate.html',
    appDir: 'js/groupDonate',
    uglify: true,
    hash: '',
    mode: 'production'
})