import mainTpl from '../tpl/main.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import bankList from './_bankList'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
import store from 'store'
import validate from '../js/_validate';
import campaignCard from '../tpl/_campaignCard.juicer'

let reqObj = utils.getRequestParams();

let short_link = reqObj['short_link'];
let from = reqObj['from'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

// let arr = [];
let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* translation */
import donateMoney from 'donateMoney'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateMoney);
/* translation */

let getLanguage = utils.getLanguage();
let submitData;

obj.init = function(rs) {
    // console.log('init rs = ', rs);

    rs.JumpName = titleLang.donate;
    $('title').html(titleLang.donate);

    rs.commonNavGoToWhere = '/group/' + short_link;
    rs.lang = lang;
    if (!utils.browserVersion.androids) {
        commonNav.init(rs);
    }
    for (let i = 0; i < rs.data.length; i++) {
        // handle image
        if (rs.data[i].image) {
            rs.data[i].image = JSON.parse(rs.data[i].image);
        }
    }

    // 关闭AB测试， 留下B版
    // rs.detailDonateAB = store.get('detail-donateAB');
    rs.detailDonateAB = "detailDonate-b";
    rs.from = from;

    // console.log('rs', rs)
    $UI.append(mainTpl(rs));

    $('.campaignCard').append(campaignCard(rs.getProjInfoData)); //项目卡片

    bankList.init(rs); // OVO合作项目只需要展示OVO

    commonFooter.init(rs);

    // 隐藏loading
    utils.hideLoading();

    fastclick.attach(document.body);
    obj.event(rs);
    changeMoneyFormat.init('#target_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

obj.event = function(rs) {
    // submit
    $UI.on('click', '.payment_channel', function(rs) {
        $('.alert-bank').css('display', 'block')
    })

    // choose money card
    $UI.on('click', '.amount-card', function() {
        if ($(this).hasClass('chosed')) {
            $('.amount-card').removeClass('chosed');
        } else {
            $('.amount-card').removeClass('chosed');
            $(this).addClass('chosed');
        }

    });

    // open or close anonymous
    $UI.on('click', '.open-icon', function() {
        $(this).toggleClass('open');
    });

    // 监听input输入
    $UI.on('input propertychange', '#target_amount', function() {
        if ($('.amount-card.chosed').length && ($('#target_amount').val() != $('.amount-card.chosed').val())) {
            $('.amount-card.chosed').removeClass('chosed');
        }
    })

    $UI.on('click', '#target_amount', function() {
        $('#target_amount').val('');
        $('.amount-card.chosed').removeClass('chosed');
    })

    // 判断项目分类ID，如果为13说明是zakat项目，则想要弹出zakat捐款须知
    // submit
    $UI.on('click', '.nextBtn', function() {
        submitData = getSubmitParam();
        if (validate.check(submitData, lang)) {
            store.set('donateMoney', submitData);
            utils.showLoading(lang.lang12);
            $UI.trigger('submit', [submitData]);
        }
    });
};


obj.getGroupProjInfo = function(rs) {
    console.log('campaignCard=', rs);

    rs.lang = lang;
    rs.getLanguage = getLanguage;
}



/**
 * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 4
 */
function getSubmitParam() {
    let terminal = 10;
    let group_id = store.get('groupDonate') ? store.get('groupDonate').group_id : $.cookie('groupDonate').group_id
    let sign = store.get('groupDonate') ? store.get('groupDonate').sign : $.cookie('groupDonate').sign
    let project_ratio = store.get('groupDonate') ? store.get('groupDonate').project_ratio : $.cookie('groupDonate').project_ratio
    let name = store.get('groupDonate') ? store.get('groupDonate').name : $.cookie('groupDonate').name


    // if (appVersionCode > 128) {
    //     terminal = 21
    // }

    let pay_platform; //1.faspay 2. gopay 3 sinamas 4ovo

    let bank_code = $(".payment_channel .bank-name").attr('data-bankCode');
    let support_price = $('#target_amount').val().replace(/\./g, '') || $('.amount-card.chosed span').text().replace(/\./g, '');
    let privacy_policy = $('.open-icon').hasClass('open') ? 2 : 1

    // gopay
    if (bank_code == '1') {
        pay_platform = 2;
    }
    // sinamas
    else if (bank_code == '2') {
        pay_platform = 3;
    }
    // ovo
    else if (bank_code == '4') {
        pay_platform = 4;
    }
    // faspay
    else {
        pay_platform = 1;
    }

    // console.log('pay_platform=', pay_platform);

    return {
        avatar: '',
        user_name: '',
        phone: '',
        money: parseInt(support_price),
        comment: $("textarea[name=commentArea]").val(), //备注：$('#story').val()不能获取到值？？？
        payment_channel: bank_code,
        payment_channel_name: $(".payment_channel .bank-name").text(),
        terminal: utils.terminal,
        pay_platform: pay_platform,
        privacy_policy: privacy_policy,
        ab_test: {
            'donated_button': store.get('donated_button-AB') // AB 测试需要
        },
        reference: (from == 'OVOXPeduliSehat') ? 'OVO' : '', // OVO合作项目
        group_id: group_id,
        ratio: project_ratio,
        ratio_sign: sign,
        name: name
    }
}





export default obj;