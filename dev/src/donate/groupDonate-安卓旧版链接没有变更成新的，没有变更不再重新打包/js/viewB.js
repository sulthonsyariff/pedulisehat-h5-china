import mainTpl from '../tpl/main.juicer'
// import _zakatIntentionTpl from '../tpl/_zakatIntention.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import bankList from './_bankList'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
import store from 'store'
import validateB from './_validateB';
import campaignCard from '../tpl/_campaignCard.juicer'
// import sensorsActive from 'sensorsActive'

let reqObj = utils.getRequestParams();
// let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];
let fromLogin = reqObj['fromLogin'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0
    // let appVersionName = utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName')


// let arr = [];
let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* translation */
import donateMoney from 'donateMoney'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateMoney);
/* translation */

let getLanguage = utils.getLanguage();
let submitData;
let category_id;
let getGroup;

obj.init = function(rs) {
    console.log('======B=======')

    rs.JumpName = titleLang.donate;


    rs.commonNavGoToWhere = '/group/' + short_link;

    // rs.commonNavGoToWhere = utils.browserVersion.android ?
    //     'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true' :
    //     '/' + short_link + (from ? ('?from=' + from) : '');
    rs.lang = lang;
    if (!utils.browserVersion.androids) {
        commonNav.init(rs);
    }

    for (let i = 0; i < rs.data.length; i++) {
        // handle image
        if (rs.data[i].image) {
            rs.data[i].image = JSON.parse(rs.data[i].image);
        }
    }

    // 关闭AB测试， 留下B版
    rs.detailDonateAB = "detailDonate-b";
    if (rs.user_id) {} else {
        rs.touristLoginAB = 'b'
    }

    rs.from = from;

    $UI.append(mainTpl(rs));

    $('.campaignCard').append(campaignCard(rs.getProjInfoData)); //项目卡片

    // $UI.append(_zakatIntentionTpl(rs)); //zakat项目捐款须知

    bankList.init(rs); // OVO合作项目只需要展示OVO

    commonFooter.init(rs);

    // 隐藏loading
    utils.hideLoading();

    fastclick.attach(document.body);
    obj.event(rs);
    changeMoneyFormat.init('#target_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

obj.event = function(rs) {
    // submit
    $UI.on('click', '.payment_channel', function(rs) {
        $('.alert-bank').css('display', 'block')
    })

    // choose money card
    $UI.on('click', '.amount-card', function() {
        if ($(this).hasClass('chosed')) {
            $('.amount-card').removeClass('chosed');
        } else {
            $('.amount-card').removeClass('chosed');
            $(this).addClass('chosed');
        }

    });

    // open or close anonymous
    $UI.on('click', '.open-icon', function() {
        $(this).toggleClass('open');
    });

    // 监听input输入
    $UI.on('input propertychange', '#target_amount', function() {
        if ($('.amount-card.chosed').length && ($('#target_amount').val() != $('.amount-card.chosed').val())) {
            $('.amount-card.chosed').removeClass('chosed');
        }
    })

    $UI.on('click', '#target_amount', function() {
        $('#target_amount').val('');
        $('.amount-card.chosed').removeClass('chosed');
    })

    // Intention Before Zakat : read
    $UI.on('click', '.intention-before-zakat #readIntention', function() {
        $(this).parents().find('.content-inner').toggleClass('active');
    })

    // Intention Before Zakat : close
    $UI.on('click', '.intention-before-zakat .close', function(rs) {
        $('.intention-before-zakat').css('display', 'none')
    })

    // Intention Before Zakat:confirm
    $UI.on('click', '.intention-before-zakat .content-inner.active .confirm', function() {
            utils.showLoading(lang.lang12);
            $UI.trigger('submit', [submitData]);
        })
        // donate-B login
    $UI.on('click', '.login', function() {
        location.href = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/login?req_code=600' : '/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=loginB')
    })

    // 判断项目分类ID，如果为13说明是zakat项目，则想要弹出zakat捐款须知
    // submit
    $UI.on('click', '.nextBtn', function() {
        submitData = getSubmitParam();
        if (validateB.check(submitData, lang)) {
            store.set('donateMoney', submitData);
            utils.showLoading(lang.lang12);
            console.log('submit-b-data', submitData)
            $UI.trigger('submit-b', [submitData]);
        }
    });

};


obj.getGroupProjInfo = function(rs) {
    // category_id = rs.data.category_id;
    rs.lang = lang;
    rs.getLanguage = getLanguage;

}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
};


// let getCookie;
// if (document.cookie.indexOf('groupDonate')) {
//     getGroup = JSON.parse(getCookie('groupDonate'))
//     console.log('----getCookie', document.cookie.indexOf('groupDonate'),getGroup.group_id)

// }
// getCookie('groupDonate');

/**
 * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 4
 */
function getSubmitParam() {
    let terminal = 10;
    let group_id
    let sign
    let project_ratio
    let name
    if (store.get('groupDonate')) {
        group_id = store.get('groupDonate').group_id
        sign = store.get('groupDonate').sign
        project_ratio = store.get('groupDonate').project_ratio
        name = store.get('groupDonate').name
    } else {
        // group_id = getGroup.group_id
        // sign = getGroup.sign
        // project_ratio = getGroup.project_ratio
        // name = getGroup.name
        group_id = JSON.parse(getCookie('groupDonate')).group_id
        sign = JSON.parse(getCookie('groupDonate')).sign
        project_ratio = JSON.parse(getCookie('groupDonate')).project_ratio
        name = JSON.parse(getCookie('groupDonate')).name
    }

    console.log('group_id', group_id)
        // if (appVersionCode) {
        //     terminal = 21
        // }

    let pay_platform; //1.faspay 2. gopay 3 sinamas 4ovo

    let bank_code = $(".payment_channel .bank-name").attr('data-bankCode');
    let support_price = $('#target_amount').val().replace(/\./g, '') || $('.amount-card.chosed span').text().replace(/\./g, '');
    let privacy_policy = $('.open-icon').hasClass('open') ? 2 : 1

    // gopay
    if (bank_code == '1') {
        pay_platform = 2;
    }
    // sinamas
    else if (bank_code == '2') {
        pay_platform = 3;
    }
    // ovo
    else if (bank_code == '4') {
        pay_platform = 4;
    }
    // dana
    else if (bank_code == '5') {
        pay_platform = 5;
    }
    // faspay
    else {
        pay_platform = 1;
    }


    return {
        avatar: '',
        user_name: $('.name-box').val() || '',
        phone: $('#mobile').val() || '',
        money: parseInt(support_price),
        comment: $("textarea[name=commentArea]").val(), //备注：$('#story').val()不能获取到值？？？
        // project_id: project_id,
        payment_channel: bank_code,
        payment_channel_name: $(".payment_channel .bank-name").text(),
        terminal: utils.terminal,
        pay_platform: pay_platform,
        privacy_policy: privacy_policy,
        // ab_test: {
        //     'tourists_donated': 'tourists_donated_b' // AB 测试需要
        // },
        reference: (from == 'OVOXPeduliSehat') ? 'OVO' : '', // OVO合作项目
        group_id: parseInt(group_id),
        ratio: project_ratio,
        ratio_sign: sign,
        name: name,
        version_code: appVersionCode,
        version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
    }
}





export default obj;