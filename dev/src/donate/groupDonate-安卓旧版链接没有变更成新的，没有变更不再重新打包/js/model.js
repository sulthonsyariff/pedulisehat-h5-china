import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;
// console.log(ajaxProxy);

obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

/**
 * 获取专题信息
 */
obj.getGroupProjInfo = function(o) {
    var url = domainName.project + '/v1/group_project?short_link=' + o.param.short_link;
    if (isLocal) {
        url = 'mock/detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

//get payment
obj.getPayment = function(o) {
    var url = domainName.trade + '/v2/pay_channel' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    if (isLocal) {
        url = 'mock/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//create payment
obj.createPayment = function(o) {
    var url = domainName.trade + '/v1/support_group';

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

//visit create payment
obj.visitCreatePayment = function(o) {
    // var url = domainName.trade + '/v1/anonymity_support';
    var url = domainName.trade + '/v1/tourists_group_support';

    if (isLocal) {
        url = 'mock/v1_anonymity_support.json'
    }
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

export default obj;