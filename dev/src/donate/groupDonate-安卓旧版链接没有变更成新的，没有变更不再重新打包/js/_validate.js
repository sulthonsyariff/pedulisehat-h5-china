/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    if (isNaN(param.money)) {
        utils.alertMessage(lang.lang13);
        return false;
    }
    if (param.comment.length > 140) {
        utils.alertMessage(lang.lang14);
        return false;
    }

    if (!(param.money > 9999 && param.money % 1000 == 0)) {
        $('.target_amount_tips').css('color', '#FF6100')
        return false;
    } else {
        $('.target_amount_tips').css('color', '#999999')
    }

    if (!$(".payment_channel .bank-name").attr('data-bankCode')) {
        utils.alertMessage(lang.lang5);
        return false;
    }

    return true;
}

export default obj;