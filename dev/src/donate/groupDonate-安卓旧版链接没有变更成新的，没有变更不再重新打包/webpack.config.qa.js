var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'groupDonate',
    htmlFileURL: 'html/groupDonate.html',
    appDir: 'js/groupDonate',
    uglify: true,
    hash: '',
    mode: 'development'
})