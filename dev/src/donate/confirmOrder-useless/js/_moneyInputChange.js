import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import _calculationFormula from "./_calculationFormula";

let obj = {};
let $UI = $('body');
obj.UI = $UI;

// let reqObj = utils.getRequestParams();
// let project_id = reqObj['project_id'];
// let reqSelect = location.href
let category_id;

let bankCodeArr_1 = [302, 1, 4, 5]; //LinkAja, GoPay, OVO, and DANA
let bankCodeArr_2 = [2, 801, 802, 402, 708, 408, 302, 1, 4, 5];
// Sinarmas Virtual Account, BNI Virtual Account,
// Mandiri Virtual Account, Permata Virtual Account,
// Danamon Virtual Account, Maybank Virtual Account,
// LinkAja, GoPay, OVO, and DANA

obj.init = function (res) {
    category_id = res.category_id;

    let inputMoney = parseInt($('.total-money').attr('total-price').replace(/\./g, '')); //支付金额

    // propertychange
    // //监听input输入
    // $UI.on('focusout', '#target_amount', function () {
    //     let inputMoney = parseInt($('#target_amount').val().replace(/\./g, '') || 0); //支付金额
    //     console.log('===inputMoney===', inputMoney);

    handleAmount(inputMoney); //处理联合互助产品金额+捐款金额
    handleBankList(res, inputMoney); //处理银行列表
    // })
}

function handleAmount(inputMoney) {

    if (!inputMoney) {
        // console.log('1111');
        handleCampaignDonation(0);
        // handleRecMoney(3000);

        // $('.amount-show,.rec-show').css('display', 'none'); //隐藏资金公式 + 互助
    } else {
        // console.log('222');

        handleCampaignDonation(inputMoney);
        // judgeAndHandleRecMoney(inputMoney);

        // _calculationFormula.init(); //资金公式

        // $('.amount-show,.rec-show').css('display', 'block'); //展示资金公式 + 互助
    }
}

/*****
 * 处理联合互助产品金额

 1.捐款金额≦ Rp 10.000, 联合支付金额Rp 3.000
 2.Rp 10.000＜ 捐款金额≦ Rp 50.000， 联合支付金额Rp 5.000
 3.捐款金额＞ Rp 50.000， 联合支付金额Rp 10.000
 */
// function judgeAndHandleRecMoney(inputMoney) {
//     if (inputMoney > 50000) {
//         handleRecMoney(10000);
//     } else if (inputMoney > 10000 &&
//         (inputMoney < 50000 || inputMoney == 50000)) {
//         handleRecMoney(5000);
//     } else {
//         handleRecMoney(3000);
//     }
// }

/**
 * 联合互助产品金额展示
 */
// function handleRecMoney(money) {
//     $('.recmdt_amount,.rec-money').html(changeMoneyFormat.moneyFormat(money));
//     $('.rec-money,.recmdt_amount').attr('data-amount', money);
// }

/****
 *  项目捐助金额
 */
function handleCampaignDonation(inputMoney) {
    $('.campaign-money').attr('data-amount', inputMoney);
    $('.campaign-money').html(changeMoneyFormat.moneyFormat(inputMoney));
}

/****
 * 不同项目不同的比例:
 *
 * "ratio":比例
 * "type": 1不包含 2包含
 *
一.平台费包含渠道费，2.5% （ratio=0.025,type=2）
        1. Rp 1.000 ≦ 支付金额 ＜ Rp 110.000
            展示 bankCodeArr_1 ,其他渠道置灰不可点击
        2. Rp 110.000≦支付金额＜ Rp 200.000
            展示 bankCodeArr_2 ,其他渠道置灰不可点击
        3. 支付金额 ≧ Rp 200.000,
            展示所有支付渠道。
二.平台费包含渠道费，3.5%（ratio=0.035,type=2）
        1. Rp 1.000 ≦ 支付金额 ＜ Rp 110.000
            展示 bankCodeArr_1 ,其他渠道置灰不可点击
        2. 支付金额 ≧ Rp 110.000
            展示所有支付渠道。

三.平台费不包含渠道费 (type=1)
        1. Rp 1.000 ≦ 支付金额 ＜ Rp 10.000
            展示 bankCodeArr_1 ,其他渠道置灰不可点击
        2. 支付金额 ≧ Rp 10.000
            展示所有支付渠道。
 *
 */
function handleBankList(res, inputMoney) {
    console.log('handleBankList', inputMoney);


    if (category_id == 15) {

        hideQurbanGopay();
        if (inputMoney > 2000000) {
            console.log('hide bankArr 1');

            handleQurban(bankCodeArr_1, res); //？？？
        }
    } else if (inputMoney > 2000000) {
        console.log('hide bankArr 1');

        handleQurban(bankCodeArr_1, res); //？？？
    } else if (res.ratioData.ratio == 0.025 && res.ratioData.type == 2) {
        console.log('平台费包含渠道费，2.5% ');

        if ((inputMoney == 1000 || inputMoney > 1000) && inputMoney < 110000) {
            handle(bankCodeArr_1, res); //？？？

        } else if ((inputMoney == 110000 || inputMoney > 110000) && inputMoney < 200000) {
            handle(bankCodeArr_2, res);

        } else if (inputMoney == 200000 || inputMoney > 200000) {
            // 展示所有支付渠道。
            handleAll(res);
        }

    } else if (res.ratioData.ratio == 0.035 && res.ratioData.type == 2) {
        console.log('平台费包含渠道费，3.5% ');

        if ((inputMoney == 1000 || inputMoney > 1000) && inputMoney < 110000) {
            handle(bankCodeArr_1, res);

        } else if (inputMoney == 110000 || inputMoney > 110000) {
            // 展示所有支付渠道。
            handleAll(res);
        }

    } else if (res.ratioData.type == 1) {
        console.log('平台费不包含渠道费');

        if ((inputMoney == 1000 || inputMoney > 1000) && inputMoney < 10000) {
            handle(bankCodeArr_1, res);

        } else if (inputMoney == 10000 || inputMoney > 10000) {
            // 展示所有支付渠道。
            handleAll(res);
        }
    }
}


function hideQurbanGopay() {
    $('.list-items[list-bankcode=1]').css({
        'pointer-events': 'none',
        'color': '#ccc',
        'opacity': '0.2'
    })
}

function handleQurban(bankCodeArr, res) {
    $('.list-items').css({
        'pointer-events': 'auto',
        'color': '#333',
        'opacity': '1.0'
    })
    // 展示bankCodeArr， 其他渠道置灰不可点击
    for (let i = 0; i < bankCodeArr.length; i++) {
        $('.list-items[list-bankcode=' + bankCodeArr[i] + ']').css({
            'pointer-events': 'none',
            'color': '#ccc',
            'opacity': '0.2'
        })

    }
}


function handle(bankCodeArr, res) {
    $('.list-items').css({
        'pointer-events': 'none',
        'color': '#ccc',
        'opacity': '0.2'
    })

    // 展示bankCodeArr， 其他渠道置灰不可点击
    for (let i = 0; i < bankCodeArr.length; i++) {
        $('.list-items[list-bankcode=' + bankCodeArr[i] + ']').css({
            'pointer-events': 'auto',
            'color': '#333',
            'opacity': '1.0'
        })

    }
}



// function handle(bankCodeArr, res) {
//     console.log('bankCodeArr', bankCodeArr);

//     $('.list-items').css({
//         'pointer-events': 'auto',
//         'color': '#333',
//         'opacity': '1.0'
//     })

//     // 展示bankCodeArr， 其他渠道置灰不可点击
//     for (let i = 0; i < bankCodeArr.length; i++) {
//         $('.list-items[list-bankcode=' + bankCodeArr[i] + ']').css({
//             // 'pointer-events': 'auto',
//             // 'color': '#333',
//             // 'opacity': '1.0'
//             'pointer-events': 'none',
//             'color': '#ccc',
//             'opacity': '0.2'
//         })

//     }
// }

function handleAll(res) {
    // 展示所有支付渠道。
    console.log('===handleAll===');

    $('.list-items').css({
        'pointer-events': 'auto',
        'color': '#333',
        'opacity': '1.0'
    })
}

function hideALL(res) {
    console.log('===hideAll===');
    $('.list-items').css({
        'pointer-events': 'none',
        'color': '#ccc',
        'opacity': '0.2'
    })

}

export default obj;