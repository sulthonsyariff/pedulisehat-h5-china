import utils from 'utils'
import store from 'store'
import 'jq_cookie' //ajax cookie

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let from = reqObj['from'];
let statistics_link = reqObj['statistics_link'];
let qurbanName = reqObj['qurbanName'];
let group_id = reqObj['group_id'];
let ifRec = reqObj['ifRec'];
let recMoney = reqObj['recMoney'];
let originPrice = reqObj['originPrice'];

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0;
let client_id = $.cookie('client_id') || '';

/**
 * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 4
 */
obj.init = function() {
    let bank_code = $(".channel").attr('data-bankCode');
    // let support_price = $('#target_amount').val().replace(/\./g, '') || $('.amount-card.chosed span').text().replace(/\./g, '');
    let privacy_policy = $('.open-icon').hasClass('open') ? 2 : 1;
    let support_price = $('.total-money').attr('total-price').replace(/\./g, '');;

    let sign
    let project_ratio
    let name

    if (store.get('groupDonate')) {
        // group_id = store.get('groupDonate').group_id
        sign = store.get('groupDonate').sign
        project_ratio = store.get('groupDonate').project_ratio
        name = store.get('groupDonate').name
    } else {
        // group_id = JSON.parse(getCookie('groupDonate')).group_id
        sign = JSON.parse(getCookie('groupDonate')).sign
        project_ratio = JSON.parse(getCookie('groupDonate')).project_ratio
        name = JSON.parse(getCookie('groupDonate')).name
    }



    let params = {
        avatar: '',
        user_name: $('.name-box').val() || '',
        phone: $('#mobile').val() || '',
        email: $('#email').val(),
        money: parseInt(support_price),
        comment: $("textarea[name=commentArea]").val(), //备注：$('#story').val()不能获取到值？？？
        // project_id: project_id,
        payment_channel: bank_code,
        payment_channel_name: $(".payment_channel .bank-name").text(),
        // terminal: appVersionCode ? 21 : 10,
        terminal: utils.terminal,
        pay_platform: get_pay_platform(bank_code),
        privacy_policy: privacy_policy,
        // ab_test: {
        //     'detail_page': get_detail_page() // AB 测试需要
        // },
        group_id: parseInt(group_id),

        reference: (from == 'OVOXPeduliSehat') ? 'OVO' : '', // OVO合作项目
        version_code: appVersionCode,
        version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
        client_id: client_id,


        ratio: project_ratio,
        ratio_sign: sign,
        name: name,



    }



    return params;


}

// 获取cookie中group订单信息
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
};


//1.faspay 2. gopay 3 sinamas 4ovo
function get_pay_platform(bank_code) {
    // gopay
    if (bank_code == '1') {
        pay_platform = 2;
    }
    // sinamas
    else if (bank_code == '2') {
        pay_platform = 3;
    }
    // ovo
    else if (bank_code == '4') {
        pay_platform = 4;
    }
    // dana
    else if (bank_code == '5') {
        pay_platform = 5;
    }
    // faspay
    else {
        pay_platform = 1;
    }

    return pay_platform;
}

export default obj;