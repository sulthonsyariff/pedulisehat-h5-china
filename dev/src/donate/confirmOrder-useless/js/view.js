import mainTpl from '../tpl/main.juicer'
import _zakatIntentionTpl from '../tpl/_zakatIntention.juicer'
import _qurbanIntention from '../tpl/_qurbanIntention.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
// import bankList from './_bankList'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
import store from 'store'
import _validate from './_validate';
import _validateZakat from './_validateZakat'; //项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
import _validateGroup from './_validateGroup'; //项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
import campaignCard from '../tpl/_campaignCard.juicer'
// import sensorsActive from 'sensorsActive'
import getSubmitParam from "./_getSubmitParam";
import getGroupSubmitParam from "./_getGroupSubmitParam";
import moneyInputChange from './_moneyInputChange'

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let group_id = reqObj['group_id'];
let short_link = reqObj['short_link'];
let originPrice = reqObj['originPrice'];
// let originPrice = store.get('chooseBank').originPrice;
let channelFee = reqObj['channelFee'];;
let dataBankCode = reqObj['dataBankCode'];


let ifRec = reqObj['ifRec'];
let recMoney = reqObj['recMoney'];

let from = reqObj['from'];
let fromLogin = reqObj['fromLogin'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0
// let appVersionName = utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName')


// let arr = [];
let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* translation */
import donateMoney from 'donateMoney'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateMoney);
/* translation */

let getLanguage = utils.getLanguage();
let submitData;
let category_id;
let anonymous = 'non-anonymous';
let is_comment;

obj.getProjInfo = function (rs) {
    category_id = rs.data.category_id;
    rs.lang = lang;
    rs.getLanguage = getLanguage;
}

obj.getGroupProjInfo = function (rs) {
    // category_id = rs.data.category_id;
    rs.lang = lang;
    rs.getLanguage = getLanguage;

}
obj.init = function (rs) {

    // if (rs.getProjInfoData && rs.getProjInfoData.data && rs.getProjInfoData.data.category_id == 13) {
    //     rs.JumpName = 'Zakat';
    //     $('title').html('Zakat');
    // } else if(rs.getProjInfoData && rs.getProjInfoData.data && rs.getProjInfoData.data.category_id == 13){
    //     rs.JumpName = 'Zakat';
    //     $('title').html('Zakat');
    // } else{
    rs.JumpName = lang.lang51;
    $('title').html(lang.lang51);
    // }

    rs.category_id = category_id
    // rs.commonNavGoToWhere = utils.browserVersion.android ?
    //     'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true' :
    //     '/' + short_link + (from ? ('?from=' + from) : '');
    // rs.commonNavGoToWhere = commonNavGoToWhere();
    rs.lang = lang;
    // console.log('======B=======', rs.lang)

    commonNav.init(rs);

    // for (let i = 0; i < rs.data.length; i++) {
    //     // handle image
    //     if (rs.data[i].image) {
    //         rs.data[i].image = JSON.parse(rs.data[i].image);
    //     }
    // }
    if (category_id == 15) {
        rs.totalPrice = changeMoneyFormat.moneyFormat(parseFloat(originPrice) + parseFloat(channelFee))
        rs.originPrice = changeMoneyFormat.moneyFormat(originPrice)
        rs.channelFee = changeMoneyFormat.moneyFormat(channelFee)
    } else {
        rs.totalPrice = changeMoneyFormat.moneyFormat(parseFloat(originPrice) + parseFloat(recMoney))
        rs.originPrice = changeMoneyFormat.moneyFormat(originPrice)
        rs.recMoney = changeMoneyFormat.moneyFormat(recMoney)
        rs.ifRec = ifRec
    }
    // rs.totalPrice = changeMoneyFormat.moneyFormat(parseFloat(originPrice) + parseFloat(channelFee))
    // rs.originPrice = changeMoneyFormat.moneyFormat(originPrice)
    // rs.channelFee = changeMoneyFormat.moneyFormat(channelFee)
    rs.dataBankCode = dataBankCode
    rs.bankImg = store.get('chooseBank').bankImg
    rs.bankName = store.get('chooseBank').bankName

    // 关闭AB测试， 留下B版
    rs.detailDonateAB = "detailDonate-b";
    if (rs.user_id) {} else {
        rs.touristLoginAB = 'b'
    }
    // console.log('rs.touristLoginAB', rs.touristLoginAB)

    rs.from = from;
    console.log('from', from, typeof (from))

    console.log('view=', rs);

    $UI.append(mainTpl(rs));
    // $('.campaignCard').append(campaignCard(rs.getProjInfoData)); //项目卡片
    $UI.append(_zakatIntentionTpl(rs)); //zakat项目捐款须知
    $UI.append(_qurbanIntention(rs)); //zakat项目捐款须知
    // bankList.init(rs); // OVO合作项目只需要展示OVO
    commonFooter.init(rs);
    // judgeCanRec(rs); //判断能否联合支付

    // sensorsActive.init();

    // 隐藏loading
    utils.hideLoading();

    fastclick.attach(document.body);
    obj.event(rs);
    changeMoneyFormat.init('#target_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

// 返回
function commonNavGoToWhere() {
    if (utils.browserVersion.android) {
        return 'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true';
    }
    // else if (document.referrer.indexOf('supporterRanking') != -1) {
    //     return document.referrer; // 上一个页面的URL: document.referrer
    // } 
    else {
        return '/' + short_link + (from ? ('?from=' + from) : '');
    }
}

/**
 * 判断能否联合支付:
 *
 * regular 项目、 bulk 项目捐款页， 增加给互助项目捐款的联合支付功能
 * category_id == 13 为zakat类项目
 */
function judgeCanRec(rs) {
    if (category_id == 12 || category_id == 14) {
        $UI.trigger('canRec', rs);
    }
}

// obj.showRecommendationAmountList = function ($this) {
//     if ($($this).hasClass('add')) {
//         $('.recommendationAmountList').css('display', 'block');
//     } else {
//         $('.recommendationAmountList').css('display', 'none');
//     }
// }

obj.event = function (rs) {
    // moneyInputChange.init(rs);//监听input输入
    console.log('event', rs);

    // submit
    $UI.on('click', '.payment_channel', function (res) {
        $('.alert-bank').css('display', 'block')
        moneyInputChange.init(rs); //监听input输入

    })

    // choose money card
    // $UI.on('click', '.amount-card', function() {
    //     if ($(this).hasClass('chosed')) {
    //         $('.amount-card').removeClass('chosed');
    //     } else {
    //         $('.amount-card').removeClass('chosed');
    //         $(this).addClass('chosed');
    //     }

    // });

    // open or close anonymous
    $UI.on('click', '.open-icon', function () {
        $(this).toggleClass('open');
    });

    $UI.on('click', '.open-comment-icon', function () {
        $(this).toggleClass('open');
        $('.commentArea').toggleClass('close');
        // $('.commentArea').css('display','none');
    });

    // $UI.on('click', '#target_amount', function () {
    //     $('#target_amount').val('');
    //     $('.amount-card.chosed').removeClass('chosed');
    // })

    // Intention Before Zakat : read
    $UI.on('click', '.intention-before-zakat #readIntention', function () {
        $(this).parents().find('.content-inner').toggleClass('active');
    })

    // Intention Before Zakat : close
    $UI.on('click', '.intention-before-zakat .close', function (rs) {
        $('.intention-before-zakat').css('display', 'none')
    })

    // Intention Before Zakat:confirm
    $UI.on('click', '.intention-before-zakat .content-inner.active .confirm', function () {
        utils.showLoading(lang.lang12);
        $UI.trigger('submit-b', [submitData]);
    })



    // Intention Before Qurban : read
    $UI.on('click', '.intention-before-qurban #readIntention', function () {
        $(this).parents().find('.content-inner').toggleClass('active');
    })

    // Intention Before Qurban : close
    $UI.on('click', '.intention-before-qurban .close', function (rs) {
        $('.intention-before-qurban').css('display', 'none')
    })

    // Intention Before Qurban:confirm
    $UI.on('click', '.intention-before-qurban .content-inner.active .confirm', function () {
        utils.showLoading(lang.lang12);
        $UI.trigger('submit-b', [submitData]);
    })


    // donate-B login
    $UI.on('click', '.login', function () {
        location.href = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/login?req_code=600' : '/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=loginB')
        // location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=loginB')
    })

    //计算渠道费
    $UI.on('chooseQurbanBankNext', function () {
        console.log('chooseQurbanBankNext');
        calculateChannelFee();
    })

    $UI.on('chooseCommonBankNext', function (rs) {
        console.log('chooseCommonBankNext');
        // chooseCommonBankNext(rs);
    })

    // 判断项目分类ID，如果为13说明是zakat项目，则想要弹出zakat捐款须知
    // submit
    $UI.on('click', '.nextBtn', function () {
        if (group_id) {
            submitData = getGroupSubmitParam.init();
            // submitData.category_id = category_id;
            // submitData.ifRec = ifRec;
            console.log('submitData = ', submitData)
            if (submitData.comment) {
                is_comment = true
            } else {
                is_comment = false
            }
            // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
            if (_validateGroup.check(submitData, lang)) {
                store.set('donateMoney', submitData);
                utils.showLoading(lang.lang12);
                $UI.trigger('submit-b', [submitData]);
            }

        } else {
            submitData = getSubmitParam.init();
            submitData.category_id = category_id;
            // submitData.ifRec = ifRec;
            console.log('submitData = ', submitData)
            if (submitData.comment) {
                is_comment = true
            } else {
                is_comment = false
            }
            // sensors.track('DonateInfoFill', {
            //     from: document.referrer,
            //     payment_method: submitData.payment_channel,
            //     order_amount: submitData.money,
            //     is_anonim: anonymous,
            //     is_comment: is_comment,
            // })

            // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
            if (_validate.check(submitData, lang)) {


                store.set('donateMoney', submitData);

                if (category_id == 13) {
                    console.log('category_id == 13');

                    $('.intention-before-zakat').css('display', 'block');
                    $('.intention-before-zakat .money span').html($('.total-money').attr('total-price'));
                } else if (category_id == 15) {

                    $('.intention-before-qurban').css('display', 'block');
                    $('.intention-before-qurban .money span').html($('.total-money').attr('total-price'));
                } else {
                    utils.showLoading(lang.lang12);
                    $UI.trigger('submit-b', [submitData]);
                }


            }
        }

    });

};

// 实际支付总金额 + 费率更改

function calculateChannelFee(res) {

    originPrice = parseInt(originPrice);

    let bankChannelFee = $(".alert-qurban-bank").attr('data-methodFee') || 0;
    let bankCode = $(".alert-qurban-bank").attr('data-bankcode');
    let expense_type = $(".alert-qurban-bank").attr('data-expenseType');
    // let channelFee

    // GO-Pay：GO-PAY按照总金额*2%计算
    if (expense_type == 2) {
        // 实际支付总金额

        channelFee = Math.ceil(originPrice / (1 - bankChannelFee)) - originPrice

        store.set('chooseBank', {
            'bankcode': $('.alert-qurban-bank').attr('data-bankcode'),
            'bankImg': $('.alert-qurban-bank').attr('data-bankImg'),
            'bankName': $('.alert-qurban-bank').attr('data-bankName'),

            'originPrice': originPrice,
            'channelFee': channelFee,
        })
        console.log('======1======', channelFee + originPrice);

        $('.total-money').html(changeMoneyFormat.moneyFormat(Math.ceil(originPrice / (1 - bankChannelFee)))).attr('total-price', changeMoneyFormat.moneyFormat(channelFee + originPrice));

        $('.fee').html(changeMoneyFormat.moneyFormat(channelFee));


        // OVO：OVO不计
    } else if (bankCode == 4) {
        // 实际支付总金额

        channelFee = originPrice * bankChannelFee
        $('.total-money').html(changeMoneyFormat.moneyFormat(originPrice * (1 + parseFloat(bankChannelFee)))).attr('total-price', changeMoneyFormat.moneyFormat(originPrice + bankChannelFee));
        $('.fee').html(changeMoneyFormat.moneyFormat(channelFee));
        console.log('======2======', channelFee + originPrice);

        store.set('chooseBank', {
            'bankcode': $('.alert-qurban-bank').attr('data-bankcode'),
            'bankImg': $('.alert-qurban-bank').attr('data-bankImg'),
            'bankName': $('.alert-qurban-bank').attr('data-bankName'),

            'originPrice': originPrice,
            'channelFee': channelFee,
        })

        // VA和online bank:按照每笔订单计算
    } else if (expense_type == 1) {
        channelFee = parseFloat(bankChannelFee)
        $('.total-money').html(changeMoneyFormat.moneyFormat(originPrice + parseFloat(bankChannelFee))).attr('total-price', changeMoneyFormat.moneyFormat(originPrice + channelFee));

        $('.fee').html(changeMoneyFormat.moneyFormat(channelFee));

        store.set('chooseBank', {
            'bankcode': $('.alert-qurban-bank').attr('data-bankcode'),
            'bankImg': $('.alert-qurban-bank').attr('data-bankImg'),
            'bankName': $('.alert-qurban-bank').attr('data-bankName'),

            'originPrice': originPrice,
            'channelFee': channelFee,
        })
        console.log('======3======', channelFee + originPrice);


    }

    // console.log('channelFee', channelFee, originPrice);

    // return location.href = '/confirmOrder.html?project_id='+ project_id +'&originPrice=' + originPrice + '&channelFee=' + channelFee
}



export default obj;