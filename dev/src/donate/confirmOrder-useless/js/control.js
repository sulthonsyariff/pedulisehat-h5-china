import view from './view'
import model from './model'
import store from 'store'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import domainName from 'domainName' // port domain name
import moneyInputChange from './_moneyInputChange'
import commonDonateRecommendation from 'commonDonateRecommendation' //联合支付
import _recommondationAmount from "./_recommondationAmount"; //联合支付处资金公式
import _calculationFormula from "./_calculationFormula";

import qurbanBankList from 'qurbanBankList'
import commonBankList from 'commonBankList'

/* translation */
import system from 'system'
import qscLang from 'qscLang'
let lang = qscLang.init(system);
/* translation */

let UI = view.UI;
let generateQRcode;
let deeplink_redirect;
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let group_id = reqObj['group_id'];
let short_link = reqObj['short_link'];
let fromLogin = reqObj['fromLogin'];
let from = reqObj['from'];
let category_id;

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';

/*
 * H5: visit login or login
 */
// visit login or login

// qurbanBankList.init({});


if ((reqObj.fromLogin == 'loginB' && store.get('donateMoney') && store.get('donateMoney').payment_channel) || (store.get('donateMoney') && store.get('donateMoney').user_name && store.get('donateMoney').phone && store.get('donateMoney').payment_channel)) {
    console.log('fromLogin====B');

    loginHandleB(store.get('donateMoney'));
}
// load the juicer
else {
    if (group_id) {
        getGroupProjInfo();
    } else {
        getProjInfo(); // get projectInfo
    }
}

function getGroupProjInfo(rs) {
    model.getGroupProjInfo({
        param: {
            group_id: group_id,
            short_link: short_link
        },
        success: function (res) {
            if (res.code == 0) {
                view.getGroupProjInfo(res);
                //  get payment channel
                model.getPayment({
                    param: {
                        bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
                    },
                    success: function (rs) {
                        if (rs.code == 0) {

                            category_id = 12;
                            commonBankList.init(category_id);

                            res.data.category_id = ''

                            view.getProjInfo(res);
                            getProjectPoundage(res); //项目手续费情况

                            // view.init(rs);
                        } else {
                            utils.alertMessage(rs.msg)
                        }
                    },
                    error: utils.handleFail
                })

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })

}


function getProjInfo() {
    model.getProjInfo({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function (res) {
            if (res.code == 0) {
                category_id = res.data.category_id;

                if (res.data.category_id == 15) {
                    qurbanBankList.init(category_id);

                } else {
                    commonBankList.init(category_id);

                }

                // move b to donate
                view.getProjInfo(res);
                getProjectPoundage(res); //项目手续费情况
                // getPayment(res); //获取银行列表

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// 项目手续费情况
function getProjectPoundage(rs) {
    model.getProjectPoundage({
        param: {
            project_id: project_id,
            category_id: rs.data.category_id
        },
        success: function (res) {
            if (res.code == 0) {
                console.log('===getProjectPoundage===', res.data);
                rs.ratioData = res.data

                // res.category_id = rs.data.category_id;
                // moneyInputChange.init(res); //监听input输入
                getPayment(rs); //获取银行列表

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// 获取银行列表
function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function (rs) {
            if (rs.code == 0) {
                console.log(' get payment channel =', rs);
                rs.getProjInfoData = res;
                rs.ratioData = res.ratioData

                // move b to donate
                rs.user_id = user_id
                getUserInfo(rs)
                // view.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

function getUserInfo(res) {

    model.getUserInfo({
        success: function (rs) {
            res.email = rs.data.email
            view.init(res);

            // userFondRecord(objData)
        },
        unauthorizeTodo: function (rs) {
            view.init(res);
        },
        error: utils.handleFail
    });
}


/*
 submit form
*/

UI.on('submit-b', function (e, objData) {
    loginHandleB(objData);
});

// UI.on('canRec', function(e, rs) {
//     console.log('canRec', rs);
//     commonDonateRecommendation.init(rs); //联合支付
//     _recommondationAmount.init(rs);
// })

// 添加联合支付
// UI.on('recommendationAdd', function(e, $this) {
//     view.showRecommendationAmountList($this);

//     _calculationFormula.init(); //资金公式
// })

/**
 * judge login or visit login
 *
 */
function loginHandleB(objData) {
    model.getUserInfo({
        success: function (rs) {
            objData.avatar = rs.data.avatar;
            // objData.user_name = rs.data.user_name;
            objData.phone = rs.data.mobile;

            objData.user_name = objData.user_name ? objData.user_name : rs.data.user_name; //如果选择匿名，优先匿名

            objData.fond_in = 'toruists_donated_b'

            // console.log('getUserInfo');

            if (group_id) {
                loginCreateGroupPayment(objData); //logined in

            } else {

                loginCreatePayment(objData); //logined in
            }

            // userFondRecord(objData)
        },
        unauthorizeTodo: function (rs) {
            // judge if visit login
            // if (store.get('donateMoney') && store.get('donateMoney').user_name && store.get('donateMoney').phone) {
            if (group_id) {
                visitCreateGroupPayment(objData);

            } else {

                visitCreatePayment(objData);
            }
            // }
        },
        error: utils.handleFail
    });
}



/**
 * group login pay
 */
function loginCreateGroupPayment(objData) {
    objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
    store.set('donateMoney', ''); //clear the cache

    model.createGroupPayment({
        param: objData,
        success: function (res) {
            if (res.code == 0) {
                successHandle(objData, res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}



/**
 * login pay
 */
function loginCreatePayment(objData) {

    console.log('loginCreatePayment');

    objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
    store.set('donateMoney', ''); //clear the cache

    model.createPayment({
        param: objData,
        success: function (res) {
            if (res.code == 0) {
                successHandle(objData, res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

/**
 * group vistit pay
 *
 * * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 3?
 */
function visitCreateGroupPayment(objData) {
    objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
    store.set('donateMoney', ''); //clear the cache
    model.visitCreateGroupPayment({
        param: objData,
        success: function (res) {
            if (res.code == 0) {
                successHandle(objData, res);
                console.log('successHandle',objData)
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}



/**
 * vistit pay
 *
 * * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 3?
 */
function visitCreatePayment(objData) {
    objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
    store.set('donateMoney', ''); //clear the cache

    model.visitCreatePayment({
        param: objData,
        success: function (res) {
            if (res.code == 0) {
                successHandle(objData, res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

function successHandle(objData, res) {
    utils.showLoading(lang.lang1);

    // gopay: bank_code = 1
    if (objData.payment_channel == '1') {
        storeGopay(res);
        setTimeout(() => {
            location.href = '/goPayQRcode.html?project_id=' + project_id + '&short_link=' + short_link + '&order_id=' + res.data.order_id + '&amount=' + res.data.gross_amount

            // location.href = '/goPay.html?project_id=' + project_id + '&short_link=' + short_link + '&order_id=' + res.data.order_id + '&amount=' + res.data.gross_amount + '&category_id=' + category_id
        }, 300);
    }
    // sinamas: bank_code = 2
    else if (objData.payment_channel == '2') {
        storeSinamas(res);
        setTimeout(() => {
            location.href = '/OrderTransactionDetail.html?project_id=' + project_id + '&short_link=' + short_link + '&channel=' + objData.payment_channel + '&category_id=' + category_id
        }, 300);
    }
    // OVO:bank_code = 4
    else if (objData.payment_channel == '4') {
        console.log('ovo = ', res, res.data.order_id, objData);
        setTimeout(() => {
            location.href = '/OVO.html?project_id=' + project_id +
                '&short_link=' + short_link +
                '&order_id=' + res.data.order_id +
                // '&amount=' + objData.money +
                '&amount=' + (objData.total_money || objData.money) +
                '&phone=' + objData.phone +
                (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '') + '&category_id=' + category_id
        }, 300);
    }
    // dana:bank_code = 5
    else if (objData.payment_channel == '5') {
        // storeDana(res);
        console.log('dana = ', res);
        if (user_id) {
            setTimeout(() => {
                location.href = domainName.trade + '/v1/dana/oauth?Qsc-Peduli-Token=' + accessToken + '&checkout_url=' + encodeURI(res.data.checkout_url)
            }, 300);
        } else {
            setTimeout(() => {
                location.href = res.data.checkout_url
            }, 300);
        }
    }
    // faspay:bank_code = *
    else if (objData.payment_channel == '707' ||
        objData.payment_channel == '801' ||
        objData.payment_channel == '708' ||
        objData.payment_channel == '802' ||
        objData.payment_channel == '408' ||
        objData.payment_channel == '402' ||
        objData.payment_channel == '702' ||
        objData.payment_channel == '800') {
        // console.log('VAorderPage', objData.payment_channel)
        // console.log('VAorderPage===', JSON.stringify(res.data))
        storeFaspay(res);
        setTimeout(() => {
            location.href = '/OrderTransactionDetail.html?project_id=' + project_id + '&short_link=' + short_link + '&channel=' + objData.payment_channel + '&category_id=' + category_id
        }, 300);
    }
    // 其他银行
    else {
        location.href = res.data.redirect_url;
    }
}

function storeGopay(res) {
    generateQRcode = res.data.actions[0].url
    deeplink_redirect = res.data.actions[1].url
    store.set('goPay', {
        generate_qr_code: generateQRcode,
        deeplink_redirect: deeplink_redirect
    });
}

function storeSinamas(res) {
    //sensors
    // sensors.track('PayOrder', {
    //     from: document.referrer,
    //     order_id: res.data.NoRef,
    //     order_amount: Amount,
    //     is_succeed: ''
    // })

    SourceID = res.data.SourceID
    IssueDate = res.data.IssueDate
    NoRef = res.data.NoRef
    VirtualAccountNumber = res.data.VirtualAccountNumber
    Amount = res.data.Amount
    let VAorder = {
        SourceID: SourceID,
        IssueDate: IssueDate,
        NoRef: NoRef,
        VirtualAccountNumber: VirtualAccountNumber,
        Amount: Amount,
        from: 'sinarmas'
    }
    sessionStorage.setItem('VA', JSON.stringify(VAorder));
}

function storeFaspay(res) {
    SourceID = res.data.merchant
    IssueDate = res.data.pay_expired
    NoRef = res.data.bill_no
    VirtualAccountNumber = res.data.trx_id
    Amount = res.data.money
    let VAorder = {
        SourceID: SourceID,
        IssueDate: IssueDate,
        NoRef: NoRef,
        VirtualAccountNumber: VirtualAccountNumber,
        Amount: Amount,
        from: 'faspay'
    }
    sessionStorage.setItem('VA', JSON.stringify(VAorder));
}

/*
 android: visit login or login
{“
    user_name:"123"
    phone:'123'
}
当俩个字段对应的数值均为空的时候 表示当前用户登录成功;
当俩个字段均不为空的时候 表示其为游客 对应的字段就是游客信息;
req_code = 500 监听app登陆成功或者游客模式下， 返回捐赠页， H5自动跳转到相应支付页面（ gopay / fastpay） req_code = 500
*/
window.appHandle = function (requestCode, jsonBean) {
    let objData = store.get('donateMoney');

    // visit login
    if (requestCode == 500 && jsonBean.user_name && jsonBean.phone) {
        utils.showLoading(lang.lang1);

        objData.user_name = jsonBean.user_name;
        objData.phone = jsonBean.phone;

        if(group_id){
            visitCreateGroupPayment(objData);

        } else {

            visitCreatePayment(objData);
        }
    }
    // login
    else if (requestCode == 500 && !jsonBean.user_name && !jsonBean.phone) {
        utils.showLoading(lang.lang1);

        loginHandleB(objData);
    } else if (requestCode == 600) {
        location.reload();
    }
}