import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import _calculationFormula from "./_calculationFormula";

let obj = {};
let $UI = $('body');
obj.UI = $UI;

// let reqObj = utils.getRequestParams();
// let project_id = reqObj['project_id'];
// let reqSelect = location.href
// let category_id;

let bankCodeArr_1 = [302, 1, 4, 5, 6, 7]; //LinkAja, GoPay, OVO, and DANA
let bankCodeArr_2 = [2, 801, 802, 402, 708, 408, 302, 1, 4, 5, 6, 7];
// Sinarmas Virtual Account, BNI Virtual Account,
// Mandiri Virtual Account, Permata Virtual Account,
// Danamon Virtual Account, Maybank Virtual Account,
// LinkAja, GoPay, OVO, and DANA
let inputMoney
let resAll;

obj.init = function(res) {
    // category_id = res.category_id;
    console.log('commonMoney init == ', res);
    resAll = res;

    $UI.on('click', '#target_amount', function() {
        if ($('.amount-card').hasClass('chosed')) {
            $('#target_amount').val('').attr('data-amount', '');

            $('.amount-card.chosed').removeClass('chosed');
            inputMoney = 0
            if (res.category_id) {
                handleAmount(inputMoney); //处理联合互助产品金额+捐款金额
            }
        }
    })

    // choose money card
    $UI.on('click', '.amount-card', function() {
        if ($(this).hasClass('chosed')) {
            $('.amount-card').removeClass('chosed');
            // inputMoney = 0
            // handleAmount(inputMoney); //处理联合互助产品金额+捐款金额

        } else {
            $('.amount-card').removeClass('chosed');
            $(this).addClass('chosed');
            inputMoney = $(this).attr('data-value')
            $('#target_amount').val(changeMoneyFormat.newMoneyFormat(inputMoney)).attr('data-amount', inputMoney)
            
            if (res.category_id) {
                handleAmount(inputMoney); //处理联合互助产品金额+捐款金额
            }
            handleBankList(res, inputMoney); //处理银行列表
        }
    });


    // propertychange
    // //监听input输入
    $UI.on('input propertychange', '#target_amount', function() {
        // $UI.on('focusout', '#target_amount', function () {
        inputMoney = parseInt($('#target_amount').val().replace(/\./g, '') || 0); //支付金额
        console.log('===inputMoney===', inputMoney);
        $('#target_amount').attr('data-amount', inputMoney)
        if (res.category_id) {
            handleAmount(inputMoney); //处理联合互助产品金额+捐款金额
        }
        handleBankList(res, inputMoney); //处理银行列表
    })
}

obj.setQuickDonate = function(quickDonateValue) {
    $('#target_amount').attr('data-amount', quickDonateValue);
    $('#target_amount').val(changeMoneyFormat.newMoneyFormat(quickDonateValue));
    if (resAll.category_id) {
        handleAmount(quickDonateValue);
    }
    handleBankList(resAll, quickDonateValue);
}

function handleAmount(inputMoney) {
    if (!inputMoney) {

        handleCampaignDonation(0);
        handleRecMoney(3000);
        $('.amount-show,.rec-show').css('display', 'none'); //隐藏资金公式 + 互助

    } else {

        handleCampaignDonation(inputMoney);
        judgeAndHandleRecMoney(inputMoney);
        _calculationFormula.init(); //资金公式
        $('.amount-show,.rec-show').css('display', 'block'); //展示资金公式 + 互助

    }
}

/*****
 * 处理联合互助产品金额

 1.捐款金额≦ Rp 10.000, 联合支付金额Rp 3.000
 2.Rp 10.000＜ 捐款金额≦ Rp 50.000， 联合支付金额Rp 5.000
 3.捐款金额＞ Rp 50.000， 联合支付金额Rp 10.000
 */
function judgeAndHandleRecMoney(inputMoney) {
    if (inputMoney > 50000) {
        handleRecMoney(10000);
    } else if (inputMoney > 10000 &&
        (inputMoney < 50000 || inputMoney == 50000)) {
        handleRecMoney(5000);
    } else {
        handleRecMoney(3000);
    }
}

/**
 * 联合互助产品金额展示
 */
function handleRecMoney(money) {
    $('.recmdt_amount,.rec-money').html(changeMoneyFormat.moneyFormat(money));
    $('.rec-money,.recmdt_amount').attr('data-amount', money);
}

/****
 *  项目捐助金额
 */
function handleCampaignDonation(inputMoney) {
    $('.campaign-money').attr('data-amount', inputMoney);
    $('.campaign-money').html(changeMoneyFormat.moneyFormat(inputMoney));
}

/****
 * 不同项目不同的比例:
 *
 * "ratio":比例
 * "type": 1不包含 2包含
 *
一.平台费包含渠道费，2.5% （ratio=0.025,type=2）
        1. Rp 1.000 ≦ 支付金额 ＜ Rp 110.000
            展示 bankCodeArr_1 ,其他渠道置灰不可点击
        2. Rp 110.000≦支付金额＜ Rp 200.000
            展示 bankCodeArr_2 ,其他渠道置灰不可点击
        3. 支付金额 ≧ Rp 200.000,
            展示所有支付渠道。
二.平台费包含渠道费，3.5%（ratio=0.035,type=2）
        1. Rp 1.000 ≦ 支付金额 ＜ Rp 110.000
            展示 bankCodeArr_1 ,其他渠道置灰不可点击
        2. 支付金额 ≧ Rp 110.000
            展示所有支付渠道。

三.平台费不包含渠道费 (type=1)
        1. Rp 1.000 ≦ 支付金额 ＜ Rp 10.000
            展示 bankCodeArr_1 ,其他渠道置灰不可点击
        2. 支付金额 ≧ Rp 10.000
            展示所有支付渠道。
 *
 */
function handleBankList(res, inputMoney) {
    // if 2jt hide ewallet
    if (inputMoney > 2000000) {
        console.log('hide bankArr 1');

        hideBankArr1(bankCodeArr_1, res); // ewallet
    } else {
        // show va
        if (inputMoney >= Number(res.minimum_donation.EWalletMin) && inputMoney < Number(res.minimum_donation.BankTransferMin)) {
            console.log('handle 1');  
            handle(bankCodeArr_1);
        }
        
        // show all
        else if (inputMoney >= Number(res.minimum_donation.EWalletMin) && inputMoney >= Number(res.minimum_donation.BankTransferMin)) {
            handleAll(res);
        }
    }
}

function hideBankArr1(bankCodeArr, res) {
    $('.list-items').css({
            'pointer-events': 'auto',
            'color': '#333',
            'opacity': '1.0'
        })
        // 展示bankCodeArr， 其他渠道置灰不可点击
    for (let i = 0; i < bankCodeArr.length; i++) {
        $('.list-items[list-bankcode=' + bankCodeArr[i] + ']').css({
            'pointer-events': 'none',
            'color': '#ccc',
            'opacity': '0.2'
        })

    }
}

function handle(bankCodeArr, res) {
    $('.list-items').css({
        'pointer-events': 'none',
        'color': '#ccc',
        'opacity': '0.2'
    })

    // 展示bankCodeArr， 其他渠道置灰不可点击
    for (let i = 0; i < bankCodeArr.length; i++) {
        $('.list-items[list-bankcode=' + bankCodeArr[i] + ']').css({
            'pointer-events': 'auto',
            'color': '#333',
            'opacity': '1.0'
        })

    }
}

function handleAll(res) {
    // 展示所有支付渠道。
    console.log('===handleAll===');

    $('.list-items').css({
        'pointer-events': 'auto',
        'color': '#333',
        'opacity': '1.0'
    })
}

function hideALL(res) {
    console.log('===hideAll===');
    $('.list-items').css({
        'pointer-events': 'none',
        'color': '#ccc',
        'opacity': '0.2'
    })

}

export default obj;