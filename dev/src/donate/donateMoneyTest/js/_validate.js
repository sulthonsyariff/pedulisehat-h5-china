/*
 * 校验表单信息
 */
import utils from 'utils'
import 'jq_cookie' //ajax cookie

let reqObj = utils.getRequestParams();
let fromLogin = reqObj['fromLogin'];
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let obj = {};

obj.check = function(param, lang) {
    console.log('validate', 'param:', param);

    // if ((param.payment_channel == 5 ||
    //         param.payment_channel == 4 ||
    //         param.payment_channel == 1 ||
    //         param.payment_channel == 302) &&
    //     !(param.money > 999 && param.money % 1000 == 0)) {

    //     $('.target_amount_tips').addClass('changeRed');
    //     $(document).scrollTop($('.donateMoney').height()); // 定位到错误提示的地方

    //     return false;

    // } else if ((param.payment_channel != 5 &&
    //         param.payment_channel != 4 &&
    //         param.payment_channel != 1 &&
    //         param.payment_channel != 302) &&
    //     !(param.money > 9999 && param.money % 1000 == 0)) {
    //     // console.log('other',param.payment_channel,(param.payment_channel != 4 && param.payment_channel != 1))

    //     $('.target_amount_tips').addClass('changeRed');
    //     $(document).scrollTop($('.donateMoney').height()); // 定位到错误提示的地方

    //     return false;

    // } else {
    //     $('.target_amount_tips').removeClass('changeRed');
    // }

    if (isNaN(param.money)) {
        utils.alertMessage(lang.lang13_1);
        return false;
    }

    // if (param.comment.length > 140) {
    //     utils.alertMessage(lang.lang14);
    //     return false;
    // }

    // if (fromLogin != 'loginB' && !user_id) {
    //     if (!param.user_name.length) {
    //         utils.alertMessage(lang.lang32);
    //         return false;
    //     }

    //     if (param.phone.length < 7) {
    //         utils.alertMessage(lang.lang31);
    //         return false;
    //     }
    // }
    // if (!(param.money > 9999 && param.money % 1000 == 0) && (param.payment_channel != 4 || param.payment_channel != 1)) {
    //     $('.target_amount_tips').css('color', '#FF6100')
    //     return false;
    // } else {
    //     $('.target_amount_tips').css('color', '#999999')
    // }

    // if (!$(".payment_channel .bank-name").attr('data-bankCode')) {
    //     utils.alertMessage(lang.lang5);
    //     return false;
    // }

    return true;
}


export default obj;