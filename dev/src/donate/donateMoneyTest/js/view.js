import mainTpl from '../tpl/main.juicer'
import _zakatIntentionTpl from '../tpl/_zakatIntention.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
// import bankList from './_bankList'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
import store from 'store'
import _validate from './_validate';
import _commonValidate from './_commonValidate';
// import _validateZakat from './_validateZakat'; //项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
import campaignCard from '../tpl/_campaignCard.juicer'
import qurbanItems from '../tpl/_qurbanItems.juicer'
import commonDonateTpl from '../tpl/_commonDonate.juicer'
import _calculationFormula from "./_calculationFormula";

// import sensorsActive from 'sensorsActive'
import getSubmitParam from "./_getSubmitParam";
import getCommonSubmitParam from "./_getCommonSubmitParam";
import moneyInputChange from './_moneyInputChange'
import commonBankList from 'commonBankList'
import commonMoneyInputChange from './_commonMoneyInputChange'

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || '';
let cek_group_id = reqObj['group_id'] || -1;
let short_link = reqObj['short_link'] || '';
let short_url = reqObj['short_url'] || '';
let from = reqObj['from'] || ''; //?什么作用？
let fromLogin = reqObj['fromLogin'] || '';
let quickDonate = reqObj['quickDonate'] || 0;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0;
// let appVersionName = utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName')

let originPrice;
let bankChannelFee;
let bankCode;
let expense_type;
let channelFee

// package campaign global variable
let countPackage = 1;
let pricePackage = 0;

// let arr = [];
let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* translation */
import donateMoney from 'donateMoney'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateMoney);
/* translation */

let getLanguage = utils.getLanguage();
let submitData;
let category_id;
let qurbanName;
let recMoney;
let ifRec;

function setQuickDonate(rs) {
    let inputMoney = Number(quickDonate);
    if (inputMoney > 0) {
        commonMoneyInputChange.setQuickDonate(inputMoney);
        
        // choosed option if quickDonate same with option     
        switch (inputMoney) {
            case rs.minimum_donation_value.Option1:
                $('.amount-card.c-1').addClass('chosed');
                break;
            case rs.minimum_donation_value.Option2:
                $('.amount-card.c-2').addClass('chosed');            
                break;
            case rs.minimum_donation_value.Option3:
                $('.amount-card.c-3').addClass('chosed');        
                break;
            case rs.minimum_donation_value.Option4:
                $('.amount-card.c-4').addClass('chosed');    
                break;
            case rs.minimum_donation_value.Option5:
                $('.amount-card.c-5').addClass('chosed');
                break;
            case rs.minimum_donation_value.Option6:
                $('.amount-card.c-6').addClass('chosed');
                break;
        }
    }
}

obj.getGroupProjInfo = function(rs) {
    category_id = rs.data.category_id;
    rs.lang = lang;
    rs.getLanguage = getLanguage;

    // PACKAGE CAMPAIGN
    const packageCampaign = false;

    if (packageCampaign) {
        category_id = 999;
    }
}

obj.getProjInfo = function(rs) {
    category_id = rs.data.category_id;
    rs.lang = lang;
    rs.getLanguage = getLanguage;

    // PACKAGE CAMPAIGN
    const packageCampaign = false;

    if (packageCampaign) {
        category_id = 999;
    }    
}

obj.insertQurbanItems = function(rs) {
    rs.getLanguage = getLanguage;
    rs.lang = lang;
    console.log('insert Qurban or Package Items', rs);
    for (let i = 0; i < rs.data.length; i++) {
        rs.data[i].format_price = changeMoneyFormat.moneyFormat(rs.data[i].price)
    }
    $('.donateMoney').append(qurbanItems(rs)); //项目卡片
}

obj.init = function(rs) {
    
    rs.category_id = category_id;
    console.log('category_id', category_id);

    if (rs.getProjInfoData && rs.getProjInfoData.data && rs.getProjInfoData.data.category_id == 13) {
        rs.JumpName = 'Zakat';
        $('title').html('Zakat');
    } else if (rs.getProjInfoData && rs.getProjInfoData.data && rs.getProjInfoData.data.category_id == 15) {
        rs.JumpName = lang.lang54;

    } else {
        rs.JumpName = titleLang.donate;
        $('title').html(titleLang.donate);
    }

    // rs.commonNavGoToWhere = utils.browserVersion.android ?
    //     'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true' :
    //     '/' + short_link + (from ? ('?from=' + from) : '');
    rs.commonNavGoToWhere = commonNavGoToWhere();
    rs.lang = lang;
    // console.log('======B=======', rs.lang)
    // $('title').html(lang.lang54);

    commonNav.init(rs);


    rs.from = from;

    // minimum donation
    // for display
    rs.minimum_donation_display = {
        BankTransferMin: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.BankTransferMin),
        EWalletMin: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.EWalletMin),
        Option1: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.Option1),
        Option2: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.Option2),
        Option3: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.Option3),
        Option4: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.Option4),
        Option5: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.Option5),
        Option6: changeMoneyFormat.moneyFormat(rs.getProjInfoData.data.minimum_donation.Option6)
    }
    // for data value
    rs.minimum_donation_value = {
        BankTransferMin: Number(rs.getProjInfoData.data.minimum_donation.BankTransferMin),
        EWalletMin: Number(rs.getProjInfoData.data.minimum_donation.EWalletMin),
        Option1: Number(rs.getProjInfoData.data.minimum_donation.Option1),
        Option2: Number(rs.getProjInfoData.data.minimum_donation.Option2),
        Option3: Number(rs.getProjInfoData.data.minimum_donation.Option3),
        Option4: Number(rs.getProjInfoData.data.minimum_donation.Option4),
        Option5: Number(rs.getProjInfoData.data.minimum_donation.Option5),
        Option6: Number(rs.getProjInfoData.data.minimum_donation.Option6)
    }

    $UI.append(mainTpl(rs));
    $('.campaignCard').append(campaignCard(rs.getProjInfoData)); //项目卡片
    // $UI.append(_zakatIntentionTpl(rs)); //zakat项目捐款须知

    if (category_id != 15 && category_id != 18) {
        $('.donateMoney').append(commonDonateTpl(rs)); //项目卡片
        changeMoneyFormat.init('#target_amount');
    }
    // bankList.init(rs); // OVO合作项目只需要展示OVO

    judgeCanRec(rs); //判断能否联合支付

    commonFooter.init(rs);
    // return params;.init();

    // 隐藏loading
    utils.hideLoading();

    fastclick.attach(document.body);
    obj.event(rs);
    // changeMoneyFormat.init('#target_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // add text validation
    $('.target_amount_tips').html(lang.lang58 + rs.minimum_donation_display.EWalletMin);
    
    // set quick donate
    setQuickDonate(rs);
};

// 返回
function commonNavGoToWhere() {
    if (utils.browserVersion.android) {
        return 'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true';
    }
    // else if (document.referrer.indexOf('supporterRanking') != -1) {
    //     return document.referrer; // 上一个页面的URL: document.referrer
    // }
    else {
        if (group_id) {
            return '/group/' + short_link + (from ? ('?from=' + from) : '');

        } else {

            return '/' + short_link + (from ? ('?from=' + from) : '');
        }
    }
}

function judgeCanRec(rs) {
    if (category_id == 12 || category_id == 14) {
        $UI.trigger('canRec', rs);
    }
}


obj.showRecommendationAmountList = function($this) {
    if ($($this).hasClass('add')) {
        $('.recommendationAmountList').css('display', 'block');
    } else {
        $('.recommendationAmountList').css('display', 'none');
    }
}

obj.event = function(rs) {
    console.log('rs', rs);

    // open or close anonymous
    $UI.on('click', '.open-icon', function() {
        $(this).toggleClass('open');
    });

    // choose queban items
    $UI.on('click', '.list-items-wrap', function() {
        pricePackage = Number($(this).attr('data-price'));
        console.log('click-qurban', $(this).attr('data-price'));

        // reset countPackage when click items
        countPackage = 1;
        // display count and total harga package
        $('.countPackage').text(countPackage);
        let totalPackage = pricePackage * countPackage;
        $('.wrap-total').text(`Total Rp ${changeMoneyFormat.moneyFormat(totalPackage)}`);

        $(this).siblings().removeClass('chose')
        $('.qurban-wrap').removeAttr('price')
        $(this).toggleClass('chose');

        // campaign package
        // yg asalnya qurbanName diganti jadi item_id
        if (category_id == 18) {
            if ($(this).hasClass('chose')) {
                $('.qurban-wrap').attr({
                    'price': $(this).attr('data-price'),
                    'qurbanName': $(this).attr('data-item'),
                })
            }    
        } else {
            if ($(this).hasClass('chose')) {
                $('.qurban-wrap').attr({
                    'price': $(this).attr('data-price'),
                    'qurbanName': $(this).attr('data-name')
                })
            }
        }

        if ($('.list-items-wrap').hasClass('chose')) {
            $('.wrap-campaign-package-count').css('display', 'block');
        } else {
            $('.wrap-campaign-package-count').css('display', 'none');            
        }

        moneyInputChange.init(rs); //监听input输入

    });

    // count min campaign
    $UI.on('click', '.btn-count-min', function() {
        let totalPackage;
        if (countPackage > 1) {
            // kurang
            countPackage--;
            // count text
            $('.countPackage').text(countPackage);
            // total text
            totalPackage = pricePackage * countPackage;
            $('.wrap-total').text(`Total Rp ${changeMoneyFormat.moneyFormat(totalPackage)}`);
            // set attr
            if ($('.list-items-wrap').hasClass('chose')) {
                $('.qurban-wrap').attr({
                    'price': totalPackage,
                    'qurbanName': $(this).attr('data-item')
                })
            }
            // triggered validate bank list
            moneyInputChange.init(rs); //监听input输入

            console.log('count min campaign', totalPackage);
        }
    });

    // count plus campaign
    $UI.on('click', '.btn-count-plus', function() {
        let totalPackage;
        // tambah
        countPackage++;
        // count text
        $('.countPackage').text(countPackage);
        // total text
        totalPackage = pricePackage * countPackage;
        $('.wrap-total').text(`Total Rp ${changeMoneyFormat.moneyFormat(totalPackage)}`);
        // set attr
        if ($('.list-items-wrap').hasClass('chose')) {
            $('.qurban-wrap').attr({
                'price': totalPackage,
                'qurbanName': $(this).attr('data-item')
            })
        }
        // triggered validate bank list
        moneyInputChange.init(rs); //监听input输入

        console.log('count plus campaign', totalPackage);
    });

    // Intention Before Zakat : read
    $UI.on('click', '.intention-before-zakat #readIntention', function() {
        $(this).parents().find('.content-inner').toggleClass('active');
    })

    // Intention Before Zakat : close
    $UI.on('click', '.intention-before-zakat .close', function(rs) {
        $('.intention-before-zakat').css('display', 'none')
    })

    // Intention Before Zakat:confirm
    $UI.on('click', '.intention-before-zakat .content-inner.active .confirm', function() {
        utils.showLoading(lang.lang12);
        $UI.trigger('submit-b', [submitData]);
    })

    // 计算渠道费:普通项目/zakat/bulk不需要计算渠道费，qurban项目需要计算渠道费
    $UI.on('chooseQurbanBankNext', function(rs) {
        console.log('chooseQurbanBankNext');
        chooseQurbanBankNext(rs);
    })

    // 普通项目/zakat/bulk/专题 ,跳转
    $UI.on('chooseCommonBankNext', function(rs) {
        console.log('chooseCommonBankNext');
        chooseCommonBankNext(rs);
    })


    // 判断项目分类ID，如果为13说明是zakat项目，则想要弹出zakat捐款须知
    // submit
    $UI.on('click', '.nextQurbanBtn', function() {
        submitData = getSubmitParam.init();

        // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
        if (_validate.check(submitData, lang)) {
            $('.alert-qurban-bank').css('display', 'block')
        }

    });


    // 判断项目分类ID，如果为13说明是zakat项目，则想要弹出zakat捐款须知
    // submit
    $UI.on('click', '.commonNextBtn', function() {
        submitData = getCommonSubmitParam.init();
        // minimumDonation
        submitData.minimum_donation = rs.minimum_donation_value;

        console.log('submitData = ', submitData)

        // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
        if ((category_id != 15 && _commonValidate.check(submitData, lang))) {
            let totalDonation;

            store.set('donateMoney', submitData);
            $('.alert-common-bank').css('display', 'block');

            if ($('.alert-bank').css('display') == 'block') {
                $('.wrap-bank-note').css("display", "block");
            } else {
                $('.wrap-bank-note').css("display", "none");
            }    

            // if zakat
            if (category_id == 13) {
                totalDonation = submitData.money;
            } else {
                totalDonation = _calculationFormula.totalDonation();
            }
            
            const saldoGopay = commonBankList.getGopayData();
            console.log("saldoGopay", saldoGopay);
            console.log("total donation", totalDonation);

            if (saldoGopay !== null) {
                if (Number(saldoGopay.data.GopayBalance) < totalDonation) {
                    $('.alert-common-bank .gopay').addClass("disabled");
                    $('.alert-common-bank .wrap-not-enough').css("display", "flex");
                } else {
                    $('.alert-common-bank .gopay').removeClass("disabled");
                    $('.alert-common-bank .wrap-not-enough').css("display", "none");
                }
            }
        }

    });
};




// 实际支付总金额 + 费率更改
function chooseQurbanBankNext(res) {
    originPrice = parseInt($('.qurban-wrap').attr('price'));
    bankChannelFee = $(".alert-qurban-bank").attr('data-methodFee') || 0;
    bankCode = $(".alert-qurban-bank").attr('data-bankcode');
    expense_type = $(".alert-qurban-bank").attr('data-expenseType');
    // channelFee

    if (expense_type == 2) {
        channelFee = Math.ceil(originPrice / (1 - bankChannelFee)) - originPrice
        storeSet()
    } else if (expense_type == 1) {
        channelFee = bankChannelFee
        storeSet()
    }

    console.log('channelFee', channelFee, originPrice);

    function storeSet() {
        store.set('chooseBank', {
            'bankcode': $('.alert-qurban-bank').attr('data-bankcode'),
            'bankImg': $('.alert-qurban-bank').attr('data-bankImg'),
            'bankName': $('.alert-qurban-bank').attr('data-bankName'),

            'originPrice': originPrice,
            'channelFee': channelFee,
        })
    }

    return location.href = '/confirmOrderTest.html?project_id=' + project_id +
        // '&from=' + from +
        (from ? ('&from=' + from) : '') + //避免下级页面undefined
        '&short_link=' + short_link +
        '&short_url=' + short_url +
        '&originPrice=' + originPrice +
        '&channelFee=' + channelFee +
        '&dataBankCode=' + bankCode +
        '&qurbanName=' + encodeURIComponent($('.qurban-wrap').attr('qurbanName')) +
        '&countPackage=' + countPackage
}

function chooseCommonBankNext(res) {
    originPrice = parseInt($('#target_amount').attr('data-amount'));
    // originPrice = parseInt($('.campaign-money').attr('data-amount'));
    bankCode = $(".alert-common-bank").attr('data-bankcode');
    expense_type = $(".alert-qurban-bank").attr('data-expenseType');
    const saldoGopay = commonBankList.getGopayData();

    if ($('.recommendation').hasClass('add')) {
        ifRec = true;
        recMoney = $('.recmdt_amount').attr('data-amount');
    } else {
        ifRec = false;
        recMoney = 0
    }
    // bankChannelFee = $(".alert-qurban-bank").attr('data-methodFee') || 0;

    store.set('chooseBank', {
        'bankcode': $('.alert-common-bank').attr('data-bankcode'),
        'bankImg': $('.alert-common-bank').attr('data-bankImg'),
        'bankName': $('.alert-common-bank').attr('data-bankName'),

        'originPrice': originPrice,
        // 'channelFee': channelFee,
    })

    let project_url = '/confirmOrderTest.html?project_id=' + project_id +
        (from ? ('&from=' + from) : '') + //避免下级页面undefined
        '&short_link=' + short_link +
        '&short_url=' + short_url +
        '&originPrice=' + originPrice +
        '&ifRec=' + ifRec +
        '&dataBankCode=' + bankCode +
        '&recMoney=' + recMoney +
        (saldoGopay != null ? ('&gopayToken=' + 1) : '')

    let group_url = '/confirmOrderTest.html?group_id=' + group_id +
        (from ? ('&from=' + from) : '') + //避免下级页面undefined
        '&short_link=' + short_link +
        '&short_url=' + short_url +
        '&originPrice=' + originPrice +
        '&ifRec=' + ifRec +
        '&dataBankCode=' + bankCode +
        '&recMoney=' + recMoney +
        (saldoGopay != null ? ('&gopayToken=' + 1) : '')

    return location.href = group_id ? group_url : project_url

}
export default obj;