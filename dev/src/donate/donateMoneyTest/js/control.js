import view from './view'
import model from './model'
import store from 'store'
import 'loading'
import '../less/main.less'
// 引入依赖

import qurbanBankList from 'qurbanBankList'
import commonBankList from 'commonBankList'

import utils from 'utils'
import domainName from 'domainName' // port domain name
import commonMoneyInputChange from './_commonMoneyInputChange'
import commonDonateRecommendation from 'commonDonateRecommendation' //联合支付
import _recommondationAmount from "./_recommondationAmount"; //联合支付处资金公式
import _calculationFormula from "./_calculationFormula";

/* translation */
import system from 'system'
import qscLang from 'qscLang'
let lang = qscLang.init(system);
/* translation */

let UI = view.UI;
let generateQRcode;
let deeplink_redirect;
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'] || 0;
let short_link = reqObj['short_link'];
let fromLogin = reqObj['fromLogin'];
let from = reqObj['from'];
let category_id;

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';


let group_id = reqObj['group_id'] || 0;
// let group_id = store.get('groupDonate') ? store.get('groupDonate').group_id : JSON.parse(getCookie('groupDonate')).group_id

// function getCookie(cname) {
//     var name = cname + "=";
//     var ca = document.cookie.split(';');
//     for (var i = 0; i < ca.length; i++) {
//         var c = ca[i].trim();
//         if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
//     }
//     return "";
// }

// getCookie('groupDonate');

// qurbanBankList.init(category_id);
// commonBankList.init(category_id);
// view.init({});

// getProjInfo();
if (group_id) {
    getGroupProjInfo(group_id)
} else {
    getProjInfo(project_id)
}

function getGroupProjInfo(rs) {
    model.getGroupProjInfo({
        param: {
            group_id: group_id,
            short_link: short_link
        },
        success: function (res) {
            if (res.code == 0) {
                view.getGroupProjInfo(res);
                //  get payment channel
                model.getPayment({
                    param: {
                        bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
                    },
                    success: function (rs) {
                        if (rs.code == 0) {

                            category_id = 12;
                            commonBankList.init(category_id);

                            res.data.category_id = ''

                            // rs.getProjInfoData = res;

                            // move b to donate
                            // rs.user_id = user_id
                            // viewB.init(rs);
                            view.getProjInfo(res);
                            getProjectPoundage(res); //项目手续费情况

                            // view.init(rs);
                        } else {
                            utils.alertMessage(rs.msg)
                        }
                    },
                    error: utils.handleFail
                })

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })

}


function getProjInfo(rs) {
    model.getProjInfo({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function (res) {
            if (res.code == 0) {
                category_id = res.data.category_id;

                const packageCampaign = false;

                if (packageCampaign) {
                    category_id = 999;
                }

                // move b to donate
                if (res.data.category_id == 15 || res.data.category_id == 18) {
                    qurbanBankList.init(category_id);

                } else {
                    commonBankList.init(category_id);

                }
                view.getProjInfo(res);
                getProjectPoundage(res); //项目手续费情况
                // getPayment(res); //获取银行列表


            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// 项目手续费情况
function getProjectPoundage(rs) {
    console.log(rs)
    model.getProjectPoundage({
        param: {
            project_id: project_id,
            category_id: rs.data.category_id
        },
        success: function (res) {
            if (res.code == 0) {
                res.minimum_donation = rs.data.minimum_donation;
                console.log('===getProjectPoundage===', res.data);

                // res.category_id = rs.data.category_id;
                rs.ratioData = res.data
                getPayment(rs); //获取银行列表
                res.category_id = rs.data.category_id
                commonMoneyInputChange.init(res); //监听input输入

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}


// getPayment();



// 获取银行列表
function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
            // bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function (rs) {
            if (rs.code == 0) {

                rs.getProjInfoData = res;

                rs.user_id = user_id
                rs.ratioData = res.ratioData
                rs.category_id = category_id;

                console.log('rs-view', rs);

                view.init(rs);


                if (category_id == 15) {
                    getQurbanItems(res);
                }

                if (category_id == 18) {
                    getPackageItems(res);
                }

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

/*
 submit form
*/

UI.on('submit', function (e, objData) {
    // loginHandleB(objData);
});


UI.on('canRec', function (e, rs) {
    console.log('canRec', rs);
    commonDonateRecommendation.init(rs); //联合支付
    _recommondationAmount.init(rs);
})

// 添加联合支付
UI.on('recommendationAdd', function (e, $this) {
    view.showRecommendationAmountList($this);

    _calculationFormula.init(); //资金公式
})


function getQurbanItems(rs) {
    model.getQurbanItems({
        param: {
            project_id: project_id
        },
        success: function (rs) {
            if (rs.code == 0) {
                const packageCampaign = false;

                if (packageCampaign) {
                    category_id = 999;
                }            
                rs.category_id = category_id;
                view.insertQurbanItems(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

// get package items
function getPackageItems(rs) {
    model.getPackageItems({
        param: {
            project_id: project_id
        },
        success: function (rs) {
            if (rs.code == 0) {
                const packageCampaign = false;

                if (packageCampaign) {
                    category_id = 999;
                }
                rs.category_id = category_id;
                view.insertQurbanItems(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}