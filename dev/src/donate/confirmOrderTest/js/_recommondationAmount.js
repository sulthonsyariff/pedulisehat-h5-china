import mainTpl from '../tpl/_recommondationAmount.juicer'

let obj = {};
let $UI = $('body');
obj.UI = $UI;

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {
    $('.recommendationAmountList').append(mainTpl(res));
    // $('.rec-show').css('display', 'block');
};

export default obj;