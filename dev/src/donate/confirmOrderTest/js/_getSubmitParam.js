import utils from 'utils'
import store from 'store'
import 'jq_cookie' //ajax cookie
import getPayPlatform from 'getPayPlatform'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0;
// OVO合作项目
let from = reqObj['from'] || '';
//分享后捐款需要吧
let statistics_link = reqObj['statistics_link'] || ''; //分享后捐款需要吧，统计分享排行需要
let client_id = $.cookie('client_id') || ''; //分享后捐款需要吧，区别唯一机型吧
// 项目ID
let project_id = reqObj['project_id'] || '';
// 专题ID
let group_id = reqObj['group_id'] || '';
// qurban 项目
let qurbanName = reqObj['qurbanName'] || '';
// short url for supporter ranking
let short_url = reqObj['short_url'] || '';
//联合互助需要
let ifRec = reqObj['ifRec'] || '';
let recMoney = reqObj['recMoney'] || '';
let originPrice = reqObj['originPrice'] || '';
let gopayToken = reqObj['gopayToken'] || '';

/**
 * 支付平台（ 1. faspay 2. gopay 3 sinamas 4 ovo 5 dana 6 shopeepay）
 *
 * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 4
 * dana: pay_platform = 5, bank_code = 5
 * ShopeePay :pay_platform = 6 , bank_code: "6" / 7
 */
obj.init = function() {
    let bank_code = $(".channel").attr('data-bankCode');
    // let support_price = $('#target_amount').val().replace(/\./g, '') || $('.amount-card.chosed span').text().replace(/\./g, '');
    let privacy_policy = $('.open-icon').hasClass('open') ? 2 : 1;
    let support_price;

    let sign; // 专题捐赠才需要
    let project_ratio; // 专题捐赠才需要
    let name; // 专题捐赠才需要

    // 联合互助
    if (ifRec == 'true') {
        support_price = originPrice;
    } else {
        support_price = $('.total-money').attr('total-price').replace(/\./g, '');
    }

    // 专题捐赠才需要
    if (group_id) {
        if (store.get('groupDonate')) {
            sign = store.get('groupDonate').sign
            project_ratio = store.get('groupDonate').project_ratio
            name = store.get('groupDonate').name
        } else {
            sign = JSON.parse(getCookie('groupDonate')).sign
            project_ratio = JSON.parse(getCookie('groupDonate')).project_ratio
            name = JSON.parse(getCookie('groupDonate')).name
        }
    }

    let params = {
        avatar: '',
        user_name: $('.name-box').val() || '', //用户名 如果是想匿名直接传 Anonymity
        phone: $('#mobile').val() || '',
        email: $('#email').val(),
        money: parseInt(support_price),
        comment: $("textarea[name=commentArea]").val(), //留言 备注：$('#story').val()不能获取到值？？？
        privacy_policy: privacy_policy, //隐私策略 1正常 2匿名
        client_id: client_id,
        payment_channel: bank_code, //支付渠道
        payment_channel_name: $(".payment_channel .bank-name").text(), //支付渠道名字
        terminal: utils.terminal,
        version_code: appVersionCode,
        version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
        pay_platform: getPayPlatform(bank_code), //支付平台（1.faspay 2. gopay 3 sinamas 4ovo 5dana 6shopeepay）
        reference: (from == 'OVOXPeduliSehat') ? 'OVO' : '', // 引用源:OVO合作项目
        statistics_link: statistics_link || '', //分享后捐款需要吧
        short_link: short_url,

        // zakat / Bulk / qurban / 普通项目
        project_id: project_id || '',
        // 古尔邦项目
        sub_commodity: decodeURIComponent(qurbanName) || '', //古尔邦项目使用，传牛羊接口那个name
        // 专题项目
        group_id: parseInt(group_id) || '',
        ratio: project_ratio || '', //专题项目捐赠比例
        ratio_sign: sign || '', //专题项目捐赠比例
        name: name || '', //专题项目
        // ab_test: {
        //     'detail_page': get_detail_page() // AB 测试需要
        // },
    }

    // if gopay tokenization pay
    if (bank_code == 1 && gopayToken == 1) {
        params.gopay_payment_mode = 1;
    }

    // 联合互助参数不一样
    if (ifRec == 'true') {
        // console.log('===jointPayParams===', params);
        return jointPayParams(params);
    } else {
        // console.log('===params===', params);
        return params;
    }

}

/**
 *
 * 提交联合支付捐款
qa:14221633053369604515
pre:18256536075545746291
live:12419737548014998025
 */
function jointPayParams(params) {
    let total_money = parseFloat($('.total-money').attr('total-price').replace(/\./g, ''));
    // let total_money = parseFloat($('#price').attr('data-amount'));
    let item = {};
    let joint_items = [];

    // 获取联合互助产品
    for (let i = 0; i < 1; i++) {
        // let _projectId = $($('.recommendation.add')[i]).attr('data-project-id');
        let _money = recMoney;
        // let _money = $($('.recommendation.add .rec-money')[i]).attr('data-amount');
        let _projectId;

        if (utils.judgeDomain() == 'qa') {
            _projectId = '14221633053369604515';
        } else if (utils.judgeDomain() == 'pre') {
            _projectId = '18256536075545746291';
        } else {
            _projectId = '12419737548014998025';
        }

        item.project_id = _projectId;
        item.money = parseFloat(_money);
        joint_items.push(item)
    }

    params.total_money = total_money;
    params.joint_items = joint_items;

    return params;
}

// 获取cookie中group订单信息
function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
};

// function get_detail_page() {
//     if (store.get('test-AB') && store.get('test-AB') == 'a') {
//         return 'original'
//     } else {
//         return 'rotate_play'
//     }
// }


export default obj;