var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'commonDonateFailure',
    htmlFileURL: 'html/commonDonateFailure.html',
    appDir: 'js/commonDonateFailure',
    uglify: true,
    hash: '',
    mode: 'development'
})