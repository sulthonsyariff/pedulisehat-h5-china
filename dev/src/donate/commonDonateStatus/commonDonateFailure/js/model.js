import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// let isLocal = location.href.indexOf("pedulisehat.id") == -1;
// let reqObj = utils.getRequestParams();
// let order_id = reqObj['order_id'];

// obj.getPayStatus = function(o) {
//     var url = domainName.trade + '/v1/gopay_state?order_id=' + order_id +'referer=fromPeduliSehat';

//     if (isLocal) {
//         url = '../mock/.json';
//     }

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url
//     }, o)
// }

/**
 * 获取项目信息
 */
obj.getProjInfo = function (o) {
    let url = domainName.project + '/v1/project/detail?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link;
    if (isLocal) {
        url = 'mock/detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

obj.getOrderState = function(o) {
    // let url = domainName.trade + '/v1/ovo_state?order_id=' + o.param;
    //切换为通用渠道支付状态查询接口
    let url = domainName.trade + '/v1/support/' + o.param;
    if (isLocal) {
        url = 'mock/v1_ovo_support.json'
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}


export default obj;