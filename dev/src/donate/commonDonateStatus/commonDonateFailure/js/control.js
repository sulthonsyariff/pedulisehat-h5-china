import view from './view'
import model from './model'
import 'loading'
import '../less/commonDonateFailure.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();

// let project_id = '';


let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let order_id = reqObj['order_id'];
let amount = reqObj['amount'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
let group_id = reqObj['group_id'];
let from = reqObj['from'];
//失败页传入errorMsg 则为ovo支付渠道
let errorMsg = reqObj['errorMsg'];



// 隐藏loading
utils.hideLoading();


if (project_id) {
    model.getProjInfo({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(res) {
            if (res.code == 0) {
                view.init(res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
} else {
    view.init({});
}


/****
 * 当 check payment API 发现该订单状态改为已支付的话，
 * 如果用户当前还停留在支付失败页，则支付失败页自动跳转为支付成功页
 *
 * 65S，OVO支付失败后（本来成功了）OVO会再给一个订单状态的结果，估结束页继续监听接口状态，以便跳转成功页
 *
 * OVO详情页时30秒轮询 支付失败后继续轮询>25S即可
 * 十秒后轮询接口，三秒轮询一次，轮询十次，以便全面覆盖到，也避免重复调用接口
 *
 */


//  if(errorMsg){
setTimeout(() => {
    let _num = 1;
    // 轮询结果
    let timer = setInterval(() => {
        console.log('_num = ', _num);

        judgeOrderState();

        if (_num == 10) {
            console.log('===clearInterval===');
            clearInterval(timer);
        }

        _num++;

    }, 3000);
}, 10000);
//  }

/*
 * judge order state
 1 未支付
 2 已支付
 3 提现
 4 退款
 5 返还
 10 手机号有问题
 100 取消订单
 */
function judgeOrderState() {
    model.getOrderState({
        param: order_id,
        success: function(res) {
            if (res.code == 0) {
                // console.log('res = ', res);
                // 2 已支付
                if (res.data.state == 2) {
                    //sensors
                    sensors.track('PayOrder', {
                        from: document.referrer,
                        order_id: order_id,
                        order_amount: amount,
                        is_succeed: true
                    })

                    // 安卓项目
                    if (appVersionCode > 128 && project_id) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' +
                            encodeURIComponent(project_id) +
                            '&order_id=' + encodeURIComponent(order_id);

                    }
                    // 安卓专题
                    else if (appVersionCode >= 202 && group_id && short_link) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?order_id=' +
                            encodeURIComponent(order_id) +
                            '&short_link=' + encodeURIComponent(short_link);

                    }
                    // H5项目+专题
                    else {
                        location.href = '/commonDonateSuccess.html?order_id=' + order_id +
                            '&short_link=' + short_link +
                            // '&terminal=' + res.data.terminal +
                            (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                            (group_id ? ('&group_id=' + group_id) :
                                ('&project_id=' + project_id))
                    }

                    // if (appVersionCode > 128) {
                    //     location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' + project_id + '&order_id=' + order_id;
                    // } else {
                    //     if (project_id) {
                    //         location.href = '/commonDonateSuccess.html?project_id=' + project_id +
                    //             '&short_link=' + short_link +
                    //             '&order_id=' + order_id +
                    //             (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    //     } else {
                    //         location.href = '/commonDonateSuccess.html?group_id=' + group_id +
                    //             '&short_link=' + short_link +
                    //             '&order_id=' + order_id +
                    //             (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');
                    //     }
                    // }
                }
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}