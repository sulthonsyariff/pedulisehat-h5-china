import mainTpl from '../tpl/commonDonateFailure.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'

/* translation */
import goPayUnFinish from 'goPayUnFinish'
import qscLang from 'qscLang'
let lang = qscLang.init(goPayUnFinish);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let group_id = reqObj['group_id'];
let short_link = reqObj['short_link'];
let errorMsg = reqObj['errorMsg'] || '';
let category_id;
let ovoErrorMessageStore = store.get('ovoErrorMessageStore') || '';



obj.init = function(rs) {


    rs.lang = lang;
    rs.JumpName = '';
    if (project_id) {
        rs.commonNavGoToWhere = '/' + short_link;

        category_id = rs.data.category_id
            // if (category_id == 15) {
            //     rs.commonNavGoToWhere = '/donateMoney2.html?project_id=' + project_id + '&short_link=' + short_link;
            // } else {
            //     rs.commonNavGoToWhere = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link;
            // }
    } else {
        rs.commonNavGoToWhere = '/group/' + short_link
    }

    if (errorMsg) {
        rs.ovoErrorMessageStore = ovoErrorMessageStore;
    } else {
        rs.ovoErrorMessageStore = '';
    }

    commonNav.init(rs);
    rs.domainName = domainName;

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);
    fastclick.attach(document.body);

    utils.hideLoading();


    $('.payAgain').on('click', function(e) {
        if (project_id) {
            // if (category_id == 15) {
            //     location.href = '/donateMoney2.html?project_id=' + project_id + '&short_link=' + short_link;
            // } else {
            location.href = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link;
            // }
        } else {
            location.href = '/donateMoney.html?short_link=' + short_link + '&group_id=' + group_id
        }
    })
    $('.reviewCampaign').on('click', function(e) {
        if (project_id) {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true' : '/' + short_link
        } else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/special_projects?short_link=' + short_link : '/group/' + short_link
        }
    })

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();



    //sensors
    // sensors.track('PayOrder', {
    //     from: document.referrer,
    //     order_id: '',
    //     order_amount: '',
    //     is_succeed: false
    // })
};




export default obj;