var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'commonDonateFailure',
    htmlFileURL: 'html/commonDonateFailure.html',
    appDir: 'js/commonDonateFailure',
    uglify: true,
    hash: '',
    mode: 'production'
})