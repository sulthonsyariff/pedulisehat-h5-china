var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'commonDonateSuccess',
    htmlFileURL: 'html/commonDonateSuccess.html',
    appDir: 'js/commonDonateSuccess',
    uglify: true,
    hash: '',
    mode: 'development'
})