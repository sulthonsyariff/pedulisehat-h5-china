import mainTpl from '../tpl/commonDonateSuccess.juicer'
import commonDonateSuccess from 'commonDonateSuccess'
import commonNav from 'commonNav'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'
import _donateToast from '../tpl/_donateToast.juicer';

/* translation */
import donateSuccess from 'donateSuccess'
let lang = qscLang.init(donateSuccess);
import common from 'common'
import qscLang from 'qscLang'
let commonLang = qscLang.init(common);
/* translation */

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || '';
let short_link = reqObj['short_link'];
let [obj, $UI] = [{}, $('body')];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
var category_id = reqObj['category_id'];

obj.UI = $UI;

obj.donateToast = function (rs){
    $UI.append(_donateToast(rs));
}

obj.init = function(rs) {

    rs.getLanguage = utils.getLanguage();
    rs.lang = lang;
    rs.JumpName = '';
    if (project_id) {
        rs.commonNavGoToWhere = '/' + short_link;

        // if (category_id == 15) {
        //     rs.commonNavGoToWhere = '/donateMoney2.html?project_id=' + project_id + '&short_link=' + short_link;
        // } else {
        //     rs.commonNavGoToWhere = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link;
        // }
    } else {
        rs.commonNavGoToWhere = '/group/' + short_link
    }

    rs.data.paid_money = changeMoneyFormat.moneyFormat(rs.data.paid_money)
    rs.appVersionCode = appVersionCode

    rs.group_id = group_id
    rs.project_id = project_id

    commonNav.init(rs);
    rs.domainName = domainName;

    $UI.append(mainTpl(rs));
    commonDonateSuccess.init(rs);

    fastclick.attach(document.body);
    utils.hideLoading();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    //sensors
    // sensors.track('PayOrder', {
    //     from: document.referrer,
    //     order_id: '',
    //     order_amount: '',
    //     is_succeed: true
    // })

};



export default obj;