var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'commonDonateSuccess',
    htmlFileURL: 'html/commonDonateSuccess.html',
    appDir: 'js/commonDonateSuccess',
    uglify: true,
    hash: '',
    mode: 'production'
})