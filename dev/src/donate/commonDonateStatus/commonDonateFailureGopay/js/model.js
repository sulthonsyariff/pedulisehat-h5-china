import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

let obj = {};
/**
 * 获取项目信息 yangcheng
 */
obj.getProjInfo = function(o) {
    var url = domainName.project + '/v1/project/detail?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}


/**
 * 获取专题信息 yangcheng
 */
obj.groupDeatils = function(o) {
    var url = domainName.project + '/v1/group_project?short_link=' + o.param.short_link;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

//新分享统计接口
obj.newProjectShare = function(o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//获取分享相关内容
obj.getShareInfo = function(o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

// 获取订单详情接口
// /v1/support/
obj.getPro = function(o) {
    var url = domainName.trade + '/v1/support/' + o.param.bill_no;

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}

// obj.getListData = function(o) {
//     let url = domainName.project + '/v1/public?page=' + 1 + '&limit=10&state=8192&category_id=12,13,14&project_id=' + o.param.project_id;;

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url
//     }, o)
// };

export default obj;