import view from './view'
import model from './model'
import model_getListData from 'model_getListData' //列表接口调用地址
import 'loading'
import 'jq_cookie' //ajax cookie
import commonFooter from 'commonFooter'

// 引入依赖
import utils from 'utils'
import store from 'store'
import domainName from 'domainName'; //port domain name
import commonDonateFailureGopay from 'commonDonateFailureGopay'
import '../less/commonDonateSuccess.less'

let UI = view.UI;
let reqObj = utils.getRequestParams();

let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || '';

let short_link = reqObj['short_link'] || '';

let CACHED_KEY = 'VA';
// let VAorder = JSON.parse(sessionStorage.getItem(CACHED_KEY));

let bill_no = reqObj['order_id'] || '';
// let item_id;
// let item_type;

let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

if (sessionStorage.getItem('projectShare')) {
    location.href = '/donateShareSuccess.html?short_link=' + sessionStorage.getItem('projectShare')
    sessionStorage.removeItem('projectShare')
}


// 隐藏loading
utils.hideLoading();
commonFooter.init({});

model.getPro({
    param: {
        bill_no: bill_no
    },
    success: function(res) {
        project_id ? getProjInfo(res) : groupDeatils(res)
    }
})

// get project detail data
function getProjInfo(res) {
    model.getProjInfo({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.data.paid_money = res.data.paid_money
                rs.data.donor = res.data.user_name // campaigner name

                store.set('share-info', rs) //分享成功页需要

                view.init(rs);
                getListData(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },

        error: utils.handleFail
    })
}

// get project special subjects data
function groupDeatils(res) {
    model.groupDeatils({
        param: {
            group_id: group_id,
            short_link: short_link
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.data.paid_money = res.data.paid_money
                rs.data.donor = res.data.user_name // campaigner name
                rs.data.image = JSON.parse(rs.data.image);
                rs.from = 'group'

                store.set('share-info', rs) //分享成功页需要

                view.init(rs);
                getListData(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },

        error: utils.handleFail
    })
}

UI.on('projectShareSuccess', function(e, target_type) {
    // if (project_id) {
    //     share_type = 1
    //     project_id = project_id
    // } else {
    //     share_type = 2
    //     project_id = group_id
    // }
    shareActionCount(target_type);
});

/****
 TargetTypefacebook = 1 // facebook
 TargetTypeWhatsApp = 2 // whatsapp
 TargetTypeTwitter = 3 // twitter
 TargetTypeLink = 4 // link
 TargetTypeLine = 5 // line
 TargetTypeInstagram = 6 // instagram
 TargetTypeYoutube = 7 // youtube
 */
function shareActionCount(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: project_id || group_id,
            item_type: project_id ? 1 : 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            scene: 2, //1:表示捐赠/付款前的分享 2:表示捐赠/付款成功后的分享 参数不传 默认为1
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位
        },
        success: function(res) {
            //分享成功后
            if (res.code == 0) {
                // whatsapp / twitter 的移动版
                if ((target_type == 2 || target_type == 3) && !utils.isPC()) {
                    sessionStorage.setItem('projectShare', short_link)
                } else {
                    location.href = '/donateShareSuccess.html?short_link=' + short_link
                }

                //分享操作在本页面跳转其他页面，需要后退操作回到站内获取标记自动跳转
                // if (Obj.platform == 'mobileH5Whatsapp' || Obj.platform == 'mobileH5Twitter') {
                //     sessionStorage.setItem('projectShare', short_link)
                // }

                // //分享操作另开标签页，原页面不跳转 则点击分线按钮直接跳转
                // if ((target_type == 2 || target_type == 3) && Obj.platform == 'PC' || target_type == 1 || Obj.platform == 'line') {
                //     location.href = '/donateShareSuccess.html?short_link=' + short_link
                // }
            } else {
                utils.alertMessage(res.msg);
            }
        },
        error: utils.handleFail
    });
}



function getListData(rs) {
    model_getListData.getListData_commonDonateSuccess({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                commonDonateFailureGopay.insertData(res)
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    });
}

// 监听安卓分享成功回调
window.changeShareNum = function(target_type) {
    andriodChangeShareNum(target_type);
}

function andriodChangeShareNum(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: project_id || group_id,
            item_type: project_id ? 1 : 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            scene: 2,
        },
        success: function(res) {
            //分享成功后
            if (res.code == 0) {

                location.href = '/donateShareSuccess.html?short_link=' + short_link
            } else {
                utils.alertMessage(res.msg);
            }
        },
        error: utils.handleFail
    });
}