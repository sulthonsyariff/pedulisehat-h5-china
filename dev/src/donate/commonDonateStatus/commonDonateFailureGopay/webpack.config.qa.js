var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'commonDonateFailureGopay',
    htmlFileURL: 'html/commonDonateFailureGopay.html',
    appDir: 'js/commonDonateFailureGopay',
    uglify: true,
    hash: '',
    mode: 'development'
})