var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'donateSuccess',
    htmlFileURL: 'html/donateSuccess.html',
    appDir: 'js/donateSuccess',
    uglify: true,
    hash: '',
    mode: 'production'
})