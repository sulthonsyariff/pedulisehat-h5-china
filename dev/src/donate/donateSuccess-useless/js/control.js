import view from './view'
import model from './model'
import 'loading'
import '../less/donateSuccess.less'
// 引入依赖
import utils from 'utils'
import store from 'store'
import domainName from 'domainName'; //port domain name

let UI = view.UI;
let reqObj = utils.getRequestParams();
let billNo = reqObj['bill_no'];
let status = reqObj['status'];
let signature = reqObj['signature'];

window.location.href = domainName.trade + "/v1/faspay_redirect?bill_no=" + billNo + "&status=" + status + "&signature=" + signature

// 隐藏loading
utils.hideLoading();