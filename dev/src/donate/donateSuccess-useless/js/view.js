import mainTpl from '../tpl/donateSuccess.juicer'
import commonNav from 'commonNav'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics'

/* translation */
import donateSuccess from 'donateSuccess'
let lang = qscLang.init(donateSuccess);
import common from 'common'
import qscLang from 'qscLang'
let commonLang = qscLang.init(common);
/* translation */

let reqObj = utils.getRequestParams();
// let project_id = reqObj['project_id'];

// let short_link = reqObj['short_link'];
// let payStates = reqObj['result'];




// let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;



// obj.init = function (rs) {
//     console.log('rs', rs)
// 获取meta标签中的分享内容
// let meta_shareUrl = $("[property='og:url']").attr('content');
// let meta_shareTitle = $("[property='og:title']").attr('content');
// let meta_shareDescription = $("[property='og:description']").attr('content');
// let meta_shareImage = $("[property='og:image']").attr('content');

// // 如果有特定的分享链接，则分享特定的链接地址
// let shareUrl = meta_shareUrl;
// let shareTitle = meta_shareTitle;
// let shareDescription = meta_shareDescription;
// let shareImage = meta_shareImage;
// let shareWords = commonLang.lang28; //分享语

// let jumpShareSuccessUrl = '/donateShareSuccess.html?short_link=' + rs.data.short_link

// rs.getLanguage = utils.getLanguage();
// rs.lang = lang;
// rs.JumpName = '';
// rs.commonNavGoToWhere = '/donateMoney.html?project_id=' + rs.data.project_id + '&short_link=' + rs.data.short_link
// rs.data.paid_money = changeMoneyFormat.moneyFormat(rs.data.paid_money)
// commonNav.init(rs);
// rs.domainName = domainName;


// $UI.append(mainTpl(rs));
// fastclick.attach(document.body);
// utils.hideLoading();

// // close APP download
// if ($('.app-download').css('display') === 'block') {
//     $('.page-inner').css('padding-top', '118px')
// }
// $('body').on('click', '.appDownload-close', function (e) {
//     // store.set('app_download', 'false')

//     $('.app-download').css({
//         'display': 'none'
//     })
//     $('.page-inner').css('padding-top', '72px')

// });
// // open andriod share
// $('body').on('click', '.andriod-share', function (e) {
//     // $('.bubble-icon').css('display', 'none');

//     if (utils.browserVersion.android) {
//         $UI.trigger('android-share');
//     }
// });

// //facebook
// $('body').on('click', '.fb', function (e) {
//     e.preventDefault();
//     utils.fbShare(shareUrl, callback);
//     //分享计数成功后执行
//     function callback() {
//         let params = {
//             // platform: platform,
//             target_type: 1
//         }
//         // 分享后数字改变
//         $UI.trigger('projectShare', params);
//     }
//     // 分享后关闭多渠道分享弹窗
//     $('.common-share').css('display', 'none');

//     return false;
// });
// //whatsapp
// $('body').on('click', '.whatsApp', function () {
//     utils.whatsappShare(shareWords, shareUrl, callback);
//     //分享计数成功后执行
//     function callback(platform) {
//         let params = {
//             platform: platform,
//             target_type: 2
//         }
//         // 分享后数字改变
//         $UI.trigger('projectShare', params);
//         // console.log('分享后数字改变');
//     }
//     // 分享后关闭多渠道分享弹窗
//     $('.common-share').css('display', 'none');
//     return false;
// });
// //twitter
// $('body').on('click', '.twitter', function () {
//     utils.twitterShare(shareWords, shareUrl, callback);
//     //分享计数成功后执行
//     function callback(platform) {
//         // 分享后数字改变
//         let params = {
//             platform: platform,
//             target_type: 3
//         }
//         $UI.trigger('projectShare', params);
//     }
//     // 分享后关闭多渠道分享弹窗
//     $('.common-share').css('display', 'none');
//     return false;
// });

// // Line
// $('body').on('click', '.line', function () {
//     utils.lineShare(shareTitle, shareUrl, callback);
//     //分享计数成功后执行
//     function callback(platform) {
//         // 分享后数字改变
//         let params = {
//             platform: platform,
//             target_type: 5
//         }
//         $UI.trigger('projectShare', params);
//     }

//     // 分享后关闭多渠道分享弹窗
//     $('.common-share').css('display', 'none');

//     return false;
// });

// google anaytics
// let param = {};
// googleAnalytics.sendPageView(param);
// };





export default obj;