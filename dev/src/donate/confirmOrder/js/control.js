import view from './view'
import model from './model'
import store from 'store'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import domainName from 'domainName' // port domain name
import commonDonateRecommendation from 'commonDonateRecommendation' //联合支付
import _recommondationAmount from "./_recommondationAmount"; //联合支付处资金公式
import _calculationFormula from "./_calculationFormula";

import qurbanBankList from 'qurbanBankList'
import commonBankList from 'commonBankList'

import paySubmitSuccessHandle from 'paySubmitSuccessHandle' //支付成功跳转逻辑

/* translation */
import system from 'system'
import qscLang from 'qscLang'
let lang = qscLang.init(system);
/* translation */

let UI = view.UI;
let generateQRcode;
let deeplink_redirect;
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || '';
let short_link = reqObj['short_link'] || '';
let fromLogin = reqObj['fromLogin'] || '';
let from = reqObj['from'] || '';
let category_id;

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';

/*
 * H5: visit login or login
 */
// visit login or login

// qurbanBankList.init({});


if ((reqObj.fromLogin == 'loginB' && store.get('donateMoney') && store.get('donateMoney').payment_channel) ||
    (store.get('donateMoney') && store.get('donateMoney').user_name && store.get('donateMoney').phone && store.get('donateMoney').payment_channel)) {
    console.log('fromLogin====B');

    loginHandleB(store.get('donateMoney'));
}
// load the juicer
else {
    if (group_id) {
        getGroupProjInfo();
    } else {
        getProjInfo(); // get projectInfo
    }
}

function getGroupProjInfo(rs) {
    model.getGroupProjInfo({
        param: {
            group_id: group_id,
            short_link: short_link
        },
        success: function(res) {
            if (res.code == 0) {
                view.getGroupProjInfo(res);
                //  get payment channel
                model.getPayment({
                    param: {
                        bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
                    },
                    success: function(rs) {
                        if (rs.code == 0) {

                            category_id = 12;
                            commonBankList.init(category_id);

                            res.data.category_id = ''

                            view.getProjInfo(res);
                            getProjectPoundage(res); //项目手续费情况

                            // view.init(rs);
                        } else {
                            utils.alertMessage(rs.msg)
                        }
                    },
                    error: utils.handleFail
                })

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })

}


function getProjInfo() {
    model.getProjInfo({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(res) {
            if (res.code == 0) {
                category_id = res.data.category_id;

                if (res.data.category_id == 15 || res.data.category_id == 18) {
                    qurbanBankList.init(category_id);

                } else {
                    commonBankList.init(category_id);

                }

                // move b to donate
                view.getProjInfo(res);
                getProjectPoundage(res); //项目手续费情况
                // getPayment(res); //获取银行列表

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// 项目手续费情况
function getProjectPoundage(rs) {
    model.getProjectPoundage({
        param: {
            project_id: project_id,
            category_id: rs.data.category_id
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('===getProjectPoundage===', res.data);
                rs.ratioData = res.data

                // res.category_id = rs.data.category_id;
                // moneyInputChange.init(res); //监听input输入
                getPayment(rs); //获取银行列表

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// 获取银行列表
function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log(' get payment channel =', rs);
                rs.getProjInfoData = res;
                rs.ratioData = res.ratioData

                // move b to donate
                rs.user_id = user_id
                getUserInfo(rs)
                    // view.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

function getUserInfo(res) {

    model.getUserInfo({
        success: function(rs) {
            res.email = rs.data.email
            view.init(res);

            // userFondRecord(objData)
        },
        unauthorizeTodo: function(rs) {
            view.init(res);
        },
        error: utils.handleFail
    });
}


/*
 submit form
*/

UI.on('submit-b', function(e, objData) {
    loginHandleB(objData);
});

// 安卓内调支付
UI.on('appHandle_createPayment', function(e, objData, _whichPay) {
    createPayment(objData, _whichPay);
});

// UI.on('canRec', function(e, rs) {
//     console.log('canRec', rs);
//     commonDonateRecommendation.init(rs); //联合支付
//     _recommondationAmount.init(rs);
// })

// 添加联合支付
// UI.on('recommendationAdd', function(e, $this) {
//     view.showRecommendationAmountList($this);

//     _calculationFormula.init(); //资金公式
// })

/**
 * judge login or visit login
 *
 */
function loginHandleB(objData) {
    model.getUserInfo({
        success: function(rs) {
            objData.avatar = rs.data.avatar;
            objData.phone = rs.data.mobile;
            objData.user_name = objData.user_name ? objData.user_name : rs.data.user_name; //如果选择匿名，优先匿名
            objData.fond_in = 'toruists_donated_b'

            // 1:group logined pay 2:logined pay 3:group unlogin pay 4:unlogin pay
            group_id ? createPayment(objData, 1) : createPayment(objData, 2); // 游客支付/登录支付/游客专题支付/登录专题支付
            // if (group_id) {
            //     //group logined pay
            //     createPayment(objData, 1)
            //         // loginCreateGroupPayment(objData);
            // } else {
            //     //logined pay
            //     createPayment(objData, 2)
            //         // loginCreatePayment(objData);
            // }
            // userFondRecord(objData)
        },
        unauthorizeTodo: function(rs) {
            // 1:group logined pay 2:logined pay 3:group unlogin pay 4:unlogin pay
            group_id ? createPayment(objData, 3) : createPayment(objData, 4); // 游客支付/登录支付/游客专题支付/登录专题支付

            // judge if visit login
            // if (store.get('donateMoney') && store.get('donateMoney').user_name && store.get('donateMoney').phone) {
            // if (group_id) {
            //     // unlogin group pay
            //     createPayment(objData, 3)
            //         // visitCreateGroupPayment(objData);
            // } else {
            //     // unlogin pay
            //     createPayment(objData, 4)
            //         // visitCreatePayment(objData);
            // }
            // }
        },
        error: utils.handleFail
    });
}

// 游客支付/登录支付/游客专题支付/登录专题支付，model请求链接不同（通过_whichPay区分）
function createPayment(objData, _whichPay) {
    // objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
    store.set('donateMoney', ''); //clear the cache
    
    // package campaign
    if (objData.category_id == 18) {
        if (objData.email.length < 1) {
            if (utils.getLanguage() == 'id') {
                utils.alertMessage('Mohon isi email data')
            } else {
                utils.alertMessage('Please fill email data')
            }
        } else {
            model.createPayment({
                _whichPay: _whichPay, // 此参数决定是 1:group logined pay 2:logined pay 3:group unlogin pay 4:unlogin pay
                param: objData,
                success: function(res) {
                    if (res.code == 0) {
                        paySubmitSuccessHandle(objData, res);
                    } else {
                        utils.alertMessage(res.msg)
                    }
                },
                error: utils.handleFail
            });    
        }
    } else {
        model.createPayment({
            _whichPay: _whichPay, // 此参数决定是 1:group logined pay 2:logined pay 3:group unlogin pay 4:unlogin pay
            param: objData,
            success: function(res) {
                if (res.code == 0) {
                    paySubmitSuccessHandle(objData, res);
                } else {
                    utils.alertMessage(res.msg)
                }
            },
            error: utils.handleFail
        });
    }
}


// /**
//  * group login pay
//  */
// function loginCreateGroupPayment(objData) {
//     objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
//     store.set('donateMoney', ''); //clear the cache

//     model.createGroupPayment(modelObj(objData))
// }



// /**
//  * login pay
//  */
// function loginCreatePayment(objData) {
//     objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
//     store.set('donateMoney', ''); //clear the cache

//     model.createPayment(modelObj(objData))
// }

// /**
//  * group vistit pay
//  */
// function visitCreateGroupPayment(objData) {
//     objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
//     store.set('donateMoney', ''); //clear the cache

//     model.visitCreateGroupPayment(modelObj(objData))
// }

// /**
//  * vistit pay
//  */
// function visitCreatePayment(objData) {
//     objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
//     store.set('donateMoney', ''); //clear the cache

//     model.visitCreatePayment(modelObj(objData))
// }

// function modelObj(objData) {
//     return {
//         param: objData,
//         success: function(res) {
//             if (res.code == 0) {
//                 paySubmitSuccessHandle(category_id, objData, res);
//             } else {
//                 utils.alertMessage(res.msg)
//             }
//         },
//         error: utils.handleFail
//     }
// }

/*****
 * 众筹 + 互助：yay账户（基金会账户） ，保险：pt公司账户
 *
 *  众筹： 成功 / commonDonateSuccess.html 失败 / commonDonateFailure.html
 *  互助：成功 / paymentSucceed.html 失败 / paymentFailed.html
 *
 */
// function successHandle(objData, res) {
//     utils.showLoading(lang.lang1);

//     // faspay
//     let OrderTransactionDetailUrl =
//         '/OrderTransactionDetail.html?project_id=' + project_id +
//         '&short_link=' + short_link +
//         '&channel=' + objData.payment_channel +
//         '&category_id=' + category_id;

//     // gopay
//     let goPayQRcodeUrl =
//         '/goPayQRcode.html?project_id=' + project_id +
//         '&short_link=' + short_link +
//         '&order_id=' + res.data.order_id +
//         '&amount=' + res.data.gross_amount;

//     // OVO
//     let OVOUrl =
//         '/OVO.html?project_id=' + project_id +
//         '&short_link=' + short_link +
//         '&order_id=' + res.data.order_id +
//         '&amount=' + (objData.total_money || objData.money) +
//         '&phone=' + objData.phone +
//         '&category_id=' + category_id +
//         (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '');

//     // dana
//     let danaUrl = domainName.trade +
//         '/v1/dana/oauth?Qsc-Peduli-Token=' + accessToken +
//         '&checkout_url=' + encodeURI(res.data.checkout_url);

//     // shopeePay QR
//     let shopeePayQRcodeUrl =
//         '/shopeePayQRcode.html?project_id=' + project_id +
//         '&short_link=' + short_link +
//         '&order_id=' + res.data.order_id;


//     // gopay: bank_code = 1
//     if (objData.payment_channel == '1') {
//         storeGopay(res);
//         setTimeout(() => {
//             location.href = goPayQRcodeUrl;
//             // location.href = '/goPay.html?project_id=' + project_id + '&short_link=' + short_link + '&order_id=' + res.data.order_id + '&amount=' + res.data.gross_amount + '&category_id=' + category_id
//         }, 300);
//     }
//     // sinamas: bank_code = 2
//     else if (objData.payment_channel == '2') {
//         storeSinamas(res);
//         setTimeout(() => {
//             location.href = OrderTransactionDetailUrl
//         }, 300);
//     }
//     // OVO:bank_code = 4
//     else if (objData.payment_channel == '4') {
//         console.log('ovo = ', res, res.data.order_id, objData);
//         setTimeout(() => {
//             location.href = OVOUrl;
//         }, 300);
//     }
//     // dana:bank_code = 5
//     else if (objData.payment_channel == '5') {
//         // storeDana(res);
//         console.log('dana = ', res);
//         if (user_id) {
//             setTimeout(() => {
//                 location.href = danaUrl;
//             }, 300);
//         } else {
//             setTimeout(() => {
//                 location.href = res.data.checkout_url
//             }, 300);
//         }
//     }
//     /****
//      * ShopeePay QR: bank_code = 6
//      "data": {
//          "request_id": "1596435591600568953",
//         "debug_msg": "success",
//         "store_name": "Peduli Sehat",
//          qr_content: "" //二维码的字符串
//          qr_url: "" //二维码地址
//         "order_id": "14221633053369627081",
//         "trade_id": "14221633053369627081"
//     },
//         *
//         */
//     else if (objData.payment_channel == '6') {
//         storeShopee(res);
//         setTimeout(() => {
//             location.href = shopeePayQRcodeUrl
//         }, 300);
//     }
//     /*****
//      * ShopeePay App: bank_code = 7
//     "data": {
//         "request_id": "1596433893054854741",
//         "debug_msg": "success",
//         "redirect_url_app": "shopeeid://main?apprl=%2Frn%2FT",//app跳转地址，暂时只用了这个
//         "redirect_url_http": "https://wsa.uat.wallet.a"
//     },
//     */
//     else if (objData.payment_channel == '7') {
//         console.log('ShopeePay APP = ', res);
//         // 跳转APP支付
//         setTimeout(() => {
//             utils.hideLoading();
//             location.href = res.data.redirect_url_app;
//         }, 300);
//     }

//     // faspay:bank_code = *
//     else if (objData.payment_channel == '707' ||
//         objData.payment_channel == '801' ||
//         objData.payment_channel == '708' ||
//         objData.payment_channel == '802' ||
//         objData.payment_channel == '408' ||
//         objData.payment_channel == '402' ||
//         objData.payment_channel == '702' ||
//         objData.payment_channel == '800') {
//         // console.log('VAorderPage', objData.payment_channel)
//         // console.log('VAorderPage===', JSON.stringify(res.data))
//         storeFaspay(res);
//         setTimeout(() => {
//             location.href = OrderTransactionDetailUrl
//         }, 300);
//     }
//     // 其他银行
//     else {
//         location.href = res.data.redirect_url;
//     }
// }

// function storeGopay(res) {
//     generateQRcode = res.data.actions[0].url
//     deeplink_redirect = res.data.actions[1].url
//     store.set('goPay', {
//         generate_qr_code: generateQRcode,
//         deeplink_redirect: deeplink_redirect
//     });
// }

/**
 debug_msg: "success"
 order_id: "14221633053369627081"
 qr_content: "" //二维码的字符串
 qr_url: "" //二维码地址
 request_id: "1596435591600568953"
 store_name: "Peduli Sehat"
 trade_id: "14221633053369627081"
 */
// function storeShopee(res) {
//     store.set('storeShopee', {
//         qr_content: res.data.qr_content,
//         qr_url: res.data.qr_url,
//     });
// }

// function storeSinamas(res) {
//     //sensors
//     // sensors.track('PayOrder', {
//     //     from: document.referrer,
//     //     order_id: res.data.NoRef,
//     //     order_amount: Amount,
//     //     is_succeed: ''
//     // })

//     SourceID = res.data.SourceID
//     IssueDate = res.data.IssueDate
//     NoRef = res.data.NoRef
//     VirtualAccountNumber = res.data.VirtualAccountNumber
//     Amount = res.data.Amount
//     let VAorder = {
//         SourceID: SourceID,
//         IssueDate: IssueDate,
//         NoRef: NoRef,
//         VirtualAccountNumber: VirtualAccountNumber,
//         Amount: Amount,
//         from: 'sinarmas'
//     }
//     sessionStorage.setItem('VA', JSON.stringify(VAorder));
// }

// function storeFaspay(res) {
//     SourceID = res.data.merchant
//     IssueDate = res.data.pay_expired
//     NoRef = res.data.bill_no
//     VirtualAccountNumber = res.data.trx_id
//     Amount = res.data.money
//     let VAorder = {
//         SourceID: SourceID,
//         IssueDate: IssueDate,
//         NoRef: NoRef,
//         VirtualAccountNumber: VirtualAccountNumber,
//         Amount: Amount,
//         from: 'faspay'
//     }
//     sessionStorage.setItem('VA', JSON.stringify(VAorder));
// }

/*
 android: visit login or login
{“
    user_name:"123"
    phone:'123'
}
当俩个字段对应的数值均为空的时候 表示当前用户登录成功;
当俩个字段均不为空的时候 表示其为游客 对应的字段就是游客信息;
req_code = 500 监听app登陆成功或者游客模式下， 返回捐赠页， H5自动跳转到相应支付页面（ gopay / fastpay） req_code = 500
*/
// window.appHandle = function(requestCode, jsonBean) {
//     let objData = store.get('donateMoney');

//     // visit login
//     if (requestCode == 500 && jsonBean.user_name && jsonBean.phone) {
//         utils.showLoading(lang.lang1);

//         objData.user_name = jsonBean.user_name;
//         objData.phone = jsonBean.phone;

//         if (group_id) {
//             visitCreateGroupPayment(objData);

//         } else {

//             visitCreatePayment(objData);
//         }
//     }
//     // login
//     else if (requestCode == 500 && !jsonBean.user_name && !jsonBean.phone) {
//         utils.showLoading(lang.lang1);

//         loginHandleB(objData);
//     } else if (requestCode == 600) {
//         location.reload();
//     }
// }