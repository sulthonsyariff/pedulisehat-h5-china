import utils from 'utils'
import store from 'store'
import 'jq_cookie' //ajax cookie

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let from = reqObj['from'];
let statistics_link = reqObj['statistics_link'];
let project_id = reqObj['project_id'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0;
let client_id = $.cookie('client_id') || '';

/**
 * gopay: pay_platform = 2 ,bank_code = 1
 * sinamas: pay_platform = 3,bank_code = 2
 * faspay: pay_platform = 1, bank_code = *
 * OVO: pay_platform = 4, bank_code = 4
 */
obj.init = function() {
    let bank_code = $(".payment_channel .bank-name").attr('data-bankCode');
    // let support_price = $('#target_amount').val().replace(/\./g, '') || $('.amount-card.chosed span').text().replace(/\./g, '');
    // let privacy_policy = $('.open-icon').hasClass('open') ? 2 : 1
    let support_price = $('#target_amount').attr('data-amount');
    // let support_price = $('#target_amount').val().replace(/\./g, '');

    let params = {
        avatar: '',
        user_name: $('.name-box').val() || '',
        phone: $('#mobile').val() || '',
        money: parseInt(support_price),
        // comment: $("textarea[name=commentArea]").val(), //备注：$('#story').val()不能获取到值？？？
        project_id: project_id,
        // payment_channel: bank_code,
        // payment_channel_name: $(".payment_channel .bank-name").text(),
        // terminal: appVersionCode ? 21 : 10,
        // pay_platform: get_pay_platform(bank_code),
        // privacy_policy: privacy_policy,
        // ab_test: {
        //     'detail_page': get_detail_page() // AB 测试需要
        // },
        // reference: (from == 'OVOXPeduliSehat') ? 'OVO' : '', // OVO合作项目
        // version_code: appVersionCode,
        // version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
        // statistics_link: statistics_link || '',
        // client_id: client_id
    }


    // 联合互助参数不一样
    if ($('.recommendation.add').length) {
        console.log('===jointPayParams===', params);

        return jointPayParams(params);
    } else {
        console.log('===params===', params);

        return params;
    }

}

/**
 *
 * 提交联合支付捐款
qa:14221633053369604515
pre:18256536075545746291
live:12419737548014998025
 */
function jointPayParams(params) {
    let total_money = parseFloat($('#price').attr('data-amount'));
    let item = {};
    let joint_items = [];

    // 获取联合互助产品
    for (let i = 0; i < $('.recommendation.add').length; i++) {
        // let _projectId = $($('.recommendation.add')[i]).attr('data-project-id');
        let _money = $($('.recommendation.add .rec-money')[i]).attr('data-amount');
        let _projectId;

        if (utils.judgeDomain() == 'qa') {
            _projectId = '14221633053369604515';
        } else if (utils.judgeDomain() == 'pre') {
            _projectId = '18256536075545746291';
        } else {
            _projectId = '12419737548014998025';
        }

        item.project_id = _projectId;
        item.money = parseFloat(_money);
        joint_items.push(item)
    }

    params.total_money = total_money;
    params.joint_items = joint_items;

    return params;
}

// function get_detail_page() {
//     if (store.get('test-AB') && store.get('test-AB') == 'a') {
//         return 'original'
//     } else {
//         return 'rotate_play'
//     }
// }

// //1.faspay 2. gopay 3 sinamas 4ovo
// function get_pay_platform(bank_code) {
//     // gopay
//     if (bank_code == '1') {
//         pay_platform = 2;
//     }
//     // sinamas
//     else if (bank_code == '2') {
//         pay_platform = 3;
//     }
//     // ovo
//     else if (bank_code == '4') {
//         pay_platform = 4;
//     }
//     // dana
//     else if (bank_code == '5') {
//         pay_platform = 5;
//     }
//     // faspay
//     else {
//         pay_platform = 1;
//     }

//     return pay_platform;
// }

export default obj;