import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

// let reqObj = utils.getRequestParams();
// let showSP = reqObj['showSP'] || false; //是否展示shopeepay

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;
// console.log(ajaxProxy);

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

/****
 * 项目手续费情况
 */
obj.getProjectPoundage = function(o) {
    let url = domainName.project + '/v1/project_poundage?project_id=' + o.param.project_id + '&category_id=' + o.param.category_id;
    if (isLocal) {
        url = 'mock/v1_project_poundage.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o)
}


/**
 * 获取项目信息
 */
obj.getProjInfo = function(o) {
        let url = domainName.project + '/v1/project/detail?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link;
        if (isLocal) {
            url = 'mock/detail.json'
        }
        ajaxProxy.ajax({
            type: 'get',
            url: url,
            dataType: 'json'
        }, o, 'unauthorizeTodo')
    }
    //获取qurban商品
obj.getQurbanItems = function(o) {
    let url = domainName.project + '/v1/project_category/' + o.param.project_id + '/qurban';
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

// package campaign
obj.getPackageItems = function(o) {
    let url = domainName.project + '/v2/package_list/' + o.param.project_id;
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

//get payment
obj.getPayment = function(o) {
    let url = domainName.trade + '/v2/pay_channel' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    //  +
    // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    if (isLocal) {
        url = 'mock/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//create payment
obj.createPayment = function(o) {
    let url = domainName.trade + '/v1/support';
    if (isLocal) {
        url = 'mock/initiateInfo.json'
    }
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

/**
 * 获取专题信息
 */
obj.getGroupProjInfo = function(o) {
    var url = domainName.project + '/v1/group_project?short_link=' + o.param.short_link;
    if (isLocal) {
        url = 'mock/detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

export default obj;