var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'goPay',
    htmlFileURL: 'html/goPay.html',
    appDir: 'js/goPay',
    uglify: true,
    hash: '',
    mode: 'production'
})