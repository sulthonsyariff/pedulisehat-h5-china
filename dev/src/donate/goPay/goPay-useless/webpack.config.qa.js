var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'goPay',
    htmlFileURL: 'html/goPay.html',
    appDir: 'js/goPay',
    uglify: true,
    hash: '',
    mode: 'development'
})