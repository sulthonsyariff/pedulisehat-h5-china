import view from './view'
import model from './model'
import 'loading'
import '../less/goPay.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let req = utils.getRequestParams();

// 隐藏loading
utils.hideLoading();

view.init({});
