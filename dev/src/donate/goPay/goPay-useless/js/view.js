import mainTpl from '../tpl/goPay.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'
// import sensorsActive from 'sensorsActive'

/* translation */
import goPay from 'goPay'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(goPay);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'] || '';
var group_id = reqObj['group_id'] || '';
var short_link = reqObj['short_link'];
var amount = reqObj['amount'];
var order_id = reqObj['order_id'];
var fromCalculator = reqObj['fromCalculator'];
var category_id = reqObj['category_id'];

obj.init = function(rs) {
    if (fromCalculator == 'zakatCalculator') {
        rs.commonNavGoToWhere = '/zakatCalculator.html';
        // console.log('from', from)
    } else if (project_id) {
        // if (store.get('test-AB')){
        //     rs.commonNavGoToWhere = '/donateMoneyAB.html?project_id=' + project_id + '&short_link=' + short_link;
        // } else {
        // if (category_id == 15) {
        //     rs.commonNavGoToWhere = '/donateMoney2.html?project_id=' + project_id + '&short_link=' + short_link;

        // } else {

        rs.commonNavGoToWhere = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link;
        // }
        // }
        console.log('project_id', project_id, group_id)
    } else if (group_id) {
        rs.commonNavGoToWhere = '/groupDonate.html?short_link=' + short_link;
        console.log('group_id', group_id, project_id)
    }
    rs.lang = lang;
    rs.JumpName = titleLang.goPay;
    if (!utils.browserVersion.androids) {
        commonNav.init(rs);
    }
    rs.domainName = domainName;

    rs.amount = changeMoneyFormat.moneyFormat(parseInt(amount));

    rs.order_id = order_id

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);
    fastclick.attach(document.body);

    utils.hideLoading();


    // goPay
    $('body').on('click', '.goPayBtn', function(rs) {
        $UI.trigger('goPay');
        if (project_id) {
            location.href = '/goPayQRcode.html?project_id=' + project_id + '&short_link=' + short_link + '&order_id=' + order_id + '&amount=' + amount
        } else if (group_id) {
            location.href = '/goPayQRcode.html?group_id=' + group_id + '&short_link=' + short_link + '&order_id=' + order_id + '&amount=' + amount
        }
    });


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensorsActive.init();

};




export default obj;