// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils'
import store from 'store'
// import AppHasInstalled from '../js/AppHasInstalled';
import AppHasInstalled from 'AppHasInstalled'
// import sensorsActive from 'sensorsActive'

/* translation */
import goPayQRcode from 'goPayQRcode'
import qscLang from 'qscLang'
let lang = qscLang.init(goPayQRcode);
/* translation */

let [obj, $UI] = [{}, $('body')];
let project_id = utils.getRequestParams().project_id;
let group_id = utils.getRequestParams().group_id;
let order_id = utils.getRequestParams().order_id;
var short_link = utils.getRequestParams().short_link;

let CACHED_KEY = 'goPay';
let generate_qr_code = store.get(CACHED_KEY).generate_qr_code;
let deeplink_redirect = store.get(CACHED_KEY).deeplink_redirect;
// let deeplink_redirect = 'gojek://gopay/merchanttransfer?tref=1542080408150181126GCFJ&amount=200000&activity=GP:RR';

// judge app is installed or not
AppHasInstalled.AppHasInstalled(deeplink_redirect, lang.lang11);

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    res.JumpName = 'GO-PAY';
    commonNav.init(res);

    res.lang = lang;
    res.getLanguage = utils.getLanguage();
    res.order_id = order_id;
    res.generate_qr_code = generate_qr_code;
    $UI.append(mainTpl(res)); //主模版
    commonFooter.init(res);

    utils.hideLoading();
    fastclick.attach(document.body);


    // back-to-campaign
    // $('body').on('click', '.back-to-campaign', function() {
    //     if (group_id) {
    //         location.href = '/group/' + short_link
    //     } else {
    //         location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id + '&clear_top=true' : '/' + short_link;
    //     }
    // });

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

// obj.judgeHasApp = function() {
//     // judge mobile or PC
//     // if in mobile，check have gojek app or not;
//     if (utils.browserVersion.mobile) {
//         var ifr = document.createElement('iframe');
//         ifr.src = deeplink_redirect;
//         ifr.style.display = 'none';
//         document.body.appendChild(ifr);

//         window.setTimeout(function() {
//             document.body.removeChild(ifr);
//             window.location.href = deeplink_redirect; // open app in mobile
//         }, 2000);
//     }
// }

export default obj;