import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let order_id = utils.getRequestParams().order_id;
let group_id = utils.getRequestParams().group_id;
let short_link = utils.getRequestParams().short_link;
let amount = utils.getRequestParams().amount;
let project_id = utils.getRequestParams().project_id;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

judgeOrderState();

var int = setInterval(() => {
    judgeOrderState();
}, 3000);




view.init({});

/*
 * judge order state
 */
function judgeOrderState() {
    model.getOrderState({
        param: order_id,
        success: function(res) {
            if (res.code == 0) {
                if (res.data && res.data.status == 'success') {
                    //sensors
                    sensors.track('PayOrder', {
                        from: document.referrer,
                        order_id: res.data.order_id,
                        order_amount: amount,
                        is_succeed: true
                    })

                    // 安卓项目
                    if (appVersionCode > 128 && project_id) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' +
                            encodeURIComponent(project_id) +
                            '&order_id=' + encodeURIComponent(order_id);

                    }
                    // 安卓专题
                    else if (appVersionCode >= 202 && group_id && short_link) {
                        location.href = 'qsc://app.pedulisehat/go/donate_success?order_id=' +
                            encodeURIComponent(order_id) +
                            '&short_link=' + encodeURIComponent(short_link);

                    }
                    // H5项目+专题
                    else {
                        location.href = '/commonDonateSuccess.html?order_id=' + order_id +
                            '&short_link=' + short_link +
                            // '&terminal=' + res.data.terminal +
                            (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                            (group_id ? ('&group_id=' + group_id) :
                                ('&project_id=' + project_id))
                    }

                    // if (appVersionCode > 128) {
                    //     location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' + res.data.project_id + '&order_id=' + res.data.order_id;
                    // } else {
                    //     if (group_id) {
                    //         // location.href = res.data.redirect_url + '?order_id=' + res.data.order_id + '&group_id=' + group_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                    //         location.href = '/commonDonateSuccess.html?order_id=' + res.data.order_id + '&group_id=' + group_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                    //     } else {
                    //         // location.href = res.data.redirect_url + '?order_id=' + res.data.order_id + '&project_id=' + res.data.project_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                    //         location.href = '/commonDonateSuccess.html?order_id=' + res.data.order_id + '&project_id=' + res.data.project_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                    //     }
                    // }

                } else if (res.data && res.data.status == 'failed') {
                    sensors.track('PayOrder', {
                        from: document.referrer,
                        order_id: res.data.order_id,
                        order_amount: amount,
                        is_succeed: false
                    })
                    if (group_id) {
                        // location.href = res.data.redirect_url + '?order_id=' + res.data.order_id + '&group_id=' + group_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                        location.href = '/commonDonateFailure.html?order_id=' + res.data.order_id + '&group_id=' + group_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                    } else {
                        // location.href = res.data.redirect_url + '?order_id=' + res.data.order_id + '&project_id=' + res.data.project_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                        location.href = '/commonDonateFailure.html?order_id=' + res.data.order_id + '&project_id=' + res.data.project_id + '&short_link=' + short_link + '&terminal=' + res.data.terminal;
                    }
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}