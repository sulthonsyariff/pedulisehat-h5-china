var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'goPayQRcode',
    htmlFileURL: 'html/goPayQRcode.html',
    appDir: 'js/goPayQRcode',
    uglify: true,
    hash: '',
    mode: 'development'
})