var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'goPayQRcode',
    htmlFileURL: 'html/goPayQRcode.html',
    appDir: 'js/goPayQRcode',
    uglify: true,
    hash: '',
    mode: 'production'
})