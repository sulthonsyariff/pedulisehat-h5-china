// 公共库
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import domainName from 'domainName' // port domain name
import store from 'store'
import changeMoneyFormat from 'changeMoneyFormat'
import swiperTpl from '../tpl/swiper.juicer' //swiper
import swiperInit from './_swiperInit' //轮播图初始化
import _littleBannerTpl from '../tpl/_littleBannerTpl.juicer'

import utils from 'utils';
// import levelTpl from '../tpl/level.juicer'
import alertBoxTpl from '../tpl/_alertBoxTpl.juicer'
// import sensorsActive from 'sensorsActive'
import clickHandle from './_clickHandle'
import campaignListComponents from 'campaignListComponents'
import urgentListComponents from 'urgentListComponents'
import navBottom from 'navBottom'
import saveH5Tips from 'saveH5Tips'
import promoPopup from '../../../common/promoPopup/js/main'

/* translation */
import home from 'home'
import qscLang from 'qscLang'
let lang = qscLang.init(home);
/* translation */

let [obj, $UI] = [{}, $('body')];
let CACHED_KEY = 'switchLanguage';
let user_points;
let BANNER_TOAST = 'banner_toast';
let banner_toast = sessionStorage.getItem(BANNER_TOAST) ? sessionStorage.getItem(BANNER_TOAST) : {};
let reqObj = utils.getRequestParams();
// let isTest = reqObj["isTest"] || '';


// utils.browserVersion();



obj.UI = $UI;
// 初始化
obj.init = function(res) {
    // res.isTest = isTest;

    // res.JumpName = 'page';
    commonNav.init(res);

    res.lang = lang
    res.domainName = domainName;
    // res.data.total_money = changeMoneyFormat.moneyFormat(res.data.total_money);
    // insertData(res);
    $UI.trigger('insertBanner');

    $UI.append(mainTpl(res)); //主模版

    saveH5Tips.init(); //save h5 to screen

    res.fromWhichPage = 'index.html';
    navBottom.init(res, 1); //nav bottom bar

    urgentListComponents.init({
        fromWhichPage: 'index.html'
    }); //Urgent Campaigns

    // commonFooter.init(res);
    fastclick.attach(document.body);
    clickHandle.init(res);
    // sensorsActive.init();

    promoPopup.init()
    utils.hideLoading();

    // 设置avatar 头像
    if (res && res.data && res.data.avatar) {
        if (res.data.avatar.indexOf('http') == 0) {
            $('.avatar-img').attr('src', utils.imageChoose(res.data.avatar));
        } else {
            $('.avatar-img').attr('src', domainName.static + '/img/avatar/' + res.data.avatar);
        }
    }
    // name overflow
    $('.avatar-name .name').css('max-width', ($('.self-center-bg').width() - 85) * 0.95);

    console.log("BGKI", res.description);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

};

obj.insertBanner = function(rs) {
    for (let j = 0; j < rs.data.length; j++) {
        rs.data[j].images = JSON.parse(rs.data[j].image)
        if (banner_toast != 'false') {
            if (rs.data[j].type == 1) {
                // console.log('type---', j, rs.data[j])
                $('html,body').on("touchmove", function(e) {
                    e.preventDefault();
                })
                $UI.append(alertBoxTpl(rs.data[j]));
            }
        }
    }
    $('.insert-swiper').append(swiperTpl(rs)); //swiper

    // console.log(`=== ${$('.insert-swiper').innerWidth()}, ${$('.insert-swiper').width()} ===`);

    $('.swiper-container').height($('.insert-swiper').width() / 2); // 图片高度不一样,避免分页按钮’置顶‘
    swiperInit.init(rs); //轮播图初始化
}

obj.insertBanner2 = function(rs) {
    rs.data[0].images = JSON.parse(rs.data[0].image)
    $('.little-banner').append(_littleBannerTpl(rs)); // 小banner

    // console.log('=-=-=-=-=-=-=-=-=-=', $('.img').width(), $('.img').width() / 3.56);

    // $('.img').height($('.img').width() / 3.56)
}

obj.insertData = function(rs) {
    if (rs.data.length != 0) {
        rs.fromWhichPage = 'index.html'
        campaignListComponents.init(rs); //list
    }
}

obj.setHowToStartCampaign = function(res){
    $('.tips-wrapper').html(res.description);
}

obj.raiseDay = function(created_at) {
    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}

export default obj;