import utils from 'utils';
import actionPromoPopup from '../../../common/promoPopup/js/_clickHandle'

let [obj, $UI] = [{}, $('body')];
let BANNER_TOAST = 'banner_toast';

obj.init = function(rs) {
    // view all campaigns
    $('body').on('click', '.view-all-btn', function() {
        actionPromoPopup.click()
        location.href = '/campaignList.html?composite=1'
    })

    // start campaign
    $('body').on('click', '.start-btn-wrapper', function(e) {
        actionPromoPopup.click()
        $UI.trigger('initiate');
    });

    // $('body').on('click', '.download-icon', function() {
    //     location.href = 'market://details?id=com.qschou.pedulisehat.android'
    // })

    // sensors:WhatsApp Contact
    // $('body').on('click', '.consult-btn-wrapper', function(e) {
    //     sensors.track('ContactCS', {
    //         contact_type: 'whatsApp',
    //     }, location.href = 'https://api.whatsapp.com/send?phone=6281230009479&text=Halo%20,%20Saya%20mau%20konsultasi%20donasi%20di%20pedulisehat.id')
    // });

    // 弹窗： close toast
    $('body').on('click', '.close-pop,.mask', function() {
        // sensors.track('SearchBarClick')
        sessionStorage.setItem(BANNER_TOAST, 'false')
        $('.homeToast').css('display', 'none')
    })

    // Donation list
    $('body').on('click', '#donat-button', function(e) {
        actionPromoPopup.click()
        location.href = '/donateList.html?composite=1'
    });

    // zakat list
    $('body').on('click', '#zakat-button', function(e) {
        actionPromoPopup.click()
        location.href = '/zakatList.html?composite=1'
    });

    // Gotongroyong
    $('body').on('click', '#Gotongroyong-button', function(e) {
        actionPromoPopup.click()
        $UI.trigger('routeToGtryClosed');
        // if (utils.judgeDomain() == 'qa') {
        //     location.href = 'https://gtry-qa.pedulisehat.id'
        // } else if (utils.judgeDomain() == 'pre') {
        //     location.href = 'https://gtry-pre.pedulisehat.id'
        // } else {
        //     location.href = 'https://gtry.pedulisehat.id'
        // }
    });

    // Data Covid-19
    $('body').on('click', '#Covid-19-button', function(e) {
        actionPromoPopup.click()
        location.href = '/datacovid19.html'
    });

    // sijicorona
    $('body').on('click', '#sijicorona', function(e) {
        actionPromoPopup.click()
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://asuransi-qa.pedulisehat.id'
        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://asuransi-pre.pedulisehat.id'
        } else {
            location.href = 'https://asuransi.pedulisehat.id'
        }
    });

    // snsors :homeBanner-bg
    $('body').on('click', '.homeBanner-bg', function() {
        location.href = $(this).parent().attr('data-href')
    })

    // sensors: search
    // $('body').on('click', '.right', function() {
    //     sensors.track('SearchBarClick')
    // })

    // about us
    $('body').on('click', '.detail-btn', function() {
        actionPromoPopup.click()
        location.href = '/aboutUs.html'
    })

    $('body').on('click', '.urgent-campaigns_list', function() {
        actionPromoPopup.click()
    })

    // $('body').on('click', '.gtry-entrance', function(e) {
    //     if (utils.judgeDomain() == 'qa') {
    //         location.href = 'https://gtry-qa.pedulisehat.id'

    //     } else if (utils.judgeDomain() == 'pre') {
    //         location.href = 'https://gtry-pre.pedulisehat.id'

    //     } else {
    //         location.href = 'https://gtry.pedulisehat.id'

    //     }
    // });

    // $('body').on('click', '.btn-top', function() {
    //     location.href = '/'
    // })

    // $('body').on('click', '.empty-icon', function () {
    //     // store.remove('search');
    //     $UI.trigger('deleteSearchHistory')
    // })

    // judge which language is right now
    // let language = ['en', 'id'];
    // for (let i = 0; i < language.length; i++) {
    //     console.log('===',language)
    //     if (store.get(CACHED_KEY) && store.get(CACHED_KEY).lang == language[i]) {
    //         console.log('==')
    //         // $('.' + language[i]).addClass('choosed');
    //     } else if (!store.get(CACHED_KEY)) {
    //         $('.id').addClass('choosed');
    //     }
    // }

    // indonesia / english
    // $('body').on('click', '.language-switch', function () {
    //     if (store.get(CACHED_KEY) && store.get(CACHED_KEY).lang == 'en') {
    //         console.log('CACHED_KEY_id', store.get(CACHED_KEY))
    //         store.set(CACHED_KEY, {
    //             lang: 'id'
    //         });
    //     } else {
    //         console.log('CACHED_KEY_en', store.get(CACHED_KEY))
    //         store.set(CACHED_KEY, {
    //             lang: 'en'
    //         });
    //     }

    //     setTimeout(() => {
    //         location.reload();
    //     }, 100);
    // });
}

export default obj;