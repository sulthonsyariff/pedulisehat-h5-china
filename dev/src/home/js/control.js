import view from './view'
import model from './model'
import model_getListData from 'model_getListData' //列表接口调用地址
import 'loading'
import '../less/main.less'
import 'freshchat.widget'
import store from 'store'
import search from 'search'

// 引入依赖
import utils from 'utils'
let UI = view.UI;

let initiateJumpUrl;

// 隐藏loading
utils.showLoading();

function setTokenFcm() {
    let tokenFcm = localStorage.getItem("tokenFcm") || null;
    let tokenFcmExisting = localStorage.getItem("tokenFcmExisting") || null;

    // jika ada token fcm
    if (tokenFcm) {
        // jika token fcm tidak sama dengan yang exisiting atau yang sudah dikirim
        if (tokenFcm != tokenFcmExisting) {
            console.log("panggil set token fcm");
            model.setTokenFcm({
                param: {
                    token: tokenFcm
                },
                success: function(rs) {
                    if (rs.code == 0) {
                        console.log("berhasil set token", rs);
                        localStorage.setItem("tokenFcmExisting", tokenFcm);
                    } else {
                        utils.alertMessage(rs.msg)
                    }
                },
                error: utils.handleFail,
            });
        }
    }
}

UI.on('routeToGtryClosed', function() {
    model.getUserInfo({
        success: function(rs) {
            if (rs.code == 0) {
                if (utils.judgeDomain() == 'qa') {
                    location.href = 'https://gtry-qa.pedulisehat.id/gtry/closed/refund'
                } else if (utils.judgeDomain() == 'pre') {
                    location.href = 'https://gtry-pre.pedulisehat.id/gtry/closed/refund'
                } else {
                    location.href = 'https://gtry.pedulisehat.id/gtry/closed/refund'
                }
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        unauthorizeTodo: function(rs) {
            // 加载主模版
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id/gtry/closed/'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id/gtry/closed/'
            } else {
                location.href = 'https://gtry.pedulisehat.id/gtry/closed/'
            }
            view.init({});
        },
        error: utils.handleFail,
    });
});

UI.on('insertBanner', function() {
    // 0普通banner 1首页弹窗 2中间的banner
    model.getBanner({
        param: {
            type: '0,1'
        },
        success: function(rs) {
            if (rs.code == 0) {
                view.insertBanner(rs);
                utils.hideLoading();
            }
        },
        error: utils.handleFail
    })

    // 小banner
    model.getBanner({
        param: {
            type: '2'
        },
        success: function(rs) {
            if (rs.code == 0) {
                // state	Y	int	0 下线 1上线
                if (rs.data && rs.data[0].state == 1) {
                    view.insertBanner2(rs);
                }
                utils.hideLoading();
            }
        },
        error: utils.handleFail
    })
})

//删除搜索历史记录
UI.on('deleteSearchHistory', function() {
    model.deleteSearcnHistory({
        success: function(data) {
            if (data.code == 0) {
                //     view.insertData(data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
})


function getListData() {
    model_getListData.getListData_home({
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                utils.hideLoading();
                view.insertData(res);
            } else {
                utils.alertMessage(res.msg)
            }
            // console.log('insertData', res);
        },
        error: utils.handleFail
    })
}

function getHowToStartCampaign() {
    model.getHowToStartCampaign({
        success: function(res) {
            utils.hideLoading();
            view.setHowToStartCampaign(res);
        },
    })
}

model.getUserInfo({
    success: function(rs) {
        if (rs.code == 0) {
            // 加载主模版
            getListData();
            getHowToStartCampaign()
            view.init(rs);
            // set token fcm notification
            setTokenFcm();

            // zakat项目发起
            if (rs.data.user_id == '12419737548001456892' || rs.data.user_id == '12419737548005254075') {
                initiateJumpUrl = '/initiateChoose.html'
            } else {
                initiateJumpUrl = '/initiatePage.html'
            }

        } else {
            utils.alertMessage(rs.msg)
        }
    },
    unauthorizeTodo: function(rs) {
        // 加载主模版
        getListData();
        getHowToStartCampaign()
        view.init({});

        initiateJumpUrl = '/login.html?qf_redirect=' + encodeURIComponent('/');
        // 发起回跳地址不能写发起页地址，避免返回bug，故做缓存处理
        // 登陆成功后在缓存中找homepage的jump_qf_redirect，如果存在则跳到jump_qf_redirect
        // store.set('homepage', {
        //     jump_qf_redirect: encodeURIComponent('/initiatePage.html')
        // });

    },
    error: utils.handleFail,
});



// initiate点击跳转事件
UI.on('initiate', function(e) {
    location.href = initiateJumpUrl;
});

// 获取搜索历史记录
model.getSearchHistory({
    success: function(rs) {
        if (rs.code == 0) {
            getPopularWord(rs);
            // console.log('insertData', data);
        } else {
            utils.alertMessage(data.msg)
        }
    },
    unauthorizeTodo: function(rs) {},
    error: utils.handleFail,

})

//获取推荐词
function getPopularWord(rs) {
    model.getPopularWord({
        success: function(res) {
            if (res.code == 0) {
                res.searchHistory = rs.data
                res.from = 'campaignList'
                search.init(res);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
}

// Simobi app要内嵌我们的H5，http://pedulisehat.id?from=sinarmas 给他们这个链接，他们要求只能有金光VA这一种支付方式
if (location.href.indexOf('?from=sinarmas') != -1) {
    console.log('===from sinarmas===');

    //存localstorage
    $.cookie('from', 'sinarmas', {
        expires: 365,
        path: '/',
        domain: 'pedulisehat.id'
    });
}