/**
 * 轮播图初始化
 */
import Swiper from 'swiper'
// import alertBoxTpl from '../tpl/_alertBoxTpl.juicer'

let obj = {};
// let [obj, $UI] = [{}, $('body')];
obj.init = function(rs) {

    // for (let i = 0; i < rs.data.length; i++) {
    //     if (rs.data[i].type == 1) {
    //         console.log('type---',i,rs.data[i])
    //         $UI.append(alertBoxTpl(rs.data[i]));
    //     }
    // }


    /*if there is more than one banner,then use the swiper */
    if (rs.data.length > '1') {

        let swiper = new Swiper('.swiper-container', {
            // paginationClickable: true,
            autoplay: {
                // delay: 3000,
                stopOnLastSlide: false,
                // disableOnInteraction: true,
            },
            speed: 500,
            loop: true,
            observer: true,
            autoplayDisableOnInteraction: false,
            observeParents: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
            },
        });

    }
}

export default obj;