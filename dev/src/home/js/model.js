import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.setTokenFcm = function(o) {
    let url = domainName.mns + '/v1/fcm_token';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/userInfo.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

obj.getBanner = function(o) {
    let url = domainName.project + '/v1/banner?UIVersion=Campaigner&type=' + o.param.type;

    if (isLocal) {
        url = '/mock/banner.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/****
state　
2 隐藏
 8 失败
 16 冻结
 512 成功
 8192 筹资中

 结束： 512 / 8 筹款成功或者失败
 */
// obj.getListData = function(o) {
//     let url = domainName.project + '/v1/public?page=' + 1 + '&limit=3' + '&category_id=12,13,14&state=8192';
//     if (isLocal) {
//         url = '/mock/project_lists_' + 1 + '.json';
//     }

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url
//     }, o)
// };

// those 3 below apis is about search function
obj.getSearchHistory = function(o) {
    let url = domainName.project + '/v1/project_search_history';
    // if (isLocal) {
    //     url = '/mock/project_lists_' + o.param.page + '.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};
obj.getPopularWord = function(o) {
    let url = domainName.project + '/v1/search_popular_word';
    // if (isLocal) {
    //     url = '/mock/project_lists_' + o.param.page + '.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

obj.deleteSearcnHistory = function(o) {
    let url = domainName.project + '/v1/project_search_history';
    // if (isLocal) {
    //     url = '/mock/project_lists_' + o.param.page + '.json';
    // }

    ajaxProxy.ajax({
        type: 'delete',
        url: url
    }, o)
};

obj.getHowToStartCampaign = function(o) {
    ajaxProxy.ajaxFreshdesk({
        id: 43000600763
    },
    callback);

    function callback(res) {
        o.success(res)
    }
};

export default obj;