import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

// console.log(ajaxProxy);

obj.verify = function(o) {
    var url = domainName.passport + '/v1/login/verify';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;