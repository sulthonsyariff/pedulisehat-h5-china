import mainTpl from '../tpl/initiateSuccessPage.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import initiateSuccessPage from 'initiateSuccessPage'
import qscLang from 'qscLang'
let lang = qscLang.init(initiateSuccessPage);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

obj.init = function(rs) {
    rs.JumpName = '';
    commonNav.init(rs);

    rs.domainName = domainName;
    rs.lang = lang;
    $UI.append(mainTpl(rs));
    commonFooter.init(rs);

    fastclick.attach(document.body);

    $('.viewBtn').on('click', function(e) {
        location.href = '/' + utils.getRequestParams().short_link;
    });
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // console.log('view:', rs);
    // sensorsActive.init();

};




export default obj;