var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'initiateSuccessPage',
    htmlFileURL: 'html/initiateSuccessPage.html',
    appDir: 'js/initiateSuccessPage',
    uglify: true,
    hash: '',
    mode: 'production'
})