var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'initiateSuccessPage',
    htmlFileURL: 'html/initiateSuccessPage.html',
    appDir: 'js/initiateSuccessPage',
    uglify: true,
    hash: '',
    mode: 'development'
})