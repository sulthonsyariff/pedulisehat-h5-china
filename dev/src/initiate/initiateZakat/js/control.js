import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;

// 隐藏loading
utils.hideLoading();

/*
 * judge是否登陆：登陆才能发布项目
 */
model.getUserInfo({
    success: function(rs) {
        // console.log(rs);
        if (rs.code == 0) {
            if (rs.data.mobile != '') {
                view.init({});
            } else {
                //绑定手机号
                location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href);
            }
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    // unauthorizeTodo: function(rs) {
    //     console.log('unauthorizeTodo')
    //         //手机号登陆或注册
    //     location.href = '/phoneBind.html?qf_redirect=' + encodeURIComponent(location.href);
    // },
    error: utils.handleFail
});

/*
 *submit
 */
UI.on('submit', function(e, params) { // send to server directly
    model.submit({
        data: params,
        success: submitSuccessHandler,
        error: utils.handleFail
    });

})

function submitSuccessHandler(res) {
    if (res.code == 0) {
        view.removeCache(); //remove cache
        location.href = '/initiateSuccessPage.html?short_link=' + res.data.short_link
    } else {
        utils.alertMessage(res.msg)
    }
}