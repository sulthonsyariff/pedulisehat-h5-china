import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

console.log(ajaxProxy);

obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/*
*{
    "title": "We Will Beat United Airline",
    "story": "The protection of Giant Panda Bears CANNOT Wait!!!",
    "total_amount": 958300,
    "country": "id",
    "city": "yjd",
    "category_id": 12,
    "platform": "android",
    "images": [{
        "name": "13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
        "image": "https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
        "thumb": "https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
        "location": "cloudinary"
    }]
}
*/
obj.submit = function(params) {
    var url = domainName.project + '/v2/project/create';

    if (isLocal) {
        url = '/mock/patientVerification.json';
    }

    ajaxProxy.ajax({
        url: url,
        data: JSON.stringify(params.data),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8'
    }, params);
}

export default obj;