// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import imgUploader from 'uploadCloudinaryMore'
import utils from 'utils'
import fastclick from 'fastclick'
import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
// import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

import validate from './_validate' // validate the form

/* translation */
import initiatePage from 'initiatePage2'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(initiatePage);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
/* storage key */
let CACHED_KEY = 'initiate_info';

/* upload img */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : {};
/* upload img */

obj.init = function(rs) {
    // console.log('view:', rs);
    rs.commonNavGoToWhere = '/';
    rs.JumpName = titleLang.quickfund;
    commonNav.init(rs);
    $('title').html(titleLang.quickfund);

    datas.lang = lang;
    $UI.append(mainTpl(datas || '')); // get data from the local cache

    // 从本地缓存中读取图片
    if (datas && datas.images) {
        imgUploader.setImageList(uploadList, uploadKey, datas.images);
    }

    $('.fancybox').fancybox();
    fastclick.attach(document.body);

    setupUIHandler(); //Main Js

    // changeMoneyFormat.init('#total_amount');
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });


    // hideLinkToast();
    getTitleParams();
    getAssignParams();
    getTextareaParams();
};

//assign link fade
function senddata() {
    $('.link-fade').css('display', 'inline-block')
    var link = $('#assign-link')[0].value;
    $('.link-toast')[0].innerText = 'share.pedulisehat.id/' + link;
}

function hideLinkToast() {
    $("input").blur(function() {
        $(".link-fade").css("display", "none");
    });
}

/**
 * remove the cached project information
 */
obj.removeCache = function() {
    store.remove(CACHED_KEY);
}

/**
 * //Update local cache
 */
obj.updateCache = function() {
    updateCache();
}

/**
 *Main Js
 */

function setupUIHandler() {
    // judge
    // if (datas && datas.total_amount) {
    //     $('#total_amount').val(changeMoneyFormat.moneyFormat(datas.total_amount));
    // }

    // initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
    hideOrShowInputButton.init(); //show or hide upload btn

    //set cover
    picCover.setCover(lang);

    // 输入框监听，本地存储数据
    $('input, select, textarea').on('input selected', function(e) {
        updateCache();
    });

    // 获取输入框字数
    // $('textarea[name=story]').keyup(function() {
    //     getTextareaParams();
    // });
    // 监听输入框内容， 获取输入框字数
    $(document).on('input propertychange', 'textarea[name=story]', function(e) {
        getTextareaParams();
    });
    $(document).on('input propertychange', '#assign-link', function(e) {
        getAssignParams();
        senddata();
        hideLinkToast();
    });
    $(document).on('input propertychange', '#title', function(e) {
        getTitleParams();
    });

    // submit
    $('.submitBtn').on('click', function() {
        let submitData = getSubmitParam();

        if (validate.check(submitData, lang)) {
            // $('.submitBtn').addClass('disabled');
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang26);
        }
    });

}

/**
 * 获取提交数据
{
     "title": "We Will Beat United Airline",
     "story": "The protection of Giant Panda Bears CANNOT Wait!!!",
     "total_amount": 958300,
     "country": "",
     "city": "yjd",
     "category_id": 12,
     "platform": "android",
     "images": [{
         "name": "13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "image": "https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "thumb": "https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "location": "cloudinary"
     }]
 }
 */
function getSubmitParam() {
    return {
        title: $('#title').val(),
        story: $("textarea[name=story]").val(), //备注：$('#story').val()不能获取到值？？？
        // total_amount: parseInt($('#total_amount').val().replace(/\./g, '')) || '',
        country: '1',
        city: $('#city').val(),
        category_id: 13, //zakat项目
        platform: 'h5',
        images: imgUploader.getImageList(uploadList, uploadKey),
        invite_code: $('#invitation-code').val(),
        short_link: $('#assign-link').val()
    }
}

/**
 * //Update local cache
 */
function updateCache() {
    // console.log('updateCache:', store.get(CACHED_KEY));
    store.set(CACHED_KEY, getSubmitParam());
}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    hideOrShowInputButton.init(); //show or hide upload btn

    // console.log('initiate info img changed');
    imgUploader.refreshPictureNumber(uploadList);
    updateCache();
    $('.fancybox').fancybox();

    //set cover
    picCover.setCover(lang);
}

// 输入框字数提示
function getTextareaParams() {
    var num;
    num = $("textarea[name=story]").val() ? $("textarea[name=story]").val().length : '0';
    // num = $('.input_story').val().length;

    // console.log('num-125', num)
    if (num <= 5000) {
        num = num;
    } else {
        $(num).substr(0, 5000)
        utils.alertMessage(lang.lang1);
    }
    $(".contentcount").html(num)
}
// 输入框字数提示
function getAssignParams() {
    var num;
    num = $("#assign-link").val() ? $("#assign-link").val().length : '0';
    // num = $('.input_story').val().length;

    // console.log('num-125', num)
    if (num <= 100) {
        num = num;
    } else {
        $(num).substr(0, 100)
        utils.alertMessage(lang.lang1);
    }
    $(".assign-contentcount").html(num)
}
// 输入框字数提示
function getTitleParams() {
    var num;
    num = $("#title").val() ? $("#title").val().length : '0';
    // num = $('.input_story').val().length;

    // console.log('num-125', num)
    if (num <= 100) {
        num = num;
    } else {
        $(num).substr(0, 100)
        utils.alertMessage(lang.lang1);
    }
    $(".title-contentcount").html(num)
}


export default obj;