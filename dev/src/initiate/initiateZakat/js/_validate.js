/*
 * validate form
 */
import utils from 'utils'
import {
    type
} from 'os';


let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    // if (param.total_amount == '') {
    //     utils.alertMessage(lang.lang2);
    //     return false;
    // }

    // if (param.total_amount % 10000 != 0) {
    //     // console.log('total_amount%10000',param.total_amount%10000)
    //     utils.alertMessage(lang.lang28);
    //     return false;
    // }

    if (param.title == '') {
        utils.alertMessage(lang.lang3);
        return false;
    }
    if (param.title.length > 200) {
        utils.alertMessage(lang.lang4);
        return false;
    }
    if (param.city == '') {
        utils.alertMessage(lang.lang5);
        return false;
    }
    if (param.story == '') {
        utils.alertMessage(lang.lang6);
        return false;
    }

    let str = $.trim(param.story.replace(/<[^>]+>/g, "")); //去掉所有的html标记 计算长度
    // console.log('str', str);

    if (str.replace(/\&nbsp;/g, "").trim() == '' || str.length < 2) {
        utils.alertMessage(lang.lang7);
        return false;
    }

    if (param.images.length < 1) {
        utils.alertMessage(lang.lang8);

        return false;
    }

    if (param.images.length > 8) {
        utils.alertMessage(lang.lang9);

        return false;
    }

    //finish upload or not
    var imgs = param.images;
    for (var i = 0; i < imgs.length; i++) {
        if (!imgs[i].thumb || !imgs[i].image) {
            utils.alertMessage(lang.lang10);

            return false;
        }
    }

    // let assignReg = /[a-zA-Z0-9][\w-_]{1,100}$/g
    var temp = param.short_link.match(/[\w-_]{1,100}$/g)

    // console.log('match3===',param.short_link == temp[0])

    if (temp == undefined || temp[0] != param.short_link) {
        utils.alertMessage(lang.lang34);
        return false;
    }

    return true;
}

export default obj;