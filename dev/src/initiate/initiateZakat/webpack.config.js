var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'initiateZakat',
    htmlFileURL: 'html/initiateZakat.html',
    appDir: 'js/initiateZakat',
    uglify: true,
    hash: '',
    mode: 'production'
})