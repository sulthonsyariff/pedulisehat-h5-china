var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'initiateZakat',
    htmlFileURL: 'html/initiateZakat.html',
    appDir: 'js/initiateZakat',
    uglify: true,
    hash: '',
    mode: 'development'
})