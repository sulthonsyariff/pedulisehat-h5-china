import tpl from "../tpl/_alertCovid19.juicer";

let [obj, $UI] = [{}, $("body")];

obj.init = function(lang) {
    $UI.append(tpl(lang));

    // here1
    $UI.on('click', '.checkRules', function() {
        window.open("https://blog.pedulisehat.id/2020/03/27/peraturan-penggalangan-dana-kampanye-covid-19/");
    })

    // here2
    $UI.on('click', '.form', function() {
        window.open("https://blog.pedulisehat.id/2020/05/27/kegiatan-sosial-bersama-pedulisehat-id/");

        setTimeout(() => {
            $('.alert-box._alertCovid19').css('display', 'none');
        }, 1000);
    })

    // yes
    $UI.on('click', '#yes', function() {
        window.open("https://pedulisehat.typeform.com/to/fzVm8A");

        setTimeout(() => {
            $('.alert-box._alertCovid19').css('display', 'none');
        }, 1000);
    })

    // no
    $UI.on('click', '#no', function() {
        $('.alert-box._alertCovid19').css('display', 'none');
    })
}

export default obj;