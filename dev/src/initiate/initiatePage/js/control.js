import view from "./view";
import model from "./model";
import "loading";
import "../less/main.less";
// 引入依赖
import utils from "utils";

let UI = view.UI;

// 隐藏loading
utils.hideLoading();

/*
 * judge是否登陆：登陆才能发布项目
 */
model.getUserInfo({
    success: function(rs) {
        // console.log(rs);
        if (rs.code == 0) {
            if (rs.data.mobile != "") {
                // 获取时间限制
                getTimeLimitData();
            } else {
                //绑定手机号
                location.href =
                    "/login.html?qf_redirect=" + encodeURIComponent(location.href);
            }
        } else {
            utils.alertMessage(rs.msg);
        }
    },
    error: utils.handleFail
});

// 获取时间限制
function getTimeLimitData(e) {
    model.projectLimit({
        data: 1, //1 项目限制天数 2 延期天数
        success: function(res) {
            if (res.code == 0) {
                console.log("===projectLimit===", res);
                res.arr = res.data;
                view.init(res);
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}

/*
 *submit
 */
UI.on("submit", function(e, params) {
    // send to server directly
    model.submit({
        data: params,
        success: submitSuccessHandler,
        error: utils.handleFail
    });
});

// 更新时间时刷新存储数据
UI.on("choose_time", function(e, time) {
    console.log("===choose_time====");
    view.updateCache();
});

UI.on("tag_search", function(e, params) {
    // send to server directly
    model.getTag({
        query : params,
        limit : 5,
        success: function(rs) {
            // console.log(rs);
            if (rs.code == 0) {
                view.tagSearch(rs.data)
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});

function submitSuccessHandler(res) {
    if (res.code == 0) {
        view.removeCache(); //remove cache
        location.href =
            "/initiateSuccessPage.html?short_link=" + res.data.short_link;
    } else {
        utils.alertMessage(res.msg);
    }
}
