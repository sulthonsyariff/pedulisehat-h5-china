// 公共库
import commonNav from "commonNav";
// import commonFooter from "commonFooter";
import mainTpl from "../tpl/main.juicer";

import imgUploader from "uploadCloudinaryMore";

import utils from "utils";
import fastclick from "fastclick";
// import picCover from "./_picCover";
import store from "store";
import "fancybox";
import hideOrShowInputButton from "./_hideOrShowInputButton"; //Control image upload button to show or hide
import changeMoneyFormat from "changeMoneyFormat"; //divide the amount by decimal point
import googleAnalytics from "google.analytics";
// //import sensorsActive from "sensorsActive";

import validate from "./_validate"; // validate the form
import storyTpl from "../tpl/_storyTpl.juicer"; //story template
import storyTpl2 from "../tpl/_storyTpl2.juicer"; //story template

import commonTimeLimit from "commonTimeLimit";
import _alertCovid19 from "./_alertCovid19";
import navBottom from 'navBottom'

/* translation */
import initiatePage from "initiatePage";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(initiatePage);
/* translation */

let [obj, $UI] = [{}, $("body")];
obj.UI = $UI;
/* storage key */
let CACHED_KEY = "initiate_info";
let getLanguage = utils.getLanguage();

let random = Math.random().toFixed(1); //0-1之间随机数,保留一位小数
let reqObj = utils.getRequestParams();
let fromGrab = reqObj["fromGrab"] ? reqObj["fromGrab"] : "";
// console.log("fromGrab", fromGrab);

let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : {};

/* upload img */
let uploadWrapper = "#uploadWrapper";
let uploadBtn = "upload-btn"; // upload btn
let uploadList = ".uploadList"; // img container
let uploadKey = "cover"; // upload key
let uploadNumLimit = 8; // limit number
/* upload img */


/* upload cover */
let uploadWrapper2 = '#uploadWrapper2';
let uploadBtn2 = 'upload-btn2'; // upload btn
let uploadList2 = '.uploadList2'; // img container
let uploadKey2 = 'cover2'; // upload key
let uploadNumLimit2 = 1; // limit number
/* upload cover */

let tagIds = [];
let tagNames = [];

obj.init = function(rs) {
    rs.commonNavGoToWhere = "#";
    rs.JumpName = titleLang.quickfund;
    commonNav.init(rs);

    $("title").html(titleLang.quickfund);

    datas.arr = rs.arr;
    datas.lang = lang;
    datas.getLanguage = getLanguage;

    _alertCovid19.init(lang);
    $UI.append(mainTpl(datas || "")); // get data from the local cache
    // commonFooter.init(rs);

    rs.fromWhichPage = 'initiatePage.html';
    navBottom.init(rs, 3); //nav bottom bar

    if (random > 0.5) {
        $UI.append(storyTpl(datas)); // story template
    } else {
        $UI.append(storyTpl2(datas)); // story template
    }

    commonTimeLimit.init(rs); //limit time choose

    // 从本地缓存中读取图片
    if (datas && datas.images) {
        imgUploader.setImageList(uploadList, uploadKey, datas.images);
    }

    if (datas && datas.cover) {
        imgUploader.setImageList(uploadList2, uploadKey2, datas.cover); //cover
    }

    $(".fancybox").fancybox();
    fastclick.attach(document.body);

    setupUIHandler(); //Main Js

    changeMoneyFormat.init("#total_amount");

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // // sensorsActive.init();

    // quill editor
    var toolbarOptions = [
        ['bold', 'italic', 'underline']
      ];

      let placeholder = getLanguage=='id' ? 'Tulis ceritamu dan pasien disini. Kamu bisa menceritakan: Latar belakang, Penyakit yang dialami pasien, dan Kondisi pasien saat ini' : 'Tell us patient’s and your story here. You can tell: Background story, The patient’s disease, and Patient condition in the present';
  
      var quill = new Quill('#story', {
        modules: {
          toolbar: toolbarOptions
        },
        placeholder: placeholder,
        theme: 'snow'
      });
  
      // end quill editor    
      
      // quill editor
        function getStory() {
            var delta = quill.getContents();
            
            return quillGetHTML(delta);
        }

        function getTextStory() {
            return quill.getText().replace(/\n/g, '') + "";
        }
      
        function quillGetHTML(inputDelta) {
          var tempCont = document.createElement("div");
          (new Quill(tempCont)).setContents(inputDelta);
      
          return tempCont.getElementsByClassName("ql-editor")[0].innerHTML;
        }
      
        function insertCanned(canned_html) {
          quill.clipboard.dangerouslyPasteHTML(canned_html);
        }
      // end quill editor

      // hideLinkToast();
        getTextareaParams(getTextStory());
        getTitleParams();
        getAssignParams();

        // 监听输入框内容， 获取输入框字数
        $(document).on("input propertychange", "#story .ql-editor", function(e) {
            getTextareaParams(getTextStory());
        });

        // story template :hide
        $("body").on("click", ".story-template .confirm", function() {
            $(".story-template").css("display", "none");

            $("#story .ql-editor").val($(".story-template .copy-detail").text());

            getTextareaParams(getTextStory()); //更新字数提示
            updateCache();
        });

        // submit
        $(".submitBtn").on("click", function() {
            let submitData = getSubmitParam(getStory());
            
            // console.log("submit Data", submitData);
            if (validate.check(submitData, lang)) {
                // $('.submitBtn').addClass('disabled');
                $UI.trigger("submit", [submitData]);
                utils.showLoading(lang.lang26);
            }
        });
};

//assign link fade
function senddata() {
    $(".link-fade").css("display", "block");
    var link = $("#assign-link")[0].value;
    $(".link-toast")[0].innerText = "share.pedulisehat.id/" + link;
}

function hideLinkToast() {
    $("input").blur(function() {
        $(".link-fade").css("display", "none");
    });
}

//assign tag fade
// function hideTagSearch() {
//     $("input").blur(function() {
//         $(".tag-fade").css("display", "none");
//     });
// }

function createTag(id, name) {
    const div = document.createElement('div');
    div.setAttribute('class', 'tag');
    div.setAttribute('id', 'tag-' + id);
    const span = document.createElement('span');
    span.innerHTML = name;
    const closeIcon = document.createElement('i');
    closeIcon.setAttribute('data-item', id);
    div.appendChild(span);
    div.appendChild(closeIcon);
    $("body").on("click", "#tag-" + id, function() {
        removeTags(id, name)
    });
    return div;
}
  
function clearTags() {
    document.querySelectorAll('.tag').forEach(tag => {
      tag.parentElement.removeChild(tag);
    });
}

Array.prototype.removeArray = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function removeTags(id, name){
    $("#tag-" + id).remove()
    tagIds.removeArray(id)
    tagNames.removeArray(name)
}
  
function addTags(id, name) {
    $("#tag-campaign").val("")
    if(!tagIds.includes(id)){
        clearTags();
        tagIds.push(id)
        tagNames.push(name)
        tagIds.slice().reverse().forEach((e, i) => {
            $(".tag-container").prepend(createTag(e, tagNames.slice().reverse()[i]));
        });
    }
}


/**
 * remove the cached project information
 */
obj.removeCache = function() {
    store.remove(CACHED_KEY);
};

/**
 * //Update local cache
 */
obj.updateCache = function() {
    updateCache();
};

obj.tagSearch = function(data) {
    $(".tag-toast").empty();
    if(data.length === 0){
        $(".tag-fade").css("display", "none");
    }else{
        data.forEach(e => {
            $(".tag-toast").append("<li id='tag-list-"+ +e.id + "'><div>" + e.name + "</div></li>");
            $("body").on("click", "#tag-list-" + e.id, function() {
                $(".tag-fade").css("display", "none");
                addTags(e.id, e.name)
            });
        });
        $(".tag-fade").css("display", "block");
        // var tag = ""
        // data.forEach(e => tag += e.name + "\n");
        // $(".tag-toast")[0].innerText = tag;
    }
}

/**
 *Main Js
 */

function setupUIHandler() {
    // judge
    if (datas && datas.total_amount) {
        $("#total_amount").val(changeMoneyFormat.moneyFormat(datas.total_amount));
    }

    // initialize img upload
    imgUploader.create(
        uploadBtn,
        uploadList,
        uploadKey,
        uploadNumLimit,
        imgChangedHandler,
        uploadWrapper
    );
    imgUploader.create_cover(
        uploadBtn2,
        uploadList2,
        uploadKey2,
        uploadNumLimit2,
        imgChangedHandler,
        uploadWrapper2
    );
    hideOrShowInputButton.init(); //show or hide upload btn

    //set cover
    // picCover.setCover(lang);

    // 输入框监听，本地存储数据
    $("input, select, textarea").on("input selected", function(e) {
        updateCache();
    });

    $(document).on("input propertychange", "#assign-link", function(e) {
        getAssignParams();
        senddata();
        hideLinkToast();
    });
    $(document).on("input propertychange", "#title", function(e) {
        getTitleParams();
    });

    // story template :show
    $("body").on("click", ".template", function() {
        $(".story-template").css("display", "block");
    });

    // story template :hide
    $("body").on("click", ".story-template .close", function() {
        $(".story-template").css("display", "none");
    });


    $("body").on("click", ".tips", function() {
        $(".tips-box").toggleClass("show");
    });

    // show limit time choose
    $("body").on("click", ".select-time-limit", function() {
        $(".common_limit_time").css({
            display: "block"
        });
    });

    $(document).on("input propertychange", "#tag-campaign", function(e) {
        var tag = $("#tag-campaign")[0].value;
        $UI.trigger("tag_search", tag);
        // hideTagSearch()
    });
}

/**
 * 获取提交数据
{
     "title": "We Will Beat United Airline",
     "story": "The protection of Giant Panda Bears CANNOT Wait!!!",
     "total_amount": 958300,
     "country": "",
     "city": "yjd",
     "category_id": 12,
     "platform": "android",
     "images": [{
         "name": "13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "image": "https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "thumb": "https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "location": "cloudinary"
     }]
 }
 */
function getSubmitParam(story) {
    return {
        title: $("#title").val(),
        story: story, //备注：$('#story').val()不能获取到值？？？
        total_amount: parseInt(
            $("#total_amount")
            .val()
            .replace(/\./g, "")
        ) || "",
        country: "1",
        city: $("#city").val(),
        category_id: 12,
        platform: "h5",
        tags: tagIds,
        images: imgUploader.getImageList(uploadList, uploadKey),
        cover: imgUploader.getImageList(uploadList2, uploadKey2),
        invite_code: $("#invitation-code").val(),
        short_link: $("#assign-link").val(),
        group_short_link: fromGrab,
        limit_day: parseInt($(".select-time-limit .num").text()) //时间限制
    };
}

/**
 * //Update local cache
 */
function updateCache() {
    // console.log('updateCache:', store.get(CACHED_KEY));
    store.set(CACHED_KEY, getSubmitParam());
}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    hideOrShowInputButton.init(); //show or hide upload btn

    console.log('initiate info img changed 1111');

    // imgUploader.refreshPictureNumber(uploadList);
    updateCache();
    $(".fancybox").fancybox();

    //set cover
    // picCover.setCover(lang);
}

/**
 * img changed 监听事件
 */
// function imgChangedHandler2() {
//     hideOrShowInputButton.init(); //show or hide upload btn

//     console.log('initiate info img changed 2222');

//     // imgUploader.refreshPictureNumber(uploadList2);
//     updateCache();
//     $(".fancybox").fancybox();

//     //set cover
//     // picCover.setCover(lang);
// }

// 输入框字数提示
function getTextareaParams(value) {
    let num;
    
    num = value.length;

    // console.log('num-125', value);
    if (num <= 5000) {
        num = num;
    } else {
        utils.alertMessage(lang.lang1);
    }
    $(".contentcount").html(num);
}
// 输入框字数提示
function getAssignParams() {
    var num;
    num = $("#assign-link").val() ? $("#assign-link").val().length : "0";
    // num = $('.input_story').val().length;

    // console.log('num-125', num)
    if (num <= 100) {
        num = num;
    } else {
        $(num).substr(0, 100);
        utils.alertMessage(lang.lang1);
    }
    $(".assign-contentcount").html(num);
}
// 输入框字数提示
function getTitleParams() {
    var num;
    num = $("#title").val() ? $("#title").val().length : "0";
    // num = $('.input_story').val().length;

    // console.log('num-125', num)
    if (num <= 100) {
        num = num;
    } else {
        $(num).substr(0, 100);
        utils.alertMessage(lang.lang1);
    }
    $(".title-contentcount").html(num);
}

export default obj;
