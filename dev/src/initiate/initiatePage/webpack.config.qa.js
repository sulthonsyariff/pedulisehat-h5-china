
var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'initiatePage',
    htmlFileURL: 'html/initiatePage.html',
    appDir: 'js/initiatePage',
    uglify: true,
    hash: '',
    mode: 'development'
})