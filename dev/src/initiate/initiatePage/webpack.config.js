var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'initiatePage',
    htmlFileURL: 'html/initiatePage.html',
    appDir: 'js/initiatePage',
    uglify: true,
    hash: '',
    mode: 'production'
})