// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import initiateChoose from 'initiateChoose'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(initiateChoose);
/* translation */

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    res.JumpName = '';
    commonNav.init(res);

    res.lang = lang;
    $UI.append(mainTpl(res)); //主模版

    // commonFooter.init(res);
    fastclick.attach(document.body);


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

export default obj;