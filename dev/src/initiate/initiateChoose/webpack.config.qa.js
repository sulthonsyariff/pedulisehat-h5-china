var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: '',
    htmlFileURL: 'html/initiateChoose.html',
    appDir: 'js/initiateChoose',
    uglify: true,
    hash: '',
    mode: 'development'
})