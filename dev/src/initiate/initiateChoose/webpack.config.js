var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: '',
    htmlFileURL: 'html/initiateChoose.html',
    appDir: 'js/initiateChoose',
    uglify: true,
    hash: '',
    mode: 'production'
})