var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    // htmlTitle: 'home',
    htmlFileURL: 'html/specialSubject.html',
    appDir: 'js/specialSubject',
    uglify: true,
    hash: '',
    mode: 'development'
})