// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
// import listItemTpl from '../tpl/listItem.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import domainName from 'domainName' // port domain name
import store from 'store'
import changeMoneyFormat from 'changeMoneyFormat'
import utils from 'utils';
// import sensorsActive from 'sensorsActive'
import share from 'share'
import campaignListComponents from 'campaignListComponents'

/* translation */
import home from 'home'
import qscLang from 'qscLang'
let lang = qscLang.init(home);
/* translation */

let [obj, $UI] = [{}, $('body')];
let CACHED_KEY = 'switchLanguage';
let short_link = location.pathname.split("/")[2]
let disabledDonateAll = false;

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    // res.JumpName = 'page';
    res.JumpName = res.data.name;
    res.commonNavGoToWhere = '/';

    commonNav.init(res);

    res.lang = lang
    res.domainName = domainName;
    res.data.total_current_money = changeMoneyFormat.moneyFormat(res.data.total_current_money);
    groupDetialHandle(res);
    res.image = JSON.parse(res.data.image)
    console.log("resssss", res);
    $UI.append(mainTpl(res)); //主模版
    commonFooter.init(res);
    fastclick.attach(document.body);
    clickHandle(res);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();
};

obj.insertData = function(rs) {
    if (rs.data.length != 0) {
        rs.hotCampaigns = rs.data
        rs.fromWhichPage = 'specialSubject.html'
        campaignListComponents.init(rs);

        // validate button if campaign all finished

        rs.data.every(element => {
            if (element.stopped == 1) {
                console.log('true');
                disabledDonateAll = true;
                return true;
            } else {
                console.log('false');
                disabledDonateAll = false;
                return false;
            }
        });

        if (disabledDonateAll) {
            $('.donate').addClass('disabled');
        }
    }
}

function groupDetialHandle(rs) {
    let project_ratio = rs.data.project_ratio
    let sign = rs.data.sign
    let name = rs.data.name
        // $.cookie('groupDonate',JSON.stringify({
        //     sign: sign,
        //     project_ratio: project_ratio,
        //     group_id: rs.data.group_id,
        //     short_link: rs.data.short_link,
        //     name: name
        // }))
    store.set('groupDonate', {
        sign: sign,
        project_ratio: project_ratio,
        group_id: rs.data.group_id,
        short_link: rs.data.short_link,
        name: name
    })
    console.log('project_ratio', project_ratio, sign)
}

function clickHandle(rs) {
    console.log('rs', rs)

    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        let paramObj = {
            item_id: JSON.stringify(rs.data.group_id),
            item_type: 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            item_short_link: short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'specialSubject.html' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // 分享组件
            share.init(paramObj);
        }
    });

    // share
    $('body').on('click', '#shareClose', function(e) {
        $('.common-share').css({
            'display': 'none'
        });
    });

    $('body').on('click', '#donate', function(e) {
        if (!disabledDonateAll) {
            $('.donate').children().css({
                    'display': 'block',
                })
                // setTimeout(function() {
                //     $('.donate').children().css({
                //         'display': 'none',
                //     })
                // }, 200)
            // sensors.track('DonateButtonClick', {
            //     from: document.referrer,
            //     type: 'campaign group',
            //     campaign_group_id: rs.data.group_id,
            //     campaign_group_name: rs.data.name,
            // })
            $UI.trigger('donate')
        }
    });

    $('body').on('click', '.campaign_box', function(e) {
        store.set('group', short_link)
    });
}

obj.raiseDay = function(created_at) {

    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}


export default obj;