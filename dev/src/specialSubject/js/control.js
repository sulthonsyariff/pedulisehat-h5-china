import view from './view'
import model from './model'
import model_getListData from 'model_getListData' //列表接口调用地址
import 'jq_cookie' //ajax cookie

import 'loading'
import '../less/main.less'
import store from 'store'
import domainName from 'domainName'; //port domain name

// 引入依赖
import utils from 'utils'
let UI = view.UI;

let short_link = location.pathname.split("/")[2]
let group_id;
let forwarding_default;
let forwarding_desc;
let shareUrl;
// let donatedButtonAB = store.get('donated_button-AB')
let reqObj = utils.getRequestParams();
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

console.log('user-id')
if (store.get('group')) {
    store.remove('group')
}
// 隐藏loading
utils.hideLoading();

if (statistics_link && statistics_link != 'null') {
    model.shareAccessCount({
        param: {
            statistics_link: statistics_link,
            source_type: 0,
            visitor_id: user_id,
            terminal: 'H5',
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('submit share count')
            } else {
                utils.alertMessage(rs.msg)
            }
        },
    })
}

model.groupCount({
    params: {
        short_link: short_link
    },
    success: function(res) {
        if (res.code == 0) {
            groupDeatils(res);
        } else {
            utils.alertMessage(res.msg)
        }
    },

    error: utils.handleFail
})

function groupDeatils(rs) {
    model.groupDeatils({
        params: {
            short_link: short_link
        },
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                group_id = res.data.group_id
                utils.changeFbHead({
                    url: "https:" + domainName.share + '/group/' + short_link,
                    title: res.data.name,
                    description: res.data.description.substr(1, 100),
                    image: res.data.image ? res.data.image.image : ''
                });
                res.data.count = rs.data.count
                res.data.total_current_money = rs.data.total_current_money
                getGroupProjects(res);
                // getShareInfo(res);
                view.init(res);
            } else {
                utils.alertMessage(res.msg)
            }
        },

        error: utils.handleFail
    })
}


function getGroupProjects(rs) {
    model_getListData.getListData_specialSubject({
        params: {
            short_link: short_link
        },
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                console.log('res===control', res)

                utils.hideLoading();
                view.insertData(res);
            } else {
                utils.alertMessage(res.msg)
            }
            // console.log('insertData', res);
        },
        // unauthorizeTodo : function(rs){
        //     utils.hideLoading();
        //     view.insertData(res);
        // },
        error: utils.handleFail
    })
}

UI.on('projectShareSuccess', function(e, target_type) {
    shareActionCount(target_type);
});
// 修改dom分享数
function shareActionCount(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_type: 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            item_id: JSON.stringify(group_id),
            // short_link: short_link,
            scene: 1, //1:表示捐赠/付款前的分享 2:表示捐赠/付款成功后的分享 参数不传 默认为1
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位

            // activity_id: 'group', // 此处不能传空，与证实分享区分
            // ab_test: {
            //     'donated_button': donatedButtonAB // AB 测试需要
            // }
        },
        success: function(res) {
            if (res.code == 0) {

            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}

UI.on('donate', function(e) {
    location.href = '/donateMoney.html?short_link=' + short_link + '&group_id=' + group_id;
})