import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


// obj.getUserInfo = function(o) {
//     let url = domainName.passport + '/v1/user';

//     if (isLocal) {
//         url = '../mock/userInfo.json';
//     }

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url
//     }, o, 'unauthorizeTodo')
// };

obj.groupDeatils = function(o) {
    let url = domainName.project + '/v1/group_project?short_link=' + o.params.short_link;

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

obj.groupCount = function(o) {
    let url = domainName.project + '/v1/group_projects/count?short_link=' + o.params.short_link;

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

// obj.projectShare = function(o) {
//     var url = domainName.project + '/v1/project_share';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }

//新分享统计接口
obj.newProjectShare = function(o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//获取分享相关内容
obj.getShareInfo = function(o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//分享数据上报
obj.shareAccessCount = function(o) {
    let url = domainName.share + '/v1/share_access_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}
export default obj;