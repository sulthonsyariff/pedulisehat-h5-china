// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import listItemTpl from '../tpl/listItem.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import domainName from 'domainName' // port domain name
import store from 'store'
import changeMoneyFormat from 'changeMoneyFormat'
import utils from 'utils';
// import sensorsActive from 'sensorsActive'
import share from 'share'

/* translation */
import home from 'home'
import qscLang from 'qscLang'
let lang = qscLang.init(home);
/* translation */

let [obj, $UI] = [{}, $('body')];
let CACHED_KEY = 'switchLanguage';
let short_link = location.pathname.split("/")[2]

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    // res.JumpName = 'page';
    res.JumpName = res.data.name;
    res.commonNavGoToWhere = '/';

    commonNav.init(res);

    res.lang = lang
    res.domainName = domainName;
    res.data.total_current_money = changeMoneyFormat.moneyFormat(res.data.total_current_money);
    groupDetialHandle(res);
    res.image = JSON.parse(res.data.image)
    $UI.append(mainTpl(res)); //主模版
    commonFooter.init(res);
    fastclick.attach(document.body);
    clickHandle(res);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};


obj.insertData = function(rs) {
    // function insertData (rs) {
    rs.domainName = domainName;
    let arr = [];

    if (rs.data.length != 0) {
        // rs.hotCampaigns = rs.data
        for (let i = 0; i < rs.data.length; i++) {
            if (!rs.data[i].stopped) {
                arr.push(rs.data[i])
            }
        }

        for (let i = 0; i < rs.data.length; i++) {

            rs.hotCampaigns = rs.data
                //时间
                // rs.hotCampaigns[i].created_at = obj.raiseDay(rs.hotCampaigns[i].created_at);
                //时间
            if (rs.hotCampaigns[i].closed_at) {
                rs.hotCampaigns[i].timeLeft = utils.timeLeft(rs.hotCampaigns[i].closed_at);
            }
            // 进度条：设置基准的高度为1%，
            rs.hotCampaigns[i].progress = rs.hotCampaigns[i].current_amount / rs.hotCampaigns[i].target_amount * 100 + 1;

            //   图片
            rs.hotCampaigns[i].cover = JSON.parse(rs.hotCampaigns[i].cover);

            rs.hotCampaigns[i].rightUrl = utils.imageChoose(rs.hotCampaigns[i].cover)
            rs.hotCampaigns[i].avatar = utils.imageChoose(rs.hotCampaigns[i].avatar)

            // 格式化金额
            rs.hotCampaigns[i].target_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].target_amount);
            rs.hotCampaigns[i].current_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].current_amount);

            rs.hotCampaigns[i].corner_mark = rs.hotCampaigns[i].corner_mark ? JSON.parse(rs.hotCampaigns[i].corner_mark) : '';

            // 添加结束印戳
            rs.hotCampaigns[i].getLanguage = utils.getLanguage();

        }
        rs.lang = lang;

        $('.campaigns_list').append(listItemTpl(rs));
        // if row's number more than 2,show the overflow class
        for (let i = 0; i < rs.data.length; i++) {
            if ($(".inner_name" + '_' + i).height() > 32) {
                $('.campaign_name' + '_' + i).addClass('overflow')
            }
        }

        let windowWidth = $(window).width() / 2
        $(".user-name").each(function() {
            console.log('windowWidth',windowWidth)
            if ($(this).width() > windowWidth) {
                $(this).css({
                    'width': windowWidth,
                    'overflow': 'hidden',
                    'white-space': 'nowrap',
                    'text-overflow': 'ellipsis',
                })
            }
        });

    }
    rs.lang = lang;
}

function groupDetialHandle(rs) {
    let project_ratio = rs.data.project_ratio
    let sign = rs.data.sign
    let name = rs.data.name
    store.set('groupDonate', {
        sign: sign,
        project_ratio: project_ratio,
        group_id: rs.data.group_id,
        short_link: rs.data.short_link,
        name: name
    })
    console.log('project_ratio', project_ratio, sign)
}

function clickHandle(rs) {
    console.log('rs', rs)

    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        let paramObj = {
            item_id: JSON.stringify(rs.data.group_id),
            item_type: 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            item_short_link: short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'specialSubject.html' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // 分享组件
            share.init(paramObj);
        }
    });

    // share
    $('body').on('click', '#shareClose', function(e) {
        $('.common-share').css({
            'display': 'none'
        });
    });

    $('body').on('click', '#donate', function(e) {
        $('.donate').children().css({
                'display': 'block',

            })
            // setTimeout(function() {
            //     $('.donate').children().css({
            //         'display': 'none',
            //     })
            // }, 200)
        sensors.track('DonateButtonClick', {
            from: document.referrer,
            type: 'campaign group',
            campaign_group_id: rs.data.group_id,
            campaign_group_name: rs.data.name,
        }, $UI.trigger('donate'))

    });

    $('body').on('click', '.campaign_box', function(e) {
        store.set('group', short_link)
    });
}

obj.raiseDay = function(created_at) {

    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}


export default obj;