import ajaxProxy from "ajaxProxy";
import "jq_cookie"; //ajax cookie
import domainName from "domainName"; // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getInsOrder = function(o) {
    let url = domainName.psuic + "/v1/ins_order?page=" + o.param.page;

    if (isLocal) {
        url = "mock/Asuransi/v1_ins_order_get_" + o.param.page + '.json';
    }

    ajaxProxy.ajax({
            type: "get",
            url: url,
        },
        o
    );
};

obj.getInsPolicy = function(o) {
    let url = domainName.psuic + "/v1/ins_policy/my?page=" + o.param.page;

    if (isLocal) {
        url = "mock/Asuransi/v1_ins_policy_my_" + o.param.page + '.json';
    }

    ajaxProxy.ajax({
            type: "get",
            url: url,
        },
        o
    );
};

// obj.getIsellerUrl = function(o) {
//     let url = domainName.psuic + "/v1/psac/get-iseller-url?policy_id=" + o.param.policy_id;

//     ajaxProxy.ajax({
//             type: "get",
//             url: url,
//         },
//         o
//     );
// };

obj.getIsellerUrl = function(o) {
    let url = domainName.psuic + "/v1/psac/check-ktp-eligible";

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

export default obj;