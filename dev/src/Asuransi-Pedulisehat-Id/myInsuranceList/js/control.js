import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

utils.showLoading();

let UI = $('body');

view.init({});

UI.on('needload', function(e, page) {
    model.getInsOrder({
        param: {
            page: page,
        },
        success: function(rs) {
            if (rs.code == 0) {
                if (rs.data) {
                    for (let i = 0; i < rs.data.length; i++) {
                        rs.data[i].payable_amount = changeMoneyFormat.moneyFormat(rs.data[i].payable_amount)
                        rs.data[i].sum_order_amount = changeMoneyFormat.moneyFormat(rs.data[i].sum_order_amount)
                    }
                }
                view.insertInsOrderData(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
})

UI.on('needload2', function(e, page2) {
    model.getInsPolicy({
        param: {
            page: page2,
        },
        success: function(rs) {
            if (rs.code == 0) {
                if (rs.data) {
                    for (let i = 0; i < rs.data.length; i++) {
                        rs.data[i].effective_time = utils.ddmmyyyy(rs.data[i].effective_time)
                        rs.data[i].expire_date = utils.ddmmyyyy(rs.data[i].expire_date)
                    }
                }

                view.insertInsPolicyData(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
})

UI.on('getIsellerUrl', function(e, identityNumber) {
    model.getIsellerUrl({
        param: {
            identityNumber
        },
        success: function(rs) {
            if (rs.code == 0) {                
                if (rs.data.status == "init") {
                    let payload = {
                        policy: {
                            summaryFinal: {
                                noPolis: rs.data.polisNo,
                                urlIseller: rs.data.iseller_url,
                                policyPeriod: rs.data.policy_period,
                                totalAmount: rs.data.total_amount,
                                policyId: rs.data.policy_id
                            }
                        }
                    };

                    localStorage.setItem("pedulisehat-asuransi", JSON.stringify(payload));
                    location.href = "/pedulicare/summary-final";
                } else {
                    utils.alertMessage("Nomor KTP Sudah Terdaftar");
                }
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
})