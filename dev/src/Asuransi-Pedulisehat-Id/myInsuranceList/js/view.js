import mainTpl from '../tpl/main.juicer'
import _insuranceOrderItem from '../tpl/_insurance-order.juicer'
import _myPolicyItem from '../tpl/_my-policy.juicer'

import nav from 'nav'
import qscScroll_timestamp from 'qscScroll_timestamp'
import qscScroll_timestamp2 from 'qscScroll_timestamp'
import utils from 'utils'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'

/* translation */
import myInsuranceList from 'asuransi-myInsuranceList'
import qscLang from 'qscLang'
let lang = qscLang.init(myInsuranceList);
/* translation */

let is_first = true;
let page = 0;
let is_first2 = true;
let page2 = 0;
let scroll_list = new qscScroll_timestamp();
let scroll_list2 = new qscScroll_timestamp2();

let [obj, $UI] = [{}, $('body')];

fastclick.attach(document.body);

// google anaytics
let param = {};
googleAnalytics.sendPageView(param);

obj.init = function(res) {
    res.JumpName = lang.lang_title;
    // res.commonNavGoToWhere = utils.judgeDomain() == 'qa' ? 'https://qa.pedulisehat.id' : utils.judgeDomain() == 'pre' ? "https://pre.pedulisehat.id" : 'https://www.pedulisehat.id'
    res.commonNavGoToWhere = 'https://' + utils.judgeDomain() + '.pedulisehat.id/myProfile.html';
    $('title').html(lang.lang_title);
    res.lang = lang;

    if (!utils.browserVersion.android) {
        // res.JumpName = lang.JumpName;
        nav.init(res);
    } else {
        location.href = 'native://do.something/setTitle?title=' + lang.lang_title
    }

    $UI.append(mainTpl(res));


    initScorll();

    clickHandle(res);

    // handle UI
    for (let i = 0; i < $('.my-policy-cont .title').length; i++) {
        console.log('==', $($('.btn')[i]).width())
        $($('.my-policy-cont .title')[i]).css({
            'padding-right': $($('.btn')[i]).width() + 40
        })
    }
};

function initScorll() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });

    scroll_list.run();

    // console.log('scroll');
    scroll_list2.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload2', [++page2])
        }
    });

    scroll_list2.run();
};

// my insurance order
obj.insertInsOrderData = function(rs) {
    rs.lang = lang;
    utils.hideLoading();

    if (rs.data) {
        console.log('rs_data', rs.data)
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (let index = 0; index < rs.data.length; index++) {
            let data = {
                data: null,
                lang: rs.lang
            };
            data.data = rs.data[index];
            $('.insurance-order-cont').append(_insuranceOrderItem(data));
        }
        scroll_list.run();

    } else if (is_first) {
        console.log("is first dude");
        $('.loading').hide();
        $('.insurance-order-cont').append(_insuranceOrderItem(rs));
    } else if (rs.data === null) {
        $('.loading').hide();
    }
}

obj.insertInsPolicyData = function(rs) {
    rs.lang = lang;

    utils.hideLoading();

    if (rs.data) {
        // console.log('rs_data', rs.data)
        if (is_first2) {
            rs.is_first = is_first2;
        }
        is_first2 = false;

        $('.my-policy-cont').append(_myPolicyItem(rs));

        scroll_list2.run();

    } else if (is_first2) {

        $('.loading2').hide();
        $('.my-policy-cont').append(_myPolicyItem(rs));

    } else if (rs.data === null) {
        $('.loading2').hide();
    }
}

function clickHandle(res) {
    // tab
    $UI.on('click', '.tab', function() {
        $('.tab').removeClass('chosed');
        $(this).addClass('chosed')

        if ($(this).hasClass('insurance-order-tab')) {
            $('.cont-list').removeClass('chosed');
            $('.insurance-order-cont-wrapper').addClass('chosed')

        } else if ($(this).hasClass('my-policy-tab')) {
            $('.cont-list').removeClass('chosed')
            $('.my-policy-cont-wrapper').addClass('chosed')
        }

    })

    // pay siji
    $UI.on('click', '.pay', function() {
        location.href = "/sijicorona/beneficiary-data/" + $(this).attr('data-order_no') + "/payment";
        // location.href = "/benifitInfo.html?policy_id=" + $(this).attr('data-order_no') + "&paymentPopup=true" + "&policy_state=" + $(this).attr('data-policy_state');
    })

    // pay psac
    $UI.on('click', '.pay_psac', function() {
        $UI.trigger('getIsellerUrl', $(this).attr('data-identity_number'));
    })

    // check
    $UI.on('click', '.my-policy-cont .list', function() {
        location.href = "/policyData.html?policy_id=" + $(this).attr('data-order_no')
    })

    // complete data
    $UI.on('click', '.complete-data', function() {
        location.href = "/sijicorona/policy-holder/" + $(this).attr('data-order_no')
        // location.href = "/holderInfo.html?policy_id=" + $(this).attr('data-order_no')
    })

    // whatsapp contact
    $UI.on('click', '.whatsappContact', function(e) {
        let appSrc = 'https://api.whatsapp.com/send?phone=6281230009479&text=Halo%20,%20Saya%20mau%20konsultasi%20asuransi%20di%20pedulisehat.id';
        // in our android app
        if (utils.browserVersion.androids) {
            // native://intent?url=<gojet url>&errorInfo=<在此加上协议解析失败的提示 比如提示用户未安装app之类的>
            window.location = 'native://intent?url=' + encodeURIComponent(appSrc) + '&errorInfo=Error';
        } else {
            location.href = appSrc;

        }
    });

}
export default obj;