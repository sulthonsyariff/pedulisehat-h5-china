import mainTpl from '../tpl/main.juicer'
import nav from 'nav'
import qscScroll_timestamp from 'qscScroll_timestamp'
import utils from 'utils'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'

/* translation */
import policyData from 'asuransi-policyData'
import qscLang from 'qscLang'
let lang = qscLang.init(policyData);
/* translation */

let is_first = true;
let page = 0;
let scroll_list = new qscScroll_timestamp();

let reqObj = utils.getRequestParams();
let typeInsurance = reqObj['type'];

let [obj, $UI] = [{}, $('body')];

fastclick.attach(document.body);
// google anaytics
let param = {};
googleAnalytics.sendPageView(param);

obj.init = function(res) {
    res.JumpName = 'Policy Data';

    if (!utils.browserVersion.android) {
        // res.JumpName = lang.JumpName;
        nav.init(res);
    } else {
        location.href = 'native://do.something/setTitle?title=' + res.JumpName
    }

    $('title').html('Policy Data');

    res.lang = lang;
    res.typeInsurance = typeInsurance;
    $UI.append(mainTpl(res));

    // 隐藏loading
    utils.hideLoading();

    clickHandle(res);
};


function clickHandle(res) {
    // red-btn-edit
    $UI.on('click', '.red-btn-edit', function() {
        utils.showLoading();
        setTimeout(() => {
            location.href = "/holderInfo.html?order_no=" + res.data.order_no
        }, 300);
    })
}
export default obj;