import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

//idcard_verify
obj.getPolicyData = function(o) {
    let url = domainName.psuic + '/v1/ins_policy?policy_id=' + o.param.policy_id;

    if (isLocal) {
        url = 'mock/Asuransi/v1_ins_policy.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};


export default obj;