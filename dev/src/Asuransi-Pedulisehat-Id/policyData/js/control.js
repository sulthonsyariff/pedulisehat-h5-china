import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import utils from 'utils'

let reqObj = utils.getRequestParams();
let policy_id = reqObj['policy_id'];

// 隐藏loading
utils.showLoading();

model.getPolicyData({
    param: {
        policy_id: policy_id,
    },
    success: function(rs) {
        if (rs.code == 0) {
            console.log("rs", rs);
            rs.data.policy_holder.id_card = utils.change4in1(rs.data.policy_holder.id_card)
            rs.data.policy_ins_person.forEach((element, index) => {
                rs.data.policy_ins_person[index].id_card = utils.change4in1(element.id_card);
            });

            view.init(rs);
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});