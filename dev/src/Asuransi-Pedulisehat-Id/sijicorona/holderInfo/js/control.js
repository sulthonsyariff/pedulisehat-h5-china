import view from './view'
import model from './model'
import locationChoose from 'locationChoose'
import store from 'store'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();
let policy_id = reqObj['policy_id'];
let order_no;
// let p_holder_id = reqObj['p_holder_id'];
let relationship;
let order_sub_id;

// obj.removeCache = function () {
//     store.remove(CACHED_KEY);
// };

if (policy_id) {
    store.remove('holderInfo');
    getHolderInfo();
    // model.getSijiInsuranceDetail({
    //     param: {
    //         terminal: 0,
    //         policy_id
    //     },
    //     success: function(rs) {
    //         if (rs.code == 0) {
    //             // policy_state 1 = proses isi data
    //             // policy_state 2 = data telah terisi
    //             // policy_state > 2 sudah bayar
    //             relationship = rs.data.insured_user.relationship
    //             order_sub_id = rs.data.commodity_items[0].order_sub_id;
    //             order_no = rs.data.order_no
    //             getHolderInfo(rs)
    //         } else {
    //             utils.alertMessage(rs.msg)
    //         }
    //     },
    //     // 游客可执行的操作？？？
    //     // unauthorizeTodo: function(rs) {
    //     // },
    
    //     error: utils.handleFail,
    // });
} else {
    let rs = {
        data: {
            lang: null
        }
    }
    locationList();
    view.init(rs);
}

function getHolderInfo() {
    model.getHolderInfo({
        param: policy_id,
        success: function(rs) {
            if (rs.code == 0) {
                rs.order_sub_id = order_sub_id
                    // rs.order_sub_id = res.data.commodity_items[0].order_sub_id

                view.init(rs);
                locationList();

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}



function locationList() {
    model.locationList({
        success: function(rs) {
            if (rs.code == 0) {
                // sort ascending name
                let locationTemp;
                locationTemp = rs.data.sort((a, b) => {
                    if (a.name < b.name)
                      return -1;
                    if (a.name > b.name)
                      return 1;
                    return 0;
                });
                rs.data = locationTemp;

                // push location
                locationChoose.renderList(rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}


UI.on('submit', function(e, params) {
    if (policy_id) {
        params.policy_id = policy_id;
        delete params["commodity_item"];         
    }

    // params.order_sub_id = order_sub_id;
    model.submitInsuranceInfo({
        param: params,
        success: function(res) {
            if (res.code == 0) {
                store.remove('holderInfo');
                location.href = '/insuredInfo.html?policy_id=' + res.data.policy_id
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})