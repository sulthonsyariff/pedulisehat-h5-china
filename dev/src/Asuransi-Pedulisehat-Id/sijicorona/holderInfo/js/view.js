// 公共库
import nav from 'nav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import locationChoose from 'locationChoose'

// import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import validate from './_validate' // validate the form

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils'
// // import sensorsActive from 'sensorsActive'

/* translation */
import asuransiPolicy from 'asuransiPolicy'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(asuransiPolicy);
/* translation */

/* storage key */
let CACHED_KEY = 'holderInfo';
let getLanguage = utils.getLanguage();
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : '';

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let policy_id = reqObj['policy_id'];
let policy_state = reqObj['policy_state'];
let dataSubmitAsuransi = localStorage.getItem("dataSubmitAsuransi");

// 初始化
obj.init = function(res) {

    if (!policy_id) {
        if (!datas) {
            if (dataSubmitAsuransi) {
                datas = JSON.parse(dataSubmitAsuransi).policyholder_user;
            }
        }
    }

    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang1
    }


    res.lang = lang;
    if (!utils.browserVersion.androids) {

        res.JumpName = lang.JumpName;

        nav.init(res);
        console.log('nav', res)

    }

    if (datas) {
        console.log('---datas', datas)

        order_sub_id = datas.order_sub_id
        datas.lang = lang;
        datas.getLanguage = getLanguage;
        datas.asuransiPolicy_step = 1

    } else {
        console.log('---datas---', datas)

        order_sub_id = res.order_sub_id
            // res.data = {}
        res.data.lang = lang;
        res.data.asuransiPolicy_step = 1

    }


    console.log('view=', res)


    $UI.append(mainTpl(datas || res.data)); //主模版
    // $UI.append(alertBoxTpl(res)); //验证弹窗

    locationChoose.init(datas || res.data);

    utils.inputFourInOne("#KTP"); //限制4位一格


    // 隐藏loading
    utils.hideLoading();
    commonFooter.init(res);


    fastclick.attach(document.body);
    cliclHandle(res);
    // // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {

    // 输入框监听，本地存储数据
    $("input, select, textarea").on("input selected", function(e) {
        updateCache();
    });

    // 选择地区更新
    $('body').on('click', '.list-items', function() {
        console.log('location-update')
        updateCache();
    });

    // $('body').on('click', '.address-wrap', function () {
    //     $('level-1')
    // })

    // got it
    $('body').on('click', '.confirm', function() {
        $('.alert-box').css('display', 'none');
    });

    // holder
    // $('body').on('click', '.holder-info', function () {
    //     location.href="/holderInfo.html?policy_id=" + policy_id;
    // })

    // insured
    $('body').on('click', '.insured-info', function () {
        if (policy_state == "2" || policy_state == "11" || policy_state == "12") {
            location.href="/insuredInfo.html?policy_id=" + policy_id + "&policy_state=" + policy_state;
        }
    })
    
    // benefit
    $('body').on('click', '.benefit-info', function () {
        if (policy_state == "2" || policy_state == "11" || policy_state == "12") {
            location.href="/benifitInfo.html?policy_id=" + policy_id + "&policy_state=" + policy_state;
        }
    })

    // submit
    $('.next-btn').on('click', function() {
        let submitData = getSubmitParam();
        updateCache();
        console.log('submitData', submitData)
        store.set(CACHED_KEY, getSubmitParam());

        if (validate.check(submitData, lang)) {
            // $('.submitBtn').addClass('disabled');
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang44);
        }
    });
}



/**
 * //Update local cache
 */
function updateCache() {
    console.log('updateCache', getSubmitParam())
    store.set(CACHED_KEY, getSubmitParam());
}


function getSubmitParam() {

    let province_name = $('.address-content').attr('province-name') || $('.region-choose-wrap').attr('province-name');
    let city_name = $('.address-content').attr('city-name') || $('.region-choose-wrap').attr('city-name');

    province_name = province_name || ''
    city_name = city_name || ''

    console.log(province_name || '')
    return {
        // order_sub_id: order_sub_id,
        policy_id: policy_id,

        full_name: $('#name').val(),
        id_card: $('#KTP').val().replace(/\s|\xA0/g, ""),
        email: $('#email').val(),
        // tax_number: $('#npwp').val(),
        mobile_number: $('#phone').val(),

        province_name: province_name,
        city_name: city_name,
        // province_name: $('.address-content').attr('province-name') || $('.region-choose-wrap').attr('province-name'),
        // city_name: $('.address-content').attr('city-name') || $('.region-choose-wrap').attr('city-name'),
        postal_code: $('#zip-code').val(),

        address: $('#full-address').val(),
        commodity_item: localStorage.getItem("commodityItem")
    }
}
export default obj;