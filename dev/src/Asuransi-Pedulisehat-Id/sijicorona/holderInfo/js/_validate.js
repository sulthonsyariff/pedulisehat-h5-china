/*
 * validate form
 */
import utils from 'utils'


let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    if (param.full_name == '') {
        utils.alertMessage(lang.lang22);
        return false;
    }
    if (param.id_card == '') {
        utils.alertMessage(lang.lang23);
        return false;
    }
    if (param.email == '') {
        utils.alertMessage(lang.lang24);
        return false;
    }
    // if (param.tax_number == '') {
    //     utils.alertMessage(lang.lang25);
    //     return false;
    // }
    if (param.mobile_number == '' || param.mobile_number.length < 7) {
        utils.alertMessage(lang.lang26);
        return false;
    }

    // if (param.province_name) {
    //     utils.alertMessage(lang.lang28_1);
    //     return false;
    // }
    if (param.province_name == '') {
        utils.alertMessage(lang.lang28_1);
        return false;
    }
    if (param.city_name == '') {
        utils.alertMessage(lang.lang28_1);
        return false;
    }

    if (param.postal_code == '') {
        utils.alertMessage(lang.lang27);
        return false;
    }
    if (param.address == '') {
        utils.alertMessage(lang.lang28);
        return false;
    }


    return true;
}

export default obj;