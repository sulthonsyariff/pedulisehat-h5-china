/*
 * 校验表单信息
 */
import utils from 'utils'
import checkAge from './_checkAge'
let obj = {};

obj.init = function(param, lang) {
    // console.log(' checkAge.init',checkAge.init(param,lang))


    // insured_user:birth_date
    if (!parseInt(param.insured_user.yyyy) > 0 ||
        !parseInt(param.insured_user.mm) > 0 ||
        !parseInt(param.insured_user.dd) > 0 ||
        param.insured_user.mm > 12 ||
        param.insured_user.dd > 31 ||
        param.insured_user.yyyy.length < 4) {

        utils.alertMessage(lang.lang_check1);
        return false;
    }
    // policyholder_user: full_name
    if (!param.policyholder_user.full_name) {
        utils.alertMessage(lang.lang_check2);
        return false;
    }
    // policyholder_user: mobile_number
    if (!param.policyholder_user.mobile_number ||
        param.policyholder_user.mobile_number.length < 7) {
        utils.alertMessage(lang.lang_check3);
        return false;
    }
    // policyholder_user: birth_date
    if (!parseInt(param.policyholder_user.yyyy) > 0 ||
        !parseInt(param.policyholder_user.mm) > 0 ||
        !parseInt(param.policyholder_user.dd) > 0 ||
        param.policyholder_user.mm > 12 ||
        param.policyholder_user.dd > 31 ||
        param.insured_user.yyyy.length < 4) {

        utils.alertMessage(lang.lang_check4);
        return false;
    }

    let [validI, validH] = checkAge.init(param, lang)

    if (!validI || !validH) {
        return false;
    }

    // if(!checkAge.init(param,lang)){
    //     return false;
    // }

    // healthStatement
    if (!$('.statement .chosed').length) {
        utils.alertMessage(lang.lang_check5);
        return false;
    }
    console.log('validate', 'param:', param);



    return true;
}

/****
 * get:order
 *{
     "order_no": "",
     "commodity_items": [{
         //siji-COVID-19-A  siji-COVID-19-B siji-COVID-19-C siji-COVID-19-D
         "sku_key": "siji-COVID-19-A",
         "sku_number": 1,
         "is_print": false
     }],
     "policyholder_user": {
         "birth_date": "1990-04-02",
         "full_name": "",
         "mobile_number": ""
     },
     "terminal": 0,
     "insured_user": {
         "relationship": 0,
         "birth_date": "1990-04-02",
         "gender": 0
     }
 }
 */

export default obj;