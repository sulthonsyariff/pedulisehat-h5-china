// import paymentPopup from 'paymentPopup'
import _healthStatement from '../tpl/_healthStatement.juicer' //关闭项目弹窗
import _termsAndAgreement from '../tpl/_termsAndAgreement.juicer' //关闭项目弹窗
import utils from 'utils'
import shareAsuransi from 'shareAsuransi'
import _submitParam from "./_submitParam"
import _check from './_check'
let [obj, $UI] = [{}, $('body')];
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

obj.init = function(rs) {
    // submit insurance order
    $UI.on('click', '#join', function() {
        // 1.judge login
        if (!user_id) {
            utils.jumpToLogin(location.href, 'login_1');
        }
        // 2.check submit param
        else {
            if (_check.init(_submitParam.init(), rs.lang)) {
                // 3.submit ins order
                $UI.trigger('fetchInsuranceData');
                // $UI.trigger('validIsRepeat', _submitParam.init());
            }
        }
    })

    $UI.on('click', '.unpaidInsurance-wrapper #gotIt', function() {
        let submitParam = _submitParam.init();
        $('.unpaidInsurance-wrapper').css('display', 'none');
        location.href = "/holderInfo.html";
        localStorage.setItem('commodityItem', JSON.stringify(submitParam.commodity_items));
        localStorage.setItem('dataSubmitAsuransi', JSON.stringify(submitParam));        
    })

    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        let paramObj = {
            item_id: "999999999",
            item_type: 4,
            // item_short_link: '',
            // remark: data.title, //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'sijicorona-share-wrapper', // 分享弹窗名
            shareCallBackName: 'sijicoronaShareSuccess', //大病详情分享
            fromWhichPage: 'sijicorona.html' || '' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            // let actTitle = '';
            // let actDesc = '';

            // let paramString = "&actTitle=" + encodeURIComponent(actTitle) +
            //     "&actDesc=" + encodeURIComponent(actDesc)

            $UI.trigger('android-share', ['', paramObj]);

        } else {
            // 分享组件
            shareAsuransi.init(paramObj);
        }
    });


    // join-scroll
    $UI.on('click', '#join-scroll', function() {
        $(window).scrollTop($('.info-wrapper').offset().top - $('.nav').height() - $('.app-download-wrapper').height());
    })

    // tab: bronze sliver gold platinum
    $UI.on('click', '.tabs p', function() {
        if (!$(this).hasClass('chosed')) {
            $('.tabs p').removeClass('chosed')
            $(this).addClass('chosed')

            $('.cont').removeClass('chosed');
            $('.cont.' + $(this).attr('data-tab')).addClass('chosed');
        }
    })

    // bronze sliver gold platinum
    // relation
    // gender
    $UI.on('click', '.relation,.package-plan,.gender-list p', function() {
        if (!$(this).hasClass('chosed')) {
            $(this).siblings().removeClass('chosed')
            $(this).addClass('chosed')
        }

        $UI.trigger('refreshSubmitParam')
    })

    // need-copy
    // statement
    $UI.on('click', '.need-copy,.statement-icon', function() {
        $(this).toggleClass('chosed')
        $UI.trigger('refreshSubmitParam')
    })

    // back to home index
    $UI.on('click', '.home-btn', function() {
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://qa.pedulisehat.id'
        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://pre.pedulisehat.id'
        } else {
            location.href = 'https://www.pedulisehat.id'

        }
    })

    // whatsapp contact
    $UI.on('click', '.whatsappContact', function(e) {
        let appSrc = 'https://api.whatsapp.com/send?phone=6281230009479&text=Halo%20,%20Saya%20mau%20konsultasi%20asuransi%20di%20pedulisehat.id';
        // in our android app
        if (utils.browserVersion.androids) {
            // native://intent?url=<gojet url>&errorInfo=<在此加上协议解析失败的提示 比如提示用户未安装app之类的>
            window.location = 'native://intent?url=' + encodeURIComponent(appSrc) + '&errorInfo=Error';
        } else {
            location.href = appSrc;

        }
    });
}


obj.setHealtStatement = function(rs) {
    // healthStatement
    $UI.on('click', '.healthStatement', function() {
        console.log('===healthStatement===', rs);

        if (!$('.healthStatement-wrapper').length) {
            $UI.append(_healthStatement(rs));
        }

        $('.healthStatement-wrapper .title').html(rs.title);
        $('.healthStatement-wrapper .description').html(rs.description);

        $('.healthStatement-wrapper').css('display', 'block');
    })

    // healthStatement got it
    $UI.on('click', '.healthStatement-wrapper #gotIt', function() {
        $('.healthStatement-wrapper').css('display', 'none');
    })

}

obj.setTermAgreement = function(rs) {
     // _termsAndAgreement
     $UI.on('click', '.termsAndAgreement', function() {
        console.log('===termsAndAgreement===', rs);

        if (!$('.termsAndAgreement-wrapper').length) {
            $UI.append(_termsAndAgreement(rs));
        }

        $('.termsAndAgreement-wrapper .title').html(rs.title);
        $('.termsAndAgreement-wrapper .description').html(rs.description);

        $('.termsAndAgreement-wrapper').css('display', 'block');
    })

    // _termsAndAgreement got it
    $UI.on('click', '.termsAndAgreement-wrapper #gotIt', function() {
        $('.termsAndAgreement-wrapper').css('display', 'none');
    })
}

export default obj;