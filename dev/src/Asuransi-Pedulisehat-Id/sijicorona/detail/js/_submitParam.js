let [obj, $UI] = [{}, $('body')];

//  JSON.parse(sessionStorage.getItem('sijicoronaSubmitParam'))
obj.init = function() {
    function handleData(n) {
        n = n.toString();
        return n[1] ? n : '0' + n;
    }

    let policy_mm = handleData($('.policy_holder .mm').val())
    let policy_dd = handleData($('.policy_holder .dd').val())
    let insured_user_mm = handleData($('.insured_user .mm').val())
    let insured_user_dd = handleData($('.insured_user .dd').val())

    let submitObj = {
        "order_no": "",
        // 保险
        "commodity_items": [{
            "sku_key": $('.package-plan.chosed').attr('data-sku_key'),
            "sku_number": 1,
            "is_print": $('.need-copy').hasClass('chosed')
        }],
        // 投保人
        "policyholder_user": {
            "full_name": $('.policy_holder .fullname').val(),
            "mobile_number": $('.policy_holder .phone-number').val(),
            "birth_date": $('.policy_holder .yyyy').val().replace(/^0+/g, "") + '-' + policy_mm + '-' + policy_dd,
            "yyyy": $('.policy_holder .yyyy').val().replace(/^0+/g, ""),
            "mm": $('.policy_holder .mm').val(),
            "dd": $('.policy_holder .dd').val(),
        },
        "terminal": 0,
        // 被保人
        "insured_user": {
            "relationship": parseInt($('.insured_user .relation.chosed').attr('data-relation')),
            "gender": parseInt($('.insured_user .gender-list .chosed').attr('data-gender')),
            "birth_date": $('.insured_user .yyyy').val() + '-' + insured_user_mm + '-' + insured_user_dd,
            "yyyy": $('.insured_user .yyyy').val().replace(/^0+/g, ""),
            "mm": $('.insured_user .mm').val(),
            "dd": $('.insured_user .dd').val(),
        }
    }

    // console.log('===JSON.stringify(submitObj)===', submitObj);
    return submitObj
}

export default obj;