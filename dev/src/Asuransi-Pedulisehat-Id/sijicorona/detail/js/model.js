import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName'
import utils from 'utils'

// let reqObj = utils.getRequestParams();
// let showSP = reqObj['showSP'] || false; //是否展示shopeepay

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

//order：get
obj.getInsOrder = function(o) {
    let url = domainName.psuic + '/v1/ins_order/' + o.param.order_no;

    if (isLocal) {
        url = 'mock/Asuransi/v1_ins_order.json'
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

obj.getInsuranceData = function(o) {
    let url = domainName.psuic + "/v1/ins_order?page=" + o.param.page;

    if (isLocal) {
        url = "mock/Asuransi/v1_ins_order_get_" + o.param.page + '.json';
    }

    ajaxProxy.ajax({
            type: "get",
            url: url,
        },
        o
    );
};

//order：post
obj.postInsOrder = function(o) {
    let url = domainName.psuic + '/v1/ins_order';

    if (isLocal) {
        url = 'mock/Asuransi/v1_ins_order_post.json'
    }

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//pay !!!
obj.payIns = function(o) {
    let url = domainName.psuic + '/v1/ins_pay';

    // if (isLocal || 1) {
    //     url = 'mock/Asuransi/v1_ins_pay.json'
    // }

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//get payment
// '/v2/pay_channel'
obj.getPayment = function(o) {
    // let url = domainName.trade + '/v2/pay_channel' + (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    let url = domainName.trade + '/v1/pay_channel/pt' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    //  +
    // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    if (isLocal) {
        url = 'mock/Asuransi/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//新分享统计接口
obj.newProjectShare = function(o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

// spu销售数量
// /v1/spu/sale/siji-COVID-19
obj.getSpuSaleNum = function(o) {
    let url = domainName.insmd + "/v1/spu/sale/" + o.param.spu_key;

    if (isLocal) {
        url = 'mock/Asuransi/v1_spu_sale.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};
// 投保人是否重复
obj.isRepeat = function(o) {
    let url = domainName.psuic + "/v1/ins_policy/ins_person/repeats?relationship=" + o.param.relationship + '&birth_date=' + o.param.birth_date + '&gender=' + o.param.gender + '&spu_key=siji-COVID-19';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

obj.getBenefitsOfSiji = function(o) {
    ajaxProxy.ajaxFreshdesk({
        id: 43000600792
    },
    callback);

    function callback(res) {
        o.success(res)
    }
};

obj.getAlreadyBpjs = function(o) {
    ajaxProxy.ajaxFreshdesk({
        id: 43000600794
    },
    callback);

    function callback(res) {
        o.success(res)
    }
};

obj.getClaimProcess = function(o) {
    ajaxProxy.ajaxFreshdesk({
        id: 43000582429
    },
    callback);

    function callback(res) {
        o.success(res)
    }
};


obj.getHealtStatement = function(o) {
    ajaxProxy.ajaxFreshdesk({
        id: 43000582431
    },
    callback);

    function callback(res) {
        o.success(res)
    }
};

obj.getTermAgreement = function(o) {
    ajaxProxy.ajaxFreshdesk({
        id: 43000582425
    },
    callback);

    function callback(res) {
        o.success(res)
    }
};


export default obj;