import view from './view'
import _clickHandle from './_clickHandle'
import model from './model'
import _paymentPopup from 'paymentPopup'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import bankList from 'asuransi-bankList'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
// import _getpayPlatform from './_getPayPlatform.js-useless'
import _calculatePaymentFee from 'calculatePaymentFee'
// import _payInsSuccess from './_payInsSuccess'
import _submitParam from "./_submitParam"
import getPayPlatform from 'getPayPlatform'
import paySubmitSuccessHandle from 'paySubmitSuccessHandle' //支付成功跳转逻辑

let reqObj = utils.getRequestParams();
let from = reqObj['from'];
let order_no = reqObj['order_no'] || '';
let paymentPopup = reqObj['paymentPopup'] || '';
let [obj, $UI] = [{}, $('body')];
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0
    // let terminal = 10;
    // let isRepeat = 1;
    // if (appVersionCode > 128) {
    //     terminal = 21
    // }
let isRefreshSubmitParam = false;
let page = 1;
let insuredItems = [];
utils.showLoading();

// 登录+订单：调起支付弹窗
if (user_id && order_no) {
    console.log('===user_id===', user_id);

    getInsOrder();
} else if (!user_id && order_no) {
    utils.jumpToLogin(location.href, 'login_1');

} else if (sessionStorage.getItem('sijicoronaSubmitParam')) {
    console.log('===session===');
    getBenefitsOfSiji()
    getAlreadyBpjs()
    getClaimProcess()
    getHealtStatement()
    getTermAgreement()
    view.init({
        data: JSON.parse(sessionStorage.getItem('sijicoronaSubmitParam'))
    });
} else {
    console.log('===!order_no + !session ==');
    getBenefitsOfSiji()
    getAlreadyBpjs()
    getClaimProcess()
    getHealtStatement()
    getTermAgreement()
    view.init({});
}

function getBenefitsOfSiji() {
    utils.showLoading();
    model.getBenefitsOfSiji({
        success: function(res) {
            utils.hideLoading();
            view.setBenefitsOfSiji(res);
        },
    })
}

function getAlreadyBpjs() {
    utils.showLoading();
    model.getAlreadyBpjs({
        success: function(res) {
            utils.hideLoading();
            view.setAlreadyBpjs(res);
        },
    })
}

function getClaimProcess() {
    utils.showLoading();
    model.getClaimProcess({
        success: function(res) {
            utils.hideLoading();
            view.setClaimProcess(res);
        },
    })
}

function getHealtStatement() {
    utils.showLoading();
    model.getHealtStatement({
        success: function(res) {
            utils.hideLoading();
            view.setHealtStatement(res);
        },
    })
}

function getTermAgreement() {
    utils.showLoading();
    model.getTermAgreement({
        success: function(res) {
            utils.hideLoading();
            view.setTermAgreement(res);
        },
    })
}

// $UI.on('getSpuSaleNum', function() {
getSpuSaleNum(); // spu销售数量
// })

// reset commodity_item in localstorage
localStorage.removeItem("commodityItem");
localStorage.removeItem("dataSubmitAsuransi");

// reset benifitInfo in localstorage
localStorage.removeItem("benifitInfo");

// payment channel
model.getPayment({
    param: {
        bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
    },
    success: function(rs) {
        if (rs.code == 0) {
            console.log('获取银行列表===', rs)
                // handle image
            for (let i = 0; i < rs.data.length; i++) {
                if (rs.data[i].image) {
                    rs.data[i].image = JSON.parse(rs.data[i].image);
                }
            }
            bankList.init(rs); //初始化银行列表
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
})

/****
 * get:order
 *{
     "order_no": "",
     "commodity_items": [{
         //siji-COVID-19-A  siji-COVID-19-B siji-COVID-19-C siji-COVID-19-D
         "sku_key": "siji-COVID-19-A",
         "sku_number": 1,
         "is_print": false
     }],
     "policyholder_user": {
         "birth_date": "1990-04-02",
         "full_name": "",
         "mobile_number": ""
     },
     "terminal": 0,
     "insured_user": {
         "relationship": 0,
         "birth_date": "1990-04-02",
         "gender": 0
     }
 }

 */
function getInsOrder() {
    model.getInsOrder({
        param: {
            order_no: order_no
        },
        success: function(res) {
            if (res.code == 0 && res.data) {
                // handle yyyy/mm/dd
                if (res.data.insured_user && res.data.insured_user.birth_date ||
                    res.data.policyholder_user && res.data.policyholder_user.birth_date) {

                    let _arr = res.data.insured_user.birth_date.split('-');
                    res.data.insured_user.yyyy = _arr[0];
                    res.data.insured_user.mm = _arr[1];
                    res.data.insured_user.dd = _arr[2];

                    let _arr1 = res.data.policyholder_user.birth_date.split('-');
                    res.data.policyholder_user.yyyy = _arr1[0];
                    res.data.policyholder_user.mm = _arr1[1];
                    res.data.policyholder_user.dd = _arr1[2];
                }
                view.init(res);

                if (paymentPopup) {
                    paymentPopupFun(res);
                }

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

// payment channel
// function getPayment() {
//     model.getPayment({
//         param: {
//             bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
//         },
//         success: function(rs) {
//             if (rs.code == 0) {
//                 console.log('获取银行列表===', rs)
//                     // handle image
//                 for (let i = 0; i < rs.data.length; i++) {
//                     if (rs.data[i].image) {
//                         rs.data[i].image = JSON.parse(rs.data[i].image);
//                     }
//                 }
//                 bankList.init(rs); //初始化银行列表
//             } else {
//                 utils.alertMessage(rs.msg)
//             }
//         },
//         error: utils.handleFail
//     })
// }


$UI.on('fetchInsuranceData', function() {
    fetchInsuranceData();
});

function fetchInsuranceData() {
    utils.showLoading();
    console.log("===call fetchInsuranceData===");
    model.getInsuranceData({
        param: {
            page: page,
        },
        success: function(rs) {
            if (rs.code == 0 && !rs.data && page == 1) {
                // jump to sijicorona detail
                rs.data = [];
                view.insertInsOrderData(rs);

            } else if (rs.code == 0) {
                // jika masih ada datanya maka panggil lagi api ini page++
                if (rs.data) {
                    for (let i = 0; i < rs.data.length; i++) {
                        // jika belum bayar maka push
                        // policy_state 1 = proses isi data
                        // policy_state 2 = data telah terisi
                        // policy_state 40 = sudah bayar (effective)
                        if (Number(rs.data[i].policy_state) != 40) {
                            insuredItems.push(rs.data[i]);
                        }
                    }
                    page++;
                    fetchInsuranceData();
                }
                // kalo sudah tidak ada datanya maka stop dan tampilkan
                else {
                    for (let i = 0; i < insuredItems.length; i++) {
                        insuredItems[i].payable_amount = changeMoneyFormat.moneyFormat(insuredItems[i].payable_amount)
                        insuredItems[i].sum_order_amount = changeMoneyFormat.moneyFormat(insuredItems[i].sum_order_amount)
                    }
                    rs.data = insuredItems;
                    view.insertInsOrderData(rs);
                }

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}

// 数据更新
$UI.on('refreshSubmitParam', function() {
    isRefreshSubmitParam = true;
    sessionStorage.setItem('sijicoronaSubmitParam', JSON.stringify(_submitParam.init()));
});


$UI.on('validIsRepeat', function(e, submitParam) {
    validIsRepeat(submitParam); // 检验被保人是否重复
});

function validIsRepeat(submitParam) {
    console.log('validIsRepeat', submitParam)
    model.isRepeat({
        param: {
            relationship: submitParam.insured_user.relationship,
            birth_date: submitParam.insured_user.birth_date,
            gender: submitParam.insured_user.gender,
            // spu_key: submitParam.commodity_items[0].sku_key,
        },
        success: function(rs) {
            if (rs.code == 0) {
                if (rs.data.is_repeats) {
                    // if (isRepeat == 1) {
                    //     utils.alertMessage(rs.data.toast);
                    //     isRepeat = 2
                    // } else {
                    postInsOrder(submitParam)
                    // }
                } else {
                    postInsOrder(submitParam)
                }
            } else {

            }
        },
        error: utils.handleFail
    })
}

// spu销售数量
function getSpuSaleNum() {
    model.getSpuSaleNum({
        param: {
            spu_key: 'siji-COVID-19'
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('===getSpuSaleNum===', rs)
                view.changeDonorsNum(changeMoneyFormat.moneyFormat(rs.data.count));
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}


/*******
 * submit:order
 *
 * JSON.parse(sessionStorage.getItem('sijicoronaSubmitParam'))

 admin_fee: 0
 order_no: "14221633053369616061"
 printing_fee: 0
 sum_product_amount: 367000

 Premium 用户选择的保障计划的对应保费
 Printing Fee 如果用户选择了打印保单， 则加上打印费， 如果没有选择则打印费为 0
 Admin Fee 本保险管理费为 0
 Payment Channel Fee根据总金额计算出该支付的渠道费
 =
 Premium Amount 最终金额


 */
// JSON.stringify(_submitParam.init())
// $UI.on('postInsOrder', function (e, submitParam) {
function postInsOrder(submitParam) {
    console.log('===isRefreshSubmitParam===', isRefreshSubmitParam);
    localStorage.setItem('commodityItem', JSON.stringify(submitParam.commodity_items));
    localStorage.setItem('dataSubmitAsuransi', JSON.stringify(submitParam));
    if (isRefreshSubmitParam) {
        location.href = "/holderInfo.html";
    } else {
        utils.hideLoading();

        $('.paymentPopup-wrapper').css('display', 'block');
    }
}

// })

function paymentPopupFun(res) {
    console.log("res payment popup", res);
    res.data.preTotal = res.data.admin_fee + res.data.printing_fee + res.data.sum_product_amount
    res.data._preTotal = changeMoneyFormat.moneyFormat(res.data.preTotal || 0);
    res.data._sum_product_amount = changeMoneyFormat.moneyFormat(res.data.sum_product_amount || 0);
    res.data._printing_fee = changeMoneyFormat.moneyFormat(res.data.printing_fee || 0);
    res.data._admin_fee = changeMoneyFormat.moneyFormat(res.data.admin_fee || 0);

    _paymentPopup.init(res); // payment popup
}

/****
 * pay:
 *
 sum_payable_amount Y float 实际支付金额（ 包含通道费）
 sum_actual_paid_amount Y float 实际支付金额（ 包含通道费） '实际支付总金额，根据是否分期而定，如果是全额则实际支付=sum_payable_amount如果是分期则实际支付=首期支付金额。'
 */
$UI.on('payIns', function(e) {
    let _param = {
        "order_no": order_no,
        // "ovo_phone": "", //OVO支付存储信息跳转OVO手机号填写页面
        "sum_payable_amount": parseFloat($('.totle-money').attr('data-total')),
        "sum_actual_paid_amount": parseFloat($('.totle-money').attr('data-total')),
        "sum_transfer_money": parseFloat($('.PaymentChannelFee').attr('data-money')), //通道费float
        "pay_platform": getPayPlatform($(".channel.selected .bank-name").attr('data-bankCode')), //支付渠道
        "payment_channel_id": $(".channel.selected .bank-name").attr('data-bankCode'), //支付渠道号
        "payment_channel_name": $(".channel.selected .bank-name").text(), //支付渠道名字
        "terminal": utils.terminal, //终端
    }

    // OVO: jump to OVO.html ,need phone number
    if ($(".channel.selected .bank-name").attr('data-bankCode') == 4) {
        console.log('===OVO===');

        sessionStorage.setItem('payIns-OVO', JSON.stringify(_param))
        location.href = '/OVO.html?order_id=' + order_no + '&amount=' + parseFloat($('.totle-money').attr('data-total'))

    } else {
        payIns(_param);
    }

});

function payIns(_param) {
    model.payIns({
        param: _param,
        success: function(res) {
            if (res.code == 0) {
                console.log('===insPay===', res);

                // _payInsSuccess.init(_param, res);
                paySubmitSuccessHandle(_param, res);

            } else {
                utils.hideLoading();
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}

/****
 * pay: OVO
 */
$UI.on('payIns-OVO', function() {
    let _param = JSON.stringify(sessionStorage.getItem('payIns-OVO'));
})

// 安卓内调支付
$UI.on('appHandle_createPayment', function(e, objData) {
    payIns(objData);
});

/*****
 * chosed bank : calculate
 */
$UI.on('calculationFormula', function(e, rs) {
    let param = _calculatePaymentFee.init({
        bankExpenseType: $(".channel.selected .bank-name").attr('data-expensetype'),
        bankChannelFee: parseFloat($(".channel.selected .bank-name").attr('data-methodfee')),
        preTotal: parseFloat($('.totle-money').attr('data-preTotal')),
    })

    // payment channel fee
    $('.PaymentChannelFee').attr('data-money', param.toalPaymentChannelFee)
    $('.PaymentChannelFee').html(changeMoneyFormat.moneyFormat(param.toalPaymentChannelFee))

    // totle-money
    $('.totle-money').attr('data-total', param.total)
    $('.totle-money').html(changeMoneyFormat.moneyFormat(param.total))


})

/*
监听分享成功回调
*/
$UI.on('sijicoronaShareSuccess', function(e, target_type) {
    console.log('===sijicoronaShareSuccess===')

    // share_type 1: project 2: group 3: GTRY
    // model.projectShare({
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: "999999999",
            item_type: 0,
            scene: 1,
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});

window.appHandle = function(req_code) {
    // if (req_code == 600) {
    location.href = location.reload();
    // }
}