/*
 * 校验年龄信息
 */
import utils from 'utils'
let obj = {};

obj.init = function (param, lang) {

    let insuredTotalM;
    let holderTotalM;

    let now = new Date();
    let nowY = now.getFullYear();
    let nowM = now.getMonth() + 1;
    let nowD = now.getDate();

    let insuredM = parseInt(param.insured_user.mm)
    let insuredD = parseInt(param.insured_user.dd)
    let insuredAge = parseInt(nowY - param.insured_user.yyyy)

    let holderM = parseInt(param.policyholder_user.mm)
    let holderD = parseInt(param.policyholder_user.dd)
    let holderAge = parseInt(nowY - param.policyholder_user.yyyy)

    let validI;
    let validH;
    insuredTotalM = getTotalM(nowM, nowD, insuredM, insuredD, insuredAge);
    validI = validInsM(insuredTotalM, lang);

    holderTotalM = getTotalM(nowM, nowD, holderM, holderD, holderAge);
    validH = validHolderM(holderTotalM, lang);

    return [validI, validH]


    // let validI;
    // let validH;
    // [insuredTotalM1,insuredTotalM2] = getTotalM(nowM, nowD, insuredM, insuredD, insuredAge);
    // validI = validInsM(insuredTotalM1,insuredTotalM2, lang);

    // [holderTotalM1,holderTotalM2] = getTotalM(nowM, nowD, holderM, holderD, holderAge);
    // validH = validHolderM(holderTotalM1,holderTotalM2, lang);

    // return [validI, validH]
}

function getTotalM(nowM, nowD, birthM, birthD, age) {
    console.log(nowM, birthM, nowD, birthD, age)
    let totalM = age * 12;

    if (nowM > birthM) {

        if (nowD < birthD) {
            totalM = totalM + (nowM - 1 - birthM)
        } else {
            totalM = totalM + (nowM - birthM)
        }

    } else if (nowM == birthM) {

        if (nowD < birthD) {
            totalM = totalM - 12 + (12 - birthM) + nowM - 1
        } else {
            totalM = age * 12
        }

    } else {

        if (nowD < birthD) {
            console.log('nowD < birthD',totalM)
            totalM = totalM - 12 + (12 - birthM + nowM) - 1
        } else {
            console.log('nowD >= birthD',totalM)
            totalM = totalM - 12 + (12 - birthM + nowM)
        }

    }

    console.log('totalM', totalM)

    return totalM
}

// function getTotalM(nowM, nowD, birthM, birthD, age) {
// console.log(nowM, nowD, birthM, birthD, age)

// let totalM1 = age * 12;
// let totalM2 = age * 12;

// if (nowM > birthM) {
//     console.log('nowM > birthM',nowM,birthM)

//     if (nowD < birthD) {
//         totalM1 = totalM1 + (nowM - birthM) + 1;
//         console.log('totalM1---',totalM1)
//     } else {
//         totalM1 = totalM1 + (nowM - birthM)
//         console.log('totalM===',nowM,birthM,totalM1)
//     }

// } else if (nowM == birthM) {
//     console.log('nowM = birthM',nowM,birthM)

//     if (nowD < birthD) {
//         totalM1 = totalM1 - 12 + (12 - birthM) + nowM - 1
//     } else {
//         totalM1 = totalM1
//     }

// } else {
//     console.log('nowM < birthM',nowM,birthM)

//     if (nowD > birthD) {
//         totalM1 = totalM1 - 12 + (12 - birthM + nowM) + 1
//         console.log('totalM1---',totalM1)

//     } else {
//         totalM1 = totalM1 - 12 + (12 - birthM + nowM)
//         console.log('totalM1===',totalM1)

//     }

// }

// console.log('totalM', totalM1,totalM2)

// return [totalM1,totalM1]
// }

// 校验被保人年龄
function validInsM(insuredTotalM, lang) {
    console.log('validInsM', insuredTotalM)
    if (insuredTotalM < 6 || insuredTotalM > 773) {
        utils.alertMessage(lang.lang_check1)
        return false;
    }
    return true;
}

// 校验投保人年龄
function validHolderM(holderTotalM, lang) {
    console.log('validHolderM', holderTotalM)
    if (holderTotalM < 210 || holderTotalM > 773) {
        utils.alertMessage(lang.lang_check4)
        return false
    }
    return true;
}

export default obj;