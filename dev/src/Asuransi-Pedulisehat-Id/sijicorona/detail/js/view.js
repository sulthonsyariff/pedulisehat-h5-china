// 公共库
import nav from 'nav'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import domainName from 'domainName'
import clickHandle from './_clickHandle'
import utils from "utils"
import _submitParam from "./_submitParam"
import _unpaidInsurance from '../tpl/_unpaidInsurance.juicer' //unpaid insurance
import _insuranceOrderItem from '../tpl/_unpaidInsuranceData.juicer'

/* translation */
import asuransiDetail from 'asuransiDetail'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let lang = qscLang.init(asuransiDetail);
let resGlobal;
/* translation */

let [obj, $UI] = [{}, $('body')];

fastclick.attach(document.body);

// 初始化
obj.init = function(res) {
    console.log('===view init===', res);
    resGlobal = res;

    res.domainName = domainName;
    res.lang = lang;

    res.getLanguage = utils.getLanguage();
    console.log('getLanguage', res.getLanguage)

    res.JumpName = lang.JumpName;
    $('title').html(lang.JumpName);
    res.commonNavGoToWhere = utils.judgeDomain() == 'qa' ? 'https://qa.pedulisehat.id' : utils.judgeDomain() == 'pre' ? "https://pre.pedulisehat.id" : 'https://www.pedulisehat.id'
    res.isAndroid = utils.browserVersion.android;
    $UI.append(mainTpl(res)); //主模版
    // $UI.trigger('getSpuSaleNum');

    if (!utils.browserVersion.android) {
        nav.init(res);
    } else {
        location.href = 'native://do.something/setTitle?title=' + lang.JumpName;
    }

    utils.hideLoading(); // 隐藏loading
    clickHandle.init(res);
    controlUI();


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

};

obj.changeDonorsNum = function(num) {
    $('.donors-num').html(num);
}

// pay
$UI.on('click', '#payAsuransi', function() {
    location.href = "/benifitInfo.html?policy_id=" + $(this).attr('data-order_no') + "&paymentPopup=true" + "&policy_state=" + $(this).attr('data-policy_state');
})

// complete data
$UI.on('click', '.complete-data', function() {
    localStorage.removeItem('holderInfo');
    location.href = "/holderInfo.html?policy_id=" + $(this).attr('data-order_no');
})

/****
 * submit param change
 */
$UI.on('focusout', '.insured_user input,.policy_holder input', function() {
    $UI.trigger('refreshSubmitParam')
})

function controlUI() {
    // control bottom button show or hide
    $(window).scroll(function() {
        if ($(window).scrollTop() > $('.box1 .join').offset().top) {
            $('.bottom-wrapper').css('display', 'block');
        } else {
            $('.bottom-wrapper').css('display', 'none');
        }
    })

    // 修改home悬浮按钮的UI
    if ($('.app-download ').css('display') == 'block') {
        $('.home-btn').css('top', '113px');
    }

    $UI.on('click', '.appDownload-close', function(e) {
        $('.home-btn').css('top', '70px');
    });
}

obj.insertInsOrderData = function(rs) {
    rs.lang = lang;

    utils.hideLoading();

    console.log(rs.data);
    if (rs.data.length > 0) {
        // open box unpaid insurance
        console.log('===unpaidInsurance===', rs);        
        $('.unpaidInsurance-wrapper').css('display', 'block');                
        $UI.append(_unpaidInsurance(resGlobal));

        for (let index = 0; index < rs.data.length; index++) {
            let data = {
                index: index + 1,
                data: null,
                lang: rs.lang
            };
            data.data = rs.data[index];
            $('.insurance-order-cont').append(_insuranceOrderItem(data));
        }
    } else {
        $UI.trigger('validIsRepeat', _submitParam.init());
    }
}

obj.setBenefitsOfSiji = function(res){
    $('.benefit-content').html(res.description);
}

obj.setAlreadyBpjs = function(res){
    $('.bpjs-content').html(res.description);
}

obj.setClaimProcess = function(res){
    $('.claim-content').html(res.description);
}

obj.setHealtStatement = function(res){
    clickHandle.setHealtStatement(res)
}

obj.setTermAgreement = function(res){
    clickHandle.setTermAgreement(res)
}

export default obj;