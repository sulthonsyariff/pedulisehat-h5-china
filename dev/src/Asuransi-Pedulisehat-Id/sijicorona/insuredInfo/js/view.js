// 公共库
import nav from 'nav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import locationChoose from 'locationChoose'
import relationList from './_relationList'

// import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import validate from './_validate' // validate the form

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils'
// // import sensorsActive from 'sensorsActive'

/* translation */
import asuransiPolicy from 'asuransiPolicy'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(asuransiPolicy);
/* translation */

/* storage key */
let CACHED_KEY = 'insuredInfo';
let getLanguage = utils.getLanguage();
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : '';

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
// let product_type = parseInt(reqObj['product_type']);
let policy_id = reqObj['policy_id'];
let policy_state = reqObj['policy_state'];
let order_sub_id = reqObj['order_sub_id'];
let p_holder_id = reqObj['p_holder_id'];
let relationship = reqObj['relationship'];
let dataSubmitAsuransi = localStorage.getItem("dataSubmitAsuransi");

// 初始化
obj.init = function(res) {
    console.log('resnya = ', res);
    if (res.data.full_name == null || res.data.full_name == "") {
        datas = '';
    } else {
        datas = res.data;
    }

    console.log('jadi datas = ', datas);
    if (!datas) {
        if (dataSubmitAsuransi) {
            datas = JSON.parse(dataSubmitAsuransi).insured_user;
        }
    }

    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang1
    }


    if (!utils.browserVersion.androids) {
        res.JumpName = lang.JumpName;
        nav.init(res);
    }

    if (datas) {
        datas.lang = lang;
        datas.getLanguage = getLanguage;
        datas.asuransiPolicy_step = 2;
        // datas.relationship = relationship;

    } else {
        // res.data = {}
        res.data.lang = lang;
        // res.data.relationship = relationship;
        res.data.asuransiPolicy_step = 2;

    }




    console.log('datas || res', datas || res.data)





    $UI.append(mainTpl(datas || res.data)); //主模版
    // $UI.append(alertBoxTpl(res)); //验证弹窗

    locationChoose.init(datas || res.data);
    relationList.init(datas || res.data); //初始化关系列表

    utils.inputFourInOne("#KTP"); //限制4位一格


    // 隐藏loading
    utils.hideLoading();

    commonFooter.init(datas || res.data);



    // setupUIHandler(); //Main Js

    fastclick.attach(document.body);
    cliclHandle(datas || res.data);
    // // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {

    // relation channel
    $('body').on('click', '.relation_channel_wrap', function(rs) {


        $('.alert-relation').css('display', 'block');
        $('.relation-list-items').removeClass('choosed-relation');

        if ($(this).find('.relation_default')) {
            let dataRelation = $(this).find('.relation_default').attr('data-relation');
            console.log('dataRelation', dataRelation)
            $('.alert-relation').find(".relation-name[data-relation='" + dataRelation + "'] ").parents('.relation-list-items').addClass('choosed-relation');
        }
    })

    // 输入框监听，本地存储数据
    $("input, select, textarea").on("input selected", function(e) {
        updateCache();
    });

    // 选择地区更新
    $('body').on('click', '.list-items', function() {
            updateCache();
        })
        //选择关系更新
    $('body').on('click', '.relation-list-items', function(e) {
        updateCache();
    })

    // got it
    $('body').on('click', '.confirm', function() {
        $('.alert-box').css('display', 'none');
    });

    // holder
    $('body').on('click', '.holder-info', function () {
        if (policy_state == "2" || policy_state == "11" || policy_state == "12") {
            location.href="/holderInfo.html?policy_id=" + policy_id + "&policy_state=" + policy_state;
        }
    })
    
    // benefit
    $('body').on('click', '.benefit-info', function () {
        if (policy_state == "2" || policy_state == "11" || policy_state == "12") {
            location.href="/benifitInfo.html?policy_id=" + policy_id + "&policy_state=" + policy_state;
        }
    })    

    // submit
    $('.next-btn').on('click', function() {
        let submitData = getSubmitParam();
        updateCache();
        console.log('submitData', submitData)
        store.set(CACHED_KEY, getSubmitParam());

        if (validate.check(submitData, lang)) {
            // $('.submitBtn').addClass('disabled');
            localStorage.setItem("insured-name", $('#name').val());
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang44);
        }
    });
}


/**
 * //Update local cache
 */
function updateCache() {
    console.log('updateCache:', store.get(CACHED_KEY));
    store.set(CACHED_KEY, getSubmitParam());
}


function getSubmitParam() {

    let province_name = $('.address-content').attr('province-name') || $('.region-choose-wrap').attr('province-name');
    let city_name = $('.address-content').attr('city-name') || $('.region-choose-wrap').attr('city-name');

    province_name = province_name || ''
    city_name = city_name || ''

    return {
        policy_id: policy_id,
        id_card: $('#KTP').val().replace(/\s|\xA0/g, ""),
        email: $('#email').val(),
        mobile_number: $('#phone').val(),
        address: $('#full-address').val(),
        full_name: $('#name').val(),
        province_name: province_name,
        city_name: city_name,
        postal_code: $('#zip-code').val(),
        relationship: parseInt($(".relation_default").attr('data-relation')),
        // order_sub_id: order_sub_id,
        // p_holder_id: p_holder_id,        
    }
}
export default obj;