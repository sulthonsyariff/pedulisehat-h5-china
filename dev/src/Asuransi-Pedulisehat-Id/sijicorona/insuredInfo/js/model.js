import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;



//地区列表
obj.locationList = function (o) {
    var url = domainName.base + '/v1/regions?pid=0';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};


/**
 * 被保人详情
 */
obj.getInsuredDetail = function (o) {
    let url = domainName.psuic + '/v1/ins_policy/ins_person?policy_id=' + o.param.policy_id;

    if (isLocal) {
        url = 'mock/INSURANCE/insurance.json?id_card=' + o.param;
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


obj.getHolderInfo = function (o) {
    let url = domainName.psuic + '/v1/ins_policy/holder?policy_id=' + o.param.policy_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

//heo apply for aid step 1
obj.submitInsuranceInfo = function (o) {
    var url = domainName.psuic + '/v1/ins_policy/ins_person';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};
export default obj;