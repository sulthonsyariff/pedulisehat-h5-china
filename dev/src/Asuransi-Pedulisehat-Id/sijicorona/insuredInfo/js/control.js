import view from './view'
import model from './model'
import locationChoose from 'locationChoose'
import store from 'store'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();
let policy_id = reqObj['policy_id'];
let order_sub_id = reqObj['order_sub_id'];
// view.init({});

model.getInsuredDetail({
    param: {
        policy_id
    },
    success: function (rs) {
        if (rs.code == 0) {
            console.log('code==0')
            view.init(rs);
            locationList();
        } else {
            utils.alertMessage(rs.msg);
        }
    },
    error: function (err) {
        locationList();
        let data = {
            JumpName: "",
            data: {
                full_name: null,
                lang: ""
            }
        }
        view.init(data);
        // utils.alertMessage(rs.msg);
    }
});

UI.on('copyHolderData', function (e, params) {
    model.getHolderInfo({
        param: {
            policy_id
        },
        success: function (rs) {
            if (rs.code == 0) {
                console.log('copyHolderData')
                view.init(rs);
                locationList();
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: function (err) {
            locationList();
            let data = {
                JumpName: "",
                data: {
                    full_name: null,
                    lang: ""
                }
            }
            view.init(data);
            // utils.alertMessage(rs.msg);
        }
    });
});

UI.on('resetForm', function (e, params) {
    console.log("reset-form");
    let resetForm = {
        data: {
            full_name: ""
        }
    }
    view.init(resetForm);
});

function locationList(){
    model.locationList({
        success: function (rs) {
            if (rs.code == 0) {
                // sort ascending name
                let locationTemp;
                locationTemp = rs.data.sort((a, b) => {
                    if (a.name < b.name)
                      return -1;
                    if (a.name > b.name)
                      return 1;
                    return 0;
                });
                rs.data = locationTemp;
    
                locationChoose.renderList(rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}

UI.on('submit', function (e, params) {
    model.submitInsuranceInfo({
        param: params,
        success: function (res) {
            if (res.code == 0) {
                store.remove('insuredInfo');
                location.href = '/benifitInfo.html?policy_id=' + policy_id + '&p_ins_id=' + res.data.p_ins_id;
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})