import view from './view'
import model from './model'
import store from 'store'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import domainName from 'domainName' // port domain name
import _paymentPopup from 'paymentPopup'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import bankList from 'asuransi-bankList'
import paySubmitSuccessHandle from 'paySubmitSuccessHandle' //支付成功跳转逻辑
import getPayPlatform from 'getPayPlatform'
import _calculatePaymentFee from 'calculatePaymentFee'

// 隐藏loading
// utils.showLoading();
let UI = view.UI;
let reqObj = utils.getRequestParams();

let policy_id = reqObj['policy_id'];
let p_ins_id = reqObj['p_ins_id'];
let from = reqObj['from'];
let paymentPopup = reqObj['paymentPopup'];
let order_no;
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';



utils.hideLoading();


// init
if (paymentPopup) {
    submitDataAsm();
}

model.getBeneficiary({
    param: {
        policy_id,
        p_ins_id
    },
    success: function(rs) {
        if (rs.code == 0) {
            view.init(rs);

        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});

// payment channel
model.getPayment({
    param: {
        bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
    },
    success: function(rs) {
        if (rs.code == 0) {
            console.log('获取银行列表===', rs)
                // handle image
            for (let i = 0; i < rs.data.length; i++) {
                if (rs.data[i].image) {
                    rs.data[i].image = JSON.parse(rs.data[i].image);
                }
            }
            bankList.init(rs); //初始化银行列表
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
})

function paymentPopupFun(res) {
    console.log("res payment popup", res);
    res.data.preTotal = res.data.admin_fee + res.data.printing_fee + res.data.sum_product_amount
    res.data._preTotal = changeMoneyFormat.moneyFormat(res.data.preTotal || 0);
    res.data._sum_product_amount = changeMoneyFormat.moneyFormat(res.data.sum_product_amount || 0);
    res.data._printing_fee = changeMoneyFormat.moneyFormat(res.data.printing_fee || 0);
    res.data._admin_fee = changeMoneyFormat.moneyFormat(res.data.admin_fee || 0);
    res.data._access = 'asuransi';

    _paymentPopup.init(res); // payment popup
    $('.payment').css('display', 'none');
}

function getInsOrder() {
    model.getInsOrder({
        param: {
            terminal: utils.terminal,
            policy_id: policy_id
        },
        success: function(res) {
            if (res.code == 0 && res.data) {
                order_no = res.data.order_no;
                
                // if status paid redirect to success page
                if (res.data.paid) {
                    location.href = res.data.redirect_url;
                } else {
                    paymentPopupFun(res);
                }
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

function submitDataAsm() {
    model.submitDataAsm({
        param: {
            policy_id: policy_id
        },
        success: function(res) {
            if (res.code == 0 && res.data) {
                getInsOrder();
            } else {
                localStorage.setItem("message-underwriting", res.msg);
                let urlStr = location.host;
                if (urlStr.indexOf("pre.pedulisehat.id") != -1) {
                    location.href = "https://asuransi-pre.pedulisehat.id/siji/underwritingfailed";
                } else {
                    location.href = "https://asuransi.pedulisehat.id/siji/underwritingfailed";
                }
            }
        },
        error: function(err) {
            // underwritingfailed
            localStorage.setItem("message-underwriting", err.msg);
            let urlStr = location.host;
            if (urlStr.indexOf("pre.pedulisehat.id") != -1) {
                location.href = "https://asuransi-pre.pedulisehat.id/siji/underwritingfailed";
            } else {
                location.href = "https://asuransi.pedulisehat.id/siji/underwritingfailed";
            }
            // utils.alertMessage(err.msg)
        }
    })
}


UI.on('payIns', function(e) {
    let _param = {
        "order_no": order_no,
        // "ovo_phone": "", //OVO支付存储信息跳转OVO手机号填写页面
        "sum_payable_amount": parseFloat($('.totle-money').attr('data-pretotal')),
        "sum_actual_paid_amount": parseFloat($('.totle-money').attr('data-pretotal')),
        "sum_transfer_money": 0, //通道费float
        "pay_platform": 7, //支付渠道
        // "payment_channel_id": $(".channel.selected .bank-name").attr('data-bankCode'), //支付渠道号
        // "payment_channel_name": $(".channel.selected .bank-name").text(), //支付渠道名字
        "terminal": utils.terminal, //终端
    }

    // OVO: jump to OVO.html ,need phone number
    if ($(".channel.selected .bank-name").attr('data-bankCode') == 4) {
        console.log('===OVO===');

        sessionStorage.setItem('payIns-OVO', JSON.stringify(_param))
        location.href = '/OVO.html?order_id=' + order_no + '&amount=' + parseFloat($('.totle-money').attr('data-total'))

    } else {
        payIns(_param);
    }

});

function payIns(_param) {
    model.payIns({
        param: _param,
        success: function(res) {
            if (res.code == 0) {
                console.log('===insPay===', res);
                // paySubmitSuccessHandle(_param, res);
                location.href = JSON.parse(res.data.third_platform_data).urlCheckout
                // utils.hideLoading();
            } else {
                utils.hideLoading();
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}

UI.on('calculationFormula', function(e, rs) {
    let param = _calculatePaymentFee.init({
        bankExpenseType: $(".channel.selected .bank-name").attr('data-expensetype'),
        bankChannelFee: parseFloat($(".channel.selected .bank-name").attr('data-methodfee')),
        preTotal: parseFloat($('.totle-money').attr('data-preTotal')),
    })

    // payment channel fee
    $('.PaymentChannelFee').attr('data-money', param.toalPaymentChannelFee)
    $('.PaymentChannelFee').html(changeMoneyFormat.moneyFormat(param.toalPaymentChannelFee))

    // totle-money
    $('.totle-money').attr('data-total', param.total)
    $('.totle-money').html(changeMoneyFormat.moneyFormat(param.total))


})

UI.on('submit', function(e, params) {
    model.submitInsuranceInfo({
        param: params,
        success: function(res) {
            if (res.code == 0) {
                // store.remove('benifitInfo');

                submitDataAsm();

                // location.href = '/submitSuccess.html?order_no=' + order_no
                    // + '&order_sub_id=' + res.data.order_sub_id
            } else {
                // store.remove('benifitInfo');
                // location.href = '/submitFailed.html?order_no=' + order_no
                // utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})