/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function (param, lang) {
    // console.log('validate', 'param:', param);


    var percentSum = 0
    let insuredName = localStorage.getItem("insured-name");


    for (let i = 0; i < param.beneficiaries.length; i++) {

        if (param.beneficiaries[i].full_name == insuredName) {
            utils.alertMessage(lang.lang22_1);
            return false;            
        }

        if (!param.beneficiaries[i].full_name) {
            utils.alertMessage(lang.lang22);
            return false;
        }

        if (!param.beneficiaries[i].yyyy) {
            utils.alertMessage(lang.lang29);
            return false;
        }
        if (!param.beneficiaries[i].dd) {
            utils.alertMessage(lang.lang29);
            return false;
        }
        if (!param.beneficiaries[i].mm) {
            utils.alertMessage(lang.lang29);
            return false;
        }

        if (!param.beneficiaries[i].relationship) {
            utils.alertMessage(lang.lang30);
            return false;
        }
        if (!param.beneficiaries[i].benefit || parseInt(param.beneficiaries[i].benefit) < 1 || parseInt(param.beneficiaries[i].benefit) > 100) {
            utils.alertMessage(lang.lang31);
            return false;
        }

        percentSum += param.beneficiaries[i].benefit
    }


    if (percentSum != 100) {
        utils.alertMessage(lang.lang32);
        return false;
    }

    return true;
}

export default obj;