// 公共库
import nav from 'nav'
import commonFooter from 'commonFooter'
import bankList from 'GTRY-bankList'
import clickHandle from './_clickHandle'
import relationList from './_relationList'
import utils from 'utils'
import store from 'store'
import mainTpl from '../tpl/main.juicer'
import memberCardTpl from '../tpl/_member-card.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// // import sensorsActive from 'sensorsActive'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let order_no = reqObj['order_no'];
let order_sub_id = reqObj['order_sub_id'];
let p_ins_id = reqObj['p_ins_id'];

obj.UI = $UI;


/* storage key */
let CACHED_KEY = 'benifitInfo';
let getLanguage = utils.getLanguage();
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : '';

/* translation */
import asuransiPolicy from 'asuransiPolicy'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(asuransiPolicy);
/* translation */

// let member_cache = {}
// member_cache = datas


// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + titleLang.joinGTRY
    } else {
        res.JumpName = lang.JumpName;
        nav.init(res);
    }


    if (datas) {

        datas.lang = lang;
        // datas.addNum = datas.member_info.length;
        datas.count = 1


        for (let i = 0; i < datas.beneficiaries.length; i++) {
            datas.beneficiaries[i].addNum = 'add-' + i;

            datas.beneficiaries[i].benefit = datas.beneficiaries[i].benefit
            datas.beneficiaries[i].dd = datas.beneficiaries[i].birth_date.split('-')[2]
            datas.beneficiaries[i].mm = datas.beneficiaries[i].birth_date.split('-')[1]
            datas.beneficiaries[i].yyyy = datas.beneficiaries[i].birth_date.split('-')[0]
        }
        console.log('datas', datas.beneficiaries)

    } else {

        // res.member_info = res.data.beneficiaries
        console.log('res', res)

        if (res.data && res.data.beneficiaries) {
            for (let i = 0; i < res.data.beneficiaries.length; i++) {
                res.data.beneficiaries[i].dd = res.data.beneficiaries[i].birth_date.split('-')[2]
                res.data.beneficiaries[i].mm = res.data.beneficiaries[i].birth_date.split('-')[1]
                res.data.beneficiaries[i].yyyy = res.data.beneficiaries[i].birth_date.split('-')[0]
            }
        } else {
            res.data = {}
                // res.data.beneficiaries = {}
        }


        res.data.lang = lang;

        // res.addNum = 0;
        res.data.addNum = 'add-1';
        res.data.count = 1

    }

    $UI.append(mainTpl(datas || res.data)); //主模版

    // $('.member-card-append').append(memberCardTpl(res)); //member card
    $('.member-card-append').append(memberCardTpl(datas || res.data)); //member card
    if ($('.member-card').length < 2) {
        $('.del').css('display', 'none'); //隐藏添加按钮（新的卡片内有添加按钮）
    }
    if ($('.member-card').length < 5) {
        $('.join-btn').css('display', 'block'); //隐藏添加按钮（新的卡片内有添加按钮）
    }

    relationList.init(datas || res.data); //初始化关系列表


    clickHandle.init(datas || res.data); //点击事件
    // utils.inputFourInOne(".member-ID[data-add='add-0']"); //限制4位一格

    // phone = res.mobile;
    commonFooter.init(res);
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // // sensorsActive.init();
};


export default obj;