import utils from 'utils'
import store from 'store'
import model from './model'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let policy_id = reqObj['policy_id'];
let p_ins_id = reqObj['p_ins_id'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0

if (!p_ins_id) {
    model.getInsuredDetail({
        param: {
            policy_id
        },
        success: function(rs) {
            if (rs.code == 0) {
                p_ins_id = rs.data.p_ins_id;
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
    
}

obj.init = function (res) {
    let terminal = 10;
    if (appVersionCode > 128) {
        terminal = 21
    }
    return {
        beneficiaries: getMemberArr(), // 参保人
        p_ins_id: p_ins_id,
        policy_id: policy_id
    }
}

// 参保人
function getMemberArr() {
    let membersArr = [];
    let member = {};
    for (let i = 0; i < $('.member-card').length; i++) {
        member = {
            full_name: $($('.member-name')[i]).val(),
            birth_date: handleData($($('.yyyy')[i]).val()) + '-' + handleData($($('.mm')[i]).val()) + '-' + handleData($($('.dd')[i]).val()),
            relationship: parseInt($($(".relation_channel_wrap .relation_default")[i]).attr('data-relation')) || parseInt($($(".relation_channel_wrap .relation-name")[i]).attr('data-relation')),
            benefit: parseInt($($('.member-percent')[i]).val()) || '', //参加多个互助的商品ID + 当前的商品ID
            yyyy: handleData($($('.yyyy')[i]).val()),
            mm: handleData($($('.mm')[i]).val()),
            dd: handleData($($('.dd')[i]).val()),
        }
        membersArr.push(member);
    }

    return membersArr;
}

function handleData(num) {
    if (num.length == 1) {
        num = '0' + num
    }
    return num
}

export default obj;