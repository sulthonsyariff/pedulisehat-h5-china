import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name


let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


//idcard_verify
obj.getBeneficiary = function (o) {
    let url = domainName.psuic + '/v1/ins_policy/beneficiary?policy_id=' + o.param.policy_id;

    if (isLocal) {
        url = 'mock/GTRY/v1_idcard_verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

obj.getInsuredDetail = function (o) {
    let url = domainName.psuic + '/v1/ins_policy/ins_person?policy_id=' + o.param.policy_id;

    if (isLocal) {
        url = 'mock/INSURANCE/insurance.json?id_card=' + o.param;
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

//heo apply for aid step 1
obj.submitInsuranceInfo = function (o) {
    var url = domainName.psuic + '/v1/ins_policy/beneficiary';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

// dipanggil setelah submit beneficiary untuk input ke sinarmas
obj.submitDataAsm = function (o) {
    var url = domainName.psuic + '/v1/ins_policy/submit_data_siji';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

// get data for payment
obj.getInsOrder = function(o) {
    let url = domainName.psuic + '/v1/ins_order';

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//get payment
// '/v2/pay_channel'
obj.getPayment = function(o) {
    // let url = domainName.trade + '/v2/pay_channel' + (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    let url = domainName.trade + '/v1/pay_channel/pt' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    //  +
    // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    if (isLocal) {
        url = 'mock/Asuransi/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//pay !!!
obj.payIns = function(o) {
    let url = domainName.psuic + '/v1/ins_pay';

    // if (isLocal || 1) {
    //     url = 'mock/Asuransi/v1_ins_pay.json'
    // }

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}
// export default obj;
//idcard_verify
// obj.idcardVerify = function (o) {
//     let url = domainName.heouic + '/v1/idcard_verify?id_card=' + o.param.id_card + '&product_id=' + o.param.product_id;

//     if (isLocal) {
//         url = 'mock/GTRY/v1_idcard_verify.json';
//     }

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url,
//     }, o)
// };

export default obj;