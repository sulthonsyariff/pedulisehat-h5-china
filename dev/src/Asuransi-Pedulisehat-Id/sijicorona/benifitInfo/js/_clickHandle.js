import validate from './_validate'
import utils from 'utils'
import store from 'store'


import memberCardTpl from '../tpl/_member-card.juicer'
import getSubmitParam from './_getSubmitParam'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let policy_id = reqObj['policy_id'];
let policy_state = reqObj['policy_state'];

let lang;

let CACHED_KEY = 'benifitInfo';


obj.init = function (rs) {
    console.log('click',rs)
    let addNum = $('.member-card').length + 1;
    // let addNum = rs.addNum;
    let $thisCard;
    lang = rs.lang;


    // $(".member-card[data-add='add-" + 0 + "']").find('.del').css('display', 'none');



    // pay:submit
    $('body').on('click', '.next-btn-wrap', function () {
        let submitData = getSubmitParam.init(rs);
        console.log("submit data = ", submitData);
        updateCache();

        // store.set('benifitInfo', getSubmitParam.init())


        // console.log('===', submitData);

        // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
        if (validate.check(submitData, lang)) {
            // utils.showLoading(lang.lang36);
            // console.log('===submitData===', submitData);
            $UI.trigger('submit', [submitData]);
        }
    });

    // - del member card
    $('body').on('click', '.del', function () {
        let _currentCardName = $(this).parents('.member-card').find('.member-name').val() || '';
        $(this).parents('.member-card').remove();

        // addNum = addNum;

        // joinCount = addNum;

        // if (joinCount > 3) {
        //     $('.join-btn').css('display', 'none'); //隐藏添加按钮（新的卡片内有添加按钮）

        // }
        if ($('.member-card').length < 2) {
            $('.del').css('display', 'none'); //隐藏添加按钮（新的卡片内有添加按钮）
        }

        if ($('.member-card').length < 5) {
            $('.join-btn').css('display', 'block'); //隐藏添加按钮（新的卡片内有添加按钮）
        }

        

        changeNOText(); // 修改NO.1数字，
        updateCache();
    })

    // + join  member card
    $('body').on('click', '.join-btn-inner', function () {

        rs.beneficiaries = ''

        // 添加新的卡片
        addNum = addNum + 1;
        rs.addNum = 'add-' + addNum;
        console.log('addNum',addNum)
        rs.count = addNum
        // joinCount = addNum

        $('.member-card-append').append(memberCardTpl(rs)); //member card

        $('.del').css('display', 'block'); //隐藏添加按钮（新的卡片内有添加按钮）


        if ($('.member-card').length > 4) {
            $('.join-btn').css('display', 'none'); //隐藏添加按钮（新的卡片内有添加按钮）

        }
        // 修改卡片样式
        // $thisCard = $(".member-card[data-add='add-" + addNum + "']");
        // $thisCard.find('.join-btn').css('display', 'block')
        // $thisCard.find('.del').css('display', 'block')

        changeNOText(); // 修改NO.1数字，
        updateCache();

    })



    // focusout:input
    $('body').on('focusout', "input", function (e) {
        console.log('...focusout name...');
        // getSubmitParam.init();
        updateCache();
        console.log('updateCache',getSubmitParam.init())

    })

    // focusout:input
    $('body').on('click', ".relation_channel_wrap", function (e) {
        console.log('...focusout relation...');
        updateCache();
       
    })


    // relation channel
    $('body').on('click', '.relation_channel_wrap', function (rs) {
        let $data;

        // 获取当前卡片
        $data = $(this).data('add')
        $thisCard = $(".member-card[data-add='" + $data + "']");

        $('.alert-relation').attr('data-add', $data).css('display', 'block');
        $('.relation-list-items').removeClass('choosed-relation');

        if ($(this).find('.relation-name')) {
            let dataRelation = $(this).find('.relation-name').attr('data-relation');
            $('.alert-relation').find(".relation-name[data-relation='" + dataRelation + "'] ").parents('.relation-list-items').addClass('choosed-relation');
            // console.log('11===', dataRelation, $('.alert-relation').find(".relation-name[data-relation='" + dataRelation + "'] "));
        }
    })


    // got it
    $('body').on('click', '.commonBoxStyle #gotIt', function () {
        $('.commonBoxStyle').css('display', 'none');
    })

    // holder
    $('body').on('click', '.holder-info', function () {
        if (policy_state == "2" || policy_state == "11" || policy_state == "12") {
            location.href="/holderInfo.html?policy_id=" + policy_id + "&policy_state=" + policy_state;
        }
    })

    // insured
    $('body').on('click', '.insured-info', function () {
        if (policy_state == "2" || policy_state == "11" || policy_state == "12") {
            location.href="/insuredInfo.html?policy_id=" + policy_id + "&policy_state=" + policy_state;
        }
    })
    
    // // benefit
    // $('body').on('click', '.benefit-info', function () {
    //     location.href="/benefitInfo.html?policy_id=" + policy_id;
    // })    
};

// 修改NO.1数字，
// 修改计算公式，
// 修改银行渠道下的tips费率提示
function changeNOText() {
    // NO.1
    for (let i = 0; i < $('.member-card').length; i++) {
        $($('.NO')[i]).text(i + 1);
    }
}



/**
 * //Update local cache
 */
function updateCache() {
    console.log('updateCache',getSubmitParam.init())
    store.set(CACHED_KEY, getSubmitParam.init());
}


export default obj;