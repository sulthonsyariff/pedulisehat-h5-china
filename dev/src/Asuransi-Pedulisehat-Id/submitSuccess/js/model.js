import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 会员列表
 */
obj.getMemberCards = function(o) {
    let url = domainName.heouic + '/v1/member_cards?page=1&limit=3';

    if (isLocal) {
        url = '../mock/GTRY/v1_member_cards_1.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


/**
 * 活动状态
 */
obj.getActivityState = function(o) {
    let url = domainName.heouic + '/v1/hrr_state';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


// obj.projectShare = function(o) {
//     let url = domainName.project + '/v1/project_share';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }
//新分享统计接口
obj.newProjectShare = function (o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//获取分享相关内容
obj.getShareInfo = function (o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//idcard_verify
obj.getPolicyData = function(o) {
    let jokulparams = o.param.jokulfinishpayment ? '&jokulfinishpayment=true' : '';
    let url = domainName.psuic + '/v1/ins_policy?policy_id=' + o.param.policy_id + jokulparams;

    if (isLocal) {
        url = 'mock/Asuransi/v1_ins_policy.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

// obj.referralBonus = function(o) {
//     let url = domainName.project + '/v1/share_user_relation';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }


export default obj;