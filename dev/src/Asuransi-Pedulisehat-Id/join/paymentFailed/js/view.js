// 公共库
import nav from 'nav'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import utils from 'utils'

// import googleAnalytics from 'google.analytics'
import googleAnalytics from 'google.analytics'
// // import sensorsActive from 'sensorsActive'

/* translation */
import asuransiPaymentStates from 'asuransiPaymentStates'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(asuransiPaymentStates);
/* translation */

let reqObj = utils.getRequestParams();
let order_id = reqObj['order_id'];

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }

    res.JumpName = '';
    // res.commonNavGoToWhere = '/';

    if (!utils.browserVersion.androids) {
        nav.init(res);
    }
    res.lang = lang

    $UI.append(mainTpl(res)); //主模版

    fastclick.attach(document.body);

    $('body').on('click', '.viewBtn', function() {
        location.href = '/sijicorona?order_no=' + order_id + '&paymentPopup=true'

        // if (utils.judgeDomain() == 'qa') {
        //     location.href = 'https://gtry-qa.pedulisehat.id/topUp.html?member_card_id=' + res.data.member_card_id + '&product_id=' + res.data.product_id

        // } else if (utils.judgeDomain() == 'pre') {
        //     location.href = 'https://gtry-pre.pedulisehat.id/topUp.html?member_card_id=' + res.data.member_card_id + '&product_id=' + res.data.product_id

        // } else {
        //     location.href = 'https://gtry.pedulisehat.id/topUp.html?member_card_id=' + res.data.member_card_id + '&product_id=' + res.data.product_id

        // }
    })

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // // sensorsActive.init();


};

export default obj;