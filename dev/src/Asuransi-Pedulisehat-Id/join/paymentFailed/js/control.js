import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let reqObj = utils.getRequestParams();
let order_id = reqObj['order_id'];
let trade_id = reqObj['trade_id'];

// 隐藏loading
utils.hideLoading();

model.getOrderState({
    param: {
        trade_id: trade_id,
    },
    success: function(res) {
        console.log('getOrderState', res)
        if (res.code == 0) {
            view.init(res);
        } else {
            utils.alertMessage(res.msg)
        }
    }
})


/****
 * OVO 当 check payment API 发现该订单状态改为已支付的话，
 * 如果用户当前还停留在支付失败页，则支付失败页自动跳转为支付成功页
 *
 * 65S，OVO支付失败后（本来成功了）OVO会再给一个订单状态的结果，估结束页继续监听接口状态，以便跳转成功页
 *
 * OVO详情页时30秒轮询 支付失败后继续轮询>25S即可
 * 十秒后轮询接口，三秒轮询一次，轮询十次，以便全面覆盖到，也避免重复调用接口
 *
 */
setTimeout(() => {
    let _num = 1;
    // 轮询结果
    let timer = setInterval(() => {
        console.log('_num = ', _num);

        model.getOrderState({
            param: {
                trade_id: trade_id,
            },
            success: function(res) {
                console.log('getOrderState', res)
                if (res.code == 0 && res.data && res.data.state == 3) {
                    // 2 已支付
                    location.href = '/paymentSucceed.html?order_id=' + order_id
                }
            }
        })

        if (_num == 10) {
            console.log('===clearInterval===');
            clearInterval(timer);
        }

        _num++;

    }, 3000);

}, 10000);