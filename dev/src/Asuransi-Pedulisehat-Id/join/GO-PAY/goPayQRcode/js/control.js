import view from './view'
// import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import getOrderState from '../../../_orderState'

let trade_id = utils.getRequestParams().trade_id;
let order_id = utils.getRequestParams().order_id;
let joinFrom = utils.getRequestParams().joinFrom;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

utils.showLoading();

judgeOrderState();
var int = setInterval(() => {
    judgeOrderState();
}, 3000);

view.init({});

/*
 * judge order state
 1 未支付 3 已支付 4 支付失败
 */
function judgeOrderState() {
    getOrderState.init();

    // model.getOrderState({
    //     param: {
    //         trade_id: trade_id
    //     },
    //     success: function(res) {
    //         if (res.code == 0) {
    //             if (res.data.state == 3) {
    //                 location.href = '/paymentSucceed.html?order_id=' + order_id
    //             }
    //             // 支付失败
    //             else if (res.data.state == 4) {
    //                 location.href = '/paymentFailed.html?order_id=' + order_id + '&trade_id=' + trade_id;
    //             }
    //         } else {
    //             utils.alertMessage(res.msg)
    //         }
    //     },
    //     error: utils.handleFail
    // })
}