import mainTpl from '../tpl/goPay.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
// import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'
import googleAnalytics from 'google.analytics'
// // import sensorsActive from 'sensorsActive'

/* translation */
import goPay from 'goPay'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(goPay);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

var reqObj = utils.getRequestParams();
var amount = reqObj['amount'];
var trade_id = reqObj['trade_id'];
var order_id = reqObj['order_id'];
let joinFrom = reqObj['joinFrom'];

obj.init = function(rs) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }


    rs.commonNavGoToWhere = '/'
    rs.lang = lang;
    rs.JumpName = titleLang.goPay;
    if (!utils.browserVersion.androids) {
        commonNav.init(rs);
    }

    rs.domainName = domainName;

    rs.amount = changeMoneyFormat.moneyFormat(parseInt(amount));

    rs.trade_id = trade_id

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);
    fastclick.attach(document.body);

    utils.hideLoading();


    // goPay
    $('body').on('click', '.goPayBtn', function(rs) {
        $UI.trigger('goPay');

        location.href = '/goPayQRcode.html?trade_id=' + trade_id + '&order_id=' + order_id

        // if (utils.judgeDomain() == 'qa') {
        //     location.href = 'https://gtry-qa.pedulisehat.id/goPayQRcode.html?trade_id=' + trade_id + '&order_id=' + order_id
        // } else if (utils.judgeDomain() == 'pre') {
        //     location.href = 'https://gtry-pre.pedulisehat.id/goPayQRcode.html?trade_id=' + trade_id + '&order_id=' + order_id
        // } else {
        //     location.href = 'https://gtry.pedulisehat.id/goPayQRcode.html?trade_id=' + trade_id + '&order_id=' + order_id
        // }
    });


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // // sensorsActive.init();

};




export default obj;