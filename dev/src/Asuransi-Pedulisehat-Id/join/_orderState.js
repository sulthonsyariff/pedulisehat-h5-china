import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;
let reqObj = utils.getRequestParams();
let order_id = reqObj['order_id'];
let trade_id = reqObj['trade_id'];

/****
 * 1 未支付 3 已支付 4 支付失败
 */
obj.init = function(flag) {
    let url = domainName.trade + '/v1/trade/pt/' + trade_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, {
        success: function(res) {
            console.log('===getOrderState===', res.data.state)

            if (res.code == 0) {

                // 成功
                if (res.data.state == 3) {
                    location.href = '/paymentSucceed.html?order_id=' + order_id
                }
                // 支付失败
                else if (res.data.state == 4) {
                    res.data && res.data.detail && store.set('ovoErrorMessageStore', res.data.detail); //手机号有问题等等问题存在本地，失败页需要展示
                    location.href = '/paymentFailed.html?order_id=' + order_id + '&trade_id=' + trade_id;
                }
                // VA点击(未支付 1)
                else if (flag) {
                    location.href = '/paymentFailed.html?trade_id=' + trade_id + '&order_id=' + order_id
                }

            } else {
                utils.alertMessage(res.msg)
            }
        }
    })
};

export default obj;