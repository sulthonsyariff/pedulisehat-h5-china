import view from './view'
// import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'
import getOrderState from '../../../_orderState'

var $UI = view.UI;
let short_link = utils.getRequestParams().short_link;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
let CACHED_KEY = 'VA';
let VAorder = JSON.parse(sessionStorage.getItem(CACHED_KEY));
let bill_no = VAorder.NoRef
let trade_id = VAorder.trade_id
let order_id = VAorder.order_id
let joinFrom = utils.getRequestParams().joinFrom;

utils.showLoading();

judgeOrderState();
setInterval(() => {
    judgeOrderState();
}, 10000);

// 点击查询支付状态
$UI.on('sinarmas', function(e, flag) {
    judgeOrderState(flag);
})

// 1 未支付 3 已支付 4 支付失败
function judgeOrderState(flag) {
    getOrderState.init(flag);

    // model.getOrderState({
    //     param: {
    //         trade_id: trade_id,
    //     },
    //     success: function(res) {
    //         console.log('getOrderState', res)

    //         if (res.code == 0) {
    //             if (res.data.state == 3) {
    //                 location.href = '/paymentSucceed.html?order_id=' + order_id
    //             } else if (flag) {
    //                 location.href = '/paymentFailed.html?trade_id=' + trade_id + '&order_id=' + order_id
    //             }
    //         } else {
    //             utils.alertMessage(res.msg)
    //         }
    //     }
    // })
}



view.init({});