// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import howToPayTpl from '../tpl/howToPay.juicer'
import fastclick from 'fastclick'
// import googleAnalytics from 'google.analytics'
import utils from 'utils'
import store from 'store'
import changeMoneyFormat from 'changeMoneyFormat'
import html2canvas from 'html2canvas';
import googleAnalytics from 'google.analytics'
// // import sensorsActive from 'sensorsActive'
import domainName from 'domainName' // port domain name

// 复制粘贴
import ClipboardJS from 'clipboard'
new ClipboardJS('.copy-btn');
/* translation */
import sinarmas from 'sinarmas'
import qscLang from 'qscLang'
let lang = qscLang.init(sinarmas);
/* translation */

let [obj, $UI] = [{}, $('body')];
let channel = utils.getRequestParams().channel;

let CACHED_KEY = 'VA';
let VAorder = JSON.parse(sessionStorage.getItem(CACHED_KEY));
console.log('bill_no', bill_no)
let bill_no = VAorder.NoRef
let SourceID = VAorder.SourceID;
let IssueDate = VAorder.IssueDate;
let VirtualAccountNumber = VAorder.VirtualAccountNumber;
let Amount = VAorder.Amount;
let from = VAorder.from;



// let source = '$oldUA;language=${AppConstants.Http.getDeviceLan()};appVersionName=${AppUtil.getAppVersionName(view.getContext())};appVersionCode=126;asdsad'
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

// console.log('source', source)
console.log('appVersionCode', appVersionCode)

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        if (channel == '2') {
            location.href = 'native://do.something/setTitle?title=' + 'Sinarmas Virtual Account'
        } else if (channel == '707') {
            location.href = 'native://do.something/setTitle?title=' + 'Alfagroup Virtual Account'
        } else if (channel == '801') {
            location.href = 'native://do.something/setTitle?title=' + 'BNI Virtual Account'
        } else if (channel == '708') {
            location.href = 'native://do.something/setTitle?title=' + 'Danamon Virtual Account'
        } else if (channel == '802') {
            location.href = 'native://do.something/setTitle?title=' + 'Mandiri Virtual Account'
        } else if (channel == '408') {
            location.href = 'native://do.something/setTitle?title=' + 'MayBank Virtual Account'
        } else if (channel == '402') {
            location.href = 'native://do.something/setTitle?title=' + 'Permata Virtual Account'
        } else if (channel == '702') {
            location.href = 'native://do.something/setTitle?title=' + 'BCA Virtual Account'
        } else if (channel == '800') {
            location.href = 'native://do.something/setTitle?title=' + 'BRI Virtual Account'
        }
    }

    if (channel == '2') {
        res.JumpName = 'Sinarmas Virtual Account';
    } else if (channel == '707') {
        res.JumpName = 'Alfagroup Virtual Account'
    } else if (channel == '801') {
        res.JumpName = 'BNI Virtual Account'
    } else if (channel == '708') {
        res.JumpName = 'Danamon Virtual Account'
    } else if (channel == '802') {
        res.JumpName = 'Mandiri Virtual Account'
    } else if (channel == '408') {
        res.JumpName = 'MayBank Virtual Account'
    } else if (channel == '402') {
        res.JumpName = 'Permata Virtual Account'
    } else if (channel == '702') {
        res.JumpName = 'BCA Virtual Account'
    } else if (channel == '800') {
        res.JumpName = 'BRI Virtual Account'
    }
    commonNav.init(res);
    // if (gtryFrom == 'pay'){
    //     res.commonNavGoToWhere = '/join.html';
    // } else if (gtryFrom == 'topUp') {
    //     res.commonNavGoToWhere = '/topUp.html';
    // }
    res.lang = lang;
    res.getLanguage = utils.getLanguage();
    res.SourceID = SourceID;
    // res.IssueDate = IssueDate;
    // res.NoRef = NoRef;
    res.VirtualAccountNumber = VirtualAccountNumber;
    res._VirtualAccountNumber = VirtualAccountNumber.replace(/[^0-9]+/, '').replace(/^0+/, '').replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1 ");
    res.Amount = changeMoneyFormat.moneyFormat(parseInt(Amount));;
    res.channel = channel;

    console.log('====', res)

    let time = timeHandler(IssueDate);
    res.time = time;


    $UI.append(mainTpl(res)); //主模版
    $('.how-to-pay-wrap').append(howToPayTpl(res)); //添加tips

    if (appVersionCode <= 126) {
        console.log('hideBtn')
        $('.saveAsPhoto').css('display', 'none')
    }

    commonFooter.init(res);

    utils.hideLoading();
    fastclick.attach(document.body);

    $('body').on('click', '.copy-btn', function() {
        utils.alertMessage(lang.lang9);
    });

    // back-to-campaign
    $('body').on('click', '.checkTradeState', function() {
        $UI.trigger('sinarmas', 'flag'); //区分轮询和点击
    });

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // // sensorsActive.init();


    $("#captureBtn").click(function() {
        utils.showLoading(lang.lang10);

        $('.wrapper').addClass('photo'); // hide copy button

        // element in your code is a jQuery object, not an element
        html2canvas($('#capture')[0], {
            useCORS: true
        }).then(canvas => {

            // var context = canvas.getContext('2d');
            var url = canvas.toDataURL('png');

            $('.capture-img').attr('src', url);
            $('.canvas').css('display', 'block');

            setTimeout(() => {
                $('.capture-img').css({
                    'margin-left': -($('.capture-img').width() / 2),
                    'display': 'block',
                    'animation': 'screenshot 1.5s ease-in-out',
                    'animation-fill-mode': 'forwards',
                    // 'transform': 'scale(1,0.9)',
                });
                utils.hideLoading();
            }, 200);
        });
    });

    $('body').on('click', '.canvas', function() {
        $('.wrapper').removeClass('photo');
        $('.canvas').css('display', 'none');
        // $('.capture-img').css({
        //     'position': 'absolute',
        //     'transform': 'scale(1,0.9)',
        //     'height': '85vh !important',
        //     'left': '50%',
        //     'top': '0%',
        // });
    })


    $(".tab li").click(function() {
        //通过 .index()方法获取元素下标，从0开始，赋值给某个变量
        var _index = $(this).index();
        //让内容框的第 _index 个显示出来，其他的被隐藏
        $(".tab-box>div").eq(_index).show().siblings().hide();
        //改变选中时候的选项框的样式，移除其他几个选项的样式
        $(this).addClass("change").siblings().removeClass("change");
    });
    // store.remove(CACHED_KEY);
};



function timeHandler(IssueDate) {
    console.log('IssueDate', IssueDate)
        // IssueDateString = toString(IssueDate)
        // console.log('IssueDateString',IssueDateString)
    var theString = IssueDate;
    if (from == 'sinarmas') {
        arr = theString.split(" ");
        console.log('===sinarmasArr===', arr)

        var time1 = arr[1];
        hour1 = time1.split(":");
        var hour2 = hour1[0] + ":" + hour1[1]

        var time2 = arr[0]
        date1 = time2.split("/");

        var month = date1[0];
        var day = date1[1];
        var year = date1[2];

    } else if (from == 'faspay') {
        arr = theString.split(" ");
        console.log('===faspayArr===', arr)

        var time1 = arr[1];
        hour1 = time1.split(":");
        var hour2 = hour1[0] + ":" + hour1[1]

        var time2 = arr[0]
        date1 = time2.split("-");

        var day = date1[2];
        var month = date1[1];
        var year = date1[0];
    }

    if (month == 1) {
        month = "Jan";
    } else if (month == 2) {
        month = "Feb";
    } else if (month == 3) {
        month = "Mar";
    } else if (month == 4) {
        month = "Apr";
    } else if (month == 5) {
        month = "May";
    } else if (month == 6) {
        month = "June";
    } else if (month == 7) {
        month = "July";
    } else if (month == 8) {
        month = "Aug";
    } else if (month == 9) {
        month = "Sept";
    } else if (month == 10) {
        month = "Oct";
    } else if (month == 11) {
        month = "Nov";
    } else if (month == 12) {
        month = "Dec";
    }
    return hour2 + ' WIB, ' + day + " " + month + " " + year
}

export default obj;