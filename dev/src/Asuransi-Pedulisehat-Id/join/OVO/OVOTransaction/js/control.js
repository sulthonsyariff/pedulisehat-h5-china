import view from './view'
// import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'
import getOrderState from '../../../_orderState'

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let order_id = reqObj['order_id'];
let trade_id = reqObj['trade_id'];
let mobile = reqObj['mobile'];
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

/* translation */
import OVOTransaction from 'OVOTransaction'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(OVOTransaction);
/* translation */

let param = {};
param.lang = lang;
param.order_id = order_id;
param.trade_id = trade_id;
param.JumpName = titleLang.OVO;
param.mobile = mobile;

utils.showLoading();

view.init(param);

// 轮询结果
let timer = setInterval(() => {
    judgeOrderState();
}, 3000);

/*
 * judge order state
1 未支付 3 已支付 4 支付失败
 */
function judgeOrderState() {
    getOrderState.init();

    // model.getOrderState({
    //     param: {
    //         trade_id: trade_id
    //     },
    //     success: function(res) {
    //         if (res.code == 0) {
    //             // 2 已支付
    //             if (res.data.state == 3) {
    //                 location.href = '/paymentSucceed.html?order_id=' + order_id
    //             }
    //             // 支付失败
    //             else if (res.data.state == 4) {
    //                 res.data.detail && store.set('ovoErrorMessageStore', res.data.detail); //手机号有问题等等问题存在本地，失败页需要展示
    //                 location.href = '/paymentFailed.html?order_id=' + order_id + '&trade_id=' + trade_id;
    //             }
    //         } else {
    //             utils.alertMessage(res.msg)
    //         }
    //     },
    //     error: utils.handleFail
    // })
}