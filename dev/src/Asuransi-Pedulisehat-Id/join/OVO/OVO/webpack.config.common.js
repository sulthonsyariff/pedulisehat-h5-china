module.exports = {
    htmlTitle: 'OVO',
    htmlFileURL: 'html/Asuransi-Pedulisehat-Id/OVO.html',
    appDir: 'js/Asuransi-Pedulisehat-Id_OVO',
    uglify: true,
    hash: '',

    // meta分享相关
    htmlOgUrl: 'https://asuransi.pedulisehat.id/',
    htmlOgTitle: 'asuransi.pedulisehat.id - Platform Asuransi Kesehatan Indonesia',
    htmlOgDescription: 'Semakin mudah dan cepat memiliki asuransi terpercaya untuk Keluarga Indonesia',
    htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',
};