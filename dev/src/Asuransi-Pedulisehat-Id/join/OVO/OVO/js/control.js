import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let amount = reqObj['amount'];
let order_id = reqObj['order_id'];

let phone;

/* translation */
import ovo from 'ovo'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(ovo);
/* translation */

let param = {};
param.lang = lang;
param.amount = amount;
param.order_id = order_id;
// param.trade_id = trade_id;
param.JumpName = titleLang.OVO;

model.getUserInfo({
    success: function(rs) {
        phone = rs.data.mobile;
        param.phone = phone;

        // 隐藏loading
        utils.hideLoading();
        view.init(param);
    },
    unauthorizeTodo: function(rs) {
        console.log('401');
    },
    error: utils.handleFail
});

// submit
$UI.on('check_pay', function(e) {
    let _postParam = JSON.parse(sessionStorage.getItem('payIns-OVO'));
    _postParam.ovo_phone = $('#number').val();

    if (check(_postParam)) {
        model.payIns({
            param: _postParam,
            success: function(res) {
                if (res.code == 0) {
                    console.log('===insPay===', JSON.parse(res.data.third_platform_data));

                    let _data = JSON.parse(res.data.third_platform_data);

                    setTimeout(() => {
                        location.href = '/OVOTransaction.html?trade_id=' + _data.trade_id +
                            '&mobile=' + phone + '&order_id=' + _data.order_id;
                    }, 300);
                } else {
                    utils.alertMessage(rs.msg);
                }
            },
            error: utils.handleFail
        });
    }
});

// check phone number
function check(params) {
    if (params.ovo_phone.length < 7) {
        utils.alertMessage(lang.lang1);
        return false;
    }

    return true;
}