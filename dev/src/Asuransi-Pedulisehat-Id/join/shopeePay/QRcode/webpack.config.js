let baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'shopeePayQRcode',
    htmlFileURL: 'html/Asuransi-Pedulisehat-Id/shopeePayQRcode.html',

    // meta分享相关
    htmlOgUrl: 'https://asuransi.pedulisehat.id/',
    htmlOgTitle: 'asuransi.pedulisehat.id - Platform Asuransi Kesehatan Indonesia',
    htmlOgDescription: 'Semakin mudah dan cepat memiliki asuransi terpercaya untuk Keluarga Indonesia',
    htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',

    appDir: 'js/Asuransi-Pedulisehat-Id_shopeePayQRcode',
    uglify: true,
    hash: '',
    mode: 'production'
})