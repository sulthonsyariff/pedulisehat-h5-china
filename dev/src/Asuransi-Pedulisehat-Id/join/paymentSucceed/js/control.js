import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'

// 引入依赖
import utils from 'utils'
let $UI = view.UI;

/* translation */
import gtryPaySucceed from 'gtryPaySucceed'
import qscLang from 'qscLang'
let lang = qscLang.init(gtryPaySucceed);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */


let reqObj = utils.getRequestParams();
let order_id = reqObj['order_id'];
let joinFrom = reqObj['joinFrom'];

let product_id = "999999999";
let actTitle;
let actDesc;
let forwarding_default;
let forwarding_desc;
let shareUrl;

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'));
let androidOldUrlArr;
let androidOldUrl;
// 隐藏loading
utils.hideLoading();



model.getActivityState({
    success: function(rs) {
        if (rs.code == 0) {
            if (rs.data.state == 1) {
                actTitle = commonLang.lang36
                actDesc = lang.lang54
            } else {
                actTitle = commonLang.lang26
                actDesc = ''
            }
            view.init(rs.data);

        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});

model.getMemberCards({
    success: function(rs) {
        if (rs.code == 0) {
            if (rs.data.accident) {
                // product_id = rs.data.accident[0].product_id
                // getShareInfo(rs)
            } else if (rs.data.illness) {
                // product_id = rs.data.illness[0].product_id
                // getShareInfo(rs)
            }
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});

/*
监听分享成功回调
*/
$UI.on('gtryShareSuccess', function(e, target_type) {
    console.log('===gtryShareSuccess===')

    // share_type 1: project 2: group 3: GTRY
    // model.projectShare({
    model.newProjectShare({

        param: {
            target_type: target_type,
            item_id: product_id,
            item_type: 0,
            scene: 2,

            // target_type: target_type,
            // // 传 1 代表首页分享
            // project_id: '1',
            // share_type: 3
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});

window.changeShareNum = function(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: product_id,
            item_type: 0,
            scene: 2,
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}