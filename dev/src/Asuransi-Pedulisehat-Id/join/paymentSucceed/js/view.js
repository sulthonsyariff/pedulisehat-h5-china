// 公共库
import nav from 'nav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import utils from 'utils'
// import lottie from 'lottie-web'; //port domain name

// import googleAnalytics from 'google.analytics'
import googleAnalytics from 'google.analytics'
// // import sensorsActive from 'sensorsActive'
// import shareGtry from 'shareGtry'

let [obj, $UI] = [{}, $('body')];

/* translation */
import asuransiPaymentStates from 'asuransiPaymentStates'
import qscLang from 'qscLang'
let lang = qscLang.init(asuransiPaymentStates);
/* translation */


let reqObj = utils.getRequestParams();
let order_id = reqObj['order_id'];

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }

    res.JumpName = '';
    res.lang = lang
        // res.commonNavGoToWhere = '/';
    if (!utils.browserVersion.androids) {
        nav.init(res);
    }
    console.log('res=', res)
    $UI.append(mainTpl(res)); //主模版

    commonFooter.init(res);

    fastclick.attach(document.body);


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    $('body').on('click', '.viewBtn', function() {
        location.href = '/holderInfo.html?order_no=' + order_id

        // if (utils.judgeDomain() == 'qa') {
        //     location.href = 'https://asuransi-qa.pedulisehat.id/membershipCard.html'

        // } else if (utils.judgeDomain() == 'pre') {
        //     location.href = 'https://gtry-pre.pedulisehat.id/membershipCard.html'

        // } else {
        //     location.href = 'https://gtry.pedulisehat.id/membershipCard.html'

        // }
    })


};

export default obj;