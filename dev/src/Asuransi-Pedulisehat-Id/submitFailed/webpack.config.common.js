module.exports = {
    htmlTitle: 'submitFailed',
    htmlFileURL: 'html/Asuransi-Pedulisehat-Id/submitFailed.html',
    appDir: 'js/Asuransi-Pedulisehat-Id_submitFailed',
    uglify: true,
    hash: '',

    // meta分享相关
    htmlOgUrl: 'https://asuransi.pedulisehat.id/',
    htmlOgTitle: 'asuransi.pedulisehat.id - Platform Asuransi Kesehatan Indonesia',
    htmlOgDescription: 'Semakin mudah dan cepat memiliki asuransi terpercaya untuk Keluarga Indonesia',
    htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',

};