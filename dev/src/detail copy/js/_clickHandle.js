/****
 *  所有点击事件
 */
import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import 'jq_cookie' //ajax cookie

import share from 'share'

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let obj = {};
let $UI = $('body');

obj.init = function(rs) {
    fastclick.attach(document.body);
    // 详情页才有的点击的事件：
    if ($('.bottom-slide-wrapper').length) {
        // edit:detail nav
        $('body').on('click', '#edit', function(e) {
            $('#edit span').html(rs.lang.lang43);
            $('#edit').attr('id', 'close');

            $('.bottom-slide-wrapper').css({
                'display': 'block',
                'top': '57px',
                'z-index': '2001'
            })
            $('.body-mask').css('display', 'block')
            $('.navbar').css('z-index', '2001')
        });

        // detail close
        $('body').on('click', '#close,.body-mask', function(e) {
            // console.log('detail');
            $('#close span').html('Edit');
            $('#close').attr('id', 'edit');

            $('.bottom-slide-wrapper').css({
                'display': 'none',
                'top': '-35px',
                'z-index': '0'
            })
            $('.body-mask').css('display', 'none')
            $('.navbar').css('z-index', '2000')
        });
    }

    // 禁止点击
    $('body').on('click', '.no-active #story a,.no-active #Goal a,.no-Extend#Extension', function(e) {
        event.preventDefault();
        return false;
    });

    // donate
    $('body').on('click', '#donate', function(e) {
        // sensors.track('DonateButtonClick', {
        //     from: document.referrer,
        //     type: 'campaign',
        //     campaign_id: rs.data.project_id,
        //     campaign_name: rs.data.name,
        //     campaign_category: rs.data.category_id,
        //     campaigner_id: rs.data.user_id,
        //     campaigner_name: rs.data.UserName
        // })

        $('.donate').children().css({
            'display': 'block',

        })
        setTimeout(function() {
            $('.donate').children().css({
                'display': 'none',
            })
        }, 200)
        $UI.trigger('donate');
    });

    // donate popup :show
    $('body').on('click', '.donatePopup-close', function() {
        $('.donatePopup').css({
            'display': 'none',
        })
    })

    // donate popup :hide
    $('body').on('click', '.donate-popup-btn', function() {
        $('.donatePopup').css({
            'display': 'block',
        })
    })

    let follow

    // 点击关注／取消关注
    $('body').on('click', '#star', function(e) {
        if ($(this).hasClass('star-active')) {
            follow = false
                // sensors.track('Follow', {
                //     follow: follow,
                //     campaign_id: rs.data.project_id,
                //     campaign_name: rs.data.name,
                //     campaign_category: rs.data.category_id
                // })
        } else {
            follow = true
                // sensors.track('Follow', {
                //     follow: follow,
                //     campaign_id: rs.data.project_id,
                //     campaign_name: rs.data.name,
                //     campaign_category: rs.data.category_id
                // })
        }

        // console.log('star')
        $UI.trigger('following', $(this));
    });

    // 点击关闭项目
    $('body').on('click', '#yes', function(e) {
        $UI.trigger('closeProject');
    })

    // 点击更新列表
    $('body').on('click', '.more-update', function(e) {
        location.href = '/updateList.html?project_id=' + rs.data.project_id + '&short_link=' + rs.data.short_link;
    })

    // read more & pack up
    let readMoreScrollH = 0;
    $('body').on('click', '#readMore', function(e) {
        $('.story .detail').css('max-height', $('.story .detail p').height());
        // $('.story .detail').height($('.story .detail p').height());
        $('#readMore').css('display', 'none');
        $('#packUp').css('display', 'block');
        $('.fade-wrap').css('display', 'none');

        readMoreScrollH = $(document).scrollTop();
    });
    $('body').on('click', '#packUp', function(e) {
        $('.story .detail').css('max-height', '213px');
        // $('.story .detail').height($('.story .detail p').height());
        $('#packUp').css('display', 'none');
        $('#readMore').css('display', 'block');
        $('.fade-wrap').css('display', 'block');

        $(document).scrollTop(readMoreScrollH); //滚动到原始距离，方便继续阅读
    });

    console.log('rs===', rs);

    if (rs.data.stopped != 1) {
        $('body').on('click', '#end', function(e) {
            if (rs.data.stopped == 1) {

            } else {
                $('.end-campaign.alert-box').css({
                    'display': 'block'
                });

                $('#close span').html('Edit');
                $('#close').attr('id', 'edit');

                $('.bottom-slide-wrapper').css({
                    'display': 'none',
                    'top': '-35px',
                    'z-index': '0'
                })
                $('.body-mask').css('display', 'none')
                $('.navbar').css('z-index', '2000')
            }
        });
    }

    // Invite friends to confirm
    $('body').on('click', '.invite-friends-btn', function(e) {
        let paramObj = {
            item_id: rs.data.project_id,
            item_type: 1, //3 代表勋章 / 1 代表大病详情类型
            item_short_link: rs.data.short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'detail.html' //google analytics need distinguish the page name
        }

        // 安卓端分享需要项目详情，业务页面采集，组件中使用
        if (utils.browserVersion.android) {
            let paramString = "&title=" + encodeURIComponent(rs.data.name) +
                "&description" + encodeURIComponent(rs.data.story.substr(1, 100)) +
                "&image=" + (rs.data.images[0] ? encodeURIComponent(rs.data.images[0].image) : '');

            $UI.trigger('android-share', [paramString, paramObj]);

        } else {
            // 分享组件
            share.init(paramObj);
        }
    });

    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        // $('.bubble-icon').css('display', 'none');
        // sensors.track('ShareClick', {
        //     campaign_id: rs.data.project_id,
        //     campaign_name: rs.data.name,
        //     campaign_category: rs.data.category_id
        // })

        let paramObj = {
            item_id: rs.data.project_id,
            item_type: 1, //3 代表勋章 / 1 代表大病详情类型
            item_short_link: rs.data.short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'detail.html' //google analytics need distinguish the page name
        }

        console.log('===share===', paramObj);


        // 安卓端分享需要项目详情，业务页面采集，组件中使用
        if (utils.browserVersion.android) {
            let paramString = "&title=" + encodeURIComponent(rs.data.name) +
                "&description" + encodeURIComponent(rs.data.story.substr(1, 100)) +
                "&image=" + (rs.data.images[0] ? encodeURIComponent(rs.data.images[0].image) : '');

            $UI.trigger('android-share', [paramString, paramObj]);

        } else {
            // 分享组件
            share.init(paramObj);

            //获取分享是否有配捐
            // $UI.trigger('isShareCanShareDonate');

            // $('.bubble-icon').css({
            //     'display': 'none'
            // })

            // sessionStorage.setItem('detail-activity-bubble-close', true);
        }
    });

    // share
    $('body').on('click', '#shareClose', function(e) {
        $('.common-share').css({
            'display': 'none'
        });
    });

    // alert-box:yes/no
    $('body').on('click', '#yes,#no', function(e) {
        $('.end-campaign.alert-box').css({
            'display': 'none'
        });
    });

    // scroll to top
    $('body').on('click', '#top', function(e) {
        $('body,html').animate({
            scrollTop: 0
        }, 300);
    });

    // avatar-lists
    $('body').on('click', '.avatar-lists .list', function() {
        let number = $(this).attr('data-number');
        let $choosedCommentList = $(".comment-lists[data-number=" + number + "]");

        $('.avatar-lists .list').removeClass('choosed');
        $('.comment-lists').removeClass('choosed');

        $(this).addClass('choosed');
        $choosedCommentList.addClass('choosed');

        // control hide or show '...'
        if ($choosedCommentList.find('.comment p').height() > 42) {
            $choosedCommentList.find('.comment').addClass('overflow');
        }

    });

    // Confirm Campaign
    $('body').on('click', '.confirm-campaign-btn', function() {
        console.log($.cookie('passport'));
        // signed in
        if ($.cookie('passport') && JSON.parse($.cookie('passport')) && JSON.parse($.cookie('passport')).accessToken) {
            location.href = '/confirmPage.html?project_id=' + rs.data.project_id + '&short_link=' + rs.data.short_link + '&from=detail';
        }
        // no sign in
        else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/login?req_code=300' : '/login.html?qf_redirect=' + encodeURIComponent(location.href);
        }
    });

    // see more confirm campaigns
    $('body').on('click', '.confirm-people .more', function() {
        location.href = '/confirmationList.html?project_id=' + rs.data.project_id + '&short_link=' + rs.data.short_link;
    });

    // see more supporterRanking
    $('body').on('click', '.supporter-ranking .more', function() {
        location.href = '/supporterRanking.html?project_id=' + rs.data.project_id + '&short_link=' + rs.data.short_link;
    });


    // click to log in:close
    $UI.on('click', '.click-login .login-close', function(r) {
        $('.click-login').css({
            'display': 'none'
        });

        sessionStorage.setItem('detail-activity-login-close', true);

    });

    // login-small-love
    $UI.on('click', '.login-small-love', function() {
        // 避免第一次什么都没有时候两个toggleClass同时出现
        if ($('.click-login').hasClass('show') || $('.click-login').hasClass('hide')) {
            $('.click-login').toggleClass('show');
            $('.click-login').toggleClass('hide');
        } else {
            $('.click-login').toggleClass('show');
        }
    });

    // go-log-in
    $UI.on('click', '.login-btn', function() {
        location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href);
    })

    // media partner avatar
    $UI.on('click', '#mediaPartnerAvatar', function() {
        console.log($(this).data('id'));
        // location.replace('./mediaPartner.html?userId=' + $(this).data('user_id'));
        location.href = './mediaPartner.html?userId=' + $(this).data('user_id');
    })

    // test open gojek in app
    // $('body').on('click', '.title', function() {
    //     console.log('go pay')
    //     window.location = 'native://intent?url=' + encodeURIComponent('gojek://gopay/merchanttransfer?tref=1542080408150181126GCFJ&amount=200000&activity=GP:RR') + '&errorInfo=' + encodeURIComponent('test');
    // });
}

export default obj;