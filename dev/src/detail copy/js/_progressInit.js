/**
 * 环形进度条
 */
let obj = {};

obj.init = function(o) {
    // console.log('progress===', o)
        //进度条
    var target_amount = o.data.target_amount;
    var current_amount = o.data.current_amount;
    let percent = current_amount / target_amount;

    // console.log('target_amount=', target_amount);
    // console.log('current_amount=', current_amount);

    percent = (percent >= 1) ? 1 : percent;

    // console.log('percent:', percent);

    $('.circle').circleProgress({
        startAngle: -Math.PI / 2, // 起始角度
        value: percent, //百分比
        size: 50,
        lineCap: 'round',
        thickness: 2,
        fill: {
            gradient: ["#FF6100"]
        }
    }).on('circle-animation-progress', function(event, progress, stepValue) {

        $(this).find('.inner-text').html(
            // '<span class="left">'+o.lang.lang25+'</span>' +
            parseInt(100 * stepValue) + '%'
        );
    });
}

export default obj;