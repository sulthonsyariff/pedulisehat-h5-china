let obj = {};

obj.toggleFollow = function ($this) {
    //关注 + 1
    if ($($this).hasClass('star-active')) {
        $('.star-num').html(Number($('.star-num').html()) - 1);//取消关注 - 1
    //  取消关注 - 1
    } else {
        $('.star-num').html(Number($('.star-num').html()) + 1); //关注 + 1
    }

    $($this).toggleClass('star-active');
}

export default obj;
