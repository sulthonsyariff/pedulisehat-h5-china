// import model from './model'
import store from 'store'
// import utils from 'utils'

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;

obj.getAB = function() {

    let isAB;
    // let name

    if (store.get('donate-share-words-AB')) {
        isAB = store.get('donate-share-words-AB');
    } else {
        // console.log('random=', Math.floor(Math.random() * 10 + 1));
        if (Math.floor(Math.random() * 10 + 1) < 6) {
            console.log('==========A==========')
            isAB = "A"
                // name = "original"
        } else {
            console.log('==========B==========')
            isAB = "B"
                // name = "rotate_play"
        };
        store.set('donate-share-words-AB', isAB);

        // model.abFirstTest({
        //     param : {
        //         name : name
        //     },
        //     success: function(rs){
        //         if(rs.code == 0){
        //             console.log('FirstAB',isAB)
        //         }
        //     },

        // })

    }

    return isAB;
}

export default obj;