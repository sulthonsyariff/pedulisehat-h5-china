let obj = {};
// 创建时间 yangcheng
obj.userDate = function (created_at) {
  let myDate = new Date(created_at);

  let year = myDate.getFullYear();
  let month = myDate.getMonth() + 1;

  let day = myDate.getDate();

  if (month < 2) {
    month = "January";
  } else if (month < 3) {
    month = "February";
  } else if (month < 4) {
    month = "March";
  } else if (month < 5) {
    month = "April";
  } else if (month < 6) {
    month = "May";
  } else if (month < 7) {
    month = "June";
  } else if (month < 8) {
    month = "July";
  } else if (month < 9) {
    month = "August";
  } else if (month < 10) {
    month = "September";
  } else if (month < 11) {
    month = "October";
  } else if (month < 12) {
    month = "November";
  } else if (month < 13) {
    month = "December";
  }

  if (day < 10) {
    day = "0" + day;
  }
  return day + " " + month + " " + year;
};
// 已筹时间 yangchyeng
obj.raiseDay = function (created_at) {
  let t1 = new Date(created_at);
  let t2 = new Date();
  let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
  return raiseDay;
};

// 剩余时间
obj.timeLeft = function (closed_at) {
  let t1 = new Date(closed_at);
  let t2 = new Date();
  let subtract = t1.getTime() - t2.getTime();

  return subtract > 0 ?
    Math.floor(subtract / 86400000) :
    0;
};

//  更新列表时间 yangchyeng
obj.updateHours = function (created_at) {
  let t1 = new Date(created_at);
  let t2 = new Date();
  let t3 = t2.getTime() - t1.getTime();
  let t4 = t3 % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
  let updateHours = Math.floor(t4 / (3600 * 1000)); //计算小时数

  return updateHours;
};

obj.updateMinutes = function (created_at) {
  let t1 = new Date(created_at);
  let t2 = new Date();
  let t3 = t2.getTime() - t1.getTime();

  let leave1 = t3 % (24 * 3600 * 1000); //计算天数后剩余的毫秒数
  let leave2 = leave1 % (3600 * 1000); //计算小时数后剩余的毫秒数
  let updateMinutes = Math.floor(leave2 / (60 * 1000));

  return updateMinutes;
};

export default obj;