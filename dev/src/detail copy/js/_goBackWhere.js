import utils from 'utils';
import store from 'store'
let obj = {};
let isNativeAndroid = utils.browserVersion.android;

console.log('referrer', document.referrer)

obj.init = function() {
    if (utils.getRequestParams().from == 'OVOXPeduliSehat') {
        return '/OVOXPeduliSehat.html';

    }
    // else if (utils.getRequestParams().back) {
    //     return decodeURIComponent(utils.getRequestParams().back);

    // }
    else if (
        document.referrer.indexOf('followedCampaigns') != -1 ||
        // document.referrer.indexOf('supportedCampaigns') != -1 ||
        document.referrer.indexOf('donationHistory') != -1 ||
        document.referrer.indexOf('myCampaigns') != -1 ||
        document.referrer.indexOf('campaignList') != -1 ||
        document.referrer.indexOf('zakatList') != -1 ||
        document.referrer.indexOf('donateList') != -1 ||
        document.referrer.indexOf('searchResaults') != -1
        //  ||document.referrer.indexOf('mediaPartner') != -1
    ) {
        console.log('go back where = ', document.referrer);

        return document.referrer; // 上一个页面的URL: document.referrer
    } else if (isNativeAndroid) {

        return 'native://close';
    } else if (store.get('group')) {
        console.log('go back where = ', '/group/' + store.get('group'));

        return '/group/' + store.get('group')
    } else {
        console.log('go back where = ', '/');

        return '/';
    }
}

export default obj;