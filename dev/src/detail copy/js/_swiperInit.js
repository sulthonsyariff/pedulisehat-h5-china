/**
 * 轮播图初始化
 */
import Swiper from 'swiper'

let obj = {};
let loop = false;

obj.init = function(rs) {
    // console.log('swiperInit=', rs, rs.data.images.length);
    if (rs.data.images.length > '1') {
        loop = true;
    }

    //swiper初始化
    let swiper = new Swiper('.swiper-container', {
        paginationClickable: true,
        autoplay: {
            // delay: 3000,
            stopOnLastSlide: false,
            // disableOnInteraction: true,
        },
        speed: 500,
        // loop: true,
        loop: loop,
        observer: true,
        autoplayDisableOnInteraction: false,
        observeParents: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
    });
    $(".fancybox").fancybox({});
}

export default obj;