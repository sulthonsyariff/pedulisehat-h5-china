import activityTpl from '../tpl/_ngoActivity.juicer'
import utils from 'utils'
import domainName from 'domainName'; //port domain name
import changeMoneyFormat from 'changeMoneyFormat'

/* translation */
import detail from 'detail'
import qscLang from 'qscLang'
let lang = qscLang.init(detail);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

obj.init = function(res) {

    res.data[0].domainName = domainName;
    res.data[0].lang = lang;

    if (res.data && res.data[0].count != 0) {

        if (res.data[0].partner_avatar.indexOf('http') == 0) {
            res.data[0].partner_avatar = utils.imageChoose(res.data[0].partner_avatar)
        } else {
            res.data[0].partner_avatar = domainName.static + '/img/avatar/' + res.data[0].partner_avatar;
        }

        res.data[0].total = changeMoneyFormat.moneyFormat(res.data[0].total_money)
        console.log('ngoActivity', res.data[0])

        $('.insertActivity').append(activityTpl(res.data[0]))
    }
}



export default obj;