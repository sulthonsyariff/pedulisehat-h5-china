// 公共库
// import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils';
import 'freshchat.widget'

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    // res.JumpName = 'page';
    // commonNav.init(res);

    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);

    $('body').on('click', '.btn', function() {
        FreshworksWidget('show', 'launcher');

        window.FreshworksWidget('open', 'article', {
            id: 43000526225 // article ID
        });
        // utils.alertMessage('1341234');
    })


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

export default obj;