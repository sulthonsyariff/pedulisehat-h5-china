var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'blankforapp',
    htmlFileURL: 'html/blankforapp.html',
    appDir: 'js/blankforapp',
    uglify: true,
    hash: '',
    mode: 'production'
})