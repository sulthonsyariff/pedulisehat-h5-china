var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    // htmlTitle: 'home',
    htmlFileURL: 'html/OVOXPeduliSehat.html',
    appDir: 'js/OVOXPeduliSehat',
    uglify: true,
    hash: '',
    mode: 'development'
})