var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    // htmlTitle: 'homePage',
    htmlFileURL: 'html/OVOXPeduliSehat.html',
    appDir: 'js/OVOXPeduliSehat',
    uglify: true,
    hash: '',
    mode: 'production'
})