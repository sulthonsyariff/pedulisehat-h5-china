import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// obj.getUserInfo = function(o) {
//     let url = domainName.passport + '/v1/user';

//     if (isLocal) {
//         url = '../mock/userInfo.json';
//     }

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url
//     }, o, 'unauthorizeTodo')
// };

obj.getListData = function(o) {
    let url = domainName.project + '/v1/project_ovo_group?page=' + o.param.page + '&category_id=12,13';
    if (isLocal) {
        url = '/mock/project_lists_' + o.param.page + '.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;