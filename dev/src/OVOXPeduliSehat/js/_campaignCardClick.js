import ScrollLoad from './_scrollLoad' // new scroll load
import getListPageH from './_getListPageHeight'

// campaign card
$('body').on('click', 'li.campaign_box', function() {
    let scrollTop;
    let list = $(this).data('list');
    let click_page = $(this).data('page');
    let page = click_page;
    let project_id = $(this).data('project-id');
    let short_link = $(this).data('short-link');
    let currentScrollTop = $(document).scrollTop();
    // let clickTop = $(this).offset().top;
    let pageH = getListPageH.get();
    let navH = $('.nav').height();
    // let up_loadPage = Math.floor((clickTop - bannerH) / pageH); //计算出点击时，点击元素之前的实际加载的页数
    let $at_click_page = $("li[data-page=" + click_page + "][data-list=0]"); // 换取点击元素所属page第一个元素
    let clickPageOffsetTop = $at_click_page.offset().top; //获取所属页距离顶部距离

    console.log('currentScrollTop=', currentScrollTop);
    console.log('clickPageOffsetTop=', clickPageOffsetTop);
    console.log('navH=', navH);


    // 如果滚动高度没有触及第三页，则加载page1+page2+banner
    if (currentScrollTop > clickPageOffsetTop - navH || currentScrollTop == clickPageOffsetTop - navH) {
        console.log('>')

        scrollTop = currentScrollTop - clickPageOffsetTop + navH; // 计算出点击元素所在page距离窗口顶部的距离
    } else {
        console.log('<')

        page = click_page - 1;
        scrollTop = pageH - (clickPageOffsetTop - currentScrollTop) + navH;
    }

    console.log('* click_page = ', click_page, ',page = ', page, ',list = ', list, ',scrollTop = ', scrollTop);

    // set sessionStorage
    let list_number = {
        page: page,
        click_page: click_page,
        list: list,
        scrollTop: scrollTop,
    }
    sessionStorage.setItem('home-list-history', JSON.stringify(list_number));

    ScrollLoad.stop(); // 避免scroll run时，js编码时自动将存好的数据删除,故点击后立即停止滚动监听

    // jump to detail page
    location.href = short_link ? ('/' + short_link + '?from=OVOXPeduliSehat') : ('/detail.html?project_id=' + project_id);
})