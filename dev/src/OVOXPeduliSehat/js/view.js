// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import listItemTpl from '../tpl/_listItem.juicer'
import store from 'store'
import handleScrollLoad from './_handleScrollLoad'
import ScrollLoad from './_scrollLoad' // new scroll load

import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics'
import getListPageH from './_getListPageHeight'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import campaignList from 'campaignList'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(campaignList);
/* translation */

let is_first = true;
let page = 0;

let insertMethod = 'append';
let pageH = getListPageH.get();

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;

obj.init = function(rs) {
    rs.lang = lang;
    rs.JumpName = titleLang.campaignList;
    rs.commonNavGoToWhere = '/';
    commonNav.init(rs);
    $('title').html(titleLang.campaignList);

    $UI.append(mainTpl(rs)); //insert 主模版
    commonFooter.init({});

    handleScrollLoad.init(rs); //init scroll

    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

obj.insertData = function(rs, o, method) {
    insertMethod = method;
    rs.lang = lang;
    rs.domainName = domainName;

    // 通过 is_first 值来judge是否展示空白列表页
    if (rs.data && rs.data.length != 0) {
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (let i = 0; i < rs.data.length; i++) {
            // rs.data[i].p = page;
            rs.data[i].getLanguage = utils.getLanguage();

            rs.data[i].p = rs._metadata.page;
            //时间
            rs.data[i].created_at = obj.raiseDay(rs.data[i].created_at);
            // 进度条：设置基准的高度为1%，
            rs.data[i].progress = rs.data[i].current_amount / rs.data[i].target_amount * 100 + 1;
            //   图片
            rs.data[i].cover = JSON.parse(rs.data[i].cover);

            rs.data[i].rightUrl = utils.imageChoose(rs.data[i].cover)
            rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar)

            // 格式化金额
            rs.data[i].target_amount = changeMoneyFormat.moneyFormat(rs.data[i].target_amount);
            rs.data[i].current_amount = changeMoneyFormat.moneyFormat(rs.data[i].current_amount);

            // 企业用户
            rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';

            // if row's number more than 2,show the overflow class
            if ($(".inner_name" + '_' + i + '_' + rs.data[i].p).height() > 32) {
                $('.campaign_name' + '_' + i + '_' + rs.data[i].p).addClass('overflow')
            }
        }
        console.log('language', rs.data)
            // insert page
        handleScrollLoad.insertData(rs, o, insertMethod);

    } else if (is_first) {
        $('.loading.append-loading').hide();
        $('ul.project-list').append(listItemTpl(rs)); //渲染空白列表
        ScrollLoad.stop();
    } else {
        $('.loading.append-loading').hide();
        $('.campaign_box').last().attr('last', 'last'); // 给最后一条做一个标记
        ScrollLoad.run(); // 页面加载最后一条后，继续监听滚动，但不要持续加载最后一页
    }

    if (insertMethod == 'prepend' && $('.campaign_box').eq(0).data('page') != 1) {
        $('.loading.preappend-loading').show();
    }
}

obj.raiseDay = function(created_at) {
    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}

export default obj;