// 公共库
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import 'freshchat.widget'
import store from 'store'

// 引入依赖
import utils from 'utils'

let UI = view.UI;

view.init({});

// 加载更多
UI.on('needload', function(e, o, method) {
    model.getListData({
        param: {
            page: o.page
        },
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                utils.hideLoading();

                view.insertData(res, o, method);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});