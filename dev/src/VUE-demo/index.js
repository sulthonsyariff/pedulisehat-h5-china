import app from './app';
import Vue from 'vue';

// 先获取数据，再加载组建！！！
new Vue({
    el: '#myApp', //index.html中的<div id="app"><div>中的id=“app”和这里的“#app”进行挂载
    template: '<app></app>', //表示用<app></app>替换index.html里面的<div id="app"></div>
    components: {
        app //实际是App:App的省略写法，template里使用的 <App/>标签来自组件App
    },
    data: {}
})