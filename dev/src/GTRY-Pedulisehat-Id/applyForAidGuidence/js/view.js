// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import './_cityChoose'

import alertBoxTplHealthRequirement from '../tpl/_alertBoxTplHealthRequirement.juicer' //关闭项目弹窗
import alertBoxTplConventionPlan from '../tpl/_alertBoxTplConventionPlan.juicer' //关闭项目弹窗
import alertBoxTplHealthOccupation from '../tpl/_alertBoxTplHealthOccupation.juicer' //关闭项目弹窗
import alertBoxTplAccidentTerms from '../tpl/_alertBoxTplAccidentTerms.juicer' //关闭项目弹窗

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryGuidence from 'gtryGuidence'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryGuidence);
/* translation */


let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let product_type = reqObj['product_type']
let product_id = reqObj['product_id']
let member_card_id = reqObj['member_card_id']

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang3
    }

    if (product_type == 'disease') {
        product_type = 1
    } else if (product_type == 'accident') {
        product_type = 2
    }
    res.JumpName = lang.lang3;
    res.product_type = product_type;

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    // console.log('view=', res)

    $UI.append(mainTpl(res)); //主模版

    if (res.data.commit_state == 3) {
        $('.next-btn-wrap').css('pointer-events', 'none')
        $('.btn-inner').css('background', '#999')
    } else {
        $('.next-btn-wrap').css('pointer-events', 'auto')

    }
    // 隐藏loading
    utils.hideLoading();

    commonFooter.init(res);
    fastclick.attach(document.body);
    cliclHandle(res);
    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {

    //disease
    // health-requirement
    $('body').on('click', '#health-requirements', function() {
        console.log('health')
        if (!$('.health-requirement').length) {
            $UI.append(alertBoxTplHealthRequirement(rs));
        }
        $('.health-requirement').css('display', 'block');
    })

    // convention-plan
    $('body').on('click', '#GFCDterms', function() {
        if (!$('.convention-plan').length) {
            $UI.append(alertBoxTplConventionPlan(rs));
        }
        $('.convention-plan').css('display', 'block');
    })


    //accident
    // Health-occupation
    $('body').on('click', '#health-occupation', function() {
            if (!$('.health-occupation').length) {
                $UI.append(alertBoxTplHealthOccupation(rs));
            }
            $('.health-occupation').css('display', 'block');
        })
        // accident-terms
    $('body').on('click', '#GSFAterms', function() {
        if (!$('.accident-terms').length) {
            $UI.append(alertBoxTplAccidentTerms(rs));
        }
        $('.accident-terms').css('display', 'block');
    })

    // got it
    $('body').on('click', '.commonBoxStyle #gotIt', function() {
        $('.commonBoxStyle').css('display', 'none');
    })


    // next
    $('body').on('click', '.next-btn', function() {
        location.href = '/patientInfo.html?member_card_id=' + member_card_id + '&product_id=' + product_id + '&product_type=' + product_type
    })
}

export default obj;