// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import bankList from './_bankList'
import validate from './_validate'
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat'
import cardTpl from '../tpl/_card.juicer'
import accidentCardTpl from '../tpl/_accident_card.juicer'
import mainTpl from '../tpl/main.juicer'
// import alertBoxTplHealthRequirement from '../tpl/_alertBoxTplHealthRequirement.juicer' //关闭项目弹窗
// import alertBoxTplConventionPlan from '../tpl/_alertBoxTplConventionPlan.juicer' //关闭项目弹窗
import qscScroll_timestamp from 'qscScroll_timestamp'
import fastclick from 'fastclick'
// import googleAnalytics from 'google.analytics'
import googleAnalytics from 'google.analytics.gtry'
import store from 'store'
// import sensorsActive from 'sensorsActive'
import getPayPlatform from 'getPayPlatform'

let isAgreePolicy = true; //是否同意协议
let reqObj = utils.getRequestParams();
let product_id = reqObj['product_id'];
// let select = reqObj['select'];
let member_card_id = reqObj['member_card_id'];

/* translation */
import gtryPay from 'gtryPay'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPay);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let page = 0;
let scroll_list = new qscScroll_timestamp();
// let scroll_list2 = new qscScroll_timestamp();
// let firstInsert = true;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + titleLang.topUpGTRY
    }

    res.JumpName = titleLang.topUpGTRY;
    if (utils.judgeDomain() == 'qa') {
        res.commonNavGoToWhere = 'https://gtry-qa.pedulisehat.id/membershipCard.html';

    } else if (utils.judgeDomain() == 'pre') {
        res.commonNavGoToWhere = 'https://gtry-pre.pedulisehat.id/membershipCard.html';

    } else {
        res.commonNavGoToWhere = 'https://gtry.pedulisehat.id/membershipCard.html';

    }


    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    // res.member_info.balance = changeMoneyFormat.moneyFormat(res.member_info.balance)

    // console.log('=====',res.member_info.balance)
    $UI.append(mainTpl(res)); //主模版

    for (let i = 0; i < res.data.length; i++) {
        // handle image
        if (res.data[i].image) {
            res.data[i].image = JSON.parse(res.data[i].image);
        }
    }

    bankList.init(res); // OVO合作项目只需要展示OVO
    obj.event(res);

    initScorll();
    // initScorll2();

    commonFooter.init(res);
    fastclick.attach(document.body);


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

function initScorll() {
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });
    scroll_list.run();
};
// function initScorll2() {
//     scroll_list2.config({
//         wrapper: $UI,
//         onNeedLoad: function () {
//             $UI.trigger('needload2', [++page])
//         }
//     });
//     scroll_list2.run();
// };

obj.insertData = function(res) {
    console.log('view : insertData', res);
    res.lang = lang;

    if (res && res.data.illness && res.data.illness.length != 0) {
        console.log('---insertDieaseData 1---');

        for (let i = 0; i < res.data.illness.length; i++) {
            // 格式化金额
            res.data.illness[i].joinFrom = 'detail'
            res.data.illness[i].balance = changeMoneyFormat.moneyFormat(res.data.illness[i].balance);
        }

        res._member_card_id = member_card_id;
        // res._select = select;
        $('.insertDieaseData').append(cardTpl(res));
        scroll_list.run();

    } else {
        console.log('insert1stop')

        $('.bottom').css('display', 'block')
        $('.loading').hide();
        scroll_list.stop();
    }

    //意外
    if (res && res.data.accident && res.data.accident.length != 0) {
        console.log('---insertDieaseData 2---');

        for (let i = 0; i < res.data.accident.length; i++) {
            // 格式化金额
            res.data.accident[i].joinFrom = 'accident'
            res.data.accident[i].balance = changeMoneyFormat.moneyFormat(res.data.accident[i].balance);
        }

        console.log('----accident-insert----', res)
        res._member_card_id = member_card_id;
        $('.insertAccidentwData').append(accidentCardTpl(res));
        scroll_list.run();

    } else {
        console.log('insert2stop')

        $('.bottom').css('display', 'block')
        $('.loading').hide();
        scroll_list.stop();

    }


}

obj.event = function(rs) {
    // submit
    $UI.on('click', '.pay-btn', function() {
        if (!$('.pay-btn').hasClass('pay-btn-not-agree')) {
            submitData = getSubmitParam();
            console.log('submitData', submitData);

            if (validate.check(submitData, lang)) {
                utils.showLoading(lang.lang36);
                $UI.trigger('submit', [submitData]);
            }
        }
    });

    // add card / delete card
    $UI.on('click', '.member-card-inner', function() {
        $(this).find('.choose').toggleClass('chosed');
        // 支付总金额
        $('.new_amount').attr('data-amount', $('.price-wrap .selected').attr('data-price') * ($('.member-card .chosed').length || 0));

        // 选中金额 + 人数改变 ,实际支付总金额 + 费率更改
        changeMoneyNum();
    });

    // choose price
    $('body').on('click', '.price', function(e) {
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
        $('.new_amount').attr('data-amount', $(this).attr('data-price') * ($('.member-card .chosed').length || 0))
        $('.data-amount').html(changeMoneyFormat.moneyFormat($(this).attr('data-price')));

        // 选中金额 + 人数改变 ,实际支付总金额 + 费率更改
        changeMoneyNum();
    });

    // payment channel
    $UI.on('click', '.payment_channel', function(rs) {
        $('.alert-bank').css('display', 'block');
    })
};

function changeMoneyNum() {
    // 选中金额 + 人数改变
    if ($('.member-card .chosed').length) {
        // 展示选中金额
        $('.data-amount').html(changeMoneyFormat.moneyFormat($('.price-wrap .selected').attr('data-price')));
        // 展示充值几人
        if ($('.member-card .chosed').length > 1) {
            $('.cardNum').html($('.member-card .chosed').length);
            $('.right_num').addClass('show');
        } else if ($('.member-card .chosed').length == 1) {
            $('.right_num').removeClass('show');
        }

        // btn 正常
        $('.pay-btn').removeClass('pay-btn-not-agree');

    } else {
        // 展示充值 0 人 0 元
        $('.data-amount').html(0);
        $('.right_num').removeClass('show');

        // btn 变灰色
        $('.pay-btn').addClass('pay-btn-not-agree');
    }

    // 实际支付总金额 + 费率更改
    bankList.calculateChannelFee();
}


function getSubmitParam() {
    let paid_up_money = $('#price').html().replace(/\./g, '');
    // let pay_platform; //1.faspay 2. gopay 3 sinamas 4ovo
    let bank_code = $(".payment_channel .bank-name").attr('data-bankCode');
    let methodFee = $(".payment_channel .bank-name").attr('data-methodFee');
    let member_card_ids = '';
    // let terminal = 10;

    // if (appVersionCode > 128) {
    //     terminal = 21
    // }
    for (let i = 0; i < $('.chosed').length; i++) {
        console.log('i=', i, $($('.chosed')[i]).attr('data-id'));
        member_card_ids = member_card_ids + $($('.chosed')[i]).attr('data-id') + ',';
    }

    // // gopay
    // if (bank_code == '1') {
    //     pay_platform = 2;
    // }
    // // sinamas
    // else if (bank_code == '2') {
    //     pay_platform = 3;
    // }
    // // ovo
    // else if (bank_code == '4') {
    //     pay_platform = 4;
    // }
    // // dana
    // else if (bank_code == '5') {
    //     pay_platform = 5;
    // }
    // // faspay
    // else {
    //     pay_platform = 1;
    // }

    return {
        // product_id: product_id,
        paid_up_money: parseFloat(paid_up_money),
        paid_money: parseFloat($('.new_amount').attr('data-amount')),
        members: [], // 参保人
        payment_channel: bank_code,
        payment_channel_name: $(".payment_channel .bank-name").text(),
        terminal: utils.terminal,
        email: '',
        pay_platform: getPayPlatform(bank_code),
        member_card_ids: member_card_ids.substring(0, member_card_ids.lastIndexOf(',')), // 已是会员充值 逗号分隔多个会员
        phone: '', // 支付人的手机号码
        ab_test: {
            "gtry_detail_template": store.get('gtry_detail_template') == 'a' ? 'GTRY-detail-A' : 'GTRY-detail-B'
        },
        version_code: appVersionCode,
        version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
        // avatar: '',
        // real_name: $('.member-name').attr('data-name'),
        // id_card: $('.member-ID').attr('data-id'),
        // phone: $('.member-phone').val(),
        // paid_money: $('.new_amount').attr('data-amount'),
        // paid_up_money: parseInt(paid_up_money),
        // product_id: product_id,
        // member_card_id: member_card_id,
        // // comment: $("textarea[name=commentArea]").val(), //备注：$('#story').val()不能获取到值？？？
        // // project_id: project_id,
        // payment_channel: bank_code,
        // payment_channel_name: $(".payment_channel .bank-name").text(),
        // terminal: 10,
        // pay_platform: pay_platform,
    }
}


export default obj;