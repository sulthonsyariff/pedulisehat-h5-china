/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);
    if (!param.member_card_ids) {
        utils.alertMessage('please choose at least one member!!!');
        return false;
    }

    if (!$(".payment_channel .bank-name").attr('data-bankCode')) {
        utils.alertMessage(lang.lang4);
        return false;
    }

    return true;
}

export default obj;