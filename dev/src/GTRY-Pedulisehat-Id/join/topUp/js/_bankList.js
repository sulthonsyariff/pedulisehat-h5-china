import mainTpl from '../tpl/_bankList.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point



/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let reqSelect = location.href

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {

    res.domainName = domainName;
    res.commonLang = commonLang;
    res.isAndroid = utils.browserVersion.android;

    $UI.prepend(mainTpl(res));
    console.log('res=', res);

    fastclick.attach(document.body);
    obj.event(res);
};

obj.event = function(res) {
    // close
    $('.alert-bank .close').on('click', function(rs) {
        $('.alert-bank').css('display', 'none')
    })

    // choose bank
    $('body').on('click', '.list-items', function(e) {

        $('.list-items').removeClass('choosed-bank');
        $(this).addClass('choosed-bank');

        setTimeout(() => {
            $('.alert-bank').css('display', 'none')
        }, 100)

        $('.channel').addClass('selected').html($(this).html());

        obj.calculateChannelFee(res)
        if ($(".payment_channel .bank-name").attr('data-methodFee') == 0) {
            $('.fee-desc').css('display', 'none')
        } else {
            $('.fee-desc').css('display', 'block')
        }

    })
}

// 实际支付总金额 + 费率更改
obj.calculateChannelFee = function(res) {
    let paid_money = parseInt($('.new_amount').attr('data-amount'));
    let bankChannelFee = $(".payment_channel .bank-name").attr('data-methodFee') || 0;
    let expense_type = $(".payment_channel .bank-name").attr('data-expenseType')

    // console.log('bankChannelFee = ', bankChannelFee);
    // console.log('paid_money=', paid_money);

    if ($('.member-card .chosed').length) {
        // GO-Pay：GO-PAY按照总金额*2%计算
        if ($(".payment_channel .bank-name").attr('data-expenseType') == 2) {
            // 实际支付总金额
            $('#price').html(changeMoneyFormat.moneyFormat(Math.ceil(paid_money / (1 - bankChannelFee))));
            // Fee show
            $('#channelFee').html(changeMoneyFormat.moneyFormat(Math.ceil(paid_money / (1 - bankChannelFee)) - paid_money));
            $('.amount-and-fee .channelFee').html(changeMoneyFormat.moneyFormat(Math.ceil(paid_money / (1 - bankChannelFee)) - paid_money));

            // 选择了银行后再展示
            if ($('.channel.selected').length) {
                $('.fee-desc').css('display', 'block');
                $('.amount-and-fee .fee').css('display', 'block');
            }


        }
        //  // OVO：OVO 属于type 2类型
        // else if ($(".payment_channel .bank-name").attr('data-bankCode') == 4) {
        //     // 实际支付总金额
        //     $('#price').html(changeMoneyFormat.moneyFormat(paid_money * (1 + parseInt(bankChannelFee))));

        //     // Fee hide
        //     $('.fee-desc').css('display', 'none');
        //     $('.amount-and-fee .fee').css('display', 'none');

        //     // VA和online bank:按照每笔订单计算
        // }
        else if ($(".payment_channel .bank-name").attr('data-expenseType') == 1) {
            // 实际支付总金额
            $('#price').html(changeMoneyFormat.moneyFormat(paid_money + parseInt(bankChannelFee)));
            // Fee show
            $('#channelFee').html(changeMoneyFormat.moneyFormat(bankChannelFee));
            $('.amount-and-fee .channelFee').html(changeMoneyFormat.moneyFormat(bankChannelFee));

            // 选择了银行后再展示
            if ($('.channel.selected').length) {
                $('.fee-desc').css('display', 'block');
                $('.amount-and-fee .fee').css('display', 'block');
            }
        }
    } else {
        // 实际支付总金额
        $('#price').html(0);
        // Fee hide
        $('.fee-desc').css('display', 'none');
        $('.amount-and-fee .fee').css('display', 'none');
    }

    // else if (bankChannelFee) {
    //     $('#channelFee').html(changeMoneyFormat.moneyFormat(bankChannelFee))
    //     $('#price').html(changeMoneyFormat.moneyFormat(parseFloat(paid_money) + parseFloat(bankChannelFee)))
    // } else {
    //     $('#price').html(changeMoneyFormat.moneyFormat(paid_money))
    // }
}

export default obj;