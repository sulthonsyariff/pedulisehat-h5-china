import view from './view'
import model from './model'
import store from 'store'
import 'loading'
import '../less/main.less'
import domainName from 'domainName' // port domain name
import paySubmitSuccessHandle from 'paySubmitSuccessHandle' //支付成功跳转逻辑

// 引入依赖
import utils from 'utils'

// 隐藏loading
let UI = view.UI;
let reqObj = utils.getRequestParams();
let from = reqObj['from'];
let member_card_id = reqObj['member_card_id'];

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';


// 获取会员列表
UI.on('needload', function(e, page) {
    model.getMemberCards({
        param: {
            page: page
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('===needload===rs=', rs);

                view.insertData(rs);
                // getPayment(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
});

// model.getMemberInfo({
//     param: {
//         member_card_id: member_card_id
//     },
//     success: function(rs) {
//         if (rs.code == 0) {
//             // rs.getProjInfoData = res;
//             getPayment(rs);
//         } else {
//             utils.alertMessage(rs.msg)
//         }
//     },
//     error: utils.handleFail
// })
getPayment();


function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log(' get payment channel =', rs);
                // rs.getProjInfoData = res;
                // rs.member_info = res.data
                console.log('control-----member', rs)
                view.init(rs);
                utils.hideLoading();

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}


UI.on('submit', function(e, objData) {
    loginHandle(objData);
});

// 安卓内调支付
UI.on('appHandle_createPayment', function(e, objData) {
    createPayment(objData);
});

// view.init({});
/**
 * judge login or visit login
 *
 */
function loginHandle(objData) {
    model.getUserInfo({
        success: function(rs) {
            objData.avatar = rs.data.avatar;
            // objData.user_name = rs.data.user_name;
            objData.phone = rs.data.mobile;

            objData.user_name = objData.user_name ? objData.user_name : rs.data.user_name; //如果选择匿名，优先匿名

            // createPayment(objData); //logined in
            createPayment(objData);

        },
        unauthorizeTodo: function(rs) {
            // judge if visit login
            // if (store.get('donateMoney') && store.get('donateMoney').user_name && store.get('donateMoney').phone) {
            //     visitCreatePayment(objData);
            // } else {
            utils.hideLoading();

            setTimeout(function() {
                location.href = 'https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?loginFrom=gtryTopUp&qf_redirect=' + encodeURIComponent(location.href)
            }, 30)

            //jump to visit login
            // location.href = 'https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=login');
            // }
        },
        error: utils.handleFail
    });
}

function createPayment(objData) {

    model.createPayment({
        param: objData,
        success: function(res) {
            if (res.code == 0) {
                // successHandle(objData, res);
                res.gtryFrom = 'topUp';
                paySubmitSuccessHandle(objData, res);

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// function successHandle(objData, res) {

//     // gopay: bank_code = 1
//     if (objData.payment_channel == '1') {
//         storeGopay(res);
//         console.log('-----gopay-----', res)
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id + '&amount=' + res.data.money + '&gtryFrom=topUp'

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id + '&amount=' + res.data.money + '&gtryFrom=topUp'

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id + '&amount=' + res.data.money + '&gtryFrom=topUp'

//             }
//             // location.href = 'https://gtry-' + utils.judgeDomain() + '.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id + '&amount=' + res.data.money + '&gtryFrom=topUp'
//         }, 300);
//     }
//     // sinamas: bank_code = 2
//     else if (objData.payment_channel == '2') {
//         storeSinamas(res);
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'

//             }
//             // location.href = 'https://gtry-' + utils.judgeDomain() + '.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'
//         }, 300);
//     }
//     // OVO:bank_code = 4
//     else if (objData.payment_channel == '4') {
//         console.log('ovo = ', res, res.data.order_id, objData);

//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//                     '&amount=' + res.data.amount + '&gtryFrom=topUp' +
//                     '&trade_id=' + res.data.trade_id
//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//                     '&amount=' + res.data.amount + '&gtryFrom=topUp' +
//                     '&trade_id=' + res.data.trade_id
//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//                     '&amount=' + res.data.amount + '&gtryFrom=topUp' +
//                     '&trade_id=' + res.data.trade_id
//             }
//             // location.href = 'https://gtry-' + utils.judgeDomain() + '.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//             //     '&amount=' + res.data.amount + '&gtryFrom=topUp' +
//             //     '&trade_id=' + res.data.trade_id +
//             //     (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '')
//         }, 300);
//     }
//     // dana:bank_code = 5
//     else if (objData.payment_channel == '5') {
//         setTimeout(() => {
//             console.log('dana = ', res);
//             if (user_id) {
//                 setTimeout(() => {
//                     location.href = domainName.trade + '/v1/dana/oauth?Qsc-Peduli-Token=' + accessToken + '&checkout_url=' + encodeURI(res.data.checkout_url)
//                 }, 300);
//             } else {
//                 setTimeout(() => {
//                     location.href = res.data.checkout_url
//                 }, 300);
//             }
//         }, 300);
//     }
//     // faspay:bank_code = *
//     else if (objData.payment_channel == '707' ||
//         objData.payment_channel == '801' ||
//         objData.payment_channel == '708' ||
//         objData.payment_channel == '802' ||
//         objData.payment_channel == '408' ||
//         objData.payment_channel == '402' ||
//         objData.payment_channel == '702' ||
//         objData.payment_channel == '800') {
//         // console.log('VAorderPage', objData.payment_channel)
//         // console.log('VAorderPage===', JSON.stringify(res.data))
//         storeFaspay(res);
//         console.log('FasRes', res.data.trade_id)
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'

//             }

//             // location.href = 'https://gtry-' + utils.judgeDomain() + '.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel + '&gtryFrom=topUp'
//         }, 300);
//     }
//     // 其他银行
//     else {
//         location.href = res.data.redirect_url;
//     }
// }

// function storeGopay(res) {
//     generateQRcode = res.data.actions[0].url
//     deeplink_redirect = res.data.actions[1].url
//     store.set('goPay', {
//         generate_qr_code: generateQRcode,
//         deeplink_redirect: deeplink_redirect
//     });
// }

// function storeSinamas(res) {
//     SourceID = res.data.SourceID
//     IssueDate = res.data.IssueDate
//     NoRef = res.data.NoRef
//     VirtualAccountNumber = res.data.VirtualAccountNumber
//     Amount = res.data.Amount
//     trade_id = res.data.trade_id
//     order_id = res.data.order_id
//     let VAorder = {
//         SourceID: SourceID,
//         IssueDate: IssueDate,
//         NoRef: NoRef,
//         VirtualAccountNumber: VirtualAccountNumber,
//         Amount: Amount,
//         trade_id: trade_id,
//         order_id: order_id,
//         from: 'sinarmas'
//     }
//     sessionStorage.setItem('VA', JSON.stringify(VAorder));
// }

// function storeFaspay(res) {
//     SourceID = res.data.merchant
//     IssueDate = res.data.pay_expired
//     NoRef = res.data.bill_no
//     VirtualAccountNumber = res.data.trx_id
//     Amount = res.data.money
//     trade_id = res.data.trade_id
//     order_id = res.data.order_id
//     let VAorder = {
//         SourceID: SourceID,
//         IssueDate: IssueDate,
//         NoRef: NoRef,
//         VirtualAccountNumber: VirtualAccountNumber,
//         Amount: Amount,
//         trade_id: trade_id,
//         order_id: order_id,
//         from: 'faspay'
//     }
//     sessionStorage.setItem('VA', JSON.stringify(VAorder));
// }