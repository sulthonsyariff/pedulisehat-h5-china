import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

// let reqObj = utils.getRequestParams();
// let showSP = reqObj['showSP'] || false; //是否展示shopeepay

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 会员列表
 */
obj.getMemberCards = function(o) {
    let url = domainName.heouic + '/v1/member_cards?page=' + o.param.page + '&limit=10';

    if (isLocal) {
        url = '../mock/GTRY/v1_member_cards_1.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};


obj.getMemberInfo = function(o) {
    var url = domainName.heouic + '/v1/member_card/' + o.param.member_card_id;

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};



//get payment
obj.getPayment = function(o) {
    var url = domainName.trade + '/v2/pay_channel' + (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '')
        // +
        // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    if (isLocal) {
        url = 'mock/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//heo create payment
obj.createPayment = function(o) {
    var url = domainName.heouic + '/v1/heo_order';
    console.log('heo', url)
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};



export default obj;