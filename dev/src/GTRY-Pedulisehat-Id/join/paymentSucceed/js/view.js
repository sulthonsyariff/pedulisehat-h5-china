// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import utils from 'utils'

// import googleAnalytics from 'google.analytics'
import googleAnalytics from 'google.analytics.gtry'
// import sensorsActive from 'sensorsActive'
import shareGtry from 'shareGtry'

let [obj, $UI] = [{}, $('body')];

/* translation */
import gtryPaySucceed from 'gtryPaySucceed'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
import common from 'common'
let commonLang = qscLang.init(common);
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPaySucceed);
/* translation */


obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }

    res.JumpName = '';
    res.lang = lang
    res.commonNavGoToWhere = '/';
    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    console.log('res=', res)
    $UI.append(mainTpl(res)); //主模版

    commonFooter.init(res);

    fastclick.attach(document.body);


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    $('body').on('click', '.viewBtn', function() {
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://gtry-qa.pedulisehat.id/membershipCard.html'

        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://gtry-pre.pedulisehat.id/membershipCard.html'

        } else {
            location.href = 'https://gtry.pedulisehat.id/membershipCard.html'

        }
    })

    $('body').on('click', '.shareBtn', function() {
        let paramObj = {
            item_id: "999999999",
            item_type: 0,
            // item_short_link: '',
            // remark: data.title, //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'gtry-share-wrapper', // 分享弹窗名
            shareCallBackName: 'gtryShareSuccess', //大病详情分享
            fromWhichPage: 'gtryPaymentSucceed.html' || '' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            let actTitle;
            let actDesc;

            if (res.state == 1) {
                actTitle = commonLang.lang36
                actDesc = lang.lang54
            } else {
                actTitle = commonLang.lang26
                actDesc = ''
            }

            let paramString = "&actTitle=" + encodeURIComponent(actTitle) +
                "&actDesc=" + encodeURIComponent(actDesc)

            $UI.trigger('android-share', [paramString, paramObj]);

        } else {
            // 分享组件
            shareGtry.init(paramObj);

            $('.bubble-icon').css({
                'display': 'none'
            })
        }
    });


    // sensorsActive.init();
};

export default obj;