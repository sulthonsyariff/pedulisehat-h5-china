import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let reqObj = utils.getRequestParams();
let member_card_id = reqObj['member_card_id'];

model.getMemberCardTransactions({
    param: member_card_id,
    success: function(res) {
        if (res.code == 0) {
            // event_type	账单类型：1.充值 2.奖励 3.分摊
            console.log('res = ', res);

            view.init(res);

        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
});