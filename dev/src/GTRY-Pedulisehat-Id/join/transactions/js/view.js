// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import changeMoneyFormat from 'changeMoneyFormat'
import qscScroll_timestamp from 'qscScroll_timestamp'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryTransactions from 'gtryTransactions'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryTransactions);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang1
    }

    res.JumpName = lang.lang1;

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;

    for (let i = 0; i < res.data.length; i++) {
        res.data[i].created_at = timeHandle(res.data[i].created_at);
        res.data[i].money = changeMoneyFormat.moneyFormat(res.data[i].money);

        console.log(timeHandle(res.data[i].created_at));
    }
    $UI.append(mainTpl(res)); //主模版

    // 隐藏loading
    utils.hideLoading();

    commonFooter.init(res);
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

function timeHandle(created_at) {
    let myDate = new Date(created_at);

    let year = myDate.getFullYear();
    let month = myDate.getMonth() + 1; //js从0开始取
    let day = myDate.getDate();
    let hour = myDate.getHours();
    let minutes = myDate.getMinutes();
    let second = myDate.getSeconds();

    if (month < 2) {
        month = "January";
    } else if (month < 3) {
        month = "February";
    } else if (month < 4) {
        month = "March";
    } else if (month < 5) {
        month = "April";
    } else if (month < 6) {
        month = "May";
    } else if (month < 7) {
        month = "June";
    } else if (month < 8) {
        month = "July";
    } else if (month < 9) {
        month = "August";
    } else if (month < 10) {
        month = "September";
    } else if (month < 11) {
        month = "October";
    } else if (month < 12) {
        month = "November";
    } else if (month < 13) {
        month = "December";
    }

    if (day < 10) {
        day = "0" + day;
    }

    // if (month < 10) {
    //     month = "0" + month;
    // }
    // if (day < 10) {
    //     day = "0" + day;
    // }

    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (second < 10) {
        second = "0" + second;
    }
    // return year + '.' + month + '.' + day + '  ' + hour + ':' + minutes + ':' + second;

    return hour + ':' + minutes + ' ' + day + ' ' + month + ' ' + year
}



export default obj;