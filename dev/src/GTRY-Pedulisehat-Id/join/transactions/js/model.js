import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 会员详情
 */
obj.getMemberCardTransactions = function(o) {
    let url = domainName.heouic + '/v1/member_card_bill?member_card_id=' + o.param;

    if (isLocal) {
        url = '../mock/GTRY/v1_member_card_bill.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


export default obj;