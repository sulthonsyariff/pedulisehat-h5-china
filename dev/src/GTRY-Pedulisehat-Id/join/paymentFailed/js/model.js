import 'jq_cookie' //ajax cookie
import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name


let obj = {};

obj.getOrderState = function(o) {
    console.log('======')
    var url = domainName.heouic + '/v1/order/' + o.param.order_id;
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

export default obj;