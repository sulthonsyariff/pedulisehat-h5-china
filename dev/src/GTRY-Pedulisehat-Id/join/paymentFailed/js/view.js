// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import utils from 'utils'

// import googleAnalytics from 'google.analytics'
import googleAnalytics from 'google.analytics.gtry'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryPayFailed from 'gtryPayFailed'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPayFailed);
/* translation */


let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }

    res.JumpName = '';
    res.commonNavGoToWhere = '/';

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang

    $UI.append(mainTpl(res)); //主模版

    commonFooter.init(res);
    fastclick.attach(document.body);

    $('body').on('click', '.viewBtn', function() {
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://gtry-qa.pedulisehat.id/topUp.html?member_card_id=' + res.data.member_card_id + '&product_id=' + res.data.product_id

        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://gtry-pre.pedulisehat.id/topUp.html?member_card_id=' + res.data.member_card_id + '&product_id=' + res.data.product_id

        } else {
            location.href = 'https://gtry.pedulisehat.id/topUp.html?member_card_id=' + res.data.member_card_id + '&product_id=' + res.data.product_id

        }
    })

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-wrapper').css('margin-top', '0')
        $('.failed-content').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-wrapper').css('margin-top', '-56px')

        $('.failed-content').css('padding-top', '56px')

    });
};

export default obj;