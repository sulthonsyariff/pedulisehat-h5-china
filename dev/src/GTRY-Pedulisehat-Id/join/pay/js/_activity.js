import model from './model'
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

let obj = {};
let $UI = $('body');
let reqObj = utils.getRequestParams();
let from = reqObj['from'];
let joinFrom = reqObj['joinFrom'];
let product_id = reqObj['product_id']; //当前互助商品ID
let activity_product_id;
/**
 * 修改计算公式：
 * 1.+ top up amount
 * 2.+ GTRY Accident
 * 3.+ GTRY Critical Diseases
 * 4.+ payment Channel Fee
 * 5.- promotion (活动：大病互助免费加入,加入大病互助的第一名会员免费)
 */

// 活动：正在进行的互助折扣活动
obj.init = function (_product_id) {
    console.log('...activity init...');

    // 活动：正在进行的互助折扣活动
    model.getPromotion({
        success: function (rs) {
            if (rs.data &&
                rs.data.act &&
                rs.data.activity_heo_promotion &&
                _product_id == rs.data.activity_heo_promotion.product_ids) {
                console.log('...有正在进行的活动...')

                //_product_id 可以是 1.当前商品ID和活动商品ID匹配; 2.推荐商品ID和活动商品ID匹配
                // 如果匹配，再进一步判断用户是否能参加此活动
                canJoin(rs);

                activity_product_id = rs.data.activity_heo_promotion.product_ids;
            } else {
                console.log('...没有正在进行的活动，重新计算...')
                // 没有活动，重新计算
                $UI.trigger('calculationFormula'); //重新计算支付公式
            }
        },
        error: utils.handleFail
    })
};

obj.hide = function (_product_id) {
    // 如果此互助商品是正在做活动，则隐藏相应活动元素
    if (_product_id == activity_product_id) {
        if ($('.promotion').length) {
            console.log('...有活动,则隐藏重置资金公式中的活动资金...');

            $('.promotion').attr('data-amount', 0);
            $('.promotion').attr('data-activity_product_id', '');
            $('.promotion .amount_show').html();
            $('.promotion').css('display', 'none');

            // 一旦修改金额，就需要重新计算
            $UI.trigger('calculationFormula'); //重新计算支付公式
        }
    } else {
        console.log('...没有活动...');

        $UI.trigger('calculationFormula'); //重新计算支付公式
    }

}

// 活动：是否能参加互助奖励活动
function canJoin(res) {
    model.canJoinPromotion({
        param: {
            activity_product_ids: res.data.activity_heo_promotion.product_ids
        },
        success: function (rs) {
            if (rs.data && rs.data.can_join) {

                console.log('...能参加活动，重新计算....')

                $('.promotion').attr('data-amount', res.data.activity_heo_promotion.amount_show);
                $('.promotion').attr('data-activity_product_id', res.data.activity_heo_promotion.product_ids);
                $('.promotion .amount_show').html(changeMoneyFormat.moneyFormat(res.data.activity_heo_promotion.amount_show));
                $('.promotion').css('display', 'block');

                $UI.trigger('calculationFormula'); //重新计算支付公式

            } else {
                console.log('...不能参加活动，重新计算....')

                $UI.trigger('calculationFormula'); //重新计算支付公式
            }
        },
        error: utils.handleFail
    })
}


export default obj;