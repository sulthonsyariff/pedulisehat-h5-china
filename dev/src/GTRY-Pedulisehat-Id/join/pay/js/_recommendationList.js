import recommendationTpl from '../tpl/_recommendationList.juicer' //联合支付列表
import amountTpl from '../tpl/_recommendationAmountList.juicer' //资金公式
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import activity from './_activity'

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let reqSelect = location.href
let recommendProductIdArr = []; // 所有的推荐商品ID集合

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function (res) {
    $('.Recommendation').append(recommendationTpl(res)); //联合支付列表
    $('.recommendationAmountList').append(amountTpl(res)); //联合支付资金公式

    setSessionDefault(res.data);

    obj.event(res);

    fastclick.attach(document.body);
};

obj.handle = function () {
    handleRecomUI()
}

obj.event = function (res) {
    // +- recommendation 是否选择联合支付,选择边框变绿选中状态
    $('body').on('click', '.joint-payoffs', function () {
        $(this).toggleClass('join');

        let activeProductId = $(this).data('product_id');
        let activeAmount = $(this).data('amount');
        let _activRecommendationAmount = $(".recommendationAmount[data-product_id=" + activeProductId + "]");
        // let activity_product_id = $('.promotion').attr('data_activity_product_id');

        $(_activRecommendationAmount).toggleClass('show');

        // 处理相对应计算公式中联合支付的金额
        if ($(this).hasClass('join')) {

            // 资金公式UI
            $(_activRecommendationAmount).attr('data-amount', activeAmount);
            $(_activRecommendationAmount).find('.recmdt_amount').html(changeMoneyFormat.moneyFormat(activeAmount)); //展示金额三位一个点部分

            //一旦修改金额，（不管有没有活动，能不能参加活动），都需要重新计算
            activity.init(activeProductId); //活动，判断当前商品是否正在做活动，并判断用户可以参加此活动否

        } else {

            // 资金公式UI
            $(_activRecommendationAmount).attr('data-amount', 0);

            // 一旦修改金额，就需要重新计算
            activity.hide(activeProductId); //判断此商品有在做活动吗
        }

    })
}

// 处理UI
function handleRecomUI() {

    console.log('...处理推荐商品列表UI+资金公式UI+活动部分UI...');

    let param = getSession();
    for (let key in param) {
        let _activRecommendationAmount = $(".recommendationAmount[data-product_id=" + key + "]");

        // 至少得有一个人名，才能展示互助商品项，否则隐藏相应互助商品项
        if ((param[key]).length == 0) {
            console.log('...隐藏重置推荐商品列表+隐藏重置资金公式中商品资金...');

            // ！！！判断互助商品是否在参加活动，有活动则隐藏重置资金公式中的活动资金！！！
            activity.hide(key);

            // 注意：如果推荐列表之前选中过，不仅仅需要隐藏，还需要重置：
            // 1.推荐列表:重置并隐藏相应商品（重置之前选中得互助商品（即选中的变为不选中），以免显示的时候有问题）
            $('.joint-payoffs.join').removeClass('join'); //推荐列表重置去除选中状态
            $('.joint-payoffs[data-product_id="' + key + '"]').css('display', 'none'); //推荐列表隐藏商品
            hideRecommendation(); //确认是否隐藏父元素

            // 2.资金公式：隐藏重置资金公式中的相应的公式
            $(_activRecommendationAmount).find('.cardNum2').text(0); //资金公式商品人数=0
            $(_activRecommendationAmount).attr('data-amount', 0); //资金公式商品amount=0 重置
            $(_activRecommendationAmount).removeClass('show'); //隐藏资金公式商品

        } else {
            console.log('...展示推荐商品列表+展示资金公式中商品资金...');

            // 1.推荐列表：展示符合的互助商品（修改name+人数）
            $('.joint-payoffs[data-product_id="' + key + '"] .nameArr').html((param[key]).toString()); //推荐列表name
            $('.joint-payoffs[data-product_id="' + key + '"] .nameLength').html(param[key].length); //推荐列表人数
            $('.Recommendation').css('display', 'block'); //展示列表父元素
            $('.joint-payoffs[data-product_id="' + key + '"]').css('display', 'block'); //展示列表子元素

            // 2.资金公式：改变公式中相应商品的人数
            $(_activRecommendationAmount).attr('data-amount',
                $('.joint-payoffs[data-product_id="' + key + '"]').data('amount')); //资金公式商品amount
            $(_activRecommendationAmount).find('.cardNum2').text(param[key].length); //资金公式商品人数

            $UI.trigger('calculationFormula'); //重新计算支付公式
        }
    }
}

// 如果子元素都隐藏了，再隐藏父元素
function hideRecommendation() {
    let arr = []
    for (let i = 0; i < $('.joint-payoffs').length; i++) {
        if ($($('.joint-payoffs')[i]).css('display') == 'none') {
            arr.push(i);
            if (arr.length == $('.joint-payoffs').length) {
                $('.Recommendation').css('display', 'none');
            }
        }
    }
}

// 初始化
function setSessionDefault(param) {
    let _paramObj = {};
    for (let i = 0; i < param.length; i++) {
        let _param = param[i];
        _paramObj[_param.product_id] = []; //初始化，活动商品符合的人数为零
        recommendProductIdArr.push(_param.product_id);
    }

    sessionStorage.setItem('recommendProductIdArr', JSON.stringify(recommendProductIdArr)); //推荐列表IDs [project_id1,project_id2]
    sessionStorage.setItem('projectIdGetNames', JSON.stringify(_paramObj)); //ID对应的人名 {project_id1:[人名1,人名2.。。],project_id2:[人名1,人名2，人名3.。。]}
}

function getSession() {
    return JSON.parse(sessionStorage.getItem('projectIdGetNames'));
}

export default obj;