/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    for (let i = 0; i < param.member_info.length; i++) {
        if (!param.member_info[i].real_name) {
            utils.alertMessage(lang.lang2);
            return false;
        }
        // if (param.member_info[i].id_card == '') {
        //     utils.alertMessage(lang.lang31);
        //     return false;
        // }
        if (param.member_info[i].id_card.length && param.member_info[i].id_card.length != 16) {
            // console.log('id-wrong')
            utils.alertMessage(lang.lang32);
            return false;
        }
        // if (param.member_info[i].member_phone.length < 7) {
        //     utils.alertMessage(lang.lang30);
        //     return false;
        // }
        if (!param.member_info[i].relationship) {
            utils.alertMessage(lang.lang29);
            return false;
        }
    }

    // 预支付金额不为0
    if (param.paid_money && !param.payment_channel) {
        utils.alertMessage(lang.lang4);
        return false;
    }

    // if ($('.member-ID-tips').css('display') == 'block') {
    //     console.log("$('.member-ID-tips').css('display', 'block')");
    //     utils.alertMessage(lang.lang32);
    //     return false;
    // }

    return true;
}

export default obj;