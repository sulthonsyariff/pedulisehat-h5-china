import utils from 'utils'
import store from 'store'
import getPayPlatform from 'getPayPlatform'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let product_id = reqObj['product_id'];
let joinFrom = reqObj['joinFrom'];
let share = reqObj['share'] ? reqObj['share'] : '';
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0

let activity_product_id;

/****
当前商品id:就是当前页面展示的商品id
推荐商品ID：后端基于一定规则推荐当前页面之外的其他一些商品
活动商品ID:正在进行活动的商品 可能是当前 也可能是推荐的 也可能不在这两个范围
 */
obj.init = function(res) {
    let paid_up_money = $('#price').html().replace(/\./g, '');
    let paid_money = $('#price').attr('data-preTotal');
    let bank_code = $('.payment').css('display') == 'block' ? $(".channel.selected .bank-name").attr('data-bankCode') : '';
    let methodFee = $('.payment').css('display') == 'block' ? $(".channel.selected .bank-name").attr('data-methodFee') : '';
    let payment_channel_name = $('.payment').css('display') == 'block' ? $(".channel.selected .bank-name").text() : '';
    // let terminal = 10;
    // if (appVersionCode > 128) {
    //     terminal = 21
    // }

    activity_product_id = $('.promotion').attr('data-activity_product_id')

    return {
        member_info: getMemberArr(), // 参保人
        paid_up_money: parseFloat(paid_up_money), //实际支付金额
        paid_money: parseFloat(paid_money), //预支付金额
        payment_channel: bank_code,
        payment_channel_name: payment_channel_name,
        terminal: utils.terminal,
        email: '',
        pay_platform: getPayPlatform(bank_code),
        member_card_ids: '', // 已是会员充值 逗号分隔多个会员
        phone: res.mobile, // 支付人的手机号码
        short_link: share,
        ab_test: {
            "gtry_detail_template": store.get('gtry_detail_template') == 'a' ? 'GTRY-detail-A' : 'GTRY-detail-B'
        },
        version_code: appVersionCode,
        version_name: utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') || '',
    }
}

// 参保人
function getMemberArr() {
    let membersArr = [];
    let member = {};
    for (let i = 0; i < $('.member-card').length; i++) {
        member = {
            real_name: $($('.member-name')[i]).val(),
            id_card: $($('.member-ID')[i]).val().replace(/\s|\xA0/g, ""),
            relationship: parseInt($($(".relation_channel_wrap .relation-name")[i]).attr('data-relation')),
            product_ids: getProductIds($($('.member-name')[i]).val()), //参加多个互助的商品ID + 当前的商品ID
            activity_product_id: activity_product_id // 活动项目ID
        }
        membersArr.push(member);
    }

    return membersArr;
}

/****
 * 参保人参加的互助商品ID集合，默认所有人参与的互助商品ID集合一致
 * {project_id1:[人名1,人名2.。。],
 * project_id2:[人名1,人名2，人名3.。。]}
 *
 * 预期获取[人名1,人名2，人名3......]
 *
 */
function getProductIds(name) {
    let _projectIdGetNames = JSON.parse(sessionStorage.getItem('projectIdGetNames')); //ID对应的人名 {project_id1:[人名1,人名2.。。],project_id2:[人名1,人名2，人名3.。。]}
    let productIdsArr = [];
    let joinProductIdsArr = []; //用户实际额外参加的互助

    // 用户实际参与的互助产品有哪些
    for (let j = 0; j < $('.joint-payoffs.join').length; j++) {
        let joinProductIds = $($('.joint-payoffs.join')[j]).attr('data-product_id');
        joinProductIdsArr.push(joinProductIds);
    }

    console.log('joinProductIdsArr=', joinProductIdsArr);

    // 一一对应互助商品列表，获取商品ID相同的，则获取相应的name集合

    // 互助商品列表
    for (let key in _projectIdGetNames) {

        // 用户实际参与的互助产品
        for (let k = 0; k < joinProductIdsArr.length; k++) {

            // 如果相同，则获取名字集合
            if (key == joinProductIdsArr[k]) {

                let _nameArr = _projectIdGetNames[key];

                // 遍历名字，将商品ID注入数组
                for (let i = 0; i < _nameArr.length; i++) {

                    if (_nameArr[i] == name) {
                        productIdsArr.push(key); // 添加选中的+符合标准的互助商品
                    }
                }

            }
        }
    }

    productIdsArr.push(product_id); //添加当前商品ID

    return productIdsArr;
}

export default obj;