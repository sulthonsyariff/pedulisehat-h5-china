import validate from './_validate'
import utils from 'utils'

import alertBoxTplHealthRequirement from '../../../../../common/qsc/tpl/GTRY_alertBoxTplHealthRequirement.juicer' //关闭项目弹窗
import alertBoxTplCriticalDiseaseTerms from '../../../../../common/qsc/tpl/GTRY_alertBoxTplCriticalDiseaseTerms.juicer' //关闭项目弹窗
import alertBoxTplHealthOccupation from '../../../../../common/qsc/tpl/GTRY_alertBoxTplHealthOccupation.juicer' //关闭项目弹窗
import alertBoxTplAccidentTerms from '../../../../../common/qsc/tpl/GTRY_alertBoxTplAccidentTerms.juicer' //关闭项目弹窗

import memberCardTpl from '../tpl/_member-card.juicer'
import getSubmitParam from './_getSubmitParam'
import recommendationList from './_recommendationList'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let product_id = reqObj['product_id'];
let joinFrom = reqObj['joinFrom'];
let share = reqObj['share'] ? reqObj['share'] : '';
let phone;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0
let lang;
let recommendProductIdArr;

obj.init = function(rs) {
    let isAgreePolicy = true; //是否同意协议
    let addNum = 0;
    let $thisCard;
    lang = rs.lang;

    // pay:submit
    $('body').on('click', '.pay-btn', function() {
        if (isAgreePolicy) {
            let submitData = getSubmitParam.init(rs);

            // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
            if (validate.check(submitData, lang)) {
                utils.showLoading(lang.lang36);


                // sensors.track('GTRYJoin', {
                //     gtry_id: product_id,
                //     gtry_name: joinFrom,
                //     amount: submitData.paid_up_money,
                //     payment_method: submitData.payment_channel,
                //     members: submitData.member_info,
                // })

                console.log('===submitData===', submitData);
                $UI.trigger('submit', [submitData]);
            }
        } else {
            utils.alertMessage(lang.lang38);
        }
    });

    // - del member card
    $('body').on('click', '.del', function() {
        let _currentCardName = $(this).parents('.member-card').find('.member-name').val() || '';
        $(this).parents('.member-card').remove();

        // join btn css
        $('.join-btn').css('display', 'none');
        let length = $('.join-btn').length;
        $($('.join-btn')[length - 1]).css('display', 'block');

        changeNOText(); // 修改NO.1数字，

        // 参保人校验
        $UI.trigger('memberCardVerify');

        $UI.trigger('calculationFormula'); //重新计算支付公式

    })

    // + join  member card
    $('body').on('click', '.join-btn-inner', function() {
        // 添加新的卡片
        addNum = addNum + 1;
        rs.addNum = 'add-' + addNum;
        $('.member-card-append').append(memberCardTpl(rs)); //member card
        $('.join-btn').css('display', 'none'); //隐藏添加按钮（新的卡片内有添加按钮）
        // 修改卡片样式
        $thisCard = $(".member-card[data-add='add-" + addNum + "']");
        $thisCard.find('.join-btn').css('display', 'block')
        $thisCard.find('.del').css('display', 'block')
        utils.inputFourInOne(".member-ID[data-add='add-" + addNum + "']"); //限制4位一格

        changeNOText(); // 修改NO.1数字，
        // $UI.trigger('setProductIdsInMemberCard', $thisCard); //商品ID添加到参保人中：初始化和新增参保人时执行此函数
        $UI.trigger('calculationFormula'); //重新计算支付公式
    })

    // focusout:ID
    $('body').on('focusout', ".member-ID", function(e) {
        console.log('...focusout ID...');

        // 如果为空
        if (!$(e.currentTarget).val()) {
            $(".member-ID-tips[data-add='" + $(e.currentTarget).attr('data-add') + "']").css('display', 'block');
            //商品ID添加到参保人中：初始化和新增参保人时执行此函数
            // $UI.trigger('setProductIdsInMemberCard', $(e.currentTarget).parents('.member-card'));

            // 参保人校验
            $UI.trigger('memberCardVerify');
        } else {
            // 前端先校验ID card Number是否有重复
            let isIDCardRepeat = false;
            if ($('.member-ID').length > 1) {
                // 两个以上对比是否重复
                for (let i = 0; i < $('.member-ID').length; i++) {
                    if (!isIDCardRepeat &&
                        $(e.currentTarget).val() == $($('.member-ID')[i]).val() &&
                        $($('.member-ID')[i]).attr('data-add') != $(e.currentTarget).attr('data-add')) {
                        isIDCardRepeat = true;

                        //ID重复了，设置tips提示
                        $(".tips-normal[data-add='" + $(e.currentTarget).attr('data-add') + "']").css('display', 'none');
                        $(".member-ID-tips[data-add='" + $(e.currentTarget).attr('data-add') + "']").html(lang.lang49);
                        $(".member-ID-tips[data-add='" + $(e.currentTarget).attr('data-add') + "']").css('display', 'block');
                    }
                }
            }

            //  ID card 接口实时校验，如果重复则前端排重
            $UI.trigger('idcardVerify', {
                // product_id: product_id,
                currentCard: $(e.currentTarget).parents('.member-card'),
                id_card: $(e.currentTarget).val().replace(/\s|\xA0/g, ""),
                data_add: $(e.currentTarget).attr('data-add'),
                isIDCardRepeat: isIDCardRepeat
            });
        }
    })

    // focusout:name
    $('body').on('focusout', ".member-name", function(e) {
        console.log('...focusout name...');

        // 参保人校验
        $UI.trigger('memberCardVerify');
    })

    // focus:ID
    $('body').on('focus', ".member-ID", function(e) {
        // console.log('member-ID focus...');

        // 对应tips隐藏
        $(".member-ID-tips[data-add='" + $(e.currentTarget).attr('data-add') + "']").html('');
        $(".member-ID-tips[data-add='" + $(e.currentTarget).attr('data-add') + "']").css('display', 'none');
        $(".tips-normal[data-add='" + $(e.currentTarget).attr('data-add') + "']").css('display', 'block');
    })

    // relation channel
    $('body').on('click', '.relation_channel_wrap', function(rs) {
        let $data;

        // 获取当前卡片
        $data = $(this).data('add')
        $thisCard = $(".member-card[data-add='" + $data + "']");

        $('.alert-relation').attr('data-add', $data).css('display', 'block');
        $('.relation-list-items').removeClass('choosed-relation');

        if ($(this).find('.relation-name')) {
            let dataRelation = $(this).find('.relation-name').attr('data-relation');
            $('.alert-relation').find(".relation-name[data-relation='" + dataRelation + "'] ").parents('.relation-list-items').addClass('choosed-relation');
            // console.log('11===', dataRelation, $('.alert-relation').find(".relation-name[data-relation='" + dataRelation + "'] "));
        }
    })

    // payment channel
    $('body').on('click', '.payment_channel', function(rs) {
        $('.alert-bank').css('display', 'block')
    })

    // agree policy
    $('body').on('click', '.agree', function(e) {
        isAgreePolicy = !isAgreePolicy;
        $('.agree').toggleClass('notAgree');
        $('.pay-btn').toggleClass('pay-btn-not-agree');
    });

    // health-requirement 43000526217
    $('body').on('click', '#health-requirement', function() {
        if (!$('.health-requirement').length) {
            utils.showLoading();
            $UI.trigger('getArticle', [43000526217, callback]);

            function callback(res) {
                utils.hideLoading();

                $UI.append(alertBoxTplHealthRequirement(res));
                $('.health-requirement .detail').html(res.data.description)
                $('.health-requirement').css('display', 'block');
            }
        } else {
            $('.health-requirement').css('display', 'block');
        }
    })

    // plan Tnc 43000526225
    $('body').on('click', '#convention-plan', function() {
        if (!$('.convention-plan').length) {
            utils.showLoading();
            $UI.trigger('getArticle', [43000526225, callback]);

            function callback(res) {
                utils.hideLoading();

                $UI.append(alertBoxTplCriticalDiseaseTerms(res));
                $('.convention-plan .detail').html(res.data.description)
                $('.convention-plan').css('display', 'block');
            }
        } else {
            $('.convention-plan').css('display', 'block');
        }
    })

    // Health-occupation
    $('body').on('click', '#health-occupation', function() {
        if (!$('.health-occupation').length) {
            $UI.append(alertBoxTplHealthOccupation(rs));
        }
        $('.health-occupation').css('display', 'block');
    })

    // accident-terms
    $('body').on('click', '#accident-terms', function() {
        if (!$('.accident-terms').length) {
            $UI.append(alertBoxTplAccidentTerms(rs));
        }
        $('.accident-terms').css('display', 'block');
    })

    // got it
    $('body').on('click', '.commonBoxStyle #gotIt', function() {
        $('.commonBoxStyle').css('display', 'none');
    })
};

// 修改NO.1数字，
// 修改计算公式，
// 修改银行渠道下的tips费率提示
function changeNOText() {
    // NO.1
    for (let i = 0; i < $('.member-card').length; i++) {
        $($('.NO')[i]).text(i + 1);
    }
}


export default obj;