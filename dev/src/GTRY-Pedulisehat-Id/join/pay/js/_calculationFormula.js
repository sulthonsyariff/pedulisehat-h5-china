import calculatePaymentFee from 'GTRY-calculatePaymentFee' //计算fee
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

let obj = {};
let $UI = $('body');
obj.UI = $UI;

/**
 * 修改计算公式：
 * 
 * top up amount 充值金额
 * recommendationAmount 联合互助金额（隐藏不计入）
 * - promotion (活动减免金额)（隐藏不计入）
 * toalPaymentChannelFee 渠道费（隐藏不计入）
 * 
 * preTotal 预支付金额
 * total 支付金额
 */
obj.init = function () {
    console.log('*** 重新计算资金公式 ***');

    let _preTotal = preTotal(); //预支付金额

    let paramObj = {
        topUpAmount: topUpAmount(), //充值金额 Top Up Amount:
        recommendationAmount: recommendationAmount(), //联合支付金额
        promotion: promotion(), //活动优惠金额

        preTotal: _preTotal, //预支付金额
        toalPaymentChannelFee: 0, //总渠道费,初始状态下默认渠道费为0
        total: _preTotal, //支付金额，初始状态没有渠道费情况下为预支付金额

        bankChannelFee: $(".payment_channel .bank-name").attr('data-methodFee') || 0,
        bankExpenseType: $(".payment_channel .bank-name").attr('data-expenseType') || 0,
        bankCode: $(".payment_channel .bank-name").attr('data-bankCode') || 0
    }

    UIHandle(paramObj);
};

/****
 * 处理UI
 */
function UIHandle(paramObj) {
    console.log('===资金公式===', paramObj);

    changeNumText(); //修改计算公式 X 2

    // 没有算平台费的情况下，支付金额=预支付金额，并四位一个点展示
    $('#price').attr('data-preTotal', paramObj.preTotal);
    $('#price').html(changeMoneyFormat.moneyFormat($('#price').attr('data-preTotal')));

    // 预支付金额为0，隐藏支付渠道
    if (paramObj.preTotal == 0) {
        $('.payment').css('display', 'none'); //隐藏支付渠道
        hideFeeTips(); //隐藏所有fee tips提示
    } else {
        $('.payment').css('display', 'block'); //展示支付渠道
    }

    // 选中了银行才能计算总平台费以及支付金额，并且不是隐藏的银行
    if ($('.payment').css('display') == 'block' && $(".channel.selected .bank-name")) {
       
        paramObj = calculatePaymentFee.init(paramObj); //计算总平台费,以及支付金额

        // 有平台费的展示平台费 没有的不展示
        // expense_type = 2 / 1 
        if (paramObj.toalPaymentChannelFee) {
            // Payment Channel Fee:修改计算公式中的Fee值
            $('.amount-and-fee .channelFee').html(changeMoneyFormat.moneyFormat(paramObj.toalPaymentChannelFee));
            // 支付渠道下tips提示
            $('#channelFee').html(changeMoneyFormat.moneyFormat(paramObj.toalPaymentChannelFee));
            // Total：实际支付总金额
            $('#price').html(changeMoneyFormat.moneyFormat(paramObj.total));

            showFeeTips(); //隐藏所有fee tips提示
        } else {
            // Total：实际支付总金额,四位一点
            $('#price').html(changeMoneyFormat.moneyFormat(paramObj.total));
            hideFeeTips(); //隐藏所有fee tips提示
        }

        // // OVO：OVO不计 2019-11 OVO开始收平台费
        // if (paramObj.bankCode == 4) {
        //     // Total：实际支付总金额,四位一点
        //     $('#price').html(changeMoneyFormat.moneyFormat(paramObj.total));

        //     hideFeeTips(); //隐藏所有fee tips提示
        // }
        // // bankExpenseType == 2 GO-Pay：GO-PAY按照总金额*2%计算
        // // bankExpenseType == 1 VA和online bank:按照每笔订单计算
        // else if (paramObj.bankExpenseType == 2 || paramObj.bankExpenseType == 1) {
        //     // Payment Channel Fee:修改计算公式中的Fee值
        //     $('.amount-and-fee .channelFee').html(changeMoneyFormat.moneyFormat(paramObj.toalPaymentChannelFee));
        //     // 支付渠道下tips提示
        //     $('#channelFee').html(changeMoneyFormat.moneyFormat(paramObj.toalPaymentChannelFee));
        //     // Total：实际支付总金额
        //     $('#price').html(changeMoneyFormat.moneyFormat(paramObj.total));

        //     showFeeTips(); //隐藏所有fee tips提示
        // }
    }
}

/****
 * 获取充值金额
 */
function topUpAmount() {
    return parseInt($('.new_amount').attr('data-amount') * $('.member-card').length);
}

/****
 * 获取优惠金额,注意隐藏的不算
 */
function promotion() {

    if ($('.promotion').css('display') == 'block') {
        return parseInt($('.promotion').attr('data-amount'));

    } else {
        return 0;
    }

}

/**
 *获取联合支付金额,注意隐藏的不算
 */
function recommendationAmount() {
    let _rdAmountTotal = 0;

    for (let i = 0; i < $('.recommendationAmount.show').length; i++) {
        let num = $($('.recommendationAmount.show')[i]).find('.cardNum2').text();
        let _rdAmount = parseInt($($('.recommendationAmount.show')[i]).attr('data-amount') * parseInt(num));

        _rdAmountTotal += _rdAmount;
    }

    return _rdAmountTotal
}

// 计算预支付金额
function preTotal() {
    let _preToal = topUpAmount() + recommendationAmount() - promotion();
    return _preToal;
}

// 修改计算公式 X 2
function changeNumText() {
    // 1.修改充值金额部分人数
    let topUp_num = $('.member-card').length;
    if (topUp_num > 1) {
        $('.cardNum').html(topUp_num); //资金公式充值人数
        $('.right_num').addClass('show'); //展示
    } else {
        $('.right_num').removeClass('show'); //隐藏
    }

    // 2.修改商品部分人数
    let param = JSON.parse(sessionStorage.getItem('projectIdGetNames')); //ID对应的人名 {project_id1:[人名1,人名2.。。],project_id2:[人名1,人名2，人名3.。。]}
    for (let key in param) {
        let _activRecommendationAmount = $(".recommendationAmount[data-product_id=" + key + "]");
        let rec_num2 = param[key].length;

        if (rec_num2 > 1) {
            $(_activRecommendationAmount).find('.right_num2').addClass('show'); //展示
        } else {
            $(_activRecommendationAmount).find('.right_num2').removeClass('show'); //隐藏
        }
    }
}

function showFeeTips() {
    console.log('...展示fee渠道...');

    // 展示或隐藏：支付渠道下tips提示 + 计算公式中的Fee
    if ($('.payment').css('display') == 'block' && $('.channel.selected').length) {
        $('.fee-desc').css('display', 'block');
        $('.amount-and-fee .fee').css('display', 'block');
    }
}

function hideFeeTips() {
    console.log('...隐藏fee渠道...');

    // 展示或隐藏：支付渠道下tips提示 + 计算公式中的Fee
    $('.fee-desc').css('display', 'none');
    $('.amount-and-fee .fee').css('display', 'none');
}


export default obj;