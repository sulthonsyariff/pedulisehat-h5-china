import view from './view'
import model from './model'
import store from 'store'
import calculationFormula from './_calculationFormula.js'
import activity from './_activity'
import recommendationList from './_recommendationList'
import paySubmitSuccessHandle from 'paySubmitSuccessHandle' //支付成功跳转逻辑

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import domainName from 'domainName' // port domain name

// 隐藏loading
// utils.showLoading();
let UI = view.UI;
let reqObj = utils.getRequestParams();
let from = reqObj['from'];
let joinFrom = reqObj['joinFrom'];
let product_id = reqObj['product_id'];

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';

let avatar;
let phone;
let user_name;
let member_card_ids = '';
let recommendProductIdStr = ''; //互助推荐列表ID
let recommendProductIdArr = []; //互助推荐列表ID

let baseAmount = '10000';
let _baseAmount = '10.000';
let originalBaseAmount = '40.000';

/* translation */
import gtryPay from 'gtryPay'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPay);
/* translation */

model.getUserInfo({
    success: function(rs) {
        avatar = rs.data.avatar;
        // objData.user_name = rs.data.user_name;
        phone = rs.data.mobile;
        user_name = rs.data.user_name; //如果选择匿名，优先匿名

        getPayment(rs); //获取银行列表，加载页面
    },
    unauthorizeTodo: function(rs) {
        // judge if visit login
        // if (store.get('donateMoney') && store.get('donateMoney').user_name && store.get('donateMoney').phone) {
        //     visitCreatePayment(objData);
        // } else {
        utils.hideLoading();

        setTimeout(function() {
            location.href = utils.browserVersion.android ?
                'qsc://app.pedulisehat/go/login' : ('https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?loginFrom=gtryPay&qf_redirect=' + encodeURIComponent(location.href))
        }, 30)

        //jump to visit login
        // location.href = 'https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?qf_redirect=' + encodeURIComponent(location.href + '&fromLogin=login');
        // }
    },
    error: utils.handleFail
});

function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function(rs) {
            if (rs.code == 0) {
                // rs.getProjInfoData = res;
                rs.mobile = res.data.mobile;
                // console.log('获取银行列表===', rs)
                rs.baseAmount = baseAmount; //设置基础金额10000
                rs._baseAmount = _baseAmount;
                rs.originalBaseAmount = originalBaseAmount; //设置基础金额40000

                view.init(rs);

                activity.init(product_id); //活动，判断当前商品是否正在做活动，并判断用户可以参加此活动否

                utils.hideLoading();
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

// 获取互助商品推荐列表
UI.on('getProductRecmmend', function(e, res) {
    model.productRecmmend({
        param: {
            product_id: product_id
        },
        success: function(rs) {
            if (rs.code == 0 && rs.data && rs.data.length) {
                // console.log('获取互助商品推荐列表=', rs.data);

                // 校验接口需要商品ID号，默认需要推荐列表全部商品ID号
                let _data = rs.data;
                let _arr = [];
                for (let i = 0; i < _data.length; i++) {
                    _arr.push(_data[i].product_id);
                }
                recommendProductIdArr = _arr;
                recommendProductIdStr = _arr.toString();

                rs.lang = res.lang;
                rs._baseAmount = _baseAmount;
                rs.baseAmount = baseAmount;

                recommendationList.init(rs); //初始化互助商品推荐列表
                // view.setProductIdsInMemberCard(); // 商品ID添加到参保人中：初始化和新增参保人时执行此函数

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
})

// 参保人校验:
UI.on('memberCardVerify', function(e) {
    memberCardVerify();
})

// 参保人校验
function memberCardVerify() {
    console.log('...遍历有效人名,并存储session...');

    let _projectIdGetNames = JSON.parse(sessionStorage.getItem('projectIdGetNames')); //ID对应的人名 {project_id1:[人名1,人名2.。。],project_id2:[人名1,人名2，人名3.。。]}
    let recommendProductIdArr = JSON.parse(sessionStorage.getItem('recommendProductIdArr')); //推荐列表IDs //推荐列表IDs [project_id1,project_id2]

    for (let key in _projectIdGetNames) {
        let cardNameArr = [];
        _projectIdGetNames[key] = []; //先置空

        for (let i = 0; i < $('.member-card').length; i++) {
            let thisCard = $('.member-card')[i];
            let cardName = $(thisCard).find('.member-name').val() || '';
            let cardRelation = $(thisCard).find('.relation-name').html() || '';
            let cardID = $(thisCard).find('.member-ID').val() || '';
            let cardIDTips = $(thisCard).find('.member-ID-tips').html() || '';

            // 遍历有效的名字:分两种情况
            //1.cardName +  cardRelation + !cardID
            //2.cardName +  cardRelation + cardID + !cardIDTips
            if (cardName && cardRelation && !cardID ||
                cardName && cardRelation && cardID && !cardIDTips && $(thisCard).attr('data-' + key) == 'true'
            ) {
                cardNameArr.push(cardName);
                _projectIdGetNames[key] = cardNameArr;
            }
        }
    }

    sessionStorage.setItem('projectIdGetNames', JSON.stringify(_projectIdGetNames)); //ID对应的人名 {project_id1:[人名1,人名2.。。],project_id2:[人名1,人名2，人名3.。。]}
    recommendationList.handle();
}

UI.on('submit', function(e, objData) {
    loginHandle(objData);
});

// 安卓内调支付
UI.on('appHandle_createPayment', function(e, objData) {
    createPayment(objData);
});

// 重新计算支付公式
UI.on('calculationFormula', function(e) {
    calculationFormula.init();
})

/*****
 * {
    "code":0,
    "msg":"",
    "more_info":"",
    "data":{
        "product_ids":[
            "374934928",
            "384929489"
        ],
        "products":{
            "374934928":"already join",
            "384929489":"already join"
        },
        "public":"Please fill in the correct KTP number"
    }
}
 */
// 校验ID号
UI.on('idcardVerify', function(e, params) {
    params.product_id = product_id + ',' + recommendProductIdStr;
    if (params.isIDCardRepeat) {
        console.log('...ID重复...');

        let _products = JSON.parse(sessionStorage.getItem('recommendProductIdArr')); //推荐列表IDs [project_id1,project_id2]
        for (let key in _products) {
            $(params.currentCard).attr('data-' + key, false);
        }

        //参保人校验:
        memberCardVerify();

    } else if (!params.isIDCardRepeat && params.id_card) {
        console.log('...ID没有重复，调接口校验...');
        idcardVerify(params)
    }
})

function idcardVerify(params) {
    model.idcardVerify({
        param: params,
        success: function(res) {
            if (res.code == 0) {
                params.res = res;
                if (res.data.products) {
                    let _products = res.data.products;

                    if (_products[product_id]) {
                        view.IDCardTipsShow(params); //当前商品是否符合标准，为什么不符合标准
                    }

                    for (let key in _products) {
                        // 更新参保人card中的商品ID值
                        if (!_products[key]) {
                            $(params.currentCard).attr('data-' + key, true);
                        } else {
                            $(params.currentCard).attr('data-' + key, false);
                        }
                    }
                }

                //参保人校验:
                memberCardVerify();

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

/**
 * judge login or visit login
 *
 */
function loginHandle(objData) {
    objData.avatar = avatar;
    // objData.user_name = rs.data.user_name;
    // objData.phone = phone;
    objData.user_name = user_name; //如果选择匿名，优先匿名
    objData.member_card_ids = member_card_ids;

    // heoCreatePayment(objData); //logined in

    // 1:group logined pay 2:logined pay 3:group unlogin pay 4:unlogin pay
    createPayment(objData);
}

// 游客支付/登录支付/游客专题支付/登录专题支付，model请求链接不同（通过_whichPay区分）
function createPayment(objData) {
    // objData.share_template = store.get('detail-activity-share-donateAB'); //测试详情页AB测试对捐赠对影响
    store.set('donateMoney', ''); //clear the cache

    model.createPayment({
        param: objData,
        success: function(res) {
            if (res.code == 0) {
                res.gtryFrom = 'pay';
                paySubmitSuccessHandle(objData, res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: function(res) {
            // utils.hideLoading();
            if (res.data && res.data.member_card_ids) {
                utils.alertMessage(res.msg);
                member_card_ids = res.data.member_card_ids;
            } else {
                utils.alertMessage(res.msg);
            }
        }
    });
}

// getArticle
UI.on('getArticle', function(e, article_id, callback) {
    getArticle(article_id, callback);
});

function getArticle(article_id, callback) {
    model.getArticle({
        param: {
            article_id: article_id
        },
        success: function(res) {
            if (res.code == 0) {

                res.lang = lang;
                if (callback) {
                    callback(res);
                }

            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    })
}

// function heoCreatePayment(objData) {
//     model.heoCreatePayment({
//         param: objData,

//         success: function(res) {
//             if (res.code == 0) {
//                 console.log('===successHandle===', res);

//                 successHandle(objData, res);
//             } else {
//                 utils.alertMessage(res.msg)
//             }
//         },
//         // error: utils.handleFail
//         error: function(res) {
//             // utils.hideLoading();
//             if (res.data && res.data.member_card_ids) {
//                 utils.alertMessage(res.msg);
//                 member_card_ids = res.data.member_card_ids;
//             } else {
//                 utils.alertMessage(res.msg);
//             }
//         }
//     })
// }

// function successHandle(objData, res) {
//     // gopay: bank_code = 1
//     if (objData.payment_channel == '1') {
//         storeGopay(res);
//         // console.log('-----gopay-----', res)
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id +
//                     '&amount=' + res.data.money +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id +
//                     '&amount=' + res.data.money +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/goPay.html?trade_id=' + res.data.trade_id +
//                     '&amount=' + res.data.money +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             }
//         }, 300);
//     }
//     // sinamas: bank_code = 2
//     else if (objData.payment_channel == '2') {
//         storeSinamas(res);
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             }
//         }, 300);
//     }
//     // OVO:bank_code = 4
//     else if (objData.payment_channel == '4') {
//         // console.log('ovo = ', res, res.data.order_id, objData);
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//                     '&amount=' + res.data.amount + '&gtryFrom=pay' +
//                     '&trade_id=' + res.data.trade_id + '&joinFrom=' + joinFrom
//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//                     '&amount=' + res.data.amount + '&gtryFrom=pay' +
//                     '&trade_id=' + res.data.trade_id + '&joinFrom=' + joinFrom
//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/OVO.html?order_id=' + res.data.order_id +
//                     '&amount=' + res.data.amount + '&gtryFrom=pay' +
//                     '&trade_id=' + res.data.trade_id + '&joinFrom=' + joinFrom
//             }
//         }, 300);
//     }
//     // dana:bank_code = 5
//     else if (objData.payment_channel == '5') {
//         setTimeout(() => {
//             // console.log('dana = ', res);
//             if (user_id) {
//                 setTimeout(() => {
//                     location.href = domainName.trade +
//                         '/v1/dana/oauth?Qsc-Peduli-Token=' +
//                         accessToken + '&checkout_url=' + encodeURI(res.data.checkout_url)
//                 }, 300);
//             } else {
//                 setTimeout(() => {
//                     location.href = res.data.checkout_url
//                 }, 300);
//             }
//         }, 300);
//     }
//     // faspay:bank_code = *
//     else if (objData.payment_channel == '707' ||
//         objData.payment_channel == '801' ||
//         objData.payment_channel == '708' ||
//         objData.payment_channel == '802' ||
//         objData.payment_channel == '408' ||
//         objData.payment_channel == '402' ||
//         objData.payment_channel == '702' ||
//         objData.payment_channel == '800') {
//         // console.log('VAorderPage', objData.payment_channel)
//         // s.log('VAorderPage===', JSON.stringify(res.data))
//         storeFaspay(res);
//         // console.log('FasRes', res.data.trade_id)
//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/OrderTransactionDetail.html?channel=' + objData.payment_channel +
//                     '&gtryFrom=pay&joinFrom=' + joinFrom

//             }

//         }, 300);
//     }
//     // 金额为零，直接跳转成功页（活动的时候就会有这种情况）
//     else if (!objData.paid_up_money && res.data.order_id) {

//         setTimeout(() => {
//             if (utils.judgeDomain() == 'qa') {
//                 location.href = 'https://gtry-qa.pedulisehat.id/paymentSucceed.html?order_id=' + res.data.order_id +
//                     '&joinFrom=' + joinFrom

//             } else if (utils.judgeDomain() == 'pre') {
//                 location.href = 'https://gtry-pre.pedulisehat.id/paymentSucceed.html?order_id=' + res.data.order_id +
//                     '&joinFrom=' + joinFrom

//             } else {
//                 location.href = 'https://gtry.pedulisehat.id/paymentSucceed.html?order_id=' + res.data.order_id +
//                     '&joinFrom=' + joinFrom

//             }
//         }, 300);
//     }

//     // 其他银行
//     else {
//         location.href = res.data.redirect_url;
//     }
// }

// function storeGopay(res) {
//     generateQRcode = res.data.actions[0].url
//     deeplink_redirect = res.data.actions[1].url
//     store.set('goPay', {
//         generate_qr_code: generateQRcode,
//         deeplink_redirect: deeplink_redirect
//     });
// }

// function storeSinamas(res) {
//     SourceID = res.data.SourceID
//     IssueDate = res.data.IssueDate
//     NoRef = res.data.NoRef
//     VirtualAccountNumber = res.data.VirtualAccountNumber
//     Amount = res.data.Amount
//     trade_id = res.data.trade_id
//     order_id = res.data.order_id
//     let VAorder = {
//         SourceID: SourceID,
//         IssueDate: IssueDate,
//         NoRef: NoRef,
//         VirtualAccountNumber: VirtualAccountNumber,
//         Amount: Amount,
//         trade_id: trade_id,
//         order_id: order_id,
//         from: 'sinarmas'
//     }
//     sessionStorage.setItem('VA', JSON.stringify(VAorder));
// }

// function storeFaspay(res) {
//     SourceID = res.data.merchant
//     IssueDate = res.data.pay_expired
//     NoRef = res.data.bill_no
//     VirtualAccountNumber = res.data.trx_id
//     Amount = res.data.money
//     trade_id = res.data.trade_id
//     order_id = res.data.order_id
//     let VAorder = {
//         SourceID: SourceID,
//         IssueDate: IssueDate,
//         NoRef: NoRef,
//         VirtualAccountNumber: VirtualAccountNumber,
//         Amount: Amount,
//         trade_id: trade_id,
//         order_id: order_id,
//         from: 'faspay'
//     }
//     sessionStorage.setItem('VA', JSON.stringify(VAorder));
// }