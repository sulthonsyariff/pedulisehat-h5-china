// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import bankList from 'GTRY-bankList'
import clickHandle from './_clickHandle'
import relationList from './_relationList'
import utils from 'utils'
import mainTpl from '../tpl/main.juicer'
import memberCardTpl from '../tpl/_member-card.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
// import sensorsActive from 'sensorsActive'

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();
let product_id = reqObj['product_id'];
let joinFrom = reqObj['joinFrom'];
let share = reqObj['share'] ? reqObj['share'] : '';
let phone;

let baseAmount = 10000;
let _baseAmount = 10.000;
let originalBaseAmount = 40.000;

obj.UI = $UI;

/* translation */
import gtryPay from 'gtryPay'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPay);
/* translation */

let recommendProductIdArr;

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + titleLang.joinGTRY
    }

    res.JumpName = titleLang.joinGTRY;
    res.commonNavGoToWhere = '/';
    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.joinFrom = joinFrom;

    res.lang = lang;
    res.addNum = 'add-0';

    $UI.append(mainTpl(res)); //主模版
    $('.member-card-append').append(memberCardTpl(res)); //member card

    // handle image
    for (let i = 0; i < res.data.length; i++) {
        if (res.data[i].image) {
            res.data[i].image = JSON.parse(res.data[i].image);
        }
    }

    bankList.init(res); //初始化银行列表
    relationList.init(res); //初始化关系列表

    $UI.trigger('getProductRecmmend', res) //初始化互助商品推荐列表，默认先隐藏

    clickHandle.init(res); //点击事件
    utils.inputFourInOne(".member-ID[data-add='add-0']"); //限制4位一格

    phone = res.mobile;
    commonFooter.init(res);
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();
};

// 输入ID信息tips提示
obj.IDCardTipsShow = function(params) {
    // console.log('view.IDCardTipsShow=', params);
    $(".member-ID-tips[data-add='" + params.data_add + "']").html(params.res.data.products[product_id]);
    $(".member-ID-tips[data-add='" + params.data_add + "']").css('display', 'block');
    $(".tips-normal[data-add='" + params.data_add + "']").css('display', 'none');
}

export default obj;