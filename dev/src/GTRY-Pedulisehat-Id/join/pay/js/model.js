import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

// let reqObj = utils.getRequestParams();
// let showSP = reqObj['showSP'] || false; //是否展示shopeepay

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

//get payment
obj.getPayment = function(o) {
    let url = domainName.trade + '/v2/pay_channel' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '')
        // +
        // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    if (isLocal) {
        url = 'mock/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//submit
obj.createPayment = function(o) {
    let url = domainName.heouic + '/v1/heo_order';
    if (isLocal) {
        url = 'mock/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

//idcard_verify
obj.idcardVerify = function(o) {
    let url = domainName.heouic + '/v1/idcard_verify?id_card=' + o.param.id_card + '&product_id=' + o.param.product_id;

    if (isLocal) {
        url = 'mock/GTRY/v1_idcard_verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

// 互助商品推荐
obj.productRecmmend = function(o) {
    let url = domainName.heoproduct + '	/v1/product/recommend?product_id=' + o.param.product_id;

    if (isLocal) {
        url = 'mock/GTRY/v1_product_recmmend.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//活动：正在进行的互助折扣活动
obj.getPromotion = function(o) {
    let url = domainName.activity + '/v1/heo/promotion?activity_type=heo_promotion';

    if (isLocal) {
        url = 'mock/GTRY/v1_heo_promotion.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

// 活动：是否能参加互助奖励活动
obj.canJoinPromotion = function(o) {
    let url = domainName.heouic + '/v1/activity/promotion/can_join?product_id=' + o.param.activity_product_ids;

    if (isLocal) {
        url = 'mock/GTRY/v1_activity_promotion_can_join.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

/****
 *
    1. TnC
    https: //pedulisehat.freshdesk.com/support/solutions/articles/43000526225-syarat-dan-ketentuan-gotongroyong-untuk-penyakit-kritis

    2. Health Req
    https: //pedulisehat.freshdesk.com/support/solutions/articles/43000526217-persyaratan-kesehatan-gotongroyong-hadapi-penyakit-kritis

    3. Definition of 62 critical diseases:
        https: //pedulisehat.freshdesk.com/support/solutions/articles/43000526402-definisi-62-jenis-penyakit-kritis-yang-dibantu-program-gotongroyong-hadapi-penyakit-kritis
*/
obj.getArticle = function(o) {
    let url = domainName.base + '/v1/freshdesk/article/' + o.param.article_id;

    // if (isLocal) {
    //     url = '../mock/GTRY/v1_products.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;