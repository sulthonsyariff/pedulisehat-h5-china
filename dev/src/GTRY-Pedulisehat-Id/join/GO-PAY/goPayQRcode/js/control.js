import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let trade_id = utils.getRequestParams().trade_id;
let joinFrom = utils.getRequestParams().joinFrom;

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

judgeOrderState();

var int = setInterval(() => {
    judgeOrderState();
}, 3000);




view.init({});

/*
 * judge order state
 */
function judgeOrderState() {
    model.getOrderState({
        param: trade_id,
        success: function (res) {
            if (res.code == 0) {
                if (res.data && res.data.status == 'success') {
                    // if (appVersionCode > 128) {
                    //     location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' + res.data.project_id + '&order_id=' + res.data.order_id;
                    // } else {
                        location.href = res.data.redirect_url + '?trade_id=' + res.data.trade_id + '&terminal=' + res.data.terminal + '&joinFrom=' + joinFrom;
                    // }
                } else if (res.data && res.data.status == 'failed') {
                    location.href = res.data.redirect_url + '?trade_id=' + res.data.trade_id + '&terminal=' + res.data.terminal + '&joinFrom=' + joinFrom;
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}