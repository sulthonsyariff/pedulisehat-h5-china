// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
// import googleAnalytics from 'google.analytics'
import utils from 'utils'
import store from 'store'
import AppHasInstalled from 'AppHasInstalled';
import googleAnalytics from 'google.analytics.gtry'
// import sensorsActive from 'sensorsActive'

/* translation */
import goPayQRcode from 'goPayQRcode'
import qscLang from 'qscLang'
let lang = qscLang.init(goPayQRcode);
/* translation */

let [obj, $UI] = [{}, $('body')];
let trade_id = utils.getRequestParams().trade_id;


let CACHED_KEY = 'goPay';
let generate_qr_code = store.get(CACHED_KEY).generate_qr_code;
let deeplink_redirect = store.get(CACHED_KEY).deeplink_redirect;
// let deeplink_redirect = 'gojek://gopay/merchanttransfer?tref=1542080408150181126GCFJ&amount=200000&activity=GP:RR';

// judge app is installed or not
AppHasInstalled.AppHasInstalled(deeplink_redirect, lang.lang11);

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + 'GO-PAY'
    }


    res.JumpName = 'GO-PAY';
    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }


    res.lang = lang;
    res.getLanguage = utils.getLanguage();
    res.order_id = trade_id;
    res.generate_qr_code = generate_qr_code;
    $UI.append(mainTpl(res)); //主模版
    commonFooter.init(res);

    utils.hideLoading();
    fastclick.attach(document.body);




    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

// obj.judgeHasApp = function() {
//     // judge mobile or PC
//     // if in mobile，check have gojek app or not;
//     if (utils.browserVersion.mobile) {
//         var ifr = document.createElement('iframe');
//         ifr.src = deeplink_redirect;
//         ifr.style.display = 'none';
//         document.body.appendChild(ifr);

//         window.setTimeout(function() {
//             document.body.removeChild(ifr);
//             window.location.href = deeplink_redirect; // open app in mobile
//         }, 2000);
//     }
// }

export default obj;