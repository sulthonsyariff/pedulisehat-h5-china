import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

//gopay:check gopay order state
obj.getOrderState = function(o) {

    // gtry order_id 部分实际上传的是 trade_id
    var url = domainName.trade + '/v1/gopay_state_self?order_id=' + o.param +'&referer=fromPeduliSehat';
    
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

export default obj;