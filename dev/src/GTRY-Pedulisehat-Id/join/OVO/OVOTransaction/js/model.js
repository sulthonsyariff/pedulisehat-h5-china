import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getOrderState = function(o) {
    var url = domainName.trade + '/v1/ovo_state?order_id=' + o.param;
    if (isLocal) {
        url = 'mock/v1_ovo_support.json'
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

export default obj;