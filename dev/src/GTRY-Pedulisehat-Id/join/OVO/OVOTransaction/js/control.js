import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let amount = reqObj['amount'];
let order_id = reqObj['order_id'];
let trade_id = reqObj['trade_id'];
let mobile = reqObj['mobile'];
let from = reqObj['from'];
let joinFrom = reqObj['joinFrom'];

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))

/* translation */
import OVOTransaction from 'OVOTransaction'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(OVOTransaction);
/* translation */

let param = {};
param.lang = lang;
param.order_id = order_id;
param.trade_id = trade_id;
param.JumpName = titleLang.OVO;
param.mobile = mobile;

// 隐藏loading
utils.hideLoading();

view.init(param);

// 轮询结果
let timer = setInterval(() => {
    judgeOrderState();
}, 3000);

/*
 * judge order state
 1 未支付
 2 已支付
 3 提现
 4 退款
 5 返还
 10 手机号有问题
 100 取消订单
 */
function judgeOrderState() {
    model.getOrderState({
        param: trade_id,
        success: function (res) {
            if (res.code == 0) {
                // console.log('res = ', res);
                // 2 已支付
                if (res.data.state == 2) {
                    // if (appVersionCode > 128) {
                    //     location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' + project_id + '&order_id=' + order_id;
                    // } else {
                    location.href = '/paymentSucceed.html' + '?joinFrom=' + joinFrom;
                    // }
                }
                // 0 手机号有问题 ？
                else if (res.data.state == 10) {
                    store.set('ovoErrorMessageStore', res.data.detail);
                    // console.log('fail!');
                    location.href = '/paymentFailed.html?order_id=' + order_id + '&joinFrom=' + joinFrom;

                    // clearInterval(timer);
                }
                // 支付失败
                else if (res.data.state != 1) {
                    // console.log('fail!');
                    location.href = '/paymentFailed.html?order_id=' + order_id + '&joinFrom=' + joinFrom;

                }
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}