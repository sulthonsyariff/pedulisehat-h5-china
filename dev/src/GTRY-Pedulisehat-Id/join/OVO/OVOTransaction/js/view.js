import mainTpl from '../tpl/main.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
// import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics.gtry'
// import sensorsActive from 'sensorsActive'

let obj = {};
let $UI = $('body');
obj.UI = $UI;

obj.init = function(rs) {
    // rs.lang = lang;
    // rs.JumpName = titleLang.OVO;
    // rs.commonNavGoToWhere = '/ovo.html?order_id=' + rs.order_id + '&amount=' + rs.amount;
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }

    if (!utils.browserVersion.androids) {
        commonNav.init(rs);
    }

    rs.domainName = domainName;

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);

    fastclick.attach(document.body);
    utils.hideLoading();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    let sec = 30;

    let timer = setInterval(() => {
        if (sec > 0) {
            --sec;
            $('.black-bg.left').text(Math.floor(sec / 10));
            $('.black-bg.right').text(sec % 10);

        } else {
            console.log('close timer');
            clearInterval(timer);
        }
    }, 1000);
};




export default obj;