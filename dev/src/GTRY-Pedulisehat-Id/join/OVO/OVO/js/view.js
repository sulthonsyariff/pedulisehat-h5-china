import mainTpl from '../tpl/main.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
// import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'
import googleAnalytics from 'google.analytics.gtry'
// import sensorsActive from 'sensorsActive'

/* translation */
// import ovo from 'ovo'
// import qscLang from 'qscLang'
// import commonTitle from 'commonTitle'
// let titleLang = qscLang.init(commonTitle);
// let lang = qscLang.init(ovo);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let from = reqObj['from'];

// let amount = reqObj['amount'];
// let order_id = reqObj['order_id'];

obj.init = function(rs) {
    // rs.lang = lang;
    // rs.JumpName = titleLang.OVO;

    // rs.commonNavGoToWhere = '/';
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title='
    }


    if (!utils.browserVersion.androids) {
        commonNav.init(rs);
    }


    rs.domainName = domainName;
    rs.amount = changeMoneyFormat.moneyFormat(parseInt(rs.amount));
    // rs.order_id = order_id

    // 自动添加上0
    rs.phone = '0' + rs.phone.replace(/^0+/, "");

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);

    fastclick.attach(document.body);
    utils.hideLoading();

    // payBtn
    $('body').on('click', '.payBtn', function(e) {
        $UI.trigger('check_pay');
    });

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};




export default obj;