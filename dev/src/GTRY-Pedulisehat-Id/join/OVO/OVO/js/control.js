import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let amount = reqObj['amount'];
let order_id = reqObj['order_id'];
let trade_id = reqObj['trade_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];
let joinFrom = reqObj['joinFrom'];

let phone;

/* translation */
import ovo from 'ovo'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(ovo);
/* translation */

let param = {};
param.lang = lang;
param.amount = amount;
param.order_id = order_id;
param.trade_id = trade_id;
param.JumpName = titleLang.OVO;

model.getUserInfo({
    success: function (rs) {
        phone = rs.data.mobile;
        param.phone = phone;

        // 隐藏loading
        utils.hideLoading();
        view.init(param);
    },
    unauthorizeTodo: function (rs) {
        console.log('401');
    },
    error: utils.handleFail
});

// submit
$UI.on('check_pay', function (e) {
    // let mobile = $('#number').val().replace(/\b(0+)/gi, "");
    let mobile = $('#number').val();

    let postParam = {
        trade_id: trade_id,
        phone: mobile
    }

    if (check(postParam)) {

        console.log(postParam);

        model.ovoSupport({
            param: postParam,
            success: function (rs) {
                utils.showLoading(lang.lang8);

                if (rs.code == 0) {
                    console.log('rs = ', rs);
                    setTimeout(() => {

                        location.href = '/OVOTransaction.html?trade_id=' + trade_id +
                            '&mobile=' + mobile + '&order_id=' + order_id + '&joinFrom=' + joinFrom;
                    }, 300);
                } else {
                    utils.alertMessage(rs.msg)
                }
            },

            error: utils.handleFail
        });
    }
});

// check phone number
function check(params) {
    if (params.phone.length < 7) {
        utils.alertMessage(lang.lang1);
        return false;
    }

    return true;
}