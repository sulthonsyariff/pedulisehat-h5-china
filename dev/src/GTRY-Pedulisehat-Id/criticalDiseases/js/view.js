// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'

import alertBoxTplCriticalDisease from '../tpl/_alertBoxTplCriticalDisease.juicer' //62 Critical Diseases
import alertBoxTplCriticalDiseaseTerms from '../../../../common/qsc/tpl/GTRY_alertBoxTplCriticalDiseaseTerms.juicer' //plan Tnc

import alertBoxTplMembership from '../tpl/_alertBoxTplMembership.juicer' //关闭项目弹窗
import alertBoxTplHealthRequirement from '../../../../common/qsc/tpl/GTRY_alertBoxTplHealthRequirement.juicer' //关闭项目弹窗
import alertBoxTplJoinFree from '../tpl/_alertBoxTplJoinFree.juicer' //关闭项目弹窗
import alertBoxTplCovid from '../tpl/_alertBoxTplCovid.juicer' //关闭项目弹窗

import 'numberAnimate' // 数字滚动
import utils from 'utils'
import domainName from 'domainName'; //port domain name
import store from 'store'
// import sensorsActive from 'sensorsActive'
import shareGtry from 'shareGtry'

/* translation */
import gtryDetail from 'gtryDetail'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryDetail);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let [obj, $UI] = [{}, $('body')];

let memberNumRun;
let fundsAmountRun;
let reqObj = utils.getRequestParams();
let share = reqObj['share'] ? reqObj['share'] : '';
// console.log('-----share----', share)

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

obj.UI = $UI;

// 初始化
obj.init = function(res) {
    res.JumpName = lang.lang77;

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }

    res.getLanguage = utils.getLanguage();
    res.lang = lang;
    res.isAB = store.get('gtry_detail_template'); // GTRY-detail-A  GTRY-detail-B
    res.isAndroid = utils.browserVersion.android;
    $UI.append(mainTpl(res)); //主模版

    $UI.trigger('getJoinCount'); //获取滚动数字,确保主模版加载完后再加载数字部分
    commonFooter.init(res);
    fastclick.attach(document.body);

    //分享添加meta头部信息
    utils.changeFbHead({
        url: "https:" + domainName.share + '/gtry/' + res.data[0].product_id,
    });

    res.joinFrom = 'detail'

    // 修改home悬浮按钮的UI
    if ($('.app-download ').css('display') == 'block') {
        $('.home-btn').css('top', '113px');
    }
    $('body').on('click', '.appDownload-close', function(e) {
        $('.home-btn').css('top', '70px');
    });

    clickHandle(res);
    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensors.track('GTRYDetailView', {
    //     gtry_id: res.data[0].product_id,
    //     gtry_name: 'penyakitkritis',
    // })

};

// obj.refferalBonusUrl = function (res) {
//     //分享添加meta头部信息
//     utils.changeFbHead({
//         url: res.data.url,
//     });
// }

function clickHandle(res) {
    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        $('.bubble-icon').css('display', 'none');

        let paramObj = {
            item_id: (res && res.data) ? res.data[0].product_id : '',
            item_type: 0,
            // item_short_link: '',
            // remark: data.title, //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'gtry-share-wrapper', // 分享弹窗名
            shareCallBackName: 'gtryShareSuccess', //大病详情分享
            fromWhichPage: 'criticalDisease.html' || '' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            console.log('～～～调安卓分享～～～');

            let actTitle;
            let actDesc;

            if (res.state == 1) {
                actTitle = commonLang.lang36 //'Referral Bonus'
                actDesc = lang.lang54
            } else {
                actTitle = commonLang.lang26 //'Just One Click to Share Your Care'
                actDesc = ''
            }

            let paramString = "&actTitle=" + encodeURIComponent(actTitle) +
                "&actDesc=" + encodeURIComponent(actDesc)

            $UI.trigger('android-share', [paramString, paramObj]);

        } else {
            // 分享组件
            shareGtry.init(paramObj);

            $('.bubble-icon').css({
                'display': 'none'
            })
        }
    });

    // join
    $('body').on('click', '#join', function() {
            if (res && res.data && res.data[0].product_id) {
                if (user_id) {
                    location.href = '/join.html?product_id=' + res.data[0].product_id + '&share=' + share + '&joinFrom=detail';
                } else {
                    if (utils.browserVersion.android) {
                        location.href = 'qsc://app.pedulisehat/go/login?req_code=600'
                    } else {
                        location.href = '/join.html?product_id=' + res.data[0].product_id + '&share=' + share + '&joinFrom=detail';
                    }
                }

            }
        })
        // back to index
    $('body').on('click', '.home-btn', function() {

        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://qa.pedulisehat.id'
        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://pre.pedulisehat.id'
        } else {
            location.href = 'https://www.pedulisehat.id'

        }

    })

    // FAQ
    $('body').on('click', '.FAQ .list', function() {
        $(this).toggleClass('show');
    })

    // covid terms
    // $('body').on('click', '.covid-terms', function() {
    //     $UI.append(alertBoxTplCovid(res));
    //     $('.covid').css('display', 'block');
    // })


     // covid terms Req 43000526217
     $('body').on('click', '.covid-terms', function() {
        if (!$('.covid').length) {
            utils.showLoading();
            $UI.trigger('getArticle', [43000526217, callback]);

            function callback(res) {
                utils.hideLoading();

                $UI.append(alertBoxTplCovid(res));
                $('.covid .detail').html(res.data.description)
                $('.covid').css('display', 'block');
            }
        } else {
            $('.covid').css('display', 'block');
        }
    })

    // plan Tnc 43000526225
    $('body').on('click', '#convention-plan', function() {
        if (!$('.convention-plan').length) {
            utils.showLoading();
            $UI.trigger('getArticle', [43000526225, callback]);

            function callback(res) {
                utils.hideLoading();
                $UI.append(alertBoxTplCriticalDiseaseTerms(res));
                $('.convention-plan .detail').html(res.data.description)
                $('.convention-plan').css('display', 'block');
            }
        } else {
            $('.convention-plan').css('display', 'block');
        }
    })

    // 62 Critical Diseases 43000526402
    $('body').on('click', '#critical-diseases', function() {
        if (!$('.critical-diseases').length) {
            utils.showLoading();
            $UI.trigger('getArticle', [43000526402, callback]);

            function callback(res) {
                utils.hideLoading();

                $UI.append(alertBoxTplCriticalDisease(res));
                $('.critical-diseases .detail').html(res.data.description)
                $('.critical-diseases').css('display', 'block');
            }

        } else {
            $('.critical-diseases').css('display', 'block');
        }
    })

    // Health Req 43000526217
    $('body').on('click', '#health-requirement', function() {
        if (!$('.health-requirement').length) {
            utils.showLoading();
            $UI.trigger('getArticle', [43000526217, callback]);

            function callback(res) {
                utils.hideLoading();

                $UI.append(alertBoxTplHealthRequirement(res));
                $('.health-requirement .detail').html(res.data.description)
                $('.health-requirement').css('display', 'block');
            }
        } else {
            $('.health-requirement').css('display', 'block');
        }
    })

    // Terms of extension of membership
    $('body').on('click', '#membership', function() {
        if (!$('.membership').length) {
            $UI.append(alertBoxTplMembership(res));
        }
        $('.membership').css('display', 'block');
    })

    // join free toast
    if (res.pop.act == true) {
        $('body').on('click', '.join-free-click', function() {
            if (!$('.joinFree').length) {
                $UI.append(alertBoxTplJoinFree(res));
            }
            $('.joinFree').css('display', 'block');
        })
    }

    $('body').on('click', '.commonBoxStyle #gotIt', function() {
        $('.commonBoxStyle').css('display', 'none');
    })

    // whatsapp contact
    $('body').on('click', '.whatsappContact', function(e) {
        let appSrc = 'https://api.whatsapp.com/send?phone=6281230009479&text=Halo%20,%20Saya%20mau%20konsultasi%20Gotongroyong%20di%20pedulisehat.id';
        // in our android app
        if (utils.browserVersion.androids) {
            // native://intent?url=<gojet url>&errorInfo=<在此加上协议解析失败的提示 比如提示用户未安装app之类的>
            location.href = 'native://intent?url=' + encodeURIComponent(appSrc) + '&errorInfo=Error';
        } else {
            location.href = appSrc;

        }
    });
}

/**
 * 滚动数字初始化
 */
obj.numberAnimate = function(rs) {

    if (rs.data && rs.data.count && rs.data.total_money) {
        /*
         * 滚动数字： member num
         */
        memberNumRun = $('#member-num').numberAnimate({
            num: rs.data.count,
            speed: 1000,
            symbol: "."
        });

        /*
         * 滚动数字： funds amount
         */
        fundsAmountRun = $('#funds-amount').numberAnimate({
            num: rs.data.total_money,
            speed: 1000,
            symbol: "."
        });
    }
}

obj.resetData = function(rs) {
    memberNumRun.resetData(rs.data.count);
    fundsAmountRun.resetData(rs.data.total_money);
}


export default obj;