import view from './view'
import model from './model'
import donationMsgSwiper from './_donation-msg-swiper'
import _publication from './_publication'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'

/* translation */
import gtryDetail from 'gtryDetail'
import qscLang from 'qscLang'
let lang = qscLang.init(gtryDetail);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let $UI = view.UI;
let product_id;
let actTitle;
let actDesc;
let reqObj = utils.getRequestParams();
let statistics_link = reqObj['statistics_link'] || '';
let share = reqObj['share'] ? reqObj['share'] : '';
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let forwarding_default;
let forwarding_desc;
let shareUrl;
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'));
let androidOldUrlArr;
let androidOldUrl;

window.appHandle = function(req_code) {
    location.href = location.reload();
}

if (utils.browserVersion.androids) {
    location.href = 'native://do.something/setTitle?title=' + 'Program GTRY'
}

if (statistics_link) {
    model.shareAccessCount({
        param: {
            statistics_link: statistics_link,
            source_type: 0,
            visitor_id: user_id,
            terminal: 'H5',
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('submit share count')
            } else {
                utils.alertMessage(rs.msg)
            }
        },
    })
}

/**** AB测试相关代码 ****/
/**** AB测试相关代码 ****/
/**** AB测试相关代码 ****/
let isAB; // GTRY-detail-A  GTRY-detail-B
if (store.get('gtry_detail_template')) {
    isAB = store.get('gtry_detail_template');
} else {
    if (Math.floor(Math.random() * 10 + 1) < 6) {
        isAB = "a"
    } else {
        isAB = "b"
    };

    store.set('gtry_detail_template', isAB);
    createNewABTemplate(isAB); // 新生成模板次数
}

// 看到模板次数,页面每刷新一次调一次接口
model.addABTemplate({
    param: isAB == 'a' ? 'GTRY-detail-A' : 'GTRY-detail-B',
    success: function(rs) {
        if (rs.code == 0) {} else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});

// 新生成模板次数
function createNewABTemplate(param) {

    model.createNewABTemplate({
        param: isAB == 'a' ? 'GTRY-detail-A' : 'GTRY-detail-B',
        success: function(rs) {
            if (rs.code == 0) {} else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
};
/**** AB测试相关代码 ****/
/**** AB测试相关代码 ****/
/**** AB测试相关代码 ****/



model.getUserInfo({
    success: function(rs) {

        getActivityState(rs);
    },
    unauthorizeTodo: function(rs) {

        utils.hideLoading();
        getActivityState(rs);


        // setTimeout(function () {
        //     location.href = utils.browserVersion.android ?
        //         'qsc://app.pedulisehat/go/login' : ('https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?loginFrom=gtryPay&qf_redirect=' + encodeURIComponent(location.href))
        // }, 30)


    },
    error: utils.handleFail
});


function getActivityState(res) {
    model.getActivityState({
        success: function(rs) {
            if (rs.code == 0) {
                if (rs.data.state == 1) {
                    actTitle = commonLang.lang36
                    actDesc = lang.lang54
                } else {
                    actTitle = commonLang.lang26
                    actDesc = ''
                }

                getProducts(rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}


// 获取商品列表
function getProducts(res) {
    model.getProducts({
        success: function(rs) {
            if (rs.code == 0) {
                console.log('actTitle', actTitle, actDesc)

                product_id = (rs && rs.data) ? rs.data[0].product_id : '';
                rs.state = res.data.state
                console.log('rs=', rs);
                getActivityPop(rs);
                // view.init(rs);

                // 隐藏loading
                utils.hideLoading();
                getSwiperDonationMsg();
                // getShareInfo(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });

}

$UI.on('getJoinCount', function() {
    // 确保主模版加载完后再加载数字部分
    //  获取滚动数字
    model.getJoinCount({
        param: {
            product_id: product_id
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('rs=', rs);
                view.numberAnimate(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
    //  获取滚动数字
});

function getActivityPop(res) {
    model.getActivityPop({

        success: function(rs) {
            if (rs.code == 0) {

                res.pop = rs.data
                console.log('pop', res)
                view.init(res);
                getPublicityLast(res);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail

    })
}


// function getShareInfo(res) {
//     model.getShareInfo({
//         param: {
//             item_id: product_id,
//             item_type: 0,
//         },
//         success: function(rs) {
//             if (rs.code == 0) {
//                 forwarding_default = rs.data.forwarding_default;
//                 forwarding_desc = rs.data.forwarding_desc;
//                 shareUrl = rs.data.url

//                 if (appVersionCode <= 190) {
//                     androidOldUrlArr = rs.data.url.split("/")
//                     androidOldUrl = androidOldUrlArr[0] + '/' + androidOldUrlArr[1] + '/' + androidOldUrlArr[2] + '/gtry/' + androidOldUrlArr[3]
//                 } else {
//                     androidOldUrl = rs.data.url
//                 }

//                 // 如果是安卓端： 吊起安卓分享弹窗
//                 $UI.on('android-share', function(e) {

//                     let android_share_url =
//                         "native://share/webpage?url=" + encodeURIComponent(androidOldUrl) +
//                         "&forwardingDesc=" + encodeURIComponent(forwarding_default + ' %s') +
//                         "&actTitle=" + encodeURIComponent(actTitle) +
//                         "&actDesc=" + encodeURIComponent(actDesc) +
//                         "&forwardingMap=" + encodeURIComponent(JSON.stringify(forwarding_desc));


//                     location.href = android_share_url;

//                 })

//             } else {
//                 utils.alertMessage(rs.msg)
//             }
//         },
//         error: utils.handleFail

//     })
// }

setInterval(() => {
    model.getJoinCount({
        param: {
            product_id: product_id
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('rs=', rs);

                view.resetData(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}, 120000);

/*
referral bonus
*/
// $UI.on('referralBonus', function(e, target_type) {
//     console.log('===referralBonus===')
//     model.referralBonus({
//         param: {
//             from: JSON.stringify(target_type),
//             third_id: product_id,
//         },
//         success: function(res) {
//             if (res.code == 0) {
//                 res.data.from = target_type;
//                 // view.refferalBonusUrl(res);
//             } else {
//                 utils.alertMessage(rs.msg);
//             }
//         },
//         error: utils.handleFail
//     });
// });




/*
监听分享成功回调
*/
$UI.on('gtryShareSuccess', function(e, target_type) {
    console.log('===gtryShareSuccess===')

    // share_type 1: project 2: group 3: GTRY
    // model.projectShare({
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: product_id,
            item_type: 0,
            scene: 1,
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});

//捐款信息轮播
function getSwiperDonationMsg(rs) {
    console.log('getSwiperDonationMsg')
    model.getSwiperDonationMsg({
        success: function(res) {
            if (res.code == 0) {
                res.lang = lang;
                //公告轮播
                console.log('getSwiperDonationMsg----1')
                if (res.data) {
                    donationMsgSwiper.init(res);
                }
            }
        }
    })
}

//获取公示总览
function getPublicityLast(res) {
    model.getPublicityLast({
        success: function(rs) {
            if (rs.code == 0) {
                getPublicMembers(rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}
//获取公示列表
function getPublicMembers(res) {
    model.getPublicMembers({
        success: function(rs) {
            if (rs.code == 0) {
                rs.getPublicityLast = res.data
                _publication.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}

window.appHandle = function(req_code) {
    if (req_code == 600) {
        location.href = '/join.html?product_id=' + product_id + '&share=' + share;
    }
}
window.changeShareNum = function(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: product_id,
            item_type: 0,
            scene: 1,
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}

// getArticle
$UI.on('getArticle', function(e, article_id, callback) {
    getArticle(article_id, callback);
});

function getArticle(article_id, callback) {
    model.getArticle({
        param: {
            article_id: article_id
        },
        success: function(res) {
            if (res.code == 0) {

                res.lang = lang;
                if (callback) {
                    callback(res);
                }

            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    })
}