// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'

// import picCover from './_picCover'
import store from 'store'
// validate the form

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import utils from 'utils'

/* translation */
import gtryDisease from 'gtryDisease'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryDisease);
/* translation */

/* storage key */
let getLanguage = utils.getLanguage();


let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let product_type = reqObj['product_type'];
let product_id = reqObj['product_id'];
let member_card_id = reqObj['member_card_id'];
let patient_ktp_number = reqObj['patient_ktp_number'];

// 初始化
obj.init = function (res) {
    // if (utils.browserVersion.android) {
    //     location.href = 'native://do.something/setTitle?title=' + lang.lang14
    // }

    res.JumpName = lang.lang2;
    res.product_type = product_type;

    // if (!utils.browserVersion.android) {
    //     commonNav.init(res);
    // }
    res.lang = lang;
    console.log('view=', res)


    $UI.append(mainTpl(res)); //主模版


    // 隐藏loading
    utils.hideLoading();


    // got it
    $('body').on('click', '.next-btn-wrap', function () {
        location.href = '/membershipCard.html'
    })



    commonFooter.init(res);


    fastclick.attach(document.body);
    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

export default obj;