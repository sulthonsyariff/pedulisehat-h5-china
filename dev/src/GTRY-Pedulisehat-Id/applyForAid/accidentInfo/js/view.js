// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import imgUploader from 'uploadCloudinaryMore'
import locationChoose from 'locationChoose'

// import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import validate from './_validate' // validate the form

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryDisease from 'gtryDisease'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryDisease);
/* translation */

/* storage key */
let CACHED_KEY = 'gtrygtryDiseaseInfo';
let getLanguage = utils.getLanguage();

/* upload img */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : {};
/* upload img */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let product_type = parseInt(reqObj['product_type']);
let product_id = reqObj['product_id'];
let member_card_id = reqObj['member_card_id'];
let patient_ktp_number = reqObj['patient_ktp_number'];
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang_2
    }

    res.JumpName = lang.lang_2;
    res.product_type = product_type;

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    console.log('view=', res)


    $UI.append(mainTpl(res)); //主模版

    res.locationChooseType = 'hospitalChoose'
    locationChoose.init(res);

    // 隐藏loading
    utils.hideLoading();

    commonFooter.init(res);

    // 从本地缓存中读取图片
    if (datas && datas.images) {
        imgUploader.setImageList(uploadList, uploadKey, datas.images);
    }
    $('.fancybox').fancybox();

    setupUIHandler(); //Main Js

    fastclick.attach(document.body);
    cliclHandle(res);
    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {


    // got it
    $('body').on('click', '.commonBoxStyle #gotIt', function() {
        $('.commonBoxStyle').css('display', 'none');
    })

    // submit
    $('.next-btn').on('click', function() {
        let submitData = getSubmitParam();
        if (validate.check(submitData, lang)) {
            // $('.submitBtn').addClass('disabled');
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang21);
        }
    });
}

function setupUIHandler() {
    // judge
    // initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
    hideOrShowInputButton.init(); //show or hide upload btn

    //set cover
    // picCover.setCover(lang);

}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    hideOrShowInputButton.init(); //show or hide upload btn

    imgUploader.refreshPictureNumber(uploadList);
    updateCache();
    $('.fancybox').fancybox();


}

/**
 * //Update local cache
 */
function updateCache() {
    store.set(CACHED_KEY, getSubmitParam());
}


function getSubmitParam() {
    var hospital_address
    if ($('.region-choose-wrap').attr('hospital-id') == 0) {
        hospital_address = $('.region-choose-wrap').attr('province-name') + ',' + $('.region-choose-wrap').attr('city-name') + ',' + $('#patient-hospital').val()
    } else {
        hospital_address = $('.region-choose-wrap').attr('province-name') + ',' + $('.region-choose-wrap').attr('city-name') + ',' + $('.region-choose-wrap').attr('hospital-name')
    }

    return {
        product_id: product_id,
        product_type: product_type,
        member_card_id: member_card_id,
        patient_ktp_number: patient_ktp_number,
        step: 2,
        accident_type: $('#patient-accident').val(),

        hospital_address: hospital_address,

        // hospital_id: parseInt($('.region-choose-wrap').attr('hospital-id')),
        accident_detail: $('#accident-detail').val(),

        diagnosis_images: imgUploader.getImageList(uploadList, uploadKey),


    }
}
export default obj;