import view from './view'
import model from './model'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();
let member_card_id = reqObj['member_card_id'];



model.getBankList({
    success: function(res) {
        if (res.code == 0) {
            res.data.bankList = res.data
            view.init(res);

        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail,
})


// model.getMemberCardDetail({
//     param: member_card_id,
//     success: function(rs) {
//         if (rs.code == 0) {
//             // view.init(rs);
//         } else {
//             utils.alertMessage(rs.msg)
//         }
//     },
//     error: utils.handleFail,
// });


// UI.on('idcardVerify', function (e, params) {
//     model.idcardVerify({
//         param: params,
//         success: function (res) {
//             if (res.code == 0) {
//                 console.log('idcardVerify',res)
//             } else {
//                 utils.alertMessage(res.msg)
//             }
//         },
//         error: utils.handleFail
//     })
// })
UI.on('submit', function (e, params) {
    submitPayeeInfo(params)
})

function submitPayeeInfo(params) {
    model.submitPayeeInfo({
        param: params,
        success: function (res) {
            if (res.code == 0) {
                console.log('submitPatientInfo',res)
                location.href = '/applyFinish.html' 
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}