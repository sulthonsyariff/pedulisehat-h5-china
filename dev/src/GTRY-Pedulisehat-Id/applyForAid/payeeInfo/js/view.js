// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import imgUploader from 'uploadCloudinaryMore'

// import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import relationChoose from './_relationChoose' // validate the form

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryPayee from 'gtryPayee'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPayee);
/* translation */

/* storage key */
let CACHED_KEY = 'gtryPayeeInfo';
let getLanguage = utils.getLanguage();

/* upload img */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : {};
/* upload img */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let product_type = reqObj['product_type'];
let product_id = reqObj['product_id'];
let member_card_id = reqObj['member_card_id'];

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang14
    }

    res.JumpName = lang.lang3;
    res.product_type = product_type;

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    res.member_card_id = member_card_id
    console.log('view=', res)


    $UI.append(mainTpl(res)); //主模版

    relationChoose.init(res);
    // 隐藏loading
    utils.hideLoading();

    commonFooter.init(res);

    // 从本地缓存中读取图片
    if (datas && datas.images) {
        imgUploader.setImageList(uploadList, uploadKey, datas.images);
    }
    $('.fancybox').fancybox();

    // setupUIHandler(); //Main Js

    fastclick.attach(document.body);
    cliclHandle(res);

    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {


    // got it
    $('body').on('click', '.commonBoxStyle #gotIt', function() {
        $('.commonBoxStyle').css('display', 'none');
    })

    // $('.relatives-next-btn').on('click', function () {
    //     let submitData = getRelativesSubmitParam();
    //     console.log('=====',submitData)
    //     // if (relativesValidate.check(submitData, lang)) {
    //     //     // $('.submitBtn').addClass('disabled');
    //     //     $UI.trigger('submit', [submitData]);
    //     //     utils.showLoading(lang.lang26);
    //     // }
    // });


}

// function setupUIHandler() {
//     // judge
//     // initialize img upload
//     imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
//     hideOrShowInputButton.init(); //show or hide upload btn

//     //set cover
//     // picCover.setCover(lang);

// }

/**
 * img changed 监听事件
 */
// function imgChangedHandler() {
//     hideOrShowInputButton.init(); //show or hide upload btn

//     imgUploader.refreshPictureNumber(uploadList);
//     updateCache();
//     $('.fancybox').fancybox();


// }

/**
 * //Update local cache
 */
// function updateCache() {
//     store.set(CACHED_KEY, getSubmitParam());
// }


// function getRelativesSubmitParam() {
//     return {
//         // product_id: product_id,
//         step: 3,
//         payee_relationship: 1,
//         payee_bank_account_number: $("#bank-number").val(),
//         bank_name: $(".bank-name").text(),
//         payee_name: $(".payee-name").text(),
//         payee_bank_image: imgUploader.getImageList(uploadListRelativesBank, uploadKeyRelativesBank),
//         payee_ktp_number: $('#patient-ktp').val().replace(/\s|\xA0/g, ""),
//         payee_mobile: $('#phoneNum').val(),
//         payee_family_card_image: imgUploader.getImageList(uploadListFamilyBook, uploadKeyFamilyBook),
//     }
// }
export default obj;