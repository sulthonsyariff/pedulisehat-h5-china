import bankList from 'bank-list'
import imgUploader from 'uploadCloudinaryMore'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import relativesValidate from './_relativesValidate' // validate the form
import store from 'store'
import utils from 'utils'

import 'fancybox'

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;


/* upload relative bankbook*/
let uploadWrapperRelativesBank = '#uploadWrapper-relativesBank';
let uploadBtnRelativesBank = 'upload-btn-relativesBank'; // upload btn
let uploadListRelativesBank = '.uploadList-relativesBank'; // img container
let uploadKeyRelativesBank = 'cover-relativesBank'; // upload key
let uploadNumLimitRelativesBank = 1; // limit number
/* ID upload relative */


/* family book upload */
let uploadWrapperFamilyBook = '#uploadWrapper-familyBook';
let uploadBtnFamilyBook = 'upload-btn-familyBook'; // upload btn
let uploadListFamilyBook = '.uploadList-familyBook'; // img container
let uploadKeyFamilyBook = 'cover-familyBook'; // upload key
let uploadNumLimitFamilyBook = 1; // limit number
/* family book upload */



obj.init = function (rs) {

    //限制4位一格
    // utils.inputFourInOne('#bank-number');
    utils.inputFourInOne('#patient-ktp');

    rs.data.payment_channel = rs.data.bankList
    for (let i = 0; i < rs.data.payment_channel.length; i++) {
        rs.data.payment_channel[i].pg_code = rs.data.bankList[i].id;
        rs.data.payment_channel[i].pg_name = rs.data.bankList[i].name;
    }
    bankList.init(rs);

    // imgUploaderHandle(rs); //图片上传相关

    clickHandle(rs)


}

function clickHandle(rs) {
    $('.bank').on('click', function () {
        $('.alert-bank').css('display', 'block')
    })

    // // submit
    // $('.relatives-next-btn').on('click', function () {
    //     let submitData = getRelativesSubmitParam();
    //     console.log('=====',submitData)
    //     // if (relativesValidate.check(submitData, lang)) {
    //     //     // $('.submitBtn').addClass('disabled');
    //     //     $UI.trigger('submit', [submitData]);
    //     //     utils.showLoading(lang.lang26);
    //     // }
    // });
}

/**
 * 图片上传相关
 */
// function imgUploaderHandle(rs) {
//     // 从本地缓存中读取图片
//     // if (rs.data.card_images) {
//     //     imgUploader.setImageList(uploadList, uploadKey, JSON.parse(rs.data.card_images));
//     //     imgUploader.setImageList(uploadList2, uploadKey2, JSON.parse(rs.data.disease_images));
//     // }

//     // 控制图片上传按钮显示或者隐藏 ‘+’
//     hideOrShowInputButton.init();

//     // initialize img upload
//     imgUploader.create(uploadBtnRelativesBank, uploadListRelativesBank, uploadKeyRelativesBank, uploadNumLimitRelativesBank, imgChangedHandlerRelativesBank, uploadWrapperRelativesBank);
//     imgUploader.create(uploadBtnFamilyBook, uploadListFamilyBook, uploadKeyFamilyBook, uploadNumLimitFamilyBook, imgChangedHandlerFamilyBook, uploadWrapperFamilyBook);
// }



// function imgChangedHandlerFamilyBook() {
//     // console.log('initiate info img changed2');
//     imgUploader.refreshPictureNumber(uploadListFamilyBook);
//     $('.fancybox').fancybox();
//     //Control image upload button to show or hide
//     hideOrShowInputButton.init();
// }

// function imgChangedHandlerRelativesBank() {
//     // console.log('initiate info img changed2');
//     imgUploader.refreshPictureNumber(uploadListRelativesBank);
//     $('.fancybox').fancybox();
//     //Control image upload button to show or hide
//     hideOrShowInputButton.init();
// }

// function getRelativesSubmitParam() {
//     return {
//         // product_id: product_id,
//         step: 3,
//         payee_relationship: 1,
//         payee_bank_account_number: $("#bank-number").val(),
//         bank_name: $(".bank-name").text(),
//         payee_name: $(".payee-name").text(),
//         payee_bank_image: imgUploader.getImageList(uploadListRelativesBank, uploadKeyRelativesBank),
//         payee_ktp_number: $('#patient-ktp').val().replace(/\s|\xA0/g, ""),
//         payee_mobile: $('#phoneNum').val(),
//         payee_family_card_image: imgUploader.getImageList(uploadListFamilyBook, uploadKeyFamilyBook),
//     }
// }

export default obj;