/*
 * validate form
 */
import utils from 'utils'


let obj = {};

obj.check = function(param, lang) {
    console.log('validate', 'param:', param);

    if (param.payee_bank_account_number == '') {
        utils.alertMessage(lang.lang28);
        return false;
    }
    if (param.bank_name == lang.lang13) {
        utils.alertMessage(lang.lang29);
        return false;
    }
    if (param.payee_name == '') {
        utils.alertMessage(lang.lang30);
        return false;
    }
    if (param.payee_bank_image.length < 1) {
        utils.alertMessage(lang.lang31);

        return false;
    }
    if (param.payee_ktp_number == '') {
        utils.alertMessage(lang.lang32);
        return false;
    }
    if (param.payee_mobile == '') {
        utils.alertMessage(lang.lang33);
        return false;
    }
    if (param.payee_mobile.length < 7 || param.payee_mobile.length > 14) {
        utils.alertMessage(lang.lang33);
        return false;
    }

    return true;
}

export default obj;