import relationChooseTpl from '../tpl/_relationToast.juicer'
import patientHimsel from '../js/_patientHimself';
import patientHimselfTpl from '../tpl/_patientHimself.juicer'
import selfValidate from './_selfValidate' // validate the form

import patientRelatives from '../js/_patientRelatives';
import patientRelativesTpl from '../tpl/_patientRelatives.juicer'
import relativesValidate from './_relativesValidate' // validate the form


import alertBankBoxTpl from '../tpl/_alertBankBox.juicer'
import alertFamilyBoxTpl from '../tpl/_alertFamilyBox.juicer'

import store from 'store'
import utils from 'utils'
import imgUploader from 'uploadCloudinaryMore'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import 'fancybox'

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;


/* upload self bankbook*/
let uploadWrapperSelfBank = '#uploadWrapper-selfBank';
let uploadBtnSelfBank = 'upload-btn-selfBank'; // upload btn
let uploadListSelfBank = '.uploadList-selfBank'; // img container
let uploadKeySelfBank = 'cover-selfBank'; // upload key
let uploadNumLimitSelfBank = 1; // limit number
/* ID upload relative */

/* upload relative bankbook*/
let uploadWrapperRelativesBank = '#uploadWrapper-relativesBank';
let uploadBtnRelativesBank = 'upload-btn-relativesBank'; // upload btn
let uploadListRelativesBank = '.uploadList-relativesBank'; // img container
let uploadKeyRelativesBank = 'cover-relativesBank'; // upload key
let uploadNumLimitRelativesBank = 1; // limit number
/* ID upload relative */


/* family book upload */
let uploadWrapperFamilyBook = '#uploadWrapper-familyBook';
let uploadBtnFamilyBook = 'upload-btn-familyBook'; // upload btn
let uploadListFamilyBook = '.uploadList-familyBook'; // img container
let uploadKeyFamilyBook = 'cover-familyBook'; // upload key
let uploadNumLimitFamilyBook = 1; // limit number
/* family book upload */


let reqObj = utils.getRequestParams();
let product_type = parseInt(reqObj['product_type']);

// let product_type = reqObj['product_type'];
let product_id = reqObj['product_id'];
let member_card_id = reqObj['member_card_id'];
let patient_ktp_number = reqObj['patient_ktp_number'];
let chooseTpl = ''

obj.init = function (rs) {
    console.log('relationChoose', rs)
    $UI.append(relationChooseTpl(rs)); //主模版

    $('.patient-himself-wrap').append(patientHimselfTpl(rs)); //主模版

    $('.patient-relatives-wrap').append(patientRelativesTpl(rs)); //主模版

    UI(rs);
    imgUploaderHandle(rs); //图片上传相关
    sumbitHandle(rs)
    //限制4位一格
    // utils.inputFourInOne("#patient-KTP");
    // utils.inputFourInOne(".bank-number");

    $UI.append(alertBankBoxTpl(rs)); //验证弹窗
    $UI.append(alertFamilyBoxTpl(rs)); //验证弹窗


}

function UI(rs) {
    if (store.get('chooseTpl')) {
        if (store.get('chooseTpl') == 'self') {
            $('.choose-relation').html(rs.lang.lang6)

            $(".patient-relatives-wrap").css('display', 'none');
            $(".patient-himself-wrap").css('display', 'block');
            patientHimsel.init(rs)

            $(".relatives-next").css('display', 'none');
            $(".self-next").css('display', 'block');

        } else if (store.get('chooseTpl') == 'relatives') {
            $('.choose-relation').html(rs.lang.lang7)

            $(".patient-himself-wrap").css('display', 'none');
            $(".patient-relatives-wrap").css('display', 'block');
            patientRelatives.init(rs)

            $(".relatives-next").css('display', 'block');
            $(".self-next").css('display', 'none');

            // utils.inputFourInOne('#patient-ktp');


        } else {
            $(".patient-himself-wrap").css('display', 'none');
            $(".patient-relatives-wrap").css('display', 'none');
            // $(".next-btn-wrap").css('display', 'none');

        }
    }
    // patientHimsel.init(rs)

    // open choose ui
    $('body').on('click', '.choose-relation', function () {
        $('.relation-toast').css('display', 'block');
    })
    // close choose ui
    $('body').on('click', '.cancle-btn-wrap', function () {
        $('.relation-toast').css('display', 'none');
    })

    // 
    $('body').on('click', '.patient-btn', function () {
        chooseTpl = 'self'
        store.set('chooseTpl', chooseTpl)
        $(".patient-relatives-wrap").css('display', 'none');
        $(".patient-himself-wrap").css('display', 'block');

        $(".relatives-next").css('display', 'none');
        $(".self-next").css('display', 'block');

        patientHimsel.init(rs)
        $('.relation-toast').css('display', 'none');
        $('.choose-relation').html(rs.lang.lang6)
            .css({
                'color': '#333',
                'font-weight': 'bold',
            });
    })

    $('body').on('click', '.relatives-btn', function () {
        chooseTpl = 'relatives'
        store.set('chooseTpl', chooseTpl)
        $(".patient-himself-wrap").css('display', 'none');
        $(".patient-relatives-wrap").css('display', 'block');

        $(".relatives-next").css('display', 'block');
        $(".self-next").css('display', 'none');

        patientRelatives.init(rs)
        $('.relation-toast').css('display', 'none');
        $('.choose-relation').html(rs.lang.lang7)
            .css({
                'color': '#333',
                'font-weight': 'bold',
            });
    })


    // got it
    $('body').on('click', '.confirm', function () {
        $('.alert-bank-box').css('display', 'none');
        $('.alert-family-box').css('display', 'none');
    })
    // alert box
    $('body').on('click', '.self-bank-desc', function () {
        $('.alert-bank-box').css('display', 'block');
    })
    // alert box
    $('body').on('click', '.relatives-bank-desc', function () {
        $('.alert-bank-box').css('display', 'block');
    })
    // alert box
    $('body').on('click', '.family-card-desc', function () {
        $('.alert-family-box').css('display', 'block');
    })

}

function sumbitHandle(rs) {
    $('.relatives-next-btn').on('click', function () {
        let submitData = getRelativesSubmitParam();
        console.log('=====', submitData)
        if (relativesValidate.check(submitData, rs.lang)) {
            // $('.submitBtn').addClass('disabled');
            $UI.trigger('submit', [submitData]);
            utils.showLoading(rs.lang.lang45);
        }
    });
    $('.self-next-btn').on('click', function () {
        let submitData = getSelfSubmitParam();
        console.log('=====', submitData)
        if (selfValidate.check(submitData, rs.lang)) {
            // $('.submitBtn').addClass('disabled');
            $UI.trigger('submit', [submitData]);
            utils.showLoading(rs.lang.lang45);
        }
    });

}
/**
 * 图片上传相关
 */
function imgUploaderHandle(rs) {
    // 从本地缓存中读取图片
    // if (rs.data.card_images) {
    //     imgUploader.setImageList(uploadList, uploadKey, JSON.parse(rs.data.card_images));
    //     imgUploader.setImageList(uploadList2, uploadKey2, JSON.parse(rs.data.disease_images));
    // }

    // 控制图片上传按钮显示或者隐藏 ‘+’
    hideOrShowInputButton.init();

    // initialize img upload
    imgUploader.create(uploadBtnSelfBank, uploadListSelfBank, uploadKeySelfBank, uploadNumLimitSelfBank, imgChangedHandlerSelfBank, uploadWrapperSelfBank);
    imgUploader.create(uploadBtnRelativesBank, uploadListRelativesBank, uploadKeyRelativesBank, uploadNumLimitRelativesBank, imgChangedHandlerRelativesBank, uploadWrapperRelativesBank);
    imgUploader.create(uploadBtnFamilyBook, uploadListFamilyBook, uploadKeyFamilyBook, uploadNumLimitFamilyBook, imgChangedHandlerFamilyBook, uploadWrapperFamilyBook);
}



function imgChangedHandlerSelfBank() {
    // console.log('initiate info img changed2');
    imgUploader.refreshPictureNumber(uploadListSelfBank);
    $('.fancybox').fancybox();
    //Control image upload button to show or hide
    hideOrShowInputButton.init();
}

function imgChangedHandlerFamilyBook() {
    // console.log('initiate info img changed2');
    imgUploader.refreshPictureNumber(uploadListFamilyBook);
    $('.fancybox').fancybox();
    //Control image upload button to show or hide
    hideOrShowInputButton.init();
}

function imgChangedHandlerRelativesBank() {
    // console.log('initiate info img changed2');
    imgUploader.refreshPictureNumber(uploadListRelativesBank);
    $('.fancybox').fancybox();
    //Control image upload button to show or hide
    hideOrShowInputButton.init();
}

function getRelativesSubmitParam() {
    return {
        product_id: product_id,
        product_type: parseInt(product_type),
        member_card_id: member_card_id,
        patient_ktp_number: patient_ktp_number,
        step: 3,

        payee_relationship: 2,
        payee_bank_account_number: $("#relatives-bank-number").val(),
        bank_name: $(".bank-name").text(),
        bank_id: $(".bank-name").data('bank_id'),

        payee_name: $(".relatives-name").val(),
        payee_bank_image: imgUploader.getImageList(uploadListRelativesBank, uploadKeyRelativesBank),
        payee_ktp_number: $('#patient-ktp').val().replace(/\s|\xA0/g, ""),
        payee_mobile: $('#phoneNum-relatives').val().replace(/\s|\xA0/g, ""),
        payee_family_card_image: imgUploader.getImageList(uploadListFamilyBook, uploadKeyFamilyBook),
    }
}

function getSelfSubmitParam() {
    return {
        product_id: product_id,
        product_type: parseInt(product_type),
        member_card_id: member_card_id,
        patient_ktp_number: patient_ktp_number,
        step: 3,

        payee_relationship: 1,
        payee_bank_account_number: $(".self-bank-number").val(),
        bank_name: $(".bank-name").text(),
        bank_id: $(".bank-name").data('bank_id'),
        payee_name: $(".self-name").val(),
        payee_bank_image: imgUploader.getImageList(uploadListSelfBank, uploadKeySelfBank),
        payee_ktp_number: $('#patient-KTP').val().replace(/\s|\xA0/g, ""),
        payee_mobile: $('#phoneNum-self').val().replace(/\s|\xA0/g, ""),
    }
}

export default obj;