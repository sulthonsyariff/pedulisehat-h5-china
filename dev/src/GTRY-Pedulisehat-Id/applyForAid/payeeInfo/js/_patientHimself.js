import patientHimselfTpl from '../tpl/_patientHimself.juicer'
import bankList from 'bank-list'
import utils from 'utils'

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

obj.init = function (rs) {

    //限制4位一格
    utils.inputFourInOne('#patient-KTP');
    rs.data.payment_channel = rs.data.bankList
    for (let i = 0; i < rs.data.payment_channel.length; i++) {
        rs.data.payment_channel[i].pg_code = rs.data.bankList[i].id;
        rs.data.payment_channel[i].pg_name = rs.data.bankList[i].name;
    }
    bankList.init(rs);


    clickHandle(rs)

}

function clickHandle(rs) {
    $('.bank').on('click', function () {
        $('.alert-bank').css('display', 'block')
    })
}
export default obj;