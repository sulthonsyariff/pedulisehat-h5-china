let obj = {};

obj.init = function(e) {
    if ($('.uploadList img').length >= 8) {
        $('#webPicPicker').css('display', 'none');
    } else {
        $('#webPicPicker').css('display', 'block');
    }

    if ($('.uploadList-familyBook img').length >= 1) {
        // console.log('uploadList', $('.uploadList img').length)
        $('#webPicPicker-familyBook').css('display', 'none');
    } else {
        $('#webPicPicker-familyBook').css('display', 'block');
    }

    if ($('.uploadList-relativesBank img').length >= 1) {
        // console.log('uploadList', $('.uploadList img').length)
        $('#webPicPicker-relativesBank').css('display', 'none');
    } else {
        $('#webPicPicker-relativesBank').css('display', 'block');
    }
}

export default obj;