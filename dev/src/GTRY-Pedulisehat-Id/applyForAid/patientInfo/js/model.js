import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 会员详情
 */
obj.getMemberCardDetail = function(o) {
    let url = domainName.heouic + '/v1/member_card/' + o.param;

    if (isLocal) {
        url = '../mock/GTRY/v1_member_card_detail.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


//idcard_verify
obj.idcardVerify = function(o) {
    var url = domainName.heouic + '/v1/idcard_verify?id_card=' + o.param.id_card + '&product_id=' + o.param.product_id;

    if (isLocal) {
        url = 'mock/GTRY/idcard_verify.json?id_card=' + o.param.id_card + '&product_id=' + o.param.product_id;
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};


//地区列表
obj.locationList = function(o) {
    var url = domainName.base + '/v1/regions?pid=0';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//医院列表
obj.hospitalList = function(o) {
    var url = domainName.base + '/v1/hospitals';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};


//heo apply for aid step 1
obj.submitPatientInfo = function(o) {
    var url = domainName.heouic + '/v1/apply_aid';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};
export default obj;