import view from './view'
import model from './model'
import locationChoose from 'locationChoose'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();
let member_card_id = reqObj['member_card_id'];
let product_type = reqObj['product_type'];
let product_id = reqObj['product_id'];



function locationList(){

    model.locationList({
        success: function (rs) {
            if (rs.code == 0) {
                // view.init(rs);
    
                locationChoose.renderList(rs)
                console.log('location',rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}

UI.on('reloadRegionList', function(e, region_level) {
    // reloadRegionList(region_level);
});



model.hospitalList({
    success: function (rs) {
        if (rs.code == 0) {
            // view.init(rs);
            console.log('hospitalList',rs)
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});


model.getMemberCardDetail({
    param: member_card_id,
    success: function (rs) {
        if (rs.code == 0) {
            view.init(rs);

            locationList();
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});


// UI.on('idcardVerify', function (e, params) {
//     model.idcardVerify({
//         param: params,
//         success: function (res) {
//             if (res.code == 0) {
//                 console.log('idcardVerify',res)
//             } else {
//                 utils.alertMessage(res.msg)
//             }
//         },
//         error: utils.handleFail
//     })
// })
UI.on('submit', function (e, params) {
    model.submitPatientInfo({
        param: params,
        success: function (res) {
            if (res.code == 0) {
                console.log('submitPatientInfo', res)
                if (product_type == 2) {
                    location.href = '/accidentInfo.html?product_id=' + product_id + '&member_card_id=' + member_card_id + '&product_type=' + product_type + '&patient_ktp_number=' + params.patient_ktp_number

                } else if (product_type == 1) {
                    location.href = '/diseaseInfo.html?product_id=' + product_id + '&member_card_id=' + member_card_id + '&product_type=' + product_type + '&patient_ktp_number=' + params.patient_ktp_number
                }
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})