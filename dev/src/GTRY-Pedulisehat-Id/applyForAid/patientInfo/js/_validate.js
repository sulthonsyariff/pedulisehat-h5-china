/*
 * validate form
 */
import utils from 'utils'


let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    // if (param.patient_full_name == '') {
    //     utils.alertMessage(lang.lang2);
    //     return false;
    // }

    // if (param.patient_ktp_number.length && param.patient_ktp_number.length != 16) {
    //     utils.alertMessage(lang.lang2);
    //     return false;
    // }

    if (param.patient_ktp_image.length < 1) {
        utils.alertMessage(lang.lang21);

        return false;
    }

    if (param.patient_address == '') {
        utils.alertMessage(lang.lang22);
        return false;
    }
    if (param.patient_mobile == '' || param.patient_mobile.length < 7) {
        utils.alertMessage(lang.lang23);
        return false;
    }





    return true;
}

export default obj;