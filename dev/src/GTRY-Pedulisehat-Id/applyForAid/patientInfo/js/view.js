// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import alertBoxTpl from '../tpl/alert-box.juicer'
import locationChoose from 'locationChoose'

import imgUploader from 'uploadCloudinaryMore'

// import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import validate from './_validate' // validate the form

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryPatient from 'gtryPatient'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPatient);
/* translation */

/* storage key */
let CACHED_KEY = 'gtryPatientInfo';
let getLanguage = utils.getLanguage();

/* upload img */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 1; // limit number
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : {};
/* upload img */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let product_type = parseInt(reqObj['product_type']);
let product_id = reqObj['product_id'];
let member_card_id = reqObj['member_card_id'];

let patient_status = 1;
let patient_full_name;
let patient_ktp_number;
// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang1
    }

    res.JumpName = lang.lang1;
    res.product_type = product_type;

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    console.log('view=', res)

    patient_full_name = res.data.real_name
    patient_ktp_number = res.data.id_card

    // store.set('CACHED_KEY', {
    //     patient_full_name: res.data.real_name,
    //     patient_ktp_number: res.data.id_card,
    //     product_type: product_type,
    // })
    res.data.form_id = res.data.id_card.replace(/\D/g, '').replace(/(....)(?=.)/g, '$1 ')


    $UI.append(mainTpl(res)); //主模版
    $UI.append(alertBoxTpl(res)); //验证弹窗

    res.locationChooseType = 'regionChoose'
    locationChoose.init(res);

    // 隐藏loading
    utils.hideLoading();

    commonFooter.init(res);

    // 从本地缓存中读取图片
    if (datas && datas.images) {
        imgUploader.setImageList(uploadList, uploadKey, datas.images);
    }
    $('.fancybox').fancybox();

    setupUIHandler(); //Main Js

    fastclick.attach(document.body);
    cliclHandle(res);
    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {

    // focusout:ID card
    // $('body').on('focusout', "#patient-KTP", function (e) {
    //     console.log('focusout');
    //     $UI.trigger('idcardVerify', {
    //         product_id: product_id,
    //         id_card: $(e.currentTarget).val().replace(/\s|\xA0/g, ""),
    //     });

    // })
    // change status
    $('body').on('click', '.alive-wrap', function() {
        patient_status = 1
        $('.passaway-wrap').removeClass('choosed');
        $('.alive-wrap').addClass('choosed');
    })
    $('body').on('click', '.passaway-wrap', function() {
        patient_status = 2
        $('.alive-wrap').removeClass('choosed');
        $('.passaway-wrap').addClass('choosed');
    })

    // got it
    $('body').on('click', '.confirm', function() {
            $('.alert-box').css('display', 'none');
        })
        // alert box
    $('body').on('click', '.img-desc', function() {
        $('.alert-box').css('display', 'block');
    })


    // submit
    $('.next-btn').on('click', function() {
        let submitData = getSubmitParam();
        console.log('submitData', submitData)
        store.set(CACHED_KEY, getSubmitParam());

        if (validate.check(submitData, lang)) {
            // $('.submitBtn').addClass('disabled');
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang26);
        }
    });
}

function setupUIHandler() {
    // judge
    // initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
    hideOrShowInputButton.init(); //show or hide upload btn

    //set cover
    // picCover.setCover(lang);

}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    hideOrShowInputButton.init(); //show or hide upload btn

    imgUploader.refreshPictureNumber(uploadList);
    updateCache();
    $('.fancybox').fancybox();


}

/**
 * //Update local cache
 */
function updateCache() {
    store.set(CACHED_KEY, getSubmitParam());
}


function getSubmitParam() {
    return {
        product_id: product_id,
        product_type: product_type,
        member_card_id: member_card_id,
        patient_ktp_number: patient_ktp_number,
        step: 1,

        patient_full_name: patient_full_name,
        patient_status: patient_status,
        patient_ktp_image: imgUploader.getImageList(uploadList, uploadKey),
        patient_province: $('.region-choose-wrap').attr('province-name'),
        patient_city: $('.region-choose-wrap').attr('city-name'),
        patient_district: $('.region-choose-wrap').attr('district-name'),
        patient_address: $('#patient-address').val(),
        patient_mobile: $('#patient-phone').val(),

    }
}
export default obj;