/*
 * validate form
 */
import utils from 'utils'


let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    if (param.disease_name == '') {
        utils.alertMessage(lang.lang15);
        return false;
    }
    // if (param.hospital_id == '') {
    //     utils.alertMessage(lang.lang7);
    //     return false;
    // }
    // if (param.hospital_address == '') {
    //     utils.alertMessage(lang.lang7);
    //     return false;
    // }
    if ($('.region-choose-wrap').attr('hospital-id') && $('.region-choose-wrap').attr('hospital-id') == 0) {
        if (!$('#patient-hospital').val()) {
            utils.alertMessage(lang.lang_16);
            return false;
        }
    } else {
        if (!$('.region-choose-wrap').attr('hospital-name')) {
            utils.alertMessage(lang.lang16);
            return false;
        }
    }
    if (param.disease_detail == '') {
        utils.alertMessage(lang.lang17);
        return false;
    }
    if (param.diagnosis_images.length < 1) {
        utils.alertMessage(lang.lang18);

        return false;
    }



    return true;
}

export default obj;