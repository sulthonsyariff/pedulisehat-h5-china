let obj = {};

obj.init = function(e) {
    if ($('.uploadList img').length >= 8) {
        $('#webPicPicker').css('display', 'none');
    } else {
        $('#webPicPicker').css('display', 'block');
    }
}

export default obj;