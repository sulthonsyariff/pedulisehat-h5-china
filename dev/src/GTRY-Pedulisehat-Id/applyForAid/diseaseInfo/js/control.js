import view from './view'
import model from './model'
import locationChoose from 'locationChoose'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();
let member_card_id = reqObj['member_card_id'];
let product_type = reqObj['product_type'];
let product_id = reqObj['product_id'];




model.getMemberCardDetail({
    param: member_card_id,
    success: function(rs) {
        if (rs.code == 0) {
            view.init(rs);
            locationList();

        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});


// UI.on('idcardVerify', function (e, params) {
//     model.idcardVerify({
//         param: params,
//         success: function (res) {
//             if (res.code == 0) {
//                 console.log('idcardVerify',res)
//             } else {
//                 utils.alertMessage(res.msg)
//             }
//         },
//         error: utils.handleFail
//     })
// })
UI.on('submit', function (e, params) {
    model.submitPatientInfo({
        param: params,
        success: function (res) {
            if (res.code == 0) {
                console.log('submitPatientInfo',res)
                location.href = '/payeeInfo.html?product_id=' + product_id + '&member_card_id=' + member_card_id + '&product_type=' + product_type + '&patient_ktp_number=' + params.patient_ktp_number
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})

//初始化三级联动列表
function locationList(){
    model.locationList({
        success: function (rs) {
            if (rs.code == 0) {
                locationChoose.renderList(rs)
                console.log('location',rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}
