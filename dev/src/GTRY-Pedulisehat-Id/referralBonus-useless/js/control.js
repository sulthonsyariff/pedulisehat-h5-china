import view from './view'
import model from './model'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

/* translation */
import gtryReferralBonus from 'gtryReferralBonus'
import qscLang from 'qscLang'
let lang = qscLang.init(gtryReferralBonus);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let $UI = view.UI;
let first = true;
let reqObj = utils.getRequestParams();
let product_id;
let share_url;
let actTitle;
let actDesc;

model.getActivityState({
    success: function(rs) {
        if (rs.code == 0) {
            if (rs.data.state == 1) {
                actTitle = commonLang.lang36
                actDesc = lang.lang54
            } else {
                actTitle = commonLang.lang26
                actDesc = ''
            }
            getReferralList(rs)
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});


model.getMemberCards({
    success: function(rs) {
        if (rs.code == 0) {
            product_id = rs.data[0].product_id
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});

function getReferralList(res) {
    model.getReferralList({
        success: function(rs) {
            if (rs.code == 0) {
                console.log('actTitle', actTitle, actDesc)
                rs.state = res.data.state
                view.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}

/*
referral bonus
*/
$UI.on('referralBonus', function(e, target_type) {
    console.log('===referralBonus===')
    model.referralBonus({
        param: {
            from: JSON.stringify(target_type),
            third_id: product_id,
        },
        success: function(res) {
            if (res.code == 0) {
                res.data.from = target_type;
                // view.refferalBonusUrl(res);
                share_url = res.data.url
                gtryShareTpl.refferalBonusUrl(res);
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});

// 如果是安卓端： 吊起安卓分享弹窗
$UI.on('android-share', function(e) {

    model.referralBonus({
        param: {
            // from: JSON.stringify(1),
            third_id: product_id,
        },
        success: function(res) {
            if (res.code == 0) {
                // res.data.from = target_type;
                // view.refferalBonusUrl(res);
                let android_share_url =
                    "native://share/webpage?url=" + encodeURIComponent(res.data.url) +
                    // "&title=" + +
                    "&forwardingDesc=" + encodeURIComponent(commonLang.lang31 + ' %s') +
                    // "&image=" + +
                    // "&hashtag=" + +
                    // "&quote=" + +
                    "&actTitle=" + encodeURIComponent(actTitle) +
                    "&actDesc=" + encodeURIComponent(actDesc)
                console.log('android_share_url=', android_share_url);

                location.href = android_share_url;

            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });


})


/*
监听分享成功回调
*/
$UI.on('projectShare', function(e, target_type) {
    console.log('===projectShare===')

    // share_type 1: project 2: group 3: GTRY
    model.projectShare({
        param: {
            target_type: target_type,
            project_id: product_id,
            share_type: 3
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});