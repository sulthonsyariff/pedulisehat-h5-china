import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 会员列表
 */
obj.getMemberCards = function(o) {
    let url = domainName.heouic + '/v1/member_cards?page=1&limit=3';

    if (isLocal) {
        url = '../mock/GTRY/v1_member_cards_1.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/**
 * 活动状态
 */
obj.getActivityState = function(o) {
    let url = domainName.heouic + '/v1/hrr_state';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


// 邀请列表
obj.getReferralList = function(o) {
    let url = domainName.heouic + '/v1/referral';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


/*
    TargetTypefacebook = 1 // facebook
    TargetTypeWhatsApp = 2 // whatsapp
    TargetTypeTwitter = 3 // twitter
    TargetTypeLink = 4 // link
    TargetTypeLine = 5 // line
    TargetTypeInstagram = 6 // instagram
    TargetTypeYoutube = 7 // youtube

    share_type 1: project 2: group 3: GTRY
*/
obj.projectShare = function(o) {
    let url = domainName.project + '/v1/project_share';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}


obj.referralBonus = function(o) {
    let url = domainName.project + '/v1/share_user_relation';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}


export default obj;