// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import alertBoxTplHowToGetBonus from '../tpl/_alertBoxTplHowToGetBonus.juicer' //关闭项目弹窗

import gtryShareTpl from 'gtryShareTpl'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import changeMoneyFormat from 'changeMoneyFormat'
import timeHandle from './_timeHandle'
import utils from 'utils'

/* translation */
import gtryReferralBonus from 'gtryReferralBonus'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryReferralBonus);
/* translation */


let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang14
    }

    res.JumpName = lang.lang14;
    res.commonNavGoToWhere = '/membershipCard.html';
    if (res.data && res.data.total_reward) {
        res.data.total_reward = changeMoneyFormat.moneyFormat(res.data.total_reward)
    }
    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    console.log('view=', res)
    $UI.append(mainTpl(res)); //主模版
    $UI.append(alertBoxTplHowToGetBonus(res));

    // 隐藏loading
    utils.hideLoading();


    commonFooter.init(res);
    fastclick.attach(document.body);
    gtryShareTpl.init(res); //share弹窗

    clickHandle(res);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function clickHandle(res) {
    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', lang);
        } else {
            $('.common-share').css({
                'display': 'block'
            });
        }
    });
    $('body').on('click', '.invite-more', function(e) {

        if (utils.browserVersion.android) {
            $UI.trigger('android-share');
        } else {
            $('.common-share').css({
                'display': 'block'
            });

        }
    });

    //apply
    $('body').on('click', '.how-to-get-wrap', function(e) {
        $('.alert-box').css('display', 'block');

    });
    $('body').on('click', '.got-it-wrap', function(e) {
        $('.alert-box').css('display', 'none');
    });

}

export default obj;