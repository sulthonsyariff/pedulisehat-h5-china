var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'GTRY-Pedulisehat-Id',
    htmlFileURL: 'html/GTRY-Pedulisehat-Id/referralBonus.html',

    htmlOgUrl: 'https://gtry.pedulisehat.id',
    htmlOgTitle: 'Program GTRY untuk Penyakit Kritis',
    htmlOgDescription: 'Bergabunglah dengan program GTRY, bantuan untuk penyakit kritis dengan dana kompensasi hingga Rp 100.000.000.https://gtry.pedulisehat.id/ ',
    htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',

    appDir: 'js/GTRY-Pedulisehat-Id_referralBonus',
    uglify: true,
    hash: '',
    mode: 'production'
})