import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


obj.heoActiveCard = function(o) {
    let url = domainName.heouic + '/v1/active_card';
    ajaxProxy.ajax({
        type: 'put',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//idcard_verify
obj.idcardVerify = function(o) {
    var url = domainName.heouic + '/v1/idcard_verify?id_card=' + o.id_card + '&product_id=' + o.product_id;

    if (isLocal) {
        url = 'mock/GTRY/idcard_verify.json?id_card=' + o.param.id_card + '&product_id=' + o.param.product_id;
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};
export default obj;