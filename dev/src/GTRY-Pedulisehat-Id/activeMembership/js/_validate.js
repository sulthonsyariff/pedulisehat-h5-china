/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function (param, lang) {
    // console.log('validate', 'param:', param);

    
    if (!param.real_name) {
        utils.alertMessage(lang.lang8);
        return false;
    }
    if (param.id_card == '') {
        utils.alertMessage(lang.lang9);
        return false;
    }
    if (param.id_card.length && param.id_card.length != 16) {
        utils.alertMessage(lang.lang10);
        return false;
    }

    if (!param.relationship) {
        utils.alertMessage(lang.lang11);
        return false;
    }


    return true;
}

export default obj;