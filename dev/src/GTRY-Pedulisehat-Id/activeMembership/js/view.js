// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import relationList from './_relationList'
import validate from './_validate'

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryActiveMembership from 'gtryActiveMembership'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryActiveMembership);
/* translation */


let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let real_name = reqObj['real_name'];
let relationship = reqObj['relationship'];
let member_card_id = reqObj['member_card_id'];
// let product_id = reqObj['product_id'];
let product_type = reqObj['product_type'];

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang13
    }

    res.JumpName = lang.lang13;
    res.commonNavGoToWhere = '/membershipCard.html';

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;
    console.log('view=', res)
    res.product_type = product_type;

    $UI.append(mainTpl(res)); //主模版

    if (relationship == 1) {
        $(".relation-name").html(lang.lang21).attr('data-relation', '1');
    } else if (relationship == 2) {
        $(".relation-name").html(lang.lang22).attr('data-relation', '2');
    } else if (relationship == 3) {
        $(".relation-name").html(lang.lang23).attr('data-relation', '3');
    } else if (relationship == 4) {
        $(".relation-name").html(lang.lang24).attr('data-relation', '4');
    } else if (relationship == 5) {
        $(".relation-name").html(lang.lang25).attr('data-relation', '5');
    } else if (relationship == 6) {
        $(".relation-name").html(lang.lang26).attr('data-relation', '6');
    } else if (relationship == 7) {
        $(".relation-name").html(lang.lang27).attr('data-relation', '7');
    } else if (relationship == 8) {
        $(".relation-name").html(lang.lang28).attr('data-relation', '8');
    }

    utils.inputFourInOne(".member-ID");

    // 隐藏loading
    utils.hideLoading();

    relationList.init(res); //初始化关系列表

    commonFooter.init(res);
    fastclick.attach(document.body);
    cliclHandle(res);
    // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

function cliclHandle(rs) {
    // relation channel
    $('body').on('click', '.relation_channel_wrap', function(rs) {
        // 获取当前卡片
        // $data = $(this).data('add')
        // $thisCard = $(".member-card[data-add='" + $data + "']");

        $('.alert-relation').css('display', 'block');
        $('.relation-list-items').removeClass('choosed-relation');

        if ($(this).find('.relation-name')) {
            let dataRelation = $(this).find('.relation-name').attr('data-relation');
            $('.alert-relation').find(".relation-name[data-relation='" + dataRelation + "'] ").parents('.relation-list-items').addClass('choosed-relation');
        }
    })

    // focusout:ID card
    // $('body').on('focusout', ".member-ID", function (e) {
    //     console.log('focusout');

    //     // 如果为空
    //     if (!$(e.currentTarget).val()) {
    //         // $(".member-ID-tips[data-add='" + $(e.currentTarget).attr('data-add') + "']").html(lang.lang31);
    //     }
    //     // 焦点在哪校验哪
    //     // ID card 实时校验
    //     else {
    //         console.log('====开始校验后端接口====');
    //         $UI.trigger('idcardVerify', {
    //             product_id: product_id,
    //             id_card: $(e.currentTarget).val().replace(/\s|\xA0/g, ""),
    //         });
    //     }

    // })
    // pay:submit
    $('body').on('click', '.active-btn', function() {
        submitData = getSubmitParam();
        console.log('submitData', submitData);

        if (validate.check(submitData, lang)) {
            // $UI.trigger('idcardVerify', {
            //     product_id: product_id,
            //     id_card: submitData.id_card,
            // });
            $UI.trigger('idcardVerify', [submitData]);

            // utils.showLoading(lang.lang12);
            // setTimeout(() => {
            //     $UI.trigger('submit', [submitData]);
            // }, 300);

        }
    });
}

function getSubmitParam() {
    // relationship = $(".relation_channel_wrap .relation-name").attr('data-relation')

    return {
        member_card_id: member_card_id, // 已是会员充值 逗号分隔多个会员
        // relationship: parseInt(relationship),
        relationship: parseInt($(".relation_channel .relation-name").attr('data-relation')),
        real_name: $('.member-name').val(),
        id_card: $('.member-ID').val().replace(/\s|\xA0/g, ""),
    }
}


export default obj;