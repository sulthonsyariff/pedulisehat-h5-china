import mainTpl from '../tpl/_relationList.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let reqSelect = location.href
    /**
     * @param {返回按钮文字} res.JumpName
     */
obj.init = function(res) {

    res.domainName = domainName;
    res.commonLang = commonLang;
    res.isAndroid = utils.browserVersion.android;

    $UI.prepend(mainTpl(res));
    console.log('res=', res);

    fastclick.attach(document.body);
    obj.event(res);

};

obj.event = function(res) {
    $('.alert-relation .close').on('click', function(rs) {
        $('.alert-relation').css('display', 'none')
    })

    $('body').on('click', '.relation-list-items', function(e) {

        $('.relation-list-items').removeClass('choosed-relation');
        $(this).addClass('choosed-relation');

        // let $data = $('.alert-relation').attr('data-add');

        // console.log('2==', $data);

        setTimeout(() => {
            $('.alert-relation').css('display', 'none')
        }, 100)

        $(".relation_channel_wrap").find('.relation_channel').html($(this).html());
    })
}

export default obj;