import view from './view'
import model from './model'
import _publication from './_publication'
import 'freshchat.widget'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'


/* translation */
import gtryDetail from 'gtryDetail'
import qscLang from 'qscLang'
let lang = qscLang.init(gtryDetail);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let $UI = view.UI;
let product_id;
// let share_url;
let actTitle;
let actDesc;
let reqObj = utils.getRequestParams();
let statistics_link = reqObj['statistics_link'] || '';

let share = reqObj['share'] ? reqObj['share'] : '';
let forwarding_default;
let forwarding_desc;
let shareUrl;
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';



window.appHandle = function (req_code) {
    location.href = location.reload();
}

if (utils.browserVersion.androids) {
    location.href = 'native://do.something/setTitle?title=' + 'Program GTRY'
}
console.log('---------gtryHome---------')

if (statistics_link) {
    model.shareAccessCount({
        param: {
            statistics_link: statistics_link,
            source_type: 0,
            visitor_id: user_id,
            terminal: 'H5',
        },
        success: function (rs) {
            if (rs.code == 0) {
                console.log('submit share count')
            } else {
                utils.alertMessage(rs.msg)
            }
        },
    })
}
model.getUserInfo({
    success: function (rs) {

        getActivityState(rs);
    },
    unauthorizeTodo: function (rs) {

        utils.hideLoading();
        getActivityState(rs);


        // setTimeout(function () {
        //     location.href = utils.browserVersion.android ?
        //         'qsc://app.pedulisehat/go/login' : ('https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?loginFrom=gtryPay&qf_redirect=' + encodeURIComponent(location.href))
        // }, 30)


    },
    error: utils.handleFail
});


function getActivityState(res) {
    model.getActivityState({
        success: function (rs) {
            if (rs.code == 0) {
                if (rs.data.state == 1) {
                    actTitle = commonLang.lang36
                    actDesc = lang.lang54
                } else {
                    actTitle = commonLang.lang26
                    actDesc = ''
                }

                getProducts(rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}


// 获取商品列表
function getProducts(res) {
    model.getProducts({
        success: function (rs) {
            if (rs.code == 0) {
                console.log('actTitle', actTitle, actDesc)

                product_id = (rs && rs.data) ? rs.data[0].product_id : '';
                rs.state = res.data.state
                console.log('rs=', rs);

                getActivityPop(rs)
                // view.init(rs);
                // 隐藏loading
                utils.hideLoading();
                // getShareInfo(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });

}

//  获取滚动数字
$UI.on('getJoinCount', function () {
    console.log('===获取滚动数字===');

    // 确保主模版加载完后再加载数字部分
    model.getJoinCount({
        success: function (rs) {
            if (rs.code == 0) {
                console.log('rs=', rs);
                view.numberAnimate(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
})


// function getShareInfo(res) {
//     model.getShareInfo({
//         param: {
//             item_id: "999999999",
//             item_type: 0,
//         },
//         success: function(rs) {
//             if (rs.code == 0) {
//                 forwarding_default = rs.data.forwarding_default;
//                 forwarding_desc = rs.data.forwarding_desc;
//                 shareUrl = rs.data.url

//                 console.log('getShareInfo', forwarding_default, forwarding_desc, shareUrl)
//             } else {
//                 utils.alertMessage(rs.msg)
//             }
//         },
//         error: utils.handleFail

//     })
// }

function getActivityPop(res) {
    model.getActivityPop({
        // param: {
        //     product_id: product_id,
        // },
        success: function (rs) {
            if (rs.code == 0) {

                res.pop = rs.data
                view.init(res);
                getPublicityLast(res);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail

    })
}

setInterval(() => {
    model.getJoinCount({
        success: function (rs) {
            if (rs.code == 0) {
                console.log('rs=', rs);

                view.resetData(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}, 120000);

//获取公示总览
function getPublicityLast(res) {
    model.getPublicityLast({
        success: function (rs) {
            if (rs.code == 0) {
                getPublicMembers(rs)
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}
//获取公示列表
function getPublicMembers(res) {
    model.getPublicMembers({
        success: function (rs) {
            if (rs.code == 0) {
                rs.getPublicityLast = res.data
                _publication.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}

/*
referral bonus
*/
// $UI.on('referralBonus', function(e, target_type) {
//     console.log('===referralBonus===')
//     model.referralBonus({
//         param: {
//             from: JSON.stringify(target_type),
//             third_id: product_id,
//         },
//         success: function(res) {
//             if (res.code == 0) {
//                 res.data.from = target_type;
//                 // view.refferalBonusUrl(res);
//             } else {
//                 utils.alertMessage(rs.msg);
//             }
//         },
//         error: utils.handleFail
//     });
// });

// 如果是安卓端： 吊起安卓分享弹窗
// $UI.on('android-share', function(e) {

//     let android_share_url =
//         "native://share/webpage?url=" + encodeURIComponent(shareUrl) +
//         "&forwardingDesc=" + encodeURIComponent(forwarding_default + ' %s') +
//         "&actTitle=" + encodeURIComponent(actTitle) +
//         "&actDesc=" + encodeURIComponent(actDesc) +
//         "&forwardingMap=" + encodeURIComponent(JSON.stringify(forwarding_desc));
//     console.log('android_share_url=', android_share_url);

//     location.href = android_share_url;


// })


/*
监听分享成功回调
*/
$UI.on('gtryShareSuccess', function (e, target_type) {
    console.log('===gtryShareSuccess===')

    // share_type 1: project 2: group 3: GTRY
    // model.projectShare({
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: "999999999",
            item_type: 0,
            scene: 1,
        },
        success: function (res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
});

window.appHandle = function (req_code) {
    if (req_code == 600) {
        location.href = '/join.html?product_id=' + product_id + '&share=' + share;
    }
}
window.changeShareNum = function (target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: "999999999",
            item_type: 0,
            scene: 1,
        },
        success: function (res) {
            if (res.code == 0) {
                console.log('/v1/project_share');
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}