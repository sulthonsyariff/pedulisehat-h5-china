import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName'; //port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};



/**
 * 商品列表
 */
obj.getProducts = function(o) {
    let url = domainName.heoproduct + '/v1/products';

    if (isLocal) {
        url = '../mock/GTRY/v1_products.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};
/**
 * 活动状态
 */
obj.getActivityState = function(o) {
    let url = domainName.heouic + '/v1/hrr_state';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/*
参加人数模拟数据
*/
obj.getJoinCount = function(o) {
    let url = domainName.heoproduct + '/v1/join_count';

    if (isLocal) {
        url = '../mock/GTRY/join_count.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/*
    TargetTypefacebook = 1 // facebook
    TargetTypeWhatsApp = 2 // whatsapp
    TargetTypeTwitter = 3 // twitter
    TargetTypeLink = 4 // link
    TargetTypeLine = 5 // line
    TargetTypeInstagram = 6 // instagram
    TargetTypeYoutube = 7 // youtube

    share_type 1: project 2: group 3: GTRY
*/
// obj.projectShare = function(o) {
//     let url = domainName.project + '/v1/project_share';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }


// obj.referralBonus = function(o) {
//     let url = domainName.project + '/v1/share_user_relation';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }
//新分享统计接口
obj.newProjectShare = function (o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//获取分享相关内容
obj.getShareInfo = function (o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}
//分享数据上报
obj.shareAccessCount = function (o) {
    let url = domainName.share + '/v1/share_access_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//get activity pop
obj.getActivityPop = function(o) {
    let url = domainName.activity + '/v1/heo/promotion?activity_type=heo_promotion';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};



/**
 * 公示总览
 */
obj.getPublicityLast = function(o) {
    let url = domainName.heouic + '	/v1/publicity/last';

    // if (isLocal) {
    //     url = '../mock/GTRY/v1_products.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};
/**
 * 公示选项卡列表
 */
obj.getPublicMembers = function(o) {
    let url = domainName.heouic + '	/v1/public_members';

    // if (isLocal) {
    //     url = '../mock/GTRY/v1_products.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


export default obj;