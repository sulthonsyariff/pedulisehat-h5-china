// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'

import alertBoxTplCriticalDisease from '../tpl/_alertBoxTplCriticalDisease.juicer' //关闭项目弹窗
import alertBoxTplMembership from '../tpl/_alertBoxTplMembership.juicer' //关闭项目弹窗
import alertBoxTplHealthRequirement from '../tpl/_alertBoxTplHealthRequirement.juicer' //关闭项目弹窗

import 'numberAnimate' // 数字滚动
import utils from 'utils'
import domainName from 'domainName'; //port domain name
// import sensorsActive from 'sensorsActive'
import shareGtry from 'shareGtry'

/* translation */
import gtryHome from 'gtryHome'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
import common from 'common'
let commonLang = qscLang.init(common);
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryHome);
/* translation */

let [obj, $UI] = [{}, $('body')];

let memberNumRun;
let fundsAmountRun;
let reqObj = utils.getRequestParams();
let share = reqObj['share'] ? reqObj['share'] : '';
// console.log('-----share----', share)

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

obj.UI = $UI;

// 初始化
obj.init = function(res) {
    res.JumpName = 'Gotongroyong';
    res.commonNavGoToWhere = "https://" + utils.judgeDomain() + '.pedulisehat.id'
    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }

    res.getLanguage = utils.getLanguage();
    res.lang = lang;
    $UI.append(mainTpl(res)); //主模版
    console.log('===加载主模版===');

    commonFooter.init(res);
    fastclick.attach(document.body);

    //分享添加meta头部信息
    utils.changeFbHead({
        url: "https:" + domainName.share + '/gtry/' + res.data[0].product_id,
    });

    $UI.trigger('getJoinCount'); //获取滚动数字,确保主模版加载完后再加载数字部分

    clickHandle(res);
    // sensorsActive.init();

    // sensors.track('GTRYHomeView', {
    //     from: document.referrer
    // })

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    $('#fc_frame').css('bottom', '120px');

    // sensors.track('GTRYHomeView', {
    //     from: document.referrer,
    // })
};


function clickHandle(res) {
    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        $('.bubble-icon').css('display', 'none');

        let paramObj = {
            item_id: "999999999",
            item_type: 0,
            // item_short_link: '',
            // remark: data.title, //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'gtry-share-wrapper', // 分享弹窗名
            shareCallBackName: 'gtryShareSuccess', //大病详情分享
            fromWhichPage: 'gtry.html' || '' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            let actTitle;
            let actDesc;

            if (res.state == 1) {
                actTitle = commonLang.lang36
                actDesc = lang.lang54
            } else {
                actTitle = commonLang.lang26
                actDesc = ''
            }

            let paramString = "&actTitle=" + encodeURIComponent(actTitle) +
                "&actDesc=" + encodeURIComponent(actDesc)

            $UI.trigger('android-share', [paramString, paramObj]);

        } else {
            // 分享组件
            shareGtry.init(paramObj);

            $('.bubble-icon').css({
                'display': 'none'
            })
        }
    });

    // join
    $('body').on('click', '#join', function() {
        if (res && res.data && res.data[0].product_id) {
            if (user_id) {
                location.href = '/join.html?product_id=' + res.data[0].product_id + '&share=' + share;
            } else {
                if (utils.browserVersion.android) {
                    location.href = 'qsc://app.pedulisehat/go/login?req_code=600'
                } else {
                    location.href = '/join.html?product_id=' + res.data[0].product_id + '&share=' + share;
                }
            }

        }
    })

    // FAQ
    $('body').on('click', '.FAQ .list', function() {
        $(this).toggleClass('show');
    })

    // Aid for Critical Diseases
    // $('body').on('click', '#critical-diseases', function() {
    //     if (!$('.critical-diseases').length) {
    //         $UI.append(alertBoxTplCriticalDisease(res));
    //     }
    //     $('.critical-diseases').css('display', 'block');
    // })

    // Terms of extension of membership
    $('body').on('click', '#membership', function() {
        if (!$('.membership').length) {
            $UI.append(alertBoxTplMembership(res));
        }
        $('.membership').css('display', 'block');
    })

    // health-requirement
    $('body').on('click', '#health-requirement', function() {
        if (!$('.health-requirement').length) {
            $UI.append(alertBoxTplHealthRequirement(res));
        }
        $('.health-requirement').css('display', 'block');
    })

    $('body').on('click', '.commonBoxStyle #gotIt', function() {
        $('.commonBoxStyle').css('display', 'none');
    })

    // whatsapp contact
    $('body').on('click', '.whatsappContact', function(e) {
        let appSrc = 'https://api.whatsapp.com/send?phone=6281230009479&text=Halo%20,%20Saya%20mau%20konsultasi%20Gotongroyong%20di%20pedulisehat.id';
        // in our android app
        if (utils.browserVersion.androids) {
            // native://intent?url=<gojet url>&errorInfo=<在此加上协议解析失败的提示 比如提示用户未安装app之类的>
            location.href = 'native://intent?url=' + encodeURIComponent(appSrc) + '&errorInfo=Error';
        } else {
            location.href = appSrc;

        }
    });
}

/**
 * 滚动数字初始化
 */
obj.numberAnimate = function(rs) {

    if (rs.data && rs.data.count && rs.data.total_money) {
        /*
         * 滚动数字： member num
         */
        memberNumRun = $('#member-num').numberAnimate({
            num: rs.data.count,
            speed: 1000,
            symbol: "."
        });

        /*
         * 滚动数字： funds amount
         */
        fundsAmountRun = $('#funds-amount').numberAnimate({
            num: rs.data.total_money,
            speed: 1000,
            symbol: "."
        });
    }
}

obj.resetData = function(rs) {
    memberNumRun.resetData(rs.data.count);
    fundsAmountRun.resetData(rs.data.total_money);
}


export default obj;