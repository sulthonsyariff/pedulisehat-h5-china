const baseConfig = require('../../../../../base.config.qa.js');
const merge = require('webpack-merge');
const commonConfig = require("./webpack.config.common");

module.exports = merge(baseConfig(commonConfig), {
    mode: "development",
});