const baseConfig = require('../../../../../base.config.js');
const merge = require('webpack-merge');
const commonConfig = require("./webpack.config.common");

module.exports = merge(baseConfig(commonConfig), {
    mode: "production",
});