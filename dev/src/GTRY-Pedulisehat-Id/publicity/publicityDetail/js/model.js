import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName'; //port domain name

let obj = {};
obj.getPublicMemberInfo = function(o) {
    var url = domainName.heouic + '/v1/public_member?apply_id=' + o.param.apply_id;
 
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;