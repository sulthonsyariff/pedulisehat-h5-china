import view from './view'
import model from './model'

import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'

/* translation */
import gtryDetail from 'gtryDetail'
import qscLang from 'qscLang'
let lang = qscLang.init(gtryDetail);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let $UI = view.UI;

let reqObj = utils.getRequestParams();
let apply_id = reqObj['apply'];





model.getPublicMemberInfo({
    param: {
        apply_id: apply_id
    },
    success: function (rs) {
        if (rs.code == 0) {
            view.init(rs);
            utils.hideLoading();
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
})