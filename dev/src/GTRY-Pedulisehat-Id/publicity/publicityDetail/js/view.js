// 公共库
import nav from 'nav'
// import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat'

/* translation */
import gtryPublicity from 'gtryPublicity'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryPublicity);
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let [obj, $UI] = [{}, $('body')];


let reqObj = utils.getRequestParams();
let product_id = reqObj['product_id'];
let product_type = reqObj['product_id'];
let apply_id = reqObj['apply'];


obj.UI = $UI;

// 初始化
obj.init = function (res) {
    console.log('publicationTitle', res);
    res.JumpName = lang.lang14;
    res.lang = lang
    nav.init(res);

    res.data._name = res.data.real_name.slice(0, 5)
    res.data._join_at = timeHandle(res.data.join_at) 
    res.data._aid_amount = changeMoneyFormat.moneyFormat(res.data.aid_amount);

    if (res.data.gender == 1) {
        res.data._gender = lang.lang12
    } else {
        res.data._gender = lang.lang13
    }

    $UI.append(mainTpl(res)); //主模版

};

// obj.refferalBonusUrl = function (res) {
//     //分享添加meta头部信息
//     utils.changeFbHead({
//         url: res.data.url,
//     });
// }

function clickHandle(res) {
    // share:如果是安卓端：吊起安卓分享弹窗


    // FAQ
    $('body').on('click', '.FAQ .list', function () {
        $(this).toggleClass('show');
    })

    // covid terms



}


obj.resetData = function (rs) {

}


function timeHandle(created_at) {
    let myDate = new Date(created_at);

  let year = myDate.getFullYear();
  let month = myDate.getMonth() + 1;

  let day = myDate.getDate();

  if (month < 2) {
    month = "Jan";
  } else if (month < 3) {
    month = "Feb";
  } else if (month < 4) {
    month = "Mar";
  } else if (month < 5) {
    month = "Apr";
  } else if (month < 6) {
    month = "May";
  } else if (month < 7) {
    month = "Jun";
  } else if (month < 8) {
    month = "Jul";
  } else if (month < 9) {
    month = "Aug";
  } else if (month < 10) {
    month = "Sep";
  } else if (month < 11) {
    month = "Oct";
  } else if (month < 12) {
    month = "Nov";
  } else if (month < 13) {
    month = "Dec";
  }

  if (day < 10) {
    day = "0" + day;
  }
  return day + " " + month + " " + year;

}

// return (
//   year + "." + month + "." + date + "  " + hour + ":" + minutes + ":" + second
// );


export default obj;