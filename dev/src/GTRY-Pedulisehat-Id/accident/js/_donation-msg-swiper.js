import donationMsgSwiperTpl from '../tpl/_donation-msg-swiper.juicer'
import changeMoneyFormat from 'changeMoneyFormat'
import domainName from 'domainName'; //port domain name

let obj = {};

obj.init = function (rs) {
    console.log('donation-msg-swiper', rs)
    let arr = [];
    rs.arr = arr;


    for (let i = 0; i < rs.data.length; i++) {
        rs.arr.push(rs.data[i])

        if (rs.arr[i].user_name.length > 5) {
            rs.arr[i].user_name = rs.arr[i].user_name.substring(0, 5) + "...";
        }


        if (rs.arr[i].avatar.indexOf('http') == 0) {
            rs.arr[i].avatar = rs.arr[i].avatar
        } else {
            rs.arr[i].avatar = domainName.static + '/img/avatar/' + rs.arr[i].avatar;
        }

        rs.arr[i].money = changeMoneyFormat.moneyFormat(rs.arr[i].money)
        rs.domainName = domainName;

    }


    console.log('====arr====', rs.lang)
    
    
    if(rs.data.length >= 3) {
        $('.donation-msg-swiper-wrap').append(donationMsgSwiperTpl(rs));
        scrollDonationList();
    }
    // setInterval(noticeUp('.scroll ul','-35px',500), 2000);
    // setInterval(
    //     function () {
    //         noticeUp('.scroll ul', '-35px', 500)
    //     }, 3000);

}


// $(function () {
//     // 调用 公告滚动函数
//     setInterval("noticeUp('.notice ul','-35px',500)", 2000);
// });


// function noticeUp(obj, top, time) {
//     console.log('noticeUp', obj, top, time)
//     $(obj).animate({
//         marginTop: top
//     }, time, function () {
//         $(this).css({
//             marginTop: "0"
//         }).find(":first").appendTo(this);
//     })
// }


function scrollDonationList() {
    var scrollDiv = $(".scroll")
    var $ul = scrollDiv.find("ul")
    var $li = scrollDiv.find("li")
    // console.log('$li', $li)
    var $length = $li.length
    var $liHeight = $li.height() + 16

    num = 0;
    if (scrollDiv.length == 0) {
        return;
    }

    if ($length > 1) {
        $ul.append($li.eq(0).clone());
        setInterval(function () {
            num++;
            // console.log('num---', num)
            $ul.addClass("animate").css("-webkit-transform", "translateY(-" + $liHeight * (num) + "px)");
            // console.log('scroll-height', $liHeight * (num))
            setTimeout(
                function () {
                    if (num == $length) {
                        $ul.removeClass("animate").css("-webkit-transform", "translateY(0)");
                        num = 0;
                    }
                }, 300);
        }, 3000);
    }
}
export default obj;