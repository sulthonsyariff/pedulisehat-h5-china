import mainTpl from '../tpl/_publication.juicer'
import changeMoneyFormat from 'changeMoneyFormat'
import alertPublicityTpl from '../tpl/_alertPublicity.juicer' //关闭项目弹窗

import utils from 'utils'
import domainName from 'domainName'; //port domain name

/* translation */
import gtryHome from 'gtryHome'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
import common from 'common'
let commonLang = qscLang.init(common);
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryHome);
/* translation */

let [obj, $UI] = [{}, $('body')];


let reqObj = utils.getRequestParams();
let share = reqObj['share'] ? reqObj['share'] : '';
let apply;

obj.UI = $UI;

// 初始化
obj.init = function (res) {
    res.lang = lang



    if (res.data.pm.accident) {
        res.accidentArr = res.data.pm.accident.slice(0, 2)
        for (let i = 0; i < res.accidentArr.length; i++) {
            res.accidentArr[i]._aid_amount = changeMoneyFormat.moneyFormat(res.accidentArr[i].aid_amount)
            res.accidentArr[i]._real_name = res.accidentArr[i].real_name.slice(0, 5)
        }
    }

    $('.publicity-insert').append(mainTpl(res)); //主模版
    clickHandle(res)

};

function clickHandle(res) {

    // 打开app项目详情
    // 非安卓APP + 安卓手机 : 显示弹窗

    $('body').on('click', '.cardWrap', function () {
        if (utils.browserVersion.androidEnd && !utils.browserVersion.androids) {
            if (!$('.publicity').length) {
                $UI.append(alertPublicityTpl(res));
            }
            $('.publicity').css('display', 'block');
            apply = $(this).attr('data-apply')
        } else {
            location.href = '/publicityDetail.html?apply=' + $(this).attr('data-apply')
        }
    })

    // 非安卓APP + 安卓手机 
    $('body').on('click', '.commonBoxStyle .gotIt', function () {
        if (utils.browserVersion.androidEnd && !utils.browserVersion.androids) {
            location.href = 'https://play.google.com/store/apps/details?id=com.qschou.pedulisehat.android';
        } else if (utils.browserVersion.android) {
            location.href = 'qsc://app.pedulisehat/do/jump?url=' + encodeURIComponent("https://gtry") + encodeURIComponent(domainName) + encodeURIComponent("/publicityDetail.html?apply=") + encodeURIComponent(apply)
        } else {

            location.href = '/publicityDetail.html?apply=' + apply
        }
    })


    $('body').on('click', '.mask', function () {
        $('.commonBoxStyle').css('display', 'none');
    })
    $('body').on('click', '.cancel', function () {
        $('.commonBoxStyle').css('display', 'none');
    })


}


export default obj;