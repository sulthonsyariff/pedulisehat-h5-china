// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import dieaseCardTpl from '../tpl/_diease_card.juicer'
import accidentCardTpl from '../tpl/_accident_card.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics.gtry'
import changeMoneyFormat from 'changeMoneyFormat'
import qscScroll_timestamp from 'qscScroll_timestamp'
import timeHandle from './_timeHandle'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import gtryMembershipCard from 'gtryMembershipCard'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryMembershipCard);
/* translation */

let page = 0;
let share = '';
let scroll_list = new qscScroll_timestamp();
let [obj, $UI] = [{}, $('body')];
let diseaseFirst = true;
let accidentFirst = true;
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    if (utils.browserVersion.androids) {
        location.href = 'native://do.something/setTitle?title=' + lang.lang13
    }

    res.JumpName = lang.lang13;
    // res.commonNavGoToWhere = 'https://' + utils.judgeDomain() + '.pedulisehat.id';
    res.commonNavGoToWhere = 'https://' + utils.judgeDomain() + '.pedulisehat.id/myProfile.html';

    if (!utils.browserVersion.androids) {
        commonNav.init(res);
    }
    res.lang = lang;

    $UI.append(mainTpl(res)); //主模版

    // 隐藏loading
    utils.hideLoading();

    initScorll();

    commonFooter.init(res);
    fastclick.attach(document.body);

    clickHandle(res);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

obj.insertData = function(res) {
    console.log('view : insertData', res);
    res.lang = lang;
    console.log('view : insertData===', res.data.illness);
    res.diseaseFirst = diseaseFirst
    res.accidentFirst = accidentFirst
        //大病
    if (res && res.data.illness && res.data.illness.length != 0) {
        for (let i = 0; i < res.data.illness.length; i++) {
            // 格式化金额

            res.data.illness[i].balance = changeMoneyFormat.moneyFormat(res.data.illness[i].balance);
            res.data.illness[i].joinFrom = 'disease';
            // res.data.illness[i].state = 40;
        }

        $('.insertDieaseData').append(dieaseCardTpl(res));
        diseaseFirst = false;
        scroll_list.run();

    } else {
        scroll_list.stop();
        $('.loading').hide();
    }


    //意外
    if (res && res.data.accident && res.data.accident.length != 0) {
        for (let i = 0; i < res.data.accident.length; i++) {
            // 格式化金额
            res.data.accident[i].balance = changeMoneyFormat.moneyFormat(res.data.accident[i].balance);
            res.data.accident[i].joinFrom = 'accident';
        }

        $('.insertAccidentwData').append(accidentCardTpl(res));
        accidentFirst = false;
        // scroll_list.run();

    } else {
        scroll_list.stop();
        $('.loading').hide();
    }


}

function initScorll() {
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });
    scroll_list.run();
};

obj.setCardDetail = function(res, $this) {
    if (res.data.id_card) {
        $($this).find('.id_card').html(res.data.id_card.replace(/[^0-9]+/, '').replace(/^0+/, '').replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1-"));
    } else {
        $($this).find('.id_card').html(lang.lang21);
    }
    $($this).find('.created_at').html(timeHandle.userDate(res.data.created_at));
    $($this).find('.effective_at').html(timeHandle.userDate(res.data.effective_at));
    $($this).find('.help_num').html(res.data.help_people);
}

function clickHandle(res) {
    // card animate
    $('body').on('click', '.flip', function() {
        if (!$(this).hasClass('rotate')) {
            console.log('front :getCardDetail')
            $UI.trigger('getCardDetail', $(this));
        }

        $(this).toggleClass('rotate');
    });

    // top up
    $('body').on('click', '.topUp', function(e) {
        // console.log($(this).data('member_card_id'));
        location.href = '/topUp.html?member_card_id=' + $(this).data('member_card_id') + '&product_id=' + $(this).data('product_id');
        return false;
    });

    //apply
    $('body').on('click', '.apply', function(e) {
        location.href = '/applyForAidGuidence.html?member_card_id=' + $(this).data('member_card_id') + '&product_type=' + $(this).data('product_type') + '&product_id=' + $(this).data('product_id');

        return false;
    });
    //active
    $('body').on('click', '.active', function(e) {
        location.href = '/activeMembership.html?real_name=' + $(this).data('real_name') + '&relationship=' + $(this).data('relationship') + '&member_card_id=' + $(this).data('member_card_id') + '&product_type=' + $(this).data('product_type') + '&product_id=' + $(this).data('product_id');
        return false;
    });

    // check
    $('body').on('click', '#check', function(e) {
        location.href = '/transactions.html?member_card_id=' + $(this).data('member_card_id');
        return false;
    });
    console.log('----116----', res)
        // referral bonus
    $('body').on('click', '.referral-bonus-wrap', function(e) {
        location.href = '/referralBonus.html'
    });

    // referral bonus
    $('body').on('click', '.join-btn-wrap', function(e) {
        // location.href = '/join.html?product_id=' + res.data[0].product_id + '&share=' + share;
        location.href = '/';
    });

    // gtry detail
    $('body').on('click', '.program-wrap', function(e) {
        location.href = '/';
    });

    // 重新加入大病互助
    $('body').on('click', '.rejoin-D', function(e) {
        location.href = '/join.html?product_id=' + $(this).data('product_id') + '&joinFrom=detail';
    });
    // 重新加入意外互助
    $('body').on('click', '.rejoin-A', function(e) {
        location.href = '/join.html?product_id=' + $(this).data('product_id') + '&joinFrom=accident';
    });

}

export default obj;