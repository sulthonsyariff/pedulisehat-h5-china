import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let $UI = view.UI;
let first = true;

if (utils.judgeDomain() == 'qa') {
    location.href = 'https://gtry-qa.pedulisehat.id/gtry/closed/refund'
} else if (utils.judgeDomain() == 'pre') {
    location.href = 'https://gtry-pre.pedulisehat.id/gtry/closed/refund'
} else {
    location.href = 'https://gtry.pedulisehat.id/gtry/closed/refund'
}

model.getActivityState({
    success: function(rs) {
        if (rs.code == 0) {
            getProducts(rs)
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail,
});

// 获取商品列表
function getProducts(res){
    model.getProducts({
        success: function(rs) {
            if (rs.code == 0) {
                console.log('rs=', rs);
                rs.state = res.data.state
                // 隐藏loading
                view.init(rs);
                utils.hideLoading();
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
}


// 获取会员列表
$UI.on('needload', function(e, page) {
    model.getMemberCards({
        param: {
            page: page
        },
        success: function(rs) {
            if (rs.code == 0) {
                // console.log('rs=', rs, 'first =', first);

                if (first && !rs.data.accident && !rs.data.illness ) {
                    // 跳转互助首页
                    location.replace('/');
                }
                first = false;
                view.insertData(rs);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
});

// 获取会员详情
$UI.on('getCardDetail', function(e, $this) {
    model.getMemberCardDetail({
        param: $($this).data('member_card_id'),
        success: function(rs) {
            if (rs.code == 0) {
                view.setCardDetail(rs, $this);

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail,
    });
});