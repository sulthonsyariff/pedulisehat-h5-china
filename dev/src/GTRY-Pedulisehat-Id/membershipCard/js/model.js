import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 商品列表
 */
obj.getProducts = function(o) {
    let url = domainName.heoproduct + '/v1/products';

    if (isLocal) {
        url = '../mock/GTRY/v1_products.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/**
 * 会员列表
 */
obj.getMemberCards = function(o) {
    let url = domainName.heouic + '/v1/member_cards?page=' + o.param.page + '&limit=10';

    if (isLocal) {
        url = '../mock/GTRY/v1_member_cards_1.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/**
 * 会员详情
 */
obj.getMemberCardDetail = function(o) {
    let url = domainName.heouic + '/v1/member_card/' + o.param;

    if (isLocal) {
        url = '../mock/GTRY/v1_member_card_detail.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/**
 * 活动状态
 */
obj.getActivityState = function(o) {
    let url = domainName.heouic + '/v1/hrr_state';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


export default obj;