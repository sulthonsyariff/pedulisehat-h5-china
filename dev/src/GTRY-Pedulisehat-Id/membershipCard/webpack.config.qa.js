var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'GTRY-Pedulisehat-Id',
    htmlFileURL: 'html/GTRY-Pedulisehat-Id/membershipCard.html',

    htmlOgUrl: 'https://gtry.pedulisehat.id',
    htmlOgTitle: 'Program GTRY untuk Penyakit Kritis',
    htmlOgDescription: 'Bergabunglah dengan program GTRY, bantuan untuk penyakit kritis dengan dana kompensasi hingga Rp 100.000.000.https://gtry.pedulisehat.id/ ',
    htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',

    appDir: 'js/GTRY-Pedulisehat-Id_membershipCard',
    uglify: true,
    hash: '',
    mode: 'development'
})