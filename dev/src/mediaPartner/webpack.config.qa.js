var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    // htmlTitle: 'home',
    htmlFileURL: 'html/mediaPartner.html',
    appDir: 'js/mediaPartner',
    uglify: true,
    hash: '',
    mode: 'development'
})