var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    // htmlTitle: 'homePage',
    htmlFileURL: 'html/mediaPartner.html',
    appDir: 'js/mediaPartner',
    uglify: true,
    hash: '',
    mode: 'production'
})