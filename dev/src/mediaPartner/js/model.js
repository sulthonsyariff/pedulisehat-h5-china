import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// 企业信息
obj.getPartner = function(o) {
    let url = domainName.project + '/v1/partner?user_id=' + o.param.user_id;
    if (isLocal) {
        url = '/mock/v1_partner.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

export default obj;