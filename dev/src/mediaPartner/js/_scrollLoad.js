// import getListPageH from './_getListPageHeight'

let obj = {};
let onNeedLoad;
let intId;
let jumpTime = 0;
let loading = false;
// let pageH = getListPageH.get();

obj.config = function(o) {
    onNeedLoad = o.onNeedLoad;
}

obj.run = function() {
    intId = setInterval(function() {

        if (loading) {
            return;
        }

        let scrollTop = $(document).scrollTop();
        let dmtHeight = $(document).height();
        let windowHeight = $(window).height();
        let bottomDis = dmtHeight - scrollTop - windowHeight;

        let willMethod = "";
        if (scrollTop < 1500) {
            willMethod = "prepend";
        }
        if (bottomDis < 500) {
            willMethod = "append";
            firstLoad = false;
        }

        if (willMethod) {
            clearInterval(intId);
            loading = false;
            onNeedLoad(willMethod);
        }

        if (jumpTime == 0) {
            jumpTime = 500;
        }

    }, jumpTime)
}


obj.stop = function() {
    clearInterval(intId);
}

export default obj;