// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import listItemTpl from '../tpl/_listItem.juicer'
import store from 'store'
import handleScrollLoad from './_handleScrollLoad'
import ScrollLoad from './_scrollLoad' // new scroll load

import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics'
import getListPageH from './_getListPageHeight'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'
import campaignListComponents from 'campaignListComponents'

/* translation */
import campaignList from 'campaignList'
import mediaPartner from 'mediaPartner'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(campaignList);
let lang_mediaPartner = qscLang.init(mediaPartner);
/* translation */

let is_first = true;
let page = 0;

let insertMethod = 'append';
let pageH = getListPageH.get();

let [obj, $UI] = [{}, $('body')];
let reqObj = utils.getRequestParams();

obj.UI = $UI;

obj.init = function(rs) {
    rs.lang = lang;
    rs.lang_mediaPartner = lang_mediaPartner;
    rs.JumpName = titleLang.MediaPartner;

    // rs.commonNavGoToWhere = decodeURIComponent(reqObj.qf_redirect) + '?from=mediaPartner';
    // rs.commonNavGoToWhere = document.referrer;

    commonNav.init(rs);
    $('title').html(titleLang.MediaPartner);

    rs.data.current_amount = changeMoneyFormat.moneyFormat(rs.data.current_amount || '0');
    rs.data.corner_mark = rs.data.corner_mark ? JSON.parse(rs.data.corner_mark) : '';
    rs.data.avatar = utils.imageChoose(rs.data.avatar)

    $UI.append(mainTpl(rs)); //insert 主模版
    commonFooter.init({});

    handleScrollLoad.init(rs); //init scroll

    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

obj.insertData = function(rs, o, method) {
    insertMethod = method;
    rs.lang = lang;
    rs.domainName = domainName;

    // 通过 is_first 值来judge是否展示空白列表页
    if (rs.data && rs.data.length != 0) {
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        // insert page
        handleScrollLoad.insertData(rs, o, insertMethod);

        rs.fromWhichPage = 'mediaPartner.html'
        rs.$dom = $('.Campaigns_List.page_' + rs._metadata.page)
        campaignListComponents.init(rs);

    } else if (is_first) {
        $('.loading.append-loading').hide();
        $('ul.project-list').append(listItemTpl(rs)); //渲染空白列表
        ScrollLoad.stop();
    } else {
        $('.loading.append-loading').hide();
        $('.campaign_box').last().attr('last', 'last'); // 给最后一条做一个标记
        ScrollLoad.run(); // 页面加载最后一条后，继续监听滚动，但不要持续加载最后一页
    }

    if (insertMethod == 'prepend' && $('.campaign_box').eq(0).data('page') != 1) {
        $('.loading.preappend-loading').show();
    }
}

obj.raiseDay = function(created_at) {
    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}

export default obj;