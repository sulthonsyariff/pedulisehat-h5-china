// 公共库
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import model_getListData from 'model_getListData' //列表接口调用地址

// 引入依赖
import utils from 'utils'

let parmas = utils.getRequestParams();
let user_id = parmas.userId;

let UI = view.UI;

// 企业用户信息
model.getPartner({
    param: {
        user_id: user_id
    },
    success: function(rs) {
        if (rs.code == 0) {
            view.init(rs);
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
});

// 加载更多
UI.on('needload', function(e, o, method) {
    model_getListData.getListData_mediaPartner({
        param: {
            user_id: user_id,
            page: o.page
        },
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                utils.hideLoading();

                view.insertData(res, o, method);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});