 //  获取十条一页的高度
 let obj = {};

 obj.get = function() {
     let deviceH = $(window).height();
     let deviceW = $(window).width();
     let pageH;

     if (deviceW > 767) {
         pageH = 3080;
     } else if (deviceW > 413) {
         pageH = 2330;
     } else if (deviceW > 374) {
         pageH = 2180;
     } else if (deviceW > 319) {
         pageH = 1990;
     }

     return pageH;
 }


 export default obj;