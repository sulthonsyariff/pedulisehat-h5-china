let baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'zakatCalculator',
    htmlFileURL: 'html/zakatCalculator.html',
    appDir: 'js/zakatCalculator',
    uglify: true,
    hash: '',
    mode: 'production'
})