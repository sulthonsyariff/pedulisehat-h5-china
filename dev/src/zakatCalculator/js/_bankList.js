import mainTpl from '../tpl/_bankList.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name



/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let reqSelect = location.href
/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function (res) {

    res.domainName = domainName;
    res.commonLang = commonLang;
    res.isAndroid = utils.browserVersion.android;

    $UI.prepend(mainTpl(res));
    console.log('res=', res);

    fastclick.attach(document.body);
    obj.event(res);



};

obj.event = function (res) {
    // useless code ？？？
    // for (let i = 0; i < $('.list-items').length; i++) {
    //     $(".list-items").each(function() {
    //         // if($(this).attr('data-value') == '302'){
    //         //     // console.log('this',$(this))
    //         //     $(this).css('display', 'none')
    //         // }
    //         if ($(this).text() == res.data.bank_agency) {
    //             // console.log('$(this)')
    //             $(this).addClass('choosed-bank')
    //         }
    //     });
    // }  

    // 监听input输入
    // $UI.on('input propertychange', '#target_amount', function () {

    //     // if ($('.amount-card.chosed').length && ($('#target_amount').val() != $('.amount-card.chosed').val())) {
    //     //     $('.amount-card.chosed').removeClass('chosed');
    //     // }
    //     // console.log('target_amount', $('#target_amount').val().replace(/\./g, ''))

    //     if (category_id != 13 && $('#target_amount').val().replace(/\./g, '') > 9999) {
    //         console.log('target_amount>10000', $('#target_amount').val().replace(/\./g, ''))
    //         $('div[class$="list-items"]').css({
    //             'pointer-events': 'auto',
    //             'color': '#333',
    //             'opacity':'1.0'

    //         })

    //     } else if (category_id == 13 && $('#target_amount').val().replace(/\./g, '') > 109999) {
    //         console.log('target_amount>109999', $('#target_amount').val().replace(/\./g, ''))
    //         $('div[class$="list-items"]').css({
    //             'pointer-events': 'auto',
    //             'color': '#333',
    //             'opacity':'1.0'

    //         })
    //     } else {
    //         $('div[class$="list-items"][list-bankCode!="4"][list-bankCode!="1"][list-bankCode!="302"][list-bankCode!="5"]').css({
    //             'pointer-events': 'none',
    //             'color': '#ccc',
    //             'opacity':'0.2'
    //         })
          
    //         // $('div[list-bankCode="4"]').css('background','red')
    //         // $('div[list-bankCode="1"]').css('background','red')

    //     }

    // })


    $('.alert-bank .close').on('click', function (rs) {
        $('.alert-bank').css('display', 'none')
    })

    $('body').on('click', '.list-items', function (e) {

        $('.list-items').removeClass('choosed-bank');
        $(this).addClass('choosed-bank');

        setTimeout(() => {
            $('.alert-bank').css('display', 'none')
        }, 100)

        $('.channel').addClass('selected').html($(this).html());

    })
}

export default obj;