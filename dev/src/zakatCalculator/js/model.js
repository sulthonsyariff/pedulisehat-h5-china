import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

// let reqObj = utils.getRequestParams();
// let showSP = reqObj['showSP'] || false; //是否展示shopeepay

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;
// console.log(ajaxProxy);

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

/****
 * 项目手续费情况
 */
obj.getProjectPoundage = function(o) {
    let url = domainName.project + '/v1/project_poundage?project_id=' + o.param.project_id + '&category_id=' + o.param.category_id;
    if (isLocal) {
        url = 'mock/v1_project_poundage.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o)
}


/**
 * 获取zakat列表信息
 */
obj.getZakatList = function(o) {
    let url = domainName.project + '/v1/public?category_id=13';
    if (isLocal) {
        url = 'mock/detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o)
}

obj.getNishabPrice = function(o) {
    let url = domainName.base + '/v1/nishab?pid=1';
    if (isLocal) {
        url = 'mock/detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o)
}

//get payment
obj.getPayment = function(o) {
    let url = domainName.trade + '/v2/pay_channel' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '')
        // +
        // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    if (isLocal) {
        url = 'mock/v2_pay_channel_donate.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

//create payment 1:group logined pay 2:logined pay 3:group unlogin pay 4:unlogin pay
obj.createPayment = function(o) {
    let url;
    // group logined pay
    if (o._whichPay == 1) {
        url = domainName.trade + '/v1/support_group';
    }
    // logined pay
    else if (o._whichPay == 2) {
        url = domainName.trade + '/v1/support';
    }
    // group unlogin pay
    else if (o._whichPay == 3) {
        url = domainName.trade + '/v1/tourists_group_support';
    }
    // unlogin pay
    else if (o._whichPay == 4) {
        url = domainName.trade + '/v1/tourists_support';
    }

    // if (isLocal) {
    //     url = 'mock/initiateInfo.json'
    //      url = 'mock/v1_anonymity_support.json'
    // }

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

// //create payment
// obj.createPayment = function(o) {
//     let url = domainName.trade + '/v1/support';
//     if (isLocal) {
//         url = 'mock/initiateInfo.json'
//     }
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// };

//visit create payment
// obj.visitCreatePayment = function(o) {
//     // let url = domainName.trade + '/v1/anonymity_support';
//     let url = domainName.trade + '/v1/tourists_support';

//     if (isLocal) {
//         url = 'mock/v1_anonymity_support.json'
//     }
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// };

export default obj;