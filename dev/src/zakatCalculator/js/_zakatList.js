import mainTpl from '../tpl/_zakatList.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name



/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function (res) {

    res.domainName = domainName;
    res.commonLang = commonLang;
    res.isAndroid = utils.browserVersion.android;

    $UI.prepend(mainTpl(res));
    console.log('res=zakatList', res);

    fastclick.attach(document.body);
    obj.event(res);



};

obj.event = function (res) {
   

    $('.alert-list .close').on('click', function (rs) {
        $('.alert-list').css('display', 'none')
    })

    $('body').on('click', '.zakat-list-items', function (e) {

        $('.zakat-list-items').removeClass('choosed-zakat');
        $(this).addClass('choosed-zakat');

        setTimeout(() => {
            $('.alert-list').css('display', 'none')
        }, 100)

        $('._channel').addClass('selected').html($(this).html());

        console.log($(this).attr('list-project_id'))
        $UI.trigger('getProjectPoundage',$(this).attr('list-project_id') );
        $('.payment').css('display', 'block')

    })
}

export default obj;