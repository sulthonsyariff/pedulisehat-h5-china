import mainTpl from '../tpl/main.juicer'
// import _zakatIntentionTpl from '../tpl/_zakatIntention.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import calculator from './_calculator'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
import store from 'store'
import bankList from './_bankList'
import zakatList from './_zakatList'

// import _validate from './_validate';
// import campaignCard from '../tpl/_campaignCard.juicer'
// import sensorsActive from 'sensorsActive'
// import getSubmitParam from "./_getSubmitParam";
// import moneyInputChange from './_moneyInputChange'
import ClipboardJS from 'clipboard'
new ClipboardJS('.result-copy');

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];
let fromLogin = reqObj['fromLogin'];
// let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode')) || 0
// let appVersionName = utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName')


// let arr = [];
let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* translation */
import zakatCalculator from 'zakatCalculator'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(zakatCalculator);
/* translation */

let getLanguage = utils.getLanguage();
let tab_flag = 0
let submitData;
let category_id;
let anonymous = 'non-anonymous';
let is_comment;

let salary = 0
let bonuses = 0
let debt = 0

let gold_asset = 0
let cash_asset = 0
let vehicles_asset = 0
let debt_asset = 0

let result_money

let nisab_zakat = 0
let price_zakat = 0
let qty_zakat = 0

obj.init = function(rs) {
    rs.JumpName = lang.lang53;
    rs.lang = lang
    commonNav.init(rs);
    console.log('view==', rs)
    for (let i = 0; i < rs.data.length; i++) {
        // handle image
        if (rs.data[i].image) {
            rs.data[i].image = JSON.parse(rs.data[i].image);
        }
    }
    for (let i = 0; i < rs.zakatList.length; i++) {
        // handle image
        if (rs.zakatList[i].cover) {
            rs.zakatList[i].cover = JSON.parse(rs.zakatList[i].cover);
        }
    }

    $UI.append(mainTpl(rs));

    changeMoneyFormat.init('#salary');
    changeMoneyFormat.init('#bonuses');
    changeMoneyFormat.init('#debt');
    changeMoneyFormat.init('#gold');
    changeMoneyFormat.init('#cash');
    changeMoneyFormat.init('#vehicles');
    changeMoneyFormat.init('#asset-debt');

    // salary = $('#salary').val()
    // bonuses = $('#bonuses').val()
    // debt = $('#debt').val()

    UIhandler(rs);
    calculator.init(rs);
    bankList.init(rs);
    zakatList.init(rs);
    commonFooter.init(rs);

    // // sensorsActive.init();

    // 隐藏loading
    utils.hideLoading();

    obj.event(rs);


    fastclick.attach(document.body);
    // changeMoneyFormat.init('#target_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};


function UIhandler() {
    $UI.on('input propertychange', '#salary', function() {
        salary = $('#salary').val()
        if (salary) {
            $('.pro-btn').css({
                "pointer-events": "auto",
                "background": "#43ac43"
            })
        } else {
            $('.pro-btn').css({
                "pointer-events": "none",
                "background": "#ccc"
            })
        }
    })

    $UI.on('input propertychange', '#gold', function() {
        gold_asset = $('#gold').val()
        assetWatch();
    })
    $UI.on('input propertychange', '#cash', function() {
        cash_asset = $('#cash').val()
        assetWatch();


    })
    $UI.on('input propertychange', '#vehicles', function() {
        vehicles_asset = $('#vehicles').val()
        assetWatch();


    })
    $UI.on('input propertychange', '#asset-debt', function() {
        debt_asset = $('#asset-debt').val()
        assetWatch();

    })

    function assetWatch() {
        if (gold_asset || cash_asset || vehicles_asset || debt_asset) {
            // console.log('=====', tab_flag, gold_asset, cash_asset, vehicles_asset, debt_asset)
            $('.asset-btn').css({
                "pointer-events": "auto",
                "background": "#43ac43"
            })
        } else {
            // console.log('=====----', tab_flag, gold_asset, cash_asset, vehicles_asset, debt_asset)
            $('.asset-btn').css({
                "pointer-events": "none",
                "background": "#ccc"
            })
        }
    }


    $(".tab-menu li").click(function() {
        //通过 .index()方法获取元素下标，从0开始，赋值给某个变量
        var _index = $(this).index();
        tab_flag = _index

        //不同标签页展示不同的按钮
        if (tab_flag == 0) {
            $('.pro-btn').css('display', 'block')
            $('.asset-btn').css('display', 'none')
        } else if (tab_flag == 1) {
            $('.pro-btn').css('display', 'none')
            $('.asset-btn').css('display', 'block')
        }

        //让内容框的第 _index 个显示出来，其他的被隐藏
        $(".tab-box>div").eq(_index).show().siblings().hide();
        //改变选中时候的选项框的样式，移除其他几个选项的样式
        $(this).addClass("change").siblings().removeClass("change");
    });



}

obj.changeProtips = function(res) {
    nisab_zakat = res.data[0].nishab;
    price_zakat = res.data[0].price;
    qty_zakat = res.data[0].qty;

    console.log('changeProtips', changeMoneyFormat.moneyFormat(nisab_zakat))
    text_tips = changeMoneyFormat.moneyFormat(nisab_zakat);//.toString();
    $('.pro-tips').html(res.data[0].nishab);

    // sessionStorage.setItem('detail-share-trophies', 'first');
    // $UI.trigger('alertTrophies')

}

obj.event = function() {

    // alert calculator
    $UI.on('click', '.pro-btn', function(rs) {
        // handleMoney();
        handleProMoney();
        // result_money = Math.ceil((parseInt($('#salary').val()) + parseInt($('#bonuses').val() || 0) - parseInt($('#debt').val() || 0)) * 0.000025) * 1000
        $('.result-money').attr('data-money', result_money).html(changeMoneyFormat.moneyFormat(result_money))
        $('.result-copy').attr('data-clipboard-text', result_money)

        $('.result-title').html(lang.lang64)
        $('.alert-calculator').css('display', 'block')
    })
    $UI.on('click', '.asset-btn', function(rs) {
        handleAssetMoney();

        // result_money = Math.ceil((parseInt($('#gold').val() || 0) + parseInt($('#cash').val() || 0) + parseInt($('#vehicles').val() || 0) - parseInt($('#asset-debt').val() || 0)) * 0.000025) * 1000
        $('.result-money').attr('data-money', result_money).html(changeMoneyFormat.moneyFormat(result_money))
        $('.result-copy').attr('data-clipboard-text', result_money)

        $('.result-title').html(lang.lang65)
        $('.alert-calculator').css('display', 'block')
    })

    $UI.on('click', '.calculator-mask', function(rs) {
        $('.alert-calculator').css('display', 'none')
    })
    $UI.on('click', '.close-calculator', function(rs) {
        $('.alert-calculator').css('display', 'none')
    })

    $('body').on('click', '.result-copy', function() {
        utils.alertMessage(lang.lang62);
    });

}

function handleProMoney() {

    salary = parseInt($('#salary').val().replace(/\./g, ''))
    bonuses = parseInt($('#bonuses').val().replace(/\./g, '') || 0)
    debt = parseInt($('#debt').val().replace(/\./g, '') || 0)

    all_money = salary + bonuses - debt
    result_money = Math.ceil((salary + bonuses - debt) * 0.000025) * 1000
    
        console.log('nisab_zakat', nisab_zakat)
    
        nisab_tips = lang.lang59 + changeMoneyFormat.moneyFormat(nisab_zakat);
        // asset_tips = lang.lang60 + changeMoneyFormat.moneyFormat(price_zakat) + lang.lang60a;

    if (all_money < nisab_zakat) {
        $('.pro-tips').html(nisab_tips);
        // $('.assets-tips').html(asset_tips);

        $('.result-tips').css('display', 'none')
        $('.pro-tips').css('display', 'block')
        // $('.assets-tips').css('display', 'block')
    } else {
        $('.result-tips').css('display', 'none')

    }

    if (result_money < 0) {

        // $('.list-items').css({
        //     'pointer-events': 'none',
        //     'color': '#ccc',
        //     'opacity': '0.2'
        // })


        return result_money = 0;
    } else {
        return result_money;
    }

}

function handleAssetMoney() {

    gold_asset = parseInt($('#gold').val().replace(/\./g, '') || 0)
    cash_asset = parseInt($('#cash').val().replace(/\./g, '') || 0)
    vehicles_asset = parseInt($('#vehicles').val().replace(/\./g, '') || 0)
    debt_asset = parseInt($('#asset-debt').val().replace(/\./g, '') || 0)

    all_money = gold_asset + cash_asset + vehicles_asset - debt_asset
    result_money = Math.ceil((gold_asset + cash_asset + vehicles_asset - debt_asset) * 0.000025) * 1000
     asset_tips = lang.lang60 + changeMoneyFormat.moneyFormat(price_zakat) + lang.lang60a;

    if (all_money < nisab_zakat) {
        $('.assets-tips').html(asset_tips);
        $('.result-tips').css('display', 'none')
        $('.assets-tips').css('display', 'block')
    } else {
        $('.result-tips').css('display', 'none')
    }

    if (result_money < 0) {

        // $('.list-items').css({
        //     'pointer-events': 'none',
        //     'color': '#ccc',
        //     'opacity': '0.2'
        // })

        return result_money = 0;
    } else {
        return result_money;
    }

    // return result_money;

}

// function handleMoney() {

//     salary = parseInt($('#salary').val().replace(/\./g, ''))
//     bonuses = parseInt($('#bonuses').val().replace(/\./g, '') || 0)
//     debt = parseInt($('#debt').val().replace(/\./g, '') || 0)



//     gold_asset = parseInt($('#gold').val().replace(/\./g, '') || 0)
//     cash_asset = parseInt($('#cash').val().replace(/\./g, '') || 0)
//     vehicles_asset = parseInt($('#vehicles').val().replace(/\./g, '') || 0)
//     debt_asset = parseInt($('#asset-debt').val().replace(/\./g, '') || 0)


//     if (tab_flag == 0) {

//         all_money = salary + bonuses - debt
//         result_money = Math.ceil((salary + bonuses - debt) * 0.000025) * 1000

//         if (all_money < 5240000) {
//             $('.result-tips').css('display', 'none')
//             $('.pro-tips').css('display', 'block')
//         } else {
//             $('.result-tips').css('display', 'none')

//         }
//     } else if (tab_flag == 1) {

//         all_money = gold_asset + cash_asset + vehicles_asset - debt_asset
//         result_money = Math.ceil((gold_asset + cash_asset + vehicles_asset - debt_asset) * 0.000025) * 1000

//         if (all_money < 51000000) {
//             $('.result-tips').css('display', 'none')
//             $('.assets-tips').css('display', 'block')
//         } else {
//             $('.result-tips').css('display', 'none')
//         }
//     }

//     if (result_money <= 0) {
//         result_money = 0
//         return result_money;
//     } else {
//         return result_money;
//     }
// }
export default obj;