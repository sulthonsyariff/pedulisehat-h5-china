import mainTpl from '../tpl/_calculator.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
// import bankList from './_bankList'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

import getSubmitParam from "./_getSubmitParam";
import _validate from './_validate';
import utils from 'utils';
import store from 'store'


/* translation */
import zakatCalculator from 'zakatCalculator'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(zakatCalculator);
/* translation */
let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let reqSelect = location.href
let category_id;
/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function (res) {

    res.domainName = domainName;
    res.lang = lang;
    res.isAndroid = utils.browserVersion.android;

    $UI.prepend(mainTpl(res));
    changeMoneyFormat.moneyFormat('#result-money');

    console.log('res=', res);

    fastclick.attach(document.body);
    
    obj.event(res);



};

obj.event = function (res) {
      // alert zakat channel
      $UI.on('click', '.zakat_channel', function (rs) {
        $('.alert-list').css('display', 'block')
    })

    // alert payment channel
    $UI.on('click', '.payment_channel', function (rs) {
        $UI.trigger('getProjectPoundage',$('.zakat-name').attr('data-project_id') );

        $('.alert-bank').css('display', 'block')
    })

    // open or close anonymous
    $UI.on('click', '.open-icon', function () {
        $(this).toggleClass('open');
    });


    // submit
    $UI.on('click', '.nextBtn', function () {
        submitData = getSubmitParam.init();

        console.log('submitData = ', submitData)
        // if (submitData.comment) {
        //     is_comment = true
        // } else {
        //     is_comment = false
        // }
        // sensors.track('DonateInfoFill', {
        //     from: document.referrer,
        //     payment_method: submitData.payment_channel,
        //     order_amount: submitData.money,
        //     is_anonim: anonymous,
        //     is_comment: is_comment,
        // })

        // 项目分类不一样，验证条件不一样: category_id == 13为zakat类项目
        if (_validate.check(submitData, lang)) {
            store.set('donateMoney', submitData);
            utils.showLoading(lang.lang12);
            $UI.trigger('submit-b', [submitData]);
        }

    });
}

export default obj;