/*
 * 校验表单信息
 */
import utils from 'utils'
import 'jq_cookie' //ajax cookie

let reqObj = utils.getRequestParams();
let fromLogin = reqObj['fromLogin'];
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let obj = {};

obj.check = function(param, lang) {
    console.log('validate', 'param:', param);

    if (!$(".zakat_channel .zakat-name").attr('data-project_id')) {
        utils.alertMessage(lang.lang66);
        return false;
    }

    if (!param.payment_channel) {
        utils.alertMessage(lang.lang5);
        return false;
    }


    if (isNaN(param.money)) {
        utils.alertMessage(lang.lang63);
        return false;
    }

    if ((param.payment_channel == 5 ||
            param.payment_channel == 6 ||
            param.payment_channel == 7 ||
            param.payment_channel == 4 ||
            param.payment_channel == 1 ||
            param.payment_channel == 302) &&
        !(param.money > 999 && param.money % 1000 == 0)) {
        // utils.alertMessage('-----');
        utils.alertMessage(lang.lang67);

        return false;

    } else if ((param.payment_channel != 5 &&
            param.payment_channel != 6 &&
            param.payment_channel != 7 &&
            param.payment_channel != 4 &&
            param.payment_channel != 1 &&
            param.payment_channel != 302) &&
        !(param.money > 9999 && param.money % 1000 == 0)) {

        utils.alertMessage(lang.lang67);

        console.log('other', param.payment_channel, (param.payment_channel != 4 && param.payment_channel != 1))
        return false;

    }
    if (param.money <= 0) {
        utils.alertMessage(lang.lang63);
        return false;
    }



    if (!user_id) {
        if (!param.user_name.length) {
            utils.alertMessage(lang.lang32);
            return false;
        }

        if (param.phone.length < 7) {
            utils.alertMessage(lang.lang31);
            return false;
        }
    }


    return true;
}


export default obj;