let baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'zakatCalculator',
    htmlFileURL: 'html/zakatCalculator.html',
    appDir: 'js/zakatCalculator',
    uglify: true,
    hash: '',
    mode: 'development'
})