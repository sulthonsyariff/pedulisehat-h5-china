import 'jq_cookie' //ajax cookie
import store from 'store'

import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import utils from 'utils'
import turingVerification from 'turingVerification'
import _jump from './_jump_redirect'

let obj = {};
let UI = view.UI;
let $UI = $('body');

/* translation */
import modifyName from 'modifyName'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyName);
/* translation */

let reqObj = utils.getRequestParams();
let fb_code = reqObj["code"];
let flag = reqObj["flag"];
let loginFrom = reqObj["loginFrom"];
let from = reqObj["from"]; //https://qa.pedulisehat.id/login.html?from=sinarmas 金光登录输入邮箱流程
from && store.set('from', 'sinarmas'); //金光登录输入邮箱流程需要

let jump_qf_redirect; //登陆成功后回跳回相关页面地址
let redirectURI; //第三方登陆后跳回login地址，区分Google 还是 Facebook
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';
let countWrongPassword = 0;

utils.showLoading();
view.init({});


/*
 * 如果存在回跳地址，将回跳地址存在本地
 * 如果不存在回跳地址，将首页存本地，（ 排除第三方登陆回跳login干扰,避免之前存好的回调地址被替换）
 */
// store.set('login', {
//     qf_redirect_store: reqObj.qf_redirect ? reqObj.qf_redirect : encodeURIComponent('/')
// });
if (reqObj.qf_redirect) {
    store.set('login', {
        qf_redirect_store: reqObj.qf_redirect
    });
} else if (!reqObj.qf_redirect && !flag) {
    if (loginFrom == 'gtry' || loginFrom == 'gtryPay' || loginFrom == 'sijicorona') {
        store.set('login', {
            qf_redirect_store: reqObj.qf_redirect
        });
    } else {
        store.set('login', {
            qf_redirect_store: encodeURIComponent('/')
        });
    }
}

//登陆成功后获取存在本地的回跳页面地址
if (store.get('login') && store.get('login').qf_redirect_store) {
    jump_qf_redirect = decodeURIComponent(store.get('login').qf_redirect_store);
}

/**
 * judgeURL中是否带有code / state
 */
if (fb_code) {
    if (flag == 'google') {
        redirectURI = location.origin + "/login.html?flag=google"
    } else {
        redirectURI = location.origin + "/login.html?flag=facebook"
    }
    // console.log('===login start===');
    // login(redirectURI);
    loginByThirdParty(redirectURI);
}

function loginByThirdParty(redirectURI) {
    utils.showLoading();

    // 1， 如果新用户或未绑定手机者将返回uid, openId 两个字段， 如果是这种情况则跳转到绑定手机号的界面；
    // 2， 如果已经绑定了手机号， 直接登录， 接口返回参数和以往的一样。
    model.loginByThirdParty({
        code: decodeURIComponent(reqObj["code"]), //decodeURIComponent解决gmail登陆400bug
        state: reqObj['state'],
        platform: 'h5', //h5|android|ios
        redirectURI: redirectURI,
        authType: reqObj['flag'] == 'google' ? 102 : 101 //facebook:101,google:102
    }).done(function(res) {
        if (res.code == 0) {

            // console.log('===model.login, res.code == 0', res);

            if (res.data && res.data.openId) {
                // 跳转手机绑定页面
                _jump.toPhoneBind(res);
                // 跳转手机绑定页面
                // location.href = '/phoneBind.html?uid=' + (res.data.uid ? res.data.uid : '') + '&openId=' + res.data.openId + '&qf_redirect=' + encodeURIComponent('/login.html?qf_redirect=' + store.get('login').qf_redirect_store);

            } else if (res.data && res.data.accessToken) {
                console.log('login res === ', res.data.userInfo.mobile_country_code + res.data.userInfo.mobile);

                //sensors
                // sensors.track('Login', {
                //     account: res.data.userInfo.mobile_country_code + res.data.userInfo.mobile,
                //     login_method: reqObj['flag'] == 'google' ? 'google' : 'facebook',
                //     is_succeed: true,
                //     fail_reason: ''
                // })

                storeCookie(res) //存localstorage，并跳转

                _jump.toRedirect(jump_qf_redirect, res);

                // console.log('location.href = jump_qf_redirect =', jump_qf_redirect);
                // sessionStorage.setItem('OldHome',3)
                // setTimeout(function() {
                //     location.href = jump_qf_redirect;
                // }, 30)
            }

        } else {

            //sensors
            // sensors.track('Login', {
            //     account: '',
            //     login_method: authType,
            //     is_succeed: false,
            //     fail_reason: res.msg
            // })

            utils.alertMessage(res.msg)
        }
    }).fail(utils.handleFail)
}

/**
 * 预登陆
 */
$UI.on('prelogin', function(e, param) {
    utils.showLoading(lang.lang4);

    model.preLogin(param).done(function(res) {
        if (res.code == 0) {
            // console.log('===prelogin：res', param, res.data.redirectURI);

            location.href = res.data.redirectURI;
        } else {
            utils.alertMessage(res.msg)
        }
    }).fail(utils.handleFail);
});

//sendCode
$UI.on('sendCode', function(e, objData) {

    model.sendCode({
        param: {
            channel: objData.channel,
            mobile: parseInt(objData.mobile),
            mobile_country_code: parseInt(objData.mobile_country_code),
            token: objData.token,
            session_id: objData.session_id,
            sig: objData.sig,
            // image_key: objData.image_key,
            // image_code: objData.image_code,
        },
        success: function(rs) {
            if (rs.code == 0) {
                if (objData.channel == 'WhatsApp') {
                    turingVerification.invokeWhatsAppSettime();
                } else {
                    utils.alertMessage(rs.msg)
                    turingVerification.invokeSMSSettime();
                }

                // turingVerification.changeBtn();
                utils.hideLoading();
            } else if (rs.code == 4151) {
                // console.log('4151')
                utils.alertMessage(rs.msg)
                    // turingVerification.updateTuringVerifyImg();
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        // error: function (rs) {
        //     if (rs.code == 4151) {
        //         turingVerification.updateTuringVerifyImg();
        //     }
        // }
        error: utils.handleFail
    })
})


// 手机号登陆或注册
$UI.on('sumbit', function(e, objData) {
    utils.showLoading(lang.lang4);

    model.loginByMobile({

        param: {
            mobile: objData.mobile,
            mobile_country_code: objData.mobile_country_code,
            captcha: objData.captcha,
            authType: objData.authType,
            platform: objData.platform,
            uid: '',
            openId: '',
            statistics_link: statistics_link
        },
        success: function(res) {

            if (res.code == 0) {
                // console.log('mobile submit:', res);

                storeCookie(res) //存localstorage，并跳转

                if (res.data.userInfo.setting_password == 0) {
                    //sensors
                    // sensors.track('Login', {
                    //         account: res.data.userInfo.mobile_country_code + res.data.userInfo.mobile,
                    //         login_method: 'phone',
                    //         is_succeed: true,
                    //         fail_reason: ''
                    //     },

                        _jump.toSetPassword(jump_qf_redirect) // 数据采集需要，故放在里面

                } else {
                    //sensors
                    // sensors.track('Login', {
                    //     account: res.data.userInfo.mobile_country_code + res.data.userInfo.mobile,
                    //     login_method: 'phone',
                    //     is_succeed: true,
                    //     fail_reason: ''
                    // })

                    _jump.toRedirect(jump_qf_redirect, res);

                }

            } else {
                //sensors
                // sensors.track('Login', {
                //     account: '',
                //     login_method: authType,
                //     is_succeed: false,
                //     fail_reason: res.msg
                // })
            }
        },
        error: utils.handleFail
    })

})


// loginByPassword
$UI.on('passwordLogin', function(e, objData) {
    utils.showLoading(lang.lang4);

    model.loginByPassword({
        param: {
            mobile_country_code: objData.mobile_country_code,
            mobile: objData.mobile,
            code: objData.code,
            password: objData.password,
            login_with: 'mobile',
            uid: '',
            openId: ''
        },
        success: function(res) {

            if (res.code == 0) {
                // console.log('mobile submit:', res);
                // sensors.track('Login', {
                //     account: res.data.userInfo.mobile_country_code + res.data.userInfo.mobile,
                //     login_method: 'phone-pwd',
                //     is_succeed: true,
                //     fail_reason: ''
                // })

                storeCookie(res) //存localstorage，并跳转

                _jump.toRedirect(jump_qf_redirect, res);
                // setTimeout(function() {
                //     location.href = jump_qf_redirect;
                // }, 30)
            } else {
                //sensors
                // sensors.track('Login', {
                //     account: '',
                //     login_method: authType,
                //     is_succeed: false,
                //     fail_reason: res.msg
                // })
            }
        },
        error: function(err) {
            utils.hideLoading();
            utils.alertMessage(err.msg)
            countWrongPassword++;

            if (countWrongPassword == 3) {
                // reset wrong password
                countWrongPassword = 0;
                localStorage.setItem('dateWrongPassword', new Date(new Date().getTime() + 15 * 60000));
                view.triggerCountWrongPassword();
            }
        }
    })

})

function storeCookie(res) {
    //sensors
    // sensors.track('Login', {
    //     account: res.data.userInfo.mobile_country_code + res.data.userInfo.mobile,
    //     login_method: authType,
    //     is_succeed: true,
    //     fail_reason: ''
    // })

    // remove localstorage tokenFcmExisting
    localStorage.removeItem("tokenFcmExisting");

    //存localstorage，并跳转
    $.cookie('passport', JSON.stringify({
        accessToken: res.data.accessToken,
        expiresIn: res.data.expiresIn,
        refreshToken: res.data.refreshToken,
        tokenType: res.data.tokenType,
        tokenExpires: res.data.tokenExpires,
        uid: res.data.uid,
        signature: res.data.signature,
        serverTimestamp: (new Date()).getTime() // 获取登陆成功时当前时间，判断token是否过期时使用
    }), {
        expires: 365,
        path: '/',
        domain: 'pedulisehat.id'
    });
}