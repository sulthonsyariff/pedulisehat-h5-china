import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

//未登录使用手机号登陆或注册
obj.loginByMobile = function (o) {
    var url = domainName.passport + '/v2/login/bymobile';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

// login by password
obj.loginByPassword = function (o) {
    let url = domainName.passport + '/v1/login/bypassword';

    ajaxProxy.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(o.param)
    }, o)
};

/**
 * loginByThirdParty
	 * {
		"code":"",
		"state":"",
		"platform":"",
		"auth_type": = 101(101: facebook, 102: google)
	}
     */
obj.loginByThirdParty = function (o) {
    var ajaxType = 'POST';
    var url = domainName.passport + '/v2/oauth2/login';

    if (isLocal) {
        ajaxType = 'GET';
        url = 'mock/login.json';
    }

    return $.ajax({
        url: url,
        type: ajaxType,
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        xhrFields: {
            withCredentials: true
        },
        data: JSON.stringify(o),
        crossDomain: true,
        cache: false
    })
}

/**
 * authType = 101 (101:facebook,102:google)
 * platform = h5
 */
obj.preLogin = function (params) {
    var url = domainName.passport + '/v1/oauth2/prelogin?authType=' + params.authType + '&platform=h5' + '&redirectURI=' + params.redirectURI;

    if (isLocal) {
        url = '/mock/prelogin.json';
    }

    return $.ajax({
        url: url,
        type: 'get',
    })
};

//发送验证码
obj.sendCode = function (o) {
    var url = domainName.passport + '/v3/sms/captcha?mobile=' + o.param.mobile + '&mobile_country_code=' + o.param.mobile_country_code + '&channel=' + o.param.channel + '&token=' + o.param.token + '&session_id=' + o.param.session_id + '&sig=' + o.param.sig;
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}



export default obj;