import utils from 'utils'
import store from 'store'
let obj = {};

let sinarmasEmailArray = ['@sinarmasforestry.com', '@app.co.id,', '@simasjiwa.co.id', '@sinarmas.co.id', '@sinarmasmsiglife.co.id', '@banksinarmas.com', '@beraucoal.co.id', '@bsa-logistics.co.id', '@bizzy.co.id', '@borneo-indobara.com', '@danamas.com', '@dsspower.co.id,', '@ekahospital.com', '@ekatjipta.org', '@ptesm.com', '@excellerate.co.id', '@finmas.co.id', '@goldenenergymines.com', '@ibstower.com', '@ibsmulti.com', '@kiic.co.id', '@myrepublic.net.id', '@nusantarare.com', '@asuransisimasnet.com', '@sinarmas-am.co.id', '@sinarmascepsa.com', '@sinarmasforestry.com', '@sinarmasland.com', '@sinarmasmining.com', '@simasfinance.co.id', '@sinarmassekuritas.co.id', '@swa-jkt.com', '@sinarsyno.com', '@sinarmas-agri.com', '@sinarmas-agribusiness.com', '@smartfren.com', '@smdv.com', '@lifewithsun.com', '@sinarmas.org', '@ekatjipta.org']
let from = store.get('from') ? store.get('from') : ''
// let sinarmasEmailArrayExample = '@' + 'sdadasd@ekatjipta.org'.split('@')[1]


// var EmailReg = sinarmasEmailArray.some(function(item,index){
//     if(item == sinarmasEmailArrayExample){
//         return true  
//     }
// })
// console.log('sinarmasEmailArray',sinarmasEmailArrayExample,sinarmasEmailArray.length(),EmailReg);


// redirect url
obj.toRedirect = function (jump_qf_redirect, res) {
    // judge sinarmas:金光登录输入邮箱流程需要
    // 如果没有金光后缀的邮箱则跳转金光添加邮箱页面


    let sinarmasEmailSuffix = '@' + res.data.userInfo.email.split('@')[1];

    let sinarmasReg = sinarmasEmailArray.some(function (item, index) {
        if (item == sinarmasEmailSuffix) {
            return true
        }
    })

    console.log('8', sinarmasReg);



    if (store.get('from') == 'sinarmas' && !sinarmasReg) {
        setTimeout(function () {
            location.href = '/bindEmail.html';
        }, 30)


    } else {
        setTimeout(function () {
            location.href = jump_qf_redirect;
        }, 30)
    }

    // if (store.get('from') == 'sinarmas' && res.data.userInfo.email.indexOf('@sinarmas.co.id') == -1) {
    //     setTimeout(function () {
    //         location.href = '/bindEmail.html';
    //     }, 30)
    // } else {
    //     setTimeout(function () {
    //         location.href = jump_qf_redirect;
    //     }, 30)
    // }

}

// setPassword
obj.toSetPassword = function (jump_qf_redirect) {
    location.href = '/setPassword.html?jump_qf_redirect=' + encodeURIComponent(jump_qf_redirect)
}

// 跳转手机绑定页面
obj.toPhoneBind = function (res) {
    // 跳转手机绑定页面
    location.href = '/phoneBind.html?uid=' +
        (res.data.uid ? res.data.uid : '') +
        '&openId=' + res.data.openId +
        '&qf_redirect=' + encodeURIComponent('/login.html?qf_redirect=' + store.get('login').qf_redirect_store)
         + '&from=' + from;
}


export default obj;