// 公共库
import utils from 'utils'
import fastclick from 'fastclick'
import mainTpl from '../tpl/main.juicer'
import phoneTpl from '../tpl/_phone.juicer'
import domainName from 'domainName'; //port domain name
import validate from '../js/_validate'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'
import turingVerification from 'turingVerification'

/* translation */
import login from 'login'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(login);
/* translation */

let [obj, $UI] = [{}, $('body')];
let isAgreePolicy = true; //是否同意协议
let CACHED_KEY = 'switchLanguage';
let Lang = store.get(CACHED_KEY);

let reqObj = utils.getRequestParams();
let qf_redirect = reqObj["qf_redirect"] ? decodeURIComponent(reqObj["qf_redirect"]) : '/';
let loginFrom = reqObj["loginFrom"];
let disabledButton = false;

obj.UI = $UI;

if (utils.browserVersion.androids) {
    $('body').addClass('inAndroid');
}

obj.init = function(rs) {


    rs.domainName = domainName;
    $('title').html(titleLang.login);

    rs.lang = lang;
    rs.Lang = Lang ? Lang.lang : 'id';
    $UI.append(mainTpl(rs)); //主模版
    $('.phoneVerification').append(phoneTpl(rs));
    turingVerification.init(rs);


    utils.hideLoading();

    fastclick.attach(document.body);
    clickHandle(rs);
    watchKeyboard();
    store.set('loginBy', 1)

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    checkSessionWrongPassword();
};

function checkSessionWrongPassword() {
    let dateDeadline = localStorage.getItem('dateWrongPassword');
    const deadline = new Date(dateDeadline);

    if (new Date(deadline).getTime() < new Date().getTime()) {
        disabledButton = false;
        $('.countWrongPassword').removeClass('active');
    } else {
        countWrongPassword();
    }
}

obj.triggerCountWrongPassword = function() {
    countWrongPassword();
};

function countWrongPassword() {
    $('.countWrongPassword').addClass('active');
    let dateDeadline = localStorage.getItem('dateWrongPassword');
    const deadline = new Date(dateDeadline);
    initializeClock(deadline);
}

function getTimeRemaining(endtime) {
    const total = Date.parse(endtime) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    
    return {
      total,
      minutes,
      seconds
    };
}
  
function initializeClock(endtime) {
    // disabled button
    disabledButton = true;
    $('.loginByPassword').addClass('disabled');

    function updateClock() {
        const t = getTimeRemaining(endtime);
        
        // minutes
        if (t.minutes > 0) {
            if (t.minutes < 10) {
                $('.count-minutes').html('0' + t.minutes + ' ' + lang.lang27);
            } else {
                $('.count-minutes').html(t.minutes + ' ' + lang.lang27);
            }
        } else {
            $('.count-minutes').html('');
        }


        // seconds
        if (t.minutes <= 0) {
            if (t.seconds < 10) {
                $('.count-seconds').html('0' + t.seconds + ' ' + lang.lang28);
            } else {
                $('.count-seconds').html(t.seconds + ' ' + lang.lang28);
            }
        } else {
            $('.count-seconds').html('');
        }

        if (t.total <= 0) {
            clearInterval(timeinterval);
            // remove localstorage
            localStorage.removeItem('dateWrongPassword');
            // remove class
            $('.countWrongPassword').removeClass('active');

            // disabled button
            disabledButton = false;
            $('.loginByPassword').removeClass('disabled');
        }
    }

    updateClock();
    const timeinterval = setInterval(updateClock, 1000);
}

function clickHandle(rs) {

    // facebook
    $('body').on('click', '#facebookBtn', function(params) {
        if (isAgreePolicy) {
            // console.log('facebook login');
            $UI.trigger('prelogin', {
                authType: '101',
                redirectURI: location.origin + "/login.html?flag=facebook"
            }); //facebook
        } else {
            utils.alertMessage(lang.lang9);
        }
    });

    // google
    $('body').on('click', '#googleBtn', function(params) {
        if (isAgreePolicy) {
            // console.log('google login');
            $UI.trigger('prelogin', {
                authType: '102',
                redirectURI: location.origin + "/login.html?flag=google"
            });
        } else {
            utils.alertMessage(lang.lang9);
        }
    });

    // goBack
    $('body').on('click', '#signIn', function(e) {
        if (loginFrom == 'gtryPay') {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id'
            } else {
                location.href = 'https://gtry.pedulisehat.id'
            }

        } else if (loginFrom == 'gtryTopUp') {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id/membershipCard.html'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id/membershipCard.html'
            } else {
                location.href = 'https://gtry.pedulisehat.id/membershipCard.html'
            }
        } else if (loginFrom == 'sijicorona') {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://asuransi-qa.pedulisehat.id/sijicorona'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://asuransi-pre.pedulisehat.id/sijicorona'
            } else {
                location.href = 'https://asuransi.pedulisehat.id/sijicorona'
            }
        } else {
            if (utils.validXss(qf_redirect)) {
                location.href = qf_redirect;
            } else {
                // utils.alertMessage('Oops, something went wrong :(');
            }
        }
        // history.go(-1);
    });

    // change login method
    $('body').on('click', '.password-tips', function(e) {
        store.set('loginBy', 2)

        $('.changeLoginMethods').removeClass('active');
        $(this).addClass('active');

        // $('.password-tips').css('display', 'none')
        $('.phoneVerification').css('display', 'none')
        $('.passwordLogin').css('display', 'block')
            // $('.SMS-tips').css('display', 'block')
    });

    $('body').on('click', '.SMS-tips', function(e) {
        store.set('loginBy', 1)
        $('.changeLoginMethods').removeClass('active');
        $(this).addClass('active');

        // $('.SMS-tips').css('display', 'none')
        $('.passwordLogin').css('display', 'none')
        $('.phoneVerification').css('display', 'block')
            // $('.password-tips').css('display', 'block')
    });

    // agree policy
    $('body').on('click', '.agree', function(e) {
        isAgreePolicy = !isAgreePolicy;
        $('.agree').toggleClass('notAgree');
    });

    // termCondition
    $('body').on('click', '.use.policy', function(e) {
        location.href = '/termCondition.html';

        // if (Lang && Lang.lang && Lang.lang == 'en') {
        //     location.href = '/policy/Terms-and-Conditions-of-Use.html';
        // } else {
        //     location.href = '/policy/Syarat-dan-Ketentuan-Penggunaan.html';
        // }
    });

    // privacypolicy
    $('body').on('click', '.privacy.policy', function(e) {
        location.href = '/privacyPolicy.html';
        // if (Lang && Lang.lang && Lang.lang == 'en') {
        //     location.href = '/policy/Privacy-Policy.html';
        // } else {
        //     location.href = '/policy/Kebijakan-Privasi.html';
        // }
    });

    //phoneVerification toggle
    // $("body").on("click", ".orTips", function(e) {
    //     // $('.page-wrapper').addClass('animate');

    //     $(this).next(".phoneVerification").slideDown();
    //     $('.phoneVerification').css('display', 'block');
    //     $(this).css("display", "none");

    //     var winHeight = $(window).height(); //获取当前页面高度
    //     if (winHeight < 600) {
    //         $('.header').css('display', 'none');
    //     }

    //     // if ($(this).next(".phoneVerification").css("display") == "none") {
    //     //     $(this).next(".phoneVerification").css({
    //     //         "position": "absolute",
    //     //         "top": "170px",
    //     //         "width": "100%"
    //     //     });
    //     //     $(this).next(".phoneVerification").slideDown();
    //     //     $(this).parents(".content").css("bottom", "48%");
    //     //     // $(this).css("display","none")
    //     // }
    //     // else
    //     // {
    //     //     $(this).next(".phoneVerification").slideUp();
    //     //     $(this).parents(".content").css("bottom", "130px");
    //     // }
    // });

    $('.sendCode').off('click').on('click', function() {
        let sumbitData = getSumbitParam();
        let channel = 'WhatsApp';
        sumbitData.channel = channel;
        if ($('#number').val().length < 7) {
            utils.alertMessage(lang.lang15);
            return false;
        } else {
            // utils.alertMessage(lang.lang24);
            // turingVerification.getTuringVerifyImg(sumbitData);
            // turingVerification.init(sumbitData);
            turingVerification.nc_cb(sumbitData);


            //send code
            // invokeWhatsAppSettime("#sendCode");


            // $UI.trigger('sendCode', [sumbitData]);
        }
    })

    //send whatsapp code
    // $('body').on('click', '.sendCode', function () {
    //     let sumbitData = getSumbitParam();
    //     let channel = 'WhatsApp';
    //     sumbitData.channel = channel;
    //     if ($('#number').val().length < 7) {
    //         utils.alertMessage(lang.lang15);
    //         return false;
    //     } else {
    //         // utils.alertMessage(lang.lang24);
    //         // turingVerification.getTuringVerifyImg(sumbitData);
    //         turingVerification.init(sumbitData);


    //         //send code
    //         // invokeWhatsAppSettime("#sendCode");
    //         // sensors.track('GetCode', {
    //         //     service_type: 'login',
    //         //     channel: 'WhatsApp',
    //         // }, );

    //         // $UI.trigger('sendCode', [sumbitData]);
    //     }
    // });

    //send SMS code
    $('body').on('click', '.SMSCode', function() {
        let sumbitData = getSumbitParam();
        let channel = 'Sms';
        sumbitData.channel = channel;
        if ($('#number').val().length < 7) {
            utils.alertMessage(lang.lang15);
            return false;
        } else {
            //send code
            // utils.alertMessage(lang.lang24);
            // turingVerification.init(sumbitData);
            turingVerification.nc_cb(sumbitData);

            // invokeSMSSettime("#sendSMSCode");

            // $UI.trigger('sendCode', [sumbitData]);
        }
    });

    // login by phone
    $('body').on('click', '.loginByPhone', function(e) {
        let sumbitData = getSumbitParam();
        console.log('=====', sumbitData)
        if (validate.check(sumbitData, lang)) {
            if (isAgreePolicy) {
                $UI.trigger('sumbit', [sumbitData]);
            } else {
                utils.alertMessage(lang.lang9);
            }
        }
    })


    // login by password
    $('body').on('click', '.loginByPassword', function(e) {
        // console.log('wrong',);
        if (!disabledButton) {
            let sumbitData = getSumbitParam();
            console.log('password=====', sumbitData)
            if (validate.check(sumbitData, lang)) {
                if (isAgreePolicy) {
                    $UI.trigger('passwordLogin', [sumbitData]);
                } else {
                    utils.alertMessage(lang.lang9);
                }
            }
        }
    })

    // find back password
    $('body').on('click', '.findBack', function(e) {
        // store.set('findBack',true)
        location.href = '/phoneNumVerification.html?go=findBackPassword'
    })
}

function getSumbitParam() {
    let mobile;
    if (store.get('loginBy') && store.get('loginBy') == 1) {
        mobile = $('#number').val().replace(/\b(0+)/gi, "")
    } else {
        mobile = $('#password-number').val().replace(/\b(0+)/gi, "")
    }
    return {
        mobile: mobile,
        mobile_country_code: 62,
        captcha: $('#code').val(),
        authType: 100,
        platform: 'h5',
        uid: '',
        openId: '',
        password: $('#password').val()
    }
}

//倒计时
function invokeWhatsAppSettime(obj) {
    var countdown = 60;
    settime(obj);

    function settime(obj) {
        if (countdown == 0) {
            $(obj).attr("disabled", false).removeClass('active');
            $(obj).text(lang.lang10);
            countdown = 60;
            return;
        } else {
            $(obj).attr("disabled", true).addClass('active');
            $(obj).text("" + countdown + " s");
            countdown--;
        }
        setTimeout(function() {
            settime(obj)
        }, 1000)
    }
}

function invokeSMSSettime(obj) {
    var countSMSdown = 60;
    settime(obj);

    function settime(obj) {
        if (countSMSdown == 0) {
            $(obj).attr("disabled", false).css({
                'pointer-events': 'auto',
                'color': '#999'
            });
            // $(obj).text(lang.lang10);
            countSMSdown = 60;
            return;
        } else {
            $(obj).attr("disabled", true).css({
                'pointer-events': 'none',
                'color': '#ccc'
            });
            // $(obj).text("" + countSMSdown + " s");
            countSMSdown--;
        }
        setTimeout(function() {
            settime(obj)
        }, 1000)
    }
}


function watchKeyboard() {
    var winHeight = $(window).height(); //获取当前页面高度
    // console.log('$(window).height()', $(window).height())

    $(window).resize(function() {
        console.log('resize');


        var thisHeight = $(this).height();

        if (winHeight - thisHeight > 140) {
            //键盘弹出
            $('.footer').css('display', 'none');
            $('.content').css('bottom', '20px');
            $('.bg').css('bottom', '376px');

            $('.header').css('display', 'none');

        } else {
            //键盘收起
            $('.footer').css('display', 'block');
            $('.content').css('bottom', '100px');
            $('.bg').css('bottom', '460px');

            if (winHeight < 600) {
                $('.header').css('display', 'none');
            } else {
                $('.header').css('display', 'block');
            }
        }

    })
}

export default obj;