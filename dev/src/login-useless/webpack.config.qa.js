var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'login',
    htmlFileURL: 'html/login.html',
    appDir: 'js/login',
    uglify: true,
    hash: '',
    mode: 'development'
})