import mainTpl from '../tpl/verificationSuccess.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import verificationSuccess from 'verificationSuccess'
import qscLang from 'qscLang'
let lang = qscLang.init(verificationSuccess);
/* translation */

let [obj, $UI] = [{}, $('body')];
let short_link = utils.getRequestParams().short_link;
let project_id = utils.getRequestParams().project_id;
let from = utils.getRequestParams().from;

obj.UI = $UI;

obj.init = function(rs) {
    rs.JumpName = '';
    commonNav.init(rs);

    rs.domainName = domainName;
    rs.lang = lang;

    $UI.append(mainTpl(rs));
    commonFooter.init(rs);

    fastclick.attach(document.body);

    $('.viewBtn').on('click', function(e) {
        location.href = (from == 'detail') ?
            '/' + short_link :
            '/myWallet.html?project_id=' + project_id + '&short_link=' + short_link;
    });

    // console.log('view:', rs);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};




export default obj;