var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'verificationSuccess',
    htmlFileURL: 'html/verificationSuccess.html',
    appDir: 'js/verificationSuccess',
    uglify: true,
    hash: '',
    mode: 'production'
})