var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'verificationSuccess',
    htmlFileURL: 'html/verificationSuccess.html',
    appDir: 'js/verificationSuccess',
    uglify: true,
    hash: '',
    mode: 'development'
})