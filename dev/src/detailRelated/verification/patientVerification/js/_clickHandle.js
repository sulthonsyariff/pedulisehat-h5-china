import validate from './_validate' // validate the form
import utils from 'utils'
import imgUploader from 'uploadCloudinaryMore'

let [obj, $UI] = [{}, $('body')];
let inputFirstFlag = true; //判断是否第一次点击
let inputFirstFlag2 = true; //判断是否第一次点击

let uploadList = '.uploadList'; // img container
let uploadKey = 'cover'; // upload key
let uploadList2 = '.uploadList2'; // img container
let uploadKey2 = 'cover2'; // upload key

obj.init = function(params) {
    // console.log('clickHandle',params)
    // submit
    $('.submitBtn').on('click', function() {
        let submitData = getSubmitParam();

        // console.log('params.lang====', params.lang);

        if (validate.check(submitData, params.lang)) {
            $UI.trigger('submit', [submitData]);
            utils.showLoading(params.lang.lang28);
        }
    });

    // alert-box :1
    $('body').on('click', '#webPicPicker', function(e) {
        // console.log($('.uploadList img').length)
        if (inputFirstFlag) {
            inputFirstFlag = !inputFirstFlag;
            $('.alert-ID-box').css({
                'display': 'block'
            });
            e.preventDefault();
        }
    });

    // alert-box :2
    $('body').on('click', '#webPicPicker2', function(e) {
        if (inputFirstFlag2) {
            inputFirstFlag2 = !inputFirstFlag2;
            $('.alert-box').css({
                'display': 'block'
            });
            e.preventDefault();
        }
    });

    // alert-box:got it
    $('body').on('click', '.confirm', function(e) {
        $('.alert-box').css({
            'display': 'none'
        });
        $('.alert-ID-box').css({
            'display': 'none'
        });
    });

    /**
     * 获取提交数据
     */
    function getSubmitParam() {
        return {
            project_id: utils.getRequestParams().project_id,
            name: $('#real_name').val(),
            id_card: $('#ID_number').val().replace(/\s|\xA0/g, ""), //去除所有空格
            disease: $('#Disease').val(),
            card_images: imgUploader.getImageList(uploadList, uploadKey),
            disease_images: imgUploader.getImageList(uploadList2, uploadKey2)
        }
    }
}

export default obj;