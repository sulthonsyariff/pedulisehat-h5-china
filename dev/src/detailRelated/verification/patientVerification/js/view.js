// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/patientVerification.juicer'
import imgUploader from 'uploadCloudinaryMore'
import utils from 'utils'
import fastclick from 'fastclick'
import 'fancybox'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'

import alertIdBoxTpl from '../tpl/alert-ID-box.juicer'
import alertDiagnosticBoxTpl from '../tpl/alert-diagnostic-box.juicer'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import clickHandle from './_clickHandle' //点击事件
// import sensorsActive from 'sensorsActive'

/* translation */
import patientVerification from 'patientVerification'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(patientVerification);
/* translation */
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from']; //区分是从钱包页来的还是从详情页来的

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* ID upload relative */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploadList'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 1; // limit number
/* ID upload relative */

/* Diagnostic upload relative */
let uploadWrapper2 = '#uploadWrapper2';
let uploadBtn2 = 'upload-btn2'; // upload btn
let uploadList2 = '.uploadList2'; // img container
let uploadKey2 = 'cover2'; // upload key
let uploadNumLimit2 = 8; // limit number
/* Diagnostic upload relative */

obj.init = function(rs) {
    // console.log('view:', rs);
    rs.domainName = domainName;
    rs.JumpName = titleLang.verification;

    // 从哪里来回哪里去
    rs.commonNavGoToWhere = (from == 'detail') ?
        ('/' + short_link) :
        ('/myWallet.html?project_id=' + project_id + '&short_link=' + short_link);

    rs.data = rs.data ? rs.data : {};
    rs.lang = lang;

    $('title').html(titleLang.verification);

    commonNav.init(rs);
    $UI.append(mainTpl(rs)); //主模版
    $UI.append(alertIdBoxTpl(rs)); //验证弹窗
    $UI.append(alertDiagnosticBoxTpl(rs)); //提示关闭项目弹窗

    //fancybox tell
    $('.uploadList .fancybox').fancybox();
    $('.uploadList2 .fancybox').fancybox();
    fastclick.attach(document.body);

    imgUploaderHandle(rs); //图片上传相关
    clickHandle.init(rs); //点击事件

    utils.inputFourInOne('#ID_number'); //限制4位一格
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};

/**
 * 图片上传相关
 */
function imgUploaderHandle(rs) {
    // 从本地缓存中读取图片
    if (rs.data.card_images) {
        imgUploader.setImageList(uploadList, uploadKey, JSON.parse(rs.data.card_images));
        imgUploader.setImageList(uploadList2, uploadKey2, JSON.parse(rs.data.disease_images));
    }

    // 控制图片上传按钮显示或者隐藏 ‘+’
    hideOrShowInputButton.init();

    // initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
    imgUploader.create(uploadBtn2, uploadList2, uploadKey2, uploadNumLimit2, imgChangedHandler2, uploadWrapper2);
}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    // console.log('initiate info img changed1');
    imgUploader.refreshPictureNumber(uploadList);
    $('.fancybox').fancybox();
    //Control image upload button to show or hide
    hideOrShowInputButton.init();
}


function imgChangedHandler2() {
    // console.log('initiate info img changed2');
    imgUploader.refreshPictureNumber(uploadList2);
    $('.fancybox').fancybox();
    //Control image upload button to show or hide
    hideOrShowInputButton.init();
}


export default obj;