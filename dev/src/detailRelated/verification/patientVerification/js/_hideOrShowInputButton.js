let obj = {};

obj.init = function(e) {
    // ID card upload img
    if ($('.uploadList img').length == 1) {
        // console.log('uploadList', $('.uploadList img').length)
        $('#webPicPicker').css('display', 'none');
    } else {
        $('#webPicPicker').css('display', 'block');
    }

    if ($('.uploadList2 img').length >= 8) {
        // console.log('uploadList', $('.uploadList img').length)
        $('#webPicPicker2').css('display', 'none');
    } else {
        $('#webPicPicker2').css('display', 'block');
    }
}

export default obj;