/*
 * 校验表单信息
 */
import utils from 'utils'

let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);
    // console.log('validate', 'lang:', lang);


    if (param.name == '') {
        utils.alertMessage(lang.lang29);
        return false;
    }
    // if (param.name.length > 50) {
    //     utils.alertMessage(lang.lang30);
    //     return false;
    // }
    if (param.id_card == '') {
        utils.alertMessage(lang.lang31);
        return false;
    }
    if (param.id_card.length != 16) {
        utils.alertMessage(lang.lang32);
        return false;
    }
    if (param.disease == '') {
        utils.alertMessage(lang.lang33);
        return false;
    }
    if (param.card_images.length != 1) {
        utils.alertMessage(lang.lang34);
        return false;
    }
    if (param.disease_images.length < 1) {
        utils.alertMessage(lang.lang35);
        return false;
    }
    if (param.disease_images.length > 8) {
        utils.alertMessage(lang.lang36);

        return false;
    }

    //finish upload or not
    for (var i = 0; i < param.card_images.length; i++) {
        if (!param.card_images[i].thumb || !param.card_images[i].image) {

            utils.alertMessage(lang.lang37);
            return false;
        }
    }

    for (var i = 0; i < param.disease_images.length; i++) {
        if (!param.disease_images[i].thumb || !param.disease_images[i].image) {

            utils.alertMessage(lang.lang37);
            return false;
        }
    }

    return true;
}

export default obj;