import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 提交患者验证信息
 */
obj.submit = function(params) {
    var url = domainName.project + '/v1/project_patient';

    if (isLocal) {
        url = '/mock/projects_create.json';
    }

    ajaxProxy.ajax({
        url: url,
        data: JSON.stringify(params.data),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8'
    }, params);
}

/**
 * 再次提交患者验证信息
 */
obj.update = function(params) {
    var url = domainName.project + '/v1/project_patient/' + params.data.project_id;

    if (isLocal) {
        url = '/mock/projects_create.json';
    }

    ajaxProxy.ajax({
        url: url,
        data: JSON.stringify(params.data),
        type: 'PUT',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8'
    }, params);
}

/**
 * 获取患者验证信息
 */
obj.getPationInfo = function(params) {
    var url = domainName.project + '/v1/project_patient/' + params.data.project_id;

    if (isLocal) {
        url = '/mock/project_patient.json';
    }

    ajaxProxy.ajax({
        url: url,
        type: 'get',
    }, params);
}

//获取表单信息
obj.getPayee = function(o) {
    // console.log('project_id', o)
    var url = domainName.project + '/v2/project_payee/' + o.params.project_id;

    if (isLocal) {
        url = '/mock/projects_create.json';
    }

    ajaxProxy.ajax({
        url: url,
        type: 'get',
    }, o);
}

export default obj;