import view from "./view";
import model from "./model";
import "loading";
import "../less/patientVerification.less";
// 引入依赖
import utils from "utils";

let UI = view.UI;
let project_id = utils.getRequestParams().project_id;
let short_link = utils.getRequestParams().short_link;
let from = utils.getRequestParams().from;

let submintAgainFlag = false; //做一个标记，judge是更新验证还是首次验证
let payee_type = 1; //收款人验证表单

// 隐藏loading
utils.hideLoading();
/**
 * {
    "code": 0,
    "msg": "",
    "data": {
        "created_at": "2018-08-15T12:16:19Z",
        "updated_at": "2018-08-15T12:25:43Z",
        "project_id": "14221049989731188844",
        "name": "dddd",
        "id_card": "23233333",
        "card_images": "[{\"name\":\"13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg\",\"image\":\"https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg\",\"thumb\":\"https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg\"}]",
        "disease": "aaaaaaaaa",
        "disease_images": "[{\"name\":\"13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg\",\"image\":\"https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg\",\"thumb\":\"https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg\"}]",
        "state": 1
    },
    "_metadata": {}
}
 */
// 获取患者验证信息
model.getPationInfo({
  data: {
    project_id: project_id
  },
  success: function(rs) {
    if (rs.code == 0) {
      // console.log('rs:', rs);
      if (rs.data != null) {
        submintAgainFlag = true; //做一个标记，judge是更新验证还是首次验证
        view.init(rs);
      } else {
        view.init({});
      }
    } else {
      utils.alertMessage(res.msg);
    }
  },
  error: utils.handleFail
});

/*
 *submit
 */
UI.on("submit", function(e, params) {
  // console.log('submit:', params);
  // judge是更新验证还是首次验证
  if (submintAgainFlag) {
    model.update({
      data: params,
      success: submitSuccessHandler,
      error: utils.handleFail
    });
  } else {
    model.submit({
      data: params,
      success: submitSuccessHandler,
      error: utils.handleFail
    });
  }
});

function submitSuccessHandler(res) {
  if (res.code == 0) {
    jumpToPayeeVertificate(); //跳转到不同的收款人验证表单页
  } else {
    utils.alertMessage(res.msg);
  }
}

/**
 *
 */
model.getPayee({
  params: {
    project_id: project_id
  },
  success: function(rs) {
    if (rs.code == 0) {
      console.log("getPayee rs:", rs);
      if (rs.data &&  rs.data.payees!= null) {
        let payees = rs.data.payees;
        let _data = payees[0];
        payee_type = _data.payee_type;
      }
    } else {
      utils.alertMessage(rs.msg);
    }
  },
  error: utils.handleFail
});

/**
 * 跳转到不同的收款人验证表单页
 * payee_type:
 1 患者验证 patientHimself :1
 2 直系亲属验证 directRelatives: 2
 3 夫妻验证 conjugalRelation: 3
 4 公益机构验证 commonwealOrganization: 4
 5 医院验证 hospital: 5
 */
function jumpToPayeeVertificate() {
  if (payee_type == 1) {
    location.href =
      "/patientHimself.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  } else if (payee_type == 2) {
    location.href =
      "/directRelatives.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  }
  //  else if (payee_type == 3) {
  //     location.href = '/conjugalRelation.html?project_id=' + project_id;
  // }
  else if (payee_type == 4) {
    location.href =
      "/commonwealOrganization.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  } else if (payee_type == 5) {
    location.href =
      "/hospital.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  }
}
