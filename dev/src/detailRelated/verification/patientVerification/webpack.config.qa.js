var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'patientVerification',
    htmlFileURL: 'html/patientVerification.html',
    appDir: 'js/patientVerification',
    uglify: true,
    hash: '',
    mode: 'development'
})