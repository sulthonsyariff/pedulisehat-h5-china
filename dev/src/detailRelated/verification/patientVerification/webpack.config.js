var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'patientVerification',
    htmlFileURL: 'html/patientVerification.html',
    appDir: 'js/patientVerification',
    uglify: true,
    hash: '',
    mode: 'production'
})