var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'directRelatives',
    htmlFileURL: 'html/directRelatives.html',
    appDir: 'js/directRelatives',
    uglify: true,
    hash: '',
    mode: 'production'
})