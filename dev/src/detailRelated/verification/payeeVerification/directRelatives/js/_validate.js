/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);
    // console.log('validate', 'lang:', lang);

    if (param.name == '') {
        utils.alertMessage(lang.lang27);
        return false;
    }
    if (param.id_card == '') {
        utils.alertMessage(lang.lang28);
        return false;
    }
    if (param.id_card.length != 16) {
        utils.alertMessage(lang.lang29);
        return false;
    }
    if (param.phone.length < 7) {
        utils.alertMessage(lang.lang30);
        return false;
    }
    if (param.bank_card == '') {
        utils.alertMessage(lang.lang31);
        return false;
    }
    if (param.bank_agency == lang.lang37) {
        utils.alertMessage(lang.lang36);
        return false;
    }

    if (param.bank_book.length != 1) {
        utils.alertMessage(lang.lang38);

        return false;
    }

    //finish upload or not
    let img = param.bank_book;
    for (let i = 0; i < img.length; i++) {
        if (!img[i].thumb || !img[i].image) {
            utils.alertMessage(lang.lang34);

            return false;
        }
    }

    if (param.images.length < 1) {
        utils.alertMessage(lang.lang32);

        return false;
    }

    if (param.images.length > 8) {
        utils.alertMessage(lang.lang33);

        return false;
    }

    //finish upload or not
    let imgs = param.images;
    for (let i = 0; i < imgs.length; i++) {
        if (!imgs[i].thumb || !imgs[i].image) {
            utils.alertMessage(lang.lang34);

            return false;
        }
    }

    return true;
}

export default obj;