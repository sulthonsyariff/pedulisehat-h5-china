import view from './view'
import model from './model'
import 'loading'
import '../less/directRelatives.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;

// 隐藏loading
utils.hideLoading();

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let from = reqObj['from'];

let submitAgainFlag = false; //标记调用更新还是发布接口

model.getBankList({
    success: function(res) {
        if (res.code == 0) {
            getPayee(res);
        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail,
})

function getPayee(rs) {
    model.getPayee({
        params: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                if (res.data != null) {
                    submitAgainFlag = true;
                    res.data.bankList = rs.data
                    view.init(res);
                } else {
                    res.data = {};
                    res.data.bankList = rs.data
                    view.init(res);
                    // console.log('===res===', res)
                }
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail,
    })
}

/*
 *submit
 */
UI.on('submit', function(e, params) { // send to server directly
    if (submitAgainFlag) {
        model.updatePayee({
            data: params,
            success: submitSuccessHandler,
            error: utils.handleFail
        });
    } else {
        model.submit({
            data: params,
            success: submitSuccessHandler,
            error: utils.handleFail
        });
    }

})

function submitSuccessHandler(res) {
    // console.log('res:', res.data)
    if (res.code == 401) {
        location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href);
        return;
    } else if (res.code == 0) {
        location.href = '/verificationSuccess.html?project_id=' + project_id + '&short_link=' + short_link + '&from=' + from;
    }

}
