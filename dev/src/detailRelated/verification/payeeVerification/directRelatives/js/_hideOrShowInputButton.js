let obj = {};

obj.init = function(e) {
    // bank book upload img
    if ($('.uploadList img').length == 1) {
        // console.log('uploadList', $('.uploadList img').length)
        $('#webPicPicker').css('display', 'none');
    } else {
        $('#webPicPicker').css('display', 'block');
    }

    // ID card upload img
    if ($('.uploadList2 img').length >= 8) {
        $('#webPicPicker2').css('display', 'none');
    } else {
        $('#webPicPicker2').css('display', 'block');
    }
}

export default obj;