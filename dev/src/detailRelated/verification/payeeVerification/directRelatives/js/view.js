// 公共库
import commonNav from "commonNav";
import selectBar from "select-relationship";
import bankList from "bank-list";

import mainTpl from "../tpl/directRelatives.juicer";
import imgUploader from "uploadCloudinaryMore";
import utils from "utils";
import fastclick from "fastclick";
import picCover from "./_picCover";
import "fancybox";
import alertDiagnosticBoxTpl from "../tpl/alert-diagnostic-box.juicer";
import hideOrShowInputButton from "./_hideOrShowInputButton"; //Control image upload button to show or hide
import googleAnalytics from "google.analytics";
//import sensorsActive from "sensorsActive";
import "../less/directRelatives.less";
import jumpToPreOrNextFormPage from "../../js/_jumpToPreOrNextFormPage";

/* translation */
import directRelatives from "directRelatives";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(directRelatives);
/* translation */

import validate from "./_validate"; // validate the form

let [obj, $UI] = [{}, $("body")];
obj.UI = $UI;

let inputFirstFlag = true;
let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];

let payee_type = 2;

/* ID upload bank book */
let uploadWrapper = "#uploadWrapper";
let uploadBtn = "upload-btn"; // upload btn
let uploadList = ".uploadList"; // img container
let uploadKey = "cover"; // upload key
let uploadNumLimit = 1; // limit number
/* ID upload bank book */

/* upload img */
let uploadWrapper2 = "#uploadWrapper2";
let uploadBtn2 = "upload-btn2"; // upload btn
let uploadList2 = ".uploadList2"; // img container
let uploadKey2 = "cover2"; // upload key
let uploadNumLimit2 = 8; // limit number
/* upload img */

obj.init = function(rs) {
  rs.JumpName = titleLang.verification;
  // rs.commonNavGoToWhere = '/patientVerification.html?project_id=' + project_id + '&short_link=' + short_link + '&from=' + from;
  // 首先排除是否正在填写第二份表单
  if (reqObj["form2"]) {
    rs.commonNavGoToWhere = jumpToPreOrNextFormPage.jump(
      rs.pre_form1_payee_type,
      0
    );
  } else {
    rs.commonNavGoToWhere =
      "/patientVerification.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  }

  commonNav.init(rs);
  $("title").html(titleLang.verification);

  selectBar.init(rs);

  rs.data.payment_channel = rs.data.bankList;
  for (let i = 0; i < rs.data.payment_channel.length; i++) {
    rs.data.payment_channel[i].pg_code = rs.data.bankList[i].id;
    rs.data.payment_channel[i].pg_name = rs.data.bankList[i].name;
  }
  bankList.init(rs);

  rs.data = rs.data ? rs.data : {};
  //判断bank_deposit 展示
  rs.data.bank_agency = rs.data.bank_agency ? rs.data.bank_agency : lang.lang37;
  rs.lang = lang;

  $UI.append(mainTpl(rs));
  $UI.append(alertDiagnosticBoxTpl(rs)); //提示关闭项目弹窗

  // if (rs.data.payee_type == payee_type && rs.data.images) {
  //     rs.data.images = JSON.parse(rs.data.images)
  //     if (rs.data && rs.data.images) {
  //         imgUploader.setImageList(uploadList, uploadKey, rs.data.images);
  //         imgUploader.setImageList(uploadList2, uploadKey2, rs.data.images);

  //     }
  // }

  // 读取图片
  if (rs.data && rs.data.payee_type == payee_type) {
    if (rs.data.bank_book) {
      imgUploader.setImageList(
        uploadList,
        uploadKey,
        JSON.parse(rs.data.bank_book)
      );
    }

    if (rs.data.images) {
      imgUploader.setImageList(
        uploadList2,
        uploadKey2,
        JSON.parse(rs.data.images)
      );
    }
  }

  $(".fancybox").fancybox();
  fastclick.attach(document.body);

  utils.inputFourInOne("#ID_number"); //限制4位一格
  setupUIHandler(); //Main Js
  hideOrShowInputButton.init();
  // google anaytics
  let param = {};
  googleAnalytics.sendPageView(param);

  // control the height display correct when download link is closed
  if ($(".app-download").css("display") === "block") {
    $(".page-inner").css("padding-top", "102px");
  }
  $("body").on("click", ".appDownload-close", function(e) {
    // store.set('app_download', 'false')

    $(".app-download").css({
      display: "none"
    });
    $(".page-inner").css("padding-top", "56px");
  });
  // sensorsActive.init();
};

/**
 *Main Js
 */
function setupUIHandler() {
  // initialize img upload
  imgUploader.create(
    uploadBtn,
    uploadList,
    uploadKey,
    uploadNumLimit,
    imgChangedHandler,
    uploadWrapper
  );
  imgUploader.create(
    uploadBtn2,
    uploadList2,
    uploadKey2,
    uploadNumLimit2,
    imgChangedHandler2,
    uploadWrapper2
  );

  //set cover
  picCover.setCover(lang);
  if ($(".bank-name").text() != lang.lang37) {
    $(".bank-name").css({
      color: "#000",
      "font-weight": "bold"
    });
  }
  $(".bank").on("click", function() {
    $(".alert-bank").css("display", "block");
  });

  // submit
  $(".submitBtn").on("click", function() {
    let submitData = getSubmitParam();

    // console.log('getSubmitParam()', submitData);
    if (validate.check(submitData, lang)) {
      $UI.trigger("submit", [submitData]);
      utils.showLoading(lang.lang25);
    }
  });
  // alert box
  $("body").on("click", "#webPicPicker2", function(e) {
    if (inputFirstFlag) {
      inputFirstFlag = !inputFirstFlag;
      $(".alert-box").css({
        display: "block"
      });
      e.preventDefault();
    }
  });
  // alert-box:got it
  $("body").on("click", ".confirm", function(e) {
    $(".alert-box").css({
      display: "none"
    });
    $(".alert-ID-box").css({
      display: "none"
    });
  });
}

/**
 * 获取提交数据
{
     "title": "We Will Beat United Airline",
     "story": "The protection of Giant Panda Bears CANNOT Wait!!!",
     "total_amount": 958300,
     "country": "",
     "city": "yjd",
     "category_id": 12,
     "platform": "android",
     "images": [{
         "name": "13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "image": "https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "thumb": "https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "location": "cloudinary"
     }]
 }
 */
function getSubmitParam() {
  return {
    project_id: project_id,
    payee_type: payee_type,
    name: $("#real_name").val(),
    id_card: $("#ID_number")
      .val()
      .replace(/\s|\xA0/g, ""),
    phone: $("#phoneNum").val(),
    bank_card: $("#cardNum").val(),
    bank_agency: $(".bank-name").text(),
    bank_book: imgUploader.getImageList(uploadList, uploadKey),
    images: imgUploader.getImageList(uploadList2, uploadKey2)
  };
}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
  imgUploader.refreshPictureNumber(uploadList);
  $(".fancybox").fancybox();
  //set cover
  picCover.setCover(lang);
  hideOrShowInputButton.init();
}

function imgChangedHandler2() {
  imgUploader.refreshPictureNumber(uploadList2);
  $(".fancybox").fancybox();
  //set cover
  picCover.setCover(lang);
  hideOrShowInputButton.init();
}

export default obj;
