var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'directRelatives',
    htmlFileURL: 'html/directRelatives.html',
    appDir: 'js/directRelatives',
    uglify: true,
    hash: '',
    mode: 'development'
})