var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'patientHimself',
    htmlFileURL: 'html/patientHimself.html',
    appDir: 'js/patientHimself',
    uglify: true,
    hash: '',
    mode: 'production'
})