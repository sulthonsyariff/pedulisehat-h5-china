var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'patientHimself',
    htmlFileURL: 'html/patientHimself.html',
    appDir: 'js/patientHimself',
    uglify: true,
    hash: '',
    mode: 'development'
})