// 公共库
import commonNav from "commonNav";
import selectBar from "select-relationship";
import bankList from "bank-list";

import mainTpl from "../tpl/patientHimself.juicer";
import imgUploader from "uploadCloudinaryMore";
import utils from "utils";
import fastclick from "fastclick";
import picCover from "./_picCover";
import "fancybox";
import googleAnalytics from "google.analytics";
//import sensorsActive from "sensorsActive";

import hideOrShowInputButton from "./_hideOrShowInputButton"; //Control image upload button to show or hide
import validate from "./_validate"; // validate the form

import "../less/patientHimself.less";
import jumpToPreOrNextFormPage from "../../js/_jumpToPreOrNextFormPage";

/* translation */
import patientHimself from "patientHimself";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(patientHimself);
/* translation */

let [obj, $UI] = [{}, $("body")];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];

/* upload img */
let uploadWrapper = "#uploadWrapper";
let uploadBtn = "upload-btn"; // upload btn
let uploadList = ".uploader-list"; // img container
let uploadKey = "cover"; // upload key
let uploadNumLimit = 1; // limit number
/* upload img */

obj.init = function(rs) {
  rs.JumpName = titleLang.verification;
  // rs.commonNavGoToWhere =
  //   "/patientVerification.html?project_id=" +
  //   project_id +
  //   "&short_link=" +
  //   short_link +
  //   "&from=" +
  //   from;
  // 首先排除是否正在填写第二份表单
  if (reqObj["form2"]) {
    rs.commonNavGoToWhere = jumpToPreOrNextFormPage.jump(
      rs.pre_form1_payee_type,
      0
    );
  } else {
    rs.commonNavGoToWhere =
      "/patientVerification.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  }
  commonNav.init(rs);

  $("title").html(titleLang.verification);

  selectBar.init(rs);

  rs.data.payment_channel = rs.data.bankList;
  for (let i = 0; i < rs.data.payment_channel.length; i++) {
    rs.data.payment_channel[i].pg_code = rs.data.bankList[i].id;
    rs.data.payment_channel[i].pg_name = rs.data.bankList[i].name;
  }
  bankList.init(rs);

  rs.data = rs.data ? rs.data : {};
  //判断bank_deposit 展示
  rs.data.bank_agency = rs.data.bank_agency ? rs.data.bank_agency : lang.lang25;
  rs.lang = lang;

  // console.log('rs',rs.data)
  $UI.append(mainTpl(rs)); // get data from the local cache

  $(".fancybox").fancybox();
  fastclick.attach(document.body);

  imgUploaderHandle(rs); //图片上传相关

  setupUIHandler(); //Main Js

  utils.inputFourInOne("#ID_number"); //限制4位一格

  // google anaytics
  let param = {};
  googleAnalytics.sendPageView(param);

  // control the height display correct when download link is closed
  if ($(".app-download").css("display") === "block") {
    $(".page-inner").css("padding-top", "102px");
  }
  $("body").on("click", ".appDownload-close", function(e) {
    // store.set('app_download', 'false')

    $(".app-download").css({
      display: "none"
    });
    $(".page-inner").css("padding-top", "56px");
  });
  // sensorsActive.init();
};

/**
 * 图片上传相关
 */
function imgUploaderHandle(rs) {
  // 从本地缓存中读取图片
  if (rs.data.bank_book) {
    imgUploader.setImageList(
      uploadList,
      uploadKey,
      JSON.parse(rs.data.bank_book)
    );
  }

  // 控制图片上传按钮显示或者隐藏 ‘+’
  hideOrShowInputButton.init();

  // initialize img upload
  imgUploader.create(
    uploadBtn,
    uploadList,
    uploadKey,
    uploadNumLimit,
    imgChangedHandler,
    uploadWrapper
  );
}

/**
 *Main Js
 */

function setupUIHandler() {
  // initialize img upload
  // imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);

  //set cover
  picCover.setCover(lang);
  if ($(".bank-name").text() != lang.lang25) {
    $(".bank-name").css({
      color: "#000",
      "font-weight": "bold"
    });
  }

  $(".bank").on("click", function() {
    $(".alert-bank").css("display", "block");
    // if($(this).val() == )
  });

  // submit
  $(".submitBtn").on("click", function() {
    let submitData = getSubmitParam();
    if (validate.check(submitData)) {
      // $('.submitBtn').addClass('disabled');
      $UI.trigger("submit", [submitData]);
      utils.showLoading(lang.lang22);
    }
  });
}
// var bank_agency = document.getElementById("bank")
// console.log('bank_agency',bank_agency)

function getSubmitParam() {
  return {
    project_id: project_id,
    payee_type: 1,
    name: $("#real_name").val(),
    id_card: $("#ID_number")
      .val()
      .replace(/\s|\xA0/g, ""),
    phone: $("#phoneNum").val(),
    bank_card: $("#cardNum").val(),
    bank_agency: $(".bank-name").text(),
    bank_book: imgUploader.getImageList(uploadList, uploadKey) //银行卡照片
  };
}

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
  imgUploader.refreshPictureNumber(uploadList);
  $(".fancybox").fancybox();
  //set cover
  picCover.setCover(lang);
  hideOrShowInputButton.init();
}

export default obj;
