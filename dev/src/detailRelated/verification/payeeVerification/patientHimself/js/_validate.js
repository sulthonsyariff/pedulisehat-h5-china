/*
 * 校验表单信息
 */
import utils from 'utils'
/* translation */
import patientHimself from 'patientHimself'
import qscLang from 'qscLang'
let lang = qscLang.init(patientHimself);
/* translation */
let obj = {};

obj.check = function(param) {
    if (param.name == '') {
        utils.alertMessage(lang.lang17);
        return false;
    }
    if (param.id_card == '') {
        utils.alertMessage(lang.lang18);
        return false;
    }
    if (param.id_card.length != 16) {
        utils.alertMessage(lang.lang19);
        return false;
    }
    if (param.phone.length < 7) {
        utils.alertMessage(lang.lang20);
        return false;
    }
    if (param.bank_card == '') {
        utils.alertMessage(lang.lang21);
        return false;
    }
    if (param.bank_agency == lang.lang25) {
        utils.alertMessage(lang.lang24);
        return false;
    }

    if (param.bank_book.length != 1) {
        utils.alertMessage(lang.lang27);
        return false;
    }

    //finish upload or not
    let imgs = param.bank_book;
    for (let i = 0; i < imgs.length; i++) {
        if (!imgs[i].thumb || !imgs[i].image) {
            utils.alertMessage(lang.lang28);

            return false;
        }
    }

    return true;
}

export default obj;