import view from "./view";
import model from "../../js/model";
import "loading";
// 引入依赖
import utils from "utils";
import jumpToPreOrNextFormPage from "../../js/_jumpToPreOrNextFormPage";

let UI = view.UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];
let form2 = reqObj["form2"]; // 第二个表单
let next_form2_payee_type = 1; // 第二个表单页类型：跳转需要

let submitAgainFlag = false; //标记调用更新还是发布接口
let is_hospital_partner = false; //是否医院合作项目
let payee_id; //收款人ID	医院合作项目必传

// 隐藏loading
utils.hideLoading();

model.getBankList({
  success: function(res) {
    if (res.code == 0) {
      getPayee(res); //获取表单信息
    } else {
      utils.alertMessage(res.msg);
    }
  },
  error: utils.handleFail
});

/*
 *submit
 */
UI.on("submit", function(e, params) {
  // send to server directly
  if (submitAgainFlag) {
    params.payee_id = payee_id; //payee_id 收款人ID	医院合作项目必传
    //  更新
    model.updatePayee({
      data: params,
      success: submitSuccessHandler,
      error: utils.handleFail
    });
  } else {
    // 创建
    model.submit({
      data: params,
      success: submitSuccessHandler,
      error: utils.handleFail
    });
  }
});

// 获取表单信息
function getPayee(rs) {
  model.getPayee({
    params: {
      project_id: project_id
    },
    success: function(res) {
      if (res.code == 0) {
        handleData(rs, res);
        view.init(res);
      } else {
        utils.alertMessage(res.msg);
      }
    },
    error: utils.handleFail
  });
}

function handleData(rs, res) {
  // 2020-2-19 后端修改接口，临时处理办法
  res.data = res.data.payees;

  // 有信息
  if (res.data != null) {
    // 前后跳转需要
    if (res.data.length && res.data.length == 2) {
      let _data = res.data[1];
      let _data0 = res.data[0];

      next_form2_payee_type = _data.payee_type; //第二个表单类型
      res.pre_form1_payee_type = _data0.payee_type; // 第一个表单类型
    }

    // 有两个收款人信息，展示第二个收款人信息
    if (res.data.length && res.data.length == 2 && form2) {
      submitAgainFlag = true;
      res.data = res.data[1];
    }
    // 只有一个收款人信息（表示正在填写第二份新表单）
    else if (res.data.length && res.data.length == 1 && form2) {
      res.data = {};
    }
    // 只有一个收款人信息，展示第一个收款人信息
    else {
      submitAgainFlag = true;
      res.data = res.data[0];
    }

    is_hospital_partner = res.data.is_hospital_partner; //是否医院合作项目
    payee_id = res.data.id; //收款人ID
  }
  // 无信息
  else {
    res.data = {};
  }

  res.data.bankList = rs.data;
  res.data.form2 = form2;

  console.log("====res==== ", res);
}

function submitSuccessHandler(res) {
  if (res.code == 401) {
    location.href =
      "/login.html?qf_redirect=" + encodeURIComponent(location.href);
    return;
  } else if (res.code == 0) {
    // 是医院合作项目且仅完成第一个表单，跳转至第二个收款人表单页面
    if (res.data && (res.data.is_create || is_hospital_partner) && !form2) {
      location.href = jumpToPreOrNextFormPage.jump(next_form2_payee_type, 1);
    }
    // 非医院合作项目，跳转
    else {
      location.href =
        "/verificationSuccess.html?project_id=" +
        project_id +
        "&short_link=" +
        short_link +
        "&from=" +
        from;
    }
  }
}
