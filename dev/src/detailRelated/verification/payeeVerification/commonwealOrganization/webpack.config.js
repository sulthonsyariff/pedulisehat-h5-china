var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'commonwealOrganization',
    htmlFileURL: 'html/commonwealOrganization.html',
    appDir: 'js/commonwealOrganization',
    uglify: true,
    hash: '',
    mode: 'production'
})