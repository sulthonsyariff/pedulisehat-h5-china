var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'commonwealOrganization',
    htmlFileURL: 'html/commonwealOrganization.html',
    appDir: 'js/commonwealOrganization',
    uglify: true,
    hash: '',
    mode: 'development'
})