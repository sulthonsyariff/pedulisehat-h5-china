import view from "./view";
import model from "./model-useless";
import "loading";
import "../less/commonwealOrganization.less";
// 引入依赖
import utils from "utils";

let UI = view.UI;

// 隐藏loading
utils.hideLoading();

let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];
let form2 = reqObj["form2"]; // 第二个表单
let next_form2_payee_type = 1; // 第二个表单页类型：跳转需要

let submitAgainFlag = false; //标记调用更新还是发布接口
let is_hospital_partner = false; //是否医院合作项目
let payee_id; //收款人ID	医院合作项目必传

model.getBankList({
  success: function(res) {
    if (res.code == 0) {
      getPayee(res);
    } else {
      utils.alertMessage(res.msg);
    }
  },
  error: utils.handleFail
});

function getPayee(rs) {
  model.getPayee({
    params: {
      project_id: project_id
    },
    success: function(res) {
      if (res.code == 0) {
        // 有信息
        if (res.data != null) {
          console.log("res.data.length = ", res.data.length);

          // next_form2_payee_type 第二个表单页类型：跳转需要
          if (res.data.length && res.data.length == 2) {
            let _data = res.data[1];
            next_form2_payee_type = _data.payee_type;
            console.log("===next_form2_payee_type===", next_form2_payee_type);
          }

          // 有两个收款人信息，展示第二个收款人信息
          if (res.data.length && res.data.length == 2 && form2) {
            submitAgainFlag = true;
            res.data = res.data[1];
          }
          // 只有一个收款人信息（表示正在填写第二份新表单）
          else if (res.data.length && res.data.length == 1 && form2) {
            res.data = {};
          }
          // 只有一个收款人信息，展示第一个收款人信息
          else {
            submitAgainFlag = true;
            res.data = res.data[0];
          }

          is_hospital_partner = res.data.is_hospital_partner; //是否医院合作项目
          payee_id = res.data.id; //收款人ID
        }
        // 无信息
        else {
          res.data = {};
        }

        res.data.bankList = rs.data;
        res.data.form2 = form2;
        view.init(res);

        console.log("view init :", res);
      } else {
        utils.alertMessage(res.msg);
      }
    },
    error: utils.handleFail
  });
}

/*
 *submit
 */
UI.on("submit", function(e, params) {
  // send to server directly
  if (submitAgainFlag) {
    params.payee_id = payee_id; //payee_id 收款人ID	医院合作项目必传
    model.updatePayee({
      data: params,
      success: submitSuccessHandler,
      error: utils.handleFail
    });
  } else {
    model.submit({
      data: params,
      success: submitSuccessHandler,
      error: utils.handleFail
    });
  }
});

function submitSuccessHandler(res) {
  if (res.code == 401) {
    location.href =
      "/login.html?qf_redirect=" + encodeURIComponent(location.href);
    return;
  } else if (res.code == 0) {
    // 是医院合作项目且仅完成第一个表单，跳转至第二个收款人表单页面
    if (res.data && (res.data.is_create || is_hospital_partner) && !form2) {
      jumpToPayeeVertificate(next_form2_payee_type);
    }
    // 非医院合作项目，跳转
    else {
      location.href =
        "/verificationSuccess.html?project_id=" +
        project_id +
        "&short_link=" +
        short_link +
        "&from=" +
        from;
    }
  }
}

/**
 * 跳转到不同的收款人验证表单页
 * payee_type:
 1 患者验证 patientHimself :1
 2 直系亲属验证 directRelatives: 2
 3 夫妻验证 conjugalRelation: 3
 4 公益机构验证 commonwealOrganization: 4
 5 医院验证 hospital: 5
 */
function jumpToPayeeVertificate(payee_type) {
  if (payee_type == 1) {
    location.href =
      "/patientHimself.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      "&form2=true";
  } else if (payee_type == 2) {
    location.href =
      "/directRelatives.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      "&form2=true";
  }
  //  else if (payee_type == 3) {
  //     location.href = '/conjugalRelation.html?project_id=' + project_id;
  // }
  else if (payee_type == 4) {
    location.href =
      "/commonwealOrganization.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      "&form2=true";
  } else if (payee_type == 5) {
    location.href =
      "/hospital.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      "&form2=true";
  }
}
