var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'hospital',
    htmlFileURL: 'html/hospital.html',
    appDir: 'js/hospital',
    uglify: true,
    hash: '',
    mode: 'development'
})