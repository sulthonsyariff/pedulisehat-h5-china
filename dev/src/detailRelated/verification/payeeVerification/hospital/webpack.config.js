var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'hospital',
    htmlFileURL: 'html/hospital.html',
    appDir: 'js/hospital',
    uglify: true,
    hash: '',
    mode: 'production'
})