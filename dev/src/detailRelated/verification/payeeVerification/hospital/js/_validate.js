/* translation */
import hospital from 'hospital'
import qscLang from 'qscLang'
let lang = qscLang.init(hospital);
/* translation */
/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param) {
    // console.log('validate', 'param:', param);
    if (param.name == '') {
        utils.alertMessage(lang.lang20);
        return false;
    }
    if (param.hospital_name == '') {
        utils.alertMessage(lang.lang21);
        return false;
    }
    if (param.hospital_office == '') {
        utils.alertMessage(lang.lang22);
        return false;
    }
    if (param.hospital_account == '') {
        utils.alertMessage(lang.lang23);
        return false;
    }
    if (param.bank_card == '') {
        utils.alertMessage(lang.lang24);
        return false;
    }
    if (param.bank_agency == lang.lang28) {
        utils.alertMessage(lang.lang27);
        return false;
    }
    return true;
}

export default obj;