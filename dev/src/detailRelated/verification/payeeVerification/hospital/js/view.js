// 公共库
import commonNav from "commonNav";
import selectBar from "select-relationship";
import bankList from "bank-list";

import mainTpl from "../tpl/hospital.juicer";
import utils from "utils";
import fastclick from "fastclick";
import "fancybox";
import validate from "./_validate"; // validate the form
import googleAnalytics from "google.analytics";
//import sensorsActive from "sensorsActive";

import "../less/hospital.less";
import jumpToPreOrNextFormPage from "../../js/_jumpToPreOrNextFormPage";

/* translation */
import hospital from "hospital";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(hospital);
/* translation */

let [obj, $UI] = [{}, $("body")];
obj.UI = $UI;
let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];

/* upload img */
// let uploadBtn = 'upload-btn'; // upload btn
// let uploadList = '.uploader-list'; // img container
// let uploadKey = 'cover'; // upload key
// let uploadNumLimit = 8; // limit number
/* upload img */

obj.init = function(rs) {
  rs.JumpName = titleLang.verification;
  // rs.commonNavGoToWhere = '/patientVerification.html?project_id=' + project_id + '&short_link=' + short_link + '&from=' + from;
  // 首先排除是否正在填写第二份表单
  if (reqObj["form2"]) {
    rs.commonNavGoToWhere = jumpToPreOrNextFormPage.jump(
      rs.pre_form1_payee_type,
      0
    );
  } else {
    rs.commonNavGoToWhere =
      "/patientVerification.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from;
  }

  commonNav.init(rs);
  $("title").html(titleLang.verification);

  selectBar.init(rs);

  rs.data.payment_channel = rs.data.bankList;
  for (let i = 0; i < rs.data.payment_channel.length; i++) {
    rs.data.payment_channel[i].pg_code = rs.data.bankList[i].id;
    rs.data.payment_channel[i].pg_name = rs.data.bankList[i].name;
  }

  bankList.init(rs);
  rs.data = rs.data ? rs.data : {};
  //判断bank_deposit 展示
  rs.data.bank_agency = rs.data.bank_agency ? rs.data.bank_agency : lang.lang28;
  rs.lang = lang;

  $UI.append(mainTpl(rs)); // get data from the local cache

  $(".fancybox").fancybox();
  fastclick.attach(document.body);

  setupUIHandler(); //Main Js
  // google anaytics
  let param = {};
  googleAnalytics.sendPageView(param);

  // control the height display correct when download link is closed
  if ($(".app-download").css("display") === "block") {
    $(".page-inner").css("padding-top", "102px");
  }
  $("body").on("click", ".appDownload-close", function(e) {
    // store.set('app_download', 'false')

    $(".app-download").css({
      display: "none"
    });
    $(".page-inner").css("padding-top", "56px");
  });
  // sensorsActive.init();
};

/**
 *Main Js
 */

function setupUIHandler() {
  if ($(".bank-name").text() != lang.lang28) {
    $(".bank-name").css({
      color: "#000",
      "font-weight": "bold"
    });
  }
  $(".bank").on("click", function() {
    $(".alert-bank").css("display", "block");
  });
  // submit
  $(".submitBtn").on("click", function() {
    let submitData = getSubmitParam();
    if (validate.check(submitData)) {
      $UI.trigger("submit", [submitData]);
      utils.showLoading(lang.lang19);
    }
  });
}

/**
 * 获取提交数据
{
     "title": "We Will Beat United Airline",
     "story": "The protection of Giant Panda Bears CANNOT Wait!!!",
     "total_amount": 958300,
     "country": "",
     "city": "yjd",
     "category_id": 12,
     "platform": "android",
     "images": [{
         "name": "13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "image": "https://res.cloudinary.com/dqgl4hkkx/image/upload/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "thumb": "https://res.cloudinary.com/dqgl4hkkx/image/upload/w_340,h_340,c_fill,f_auto/13a4c21b-2d1e-4948-8e7c-fcc6ff2972f6.jpg",
         "location": "cloudinary"
     }]
 }
 */
function getSubmitParam() {
  return {
    name: $("#patient_name").val(),
    project_id: project_id,
    payee_type: 5,
    hospital_name: $("#hospita_name").val(),
    hospital_office: $("#hospital_department").val(),
    hospital_account: $("#account_name").val(),
    bank_card: $("#cardNum").val(),
    bank_agency: $(".bank-name").text()
  };
}

/**
 * img changed 监听事件
 */
// function imgChangedHandler() {
//     console.log('initiate info img changed');
//     imgUploader.refreshPictureNumber(uploadList);
//     updateCache();
//     $('.fancybox').fancybox();
//     //set cover
//     picCover.setCover();
//     hideOrShowInputButton.init();

// }

export default obj;
