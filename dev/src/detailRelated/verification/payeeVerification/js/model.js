import ajaxProxy from "ajaxProxy";
import "jq_cookie"; //ajax cookie
import domainName from "domainName"; // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

//提交
obj.submit = function(params) {
  let url = domainName.project + "/v2/project_payee";

  if (isLocal) {
    url = "/mock/v2_project_payee_post.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      data: JSON.stringify(params.data),
      type: "POST",
      dataType: "json",
      contentType: "application/json;charset=utf-8"
    },
    params
  );
};
//更新
obj.updatePayee = function(params) {
  let url = domainName.project + "/v2/project_payee/" + params.data.project_id;

  if (isLocal) {
    url = "/mock/projects_create.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      data: JSON.stringify(params.data),
      type: "PUT",
      dataType: "json",
      contentType: "application/json;charset=utf-8"
    },
    params
  );
};
//获取表单信息
obj.getPayee = function(o) {
  var url = domainName.project + "/v2/project_payee/" + o.params.project_id;

  if (isLocal ) {
    url = "/mock/v2_project_payee_get.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      type: "get"
    },
    o
  );
};
//get bank list
obj.getBankList = function(o) {
  // console.log('project_id',o)
  var url = domainName.project + "/v1/bank";

  if (isLocal) {
    url = "/mock/v1_bank.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      type: "get"
    },
    o
  );
};

export default obj;
