/**
 * 跳转到不同的收款人验证表单页
 * payee_type:
 1 患者验证 patientHimself :1
 2 直系亲属验证 directRelatives: 2
 3 夫妻验证 conjugalRelation: 3
 4 公益机构验证 commonwealOrganization: 4
 5 医院验证 hospital: 5
 */
import utils from "utils";

let obj = {};

let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];

obj.jump = function(payee_type, preOrNext) {
  if (payee_type == 1) {
    return (
      "/patientHimself.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      (preOrNext ? "&form2=true" : "")
    );
  } else if (payee_type == 2) {
    return (
      "/directRelatives.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      (preOrNext ? "&form2=true" : "")
    );
  }
  //  else if (payee_type == 3) {
  //     return '/conjugalRelation.html?project_id=' + project_id;
  // }
  else if (payee_type == 4) {
    return (
      "/commonwealOrganization.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      (preOrNext ? "&form2=true" : "")
    );
  } else if (payee_type == 5) {
    return (
      "/hospital.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=" +
      from +
      (preOrNext ? "&form2=true" : "")
    );
  }
};

export default obj;
