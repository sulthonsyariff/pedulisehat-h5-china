var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'conjugalRelation',
    htmlFileURL: 'html/conjugalRelation.html',
    appDir: 'js/conjugalRelation',
    uglify: true,
    hash: '',
    mode: 'production'
})