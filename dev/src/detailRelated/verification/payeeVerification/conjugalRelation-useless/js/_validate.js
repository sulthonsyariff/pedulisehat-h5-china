/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    if (param.name == '') {
        utils.alertMessage(lang.lang27);
        return false;
    }
    if (param.id_card == '') {
        utils.alertMessage(lang.lang28);
        return false;
    }
    if (param.id_card.length != 16) {
        utils.alertMessage(lang.lang29);
        return false;
    }
    if (param.phone.length < 7) {
        utils.alertMessage(lang.lang30);
        return false;
    }
    if (param.bank_card == '') {
        utils.alertMessage(lang.lang31);
        return false;
    }
    if (param.bank_agency == lang.lang36) {
        utils.alertMessage(lang.lang35);
        return false;
    }

    if (param.images.length != 1) {
        utils.alertMessage(lang.lang32);

        return false;
    }


    //finish upload or not
    var imgs = param.images;
    for (var i = 0; i < imgs.length; i++) {
        if (!imgs[i].thumb || !imgs[i].image) {
            utils.alertMessage(lang.lang33);

            return false;
        }
    }

    return true;
}

export default obj;