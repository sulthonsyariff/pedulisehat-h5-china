// 公共库
import commonNav from 'commonNav'
import selectBar from 'select-relationship'
import bankList from 'bank-list'

import mainTpl from '../tpl/conjugalRelation.juicer'
import imgUploader from 'uploadCloudinaryMore'
import utils from 'utils'
import fastclick from 'fastclick'
import picCover from './_picCover'
import 'fancybox'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

import alertDiagnosticBoxTpl from '../tpl/alert-diagnostic-box.juicer'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import validate from './_validate' // validate the form

/* translation */
import conjugalRelation from 'conjugalRelation'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(conjugalRelation);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;


let inputFirstFlag = true;
var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];
var short_link = reqObj['short_link'];

let payee_type = 3;

/* upload img */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploadList'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 1 // limit number
    /* upload img */

obj.init = function(rs) {
    // console.log('view:', rs);
    rs.JumpName = titleLang.verification;
    rs.commonNavGoToWhere = '/patientVerification.html?project_id=' + project_id + '&short_link=' + short_link;
    commonNav.init(rs);
    $('title').html(titleLang.verification);

    selectBar.init(rs);

    rs.data.payment_channel = rs.data.bankList
    for (let i = 0; i < rs.data.payment_channel.length; i++) {
        rs.data.payment_channel[i].pg_code = rs.data.bankList[i].id;
        rs.data.payment_channel[i].pg_name = rs.data.bankList[i].name;
    }
    bankList.init(rs);

    rs.data = rs.data ? rs.data : {};
    //判断bank_deposit 展示
    rs.data.bank_agency = rs.data.bank_agency ? rs.data.bank_agency : lang.lang36;
    rs.lang = lang;

    $UI.append(mainTpl(rs)); // get data from the local cache
    $UI.append(alertDiagnosticBoxTpl(rs)); //提示关闭项目弹窗

    if (rs.data.payee_type == payee_type && rs.data.images) {
        rs.data.images = JSON.parse(rs.data.images)
        if (rs.data && rs.data.images) {
            imgUploader.setImageList(uploadList, uploadKey, rs.data.images);
        }
    }

    $('.fancybox').fancybox();
    fastclick.attach(document.body);
    setupUIHandler(); //Main Js
    utils.inputFourInOne('#ID_number'); //限制4位一格
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};

/**
 *Main Js
 */

function setupUIHandler() {
    // initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
    //set cover
    picCover.setCover(lang);

    hideOrShowInputButton.init(); //Control image upload button to show or hide
    if ($('.bank-name').text() != lang.lang36) {
        $('.bank-name').css({
            "color": "#000",
            "font-weight": "bold"
        })
    }
    $('.bank').on('click', function() {
            $('.alert-bank').css('display', 'block')
        })
        // submit
    $('.submitBtn').on('click', function() {
        let submitData = getSubmitParam();

        // console.log('getSubmitParam()');
        if (validate.check(submitData, lang)) {
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang25);
        }
    });
    // alert box
    $('body').on('click', '.webPicPicker', function(e) {
            if (inputFirstFlag) {
                inputFirstFlag = !inputFirstFlag;
                $('.alert-box').css({
                    'display': 'block'
                });
                e.preventDefault();
            }
        })
        // alert-box:got it
    $('body').on('click', '.confirm', function(e) {
        $('.alert-box').css({
            'display': 'none'
        });
    });
}

function getSubmitParam() {
    return {
        project_id: project_id,
        payee_type: payee_type,
        name: $('#real_name').val(),
        id_card: $('#ID_number').val().replace(/\s|\xA0/g, ""), //去除所有空格
        phone: $('#phoneNum').val(),
        bank_card: $('#cardNum').val(),
        bank_agency: $(".bank-name").text(),
        images: imgUploader.getImageList(uploadList, uploadKey)
    }
}


/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    imgUploader.refreshPictureNumber(uploadList);
    $('.fancybox').fancybox();
    //set cover
    picCover.setCover(lang);
    hideOrShowInputButton.init(); //Control image upload button to show or hide

}


export default obj;