let obj = {};

obj.init = function(e) {
    // ID card upload img
    if ($('.uploadList img').length == 1 || $('.uploadList img').length >= 8) {
        $('#webPicPicker').css('display', 'none');
    } else {
        $('#webPicPicker').css('display', 'block');
    }

}

export default obj;