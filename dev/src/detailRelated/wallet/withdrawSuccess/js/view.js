import mainTpl from '../tpl/withdrawSuccess.juicer'
// import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import withdrawSuccess from 'withdrawSuccess'
import qscLang from 'qscLang'
let lang = qscLang.init(withdrawSuccess);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];
var short_link = reqObj['short_link'];

obj.init = function(rs) {
    // commonNav.init(rs);
    rs.lang = lang
    rs.domainName = domainName;
    $UI.append(mainTpl(rs));
    commonFooter.init(rs);

    fastclick.attach(document.body);

    $('.viewBtn').on('click', function(e) {
        location.href = '/myWallet.html?project_id=' + project_id + '&short_link=' + short_link;
    });

    // console.log('view:', rs);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};




export default obj;