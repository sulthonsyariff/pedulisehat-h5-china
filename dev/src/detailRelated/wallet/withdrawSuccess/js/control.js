import view from './view'
import model from './model'
import 'loading'
import '../less/withdrawSuccess.less'
// 引入依赖
import utils from 'utils'
// import $ from 'jquery'

// 隐藏loading
utils.hideLoading();
view.init({});