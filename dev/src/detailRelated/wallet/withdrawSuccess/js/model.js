import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

// console.log(ajaxProxy);

obj.judgeLogin = function(o) {
    var url = '/user/setting/info';

    if (isLocal) {
        url = '../mock/userInfo.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;