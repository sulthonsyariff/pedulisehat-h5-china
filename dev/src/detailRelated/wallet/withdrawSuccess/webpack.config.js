var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'withdrawSuccess',
    htmlFileURL: 'html/withdrawSuccess.html',
    appDir: 'js/withdrawSuccess',
    uglify: true,
    hash: '',
    mode: 'production'
})