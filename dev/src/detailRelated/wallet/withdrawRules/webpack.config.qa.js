var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'withdrawRules',
    htmlFileURL: 'html/withdrawRules.html',
    appDir: 'js/withdrawRules',
    uglify: true,
    hash: '',
    mode: 'development'
})