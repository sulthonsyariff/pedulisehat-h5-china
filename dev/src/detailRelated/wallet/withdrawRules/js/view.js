// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/withdraw.juicer'
import fastclick from 'fastclick'
import utils from 'utils'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import withdraw from 'withdraw'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(withdraw);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    res.JumpName = titleLang.withdrawRules;
    res.lang = lang;
    $('title').html(titleLang.withdrawRules);

    commonNav.init(res);

    console.log(res)
  
    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};




export default obj;