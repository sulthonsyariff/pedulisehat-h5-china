import view from './view'
import model from './model'
import 'loading'
import '../less/withdrawRules.less'
import store from 'store'

// 引入依赖
import utils from 'utils'

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];
var short_link = reqObj['short_link'];

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;


// 隐藏loading
utils.hideLoading();


view.init({});
