var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'withdrawRules',
    htmlFileURL: 'html/withdrawRules.html',
    appDir: 'js/withdrawRules',
    uglify: true,
    hash: '',
    mode: 'production'
})