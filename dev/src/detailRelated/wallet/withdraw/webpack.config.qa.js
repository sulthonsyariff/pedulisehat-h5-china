var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'withdraw',
    htmlFileURL: 'html/withdraw.html',
    appDir: 'js/withdraw',
    uglify: true,
    hash: '',
    mode: 'development'
})