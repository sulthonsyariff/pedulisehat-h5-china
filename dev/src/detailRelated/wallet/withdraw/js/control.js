import view from "./view";
import model from "./model";
import "loading";
import "../less/withdraw.less";
import store from "store";

// 引入依赖
import utils from "utils";

var reqObj = utils.getRequestParams();
var project_id = reqObj["project_id"];
var short_link = reqObj["short_link"];

let [obj, $UI] = [{}, $("body")];
obj.UI = $UI;

// 隐藏loading
utils.hideLoading();

model.getWalletInfo({
  param: {
    project_id: project_id
  },
  success: function(res) {
    if (res.code == 0) {
      // view.init(res);

      getPayeeInfo(res);
    } else {
      utils.alertMessage(res.msg);
    }
  },
  error: utils.handleFail
});

function getPayeeInfo(rs) {
  model.getPayeeInfo({
    param: {
      project_id: project_id
    },
    success: function(res) {
      if (res.code == 0) {
        console.log("res:", res);

        if (res.data) {
          res.walletInfo = rs.data;
          view.init(res);
        } else {
          utils.alertMessage("Please complete the payee information");
        }
      } else {
        utils.alertMessage(res.msg);
      }
    },
    error: utils.handleFail
  });
}
$UI.on("submit", function(e, params) {
  model.postData({
    param: {
      project_id: params.project_id,
      money: parseFloat(params.money),
      reason: params.reason,
      // payee_id: JSON.stringify(params.payee_id),
      payee_id: params.payee_id
    },
    success: function(res) {
      if (res.code == 0) {
        // console.log('======', res)
        location.href =
          "/withdrawSuccess.html?project_id=" +
          project_id +
          "&short_link=" +
          short_link;
      } else {
        utils.alertMessage(res.msg);
      }
    },
    error: utils.handleFail
  });
});
