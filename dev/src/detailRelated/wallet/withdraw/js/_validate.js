/*
 * 校验表单信息
 */
import utils from 'utils'
import 'jq_cookie' //ajax cookie

let reqObj = utils.getRequestParams();
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let obj = {};

obj.check = function(param, lang) {
    console.log('validate', 'param:', param);

    if(param.stopped == 'true'){
        if(param.money < 20000){
            
        }
    } else if(param.stopped == 'false') {
        if ((param.chooseMoneyFlag == 1 )){
            if(param.money < 500000){
                $('.tips').html(lang.lang7).css('color', '#FF6100')
                utils.alertMessage(lang.lang24);

                return false;
    
            } else {
                $('.tips').css('color', '#999')
    
            }
            $('.target_amount_tips').css('color', '#FF6100')
            // return false;
    
        } 
        else if ((param.chooseMoneyFlag == 2))  {
    
            if(param.money < 500000){
                console.log('money<500000',param.money)
                $('.tips').html(lang.lang7).css('color', '#FF6100')
                utils.alertMessage(lang.lang24);

                return false;
    
            } else if (param.money > param.net_balance){
                console.log('money<max',param.money)
                $('.tips').html(lang.lang8 + ' ' + param.newNetBalance).css('color', '#FF6100')
                utils.alertMessage(lang.lang24);

                return false;
    
            } else {
                console.log('money',param.money)
                $('.tips').css('color', '#999')
            }
            // return false;
        } 
    }
    



    if (param.payee_id == '') {
        utils.alertMessage(lang.lang19);
        return false;
    }

    if (param.reason == '') {
        utils.alertMessage(lang.lang23);
        return false;
    }

    


    

    return true;
}

export default obj;