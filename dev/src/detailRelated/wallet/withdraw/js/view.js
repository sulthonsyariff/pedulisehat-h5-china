// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/withdraw.juicer'
import fastclick from 'fastclick'
import utils from 'utils'
import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'
// import sensorsActive from 'sensorsActive'
import validate from './_validate'

/* translation */
import withdraw from 'withdraw'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(withdraw);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];
var stopped = reqObj['stopped'];
let payee_id = '';
let chooseMoneyFlag = 2;
let net_balance;
console.log('stopped', stopped)
// 初始化
obj.init = function (res) {
    res.JumpName = titleLang.withdraw;
    res.lang = lang;
    // $('title').html(titleLang.withdraw);

    commonNav.init(res);

    // let testMoney1 = 90000.1
    // let testMoney2 = 90000.11
    // let testMoney3 = 90000
    // console.log('testMoney1',changeMoneyFormat.newMoneyFormat(testMoney1))
    // console.log('testMoney2',changeMoneyFormat.newMoneyFormat(testMoney2))
    // console.log('testMoney3',changeMoneyFormat.newMoneyFormat(testMoney3))

    res.data = res.data.payees
    // payee_id = res.data[0].id;
    payee_id = JSON.stringify(res.data[0].id);

    net_balance = res.walletInfo.net_balance;
    newNetBalance = changeMoneyFormat.newMoneyFormat(res.walletInfo.net_balance)
    res.walletInfo.newNetBalance = newNetBalance;
    // res.data.transfer_price = changeMoneyFormat.newMoneyFormat(res.data.transfer_price);
    // res.data.total_price = changeMoneyFormat.newMoneyFormat(res.data.total_price);
    // res.data.price = changeMoneyFormat.newMoneyFormat(res.data.price);
    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);


    init(res);
    touchUp();
    submitHandle(res);

    changeMoneyFormat.init('#total_amount');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};

function touchUp() {
    var startY = 0;
    // let that = this;
    document.addEventListener("touchstart", function (e) {
        startY = e.changedTouches[0].pageY;
    }, false);
    document.addEventListener("touchmove", function (e) {
        var endY = e.changedTouches[0].pageY;
        var changeVal = endY - startY;
        if (endY < startY) { //向上滑
            console.log("向上滑");
          
                tipsCtrl();
            
            // $(".input-amount").blur();
        } else if (endY > startY) { //向下滑
            console.log("向下滑");
            tipsCtrl();

            // $(".input-amount").blur();
        } else {}
        // 获取屏幕高度
        // var a = document.body.scrollTop || document.documentElement.scrollTop;; //滚动条的高度
        // var b = document.documentElement.clientHeight //可视区的高度
        // var c = $('#listIndex').height(); //文档的总高度
        // if (a + b >= c) {
        //     //表示浏览器已经到达最底部
        //     that.showSearchFlag = true
        // }

    }, false);
}

function tipsCtrl(){

    if ((chooseMoneyFlag == 1)) {
        // if (net_balance < 500000) {
        //     $('.tips').html(lang.lang7).css('color', '#FF6100')
        // } else {
            $('.tips').css('color', '#999')
        // }
        // $('.target_amount_tips').css('color', '#FF6100')
        // return false;
    
    } else if ((chooseMoneyFlag == 2)) {
    
        if ($('#total_amount').val().replace(/\./g, '') < 500000) {
            $('.tips').html(lang.lang7).css('color', '#FF6100')
            // return false;
    
        } else if ($('#total_amount').val().replace(/\./g, '') > net_balance) {
            $('.tips').html(lang.lang8 + ' ' + newNetBalance).css('color', '#FF6100')
            // return false;
    
        } else {
            $('.tips').css('color', '#999')
        }
        // return false;
    }
}

function init(rs) {


    if (stopped == 'true') {
        chooseMoneyFlag = 1
        $('.input-amount').css('display', 'none')
        $('.tips-withdrawAll-wrap').css('display', 'none')
        $('.maxmium-amount').attr('disabled', true);
        $('.maxmium-amount').css('display', 'inline-block')
        // console.log('stopped1')
    } else if (stopped == 'false') {
        // $('.maxmium-amount').attr('disabled', false);
        // console.log('stopped2')
        $('body').on('click', '.maxmium-amount', function () {
            $('.input-amount').css('display', 'inline-block').show().focus().val('');
            $('.maxmium-amount').css('display', 'none')
            chooseMoneyFlag = 2
        })
    }


    $(document).on('input propertychange', '#total_amount', function (e) {
        chooseMoneyFlag = 2
        // console.log('chooseMoneyFlag3', chooseMoneyFlag, $('#total_amount').val())
    })

    // var scrollH = $(document).scrollTop()
    // console.log('$(document).scrollTop()',scrollH)
    // if ($(document).scrollTop() > 0) {

    // }

    // $('.page-wrapper').scroll(function(){
    //     console.log('page scroll')
    //     $(".input-amount").blur()
    // })

    $(".input-amount").blur(function () {
        console.log('input blur')
        if ($('#total_amount').val().replace(/\./g, '') < 500000) {
            $('.tips').html(lang.lang7).css('color', '#FF6100')
        } else if ($('#total_amount').val().replace(/\./g, '') > net_balance) {
            $('.tips').html(lang.lang8 + ' ' + newNetBalance).css('color', '#FF6100')
        } else {
            $('.tips').css('color', '#999')

        }

    })
    // $('body').on('click', '.confirmBtn', function() {
    //     $UI.trigger('submit');
    // })

    $('body').on('click', '.withdraw-all', function () {
        $('.input-amount').css('display', 'none')
        $('.maxmium-amount').css('display', 'inline-block')
        if (net_balance < 500000) {
            $('.tips').html(lang.lang7).css('color', '#FF6100')
        } else {
            $('.tips').css('color', '#999')
        }
        chooseMoneyFlag = 1
    })



    $('body').on('click', '.bank-card', function () {
        $(this).find('.unchoosed').toggleClass('choosed');
        $(this).siblings().find('.card-left').removeClass('choosed');

        if ($(this).find('.card-left').hasClass('choosed')) {
            payee_id = $(this).attr('data-payeeId')
        } else {
            payee_id = ''
        }
    })
}



// 提交点击事件
function submitHandle() {
    // console.log('clickEvents');
    // 监听输入框内容， 获取输入框字数
    $(document).on('input propertychange', '.withdrawPurpose', function (e) {
        getTextareaParams();
    })

    $('body').on('click', '.submit', function () {
        var submitData = getSubmitData();
        submitData.chooseMoneyFlag = chooseMoneyFlag;
        submitData.net_balance = net_balance;
        submitData.newNetBalance = newNetBalance;
        submitData.stopped = stopped;
        // console.log('submitData', submitData)
        // utils.alertMessage(lang.lang9);
        console.log('submitData', getSubmitData())

        if (validate.check(submitData, lang)) {
            // $('.submitBtn').addClass('disabled');
            console.log('validateSuccess')
            $UI.trigger('submit', [submitData]);
            // location.href = '/withdraw.html?project_id=' + project_id + '&short_link=' + short_link;
        }

    });
}


// 输入框字数提示
function getTextareaParams() {
    var num;
    num = $('.withdrawPurpose').val().length;
    // console.log('num-125', num)
    if (num <= 500) {
        num = num;
    } else {
        $(num).substr(0, 500)
        utils.alertMessage(lang.lang11);
    }
    $(".contentcount").html(num)
}

// 获取提交数据
function getSubmitData() {
    let money;

    if (chooseMoneyFlag == 1) {
        money = net_balance
    } else if (chooseMoneyFlag == 2) {
        money = $('#total_amount').val().replace(/\./g, '')
    }
    return {
        project_id: project_id,
        money: money,
        reason: $('.withdrawPurpose').val(),
        payee_id: payee_id,
        // payee_id: JSON.stringify(payee_id),

    }
}


export default obj;