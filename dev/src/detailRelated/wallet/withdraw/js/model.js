import ajaxProxy from "ajaxProxy";
import "jq_cookie"; //ajax cookie
import domainName from "domainName"; // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * get withdraw info
 */
obj.getWalletInfo = function(o) {
  var url = domainName.project + "/v2/wallet?project_id=" + o.param.project_id;
  if (isLocal) {
    url = "/mock/withdrawInfo.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      type: "GET",
      dataType: "json"
    },
    o
  );
};

obj.getPayeeInfo = function(o) {
  var url = domainName.project + "/v2/project_payee/" + o.param.project_id;
  if (isLocal) {
    url = "../mock/v2_project_payee_get_null.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      type: "GET",
      dataType: "json"
    },
    o
  );
};

obj.postData = function(o) {
  console.log("o", o);
  var url =
    domainName.project + "/v1/withdraw?project_id=" + o.param.project_id;
  if (isLocal) {
    url = "/mock/withdrawInfo.json";
  }
  ajaxProxy.ajax(
    {
      url: url,
      type: "post",
      data: JSON.stringify(o.param)
    },
    o
  );
};

export default obj;
