var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'withdraw',
    htmlFileURL: 'html/withdraw.html',
    appDir: 'js/withdraw',
    uglify: true,
    hash: '',
    mode: 'production'
})