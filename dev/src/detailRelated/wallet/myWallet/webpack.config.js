var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'page',
    htmlFileURL: 'html/myWallet.html',
    appDir: 'js/myWallet',
    uglify: true,
    hash: '',
    mode: 'production'
})