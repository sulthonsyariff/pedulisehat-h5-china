import utils from "utils";

/* translation */
import myWallet from "myWallet";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(myWallet);
/* translation */

let [obj, $UI] = [{}, $("body")];

obj.init = function(res) {
  $(".count_down").css("display", "block"); //显示倒计时
  $(".withdraw_text").css("display", "none");

  time = res.data.withdrawButton.remain_wait;

  let $d = $(".day");
  let $h = $(".hour");
  let $m = $(".min");
  let $s = $(".sec");

  let timer = setInterval(function() {
    let d = parseInt(time / 86400);
    let h = parseInt(time / 3600) % 24;
    let m = parseInt(time / 60) % 60;
    let s = time % 60;

    if (d > 1) {
      d = d + " " + lang.lang21;
    } else {
      d = d + " " + lang.lang20;
    }

    $d.html(d);
    $h.html(addZero(h));
    $m.html(addZero(m));
    $s.html(addZero(s));

    function addZero(n) {
      return n >= 10 ? n : "0" + n;
    }

    if (time) {
      time--;
    } else {
      clearInterval(timer);

      console.log("倒计时结束，重新刷新页面······");

      location.reload();

      // console.log('倒计时结束，withdraw按钮正常');

      // $(".count_down").css("display", "none");//隐藏倒计时
      // $(".withdraw_text").css("display", "block");// withdraw按钮正常
      // $(".withdraw").removeClass("no-active");

      // $("body").on("click", ".withdraw", function(e) {
      //   $UI.trigger("jumpToWithDrawPage");
      // });
    }
  }, 1000);
};

export default obj;
