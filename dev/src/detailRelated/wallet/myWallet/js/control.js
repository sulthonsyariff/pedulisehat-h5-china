import view from "./view";
import model from "./model";
import "loading";
import "../less/main.less";
// 引入依赖
import utils from "utils";

let $UI = view.UI;
let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];

/*
Funded Amount: 项目已筹金额
Net Balance: 净余额 （项目钱包里所剩净余额）

Platform Service Fee: 总平台费
Payment Channel Fee: 总渠道费
Net Amount: 总净已筹金额 （=项目已筹金额- 总平台费-总渠道费）
*/

// 隐藏loading
utils.hideLoading();

model.getWalletInfo({
  param: {
    project_id: project_id
  },
  success: function(res) {
    if (res.code == 0) {
      console.log("walletInfo", res);

      projectVerifyStatus(res);
    } else {
      utils.alertMessage(res.msg);
    }
  }
});

/**
 *
stopped           :yes	bool	项目是否结束
verify_state      :yes	int	    项目验证状态 通过验证是3
is_bright	      :yes	bool	按钮是否亮
remain_wait	      :yes	int	    等待时间 秒为单位 （注意 当为-1的时候特殊处理，说明压根不需要倒计时，比如提现正在处理，根本没法计算等待时间）
cd_end_time	      :yes	int	    等待截止时间 时间戳
count_down_hint	  :yes	string	按钮下红字
stop_limit_money  :yes	float	项目结束提现所需最小金额
tmp_limit_money	  :yes	float	随时提现项目最小余额

1.金额为0 不需要这个接口数据，根据钱包接口的net_balance=0就行
2.未验证通过，verify_state !=3
3.项目进行中7天内再次提现  stopped:false ,is_bright:false, remain_wait:500,count_down_hint:"按钮下面的文案" 
4.项目未结束余额不足提现，前端根据  stop_limit_money和 tmp_limit_money  自己处理弹窗
5.项目进行中7天后再次提现 正常流程
6.项目结束不满24小时  stopped:true ,is_bright:false, remain_wait:500,count_down_hint:"按钮下面的文案" 
7.项目结束提现金额不足  前端根据  stop_limit_money和 tmp_limit_money  自己处理弹窗
8.项目结束后24小时提现 正常流程
9.提现处理中 is_bright:false, remain_wait:-1,count_down_hint:"按钮下面的文案" （-1代表直接不用倒计时 因为不确定处理时间）
 *  
 */
function projectVerifyStatus(rs) {
  model.projectVerifyStatus({
    param: {
      project_id: project_id
    },
    success: function(res) {
      if (res.code == 0) {
        rs.data.withdrawButton = res.data; // 将项目状态参数传过去，控制button按钮

        view.init(rs);
        view.getVerifyStatus(rs);
      } else {
        utils.alertMessage(res.msg);
      }
    }
  });
}

$UI.on("needload", function(e, page) {
  model.getTransactionInfo({
    param: {
      project_id: project_id,
      page: page
    },
    success: function(res) {
      if (res.code == 0) {
        view.insertData(res);
        // console.log('insertData', res);
      } else {
        utils.alertMessage(res.msg);
      }
    }
  });
});

$UI.on("jumpToWithDrawPage", function(e, stopped) {
  console.log(
    "跳转=",
    "/withdraw.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&stopped=" +
      stopped
  );
  location.href =
    "/withdraw.html?project_id=" +
    project_id +
    "&short_link=" +
    short_link +
    "&stopped=" +
    stopped;
});
