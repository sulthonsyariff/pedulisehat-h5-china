import utils from "utils";
let obj = {};

obj.init = function(rs) {
  var myDate = new Date(rs);

  var year = myDate.getFullYear();
  var month = myDate.getMonth() + 1; //js从0开始取
  var date = myDate.getDate();
  var hour = myDate.getHours();
  var minutes = myDate.getMinutes();
  var second = myDate.getSeconds();

  // if (month < 10) {
  //   month = "0" + month;
  // }
  if (month == 1) {
    month = "Jan";
} else if (month == 2) {
    month = "Feb";
} else if (month == 3) {
    month = "Mar";
} else if (month == 4) {
    month = "Apr";
} else if (month == 5) {
    month = "May";
} else if (month == 6) {
    month = "June";
} else if (month == 7) {
    month = "July";
} else if (month == 8) {
    month = "Aug";
} else if (month == 9) {
    month = "Sept";
} else if (month == 10) {
    month = "Oct";
} else if (month == 11) {
    month = "Nov";
} else if (month == 12) {
    month = "Dec";
}

  if (date < 10) {
    date = "0" + date;
  }
  if (hour < 10) {
    hour = "0" + hour;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (second < 10) {
    second = "0" + second;
  }

  return hour + ":" + minutes + "  " + date + ' ' + month + ' ' + year;

  // return (
  //   year + "." + month + "." + date + "  " + hour + ":" + minutes + ":" + second
  // );
};

export default obj;
