import ajaxProxy from "ajaxProxy";
import "jq_cookie"; //ajax cookie
import domainName from "domainName"; // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// get transaction info (bills flow)
obj.getTransactionInfo = function(o) {
  var url =
    domainName.project +
    "/v1/wallet_bill?project_id=" +
    o.param.project_id +
    "&page=" +
    o.param.page;

  if (isLocal) {
    url = "/mock/v1_wallet_bill.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      type: "get",
      dataType: "json"
    },
    o
  );
};

//get wallet info
obj.getWalletInfo = function(o) {
  var url = domainName.project + "/v2/wallet?project_id=" + o.param.project_id;

  if (isLocal) {
    url = "/mock/v2_wallet.json";
  }

  ajaxProxy.ajax(
    {
      url: url,
      type: "get",
      dataType: "json"
    },
    o
  );
};

//project verify status
obj.projectVerifyStatus = function(o) {
  var url =
    domainName.project + "/v1/withdraw/button?project_id=" + o.param.project_id;

  if (isLocal) {
    url = "/mock/v1_withdraw_button.json";
  }
  ajaxProxy.ajax(
    {
      url: url,
      type: "get",
      dataType: "json"
    },
    o
  );
};

export default obj;
