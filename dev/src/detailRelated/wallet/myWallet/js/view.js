// 公共库
import commonNav from "commonNav";
import commonFooter from "commonFooter";
import mainTpl from "../tpl/main.juicer";
import qscScroll_timestamp from "qscScroll_timestamp";
import transactionsItemTpl from "../tpl/_transactionsItem.juicer";
import alertBoxVerifyTpl from "../tpl/_alert-box-verify.juicer"; //弹窗：验证
import alertBoxBalanceLess_1 from "../tpl/_alert-box-balance-less-1.juicer"; //弹窗：金额<500000
import alertBoxBalanceLess_2 from "../tpl/_alert-box-balance-less-2.juicer"; //弹窗：金额<20000

import countDownHandle from "./_countDownHandle"; //倒计时处理
import timeHandle from "./_timeHandle"; //钱包账单时间处理

import fastclick from "fastclick";
import domainName from "domainName"; //port domain name
import utils from "utils";
import googleAnalytics from "google.analytics";
import changeMoneyFormat from "changeMoneyFormat";
//import sensorsActive from "sensorsActive";

/* translation */
import myWallet from "myWallet";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(myWallet);
/* translation */

var reqObj = utils.getRequestParams();
var project_id = reqObj["project_id"];
var short_link = reqObj["short_link"];

var page = 0;
var is_first = true;
var scroll_list = new qscScroll_timestamp();

let time;

let [obj, $UI] = [{}, $("body")];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
  res.JumpName = titleLang.wallet;
  res.commonNavGoToWhere = "/" + short_link;
  res.lang = lang;

  $("title").html(titleLang.wallet);
  commonNav.init(res);

  //   注意不要改变原数据，避免污染数据
  res.data._net_balance = changeMoneyFormat.newMoneyFormat(
    res.data.net_balance
  );
  res.data._platform_service_fee = changeMoneyFormat.newMoneyFormat(
    res.data.platform_service_fee
  );
  res.data._payment_channel_fee = changeMoneyFormat.newMoneyFormat(
    res.data.payment_channel_fee
  );
  res.data._net_amount = changeMoneyFormat.newMoneyFormat(res.data.net_amount);
  res.data._funded_amount = changeMoneyFormat.newMoneyFormat(
    res.data.funded_amount
  );
  res.data._ads_fee = changeMoneyFormat.newMoneyFormat(
    res.data.ads_fee
  );

  $UI.append(mainTpl(res)); //主模版
  commonFooter.init(res);

  initScorll(); //账单列表

  fastclick.attach(document.body);

  // google anaytics
  let param = {};
  googleAnalytics.sendPageView(param);
  // sensorsActive.init();
};

/**
 *
stopped           :yes	bool	项目是否结束
verify_state      :yes	int	    项目验证状态 通过验证是3
is_bright	      :yes	bool	按钮是否亮
remain_wait	      :yes	int	    等待时间 秒为单位 （注意 当为-1的时候特殊处理，说明压根不需要倒计时，比如提现正在处理，根本没法计算等待时间）
cd_end_time	      :yes	int	    等待截止时间 时间戳
count_down_hint	  :yes	string	按钮下红字
stop_limit_money  :yes	float	项目结束提现所需最小金额
tmp_limit_money	  :yes	float	随时提现项目最小余额
 *  
 */
obj.getVerifyStatus = function(res) {
  console.log("项目帐户=", res.data);

  let withdrawButtonState = res.data.withdrawButton;

  // 非提现处理中
  if (withdrawButtonState.remain_wait != -1) {
    console.log("没有提现申请······");

    // 项目是否有钱
    // if (res.data.net_balance) {
    // console.log("项目有钱");

    // 项目是否验证通过
    if (withdrawButtonState.verify_state == 3) {
      console.log("项目验证通过");
      // 项目是否结束
      if (withdrawButtonState.stopped) {
        console.log("项目结束");
        // 项目结束是否超过24H
        if (withdrawButtonState.remain_wait > 0) {
          console.log("项目结束没有超过24H，倒计时+tips提示");

          countDownHandle.init(res);
        } else {
          // 按钮下红字tips提示 + 倒计时
          console.log("项目结束超过24H");
          // 金额Rp>=20000
          if (res.data.net_balance >= withdrawButtonState.stop_limit_money) {
            console.log("项目金额多于20000，可提交了,点击可跳转");

            $("body").on("click", ".withdraw", function(e) {
              $UI.trigger("jumpToWithDrawPage", withdrawButtonState.stopped);
            });
          }
          //  金额Rp<20000
          else {
            console.log("项目金额少于20000，点击弹窗提示！");

            $UI.append(alertBoxBalanceLess_2(res)); //弹窗：金额Rp>=20000
            $("body").on("click", ".withdraw", function(e) {
              $(".alert-box-balance-less-2").css({
                display: "block"
              });
            });
          }
        }
      } else {
        console.log("项目没有结束，可随时提现");
        if (withdrawButtonState.remain_wait > 0) {
          console.log("项目进行中7天内再次提现，倒计时+tips提示");

          countDownHandle.init(res);
        } else {
          console.log("项目进行中7天后再次提现(正常流程)");
          // 金额Rp>=500000
          if (res.data.net_balance >= withdrawButtonState.tmp_limit_money) {
            console.log("项目金额多于500000，可提交了,点击可跳转");

            $("body").on("click", ".withdraw", function(e) {
              $UI.trigger("jumpToWithDrawPage", withdrawButtonState.stopped);
            });
          } else {
            console.log("项目金额少于50000,点击弹窗提示！");

            $UI.append(alertBoxBalanceLess_1(res)); //弹窗：金额Rp>=20000
            $("body").on("click", ".withdraw", function(e) {
              $(".alert-box-balance-less-1").css({
                display: "block"
              });
            });
          }
        }
      }
    } else {
      console.log("项目没有验证通过，点击弹窗提示去验证");

      $UI.append(alertBoxVerifyTpl(res)); //验证弹窗
      $("body").on("click", ".withdraw", function(e) {
        $(".alert-box-verify").css({
          display: "block"
        });
      });
    }

    // }
    // 项目没钱
    // else {
    //   // 正在打款倒计时
    //   if (withdrawButtonState.remain_wait > 0) {
    //     console.log('正在打款倒计时中······')

    //     countDownHandle.init(res);
    //   }
    // }
  } else if (withdrawButtonState.remain_wait == -1) {
    console.log("提现申请中······");
    // 此处后端自动有tips提示
  }

  clickHandle(res);
};

function clickHandle(res) {
  // no-active
  $("body").on("click", ".withdraw", function(e) {
    if ($(this).hasClass("no-active")) {
      return false;
    }
  });

  // droplist
  $("body").on("click", ".list", function() {
    $(this).toggleClass("open");
  });

  //  requirement
  $("body").on("click", ".requirement,#checkRules", function() {
    $(".alert-box").css({
      display: "none"
    });
    location.href =
      "/withdrawRules.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link;
  });

  //  关闭弹窗
  $("body").on("click", "#closeAlertBox", function(e) {
    $(".alert-box").css({
      display: "none"
    });
  });

  // verify alert box 验证没有通过弹窗
  $("body").on("click", "#yes_alertBoxVerify", function(e) {
    $(".alert-box").css({
      display: "none"
    });

    location.href =
      "/patientVerification.html?project_id=" +
      project_id +
      "&short_link=" +
      short_link +
      "&from=myWallet";
  });

  // mask
  $("body").on("click", ".alert-box-verify .mask", function(e) {
    $(".alert-box").css({
      display: "none"
    });
  });
}

obj.insertData = function(rs) {
  rs.domainName = domainName;
  rs.lang = lang;

  if (rs.data.length != 0) {
    if (is_first) {
      rs.is_first = is_first;
    }
    is_first = false;
    if (rs.data != "") {
      for (var i = 0; i < rs.data.length; i++) {
        //时间
        if (rs.data[i].total_money < 0) {
          rs.data[i].minus = true;
        } else {
          rs.data[i].minus = false;          
        }

        rs.data[i].transactionsTime = timeHandle.init(rs.data[i].created_at);

        rs.data[i].total_money = changeMoneyFormat.newMoneyFormat(
          Math.abs(rs.data[i].total_money)
        );
        rs.data[i].platform_service_fee = changeMoneyFormat.newMoneyFormat(
          rs.data[i].platform_service_fee
        );
        rs.data[i].payment_channel_fee = changeMoneyFormat.newMoneyFormat(
          rs.data[i].payment_channel_fee
        );
        rs.data[i].net_donation = changeMoneyFormat.newMoneyFormat(
          Math.abs(rs.data[i].net_donation)
        );
      }
    }

    $(".transactions_list").append(transactionsItemTpl(rs));
    scroll_list.run();
  } else if (is_first) {
    $(".loading").hide();
    $(".transactions_list").append(transactionsItemTpl(rs));
  } else if (rs.data.length == 0) {
    $(".loading").hide();
  }
};

// 账单列表
function initScorll() {
  scroll_list.config({
    wrapper: $UI,
    onNeedLoad: function() {
      $UI.trigger("needload", [++page]);
    }
  });
  scroll_list.run();
}

export default obj;
