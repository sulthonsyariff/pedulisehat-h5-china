var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'modifyStory',
    htmlFileURL: 'html/modifyStory.html',
    appDir: 'js/modifyStory',
    uglify: true,
    hash: '',
    mode: 'production'
})