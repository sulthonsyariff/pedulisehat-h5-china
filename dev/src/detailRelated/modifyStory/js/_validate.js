/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    if (param.story == '') {
        utils.alertMessage(lang.lang10);
        return false;
    }

    // cover
    if (param.cover.length != 1) {
        utils.alertMessage(lang.lang18);

        return false;
    }


    if (param.images.length < 1) {
        utils.alertMessage(lang.lang15);

        return false;
    }

    //finish upload or not
    let imgs = param.images;
    for (let i = 0; i < imgs.length; i++) {
        if (!imgs[i].thumb || !imgs[i].image) {
            utils.alertMessage(lang.lang14);
            return false;
        }
    }

    return true;
}

export default obj;