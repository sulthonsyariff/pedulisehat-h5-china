import view from './view'
import model from './model'
import 'loading'
import '../less/modifyStory.less'
// 引入依赖
import utils from 'utils'
import modifyStory from 'modifyStory'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyStory);

// 隐藏loading
utils.hideLoading();
var UI = view.UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];
var short_link = reqObj['short_link'];

getProjectInfo();

function getProjectInfo() {
    if (project_id) {
        model.getProjectInfo({
            param: {
                project_id: project_id
            },
            success: function(res) {
                if (res.code == 0) {
                    init(res);
                    // console.log('getProjectInfo', res);
                } else {
                    utils.alertMessage(res.msg)
                }
            },
            error: utils.handleFail
        })
    }
};

function init(projInfo) {
    view.init(projInfo);

    UI.on('submit', function(e, objData) {
        objData.project_id = project_id;

        model.postData({
            param: objData,
            // param: {
            //     project_id: project_id,
            //     story: objData.input_story,
            //     images: objData.images
            // },
            success: function(res) {
                if (res.code == 0) {
                    utils.hideLoading();
                    if (projInfo.data.verify_state == 3) {
                        utils.alertMessage(lang.lang9);
                    } else {
                        utils.alertMessage(lang.lang9_1);
                    }
                    setTimeout(function() {
                        location.href = ('/' + short_link)
                    }, 1000)
                } else {
                    utils.alertMessage(res.msg)
                }
                // console.log('init', res);
            },
            error: utils.handleFail
        })
    });
}