import mainTpl from '../tpl/modifyStory.juicer'
import commonNav from 'commonNav'
import utils from 'utils'
import imgUploader from 'uploadCloudinaryMore'
// import picCover from './_picCover'
import 'fancybox'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import fastclick from 'fastclick'
import validate from './_validate'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import modifyStory from 'modifyStory'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(modifyStory);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 图片上传相关
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploadList'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number

/* upload cover */
let uploadWrapper2 = '#uploadWrapper2';
let uploadBtn2 = 'upload-btn2'; // upload btn
let uploadList2 = '.uploadList2'; // img container
let uploadKey2 = 'cover2'; // upload key
let uploadNumLimit2 = 1; // limit number
/* upload cover */

obj.init = function(rs) {
    rs.JumpName = titleLang.story;
    commonNav.init(rs);
    $('title').html(titleLang.story);

    rs.lang = lang;
    $UI.append(mainTpl(rs));
    // console.log('rs', rs)
    // console.log('lang===', lang);

    // 初始化图片上传
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);
    imgUploader.create_cover(uploadBtn2, uploadList2, uploadKey2, uploadNumLimit2, imgChangedHandler, uploadWrapper2);

    // let images = JSON.parse(rs.data.images);
    if (rs.data && rs.data.images && JSON.parse(rs.data.images).length > 0) {
        imgUploader.setImageList(uploadList, uploadKey, JSON.parse(rs.data.images));
    }

    // 封面图处理
    if (rs.data && rs.data.cover_args && JSON.parse(rs.data.images).length > 0) {
        imgUploader.setImageList(uploadList2, uploadKey2, JSON.parse(rs.data.cover_args)); //cover
    }

    hideOrShowInputButton.init(); //show or hide upload btn,注意在image加载后再隐藏input

    $('.fancybox').fancybox();
    fastclick.attach(document.body);

    //set cover
    // picCover.setCover(lang);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

    // quill editor
        var toolbarOptions = [
            ['bold', 'italic', 'underline']
        ];

        let placeholder = lang.lang2;
    
        var quill = new Quill('.input_story', {
        modules: {
            toolbar: toolbarOptions
        },
        placeholder: placeholder,
        theme: 'snow'
        });
    
        // end quill editor    
        
        // quill editor
        function getStory() {
            var delta = quill.getContents();
            
            return quillGetHTML(delta);
        }

        function getTextStory() {
            return quill.getText().replace(/\n/g, '') + "";
        }
        
        function quillGetHTML(inputDelta) {
            var tempCont = document.createElement("div");
            (new Quill(tempCont)).setContents(inputDelta);
        
            return tempCont.getElementsByClassName("ql-editor")[0].innerHTML;
        }
        
        function insertCanned(canned_html) {
            quill.clipboard.dangerouslyPasteHTML(canned_html);
        }
        // end quill editor    
        getTextareaParams(getTextStory());

        $(document).on('input propertychange', '.input_story', function(e) {
            getTextareaParams(getTextStory());
        })
    
        $('body').on('click', '.saveBtn', function() {
            var submitData = getSubmitData(getStory());
            if (validate.check(submitData, lang)) {
                // $('.submitBtn').addClass('disabled');
                $UI.trigger('submit', [submitData]);
            }
    
        });    
};

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    // console.log('initiate info img changed');
    // imgUploader.refreshPictureNumber(uploadList);
    // updateCache();
    $('.fancybox').fancybox();
    //set cover
    // picCover.setCover(lang);
    hideOrShowInputButton.init(); //show or hide upload btn
}


// 获取提交数据
function getSubmitData(story) {
    return {
        project_id: '',
        story: story,
        images: imgUploader.getImageList(uploadList, uploadKey),
        cover: imgUploader.getImageList(uploadList2, uploadKey2),
    }
}
// console.log('CACHED_VAL', getSubmitData())

// 输入框字数提示
function getTextareaParams(value) {
    let num;
    
    num = value.length;
    if (num <= 5000) {
        num = num;
    } else {
        $(num).substr(0, 5000)
        utils.alertMessage(lang.lang11);
    }
    $(".contentcount").html(num)
}


export default obj;