var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'modifyStory',
    htmlFileURL: 'html/modifyStory.html',
    appDir: 'js/modifyStory',
    uglify: true,
    hash: '',
    mode: 'development'
})