import view from "./view";
import model from "./model";
import "loading";
import "../less/main.less";
// 引入依赖
import utils from "utils";

// 隐藏loading
utils.hideLoading();
let UI = view.UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];

model.projectLimit({
  data: 2, //1 项目限制天数 2 延期天数
  success: function(res) {
    if (res.code == 0) {
      console.log("===projectLimit===", res);
      res.arr = res.data;
      view.init(res);
    } else {
      utils.alertMessage(rs.msg);
    }
  },
  error: utils.handleFail
});

UI.on('choose_time',function(e,limit_day){
  view.chooseTime();
})

UI.on('choose_cancel',function(){
  view.chooseCancel();

})

UI.on("submit", function(e, limit_day) {
  console.log("===submit===", limit_day);

  model.submit({
    param: {
      project_id: project_id,
      limit_day: limit_day
    },

    success: function(res) {
      if (res.code == 0) {
        console.log("===res===", res);
        window.history.go(-1);
      } else {
        utils.alertMessage(res.msg);
      }
    },
    error: utils.handleFail
  });
});
