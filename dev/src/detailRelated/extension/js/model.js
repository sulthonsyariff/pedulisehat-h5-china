import ajaxProxy from "ajaxProxy";
import "jq_cookie"; //ajax cookie
import domainName from "domainName"; // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// 获取时间限制 1 项目限制天数 2 延期天数
obj.projectLimit = function(param) {
  let url = domainName.project + "/v1/project_limit?data_type=" + param.data;

  if (isLocal) {
    url = "../mock/v1_project_limit.json";
  }

  ajaxProxy.ajax(
    {
      type: "get",
      url: url
    },
    param
  );
};

obj.submit = function(o) {
  let url = domainName.project + "/v1/project/" + o.param.project_id + "/limit";

  ajaxProxy.ajax(
    {
      type: "put",
      url: url,
      data: JSON.stringify({
        limit_day: parseInt(o.param.limit_day)
      })
    },
    o
  );
};

export default obj;
