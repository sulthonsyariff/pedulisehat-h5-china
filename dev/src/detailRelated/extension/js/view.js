import mainTpl from "../tpl/main.juicer";
import commonFooter from "commonFooter";
import commonNav from "commonNav";
import utils from "utils";
import fastclick from "fastclick";
import changeMoneyFormat from "changeMoneyFormat"; //divide the amount by decimal point
import googleAnalytics from "google.analytics";
//import sensorsActive from "sensorsActive";
import commonTimeLimit from "commonTimeLimit";

/* translation */
import _lang from "extension";
import qscLang from "qscLang";
import commonTitle from "commonTitle";
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(_lang);
/* translation */

let total_amount;
let [obj, $UI] = [{}, $("body")];
var reqObj = utils.getRequestParams();
var short_link = reqObj["short_link"];

obj.UI = $UI;

obj.init = function(rs) {
  rs.JumpName = titleLang.Extension;
  commonNav.init(rs);
  $("title").html(titleLang.Extension);

  rs.lang = lang;
  $UI.append(mainTpl(rs));

  commonFooter.init(rs);

  commonTimeLimit.init(rs); //limit time choose

  fastclick.attach(document.body);

  // google anaytics
  let param = {};
  googleAnalytics.sendPageView(param);
  // sensorsActive.init();

  // show limit time choose
  $("body").on("click", ".select-time-limit", function() {
    $(".common_limit_time").css({
      display: "block"
    });
  });

  // save
  $("body").on("click", ".saveBtn.active", function() {
    console.log("save");
    $UI.trigger("submit", $(".select-time-limit .num").text());
    utils.showLoading(lang.lang7);
  });
};

obj.chooseTime = function() {
  $(".select-time-limit").addClass("active");
  $(".saveBtn").addClass("active");
};

obj.chooseCancel = function() {
  $(".select-time-limit").removeClass("active");
  $(".saveBtn").removeClass("active");
};

export default obj;
