var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'extension',
    htmlFileURL: 'html/extension.html',
    appDir: 'js/extension',
    uglify: true,
    hash: '',
    mode: 'development'
})