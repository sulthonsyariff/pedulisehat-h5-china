import mainTpl from '../tpl/list.juicer'
import listItemTpl from '../tpl/listItem.juicer'
import commonNav from 'commonNav'
import commonShare from 'commonShare'
import qscScroll_timestamp from 'qscScroll_timestamp'
import timeHandle from './_timeHandle'
import fastclick from 'fastclick'
import domainName from 'domainName'; //接口域名
import googleAnalytics from 'google.analytics'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'
import commonFooter from 'commonFooter'
import share from 'share'

/* 翻译包 */
import confirmationList from 'confirmationList'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(confirmationList);
/* 翻译包 */
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
var short_link = reqObj['short_link'];

var is_first = true;
var page = 0;
var scroll_list = new qscScroll_timestamp();

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

obj.init = function(res) {
    res.commonNavGoToWhere = '/' + short_link;
    res.JumpName = titleLang.confirmCampaignList;
    commonNav.init(res);
    $('title').html(titleLang.confirmCampaignList);

    res.lang = lang;
    console.log('view:', res);
    fastclick.attach(document.body);

    $UI.append(mainTpl(res));
    res.fromWhichPage = 'confirmationList.html'; //google analytics need distinguish the page name
    commonShare.init(res);

    initScorll();
    clickEvents(res);


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    commonFooter.init({}); // control the height display correct when download link is closed

    // sensorsActive.init();
};


function initScorll() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });

    scroll_list.run();
};

obj.insertData = function(rs) {
    if (rs.data) {
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (var i = 0; i < rs.data.length; i++) {

            //时间
            rs.data[i].confirmsHours = timeHandle.confirmsHours(rs.data[i].created_at);
            rs.data[i].confirmsTime = timeHandle.userDate(rs.data[i].created_at);
            rs.data[i].confirmsMinutes = timeHandle.confirmsMinutes(rs.data[i].created_at);

            // 企业用户
            rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';

            if (rs.data[i].avatar.indexOf('http') == 0) {
                rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar)
            } else {
                rs.data[i].avatar = domainName.static + '/img/avatar/' + rs.data[i].avatar;
            }
        }
        rs.domainName = domainName;
        rs.lang = lang;

        $('.confirms-list').append(listItemTpl(rs));
        $('.confirms-count').html(rs._metadata.count);
        scroll_list.run();

        //textarea 换行问题
        $(".confirms-detail").each(function() {
            var temp = $(this).text().replace(/\n|\r\n/g, '<br/>');
            $(this).html(temp);
        });

    } else {
        $('.loading').hide();
    }
}

function clickEvents(rs) {
    console.log('===clickEvents rs===', rs);

    $('body').on('click', '.confirmBtn', function() {
        $UI.trigger('confirm');
    })

    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '.inviteFriends', function(e) {
        let paramObj = {
                item_id: rs.data.project_id,
                item_type: 1, //3 代表勋章 / 1 代表大病详情类型
                item_short_link: rs.data.short_link,
                // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
                shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
                shareCallBackName: 'projectShareSuccess', //大病详情分享
                fromWhichPage: 'confirmationList.html' //google analytics need distinguish the page name
            }
            // 安卓端分享需要项目详情，业务页面采集，组件中使用
        if (utils.browserVersion.android) {
            let paramString = "&title=" + encodeURIComponent(rs.data.name) +
                "&description" + encodeURIComponent(rs.data.story.substr(1, 100)) +
                "&image=" + (rs.data.images[0] ? encodeURIComponent(rs.data.images[0].image) : '');

            $UI.trigger('android-share', [paramString, paramObj]);

        } else {
            // 分享组件
            share.init(paramObj);
        }
    });

    // share close
    // $('body').on('click', '#shareClose', function(e) {
    //     $('.common-share').css({
    //         'display': 'none'
    //     });
    // });
}

export default obj;