import view from './view'
import model from './model'
import 'loading'
import '../less/list.less'
import domainName from 'domainName'; //接口域名

// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.hideLoading();

let UI = view.UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'];
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

let confirmJumpUrl = '';

/*
    android:
    req_code = 400 android login success and jump to confirmPage
*/
window.appHandle = function(req_code) {
    if (req_code == 400) {
        this.location.href = location.origin + '/confirmPage.html?project_id=' + project_id + '&short_link=' + short_link;
    }
}

// 调用获取项目信息 yangcheng
model.getProjInfo({
    param: {
        project_id: project_id
    },
    success: function(rs) {
        if (rs.code == 0) {
            console.log('rs', rs)

            judgeOwer(rs);

            // facebook分享添加meta头部信息
            // utils.changeFbHead({
            //     url: short_link ?
            //         ("https:" + domainName.share + '/' + short_link) : ("https:" + domainName.project + "/v1/project_share_tpl?projectId=" + project_id),
            //     // url: "https:" + domainName.project + "/v1/project_share_tpl?projectId=" + project_id,
            //     title: rs.data.name,
            //     description: rs.data.story.substr(1, 100),
            //     image: rs.data.images[0].image
            // });
        } else {
            view.init({});
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
})

// judge用户是否是主态
function judgeOwer(rs) {
    // 未登录用户情况
    let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
    let project_user_id = rs.data.user_id;

    if (user_id == project_user_id) {
        rs.data.is_ower = true;
    } else {
        rs.data.is_ower = false;
    }
    view.init(rs);
}

UI.on('needload', function(e, page) {
    model.getListData({
        param: {
            page: page,
            project_id: project_id
        },
        success: function(data) {
            if (data.code == 0) {
                view.insertData(data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
});

model.getUserInfo({
    success: function(rs) {
        if (rs.code == 0) {
            // 只要登陆成功，一定会有手机号
            confirmJumpUrl = '/confirmPage.html?project_id=' + project_id + '&short_link=' + short_link + '&from=confirmationList';
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    unauthorizeTodo: function(rs) {
        //登陆页
        confirmJumpUrl = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/login?req_code=400' : '/login.html?qf_redirect=' + encodeURIComponent(location.href);
    },
    error: utils.handleFail,
});

UI.on('confirm', function(e) {
    location.href = confirmJumpUrl;
})

UI.on('projectShareSuccess', function(e, target_type) {
    console.log('===projectShareSuccess===');
    shareActionCount(target_type);
});

function shareActionCount(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: project_id,
            item_type: 1,
            // scene: 1,
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位
        },
        success: function(res) {
            //分享成功后
            console.log('project Share success');

        },
        error: utils.handleFail
    });
}