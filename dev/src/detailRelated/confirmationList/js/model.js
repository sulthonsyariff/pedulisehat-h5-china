import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

// console.log(ajaxProxy);
obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

obj.getListData = function(o) {
    var url = domainName.project + '/v1/project_confirms?project_id=' + o.param.project_id + '&page=' + o.param.page;

    if (isLocal) {
        url = '/mock/project_confirms' + o.param.page + '.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}

/**
 * 获取项目信息 yangcheng
 */
obj.getProjInfo = function(o) {
    var url = domainName.project + '/v1/project/detail?project_id=' + o.param.project_id;
    if (isLocal) {
        url = 'mock/detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

/*
{
    "target_type": 1,
    "project_id": "14221049989731189491"
}

TargetTypefacebook = 1 // facebook
TargetTypeWhatsApp = 2 // whatsapp
TargetTypeTwitter = 3 // twitter
TargetTypeLink = 4 // link
TargetTypeLine = 5 // line
TargetTypeInstagram = 6 // instagram
TargetTypeYoutube = 7 // youtube
*/
// obj.projectShare = function(o) {
//     var url = domainName.project + '/v1/project_share';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }

/*
{
    "target_type": 1,
    "project_id": "14221049989731189491"
}

TargetTypefacebook = 1 // facebook
TargetTypeWhatsApp = 2 // whatsapp
TargetTypeTwitter = 3 // twitter
TargetTypeLink = 4 // link
TargetTypeLine = 5 // line
TargetTypeInstagram = 6 // instagram
TargetTypeYoutube = 7 // youtube
*/
//旧分享统计为安卓保留 1.19版本以前
// obj.projectShare = function (o) {
//     let url = domainName.project + '/v1/project_share';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }
//新分享统计接口
obj.newProjectShare = function(o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

export default obj;