var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'confirmation list',
    htmlFileURL: 'html/confirmationList.html',
    appDir: 'js/confirmationList',
    uglify: true,
    hash: '',
    mode: 'production'
})