var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'confirmation list',
    htmlFileURL: 'html/confirmationList.html',
    appDir: 'js/confirmationList',
    uglify: true,
    hash: '',
    mode: 'development'
})