var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'update list',
    htmlFileURL: 'html/updateList.html',
    appDir: 'js/updateList',
    uglify: true,
    hash: '',
    mode: 'production'
})