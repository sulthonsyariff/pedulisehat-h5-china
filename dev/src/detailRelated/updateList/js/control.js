import view from './view'
import model from './model'
import 'loading'
import '../less/list.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.hideLoading();

var UI = view.UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];

view.init({});

UI.on('needload', function(e, page) {
    // console.log('needload');
    model.getListData({
        param: {
            page: page,
            project_id: project_id
        },
        success: function(data) {
            if (data.code == 0) {
                view.insertData(data);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
});