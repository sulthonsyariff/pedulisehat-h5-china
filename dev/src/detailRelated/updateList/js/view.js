import mainTpl from '../tpl/list.juicer'
import listItemTpl from '../tpl/listItem.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import qscScroll_timestamp from 'qscScroll_timestamp'
import timeHandle from './_timeHandle'
import 'fancybox'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
import utils from 'utils';
// import sensorsActive from 'sensorsActive'


/* translation */
import updateList from 'updateList'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(updateList);
/* translation */

var is_first = true;
var page = 0;
var scroll_list = new qscScroll_timestamp();

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

obj.init = function(res) {
    res.JumpName = titleLang.myUpdates;
    commonNav.init(res);

    $('title').html(titleLang.myUpdates);

    res.lang = lang;
    // console.log('view:', res);
    // $('.fancybox').fancybox();
    fastclick.attach(document.body);

    $UI.append(mainTpl(lang));
    initScorll();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};


function initScorll() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });

    scroll_list.run();
};

obj.insertData = function(rs) {
    if (rs.data.dynamicUpdates) {
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (var i = 0; i < rs.data.dynamicUpdates.length; i++) {


            // fancybox tell
            rs.data.dynamicUpdates[i].p = page;
            // console.log('rs.page',$('.fancybox'  + '_' + i + '_' + rs.data.dynamicUpdates[i].p))
            $('.fancybox' + '_' + i + '_' + rs.data.dynamicUpdates[i].p).fancybox();
            if (rs.data.dynamicUpdates[i].type != 1) {
                // rs.data.dynamicUpdates[i].content = JSON.parse(rs.data.dynamicUpdates[i].content);
                rs.data.dynamicUpdates[i].images = JSON.parse(rs.data.dynamicUpdates[i].images);

                for (var j = 0; j < rs.data.dynamicUpdates[i].images.length; j++) {

                    let rightUrl = utils.imageChoose(rs.data.dynamicUpdates[i].images[j])
                    rs.data.dynamicUpdates[i].images[j].rightUrl = rightUrl


                    let oldAddOverlay = utils.addOverlay(rs.data.dynamicUpdates[i].images[j])
                    rs.data.dynamicUpdates[i].images[j].oldAddOverlay = oldAddOverlay
                }
            }



            // console.log('===rs.data.dynamicUpdates===', rs.data.dynamicUpdates[i].images)
            //时间
            rs.data.dynamicUpdates[i].updatesHours = timeHandle.updateHours(rs.data.dynamicUpdates[i].created_at);
            rs.data.dynamicUpdates[i].updatesDays = timeHandle.updateDays(rs.data.dynamicUpdates[i].created_at);
            rs.data.dynamicUpdates[i].updatesTime = timeHandle.userDate(rs.data.dynamicUpdates[i].created_at);
            rs.data.dynamicUpdates[i].updatesMinutes = timeHandle.updateMinutes(rs.data.dynamicUpdates[i].created_at);

            // 企业用户
            rs.data.dynamicUpdates[i].corner_mark = rs.data.dynamicUpdates[i].corner_mark ? JSON.parse(rs.data.dynamicUpdates[i].corner_mark) : '';
        }
        console.log('rs', rs)
        rs.domainName = domainName;
        rs.lang = lang;

        // $UI.append(mainTpl(rs));

        $('.updates-list').append(listItemTpl(rs));

        for (var i = 0; i < rs.data.dynamicUpdates.length; i++) {

            if (rs.data.dynamicUpdates[i].type == 1) {
                $('.updates-wrap_' + i).css('background', '#f8f8f8')
            }
        }
        $('.updates-count').html(rs.data.count);
        scroll_list.run();

        for (var i = 0; i < rs.data.dynamicUpdates.length; i++) {
            if (rs && rs.data.dynamicUpdates && rs.data.dynamicUpdates[i].avatar) {
                if (rs.data.dynamicUpdates[i].avatar.indexOf('http') == 0) {
                    $('.avatar-updateList').attr('src', utils.imageChoose(rs.data.dynamicUpdates[i].avatar));
                } else {
                    $('.avatar-updateList').attr('src', domainName.static + '/img/avatar/' + rs.data.dynamicUpdates[i].avatar);
                }
            }
        }
        //textarea 换行问题
        $(".updates-detail").each(function() {
            var temp = $(this).text().replace(/\n|\r\n/g, '<br/>');
            $(this).html(temp);
        });

    } else {
        $('.loading').hide();
    }
}

export default obj;