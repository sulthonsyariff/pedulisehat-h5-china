let obj = {};
// 创建时间 yangcheng
obj.userDate = function (created_at) {
    var myDate = new Date(created_at);

    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;
    // var month = 11;

    var day = myDate.getDate();

    if(month < 2){
        month = "January";
    }
    else if(month < 3){
        month = "February";
    }
    else if(month < 4){
        month = "March";
    }
    else if(month < 5){
        month = "April";
    }
    else if(month < 6){
        month = "May";
    }
    else if(month < 7){
        month = "June";
    }
    else if(month < 8){
        month = "July";
    }
    else if(month < 9){
        month = "August";
    }
    else if(month < 10){
        month = "September";
    }
    else if(month < 11){
        month = "October";
    }
    else if(month < 12){
        month = "November";
    }
    else if(month < 13){
        month = "December";
    }

    if(day < 10){
        day = "0" + day;
    }
    return day + ' ' + month + ' ' + year;
};

//  更新列表时间 yangchyeng
    

obj.updateHours = function(created_at) {
    var t1 = new Date(created_at);
    var t2 = new Date();
    var t3 = t2.getTime()-t1.getTime();

    var t4 = t3%(24*3600*1000) //计算天数后剩余的毫秒数
    var updateHours = Math.floor(t4/(3600*1000)) //计算小时数
    return updateHours;
}

obj.updateDays = function(created_at) {
    
    var t1 = new Date(created_at);
    var t2 = new Date();
    var t3 = t2.getTime()-t1.getTime();
    var days=Math.floor(t3/(24*3600*1000))
    console.log('days',days)
    return days;
}
obj.updateMinutes = function(created_at) {
    
    var t1 = new Date(created_at);
    var t2 = new Date();
    var t3 = t2.getTime()-t1.getTime();


    var leave1=t3%(24*3600*1000)    //计算天数后剩余的毫秒数
    var leave2=leave1%(3600*1000)        //计算小时数后剩余的毫秒数
    var updateMinutes=Math.floor(leave2/(60*1000))


    return updateMinutes;
}
 export default obj;