import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

// console.log(ajaxProxy);

obj.verify = function(o) {
    var url = domainName.passport + '/v1/login/verify';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

obj.getListData = function(o) {
    var url = domainName.project + '/v1/project/dynamic/update?project_id=' + o.param.project_id + '&page=' + o.param.page;

    if (isLocal) {
        url = '/mock/project_lists_' + o.param.page + '.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}

export default obj;