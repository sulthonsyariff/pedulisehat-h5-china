import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/*****
 * get supporter ranking new
 */
 obj.getSupporterRankingNew = function(o) {
    let url = domainName.share + '/v1/share/list_supporter_per_campaign?project_id=' + o.param.project_id + '&page=' + o.param.page;

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

obj.getSelfSupporterRanking = function (o) {
    let url = domainName.share + '/v1/share/supporter/ranking/self?project_id=' + o.param.project_id;

    if (isLocal) {
        url = '../mock/v1_share_supporter_ranking_self.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

obj.getSupporterRanking = function (o) {
    let url = domainName.share + '/v1/share/supporter/ranking?project_id=' + o.param.project_id;

    if (isLocal) {
        url = '../mock/v1_share_supporter_ranking.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

//获取分享相关内容
obj.getShareInfo = function (o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//新分享统计接口 分享动作统计
obj.newProjectShare = function (o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

export default obj;