import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import 'jq_cookie' //ajax cookie

// 引入依赖
import utils from 'utils'
import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'

let [obj, $UI] = [{}, $('body')];
let UI = view.UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';
let short_link = reqObj['short_link'];
let forwarding_default;
let forwarding_desc;
let shareUrl;

// console.log('statistics_link===',statistics_link, reqObj['statistics_link'], sessionStorage.getItem('statistics_link'));

// disabled supporter ranking old
// model.getSupporterRanking({
//     param: {
//         project_id: project_id
//     },
//     success: function(res) {
//         if (res.code == 0) {
//             console.log('getSupporterRanking===', res);

//             view.init(res);
//             // 隐藏loading
//             utils.hideLoading();

//             getSelfSupporterRanking();

//             //获取分享url等
//             // getShareInfo(res)

//         } else {
//             utils.alertMessage(res.msg)
//         }
//     },
//     error: utils.handleFail
// })

UI.on('needload', function(e, o) {
    console.log('needload', o);

    model.getSupporterRankingNew({
        param: {
            project_id: project_id,
            page: o.page
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('getSupporterRankingNew===', res);
    
                view.addDataRanking(res);
                // 隐藏loading
                utils.hideLoading();
    
                // getSelfSupporterRanking();
    
                //获取分享url等
                // getShareInfo(res)
    
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});

model.getSupporterRankingNew({
    param: {
        project_id: project_id,
        page: 1
    },
    success: function(res) {
        if (res.code == 0) {
            console.log('getSupporterRankingNew===', res);

            view.init(res);
            // 隐藏loading
            utils.hideLoading();

            // getSelfSupporterRanking();

            //获取分享url等
            // getShareInfo(res)

        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
})

// function getShareInfo(res) {
//     // 获取分享统计链接 + 分享语
//     model.getShareInfo({
//         param: {
//             item_id: project_id,
//             item_type: 1,
//             item_short_link: short_link,
//             // statistics_link: statistics_link
//         },
//         success: function(rs) {
//             if (rs.code == 0) {
//                 forwarding_default = rs.data.forwarding_default;
//                 forwarding_desc = rs.data.forwarding_desc;
//                 shareUrl = rs.data.url

//                 // console.log('getShareInfo', forwarding_default, forwarding_desc, shareUrl)
//             } else {
//                 utils.alertMessage(rs.msg)
//             }
//         },
//         error: utils.handleFail

//     })

//     // 如果是安卓端： 吊起安卓分享弹窗
//     $UI.on('android-share', function(e) {
//         let android_share_url =
//             "native://share/webpage" +
//             "?url=" + encodeURIComponent(shareUrl) +
//             "&title=" + encodeURIComponent(rs.data.name) +
//             "&description" + encodeURIComponent(rs.data.story.substr(1, 100)) +
//             "&image=" + (rs.data.images[0] ? encodeURIComponent(rs.data.images[0].image) : '') +
//             "&forwardingDesc=" + encodeURIComponent(forwarding_default + ' %s') +
//             "&forwardingMap=" + encodeURIComponent(JSON.stringify(forwarding_desc));

//         location.href = android_share_url;
//     })
// }

function getSelfSupporterRanking() {
    model.getSelfSupporterRanking({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('getSelfSupporterRanking===', res);
                if (res.data) {
                    view.appendSelfRankingTpl(res);
                }
            } else {
                utils.alertMessage(res.msg)
            }
        },
        unauthorizeTodo: function(rs) {
            console.log('not login');
        },
        error: utils.handleFail
    })
}

/*
监听分享成功回调
*/

$UI.on('projectShareSuccess', function(e, target_type) {
    shareActionCount(target_type);
});

/*****
target_type	Y	int	分享目标
item_id	Y	string	项目ID
item_type	y	int	产品类型   0 互助  1	project  2	专题
scene	N	int	分享场景	1:表示捐赠/付款前的分享 2:表示捐赠/付款成功后的分享 参数不传 默认为1
statistics_link	N	string	分享链接
terminal Y	string	终端类型
client_id	Y	string	客户端ID
 */
function shareActionCount(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type, //分享目标
            item_id: project_id, //项目ID
            item_type: 1, // 产品类型
            // scene: 1, //
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位
        },
        success: function(res) {
            //分享成功后
            if (res.code == 0) {
                console.log('project Share success');

                // let trophiesParam = {}
                // trophiesParam.isDonatedOrshare = 'share'
                ramadanTrophiesAlertBox.init({
                    isDonatedOrshare: 'share',
                    fromWhichPage: 'supporterRanking.html' //google analytics need distinguish the page name
                });

            } else {
                utils.alertMessage(res.msg);
            }
        },
        error: utils.handleFail
    });
}