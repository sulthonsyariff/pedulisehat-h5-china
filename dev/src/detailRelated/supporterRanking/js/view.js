import mainTpl from '../tpl/main.juicer'
import _selfRankingTpl from '../tpl/_selfRanking.juicer'
import alertTpl from '../tpl/_alertBoxTpl.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
import utils from 'utils';
// import sensorsActive from 'sensorsActive'
import changeMoneyFormat from 'changeMoneyFormat'
import share from 'share'

/* translation */
import supporterRanking from 'supporterRanking'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(supporterRanking);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let short_link = reqObj['short_link'] || '';
let page = 1;
let listSupporterRanking;

obj.init = function(res) {
    res.JumpName = titleLang.supporterRanking;
    res.commonNavGoToWhere = '/' + short_link;
    commonNav.init(res);
    $('title').html(titleLang.supporterRanking);
    res.lang = lang;

    listSupporterRanking = res;

    if (res.data != null) {
        page++;

        // triggered func addDataRanking()
        $UI.trigger('needload', {
            page: page
        });

    }

    for (let i = 0; i < res.data.supporters.length; i++) {
        res.data.supporters[i]._total_money = changeMoneyFormat.moneyFormat(res.data.supporters[i].gatheredDonation);
    }
    

    res.getLanguage = utils.getLanguage();
    $UI.append(mainTpl(res));

    commonFooter.init({});
    fastclick.attach(document.body);
    clickHandle(res);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensors
    // sensorsActive.init();
};

obj.addDataRanking = function(res) {

    // push new data to existing data
    for (let index = 0; index < res.data.supporters.length; index++) {
        listSupporterRanking.data.supporters.push(res.data.supporters[index]);
    }

    // add total money and avatar for html
    for (let i = 0; i < listSupporterRanking.data.supporters.length; i++) {
        listSupporterRanking.data.supporters[i]._total_money = changeMoneyFormat.moneyFormat(listSupporterRanking.data.supporters[i].gatheredDonation);
    }

    if (listSupporterRanking.data != null) {
        page++;

        // triggered func addDataRanking()
        $UI.trigger('needload', {
            page: page
        });

    }

    $UI.html(mainTpl(listSupporterRanking));
};

// disabled self ranking
// obj.appendSelfRankingTpl = function(rs) {
//     rs.lang = lang;
//     rs.data._total_money = changeMoneyFormat.moneyFormat(rs.data.total_money);
//     rs.data.avatar = utils.imageChoose(rs.data.avatar);

//     $('.self-ranking').append(_selfRankingTpl(rs));
// }

function clickHandle(res) {
    // 分享
    $UI.on('click', '.improveBtn', function() {
        let paramObj = {
            item_id: project_id,
            item_type: 1,
            item_short_link: short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'supporterRanking.html' //google analytics need distinguish the page name
        }

        // 分享弹窗
        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // 分享组件
            share.init(paramObj);
        }
    })

    // share
    $UI.on('click', '#shareClose', function(e) {
        $('.common-share').css({
            'display': 'none'
        });
    });

    $UI.on('click', '.ques', function() {
        console.log('ques', $('.quesAlertBox').length);

        if (!$('.quesAlertBox').length) {
            $UI.append(alertTpl(res));
        }
        $('.quesAlertBox').css('display', 'block');
    })

    $UI.on('click', '.yes', function() {
        $('.quesAlertBox').css('display', 'none');
    })
}

export default obj;