var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'modifyGoal',
    htmlFileURL: 'html/modifyGoal.html',
    appDir: 'js/modifyGoal',
    uglify: true,
    hash: '',
    mode: 'production'
})