import view from './view'
import model from './model'
import 'loading'
import '../less/modifyGoal.less'
// 引入依赖
import utils from 'utils'
/* translation */
import modifyGoal from 'modifyGoal'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyGoal);

// 隐藏loading
utils.hideLoading();
var UI = view.UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];

getProjectInfo();

function getProjectInfo() {
    if (project_id) {
        model.getProjectInfo({
            param: {
                project_id: project_id
            },
            success: function(res) {
                if (res.code == 0) {
                    init(res);
                    // console.log('getProjectInfo', res);
                } else {
                    utils.alertMessage(res.msg)
                }
            },
            error: utils.handleFail
        })
    }
};

function init(projInfo) {
    console.log('projInfo', projInfo);

    UI.on('submit', function(e, objData) {
        var t = parseFloat(objData);
        model.postData({
            param: {
                project_id: project_id,
                target_amount: t
            },

            success: function(res) {
                if (res.code == 0) {
                    utils.hideLoading();

                    if (projInfo.data.verify_state == 3) {
                        utils.alertMessage(lang.lang7);
                    } else {
                        utils.alertMessage(lang.lang8);
                    }

                } else {
                    utils.alertMessage(res.msg);
                }
                // console.log('init', res);
            },
            error: utils.handleFail
        })
    });
    view.init(projInfo);

}