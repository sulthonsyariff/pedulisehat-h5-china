import mainTpl from '../tpl/modifyGoal.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import utils from 'utils'
import fastclick from 'fastclick'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import modifyGoal from 'modifyGoal'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(modifyGoal);
/* translation */

let total_amount;
let [obj, $UI] = [{}, $('body')];
var reqObj = utils.getRequestParams();
var short_link = reqObj['short_link'];

obj.UI = $UI;


obj.init = function(rs) {
    rs.JumpName = titleLang.goal;
    commonNav.init(rs);
    $('title').html(titleLang.goal);

    rs.lang = lang;
    $UI.append(mainTpl(rs));

    $('#input_money').val(changeMoneyFormat.moneyFormat(rs.data.target_amount));

    fastclick.attach(document.body);
    obj.event(rs);
    changeMoneyFormat.init('#input_money');

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};

obj.event = function(rs) {
    rs.lang = lang;

    $('body').on('click', '.saveBtn', function(e) {
        e.preventDefault();
        var targetNum = parseInt($('#input_money').val().replace(/\./g, '')) || '';

        // console.log('targetNum', targetNum)

        if (targetNum == '' || targetNum == 0) {
            utils.alertMessage(lang.lang5);
            return false;

        } else if (targetNum % 10000 != 0) {
            utils.alertMessage(lang.lang6);
            return false;

        } else {
            total_amount = Number(targetNum);
            $UI.trigger('submit', [targetNum]);
        }
        setTimeout(function() {
            location.href = '/' + short_link;
        }, 1000)
    });

};


export default obj;