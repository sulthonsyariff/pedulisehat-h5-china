import view from './view'
import model from './model'
import 'loading'
import '../less/update.less'
// 引入依赖
import utils from 'utils'

let short_link = utils.getRequestParams().short_link;

// 隐藏loading
utils.hideLoading();
var UI = view.UI;

view.init({});

UI.on('submit', function(e, objData) {
    model.postData({
        param: objData,
        success: function(res) {
            if (res.code == 0) {
                view.removeCache(); //清空本地存储
                utils.hideLoading();
                setTimeout(function() {
                    location.href = '/' + short_link;

                }, 500)
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});