/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);

    if (param.content == '') {
        utils.alertMessage(lang.lang7);
        return false;
    }

    // if (param.images.length < 1) {
    //     utils.alertMessage(lang.lang10);

    //     return false;
    // }

    //finish upload or not
    var imgs = param.images;
    for (var i = 0; i < imgs.length; i++) {
        if (!imgs[i].thumb || !imgs[i].image) {
            utils.alertMessage(lang.lang9);
            return false;
        }
    }

    return true;
}

export default obj;