import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getProjectInfo = function(o) {
    var url = domainName.project + '/v1/project/detail?project_id=' + o.param.project_id;
    // console.log('PROJ',o.param.project_id)
    ajaxProxy.ajax({
        url: url,
        type: 'get',
    }, o)
};

obj.postData = function(o) {
    var url = domainName.project + '/v1/project/dynamic/update';
    if (isLocal) {
        url = 'mock/initiateInfo.json'
    }
    // console.log('o.param',o.param)
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

export default obj;