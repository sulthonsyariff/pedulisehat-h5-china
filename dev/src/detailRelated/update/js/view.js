// 引入公共库
import mainTpl from '../tpl/update.juicer'
import commonNav from 'commonNav'
import utils from 'utils'
import imgUploader from 'uploadCloudinaryMore'
import fastclick from 'fastclick'
import picCover from './_picCover'
import store from 'store'
import 'fancybox'
import hideOrShowInputButton from './_hideOrShowInputButton' //Control image upload button to show or hide
import validate from './_validate'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import update from 'update'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(update);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
// 本地缓存KEY
let CACHED_KEY = 'update_info';
var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];

/* upload img */
let uploadWrapper = '#uploadWrapper';
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number
let datas = store.get(CACHED_KEY);
/* upload img */

// console.log('lang=', lang)

obj.init = function(rs) {
    rs.JumpName = titleLang.update;
    commonNav.init(rs);
    $('title').html(titleLang.update);

    datas = datas ? datas : {};
    datas.lang = lang;
    $UI.append(mainTpl(datas)); //get data from the local cache

    //从本地缓存中读取图片
    if (datas && datas.images) {
        imgUploader.setImageList(uploadList, uploadKey, datas.images);
    }


    $('.fancybox').fancybox();
    fastclick.attach(document.body);

    setupUIHandler(); //Main Js
    getTextareaParams(); //获取文本框字数

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();

};

// remove the cached project infomation
obj.removeCache = function() {
    store.remove(CACHED_KEY);
}

// update local caches
obj.updateCache = function() {
    updateCache();
}

// 主逻辑
function setupUIHandler() {
    //initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler, uploadWrapper);

    // set cover
    picCover.setCover(lang);
    hideOrShowInputButton.init(); //show or hide upload btn


    // 监听文本框，本地存储数据
    $('.input_story').on('input selected', function(e) {
        updateCache();
    });

    // 监听输入框内容， 获取输入框字数
    $(document).on('input propertychange', '.input_story', function(e) {
        getTextareaParams();
    });

    // 提交
    $('.saveBtn').on('click', function() {
        let submitData = getSubmitData();

        if (validate.check(submitData, lang)) {
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang6);
        }
    })

}
// 获取提交数据
function getSubmitData() {
    return {
        project_id: project_id,
        content: $("textarea[name=story]").val(), //备注：$('#story').val()不能获取到值？？？
        images: imgUploader.getImageList(uploadList, uploadKey)
    }
}

// //Update local cache
function updateCache() {
    store.set(CACHED_KEY, getSubmitData());
}

// img changed 监听事件
function imgChangedHandler() {
    // console.log('initiate info img changed');
    hideOrShowInputButton.init();

    imgUploader.refreshPictureNumber(uploadList);
    updateCache();
    $('.fancybox').fancybox();
    //set cover
    picCover.setCover(lang);
}

function getTextareaParams() {
    var num;
    num = $('.input_story').val().length;
    // console.log('num-125', num)
    if (num <= 5000) {
        num = num;
    } else {
        $(num).substr(0, 5000)
    }
    $(".contentcount").html(num)
}



export default obj;