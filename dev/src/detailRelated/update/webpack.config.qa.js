var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'update',
    htmlFileURL: 'html/update.html',
    appDir: 'js/update',
    uglify: true,
    hash: '',
    mode: 'development'
})