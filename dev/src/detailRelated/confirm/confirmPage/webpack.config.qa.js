
var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'confirmPage',
    htmlFileURL: 'html/confirmPage.html',
    appDir: 'js/confirmPage',
    uglify: true,
    hash: '',
    mode: 'development'
})