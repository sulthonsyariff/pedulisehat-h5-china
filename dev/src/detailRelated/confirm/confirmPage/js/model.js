import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

console.log(ajaxProxy);

obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


obj.submit = function(params) {
    var url = domainName.project + '/v1/project_confirm';

    if (isLocal) {
        url = '/mock/patientVerification.json';
    }

    ajaxProxy.ajax({
        url: url,
        data: JSON.stringify(params.data),
        type: 'POST',
        dataType: 'json',
    }, params);
}

export default obj;