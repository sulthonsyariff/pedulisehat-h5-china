/*
 * 校验表单信息
 */
import utils from 'utils'


let obj = {};

obj.check = function(param,lang) {
    if (param.relation_type != param.relation_type) {
        utils.alertMessage(lang.lang21);
        return false;
    }
    if (param.name == '') {
        utils.alertMessage(lang.lang6);
        return false;
    }
    if (param.id_card.length != 16) {
        utils.alertMessage(lang.lang8);
        return false;
    }
    if (param.content == '') {
        utils.alertMessage(lang.lang10);
        return false;
    }

    return true;
}

export default obj;