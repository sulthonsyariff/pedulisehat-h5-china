// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/confirmPage.juicer'
import chooseRelationshipTpl from '../tpl/confirmRelationship.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import googleAnalytics from 'google.analytics'
import validate from './_validate' // 表单验证
// import sensorsActive from 'sensorsActive'

/* 翻译包 */
import confirmPage from 'confirmPage'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(confirmPage);
/* 翻译包 */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let from = reqObj['from'];
let short_link = reqObj['short_link'] || '';

/* 本地缓存的key */
let CACHED_KEY = 'confirm_info';
let datas = store.get(CACHED_KEY) ? store.get(CACHED_KEY) : {};

obj.init = function(rs) {
    // console.log('view:', rs);
    if (from == 'detail') {
        rs.commonNavGoToWhere = '/' + short_link;
    } else if (from == 'confirmationList') {
        rs.commonNavGoToWhere = '/confirmationList.html?project_id=' + project_id + '&short_link=' + short_link;
    }
    rs.JumpName = titleLang.confirmCampaign;
    commonNav.init(rs);
    $('title').html(titleLang.confirmCampaign);

    datas.lang = lang;
    $UI.append(mainTpl(datas || '')); // 从本地缓存中读取数据

    fastclick.attach(document.body);
    $UI.append(chooseRelationshipTpl(datas));
    selectRelationship(datas);
    setupUIHandler(); //主逻辑
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
    // sensorsActive.init();
    
};

/**
 * remove the cached project information
 */
obj.removeCache = function() {
    store.remove(CACHED_KEY);
}

/**
 * 更新本地缓存
 */
obj.updateCache = function() {
    updateCache();
}

/**
 *页面主逻辑
 */
function setupUIHandler() {

    // 输入框监听，本地存储数据
    $('input, select, textarea').on('input selected', function(e) {
        updateCache();
    });
    $('.relationship').bind("DOMNodeInserted", function(e) {
            updateCache();
        })
        // submit
    $('.submitBtn').on('click', function() {
        let submitData = getSubmitParam();
        // console.log('submitData', submitData)
        if (validate.check(submitData, lang)) {
            $UI.trigger('submit', [submitData]);
            utils.showLoading(lang.lang20);
        }
    });




}

function selectRelationship() {
    $('.relationshipAidedperson').on('click', function(rs) {
        $('.choose-relationship').css('display', 'block')
    })

    for (let i = 0; i < 8; i++) {
        $(".list-items").each(function() {
            let relationship = $(".relationship").text().replace(/\ +/g, "");
            if ($(this).text() == relationship) {
                $(this).addClass('choosed-relationship')
            }
        });
    }


    $('.close').on('click', function(rs) {
        $('.choose-relationship').css('display', 'none')
    })

    $('body').on('click', '.list-items', function(e) {

        $('.list-items').removeClass('choosed-relationship');
        $(this).addClass('choosed-relationship');

        setTimeout(() => {
            $('.choose-relationship').css('display', 'none')
        }, 100)

        $('.relationship').addClass('selected').html($(this).html())
            // console.log('updateCache:', getSubmitParam());

    })

    let _store = store.get('confirm_info') || {};

    if (_store.relation_type) {
        $('.relationship').css({
            "color": "#000",
            // "font-weight": "bold"
        })
    }
}

function getSubmitParam() {
    return {
        name: $('#real_name').val(),
        id_card: $('#ID_number').val(),
        content: $("textarea[name=confirmation_content]").val(), //备注：$('#story').val()不能获取到值？？？
        relation_type: parseInt($(".choosed-relationship").attr('data-value')),
        project_id: project_id
    }
}

/**
 * 更新本地缓存
 */
function updateCache() {
    store.set(CACHED_KEY, getSubmitParam());

}





export default obj;