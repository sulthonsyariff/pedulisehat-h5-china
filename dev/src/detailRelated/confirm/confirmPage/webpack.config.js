var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'confirmPage',
    htmlFileURL: 'html/confirmPage.html',
    appDir: 'js/confirmPage',
    uglify: true,
    hash: '',
    mode: 'production'
})