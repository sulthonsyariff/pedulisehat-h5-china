var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'confirmSuccessPage',
    htmlFileURL: 'html/confirmSuccessPage.html',
    appDir: 'js/confirmSuccessPage',
    uglify: true,
    hash: '',
    mode: 'development'
})