var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'confirmSuccessPage',
    htmlFileURL: 'html/confirmSuccessPage.html',
    appDir: 'js/confirmSuccessPage',
    uglify: true,
    hash: '',
    mode: 'production'
})