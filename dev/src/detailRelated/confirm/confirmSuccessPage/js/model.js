import ajaxProxy from 'ajaxProxy' //ajax 需要
import 'jq_cookie' //ajax cookie需要
import domainName from 'domainName' // 接口域名

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

// console.log(ajaxProxy);

obj.verify = function(o) {
    var url = domainName.passport + '/v1/login/verify';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;