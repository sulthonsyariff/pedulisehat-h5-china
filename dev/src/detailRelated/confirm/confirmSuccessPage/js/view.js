import mainTpl from '../tpl/confirmSuccessPage.juicer'
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import fastclick from 'fastclick'
import domainName from 'domainName'; //接口域名
import utils from 'utils'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* 翻译包 */
import confirmSuccessPage from 'confirmSuccessPage'
import qscLang from 'qscLang'
let lang = qscLang.init(confirmSuccessPage);
/* 翻译包 */

let [obj, $UI] = [{}, $('body')];
let short_link = utils.getRequestParams().short_link;

obj.UI = $UI;

obj.init = function(rs) {
    rs.JumpName = '';
    commonNav.init(rs);

    rs.domainName = domainName;
    rs.lang = lang;
    $UI.append(mainTpl(rs));
    commonFooter.init(rs);

    fastclick.attach(document.body);

    $('.viewBtn').on('click', function(e) {
        location.href = '/' + short_link;
    });
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // console.log('view:', rs);
    // sensorsActive.init();

};




export default obj;