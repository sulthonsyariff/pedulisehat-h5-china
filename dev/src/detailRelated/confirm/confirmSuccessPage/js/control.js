import view from './view'
import model from './model'
import 'loading'
import '../less/confirmSuccessPage.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.hideLoading();

/*
 * 判断是否登陆：登陆才能发布项目
 */
model.verify({
    success: function(rs) {
        // console.log(rs);
        if (rs.code == 0) {
            view.init({});
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
});