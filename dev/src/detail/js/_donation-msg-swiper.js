import donationMsgSwiperTpl from '../tpl/_donation-msg-swiper.juicer'
import changeMoneyFormat from 'changeMoneyFormat'
import domainName from 'domainName'; //port domain name
import utils from 'utils'

let obj = {};

obj.init = function (rs) {
    // let arr = [];

    rs.arr = [];

    // map 使用
    // rs.data.map(function (item, index) {
    // console.log(item, index, item.user_name);

    rs.data.map(function (item) {
        rs.domainName = domainName;
        item = JSON.parse(item);


        // user name    
        if (item.user_name.length > 5) {
            item.user_name = item.user_name.substring(0, 5) + "...";
        }
        // avatar
        if (item.avatar.indexOf('http') == 0) {
            item.avatar = utils.imageChoose(item.avatar)
        } else {
            item.avatar = domainName.static + '/img/avatar/' + item.avatar;
        }
        // money
        item.money = changeMoneyFormat.moneyFormat(item.money)

        rs.arr.push(item)
    })



    if (rs.data.length >= 3) {
        $('.donation-msg-swiper-wrap').append(donationMsgSwiperTpl(rs));
        scrollDonationList();
    }
    // setInterval(noticeUp('.scroll ul','-35px',500), 2000);
    // setInterval(
    //     function () {
    //         noticeUp('.scroll ul', '-35px', 500)
    //     }, 3000);

}


// $(function () {
//     // 调用 公告滚动函数
//     setInterval("noticeUp('.notice ul','-35px',500)", 2000);
// });


// function noticeUp(obj, top, time) {
//     console.log('noticeUp', obj, top, time)
//     $(obj).animate({
//         marginTop: top
//     }, time, function () {
//         $(this).css({
//             marginTop: "0"
//         }).find(":first").appendTo(this);
//     })
// }


function scrollDonationList() {
    var scrollDiv = $(".scroll")
    var $ul = scrollDiv.find("ul")
    var $li = scrollDiv.find("li")
    // console.log('$li', $li)
    var $length = $li.length
    var $liHeight = $li.height() + 16

    num = 0;
    if (scrollDiv.length == 0) {
        return;
    }

    if ($length > 1) {
        $ul.append($li.eq(0).clone());
        setInterval(function () {
            num++;
            // console.log('num---', num)
            $ul.addClass("animate").css("-webkit-transform", "translateY(-" + $liHeight * (num) + "px)");
            // console.log('scroll-height', $liHeight * (num))
            setTimeout(
                function () {
                    if (num == $length) {
                        $ul.removeClass("animate").css("-webkit-transform", "translateY(0)");
                        num = 0;
                    }
                }, 300);
        }, 3000);
    }
}
export default obj;