// import 'facebook.jssdk'
import view from './view'
import model from './model'
import 'jq_cookie' //ajax cookie

import 'loading'
import domainName from 'domainName'; //port domain name
// import love from './_love';
// import store from 'store'
// 引入依赖
import utils from 'utils'
import '../less/main.less'
import followHandle from './_followHandle'

// import _activityShareDonate from './_activity-share-donate' //活动弹窗
import vertificationInfo from './_vertificationInfo' //获取项目验证详情
import donationMsgSwiper from './_donation-msg-swiper'
import ngoActivity from './_ngoActivity'
import _getqurban from './_getqurban'
import _getpackage from './_getpackage'

import shareAccessCount from 'shareAccessCount' //分享数据上报

// import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'

let $UI = view.UI;
let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'] || '';
let from = reqObj['from'] || '';
// let short_link = utils.getRequestParams().short_link || '';
let short_link = utils.isShortLink() || '';
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';
let shareUrl = reqObj['short_url'] || sessionStorage.getItem('short_url') || '';

let is_follow;
let category_id;

// let donateJumpUrl = '';

// let activity_id = '';
// let isActive;
// let abTestName;
// let forwarding_default;
// let forwarding_desc;
// let shareUrl;
let campaign_name;
let campaign_category;

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let detailActivityLoginClose = sessionStorage.getItem('detail-activity-login-close');
let detailActivityBubbleOpened = sessionStorage.getItem('detail-activity-bubble-close');
// let judgeShareTrophies = sessionStorage.getItem('detail-share-trophies') ? sessionStorage.getItem('detail-share-trophies') : '';

/* translation */
import detail from 'detail'
import qscLang from 'qscLang'
let lang = qscLang.init(detail);
/* translation */

// console.log('==== 11111 statistics_link===', typeof (statistics_link));
if (statistics_link && statistics_link != 'null') {
    shareAccessCount.init(0); //分享数据上报
}

// get project detail data
model.getProjInfo({
    param: {
        project_id: project_id,
        short_link: short_link,
        transformation: 'c_fill,g_faces:center,f_auto,w_1.0,ar_16:9' //大图配置参数
    },
    success: function(rs) {
        if (rs.code == 0) {
            project_id = rs.data.project_id; // ???
            short_link = rs.data.short_link;
            isActive = rs.data.state; //项目状态
            category_id = rs.data.category_id;
            campaign_name = rs.data.name;
            campaign_category = rs.data.category_id;

            // console.log('category_id', category_id)
            // rs.donatedButtonAB = donatedButtonAB;
            rs.user_id = user_id; //判断是否登陆
            rs.detailActivityLoginClose = detailActivityLoginClose; //判断按钮是否点击
            rs.detailActivityBubbleOpened = detailActivityBubbleOpened; //判断按钮是否点击
            // judge用户是否是主态
            judgeOwer(rs);
            // 处理数据
            handleData(rs);

            // sensors.track('CampaignDetailView', {
            //     from: document.referrer,
            //     campaign_id: project_id,
            //     campaign_name: campaign_name,
            //     campaign_category: campaign_category,
            //     campaigner_id: rs.data.user_id,
            //     campaigner_name: rs.data.UserName
            // })

        } else {
            utils.alertMessage(rs.msg)
        }
    },

    error: utils.handleFail
})



// judge if login user_id == campaign user_id
function judgeOwer(rs) {
    // 未登录用户情况
    // let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
    let project_user_id = rs.data.user_id;

    if (user_id == project_user_id) {
        rs.data.is_ower = true;
    } else {
        rs.data.is_ower = false;
    }

    console.log(`user_id =${user_id},project_user_id=${project_user_id},rs.data.is_ower = ${rs.data.is_ower}`)

    // 首先初始化commonNav，share弹窗等
    view.init(rs);

    // 活动弹窗
    // showActivityShareDonate(rs);
}

function handleData(rs) {
    // 用户是否收藏项目
    is_follow = rs.data.is_follow;

    model.getUpdates({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(res) {
            if (res.code == 0) {
                // 注入update数据
                rs.data.update = res.data;
                // 注入share / pv / follow 数据
                getStatistics(rs);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    });

    return rs;
}

// 获取项目验证详情 :先加载主模版，再加载其他
function getVertificationInfo() {
    model.getProVerify({
        param: {
            project_id: project_id
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.lang = lang;
                rs.project_id = project_id;
                rs.short_link = short_link;
                vertificationInfo.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    });

}

// 企业用户赞助项目
function getProjectLink() {
    model.getProjectLink({
        param: {
            project_id: project_id
        },
        success: function(rs) {
            if (rs.code == 0) {
                view.getProjectLink(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    });
}

function getStatistics(rs) {
    // 新增share / pv / follow 数

    model.getStatistics({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(res) {
            // console.log('getStatistics', res)

            if (res.code == 0) {
                // rs.data.pv = res.data.pv;
                rs.data.share = res.data.share;
                rs.data.follow = res.data.follow;
                rs.data.images = JSON.parse(rs.data.images);

                //facebook分享添加meta头部信息
                if (rs.data.seo == null) {
                    utils.changeFbHead({
                        url: short_link ?
                            ("https:" + domainName.share + '/' + short_link) : ("https:" + domainName.project + "/v1/project_share_tpl?projectId=" + project_id),
                        title: rs.data.name,
                        description: rs.data.story.substr(0, 100),
                        image: rs.data.images[0] ? rs.data.images[0].image : ''
                    });    
                } else {
                    utils.changeFbHead({
                        url: short_link ?
                            ("https:" + domainName.share + '/' + short_link) : ("https:" + domainName.project + "/v1/project_share_tpl?projectId=" + project_id),
                        title: rs.data.seo.title,
                        description: rs.data.seo.description.substr(0, 100),
                        image: rs.data.seo.images
                    });
                }

                // 加载主模版
                view.insertData(rs);

                // load love share tips
                // changeActivitiesNum();

                // donation msg swiper
                getSwiperDonationMsg();
                // 企业用户赞助项目
                getProjectLink();

                // 普通 12 zakat 13 bulk 14 qurban 15
                if (category_id != 13 && category_id != 144 && category_id != 15) {
                    loadConfirmData(rs); // load confirm campaign
                    getSupporterRanking(); //load supporter ranking
                    getVertificationInfo(); //获取项目验证详情 审核信息 项目验证
                }

                // get supporter ranking new
                getSupporterRankingNew();

                // qurban已经结束了 2020-8-4
                // indsert qurban :普通 12 zakat 13 bulk 14 qurban 15
                if (category_id == 15) {
                    getqurban()
                }

                if (category_id == 18) {
                    getpackage()
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

/*
 * loadConfirmData
 */
function loadConfirmData(res) {
    model.confirm({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.is_ower = res.data.is_ower;
                // console.log('is_ower', rs)
                view.loadConfirmData(rs);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: function(rs) {

        }
    });
}

function getSupporterRanking() {
    model.getSupporterRanking({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('getSupporterRanking===', res);
                view.getSupporterRanking(res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

function getSupporterRankingNew() {
    model.getSupporterRankingNew({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                console.log('getSupporterRankingNew===', res);
                view.getSupporterRankingNew(res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

$UI.on('donate', function(e) {
    let shortUrl = shareUrl.split("_")[0];
    location.href = '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link + (shortUrl ? ('&short_url=' + shortUrl) : '') + (from ? ('&from=' + from) : '') + (statistics_link ? ('&statistics_link=' + statistics_link) : '');
})

// 点击关注/取消关注
$UI.on('following', function(e, $this) {
    if (is_follow) {
        // 取消关注
        model.ProjUserUnFollow({
            param: {
                project_id: project_id,
                short_link: short_link
            },
            success: function(rs) {
                followHandleSuccess(rs, $this)
                is_follow = false
            },
            unauthorizeTodo: function(rs) {
                //登陆页
                location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/login?req_code=200' : '/login.html?qf_redirect=' + encodeURIComponent(location.href);
            },
            error: utils.handleFail
        })
    } else {
        // 关注
        model.ProjUserFollow({
            param: {
                project_id: project_id,
                short_link: short_link
            },
            success: function(rs) {
                followHandleSuccess(rs, $this)
                is_follow = true
            },
            unauthorizeTodo: function(rs) {
                //登陆页
                location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/login?req_code=200' : '/login.html?qf_redirect=' + encodeURIComponent(location.href);
            },
            error: utils.handleFail
        })
    }
})

function followHandleSuccess(rs, $this) {
    if (rs.code == 0) {
        followHandle.toggleFollow($this);
    } else {
        utils.alertMessage(rs.msg);
    }
}

// 用户关闭项目
$UI.on('closeProject', function(e) {
    model.closeProject({
        param: {
            project_id: project_id,
            short_link: short_link
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.lang = lang
                utils.alertMessage(rs.lang.lang24);
                location.reload();
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    });
});

// scroll donate list
$UI.on('needload', function(e, page, limit) {
    model.getDonationsList({
        param: {
            project_id: project_id,
            short_link: short_link,
            page: page
        },
        success: function(res) {
            if (res.code == 0) {
                res.limit = limit || ''; //标记页面捐款者列表
                view.insertDonations(res);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
});

/*
监听项目详情分享成功回调
*/

$UI.on('projectShareSuccess', function(e, target_type) {
    console.log('===projectShareSuccess===');
    shareActionCount(target_type);
});

/*
    TargetTypefacebook = 1 // facebook
    TargetTypeWhatsApp = 2 // whatsapp
    TargetTypeTwitter = 3 // twitter
    TargetTypeLink = 4 // link
    TargetTypeLine = 5 // line
    TargetTypeInstagram = 6 // instagram
    TargetTypeYoutube = 7 // youtube

分享接口 {
    "target_type": 1,
    "project_id": "14221049989731189491"
}

target_type	Y	int	分享目标
item_id	Y	string	项目ID
item_type	y	int	产品类型
scene	N	int	分享场景	1:表示捐赠/付款前的分享 2:表示捐赠/付款成功后的分享 参数不传 默认为1
statistics_link	N	string	分享链接
terminal Y	string	终端类型
client_id	Y	string	客户端ID
*/
// 修改dom分享数
function shareActionCount(target_type) {
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: project_id,
            item_type: 1,
            scene: 1,
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位
        },
        success: function(res) {
            if (res.code == 0) {

                // facebook 分享成功后动画 !!!
                // if (target_type == 1) {

                // sensors.track('ShareMethod', {
                //     campaign_id: project_id,
                //     campaign_name: campaign_name,
                //     campaign_category: campaign_category,
                //     share_type: target_type
                // })

                // res.lang = lang;
                // love.loveInit(res); //分享成功动画/吐丝提示
                // changeActivitiesNum(); // 更新活动列表


                // view.shareSuccessRefreshDonationList(); //刷新捐款列表
                // changeCurrentMoney(); //刷新已筹款金额
                // }

                changeShare(); // 更新分享数

                // let trophiesParam = {}
                // trophiesParam.isDonatedOrshare = 'share'

                // hide ramdan code
                // ramadanTrophiesAlertBox.init({
                //     isDonatedOrshare: 'share',
                //     fromWhichPage: 'detail.html' //google analytics need distinguish the page name
                // });

            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}

// 更新分享数
function changeShare() {
    model.getStatistics({
        param: {
            project_id: project_id,
        },
        success: function(res) {
            if (res.code == 0) {
                view.changeShareNum(res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

// 刷新已筹款金额
// function changeCurrentMoney() {
//     model.getProjInfo({
//         param: {
//             project_id: project_id
//         },
//         success: function(rs) {
//             if (rs.code == 0) {
//                 view.changeCurrentMoney(rs); //刷新已筹款金额
//             } else {
//                 utils.alertMessage(rs.msg)
//             }
//         },

//         error: utils.handleFail
//     })
// }


// function changeActivitiesNum() {
//     // 监听活动
//     model.activities({
//         param: {
//             project_id: project_id
//         },
//         success: function(res) {
//             if (res.code == 0) {
//                 res.lang = lang;

//                 activity_id = res.data ? res.data[0].activity_id : '';
//                 love.changeActivitiesNum(res);

//             } else {
//                 utils.alertMessage(rs.msg);
//             }
//         },
//         error: utils.handleFail
//     })
// }

//捐款信息轮播
function getSwiperDonationMsg(rs) {
    model.getSwiperDonationMsg({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                res.lang = lang;
                //公告轮播
                res.category_id = category_id;
                if (res.data) {
                    donationMsgSwiper.init(res);
                }
                getActivityCard();
            }
        }
    })
}

// 活动卡片
function getActivityCard(rs) {
    model.getActivityCard({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                if (res.data) {
                    // ngo activity
                    ngoActivity.init(res);
                }
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    })
}

function getqurban() {
    model.getqurban({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                if (res.data) {
                    console.log('****111****');
                    _getqurban.init(res);
                }
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    })
}

function getpackage() {
    model.getPackageItems({
        param: {
            project_id: project_id
        },
        success: function(res) {
            if (res.code == 0) {
                if (res.data) {
                    console.log('****999****');
                    _getpackage.init(res);
                }
            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    })
}


//hide Andriod loading '...'
if (utils.browserVersion.android) {
    location.href = 'native://hideLoading';
}

/*
    android:
    req_code = 100: android login success and jump to donate
    req_code = 200: android login success and reload detail (是否主客态 / 是否关注， 故要刷新)
    req_code = 300 android login success and jump to confirmPage
    req_code = 400: android refresh token success
*/
window.appHandle = function(req_code) {
    if (req_code == 100) {
        location.href = location.origin + '/donateMoney.html?project_id=' + project_id + '&short_link=' + short_link + (from ? ('&from=' + from) : '');
    } else if (req_code == 200) {
        location.reload();
    } else if (req_code == 300) {
        this.location.href = location.origin + '/confirmPage.html?project_id=' + project_id + '&short_link=' + short_link;
    }
}

// 监听分享成功回调
window.changeShareNum = function(target_type) {
    shareActionCount(target_type);
}

// 活动弹窗
// function showActivityShareDonate(rs) {
//     //  获取用户是否参加活动
//     model.isUserJoinDonateShare({
//         param: {
//             project_id: project_id,
//         },
//         success: function(res) {
//             if (res.code == 0) {
//                 res.lang = lang;
//                 res.data.state = rs.data.state; //项目状态
//                 res.data.donatedButtonAB = rs.donatedButtonAB;

//                 _activityShareDonate.showActivityShareDonate(res);

//             } else {
//                 utils.alertMessage(res.msg)
//             }
//         },
//         error: utils.handleFail
//     });
// }

// //获取分享是否有配捐
// $UI.on('isShareCanShareDonate', function() {
//     model.isShareCanShareDonate({
//         param: {
//             project_id: project_id,
//             target_type: 1 // 1 facebook
//         },
//         success: function(res) {
//             if (res.code == 0) {

//                 let meta_fb = '<meta property="og:url-fb" content="' + res.data.url + '">';
//                 if (!$("[property='og:url-fb']").length) {
//                     $('head').prepend(meta_fb);
//                 } else {
//                     $("[property='og:url-fb']").attr('content', res.data.url);
//                 }

//             } else {
//                 utils.alertMessage(rs.msg);
//             }
//         },
//         error: utils.handleFail
//     });
// });
