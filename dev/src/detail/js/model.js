import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName'; //port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/verify.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

/**
 * 获取项目信息 yangcheng
 */
obj.getProjInfo = function(o) {
    let url = domainName.project + '/v1/project/detail?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link + '&transformation=' + o.param.transformation;
    if (isLocal) {
        url = '../mock/v1_project_detail.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}

/*****
 * 获取支持者排行
 */
obj.getSupporterRanking = function(o) {
    let url = domainName.share + '/v1/share/supporter/ranking?project_id=' + o.param.project_id;

    if (isLocal) {
        url = '../mock/v1_share_supporter_ranking.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/*****
 * get supporter ranking new
 */
 obj.getSupporterRankingNew = function(o) {
    let url = domainName.share + '/v1/share/list_supporter_per_campaign?project_id=' + o.param.project_id + '&page=1';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


/**
 * 获取统计信息 yangcheng
 */
obj.getStatistics = function(o) {
    let url = domainName.project + '/v1/statistics?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link;
    if (isLocal) {
        url = 'mock/statistics.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o)
}

/**
 * 获取项目更新信息 yangcheng
 */
obj.getUpdates = function(o) {
        let url = domainName.project + '/v1/project/dynamic/update/detail?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link;
        if (isLocal) {
            url = 'mock/latestUpdates.json'
        }
        ajaxProxy.ajax({
            type: 'get',
            url: url,
            dataType: 'json'
        }, o)
    }
    /**
     * 用户点击关注 yangcheng
     */
obj.ProjUserFollow = function(o) {
    let url = domainName.project + '/v1/project_user_follow';
    if (isLocal) {
        url = 'mock/statistics.json'
    }

    ajaxProxy.ajax({
        type: 'post',
        url: url,
        // data:project_id,
        data: JSON.stringify(o.param),
    }, o, 'unauthorizeTodo')
}

/**
 * 用户点击取消关注 yangcheng
 */
obj.ProjUserUnFollow = function(o) {
    let url = domainName.project + '/v1/project_user_follow/' + o.param.project_id;
    if (isLocal) {
        url = 'mock/statistics.json'
    }

    ajaxProxy.ajax({
        type: 'delete',
        url: url,
        // data:project_id,
        data: JSON.stringify(o.param),
    }, o, 'unauthorizeTodo')
}

/**
 * 用户关闭项目 yangcheng
 */
obj.closeProject = function(o) {
    let url = domainName.project + '/v1/project/' + o.param.project_id + '/stop';
    ajaxProxy.ajax({
        type: 'PUT',
        url: url
    }, o)
}

//get donations List
obj.getDonationsList = function(o) {
    let url = domainName.trade + '/v1/support?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link + '&page=' + o.param.page;

    // if (1) {
    //     url = '../mock/v1_support' + '_page_' + o.param.page + '.json'
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}

/*
{
    "target_type": 1,
    "project_id": "14221049989731189491"
}

TargetTypefacebook = 1 // facebook
TargetTypeWhatsApp = 2 // whatsapp
TargetTypeTwitter = 3 // twitter
TargetTypeLink = 4 // link
TargetTypeLine = 5 // line
TargetTypeInstagram = 6 // instagram
TargetTypeYoutube = 7 // youtube
*/
//旧分享统计为安卓保留 1.19版本以前
// obj.projectShare = function (o) {
//     let url = domainName.project + '/v1/project_share';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }

//新分享统计接口
obj.newProjectShare = function(o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//获取分享相关内容
obj.getShareInfo = function(o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

// //分享数据上报
// obj.shareAccessCount = function (o) {
//     let url = domainName.share + '/v1/share_access_count';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }

// obj.pvView = function(o) {
//     let url = domainName.project + '/v1/project_pv';
//     ajaxProxy.ajax({
//         type: 'post',
//         url: url,
//         data: JSON.stringify(o.param),
//     }, o)
// }

/*
 * project confirm
 1 Relative
 2 Friend
 3 Neighbor
 4 Clleague
 5 Teacher
 6 Schoolmate
 7 Volunteer
 8 Other

 */
obj.confirm = function(o) {
    let url = domainName.project + '/v1/project_confirm?project_id=' + o.param.project_id + '&short_link=' + o.param.short_link;
    if (isLocal) {
        url = 'mock/project_confirm.json'
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// 活动列表
obj.activities = function(o) {
    let url = domainName.project + '/v1/activities?project_id=' + o.param.project_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// 获取用户是否参加活动
obj.isUserJoinDonateShare = function(o) {
    let url = domainName.project + '/v1/activity_not_event?project_id=' + o.param.project_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}



// 获取分享是否有配捐
// obj.isShareCanShareDonate = function(o) {
//     let url = domainName.project + '/v1/activity_match_gift?project_id=' + o.param.project_id + '&target_type=' + o.param.target_type;

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url,
//     }, o)
// }

// 用户验证信息展示
obj.getProVerify = function(o) {
    let url = domainName.project + '/v1/project_verify/' + o.param.project_id + '/show';

    // if (1) {
    //     url = 'mock/v1_project_verify_$projectId_show.json'
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// 企业用户赞助项目
obj.getProjectLink = function(o) {
    let url = domainName.project + '/v1/project_link?project_id=' + o.param.project_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// 捐款信息轮播
obj.getSwiperDonationMsg = function(o) {
    let url = domainName.trade + '/v1/donated_carousels?limit=200&project_id=' + o.param.project_id;

    // if (1) {
    //     url = 'mock/get_swiper_donation_msg.json'
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// 活动卡片接口
obj.getActivityCard = function(o) {
    let url = domainName.activity + '/v1/prr/partner_card?project_id=' + o.param.project_id;

    // if (1) {
    //     url = 'mock/v1_prr_partner_card.json'
    // }


    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// qurban
obj.getqurban = function(o) {
    let url = domainName.project + '/v1/project_category/' + o.param.project_id + '/qurban';

    if (isLocal) {
        url = '../mock/v1_project_category_qurban.json'
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

obj.getPackageItems = function(o) {
    let url = domainName.project + '/v2/package_list/' + o.param.project_id;
    ajaxProxy.ajax({
        type: 'get',
        url: url,
        dataType: 'json'
    }, o, 'unauthorizeTodo')
}


// 捐款信息ab
obj.abTest = function(o) {
    let url = domainName.base + '/v1/ab/template_total/' + o.param.name;

    // if (1) {
    //     url = 'mock/get_swiper_donation_msg.json'
    // }

    ajaxProxy.ajax({
        type: 'put',
        url: url,
    }, o)
}

// 捐款信息ab
obj.abFirstTest = function(o) {
    let url = domainName.base + '/v1/ab/template_total_random/' + o.param.name;
    // if (1) {
    //     url = 'mock/get_swiper_donation_msg.json'
    // }

    ajaxProxy.ajax({
        type: 'put',
        url: url,
    }, o)
}

// 获取分享trophies
obj.getShareMedal = function(o) {

    let url = domainName.psuic + '/v1/ramadan/medal/share';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}


export default obj;