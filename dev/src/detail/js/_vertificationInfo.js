import vertificationInforTpl from '../tpl/_vertification-infor.juicer'
import campaignCommitmentTpl from '../tpl/_campaign-commitment.juicer'

let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;

/*
*
state :
state = 0 / 1 未提交资料
state = 2 审核中
state = 3 审核通过

payee_type :
1 患者
2 直系亲属
3 夫妻
4 公益机构
5 医院
*/
obj.init = function(rs) {
    // console.log('get vertification info=', rs, $('.vertification-infor').length);
    $('.vertification-infor').append(vertificationInforTpl(rs));

    $('.in-progress-text').html(rs.data.text.replace(/\n|\r\n/g, '<br/>'));

    // campaigner's commitment
    $UI.on('click', '#commitment', function() {
        if (!$('.campaign-commitment').length) {
            $UI.append(campaignCommitmentTpl(rs));
        } else {
            $('.campaign-commitment').css('display', 'block');
        }

    });

    // got it
    $UI.on('click', '#gotIt', function() {
        $('.campaign-commitment').css('display', 'none');
    })

    // go-to-vertification
    $UI.on('click', '.go-to-vertification', function() {
        location.href = '/patientVerification.html?project_id=' + rs.project_id + '&short_link=' + rs.short_link + '&from=detail';
    })
}

export default obj;