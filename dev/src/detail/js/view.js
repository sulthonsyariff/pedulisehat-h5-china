// 公共库
import 'circleProgress'
import 'fancybox'
import commonNav from 'commonNavDetail'
import qscScroll_timestamp from 'qscScroll_timestamp_donate_popup' //弹窗滚动

import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics' //google analystic
import domainName from 'domainName'; //port domain name

import mainTpl from '../tpl/main.juicer'
import donationsTpl from '../tpl/_donationItem.juicer'
import alertBoxTpl from '../tpl/_alert-box.juicer' //关闭项目弹窗
// import swiperTpl from '../tpl/_swiper.juicer' //swiper
import confirmCampaignTpl from '../tpl/_confirm-campaign.juicer' //confirm campaign
// import activityShareDonateTpl from '../tpl/_activity-share-donate.juicer' //配捐活动展示弹窗
import mediaPartnerTpl from '../tpl/_media-partner.juicer' //企业用户赞助项目
import supporterRankingTpl from '../tpl/_supporter-ranking.juicer' //支持者排行
import supporterRankingTplNew from '../tpl/_supporter-ranking-new.juicer' //supporter ranking new

// 业务代码
import timeHandle from './_timeHandle'
import clickHandle from './_clickHandle'; //所有点击事件
import utils from 'utils';
import goBackWhere from './_goBackWhere.js'
// // import sensorsActive from 'sensorsActive'
import urgentListComponents from 'urgentListComponents'
import FinishCampaignPopup from 'FinishCampaignPopup'

/* translation */
import detail from 'detail'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(detail);
/* translation */

import promoPopup from '../../../common/promoPopup/js/main'

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let page = 0;
let is_first = true;
let scroll_list = new qscScroll_timestamp();

let [obj, $UI] = [{}, $('body')];
let url = location.href;
obj.UI = $UI;

// 初始化
obj.init = function(rs) {
    // commonNav初始化
    rs.JumpName = titleLang.detail;
    $('title').html(titleLang.detail);

    if (document.referrer.indexOf('mediaPartner') != -1) {
        // console.log('===document.referrer===', document.referrer);
    } else {
        rs.commonNavGoToWhere = goBackWhere.init();
    }

    commonNav.init(rs);

    rs.lang = lang;
    rs.url = url;
    $UI.append(alertBoxTpl(rs)); //'关闭项目'的弹窗

    clickHandle.init(rs); //点击事件
    // // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // 1.5.9 已结束项目弹窗 Finish Campaign Popup
    // 客态 + 项目成功/失败的项目详情页弹出此弹窗 512 / 8
    if (!rs.data.is_ower && (rs.data.state == 512 || rs.data.state == 8 || timeHandle.timeLeft(rs.data.closed_at) < 0)) {
        FinishCampaignPopup.init({
            project_id: rs.data.project_id,
            fromWhichPage: 'detail.html',
            lang: lang
        });
    }
    
    promoPopup.init()

};

// 插入数据 yangcheng
obj.insertData = function(rs) {
    //时间
    // rs.data.timePast = timeHandle.raiseDay(rs.data.created_at);
    if (rs.data.closed_at) {
        rs.data.timeLeft = timeHandle.timeLeft(rs.data.closed_at);
    }

    rs.data.createdTime = timeHandle.userDate(rs.data.created_at);

    if (rs.data.crop_img_url) {
        rs.data.crop_img_url = utils.imageChoose16_9(rs.data.crop_img_url);
    }

    for (let i = 0; i < rs.data.images.length; i++) {
        let detailRightUrl = utils.imageChoose(rs.data.images[i])
        rs.data.images[i].detailRightUrl = detailRightUrl
            // console.log('detailRightUrl', detailRightUrl)
        let oldDetailAddOverlay = utils.addOverlay(rs.data.images[i])
        rs.data.images[i].oldDetailAddOverlay = oldDetailAddOverlay
    }

    //更新时间
    if (rs.data.update && rs.data.update.created_at != null && rs.data.update.images != "") {
        rs.data.update.images = JSON.parse(rs.data.update.images)

        for (let i = 0; i < rs.data.update.images.length; i++) {
            let rightUrl = utils.imageChoose(rs.data.update.images[i])
            let oldUpdateAddOverlay = utils.addOverlay(rs.data.update.images[i])
            rs.data.update.images[i].rightUrl = rightUrl
            rs.data.update.images[i].oldUpdateAddOverlay = oldUpdateAddOverlay
        }
    }

    if (rs.data.update && rs.data.update.content) {
        rs.data.update.updatesHours = timeHandle.updateHours(rs.data.update.created_at);
        // console.log('updateHours', rs.data.update.updatesHours)
        rs.data.update.updatesTime = timeHandle.userDate(rs.data.update.created_at);

        rs.data.update.updatesMinutes = timeHandle.updateMinutes(rs.data.update.created_at);

    }

    // target_amount
    rs.data._target_amount = changeMoneyFormat.moneyFormat(rs.data.target_amount);
    rs.data._current_amount = changeMoneyFormat.moneyFormat(rs.data.current_amount);
    rs.data.progress = rs.data.current_amount / rs.data.target_amount * 100 + 1;

    // 企业用户
    rs.data.corner_mark = rs.data.corner_mark ? JSON.parse(rs.data.corner_mark) : '';

    rs.lang = lang;
    rs.getLanguage = utils.getLanguage();
    rs.project_id = project_id;


    // PACKAGE CAMPAIGN
    const packageCampaign = false;

    if (packageCampaign) {
        rs.data.category_id = 999;
    }

    $UI.append(mainTpl(rs)); //主模版

    // hide loading
    utils.hideLoading();

    uiHandle(rs); //页面样式调试
    loadDonateLists();

    $('.fancybox1').fancybox();
    $('.fancybox2').fancybox();

    // 详情页主态不展示推荐项目
    if (!rs.data.is_ower) {
        //Urgent Campaigns 详情最下面展示推荐项目
        urgentListComponents.init({
            project_id: rs.data.project_id,
            fromWhichPage: 'detail.html'
        });
    }

    // report campaign
    $('.report-campaign .text').html(lang.lang101);
    $('.report-campaign .wrap-button-report').html(lang.lang102);

    $UI.on('click', '.wrap-button-report', function() {
        if (utils.judgeDomain() == 'pre') {
            location.href = 'https://gtry-pre.pedulisehat.id/campaign-report/' + rs.data.project_id;
        } else {
            location.href = 'https://gtry.pedulisehat.id/campaign-report/' + rs.data.project_id;
        }        
    })
};

function uiHandle(rs) {
    // 监听页面滚动
    $(document).scroll(function() {
        let scroH = $(document).scrollTop(); //滚动高度
        let donateH = $('.donate-btn-big').offset().top

        if (donateH - scroH < 56) {
            $('.share-donate').css('display', 'block');
        } else {
            $('.share-donate').css('display', 'none');
        }
    })

    // banner
    $('.banner-img').height($('.banner-img').width() * 9 / 16)
        // $('.banner-img').attr('src', rs.data.crop_img_url)

    let detailH = $('.story .detail').height();
    let textH = $('.story .detail .detailText').height();
    // hide readmore
    if (textH <= detailH) {
        $('.moreOrPack').css('display', 'none');
    }

    if (rs.data.update.type == 1) {
        $('.updates-wrap').css('background', '#f8f8f8')
    }

    if (rs && rs.data && rs.data.avatar) {
        if (rs.data.avatar.indexOf('http') == 0) {
            $('.avatar-detail').attr('src', utils.imageChoose(rs.data.avatar));
        } else {
            $('.avatar-detail').attr('src', domainName.static + '/img/avatar/' + rs.data.avatar);
        }
    }
    if (rs && rs.data.update && rs.data.update.avatar) {
        if (rs.data.update.avatar.indexOf('http') == 0) {
            $('.avatar-update').attr('src', utils.imageChoose(rs.data.update.avatar));
        } else {
            $('.avatar-update').attr('src', domainName.static + '/img/avatar/' + rs.data.update.avatar);
        }
    }

    //textarea 换行问题
    $(".updates-detail").each(function() {
        let updates = $(this).text().replace(/\n|\r\n/g, '<br/>');
        $(this).html(updates);
    });
    $(".detailText").each(function() {
        let detailText = $(this).text().replace(/\n|\r\n/g, '<br/>');
        $(this).html(detailText);
    });

    // update name overflow
    $('.updates-wrapper .name').css('max-width', $('.right-part').width() * 0.95);

    // name-location
    let _parentW = $('.name-location .name').width()
    let _verifiedWrapW = $('.name-location .name').find('.verified-wrap').width()
    let _nameW = $('.name-location .name').find('.user-name').width()
    if (_nameW > _parentW - _verifiedWrapW - 10) {
        $('.name-location .name .user-name').css({
            'width': _parentW - _verifiedWrapW - 10,
            'overflow': 'hidden',
            'white-space': 'nowrap',
            'text-overflow': 'ellipsis',
        })
    }
}

//刷新已筹款金额
obj.changeCurrentMoney = function(rs) {
    $('.current-money-wrap ._money').html(changeMoneyFormat.moneyFormat(rs.data.current_amount));
}

// 更新分享数
obj.changeShareNum = function(res) {
    console.log('changeShareNum', res.data.share)
    $('.shares-num').html(res.data.share);
    // sessionStorage.setItem('detail-share-trophies', 'first');
    // $UI.trigger('alertTrophies')

}

// // trophies alert
// obj.trophiesAlert = function (res) {
//     res.data.isDonatedOrshare = 'share';
//     console.log('trophiesAlert',res)
//     ramadanTrophiesAlertBox.init(res.data);
//     $('.common-trophies').css('display', 'block')
//     // sessionStorage.setItem('detail-share-trophies', res.data.level);
// }

// 分享成功后，刷新捐款列表
// obj.shareSuccessRefreshDonationList = function() {
//     // refren donate
//     $UI.trigger('needload', [1, 3]); //加载前三个添加在页面中

//     // refresh donate popup lists
//     scroll_list.stop();
//     $('.donationsList').html('');
//     page = 0;
//     $('.loading').show();
//     scroll_list.run();
// }


function loadDonateLists() {
    // 确保仅加载一次
    if (page == 0) {
        $UI.trigger('needload', [1, 3]); //加载前三个添加在页面中
    }

    scroll_list.config({
        elDonatePopup: $('.donatePopup'), //弹窗
        wrapper: $('.donationsList-content'), //监听的dom滚动元素
        elContainer: $('.donatePopup .content'), //dom滚动元素父元素容器
        onNeedLoad: function() {
            // console.log('===scorll needload===');
            $UI.trigger('needload', [++page])
        }
    });
}

obj.insertDonations = function(rs) {
    console.log('---insertDonations---', rs.data.length);

    rs.domainName = domainName;
    rs.lang = lang;

    if (rs.data.length != 0) {
        is_first = false;
        for (let i = 0; i < rs.data.length; i++) {
            //时间
            rs.data[i].donateHours = timeHandle.updateHours(rs.data[i].created_at);
            rs.data[i].donateTime = timeHandle.userDate(rs.data[i].created_at);
            rs.data[i].donateMinutes = timeHandle.updateMinutes(rs.data[i].created_at);
            rs.data[i].raiseDay = timeHandle.raiseDay(rs.data[i].created_at);

            rs.data[i].money = changeMoneyFormat.moneyFormat(rs.data[i].money);

            if (rs.data[i].avatar.indexOf('http') == 0) {
                rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar)
            } else {
                rs.data[i].avatar = domainName.static + '/img/avatar/' + rs.data[i].avatar;
            }

            // 企业用户
            rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';
        }

        // 标记页面捐款者列表
        //点击按钮展示， 页面仅展示至多三条
        if (rs.limit) {
            if (rs.data.length > 3) {
                $('.donate-popup-btn').css('display', 'block')
            }

            rs.data = rs.data.slice(0, 3)
            $('.donationsList_show_3').append(donationsTpl(rs))
        } else {
            $('.donationsList').append(donationsTpl(rs));

            // let listH = 0;
            // $('.donatePopup').each(function() {
            //     listH += $(this).height();
            // })

            // $('.donatePopup .content').css({
            //     'height': listH + 65 > $(window).height() ? $(window).height() * 0.9 : listH + 65,
            // });
        }
        scroll_list.run();

        //textarea 换行问题
        $(".message-content").each(function() {
            let message = $(this).text().replace(/\n|\r\n/g, '<br/>');
            $(this).html(message);
        });

        // name overflow
        $('.name-money .name').css('max-width', ($('.donationsList').width() - 60) * 0.95);
    } else {
        console.log('===noData===');

        $('.loading').hide();
        if (is_first && rs.limit) {
            $('.donationsList_show_3').append(donationsTpl(rs)); //标记页面捐款者列表
        }
        scroll_list.stop()
    }
}

obj.loadConfirmData = function(rs) {
    rs.lang = lang;
    rs.domainName = domainName;
    // console.log('====', rs)

    if (rs.data) {
        for (let i = 0; i < rs.data.length; i++) {
            // 企业用户
            rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';
            rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar);
        }
    }

    $('.confirm-campaign').append(confirmCampaignTpl(rs));

    let $choosedCommentList = $(".comment-lists[data-number='0']");

    // first element control hide or show '...'
    if ($choosedCommentList.find('.comment p').height() > 42) {
        $choosedCommentList.find('.comment').addClass('overflow');
    }
}

// 企业用户赞助项目
obj.getProjectLink = function(rs) {
    rs.domainName = domainName;

    if (rs.data) {
        for (let i = 0; i < rs.data.length; i++) {
            // 企业用户
            rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';
            rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar)
        }
    }

    $('.media-partner-wrapper').append(mediaPartnerTpl(rs));
}

obj.getSupporterRanking = function(rs) {
    // disabled supporter ranking old

    let disabled = true;

    if (!disabled) {
        rs.lang = lang;
        if (rs && rs.data && rs.data.length > 0) {
            rs._data = rs.data.slice(0, 5);
    
            for (let i = 0; i < rs._data.length; i++) {
                rs._data[i].avatar = utils.imageChoose(rs._data[i].avatar)
            }
    
            $('.supporter-ranking').append(supporterRankingTpl(rs));
        } else {
            $('.supporter-ranking,.supporter-ranking-line').css('display', 'none');
        }
    }
}

obj.getSupporterRankingNew = function(rs) {
    rs.lang = lang;

    if (rs.data != null) {
        rs._data = rs.data.supporters.slice(0, 5);

        for (let i = 0; i < rs._data.length; i++) {
            rs._data[i].gatheredDonation = changeMoneyFormat.moneyFormat(rs._data[i].gatheredDonation);
        }

        $('.supporter-ranking-new').append(supporterRankingTplNew(rs));
    } else {
        $('.wrap-supporter-ranking-new').css('display', 'none');
    }
}

export default obj;