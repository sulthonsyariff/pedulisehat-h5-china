var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'detail',
    htmlFileURL: 'html/detail.html',
    appDir: 'js/detail',
    uglify: true,
    hash: '',
    mode: 'development'
})