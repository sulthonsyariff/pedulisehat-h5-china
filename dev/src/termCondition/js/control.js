import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
model.getTermCondition({
    success: function(res) {
        utils.hideLoading();
        view.init(res);
    },
})

$("[property='og:url']").attr('content', '123');
$("[property='og:title']").attr('content', '123');
$("[property='og:description']").attr('content', '123');
$("[property='og:image']").attr('content', "https://static-qa.pedulisehat.id/js/homePage/../../img/homePage/banner_home_74fd6b27.png");