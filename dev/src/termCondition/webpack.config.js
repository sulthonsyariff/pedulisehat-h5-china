var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'termCondition',
    htmlFileURL: 'html/termCondition.html',
    appDir: 'js/termCondition',
    uglify: true,
    hash: '',
    mode: 'production'
})