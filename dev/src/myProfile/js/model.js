import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    // if (1) {
    //   url = '../mock/userInfo.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

//用户等级
obj.getUserLevelPoints = function(o) {
    let url = domainName.psuic + '/v1/user_level';
    if (isLocal) {
        url = '../mock/userLevel.json';
    }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

//等级规则
obj.getLevelRules = function(o) {
    let url = domainName.psuic + '/v1/level_rules';
    // if (isLocal || 1) {
    //     url = '../mock/LevelRules.json';
    // }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

export default obj;