import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.showLoading();

model.getUserInfo({
    success: function(rs) {
        if (rs.code == 0) {

            view.init(rs);
            getLevelRules(rs); //level

        } else {
            utils.alertMessage(rs.msg)
        }
    },
    unauthorizeTodo: function(rs) {
        // 加载主模版
        view.init({});
    },
    error: utils.handleFail,
});

//获取等级规则
function getLevelRules(rs) {
    model.getLevelRules({
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                getUserLevelPoints(res);
                utils.hideLoading();
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}

//获取用户等级
function getUserLevelPoints(rs) {
    model.getUserLevelPoints({
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                res.data.levelRules = rs.data
                view.showLevel(res);
                utils.hideLoading();
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}