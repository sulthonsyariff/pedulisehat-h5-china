// 公共库
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils';
import domainName from 'domainName'
import navBottom from 'navBottom'
import clickHandle from './_clickHandle'
import levelTpl from '../tpl/_level.juicer'
import store from 'store'
// import saveH5Tips from 'saveH5Tips'

/* translation */
import common from 'common'
import qscLang from 'qscLang'
let lang = qscLang.init(common);
/* translation */

import promoPopup from '../../../common/promoPopup/js/main'


let [obj, $UI] = [{}, $('body')];

obj.UI = $UI;
fastclick.attach(document.body);

// 初始化
obj.init = function (rs) {
    rs.JumpName = 'page';
    rs.domainName = domainName;

    rs.lang = lang
    $UI.append(mainTpl(rs)); //主模版

    rs.fromWhichPage = 'myProfile.html';
    navBottom.init(rs, 5); //nav bottom bar

    clickHandle.init(rs);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    utils.hideLoading();

    // 设置avatar 头像
    if (rs && rs.data && rs.data.avatar) {
        // saveH5Tips.init(); //save h5 to screen
        $('.avatar-img').attr('src', utils.imageChoose(rs.data.avatar));
    }
    // name overflow
    $('.avatar-name .name').css('max-width', ($('.avatar-name').width() - 85) * 0.95);
    promoPopup.init()

};

obj.showLevel = function (res) {
    res.lang = lang
    console.log('level', res)
    user_points = res.data.total_points

    for (let i = 0; i < res.data.levelRules.length; i++) {

        if (user_points < res.data.levelRules[i].points) {
            res.level = i - 1
            res.title = res.data.levelRules[i - 1].name
            res.isHighest = false
            store.set('Level', {
                isHighest: false,
                level: res.level,
                current_points: user_points,
                next_level_points: res.data.levelRules[i].points,
                title: res.title,
                i,
                i
            })
            $('.name-level').append(levelTpl(res))

            return;
        } else if (user_points >= res.data.levelRules[res.data.levelRules.length - 1].points) {
            console.log('i', i)
            res.level = res.data.levelRules.length - 1
            res.title = res.data.levelRules[res.data.levelRules.length - 1].name
            res.isHighest = true
            store.set('Level', {
                isHighest: true,
                level: res.level,
                current_points: user_points,
                next_level_points: 50000,
                title: res.title,
                i,
                i
            })
            $('.name-level').append(levelTpl(res))

            return;
        }
    }
}

export default obj;