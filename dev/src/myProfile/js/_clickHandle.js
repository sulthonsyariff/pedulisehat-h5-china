import utils from 'utils';
import actionPromoPopup from '../../../common/promoPopup/js/_clickHandle'

let [obj, $UI] = [{}, $('body')];

obj.init = function(rs) {
    //  sign-in
    $('body').on('click', '.sign-in', function(e) {
        actionPromoPopup.click()
        $('.sign-in').removeClass('choosed');
        // console.log('signout===', $(this))
        $('.sign-in').addClass('choosed');
    });

    //myAccount
    $('body').on('click', '.avatar-name', function(e) {
        actionPromoPopup.click()
        location.href = '/myAccount.html'

    });

    // level
    $('body').on('click', '.level', function(e) {
        actionPromoPopup.click()
        e.stopPropagation()
        location.href = '/userLevel.html'
    })

    // myTrophies
    $('body').on('click', '.trophies', function(e) {
        actionPromoPopup.click()
        e.stopPropagation()
        location.href = '/myTrophies.html'
    })

    // list
    $('body').on('click', '.top-list', function(e) {
        actionPromoPopup.click()
        // myCampaigns
        if ($(this).hasClass('my-campaigns')) {
            location.href = '/myCampaigns.html';
        }

        // My Gotongroyong
        else if ($(this).hasClass('my-gtry')) {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id/gtry/closed/refund'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id/gtry/closed/refund'
            } else {
                location.href = 'https://gtry.pedulisehat.id/gtry/closed/refund'
            }
        }
        // my-insurance
        else if ($(this).hasClass('my-insurance')) {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://asuransi-qa.pedulisehat.id/myInsurance.html';
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://asuransi-pre.pedulisehat.id/myInsurance.html';
            } else {
                location.href = 'https://asuransi.pedulisehat.id/myInsurance.html';
            }
        }
    });

    $('body').on('click', '.middle-list', function(e) {
        actionPromoPopup.click()
        $(this).addClass('active');

        setTimeout(() => {
            $('.middle-list').removeClass('active');

            setTimeout(() => {
                // 页面跳转:页面中添加a标签跳转太快，高亮不显示
                // followed
                if ($(this).hasClass('campaigns-followed')) {
                    location.href = '/followedCampaigns.html';
                }
                // setting
                else if ($(this).hasClass('setting')) {
                    location.href = '/setting.html';

                }
                // aboutUs
                else if ($(this).hasClass('about')) {
                    location.href = '/aboutUs.html';
                }
                // help-center
                else if ($(this).hasClass('help-center')) {
                    location.href = '/faq.html';
                }

                // language
                else if ($(this).hasClass('language')) {
                    location.href = '/changeLanguage.html';
                }
            }, 100);

        }, 100);
    });

    $('body').on('click', '.wallet-list', function(e) {
        actionPromoPopup.click()
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://gtry-qa.pedulisehat.id/account/gopay'
        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://gtry-pre.pedulisehat.id/account/gopay'
        } else {
            location.href = 'https://gtry.pedulisehat.id/account/gopay'
        }
    });
}

export default obj;