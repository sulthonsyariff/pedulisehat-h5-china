var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'login',
    htmlFileURL: 'html/login.html',
    appDir: 'js/login',
    uglify: true,
    hash: '',
    mode: 'production'
})