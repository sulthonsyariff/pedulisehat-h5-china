var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'privacyPolicy',
    htmlFileURL: 'html/privacyPolicy.html',
    appDir: 'js/privacyPolicy',
    uglify: true,
    hash: '',
    mode: 'development'
})