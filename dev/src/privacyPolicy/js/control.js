import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
model.getPrivacyPolicy({
    success: function(res) {
        utils.hideLoading();
        view.init(res);
    },
})