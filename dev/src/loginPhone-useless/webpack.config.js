var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'loginPhone',
    htmlFileURL: 'html/loginPhone.html',
    appDir: 'js/loginPhone',
    uglify: true,
    hash: '',
    mode: 'production'
})