// 公共库
import fastclick from 'fastclick';
import commonNav from 'commonNav'
import mainTpl from '../tpl/loginPhone.juicer'
import domainName from 'domainName'; //port domain name
import utils from 'utils'
import validate from '../js/_validate'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'
import turingVerification from 'turingVerification'

/* translation */
import loginPhone from 'loginPhone'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(loginPhone);
/* translation */

let reqObj = utils.getRequestParams();
// 返回地址
// let qf_redirect = decodeURIComponent(reqObj["qf_redirect"]);

let [obj, $UI] = [{}, $('body')];
let isAgreePolicy = true; //是否同意协议
let CACHED_KEY = 'switchLanguage';
let Lang = store.get(CACHED_KEY);
obj.UI = $UI;

obj.init = function (rs) {
    rs.JumpName = lang.lang23;
    // rs.commonNavGoToWhere = qf_redirect;
    rs.domainName = domainName;
    rs.lang = lang;
    // console.log('view:', rs);

    commonNav.init(rs);
    $UI.append(mainTpl(rs)); //主模版

    fastclick.attach(document.body);
    clickHandle(rs);
    store.set('loginBy', 1)

    // watchKeyboard();
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

function clickHandle(rs) {
    // agree
    // $('body').on('click', '.agree', function(e) {
    //     isAgreePolicy = !isAgreePolicy;
    //     $('.agree').toggleClass('notAgree')
    // });

    // // policy
    // $('body').on('click', '.use.policy', function(e) {
    //     if (Lang && Lang.lang && Lang.lang == 'en') {
    //         location.href = '/policy/Terms-and-Conditions-of-Use.html';
    //     } else {
    //         location.href = '/policy/Syarat-dan-Ketentuan-Penggunaan.html';
    //     }
    // });

    // $('body').on('click', '.privacy.policy', function(e) {
    //     if (Lang && Lang.lang && Lang.lang == 'en') {
    //         location.href = '/policy/Privacy-Policy.html';
    //     } else {
    //         location.href = '/policy/Kebijakan-Privasi.html';
    //     }
    // });

    //sumbit
    $('.nextBtn').on('click', function () {
        if (isAgreePolicy) {
            let sumbitData = getSumbitParam();
            if (validate.check(sumbitData, lang)) {
                $UI.trigger('sumbit', [sumbitData]);
                utils.showLoading(lang.lang9)
            }
        } else {
            utils.alertMessage(lang.lang10);
        }
    });

    //send code
    $('.senCode').on('click', function () {
        let sumbitData = getSumbitParam();

        if ($('#mobile').val().length < 7) {
            utils.alertMessage(lang.lang1);
            return false;
        } else {
            turingVerification.init(sumbitData);

            //send code
            // invokeSettime("#sendCode");
            // $UI.trigger('sendCode', [sumbitData]);
            // utils.showLoading(lang.lang9)
        }

    });

    // login by password
    $('body').on('click', '.loginByPassword', function (e) {
        let sumbitData = getSumbitParam();
        console.log('password=====', sumbitData)
        if (validate.check(sumbitData, lang)) {
            if (isAgreePolicy) {
                $UI.trigger('passwordLogin', [sumbitData]);
            } else {
                utils.alertMessage(lang.lang9);
            }
        }
    })

    // find back password
    $('body').on('click', '.findBack', function (e) {
        // store.set('findBack', true)
        location.href = '/phoneNumVerification.html?go=findBackPassword'
    })

    // change login method
    $('body').on('click', '.password-tips', function (e) {
        store.set('loginBy', 2)
        $('.password-tips').css('display', 'none')
        $('.phoneVerification').css('display', 'none')
        $('.passwordLogin').css('display', 'block')
        $('.SMS-tips').css('display', 'block')
    });

    $('body').on('click', '.SMS-tips', function (e) {
        store.set('loginBy', 1)

        $('.SMS-tips').css('display', 'none')
        $('.passwordLogin').css('display', 'none')
        $('.phoneVerification').css('display', 'block')
        $('.password-tips').css('display', 'block')
    });

}

function getSumbitParam() {
    let mobile;
    if (store.get('loginBy') && store.get('loginBy') == 1) {
        mobile = $('#mobile').val().replace(/\b(0+)/gi, "")
    } else {
        mobile = $('#password-number').val().replace(/\b(0+)/gi, "")
    }
    return {
        mobile: mobile,
        mobile_country_code: 62,
        captcha: $('#captcha').val(),
        authType: 100,
        platform: 'h5',
        password : $('#password').val()

    }
}

//倒计时
function invokeSettime(obj) {
    var countdown = 60;
    settime(obj);

    function settime(obj) {
        if (countdown == 0) {
            $(obj).attr("disabled", false).removeClass('active');
            $(obj).text(lang.lang5);
            countdown = 60;
            return;
        } else {
            $(obj).attr("disabled", true).addClass('active');
            $(obj).text("" + countdown + " s");
            countdown--;
        }
        setTimeout(function () {
            settime(obj)
        }, 1000)
    }
}

// function watchKeyboard() {
//     var winHeight = $(window).height(); //获取当前页面高度
//     // console.log('$(window).height()', $(window).height())

//     $(window).resize(function() {
//         var thisHeight = $(this).height();
//         // console.log('$(this).height()', $(this).height())
//         if (winHeight - thisHeight > 140) {
//             //键盘弹出
//             $('.footer').css('display', 'none');
//         } else {
//             //键盘收起
//             $('.footer').css('display', 'block');
//         }
//     })
// }
export default obj;