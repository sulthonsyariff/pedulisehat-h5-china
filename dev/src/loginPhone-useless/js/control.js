import 'jq_cookie' //ajax cookie
import 'store'
import turingVerification from 'turingVerification'

import view from './view'
import model from './model'
import 'loading'
import '../less/loginPhone.less'
import utils from 'utils'
import store from 'store'

let obj = {};
let UI = view.UI;

let reqObj = utils.getRequestParams();
// 返回地址
let req_rul = decodeURIComponent(reqObj['qf_redirect']);
// 获取成功后回调地址
let jump_url = store.get('login') ? decodeURIComponent(store.get('login').qf_redirect_store) : '/';
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

utils.hideLoading(); // 隐藏loading
view.init({});

UI.on('sumbit', function (e, objData) {
    // 手机号绑定
    model.loginByMobile({
        param: {
            mobile: objData.mobile, //string
            mobile_country_code: objData.mobile_country_code, //number
            captcha: objData.captcha, //string
            authType: objData.authType,
            platform: objData.platform,
            uid: reqObj.uid,
            openId: reqObj.openId,
            statistics_link: statistics_link
        },
        success: function (res) {
            if (res.code == 0) {
                //存localstorage，并跳转
                $.cookie('passport', JSON.stringify({
                    accessToken: res.data.accessToken,
                    expiresIn: res.data.expiresIn,
                    refreshToken: res.data.refreshToken,
                    tokenType: res.data.tokenType,
                    tokenExpires: res.data.tokenExpires,
                    uid: res.data.uid,
                    signature: res.data.signature,
                }), {
                    expires: 365,
                    path: '/',
                    domain: 'pedulisehat.id'
                })
                // console.log('accessToken=', JSON.parse($.cookie('passport')).accessToken);

                if (res.data.userInfo.setting_password == 0) {
                    location.href = '/setPassword.html?jump_qf_redirect=' + jump_url
                } else {
                    setTimeout(function () {

                        location.href = jump_url;
                    }, 30)
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    });
})

//sendCode
UI.on('sendCode', function (e, objData) {
    // console.log('objData===',objData)
    model.sendCode({
        param: {
            mobile: parseInt(objData.mobile),
            mobile_country_code: parseInt(objData.mobile_country_code),
            image_key: objData.image_key,
            image_code: objData.image_code,
        },
        success: function (rs) {
            if (rs.code == 0) {
                turingVerification.invokeWhatsAppSettime();
                turingVerification.changeBtn();
                utils.hideLoading();
            } else if(rs.code == 4151){
                utils.alertMessage(rs.msg)
                turingVerification.updateTuringVerifyImg();
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
})

UI.on('passwordLogin', function (e, objData) {
    // utils.showLoading(lang.lang4);

    model.loginByPassword({
        param: {
            mobile_country_code: objData.mobile_country_code,
            mobile: objData.mobile,
            code: objData.code,
            password: objData.password,
            login_with: 'mobile',
        },
        success: function (res) {
            if (res.code == 0) {
                // console.log('mobile submit:', res);

                //存localstorage，并跳转
                $.cookie('passport', JSON.stringify({
                    accessToken: res.data.accessToken,
                    expiresIn: res.data.expiresIn,
                    refreshToken: res.data.refreshToken,
                    tokenType: res.data.tokenType,
                    tokenExpires: res.data.tokenExpires,
                    uid: res.data.uid,
                    signature: res.data.signature,
                }), {
                    expires: 365,
                    path: '/',
                    domain: 'pedulisehat.id'
                })

                setTimeout(function () {

                    location.href = jump_url;
                }, 30)
            }
        },
        error: utils.handleFail
    })

})