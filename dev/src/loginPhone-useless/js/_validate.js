/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    // console.log('validate', 'param:', param);
    // if (!param.mobile.length) {
    //     utils.alertMessage(lang.lang1);
    //     return false;
    // }
    if (param.mobile.length < 7) {
        utils.alertMessage(lang.lang1);
        return false;
    }

    // if (!param.captcha.length) {
    //     utils.alertMessage(lang.lang2);
    //     return false;
    // }

    return true;
}

export default obj;