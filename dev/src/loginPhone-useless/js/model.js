import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
var isLocal = location.href.indexOf("pedulisehat.id") == -1;

// console.log('domainName',domainName.passport)
//验证是否登陆
obj.getUserInfo = function(o) {
    var url = domainName.passport + '/v1/user';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

//已登陆且手机号为空更新手机号
obj.updatePhone = function(o) {
    var url = domainName.passport + '/v1/user/' + o.param.user_id;

    ajaxProxy.ajax({
        type: 'put',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
};

//未登录使用手机号登陆或注册
obj.loginByMobile = function(o) {
    var url = domainName.passport + '/v2/login/bymobile';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//发送验证码
obj.sendCode = function(o) {
    // console.log('===o===',o)
    var url = domainName.passport + '/v2/sms/captcha?mobile=' + o.param.mobile + '&mobile_country_code=' + o.param.mobile_country_code + '&image_key=' + o.param.image_key + '&image_code=' + o.param.image_code;
    // console.log('===url===',url)
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

// login by password
obj.loginByPassword = function(o) {
    let url = domainName.passport + '/v1/login/bypassword';

    ajaxProxy.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(o.param)
    }, o)
};
export default obj;