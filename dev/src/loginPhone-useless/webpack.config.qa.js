var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'loginPhone',
    htmlFileURL: 'html/loginPhone.html',
    appDir: 'js/loginPhone',
    uglify: true,
    hash: '',
    mode: 'development'
})