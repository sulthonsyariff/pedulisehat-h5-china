/**
 * 轮播图初始化
 */
import Swiper from 'swiper'
import lottie from 'lottie-web'; //port domain name
import domainName from 'domainName' // port domain name

let obj = {};

obj.init = function (rs) {
    /*if there is more than one banner,then use the swiper */
    let swiper = new Swiper('.swiper-container', {
        on: {
            // slideChangeTransitionStart: function (event) {
            //     //你的事件
            //     // $('.second-layer').css('visibility','hidden')
            //     $('.slideTopIn').RemoveClass('topIn')
            // },
            slideChangeTransitionEnd: function (event) {
                //你的事件
                console.log('slideChangeTransitionEnd', swiper.realIndex)
                if (swiper.realIndex == 1) {
                    $('.second-aninode').css('display', 'block')
                } else if (swiper.realIndex == 2) {
                    $('.third-aninode').css('display', 'block')
                } else if (swiper.realIndex == 3) {
                    $('.forth-aninode').css('display', 'block')
                } 


                // $('.slideTopIn').addClass('topIn')
            },
        },

        direction: 'vertical',
        // speed: 500,
        // loop: true,
        // observer: true,
        // autoplayDisableOnInteraction: false,
        // observeParents: true,

    });

    let pathData = domainName.static + '/mock/kindness.json'

    lottie.loadAnimation({
        container: document.getElementsByClassName('lottie'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: pathData, //如果没有图片做动效,全是设计师用画的矢量图形
    })
}

export default obj;