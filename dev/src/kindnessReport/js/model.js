import ajaxProxy from 'ajaxProxy'
import webSocket from 'webSocket'
import mnsName from 'mnsName'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserReport = function(o) {
    let url = domainName.psuic + '/v1/user_report';

    if (isLocal) {
        url = '/mock/banner.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


//新分享统计接口
obj.newProjectShare = function (o) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//获取分享相关内容
obj.getShareInfo = function (o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

//分享数据上报
obj.shareAccessCount = function (o) {
    let url = domainName.share + '/v1/share_access_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}


export default obj;