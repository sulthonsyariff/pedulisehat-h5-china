import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import 'jq_cookie' //ajax cookie

// 引入依赖
import utils from 'utils'
let $UI = view.UI;

let reqObj = utils.getRequestParams();
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
console.log(user_id)

let forwarding_default;
let forwarding_desc;
let shareUrl;
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';



// 隐藏loading
utils.hideLoading();


/*
 * 判断是否登陆
 */
// model.getUserInfo({
//     success: function(rs) {
//         // console.log(rs);
//         if (rs.code == 0) {
//             if (rs.data.mobile != '') {
//                 view.init({});
//             } else {
//                 //绑定手机号
//                 location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href);
//             }
//         } else {
//             utils.alertMessage(rs.msg)
//         }
//     },

//     error: utils.handleFail
// });

if (statistics_link && statistics_link != 'null') {
    model.shareAccessCount({
        param: {
            statistics_link: statistics_link,
            source_type: 0,
            visitor_id: user_id,
            terminal: 'H5',
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('submit share count')
            } else {
                utils.alertMessage(rs.msg)
            }
        },
    })
}

// function()
model.getUserReport({
    success: function(rs) {
        if (rs.code == 0) {
            view.init(rs);
            // getShareInfo(rs);
            utils.hideLoading();
        }
    },
    error: utils.handleFail
})

$UI.on('projectShareSuccess', function(e, target_type) {
    shareActionCount(target_type);
});

/*
    TargetTypefacebook = 1 // facebook
    TargetTypeWhatsApp = 2 // whatsapp
    TargetTypeTwitter = 3 // twitter
    TargetTypeLink = 4 // link
    TargetTypeLine = 5 // line
    TargetTypeInstagram = 6 // instagram
    TargetTypeYoutube = 7 // youtube

分享接口 {
    "target_type": 1,
    "project_id": "14221049989731189491"
}

target_type	Y	int	分享目标
item_id	Y	string	项目ID
item_type	y	int	产品类型
scene	N	int	分享场景	1:表示捐赠/付款前的分享 2:表示捐赠/付款成功后的分享 参数不传 默认为1
statistics_link	N	string	分享链接
terminal Y	string	终端类型
client_id	Y	string	客户端ID
*/
// 修改dom分享数
function shareActionCount(target_type) {
    // model.projectShare({
    model.newProjectShare({
        param: {
            target_type: target_type,
            item_id: "111111111",
            item_type: 3,
            scene: 1,
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位
        },
        success: function(res) {
            if (res.code == 0) {

            } else {
                utils.alertMessage(rs.msg);
            }
        },
        error: utils.handleFail
    });
}