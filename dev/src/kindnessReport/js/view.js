// 公共库
import 'jq_cookie' //ajax cookie
import mainTpl from '../tpl/main.juicer'
import domainName from 'domainName'; //port domain name
import lottie from 'lottie-web'; //port domain name
import swiperInit from './_swiperInit' //轮播图初始化
import changeMoneyFormat from 'changeMoneyFormat'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'
import share from 'share'

let [obj, $UI] = [{}, $('body')];
let pathData = domainName.static + '/mock/kindness.json'


/* translation */
import kindnessReport from 'kindnessReport'
import qscLang from 'qscLang'
let lang = qscLang.init(kindnessReport);
/* translation */


obj.UI = $UI;
// 初始化
obj.init = function(res) {

    res.lang = lang


    res.data.joinDate = obj.userDate(res.data.register_at);
    res.data.firstDonatedAt = obj.userDate(res.data.first_donated_at);
    res.data.countDonatedMoney = changeMoneyFormat.newMoneyFormat(res.data.count_donated_money);
    res.getLanguage = utils.getLanguage();

    console.log('res', res)
    $UI.append(mainTpl(res)); //主模版

    // console.log('pathData',pathData)
    // lottie.loadAnimation({
    //     container: document.getElementsByClassName('lottie'),
    //     renderer: 'svg',
    //     loop: true,
    //     autoplay: true,
    //     path: pathData,//如果没有图片做动效,全是设计师用画的矢量图形
    // })


    setUI(res);

    // sensorsActive.init();

}

function setUI(res) {



    swiperInit.init(res); //轮播图初始化


    // let pathData = domainName.static + '/mock/kindness.json'
    lottie.loadAnimation({
        container: document.getElementById('lottie'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: pathData, //如果没有图片做动效,全是设计师用画的矢量图形
    })
    lottie.loadAnimation({
        container: document.getElementById('lottie1'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: pathData, //如果没有图片做动效,全是设计师用画的矢量图形
    })
    lottie.loadAnimation({
        container: document.getElementById('lottie2'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: pathData, //如果没有图片做动效,全是设计师用画的矢量图形
    })

    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        let paramObj = {
            item_id: "111111111",
            item_type: 3,
            // item_short_link: '',
            // remark: data.title, //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: '2019KindnessReport.html' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // // 分享组件
            share.init(paramObj);
        }
    });


    $('body').on('click', '.goGtryBtn', function(e) {
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://gtry-qa.pedulisehat.id'
        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://gtry-pre.pedulisehat.id'
        } else {
            location.href = 'https://gtry.pedulisehat.id'
        }
    });


}
obj.userDate = function(created_at) {
    var myDate = new Date(created_at);

    var year = myDate.getFullYear();
    var month = myDate.getMonth() + 1;

    var day = myDate.getDate();

    if (month < 2) {
        month = "January";
    } else if (month < 3) {
        month = "February";
    } else if (month < 4) {
        month = "March";
    } else if (month < 5) {
        month = "April";
    } else if (month < 6) {
        month = "May";
    } else if (month < 7) {
        month = "June";
    } else if (month < 8) {
        month = "July";
    } else if (month < 9) {
        month = "August";
    } else if (month < 10) {
        month = "September";
    } else if (month < 11) {
        month = "October";
    } else if (month < 12) {
        month = "November";
    } else if (month < 13) {
        month = "December";
    }

    if (day < 10) {
        day = "0" + day;
    }
    return day + ' ' + month + ' ' + year;
};

export default obj;