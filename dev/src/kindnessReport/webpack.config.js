var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: '2019 kindness report',
    htmlFileURL: 'html/2019KindnessReport.html',
    appDir: 'js/2019KindnessReport',
    uglify: true,
    hash: '',
    mode: 'production'
})