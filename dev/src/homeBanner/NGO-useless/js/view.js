import mainTpl from '../tpl/NGO-cooperate.juicer'
import commonNav from 'commonNav'
import commonShare from 'commonShare'

// import commonFooter from 'commonFooter'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'

/* translation */
import ngoACT from 'ngoACT'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(ngoACT);
/* translation */

let [obj, $UI] = [{}, $('body')];


obj.UI = $UI;

obj.init = function(res) {
    res.JumpName = titleLang.detail;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/';

    commonNav.init(res);
    res.fromWhichPage = 'NGO.html'; //google analytics need distinguish the page name
    commonShare.init(res); //share弹窗

    $('title').html(titleLang.detail);

    res.lang = lang;
    $UI.append(mainTpl(res));
    // commonFooter.init(res);
    // console.log('view:', res);
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // share:如果是安卓端：吊起安卓分享弹窗

    $('body').on('click', '#share', function(e) {
        if (utils.browserVersion.android) {
            $UI.trigger('android-share');
        } else {
            $('.common-share').css({
                'display': 'block'
            });
        }
    });

    // share
    $('body').on('click', '#shareClose', function(e) {
        $('.common-share').css({
            'display': 'none'
        });
    });

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};


export default obj;