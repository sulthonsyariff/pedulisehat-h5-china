import view from './view'
import model from './model'
import 'loading'
import '../less/NGO-cooperate.less'
// 引入依赖
import utils from 'utils'

let obj = {};
let UI = view.UI;

// 隐藏loading
utils.hideLoading();

//facebook分享添加meta头部信息
// utils.changeFbHead({
//     url: 'https://qa.pedulisehat.id/share/_NGO-cooperate.html',
//     title: 'Campaign Overview',
//     description: 'Gempa berkekuatan 7.4 SR mengguncang dengan episentrum dekat Kota Donggala Sulawesi Tengah, Jumat (28/9) petang. Gempa turut dirasakan hingga ke Kota Palu dan Mamuju. Sesaat setelah gempa besar merontokkan bangunan, gelombang tsunami itu datang, menyapu habis Kota Palu hingga ratusan meter.',
//     image: 'https://static-qa.pedulisehat.id/img/homeBanner/NGO-1.png'
// });

// 如果是安卓端： 吊起安卓分享弹窗
UI.on('android-share', function(e) {
    let android_share_url =
        "native://share/webpage" +
        "?url=" + encodeURIComponent(location.href) +
        // "?url=" + encodeURIComponent("https://qa.pedulisehat.id/share/_NGO-cooperate.html") +
        "&title=" + encodeURIComponent("Campaign Overview") +
        "&description" + encodeURIComponent("Gempa berkekuatan 7.4 SR mengguncang dengan episentrum dekat Kota Donggala Sulawesi Tengah, Jumat (28/9) petang. Gempa turut dirasakan hingga ke Kota Palu dan Mamuju. Sesaat setelah gempa besar merontokkan bangunan, gelombang tsunami itu datang, menyapu habis Kota Palu hingga ratusan meter.") +
        "&image=" + encodeURIComponent("https://static.pedulisehat.id/img/homeBanner/NGO-1.png");

    location.href = android_share_url;
});

view.init({});