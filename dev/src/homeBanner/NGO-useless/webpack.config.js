var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'NGO-cooperate',
    htmlFileURL: 'html/NGO-cooperate.html',

    htmlOgUrl: 'https://www.pedulisehat.id/NGO-cooperate.html',
    htmlOgTitle: 'Campaign Overview',
    htmlOgDescription: 'Gempa berkekuatan 7.4 SR mengguncang dengan episentrum dekat Kota Donggala Sulawesi Tengah, Jumat (28/9) petang. Gempa turut dirasakan hingga ke Kota Palu dan Mamuju. Sesaat setelah gempa besar merontokkan bangunan, gelombang tsunami itu datang, menyapu habis Kota Palu hingga ratusan meter.',
    htmlOgImage: 'https://static.pedulisehat.id/img/homeBanner/NGO-1.png',

    appDir: 'js/NGO-cooperate',
    uglify: true,
    hash: '',
    mode: 'production'
})