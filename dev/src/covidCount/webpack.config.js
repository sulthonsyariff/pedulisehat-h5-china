var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'Data Real-Time Covid-19 di Indonesia',
    // htmlFileURL: 'html/GTRY-Pedulisehat-Id/detail.html',
    htmlFileURL: 'html/datacovid19.html',

    htmlOgUrl: 'https://www.pedulisehat.id',
    // htmlOgTitle: 'Data Real-Time Covid-19 di Indonesia',
    // htmlOgDescription: 'Data Real-Time Covid-19 di Indonesia',
    // htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',

    appDir: 'js/datacovid19',
    uglify: true,
    hash: '',
    mode: 'production'
})