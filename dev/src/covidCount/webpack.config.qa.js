var baseConfig = require('../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'Data Real-Time Covid-19 di Indonesia',
    // htmlFileURL: 'html/GTRY-Pedulisehat-Id/detail.html',
    htmlFileURL: 'html/datacovid19.html',

    htmlOgUrl: 'https://qa.pedulisehat.id',
    // htmlOgTitle: 'Program GTRY untuk Penyakit Kritis',
    // htmlOgDescription: 'Bergabunglah dengan program GTRY, bantuan untuk penyakit kritis dengan dana kompensasi hingga Rp 100.000.000.https://gtry.pedulisehat.id/ ',
    // htmlOgImage: 'https://static.pedulisehat.id/img/ico/logo.png',

    appDir: 'js/datacovid19',
    uglify: true,
    hash: '',
    mode: 'development'
})