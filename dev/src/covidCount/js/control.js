import view from './view'
import model from './model'
import provinceData from './_provinceData'
import '../less/main.less'

import 'loading'
// 引入依赖
import utils from 'utils'
import store from 'store'



let $UI = view.UI;

// let forwarding_default;
// let forwarding_desc;
// let shareUrl;

let reqObj = utils.getRequestParams();


utils.hideLoading();


// view.init({});
// provinceData.init({});

// model.getShareInfo({
//     param: {
//         item_id: "222222222",
//         item_type: 3,
//         // item_short_link: short_link
//     },
//     success: function(rs) {
//         if (rs.code == 0) {
//             // forwarding_default = rs.data.forwarding_default;
//             // forwarding_desc = rs.data.forwarding_desc;
//             // shareUrl = rs.data.url

//             // console.log('getShareInfo', forwarding_default, forwarding_desc, shareUrl)
//             getCountryCount();


//             // 如果是安卓端： 吊起安卓分享弹窗
//             // $UI.on('android-share', function(e) {
//             //     let android_share_url =
//             //         "native://share/webpage" +
//             //         "?url=" + encodeURIComponent(shareUrl) +
//             //         // "&title=" + encodeURIComponent(rs.data.name) +
//             //         // "&description" + encodeURIComponent(rs.data.story.substr(1, 100)) +
//             //         // "&image=" + (rs.data.images[0] ? encodeURIComponent(rs.data.images[0].image) : '') +
//             //         "&forwardingDesc=" + encodeURIComponent(forwarding_default + ' %s') +
//             //         "&forwardingMap=" + encodeURIComponent(JSON.stringify(forwarding_desc));

//             //     location.href = android_share_url;
//             // })

//         } else {
//             utils.alertMessage(rs.msg)
//         }
//     },
//     error: utils.handleFail

// })

getCountryCount();

function getCountryCount() {
    model.getCountryCount({

        success: function(rs) {
            if (rs.code == 0) {

                view.init(rs);
                utils.hideLoading();
                getProvinceCount(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail

    })
}

function getProvinceCount(res) {
    model.getProvinceCount({
        success: function(rs) {
            if (rs.code == 0) {
                provinceData.init(rs)

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail

    })
}