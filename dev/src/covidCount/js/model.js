import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName'; //port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


obj.getCountryCount = function(o) {
    var url = domainName.base + '/v1/total_covid';

    // if (isLocal) {
    //     url = '../mock/verify.json';
    // }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};
obj.getProvinceCount = function(o) {
    var url = domainName.base + '/v1/province_covid';

    // if (isLocal) {
    //     url = '../mock/verify.json';
    // }
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


//获取分享相关内容
obj.getShareInfo = function (o) {
    let url = domainName.share + '/v1/share_short_link';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

export default obj;