// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import share from 'share'

import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import changeMoneyFormat from 'changeMoneyFormat'


import utils from 'utils'
import domainName from 'domainName'; //port domain name
import store from 'store'
// // import sensorsActive from 'sensorsActive'

/* translation */
import covidData from 'covidData'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(covidData);
/* translation */

let [obj, $UI] = [{}, $('body')];


let reqObj = utils.getRequestParams();

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

obj.UI = $UI;

// 初始化
obj.init = function(res) {
    // res.JumpName = lang.lang77;

    // if (!utils.browserVersion.android) {
    //     commonNav.init(res);
    // }

    res.getLanguage = utils.getLanguage();
    res.lang = lang;
    console.log('view=', res)
    res.data.confirmed = res.data.confirmed.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.new_confirmed_count = res.data.new_confirmed_count.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.active_cases = res.data.active_cases.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.new_active_cases_count = res.data.new_active_cases_count.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.recovered = res.data.recovered.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.new_recovered_count = res.data.new_recovered_count.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.deaths = res.data.deaths.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    res.data.new_deaths_count = res.data.new_deaths_count.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
    $UI.append(mainTpl(res)); //主模版

    commonFooter.init(res);
    fastclick.attach(document.body);

    //分享添加meta头部信息
    // utils.changeFbHead({
    //     url: "https:" + domainName.share + '/gtry/' + res.data[0].product_id,
    // });


    clickHandle(res);
    // // sensorsActive.init();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);


};


function clickHandle(res) {
    // share:如果是安卓端：吊起安卓分享弹窗
    // $('body').on('click', '#share', function (e) {
    //     $('.bubble-icon').css('display', 'none');

    //     if (utils.browserVersion.android) {
    //         $UI.trigger('android-share');
    //     } else {
    //         $('.common-share').css({
    //             'display': 'block'
    //         });

    //         $('.bubble-icon').css({
    //             'display': 'none'
    //         })
    //     }
    // });


    // back to index
    $('body').on('click', '.home-btn', function() {
        if (utils.browserVersion.android) {
            location.href = 'native://close'
        } else {
            location.href = '/'
        }
    })

    // jump to gtry diease
    $('body').on('click', '.gtry-join-banner', function() {
        if (utils.judgeDomain() == 'qa') {
            location.href = 'https://gtry-qa.pedulisehat.id/penyakitkritis.html'
        } else if (utils.judgeDomain() == 'pre') {
            location.href = 'https://gtry-pre.pedulisehat.id/penyakitkritis.html'
        } else {
            location.href = 'https://gtry.pedulisehat.id/penyakitkritis.html'

        }

    })

    // jump to gtry group
    $('body').on('click', '.gtry-group-banner', function() {

        if (utils.browserVersion.android) {
            location.href = 'qsc://app.pedulisehat/go/special_projects?short_link=lawancorona'
        } else {
            location.href = 'https://pedulisehat.id/group/lawancorona'
        }
    })


    // jump to driver
    $('body').on('click', '.driver-banner', function() {

        if (utils.browserVersion.android) {
            location.href = 'qsc://app.pedulisehat/go/campaign_details?project_id=12419737548015796748&from=h5'
        } else {
            location.href = 'https://pedulisehat.id/berkahuntukojol'
        }
    })


    // share:如果是安卓端：吊起安卓分享弹窗
    $('body').on('click', '#share', function(e) {
        let paramObj = {
            item_id: "222222222",
            item_type: 3, //3 代表勋章 / 1 代表大病详情类型
            // item_short_link: rs.data.short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'covidCount.html' //google analytics need distinguish the page name
        };

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // 分享组件
            share.init(paramObj);
        }
    });


    // FAQ
    $('body').on('click', '.FAQ .list', function() {
        $(this).toggleClass('show');
    })


}





export default obj;