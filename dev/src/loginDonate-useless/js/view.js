// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import store from 'store'
import validate from '../js/_validate'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import loginDonate from 'loginDonate'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(loginDonate);
/* translation */

let [obj, $UI] = [{}, $('body')];

let CACHED_KEY = 'switchLanguage';
let Lang = store.get(CACHED_KEY);
let isAgreePolicy = true; //是否同意协议

obj.UI = $UI;
// 初始化
obj.init = function (res) {
    res.JumpName = titleLang.loginDonate;
    commonNav.init(res);

    res.lang = lang;
    $UI.append(mainTpl(res)); //主模版
    commonFooter.init(res);

    fastclick.attach(document.body);

    clickHandle(res);
    uiHandle();
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};



function clickHandle(rs) {
    // facebook
    $('body').on('click', '#facebookBtn', function (params) {
        if (isAgreePolicy) {
            // console.log('facebook login');
            $UI.trigger('prelogin', {
                authType: '101',
                redirectURI: location.origin + "/login.html?flag=facebook"
            }); //facebook
        } else {
            utils.alertMessage(lang.lang13);
        }
    });

    // google
    $('body').on('click', '#googleBtn', function (params) {
        if (isAgreePolicy) {
            // console.log('google login');
            $UI.trigger('prelogin', {
                authType: '102',
                redirectURI: location.origin + "/login.html?flag=google"
            });
        } else {
            utils.alertMessage(lang.lang13);
        }
    });

    // phone
    $('body').on('click', '#phoneBtn', function (params) {
        if (isAgreePolicy) {
            location.href = '/loginPhone.html'
        } else {
            utils.alertMessage(lang.lang13);
        }
    });

    // agree
    $('body').on('click', '.agree', function (e) {
        isAgreePolicy = !isAgreePolicy;
        $('.agree').toggleClass('notAgree')
    });

    // termCondition
    $('body').on('click', '.use.policy', function (e) {
        location.href = '/termCondition.html';

        // if (Lang && Lang.lang && Lang.lang == 'en') {
        //     location.href = '/policy/Terms-and-Conditions-of-Use.html';
        // } else {
        //     location.href = '/policy/Syarat-dan-Ketentuan-Penggunaan.html';
        // }
    });

    // privacypolicy
    $('body').on('click', '.privacy.policy', function (e) {
        location.href = '/privacyPolicy.html';
        // if (Lang && Lang.lang && Lang.lang == 'en') {
        //     location.href = '/policy/Privacy-Policy.html';
        // } else {
        //     location.href = '/policy/Kebijakan-Privasi.html';
        // }
    });

    //sumbit
    $('.next-btn').on('click', function () {
        if (isAgreePolicy) {
            let sumbitData = getSumbitParam();

            if (validate.check(sumbitData, lang)) {

                let donateMoneyStore = store.get('donateMoney');
                donateMoneyStore.user_name = donateMoneyStore.user_name ? donateMoneyStore.user_name : $('.name-box').val();
                donateMoneyStore.phone = $('#mobile').val().replace(/\b(0+)/gi, "");
                store.set('donateMoney', donateMoneyStore);
                store.set('un-login-name', getUnloginUser())

                $UI.trigger('submit');
                utils.showLoading(lang.lang14);
            }
        } else {
            utils.alertMessage(lang.lang13);
        }
    });

}
function getUnloginUser() {
    return {
        name: $('.name-box').val(),
    }
}

function getSumbitParam() {
    let mobile;
    mobile = $('#mobile').val().replace(/\b(0+)/gi, "")
    return {
        mobile: mobile,
        name: $('.name-box').val(),
    }
}

function uiHandle() {
    if (utils.getLanguage() == 'id') {
        $('.login .title').addClass('id-style');
    }
}
export default obj;