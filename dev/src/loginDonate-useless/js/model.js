import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * authType = 101 (101:facebook,102:google)
 * platform = h5
 */
obj.preLogin = function(params) {
    var url = domainName.passport + '/v1/oauth2/prelogin?authType=' + params.authType + '&platform=h5' + '&redirectURI=' + params.redirectURI;

    if (isLocal) {
        url = '/mock/prelogin.json';
    }

    return $.ajax({
        url: url,
        type: 'get',
    })
};

export default obj;