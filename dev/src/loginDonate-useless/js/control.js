import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'

/* translation */
import loginDonate from 'loginDonate'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let lang = qscLang.init(loginDonate);
/* translation */

let obj = {};
let UI = view.UI;
let $UI = $('body');
let reqObj = utils.getRequestParams();

// 隐藏loading
utils.hideLoading();
view.init({});

/*
 * 如果存在回跳地址，将回跳地址存在本地
 * 如果不存在回跳地址，将首页存本地，（ 排除第三方登陆回跳login干扰,避免之前存好的回调地址被替换）
 */
if (reqObj.qf_redirect) {
    store.set('login', {
        qf_redirect_store: reqObj.qf_redirect || encodeURIComponent('/')
    });
}

/**
 * 预登陆
 */
$UI.on('prelogin', function(e, param) {
    utils.showLoading(lang.lang14);

    model.preLogin(param).done(function(res) {
        if (res.code == 0) {
            location.href = res.data.redirectURI;
        } else {
            utils.alertMessage(res.msg)
        }
    }).fail(utils.handleFail);
});

/*
 * submit
 */
$UI.on('submit', function() {
    location.href = decodeURIComponent(reqObj.qf_redirect);
});