var baseConfig = require('../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'loginDonate',
    htmlFileURL: 'html/loginDonate.html',
    appDir: 'js/loginDonate',
    uglify: true,
    hash: '',
    mode: 'production'
})