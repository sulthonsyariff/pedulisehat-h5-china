// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import listItemTpl from '../tpl/_listItem.juicer'
import store from 'store'
import handleScrollLoad from './_handleScrollLoad'
import ScrollLoad from './_scrollLoad' // new scroll load

import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import changeMoneyFormat from 'changeMoneyFormat'
import googleAnalytics from 'google.analytics'
import getListPageH from './_getListPageHeight'
import utils from 'utils'
// // import sensorsActive from 'sensorsActive'
import campaignListComponents from 'campaignListComponents'
import promoPopup from '../../../common/promoPopup/js/main'
import actionPromoPopup from '../../../common/promoPopup/js/_clickHandle'

/* translation */
import campaignList from 'campaignList'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(campaignList);
/* translation */

let is_first = true;
let page = 0;

let insertMethod = 'append';
let pageH = getListPageH.get();

let [obj, $UI] = [{}, $('body')];

let reqObj = utils.getRequestParams();
let composite = reqObj['composite'] || '';
let newest = reqObj['newest'] || '';
let hottest = reqObj['hottest'] || '';
let campaigner = reqObj['campaigner'] || '3';
let isCampaignList = location.href.indexOf('campaignList') != -1;
let isZakatList = location.href.indexOf('zakatList') != -1;
let isDonateList = location.href.indexOf('donateList') != -1;

// console.log('is isCampaignList ', isCampaignList);
// console.log('is isZakatList ', isZakatList);
// console.log('is isDonateList ', isDonateList);

obj.UI = $UI;

obj.init = function(rs) {
    rs.lang = lang;
    rs.JumpName = isCampaignList && titleLang.isCampaignList ||
        isZakatList && titleLang.isZakatList ||
        isDonateList && titleLang.isDonateList;
    rs.showSearch = true;
    rs.commonNavGoToWhere = '/';
    rs.composite = composite;
    rs.newest = newest;
    rs.hottest = hottest;
    rs.campaigner = campaigner;
    rs.isZakatList = isZakatList;
    commonNav.init(rs);
    $('title').html(titleLang.campaignList);

    $UI.append(mainTpl(rs)); //insert 主模版


    clickHandle(rs);
    commonFooter.init({});

    handleScrollLoad.init(rs); //init scroll

    fastclick.attach(document.body);

    // // sensorsActive.init();

    // $('body').on('click', '.right', function() {
    //     sensors.track('SearchBarClick')
    // })

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // close APP download
    if ($('.app-download').css('display') === 'block') {
        console.log('app-download')
        $('.page-inner').css('padding-top', '102px')
        $('.fix-top').css('top', '103px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')
        $('.fix-top').css('top', '57px')
    });

    console.log('----------')

};


function clickHandle() {
    // zakat-calculator
    $('body').on('click', '#zakat-calculator', function() {
        actionPromoPopup.click()
        location.href = '/zakatCalculator.html'
    })

    $('body').on('click', '.composite-wrap', function() {
        actionPromoPopup.click()
        // $('.composite-wrap .order-content').css('color', '#43ac43')
        if (composite == '') {
            composite = 1
        }
        location.href = location.pathname + '?composite=' + composite
    })

    $('body').on('click', '.campaigner-wrap', function() {
        actionPromoPopup.click()
        $('#dropdown-sort').hide();
        $('#dropdown-campaigner').toggle();
    })

    $('body').on('click', '.sort-wrap', function() {
        actionPromoPopup.click()
        $('#dropdown-campaigner').hide();
        $('#dropdown-sort').toggle();
    })

    $('body').on('click', '#choose-organization', function() {
        actionPromoPopup.click()
        if(campaigner == 3)
            campaigner = 1
        else if(campaigner == 1)
            campaigner = 3
        else
            campaigner = 2

        var params = ''
        if (hottest != '') {
            params = '&hottest=' + hottest
        }else if (newest != '') {
            params = '&newest=' + newest
        }
        location.href = location.pathname + '?campaigner=' + campaigner + params
    })

    $('body').on('click', '#choose-individual', function() {
        actionPromoPopup.click()
        if(campaigner == 3)
            campaigner = 2
        else if(campaigner == 2)
            campaigner = 3
        else
            campaigner = 1

        var params = ''
        if (hottest != '') {
            params = '&hottest=' + hottest
        }else if (newest != '') {
            params = '&newest=' + newest
        }
        location.href = location.pathname + '?campaigner=' + campaigner + params

    })

    $('body').on('click', '#choose-hottest', function() {
        actionPromoPopup.click()
        if (hottest == '') {
            hottest = -1
        } else if (hottest == -1) {
            hottest = 1
        } else if (hottest == 1) {
            hottest = ''
        }

        var params = ''
        if (hottest != '') {
            params = '&hottest=' + hottest
        }

        location.href = location.pathname + '?campaigner=' + campaigner + params
    })

    $('body').on('click', '#choose-newest', function() {
        actionPromoPopup.click()
        if (newest == '') {
            newest = -1
        } else if (newest == -1) {
            newest = 1
        } else if (newest == 1) {
            newest = ''
        }

        var params = ''
        if (newest != '') {
            params = '&newest=' + newest
        }

        location.href = location.pathname + '?campaigner=' + campaigner + params
    })
}
obj.insertData = function(rs, o, method) {
    promoPopup.init()

    insertMethod = method;
    rs.lang = lang;
    rs.domainName = domainName;

    // 通过 is_first 值来judge是否展示空白列表页
    if (rs.data && rs.data.length != 0) {
        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        // insert page
        handleScrollLoad.insertData(rs, o, insertMethod);

        rs.fromWhichPage = location.pathname
        rs.$dom = $('.Campaigns_List.page_' + rs._metadata.page)
        campaignListComponents.init(rs);

    } else if (is_first) {
        $('.loading.append-loading').hide();
        $('ul.project-list').append(listItemTpl(rs)); //渲染空白列表

        ScrollLoad.stop();
    } else {
        $('.loading.append-loading').hide();
        $('.campaign_box').last().attr('last', 'last'); // 给最后一条做一个标记
        ScrollLoad.run(); // 页面加载最后一条后，继续监听滚动，但不要持续加载最后一页
    }

    if (insertMethod == 'prepend' && $('.campaign_box').eq(0).data('page') != 1) {
        $('.loading.preappend-loading').show();
    }
}

obj.raiseDay = function(created_at) {
    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}

export default obj;