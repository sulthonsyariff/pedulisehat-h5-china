import ScrollLoad from './_scrollLoad' // new scroll load
import './_campaignCardClick'
import getListPageH from './_getListPageHeight'
import listItemTpl from '../tpl/_listItem.juicer'

// let reqObj = utils.getRequestParams();
// let newest = reqObj['newest'] || '';

let page = 1;
let list;
let scrollTop;
let [obj, $UI] = [{}, $('body')];
let pageH = getListPageH.get();
let left_H = 0;
let onlyOnce = true;

obj.init = function(rs) {
    console.log('rs',rs)
    ScrollLoad.config({
        // wrapper: $UI,
        onNeedLoad: function(insertMethod) {
            let homeListHistory = sessionStorage.getItem('home-list-history');

            // 从本地获取页
            if (homeListHistory) {
                page = JSON.parse(homeListHistory).page;
                list = JSON.parse(homeListHistory).list;
                scrollTop = JSON.parse(homeListHistory).scrollTop;
                sessionStorage.removeItem('home-list-history'); // remove cache in sessionStorage
            } else {
                setPage(insertMethod);
            }

            // 页存在时，trigger加载DOM
            if (page) {
                console.log('trigger ', page);

                $UI.trigger('needload', [{
                    newest: rs.newest,
                    hottest: rs.hottest,
                    composite: rs.composite,
                    campaigner: rs.campaigner,
                    page: page,
                    list: list,
                    scrollTop: scrollTop
                }, insertMethod]);

            } else {
                ScrollLoad.run(); //继续监听滚动事件
            }

        }
    });
    ScrollLoad.run();
}

obj.insertData = function(rs, o, insertMethod) {
    /** 滚动加载相关 **/
    // $('ul.project-list')[insertMethod](listItemTpl(rs)); // prepend的时候不需要设置$(document).scrollTop() ？？？！！！

    if (insertMethod == "prepend") {
        $('ul.project-list')[insertMethod](listItemTpl(rs)); // prepend的时候不需要设置$(document).scrollTop() ？？？！！！
        $('.loading.preappend-loading').hide();

        setTimeout(() => {
            var curScroll = $(document).scrollTop();

            // console.log('curScroll<0 || <60', curScroll);
            if (curScroll < 60 || curScroll == 0) {
                console.log('curScroll<0', curScroll);
                $(document).scrollTop(pageH);
            }
        }, 0);

    } else if (insertMethod == "append") {
        $('ul.project-list')[insertMethod](listItemTpl(rs)); // prepend的时候不需要设置$(document).scrollTop() ？？？！！！
    }

    if (o.scrollTop && onlyOnce) {
        $(document).scrollTop(o.scrollTop);
        let current_scroll = $(document).scrollTop(); // 计算出最新的滚动高度
        onlyOnce = false;
        left_H = o.scrollTop - current_scroll; //当滚动高度过高而页面尚未加载准备时，遗留高度，下次滚动
    }

    // left_H存在是因为当insert one page后，滚动的高度较高，滚动不到这个值 ！！！下一页还来不及加载
    // 还留下一段偏差距离，下次加载的时候判断偏差距离是否存在，如果存在，则继续滚动此距离 ！！！
    while (left_H > 0) {
        let current_scroll = $(document).scrollTop(); // 计算出最新的滚动高度
        $(document).scrollTop(current_scroll + left_H);
        left_H = current_scroll - $(document).scrollTop();
    }

    ScrollLoad.run(); //继续监听滚动(前提是上一个DOM已经加载了，避免加载多余的页面)
}

function setPage(method) {
    // history load
    if ($('.campaign_box').length > 0) {
        page = undefined;
        // console.log('loading.......');

        if (method == 'append') {
            let lastPage = $('.campaign_box').last().data('page');
            if (($(".page_" + lastPage + " .campaign_box").length > 0) &&
                ($('.campaign_box').last().attr('last') != 'last' && $(".campaign_box[data-page=" + lastPage + "]").length > 0)) {
                page = lastPage + 1;
                // console.log('page ', lastPage, '加载完成');
            }
        } else {
            let firstPage = $('.campaign_box').eq(0).data('page');
            if ($(".page_" + firstPage + " .campaign_box").length > 0) {
                page = firstPage - 1;
                // console.log('page ', firstPage, '加载完成');
            }
        }
    } else {
        page = 1;
    }

}

export default obj;