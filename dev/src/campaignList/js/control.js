// 公共库
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import 'freshchat.widget'
import search from 'search'
import model_getListData from 'model_getListData' //列表接口调用地址

// 引入依赖
import utils from 'utils'

let UI = view.UI;

view.init({});

// 加载更多
UI.on('needload', function(e, o, method) {
    model_getListData.getListData_donateList_campaignList_zakatList({
        param: {
            page: o.page,
            newest: o.newest,
            hottest: o.hottest,
            composite: o.composite,
            campaigner: o.campaigner,
        },
        success: function(res) {
            if (res.code == 0) {
                // 隐藏loading
                utils.hideLoading();

                view.insertData(res, o, method);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});

//删除搜索历史记录
UI.on('deleteSearchHistory', function() {
    model.deleteSearcnHistory({
        success: function(data) {
            if (data.code == 0) {
                //     view.insertData(data);
                console.log('deleteSuccess', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
})

// 获取搜索历史记录
model.getSearchHistory({
    success: function(rs) {
        if (rs.code == 0) {
            getPopularWord(rs);
            // console.log('insertData', data);
        } else {
            utils.alertMessage(data.msg)
        }
    },
    error: utils.handleFail
})

//获取推荐词
function getPopularWord(rs) {
    model.getPopularWord({
        success: function(res) {
            if (res.code == 0) {
                res.searchHistory = rs.data
                res.from = 'campaignList'
                search.init(res);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
}