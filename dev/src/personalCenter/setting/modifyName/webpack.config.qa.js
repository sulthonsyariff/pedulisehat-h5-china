var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'page',
    htmlFileURL: 'html/modifyName.html',
    appDir: 'js/modifyName',
    uglify: true,
    hash: '',
    mode: 'development'
})