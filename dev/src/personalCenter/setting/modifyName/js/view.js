// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import modifyName from 'modifyName'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyName);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    res.JumpName = '';
    res.data.lang = lang;
    commonNav.init(res);
    $UI.append(mainTpl(res.data)); //主模版
    fastclick.attach(document.body);

    //获取焦点后光标在字符串后
    //其原理就是获得焦点后重新把自己复制粘帖一下
    let val = $('input.name').val();
    $('input.name').val('').focus().val(val);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

/*
 * close
 */
$('body').on('click', '.close', function() {
    // console.log('==close==');

    $('input.name').val('').focus()
})

/*
 * save name
 */
$('body').on('click', '.save', function() {
    // console.log('==save==', $('input.name').val(), $('input.name').val().trim().length == 0);

    if ($('input.name').val() == '') {
        utils.alertMessage(lang.lang3);
    } else {
        $UI.trigger('changeHandle', $('input.name').val().replace(/(\s*$)/g, ""));
    }

})


export default obj;