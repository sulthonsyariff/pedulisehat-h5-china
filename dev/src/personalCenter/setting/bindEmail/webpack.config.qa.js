const merge = require('webpack-merge');
const commonConfig = require("./webpack.config.common");
const baseConfig = require('../../../../../base.config.qa.js');

module.exports = merge(baseConfig(commonConfig), {
    mode: "development",
});