// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'
import store from 'store'

import suffixTpl from '../tpl/_suffixTpl.juicer'
import sinarmasObj from './list.json'

/* translation */
import modifyName from 'modifyName'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyName);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let sinarmasEmailArray

if (store.get('from') == 'sinarmas') {
    sinarmasEmailArray = ['sinarmasforestry.com', 'app.co.id', 'simasjiwa.co.id', 'sinarmas.co.id', 'sinarmasmsiglife.co.id', 'banksinarmas.com', 'beraucoal.co.id', 'bsa-logistics.co.id', 'bizzy.co.id', 'borneo-indobara.com', 'danamas.com', 'dsspower.co.id', 'ekahospital.com', 'ekatjipta.org', 'ptesm.com', 'excellerate.co.id', 'finmas.co.id', 'goldenenergymines.com', 'ibstower.com', 'ibsmulti.com', 'kiic.co.id', 'myrepublic.net.id', 'nusantarare.com', 'asuransisimasnet.com', 'sinarmas-am.co.id', 'sinarmascepsa.com', 'sinarmasforestry.com', 'sinarmasland.com', 'sinarmasmining.com', 'simasfinance.co.id', 'sinarmassekuritas.co.id', 'swa-jkt.com', 'sinarsyno.com', 'sinarmas-agri.com', 'sinarmas-agribusiness.com', 'smartfren.com', 'smdv.com', 'lifewithsun.com', 'sinarmas.org', 'ekatjipta.org']

}




// 初始化
obj.init = function (res) {
    res.JumpName = 'E-mail';
    res.lang = lang;




    if (store.get('from') == 'sinarmas') {
        //判断位置是否为金光邮箱
        let sinarmasEmailSuffix = '' + res.data.email.split('@')[1];

        let sinarmasReg = sinarmasEmailArray.some(function (item, index) {
            if (item == sinarmasEmailSuffix) {
                return true
            } 
        })

        console.log('sinarmasReg',sinarmasReg);
        
        if (!sinarmasReg){
            res.data.email = ''
        }

        res.sinarmasReg = sinarmasReg
        res.sinarmasEmailArray = sinarmasEmailArray
        res.sinarmasObj = sinarmasObj
        res.commonNavGoToWhere = '/';

        res.email = res.data.email.split('@')[0]
        res.suffix = res.data.email.split('@')[1]

    }


        
        // console.log('res', res,  !$('.suffix').attr('data-suffix'));

    commonNav.init(res);

    $UI.append(mainTpl(res)); //主模版
    $UI.append(suffixTpl(res)); //主模版
    fastclick.attach(document.body);

    if (store.get('from') == 'sinarmas') {
        $('.list-items').each(function (index) {
            // console.log(index);

            if ($(this).attr('data-suffix') == res.suffix) {
                $(this).addClass('chosed')
                console.log($(this));

            }
        })
    }
    //获取焦点后光标在字符串后
    //其原理就是获得焦点后重新把自己复制粘帖一下
    let val = $('input.email').val();
    $('input.email').val('').focus().val(val);

    utils.hideLoading();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

};

/*
 * submit
 */
$('body').on('click', '.submit', function () {
    let emailStr
    if (store.get('from') == 'sinarmas') {
        if (!$('.suffix').attr('data-suffix')) {
            utils.alertMessage(lang.lang6);
        } else {
            emailStr = $('input.email').val() + '@' + $('.suffix').attr('data-suffix');
        }
        // emailStr = $('input.email').val() + '@' +
        //     ($('.sinarmas .suffix').length ? $('.suffix').attr('data-suffix') : '');
    } else {
        emailStr = $('input.email').val()
    }


    if ($('input.email').val() == '') {
        utils.alertMessage(lang.lang6);

    } else if (emailStr.split('@').length - 1 != 1) {
        utils.alertMessage(lang.lang7);

    } else {
        utils.showLoading();

        $UI.trigger('changeHandle', emailStr.replace(/(\s*$)/g, ""));
    }
})


$('body').on('click', '.suffix', function () {
    $('.alert-suffix').css('display', 'block')
})
$('body').on('click', '.close', function () {
    $('.alert-suffix').css('display', 'none')
})
$('body').on('click', '.suffix-mask', function () {
    $('.alert-suffix').css('display', 'none')
})


$('body').on('click', '.list-items', function () {
    $(this).siblings().removeClass('chosed')
    $(this).addClass('chosed')
    $('.suffix').html($(this).attr('data-suffix')).attr('data-suffix', $(this).attr('data-suffix'))
    $('.alert-suffix').css('display', 'none')
})


export default obj;