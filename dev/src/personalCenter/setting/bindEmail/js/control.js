import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
import store from 'store'

/* translation */
import modifyName from 'modifyName'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyName);
/* translation */
let UI = view.UI;
let user_id;

// 隐藏loading
utils.showLoading();

// 获取用户信息
model.getUserInfo({
    success: function(rs) {
        if (rs.code == 0) {
            user_id = rs.data.user_id;

            // //金光登录输入邮箱流程需要
            if (store.get('from') == 'sinarmas') {
                rs.sinarmas = true
            }

            view.init(rs);
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
});

UI.on('changeHandle', function(e, email) {

    model.changeUserInfor({
        param: {
            user_id: user_id,
            email: {
                email: email
            }
        },
        success: function(res) {
            // console.log('res:', res);
            if (res.code == 0) {
                utils.hideLoading();
                utils.alertMessage(lang.lang5);

                // //金光登录输入邮箱流程需要
                if (store.get('from') == 'sinarmas') {
                    store.set('from', '');

                    setTimeout(() => {
                        location.href = '/';
                    }, 200);
                } else {
                    setTimeout(() => {
                        location.href = '/myAccount.html';
                    }, 200);
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    });


});