// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import 'freshchat.widget'

/* translation */
import setting from 'setting'
import qscLang from 'qscLang'
let lang = qscLang.init(setting);
/* translation */
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let user_id;

// 隐藏loading
utils.showLoading();

// 获取用户信息
model.getUserInfo({
    success: function(res) {
        if (res.code == 0) {
            user_id = res.data.user_id;
            view.init(res);
        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
});

UI.on('changeHandle', function(e, avatar) {
    // console.log('changeHandle:', avatar);

    model.changeUserInfor({
        param: {
            user_id: user_id,
            avatar: {
                avatar: avatar
            }
        },
        success: function(res) {
            if (res.code == 0) {
                // console.log('res:', res);
                utils.hideLoading();
                utils.alertMessage(lang.lang5);

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    });


});