// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import imgUploader from 'uploadCloudinaryAvatar'
import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import 'fancybox'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import setting from 'setting'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(setting);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* upload img */
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number
/* upload img */


// 初始化
obj.init = function(res) {
    res.JumpName = lang.lang10;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/myProfile.html'; //避免登录后一直返回到登录页
    res.data.lang = lang;
    res.data.domainName = domainName;
    commonNav.init(res);

    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;
    if (res.data.email && res.data.email.length > 0) {

        // 1*@sina.com ==> 1******@sina.com
        let arr = res.data.email.split('@');
        let first = arr[0];
        let end = arr[1];
        let firstlength = first.length;

        // 只保留6位
        first = (firstlength > 7) ? first.substring(0, 7) : first;
        // 第一位保留，
        let str_1 = first.substring(0, 1);
        // 第二位后用*替换
        let str_2 = first.substring(1, firstlength + 1).replace(new RegExp(".", "g"), "*")

        res.data.email = str_1 + str_2 + (end ? ('@' + end) : '')
    }

    $UI.append(mainTpl(res.data)); //主模版
    fastclick.attach(document.body);

    utils.hideLoading();

    // initialize img upload
    imgUploader.create(uploadBtn, uploadList, uploadKey, uploadNumLimit, imgChangedHandler);

    // 设置avatar 头像
    if (res && res.data) {
        if (res.data.avatar.indexOf('http') == 0) {
            $('.avatar-img').attr('src', utils.imageChoose(res.data.avatar));
        } else {
            $('.avatar-img').attr('src', domainName.static + '/img/avatar/' + res.data.avatar);
        }
    }

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // signOut
    $('body').on('click', '#signOut', function(e) {
        // sessionStorage.setItem('OldHome', 5)
        e.preventDefault();
        let dbObj = JSON.stringify({
            accessToken: '',
            expiresIn: 0,
            refreshToken: '',
            tokenType: '',
            uid: ''
        });

        let isLocal = location.href.indexOf("pedulisehat.id") == -1;
        let tempDM = 'pedulisehat.id';
        if (isLocal) {
            tempDM = location.host;
        }

        $.cookie('passport', dbObj, {
            expires: 365,
            path: '/',
            domain: tempDM
        });

        location.href = '/';
    })
};

/**
 * img changed 监听事件
 */
function imgChangedHandler() {
    // console.log('img changed');
    $UI.trigger('changeHandle', imgUploader.getImageList(uploadList, uploadKey));
}

export default obj;