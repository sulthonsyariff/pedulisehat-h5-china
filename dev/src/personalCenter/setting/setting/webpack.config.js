var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'page',
    htmlFileURL: 'html/setting.html',
    appDir: 'js/setting',
    uglify: true,
    hash: '',
    mode: 'production'
})