// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import setting from 'setting'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(setting);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

/* upload img */
let uploadBtn = 'upload-btn'; // upload btn
let uploadList = '.uploader-list'; // img container
let uploadKey = 'cover'; // upload key
let uploadNumLimit = 8; // limit number
/* upload img */


// 初始化
obj.init = function(res) {
    res.JumpName = titleLang.setting;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '';
    res.data.lang = lang;
    res.data.domainName = domainName;
    commonNav.init(res);

    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;

    $UI.append(mainTpl(res.data)); //主模版
    fastclick.attach(document.body);


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });

    // signOut
    $('body').on('click', '#signOut', function(e) {
        // sessionStorage.setItem('OldHome', 5)
        e.preventDefault();
        var dbObj = JSON.stringify({
            accessToken: '',
            expiresIn: 0,
            refreshToken: '',
            tokenType: '',
            uid: ''
        });

        var isLocal = location.href.indexOf("pedulisehat.id") == -1;
        var tempDM = 'pedulisehat.id';
        if (isLocal) {
            tempDM = location.host;
        }

        $.cookie('passport', dbObj, {
            expires: 365,
            path: '/',
            domain: tempDM
        });

        location.href = '/';
    })
};


export default obj;