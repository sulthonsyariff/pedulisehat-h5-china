import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 获取用户信息
 */
obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/userInfo.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

/*
 修改用户信息
*/
obj.changeUserInfor = function(o) {
    var url = domainName.passport + '/v1/user/' + o.param.user_id;

    ajaxProxy.ajax({
        url: url,
        type: 'PUT',
        dataType: 'json',
        data: JSON.stringify(o.param.avatar),
        contentType: 'application/json;charset=utf-8'
    }, o);
}

export default obj;