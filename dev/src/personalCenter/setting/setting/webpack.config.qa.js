var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'page',
    htmlFileURL: 'html/setting.html',
    appDir: 'js/setting',
    uglify: true,
    hash: '',
    mode: 'development'
})