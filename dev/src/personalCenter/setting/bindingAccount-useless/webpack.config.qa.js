var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'page',
    htmlFileURL: 'html/bindingAccount.html',
    appDir: 'js/bindingAccount',
    uglify: true,
    hash: '',
    mode: 'development'
})