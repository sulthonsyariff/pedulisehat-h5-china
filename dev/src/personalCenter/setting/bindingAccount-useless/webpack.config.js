var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'page',
    htmlFileURL: 'html/bindingAccount.html',
    appDir: 'js/bindingAccount',
    uglify: true,
    hash: ''
})