// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'

/* translation */
import bindingAccount from 'bindingAccount'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(bindingAccount);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    res.JumpName = titleLang.bindingAccount;
    res.lang = lang;
    commonNav.init(res);

    $('title').html(titleLang.bindingAccount);

    $UI.append(mainTpl(res)); //主模版

    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
};

export default obj;