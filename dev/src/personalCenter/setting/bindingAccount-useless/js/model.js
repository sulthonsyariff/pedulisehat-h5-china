import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/**
 * 获取用户信息
 */
obj.getUserInfor = function(params) {
    var url = '/user/setting/info';
    if (isLocal || 1) {
        url = '/mock/userInfo.json';
    }

    ajaxProxy.ajax({
        url: url,
        type: 'GET',
        dataType: 'json'
    }, params);
}

export default obj;