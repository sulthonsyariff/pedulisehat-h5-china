import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.hideLoading();
view.init({});

model.getUserInfor({
    success: function(res) {
        if (res.code == 0) {
            // console.log('res:', res);
        } else {
            utils.alertMessage(res.msg)
        }

    },
    error: utils.handleFail
});