var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'findBackPassword',
    htmlFileURL: 'html/findBackPassword.html',
    appDir: 'js/findBackPassword',
    uglify: true,
    hash: '',
    mode: 'development'
})