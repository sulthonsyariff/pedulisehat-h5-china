import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// update password
obj.updatePassword = function(o) {
    let url = domainName.passport + '/v1/reset_password';

    ajaxProxy.ajax({
        type: 'PUT',
        url: url,
        data: JSON.stringify(o.param)
    }, o)
};

// login by password
obj.loginByPassword = function(o) {
    let url = domainName.passport + '/v1/login/bypassword';

    ajaxProxy.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(o.param)
    }, o)
};



export default obj;