// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import store from 'store'
import 'loading'
import '../less/main.less'


// 引入依赖
import utils from 'utils'

let UI = view.UI;

let reqObj = utils.getRequestParams();


// 隐藏loading
utils.hideLoading();
view.init({});

//登陆成功后获取存在本地的回跳页面地址
if (store.get('login') && store.get('login').qf_redirect_store) {
    jump_qf_redirect = decodeURIComponent(store.get('login').qf_redirect_store);
    console.log('jump_qf_redirect', jump_qf_redirect)
}

// 手机号登陆或注册
UI.on('sumbit', function(e, objData) {
    // utils.showLoading(lang.lang4);
    model.updatePassword({
        param: {
            mobile_country_code: objData.mobile_country_code,
            mobile: objData.mobile,
            code: objData.code,
            password: objData.password,
        },
        success: function(res) {
            if (res.code == 0) {
                passwordLogin(objData);
            }
        },
        error: utils.handleFail
    })

})

function passwordLogin(objData) {
    console.log('passwordLogin', objData)
    model.loginByPassword({
        param: {
            mobile_country_code: objData.mobile_country_code,
            mobile: objData.mobile,
            code: objData.code,
            password: objData.password,
            login_with: 'mobile'
        },
        success: function(res) {
            if (res.code == 0) {
                $.cookie('passport', JSON.stringify({
                    accessToken: res.data.accessToken,
                    expiresIn: res.data.expiresIn,
                    refreshToken: res.data.refreshToken,
                    tokenType: res.data.tokenType,
                    tokenExpires: res.data.tokenExpires,
                    uid: res.data.uid,
                    signature: res.data.signature,
                    serverTimestamp: (new Date()).getTime() // 获取登陆成功时当前时间，判断token是否过期时使用
                }), {
                    expires: 365,
                    path: '/',
                    domain: 'pedulisehat.id'
                })

                setTimeout(function() {
                    location.href = jump_qf_redirect;
                }, 30)
            }
        },
        error: utils.handleFail
    })
}