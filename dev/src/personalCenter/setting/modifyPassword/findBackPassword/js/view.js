// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import SMS from '../tpl/_SMS.juicer'
import validate from '../js/_validate'
// import sensorsActive from 'sensorsActive'

import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'

/* translation */
import setPassword from 'setPassword'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(setPassword);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;


// 初始化
obj.init = function (res) {
    res.JumpName = titleLang.ResetPassword;
    res.lang = lang;

    console.log('res---findback', res)
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/';
    commonNav.init(res);

    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;

    $UI.append(mainTpl(res.data)); //主模版
    clickHandle(res);
    fastclick.attach(document.body);

    $('.page-inner').append(SMS(res))

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

function clickHandle(res) {
    // login by phone
    $('body').on('click', '.confirmBtn', function (e) {
        console.log('submmit')
        let sumbitData = getSumbitParam();
        if (validate.check(sumbitData, lang)) {
            $UI.trigger('sumbit', [sumbitData]);
        }
    })
}

function getSumbitParam() {
    let mobile
    let mobile_country_code
    let code
    if (store.get('SMS')) {
        mobile = store.get('SMS').mobile;
        mobile_country_code = store.get('SMS').mobile_country_code;
        code = store.get('SMS').captcha
    } 

    return {
        mobile_country_code: mobile_country_code,
        mobile: mobile,
        code: code,
        password: $('.setPassword').val(),
        confirmPassword: $('.confirmPassword').val(),
    }
}


export default obj;