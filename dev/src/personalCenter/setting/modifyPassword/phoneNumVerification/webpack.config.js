var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'phoneNumVerification',
    htmlFileURL: 'html/phoneNumVerification.html',
    appDir: 'js/phoneNumVerification',
    uglify: true,
    hash: '',
    mode: 'production'
})