// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import validate from '../js/_validate'

import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'
import turingVerification from 'turingVerification'

/* translation */
import phoneBind from 'phoneBind'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(phoneBind);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let go = reqObj['go']

// 初始化
obj.init = function (res) {
    res.JumpName = titleLang.PhoneNumberVerification;
    // res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/';
    res.lang = lang;
    res.domainName = domainName;
    commonNav.init(res);

    res.go = go;
    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;
    console.log('res', res)
    $UI.append(mainTpl(res)); //主模版
    turingVerification.init(res);

    if (res.data && res.data.mobile) {
        $('#mobile').attr("readonly", "readonly").css("color", "#999")
    }
    fastclick.attach(document.body);

    clickHandle(res);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensorsActive.init();
    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};


function clickHandle(res) {
    //send code
    $('body').on('click', '.sendCode', function () {
        let sumbitData = getSumbitParam();
        let channel = 'WhatsApp';
        sumbitData.channel = channel;

        if ($('#mobile').val().length < 7) {
            utils.alertMessage(lang.lang1);
            return false;
        } else {
            turingVerification.nc_cb(sumbitData);
        }
    })

    //send SMS code
    $('body').on('click', '.SMSCode', function () {
        let sumbitData = getSumbitParam();
        let channel = 'Sms';
        sumbitData.channel = channel;
        if ($('#mobile').val().length < 7) {
            utils.alertMessage(lang.lang1);
            return false;
        } else {
            // turingVerification.init(sumbitData);
            turingVerification.nc_cb(sumbitData);
        }
    });

    //sumbit
    $('body').on('click', '.nextBtn', function () {
        let sumbitData = getSumbitParam();
        store.set('SMS', sumbitData)
        if (validate.check(sumbitData, lang)) {
            $UI.trigger('sumbit', [sumbitData]);
            utils.showLoading(lang.lang9)
        }
    })

}


function getSumbitParam() {
    let mobile;
    let sms_type;
    mobile = $('#mobile').val().replace(/\b(0+)/gi, "")
    if (go == 'findBackPassword') {
        sms_type = 'reset_password'
    } else if (go == 'modifyPassword') {
        sms_type = 'update_password'
    }
    return {
        mobile: mobile,
        mobile_country_code: 62,
        captcha: $('#captcha').val(),
        sms_type: sms_type,
    }
}


//倒计时
function invokeSettime(obj) {
    var countdown = 60;
    settime(obj);

    function settime(obj) {
        if (countdown == 0) {
            $(obj).attr("disabled", false).removeClass('active');
            $(obj).text(lang.lang5);
            countdown = 60;
            return;
        } else {
            $(obj).attr("disabled", true).addClass('active');
            $(obj).text("" + countdown + " s");
            countdown--;
        }
        setTimeout(function () {
            settime(obj)
        }, 1000)
    }
}

function invokeSMSSettime(obj) {
    var countSMSdown = 60;
    settime(obj);

    function settime(obj) {
        if (countSMSdown == 0) {
            $(obj).attr("disabled", false).css({'pointer-events':'auto','color':'#999'});
            // $(obj).text(lang.lang10);
            countSMSdown = 60;
            return;
        } else {
            $(obj).attr("disabled", true).css({'pointer-events':'none','color':'#ccc'});
            // $(obj).text("" + countSMSdown + " s");
            countSMSdown--;
        }
        setTimeout(function () {
            settime(obj)
        }, 1000)
    }
}

export default obj;