// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import store from 'store'
import turingVerification from 'turingVerification'

import 'loading'
import '../less/main.less'

/* translation */
import setting from 'setting'
import qscLang from 'qscLang'
let lang = qscLang.init(setting);
/* translation */
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let reqObj = utils.getRequestParams();
let go = reqObj['go']
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

// 隐藏loading
utils.hideLoading();

// if (user_id){
    model.getUserInfo({
        success: function(rs) {
            if (rs.code == 0) {
               
                view.init(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        unauthorizeTodo: function(rs) {
            // 加载主模版
            view.init({});
        },
        error: utils.handleFail,
    });
// }

// view.init({});

// 手机号登陆或注册
UI.on('sumbit', function (e, objData) {
    utils.showLoading(lang.lang4);

    model.SMSVerify({
        param: {
            mobile: objData.mobile,
            mobile_country_code: objData.mobile_country_code,
            captcha: objData.captcha,
            sms_type: objData.sms_type,
            channel: objData.channel
        },
        success: function (res) {
            if (res.code == 0) {
                if( go == 'findBackPassword'){
                    location.href = '/findBackPassword.html';
                } else if( go == 'modifyPassword'){
                    location.href = '/resetPassword.html?from=SMS';
                }
            }
        },
        error: utils.handleFail
    })

})

//sendCode
UI.on('sendCode', function (e, objData) {
    model.sendCode({
        param: {
            mobile: parseInt(objData.mobile),
            mobile_country_code: parseInt(objData.mobile_country_code),
            sms_type: objData.sms_type,
            channel: objData.channel,
            token: objData.token,
            session_id: objData.session_id,
            sig: objData.sig,
        },
        success: function (rs) {
            if (rs.code == 0) {

                if(objData.channel == 'WhatsApp'){
                    turingVerification.invokeWhatsAppSettime();
                } else {
                    turingVerification.invokeSMSSettime();
                }
                // turingVerification.invokeWhatsAppSettime();
                // turingVerification.changeBtn();
         
                utils.hideLoading();
            } else if(rs.code == 4151){
                utils.alertMessage(rs.msg)
                // turingVerification.updateTuringVerifyImg();
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
})