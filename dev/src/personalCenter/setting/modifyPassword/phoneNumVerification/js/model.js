import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

//短信验证修改密码
obj.SMSVerify = function (o) {
    console.log('o', o)
    var url = domainName.passport + '/v1/sms/verify?mobile=' + o.param.mobile + '&mobile_country_code=' + o.param.mobile_country_code + '&sms_type=' + o.param.sms_type + '&code=' + o.param.captcha;
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}


//发送验证码
obj.sendCode = function (o) {
    var url = domainName.passport + '/v3/sms/captcha?mobile=' + o.param.mobile + '&mobile_country_code=' + o.param.mobile_country_code + '&sms_type=' + o.param.sms_type  + '&channel=' + o.param.channel + '&token=' + o.param.token + '&session_id=' + o.param.session_id + '&sig=' + o.param.sig;
    // var url = domainName.passport + '/v2/sms/captcha?mobile=' + o.param.mobile + '&mobile_country_code=' + o.param.mobile_country_code + '&sms_type=' + o.param.sms_type + '&channel=' + o.param.channel + '&image_key=' + o.param.image_key + '&image_code=' + o.param.image_code;
    
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}


obj.getUserInfo = function (o) {
    let url = domainName.passport + '/v1/user';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};
export default obj;