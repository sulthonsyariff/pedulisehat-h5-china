// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import SMS from '../tpl/_SMS.juicer'
import validate from '../js/_validate'

import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import setPassword from 'setPassword'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(setPassword);
/* translation */

let [obj, $UI] = [{}, $('body')];
let qf_redirect_url;
if (store.get('login')) {
    qf_redirect_url = store.get('login').qf_redirect_store
    console.log('qf_redirect_url', decodeURIComponent(qf_redirect_url))
}
obj.UI = $UI;


// 初始化
obj.init = function(res) {
    res.JumpName = titleLang.SetPassword;
    res.lang = lang;

    console.log('res', res)

    // 设置密码非必须项，返回强制回到金光登录输入邮箱流程
    // judge sinarmas:金光登录输入邮箱流程需要
    if (store.get('from') == 'sinarmas') {
        res.commonNavGoToWhere = '/bindEmail.html';
    } else {
        if (qf_redirect_url) {
            res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : decodeURIComponent(qf_redirect_url);

        } else {
            res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/';
        }
    }

    commonNav.init(res);

    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;

    $UI.append(mainTpl(res.data)); //主模版
    clickHandle(res);
    fastclick.attach(document.body);

    $('.page-inner').append(SMS(res))

    utils.hideLoading();

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

function clickHandle(res) {
    // login by phone
    $('body').on('click', '.confirmBtn', function(e) {
        console.log('submmit')
        let sumbitData = getSumbitParam();
        if (validate.check(sumbitData, lang)) {
            $UI.trigger('sumbit', [sumbitData]);
        }
    })
}

function getSumbitParam() {
    return {
        password: $('.setPassword').val(),
        confirmPassword: $('.confirmPassword').val(),
    }
}


export default obj;