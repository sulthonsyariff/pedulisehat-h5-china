import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

// update password
obj.updatePassword = function(o) {
    let url = domainName.passport + '/v1/set_password';

    ajaxProxy.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(o.param)
    }, o)
};

/**
 * 获取用户信息
 */
obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/userInfo.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


export default obj;