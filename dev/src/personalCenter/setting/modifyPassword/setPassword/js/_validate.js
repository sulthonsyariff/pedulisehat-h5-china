/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    let passwordRules = '^[\w\x21-\x7e]{6,16}$'
    let regResult = param.password.match(passwordRules)
    
    if (regResult != null){
        $('.tips').css('color','#333')
    } else {
        $('.tips').css('color','#FF6100')
        return false;
    }
    // console.log('regResult',param.password,regResult)
    
    if (param.confirmPassword != param.password) {
        utils.alertMessage(lang.lang16);
        return false;
    }

    return true;
}

export default obj;