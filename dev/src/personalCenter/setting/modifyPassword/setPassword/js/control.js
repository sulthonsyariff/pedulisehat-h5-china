// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
import store from 'store'

// 引入依赖
import utils from 'utils'

let UI = view.UI;
let jump_url = store.get('login') ? decodeURIComponent(store.get('login').qf_redirect_store) : '/';
let email;

// 隐藏loading
utils.showLoading();
view.init({});

model.getUserInfo({
    success: function(rs) {
        if (rs.code == 0) {
            email = rs.data.email;
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
});

// 手机号登陆或注册
UI.on('sumbit', function(e, objData) {
    model.updatePassword({
        param: {
            password: objData.password,
        },
        success: function(res) {
            if (res.code == 0) {
                utils.showLoading();

                // 如果没有金光后缀的邮箱则跳转金光添加邮箱页面
                // judge sinarmas:金光登录输入邮箱流程需要
                if (store.get('from') == 'sinarmas' && email.indexOf('@sinarmas.co.id') == -1) {
                    setTimeout(function() {
                        location.href = '/bindEmail.html';
                    }, 30)
                } else {
                    setTimeout(function() {
                        location.href = jump_url;
                    }, 30)
                }

            }
        },
        error: utils.handleFail
    })

})