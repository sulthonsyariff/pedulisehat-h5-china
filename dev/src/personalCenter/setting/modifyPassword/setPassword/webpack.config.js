var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'setPassword',
    htmlFileURL: 'html/setPassword.html',
    appDir: 'js/setPassword',
    uglify: true,
    hash: '',
    mode: 'production'
})