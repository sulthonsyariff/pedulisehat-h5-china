var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'setPassword',
    htmlFileURL: 'html/setPassword.html',
    appDir: 'js/setPassword',
    uglify: true,
    hash: '',
    mode: 'development'
})