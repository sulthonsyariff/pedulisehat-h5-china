// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import modifyPassword from 'modifyPassword'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(modifyPassword);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;


// 初始化
obj.init = function(res) {
    res.JumpName = titleLang.ModifyPassword;
    // res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/';
    res.lang = lang;
    res.domainName = domainName;
    commonNav.init(res);

    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;

    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};



export default obj;