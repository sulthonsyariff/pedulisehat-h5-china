// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'


// 引入依赖
import utils from 'utils'

let UI = view.UI;
let user_id;

// 隐藏loading
utils.hideLoading();

model.getUserInfo({
    success: function(rs) {
        if (rs.code == 0) {
            
            view.init(rs);

        } else {
            utils.alertMessage(rs.msg)
        }
    },
    unauthorizeTodo: function(rs) {
        // 加载主模版
        view.init({});
    },
    error: utils.handleFail,
});


