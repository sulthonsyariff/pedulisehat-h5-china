var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'modifyPassword',
    htmlFileURL: 'html/modifyPassword.html',
    appDir: 'js/modifyPassword',
    uglify: true,
    hash: '',
    mode: 'production'
})