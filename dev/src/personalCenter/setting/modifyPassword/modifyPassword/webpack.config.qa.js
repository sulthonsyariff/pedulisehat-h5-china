var baseConfig = require('../../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'modifyPassword',
    htmlFileURL: 'html/modifyPassword.html',
    appDir: 'js/modifyPassword',
    uglify: true,
    hash: '',
    mode: 'development'
})