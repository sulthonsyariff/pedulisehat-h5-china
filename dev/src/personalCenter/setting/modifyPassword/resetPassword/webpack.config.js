var baseConfig = require('../../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'resetPassword',
    htmlFileURL: 'html/resetPassword.html',
    appDir: 'js/resetPassword',
    uglify: true,
    hash: '',
    mode: 'production'
})