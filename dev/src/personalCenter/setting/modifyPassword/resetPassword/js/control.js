// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import store from 'store'
import 'loading'
import '../less/main.less'

/* translation */
import setting from 'setting'
import qscLang from 'qscLang'
let lang = qscLang.init(setting);
/* translation */
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let user_id;

// 隐藏loading
utils.hideLoading();
view.init({});



// 手机号登陆或注册
UI.on('sumbit', function (e, objData) {
    // utils.showLoading(lang.lang4);
    console.log('objData',objData)
    model.updatePassword({
        param: {
            mobile_country_code: objData.mobile_country_code,
            mobile: objData.mobile,
            code: objData.code,
            new_password: objData.new_password,
            old_password: objData.old_password,
        },
        success: function (res) {
            if (res.code == 0) {
                store.remove('SMS')
                location.href = '/setting.html';
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })

})