// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import SMS from '../tpl/_SMS.juicer'
import oldPassword from '../tpl/oldPassword.juicer'
import validate from '../js/_validate'

import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import setPassword from 'setPassword'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(setPassword);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let from = reqObj['from']

// 初始化
obj.init = function (res) {
    res.JumpName = titleLang.ResetPassword;
    res.lang = lang;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '/';
    commonNav.init(res);

    $('title').html(titleLang.setting);

    // console.log('isInAndroid:', utils.browserVersion.android);

    $UI.append(mainTpl(res.data)); //主模版
    clickHandle(res);
    fastclick.attach(document.body);

    if (from == 'SMS') {
        $('.page-inner').append(SMS(res))
    } else {
        $('.page-inner').append(oldPassword(res))
    }

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

function clickHandle(res) {
    // login by phone
    $('body').on('click', '.confirmBtn', function (e) {
        let sumbitData = getSumbitParam();
        if (validate.check(sumbitData, lang)) {
            $UI.trigger('sumbit', [sumbitData]);
        }
    })
}

function getSumbitParam() {
    let mobile
    let mobile_country_code
    let code
    let old_password
    if (from == 'SMS') {
        mobile = store.get('SMS').mobile;
        mobile_country_code = store.get('SMS').mobile_country_code;
        code = store.get('SMS').captcha
        old_password = ''
    } else if (from == 'oldPassword'){
        mobile = ''
        mobile_country_code = 0
        code = ''
        old_password = $('.oldPassword').val()
    }
    return {
        mobile_country_code: mobile_country_code,
        mobile: mobile,
        code: code,
        old_password: old_password,
        new_password: $('.setPassword').val(),
        confirmPassword: $('.confirmPassword').val(),
    }
}

export default obj;