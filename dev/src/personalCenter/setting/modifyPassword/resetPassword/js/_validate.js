/*
 * 校验表单信息
 */
import utils from 'utils'
let obj = {};

obj.check = function(param, lang) {
    let passwordRules = '^[\w\x21-\x7e]{6,16}$'
    let regResult = param.new_password.match(passwordRules)
    
    // if (param.new_password == '') {
    //     utils.alertMessage('Password cannot be empty');
    //     return false;
    // }

    if (regResult != null){
        $('.tips').css('color','#333')
    } else {
        $('.tips').css('color','#FF6100')
        return false;
    }
    // console.log('regResult',param.password,regResult)
    
    if (param.confirmPassword != param.new_password) {
        utils.alertMessage(lang.lang14);
        return false;
    }

    return true;
}

export default obj;