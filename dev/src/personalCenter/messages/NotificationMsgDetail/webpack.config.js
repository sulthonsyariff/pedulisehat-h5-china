var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'NotificationMsgDetail',
    htmlFileURL: 'html/NotificationMsgDetail.html',
    appDir: 'js/NotificationMsgDetail',
    uglify: true,
    hash: '',
    mode: 'production'
})