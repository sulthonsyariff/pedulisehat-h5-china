// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import modifyName from 'modifyName'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyName);
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function(res) {
    res.JumpName = ''
    // res.commonNavGoToWhere = '/messages.html'
    res.lang = lang;
    commonNav.init(res);
    res.message_template = JSON.parse(res.data.message_template)
    res.data.diffTime = utils.msgTime(res.data.created_at)

    // console.log('res',res)
    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};




export default obj;