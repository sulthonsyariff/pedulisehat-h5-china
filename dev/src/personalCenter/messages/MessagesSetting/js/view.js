// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import messages from 'messages'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(messages);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
var reqObj = utils.getRequestParams();
var from = reqObj['from'];
let commonNavGoToWhere;
let followed_updated_select;
let donated_followed_updated_select;
let donated_select;
let audit_select;
// 初始化
obj.init = function (res) {


    if (from == "Updates") {
        commonNavGoToWhere = '/UpdatesMsg.html?head_type=1'
        res.JumpName = titleLang.UpdatesSetting;
        res.msgBtnTitle1 = lang.lang3
        res.msgBtnTitle2 = lang.lang4
        res.btnType1 = res.data[0].notify_type
        res.btnType2 = res.data[1].notify_type
        followed_updated_select = res.data[0].select
        donated_followed_updated_select = res.data[1].select
    } else if (from == "Notifications") {
        commonNavGoToWhere = '/Notifications.html?head_type=2'
        res.JumpName = titleLang.NotificationsSetting;
        res.msgBtnTitle1 = lang.lang1
        res.msgBtnTitle2 = lang.lang2
        res.btnType1 = res.data[0].notify_type
        res.btnType2 = res.data[1].notify_type
        donated_select = res.data[0].select
        audit_select = res.data[1].select
    }


    // res.JumpName = titleLang.messages;
    res.commonNavGoToWhere = commonNavGoToWhere
    res.lang = lang;
    res.domainName = domainName;
    commonNav.init(res);
    // console.log('res', res)
    $('title').html(titleLang.messages);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;

    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);

    // sensorsActive.init();

    //Updates setting
    $('body').on('click', '.followed_updated', function (e) {
        followed_updated_select = !followed_updated_select

        let param = {
            select: followed_updated_select,
            id: res.data[0].id
        }

        $(this).toggleClass('close');
        $(this).children().toggleClass('close');
        $UI.trigger('select', [param])
    });


    $('body').on('click', '.donated_followed_updated', function (e) {
        donated_followed_updated_select = !donated_followed_updated_select
        let param = {
            select: donated_followed_updated_select,
            id: res.data[1].id
        }
        
        $(this).toggleClass('close');
        $(this).children().toggleClass('close');
        $UI.trigger('select',[param])
    });

    //Notification setting
    $('body').on('click', '.donated', function (e) {
        donated_select = !donated_select
        let param = {
            select: donated_select,
            id: res.data[0].id
        }

        $(this).toggleClass('close');
        $(this).children().toggleClass('close');
        $UI.trigger('select',[param])
    });
    $('body').on('click', '.audit', function (e) {
        audit_select = !audit_select
        let param = {
            select: audit_select,
            id: res.data[1].id
        }

        $(this).toggleClass('close');
        $(this).children().toggleClass('close');
        $UI.trigger('select',[param])
    });


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};



export default obj;