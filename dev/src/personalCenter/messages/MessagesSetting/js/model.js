import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import mnsName from 'mnsName'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getMsgSettingConfig = function(o){
    let url = mnsName.mnsApi + "/v1/notify_setting?head_type=" + o.param.head_type

    ajaxProxy.ajax({
        type : 'get',
        url: url
    },o)
}


obj.updateMsgSettingConfig = function(o){
    let url = mnsName.mnsApi + "/v1/notify_setting"
    ajaxProxy.ajax({
        type : 'PUT',
        data : JSON.stringify(o.param),
        url: url
    },o)
}

export default obj;