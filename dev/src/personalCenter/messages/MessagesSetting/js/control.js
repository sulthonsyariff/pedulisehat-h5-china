// 公共库
import commonNav from 'commonNav'
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'


// 引入依赖
import utils from 'utils'

let UI = view.UI;
var reqObj = utils.getRequestParams();
var from = reqObj['from'];
let head_type;

if (from == "Updates") {
    head_type = 1
} else if (from == "Notifications") {
    head_type = 2
}

// let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

// 隐藏loading
utils.hideLoading();

model.getMsgSettingConfig({
    param: {
        head_type: head_type
    },
    success: function (res) {
        if (res.code == 0) {
            view.init(res);
        }
    }
})

// all read
UI.on('select', function(e, param) {
    // console.log('param',param)
    model.updateMsgSettingConfig({
        param: param,
        success: function(res) {
            if (res.code == 0) {
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})
