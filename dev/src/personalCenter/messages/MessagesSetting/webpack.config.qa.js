var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'messagesSetting',
    htmlFileURL: 'html/messagesSetting.html',
    appDir: 'js/messagesSetting',
    uglify: true,
    hash: '',
    mode: 'development'
})