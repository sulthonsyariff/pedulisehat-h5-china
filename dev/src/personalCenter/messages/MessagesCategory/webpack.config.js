var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'Messages Category',
    htmlFileURL: 'html/MessagesCategory.html',
    appDir: 'js/MessagesCategory',
    uglify: true,
    hash: '',
    mode: 'production'
})