// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'
import navBottom from 'navBottom'
import promoPopup from '../../../../../common/promoPopup/js/main'
import actionPromoPopup from '../../../../../common/promoPopup/js/_clickHandle'

/* translation */
import messages from 'messages'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(messages);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;



// 初始化
obj.init = function(res) {
    promoPopup.init()
    // console.log('MessagesCategory',res)
    res.JumpName = titleLang.messages;
    res.commonNavGoToWhere = '#';
    res.lang = lang;
    res.domainName = domainName;
    commonNav.init(res);

    // res.head_type = res.data.head_type
    $('title').html(titleLang.messages);

    // console.log('isInAndroid:', utils.browserVersion.android);
    // res.data.isInAndroid = utils.browserVersion.android ? true : false;

    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);

    // initialize img upload

    res.fromWhichPage = 'MessagesCategory.html';
    navBottom.init(res, 4); //nav bottom bar


    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });

    $('body').on('click', '#jumpToNotifications', function(e) {
        actionPromoPopup.click()
    });

    $('body').on('click', '#jumpToUpdates', function(e) {
        actionPromoPopup.click()
    });
};



export default obj;