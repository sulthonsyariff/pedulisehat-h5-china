// 公共库
import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'

/* translation */
import setting from 'setting'
import qscLang from 'qscLang'
let lang = qscLang.init(setting);
/* translation */
// 引入依赖
import utils from 'utils'

let UI = view.UI;
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';

// 隐藏loading
utils.hideLoading();

model.getMsgCount({
    params:{
        accessToken: accessToken
    },
    success: function (res) {
        // view.refreshMsgCount(JSON.parse(res));
        // console.log('control-success', res)
    }
});


// 获取用户信息
model.getMsgCategory({
    success: function(res) {
        if (res.code == 0) {
            // console.log('res',res)
            view.init(res);
        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
});
