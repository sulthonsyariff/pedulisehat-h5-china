import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import mnsName from 'mnsName'
import webSocket from 'webSocket'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getMsgCount = function(o){
    var wsUrl = mnsName.webSocket + "/v1/ws?token=" + o.params.accessToken
    webSocket.WS(wsUrl,o);
}

obj.getMsgCategory = function(o){
    let url = mnsName.mnsApi + "/v1/message_category"

    ajaxProxy.ajax({
        type : 'get',
        url: url
    },o)
}

export default obj;