var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'UpdatesMsgDetail',
    htmlFileURL: 'html/UpdatesMsgDetail.html',
    appDir: 'js/UpdatesMsgDetail',
    uglify: true,
    hash: '',
    mode: 'production'
})