import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let UI = view.UI;

var reqObj = utils.getRequestParams();
var message_id = reqObj['message_id'];

// 隐藏loading
utils.hideLoading();





model.getMsgDetail({
    param: {
        message_id: message_id
    },
    success: function (res) {
        if (res.code == 0) {
            view.init(res);
        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
})

// each read
model.eachRead({
    param: {
        message_id: message_id
    },
    success: function (res) {
        if (res.code == 0) {

        } else {
            // utils.alertMessage(res.msg)
        }
    },
    // error: utils.handleFail
})
