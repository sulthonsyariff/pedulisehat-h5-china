import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import mnsName from 'mnsName'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


// message detail
obj.getMsgDetail = function(o) {
    let url = mnsName.mnsApi + '/v1/message/' + o.param.message_id
    if (isLocal) {
        url = '/mock/userInfo.json';
    }
    ajaxProxy.ajax({
        url: url,
        type: 'GET',
    }, o);
}

// each read
obj.eachRead = function (o) {
    let url = mnsName.mnsApi + '/v1/message/' + o.param.message_id;

    ajaxProxy.ajax({
        url: url,
        type: 'PUT',
    }, o);
}


export default obj;