// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import 'fancybox'
// import sensorsActive from 'sensorsActive'

/* translation */
import modifyName from 'modifyName'
import qscLang from 'qscLang'
let lang = qscLang.init(modifyName);
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
/* translation */

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;
// 初始化
obj.init = function(res) {
    res.JumpName = ''
        // res.commonNavGoToWhere = '/messages.html'

    $('.fancybox').fancybox();

    res.lang = lang;
    commonNav.init(res);
    res.message_template = JSON.parse(res.data.message_template)
    if (res.message_template.images) {
        res.updateImages = JSON.parse(res.message_template.images)
    }
    res.data.diffTime = utils.msgTime(res.data.created_at)

    // console.log('message_template', res)
    // if (res.updateImages  && res.updateImages != "") {
    // for (let i = 0; i < res.updateImages.length; i++) {
    // let rightUrl = utils.imageChoose(rs.data.update.images[i])
    // let oldUpdateAddOverlay = utils.addOverlay(rs.data.update.images[i])
    // rs.data.update.images[i].rightUrl = rightUrl
    // rs.data.update.images[i].oldUpdateAddOverlay = oldUpdateAddOverlay
    // }

    // }

    $UI.append(mainTpl(res)); //主模版
    fastclick.attach(document.body);

    if (res && res.message_template && res.message_template.avatar) {
        if (res.message_template.avatar.indexOf('http') == 0) {
            $('.avatar').attr('src', utils.imageChoose(res.message_template.avatar));
        } else {
            $('.avatar').attr('src', domainName.static + '/img/avatar/' + rs.data.update.avatar);
        }
    }

    // sensorsActive.init();






    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};


// if (rs && rs.data.update && rs.data.update.avatar) {
//     if (rs.data.update.avatar.indexOf('http') == 0) {
//         $('.avatar-update').attr('src', rs.data.update.avatar);
//     } else {
//         $('.avatar-update').attr('src', domainName.static + '/img/avatar/' + rs.data.update.avatar);
//     }
// }


export default obj;