var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'Notifications',
    htmlFileURL: 'html/Notifications.html',
    appDir: 'js/Notifications',
    uglify: true,
    hash: '',
    mode: 'production'
})