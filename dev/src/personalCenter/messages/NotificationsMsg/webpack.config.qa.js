var baseConfig = require('../../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'Notifications',
    htmlFileURL: 'html/Notifications.html',
    appDir: 'js/Notifications',
    uglify: true,
    hash: '',
    mode: 'development'
})