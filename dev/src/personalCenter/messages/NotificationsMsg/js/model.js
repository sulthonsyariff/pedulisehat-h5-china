import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import mnsName from 'mnsName'
import domainName from 'domainName' // port domain name
import utils from 'utils'
let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


// message list
obj.getMsgList = function (o) {
    let url = mnsName.mnsApi + '/v1/messages?head_type=' + o.param.head_type + '&page=' + o.param.page

    ajaxProxy.ajax({
        url: url,
        type: 'GET',
    }, o);
}

// all read
obj.allRead = function (o) {
    let url = mnsName.mnsApi + '/v1/message_all';
    ajaxProxy.ajax({
        url: url,
        data: JSON.stringify(o.param),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8'
    }, o);
}

// each read
// obj.eachRead = function (o) {
//     let url = mnsName.mnsApi + '/v1/message/' + o.param.message_id;

//     ajaxProxy.ajax({
//         url: url,
//         type: 'PUT',
//     }, o);
// }

export default obj;