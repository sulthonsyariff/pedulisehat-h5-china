import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'
var reqObj = utils.getRequestParams();
var head_type = reqObj['head_type'];
// 隐藏loading

// let message_id;

utils.hideLoading();
var UI = view.UI;


view.init({head_type});

UI.on('needload', function (e, page) {
    model.getMsgList({
        param: {
            head_type:head_type,
            page: page
        },
        success: function (data) {
            if (data.code == 0) {
                view.insertData(data);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
});

// all read
UI.on('allRead', function(e, version) {
    model.allRead({
        param: {
            head_type: parseInt(head_type),
            version: version
        },
        success: function(res) {
            if (res.code == 0) {
                location.reload()
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
})


// each read
// UI.on('eachRead', function (e, message_id) {
//     model.eachRead({
//         param: {
//             message_id: message_id
//         },
//         success: function (res) {
//             if (res.code == 0) {
//             } else {
//                 // utils.alertMessage(res.msg)
//             }
//         },
//         error: utils.handleFail
//     })
// })