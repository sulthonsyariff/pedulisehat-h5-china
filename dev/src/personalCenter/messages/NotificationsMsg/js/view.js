// 公共库
import commonNav from 'commonNav'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import listItemTpl from '../tpl/listItem.juicer'
import qscScroll_timestamp from 'qscScroll_timestamp'
import utils from 'utils'
// import sensorsActive from 'sensorsActive'

/* translation */
import messagesList from 'messagesList'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(messagesList);
/* translation */

let is_first = true;
let page = 0;
let scroll_list = new qscScroll_timestamp();

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;

// 初始化
obj.init = function (res) {
    res.JumpName = titleLang.Notifications;
    res.commonNavGoToWhere = '/MessagesCategory.html'

    res.lang = lang;
    commonNav.init(res);

    $('title').html(titleLang.Notifications);

    $UI.append(mainTpl(res)); //主模版
    initScorll();

    clickHandle(res);
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();
    
    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function (e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

function clickHandle(res) {

    $('body').on('click', '#msg-setting', function () {
        location.href = '/messagesSetting.html?from=Notifications'
    })
}

function initScorll() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function () {
            $UI.trigger('needload', [++page])
        }
    });

    scroll_list.run();
};

obj.insertData = function (rs) {
    rs.lang = lang;
    if (rs.data) {
        console.log('insertData',rs.data)
        $('body').on('click', '#allread', function () {
            $UI.trigger('allRead', rs.data[0].version);
        })

        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;
        for (let i = 0; i < rs.data.length; i++) {
            rs.data[i].diffTime = utils.msgTime(rs.data[i].created_at)
        }

        $('.notifications_list').append(listItemTpl(rs));

        scroll_list.run();
    } else if (is_first) {
        // console.log('scorll-72', is_first)

        $('.loading').hide();
        $('.notifications_list').append(listItemTpl(rs));
    } else if (rs.data === null) {
        $('.loading').hide();

    }
}

export default obj;