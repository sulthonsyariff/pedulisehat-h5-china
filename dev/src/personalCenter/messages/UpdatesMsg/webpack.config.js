var baseConfig = require('../../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'UpdatesMsg',
    htmlFileURL: 'html/UpdatesMsg.html',
    appDir: 'js/UpdatesMsg',
    uglify: true,
    hash: '',
    mode: 'production'
})