var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'aboutUs',
    htmlFileURL: 'html/aboutUs.html',
    appDir: 'js/aboutUs',
    uglify: true,
    hash: '',
    mode: 'development'
})