import view from './view'
import model from './model'
import 'loading'
import '../less/about.less'
// 引入依赖
import utils from 'utils'
// import $ from 'jquery'

model.getAbout({
    success: function(res) {
        utils.hideLoading();
        view.init(res);
    },
})
// });