import mainTpl from '../tpl/about.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import aboutUs from 'aboutUs'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(aboutUs);
/* translation */

let [obj, $UI] = [{}, $('body')];

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
console.log('appVersionCode', appVersionCode)

obj.UI = $UI;

obj.init = function(res) {

    res.JumpName = titleLang.aboutUs;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '';

    commonNav.init(res);

    $('title').html(titleLang.aboutUs);

    res.lang = lang;
    $UI.append(mainTpl(res));

    // VersionCode>146 或者 VersionName > '1.1.2' 以上的安卓版本 去掉title
    if (appVersionCode > 146) {
        console.log('hide title')
        $('.commonNav').css('display', 'none')
    }

    // commonFooter.init(res);
    // console.log('view:', res);
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });

    $('.pp-content').html(res.description);


};


export default obj;