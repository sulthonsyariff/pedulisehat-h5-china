var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'supported campaigns',
    htmlFileURL: 'html/supportedCampaigns.html',
    appDir: 'js/supportedCampaigns',
    uglify: true,
    hash: '',
    mode: 'production'
})