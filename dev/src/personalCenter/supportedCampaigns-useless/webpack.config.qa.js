var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'supported campaigns',
    htmlFileURL: 'html/supportedCampaigns.html',
    appDir: 'js/supportedCampaigns',
    uglify: true,
    hash: '',
    mode: 'development'
})