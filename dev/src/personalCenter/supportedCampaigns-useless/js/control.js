import view from './view'
import model from './model'
import 'loading'
import '../less/list.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.hideLoading();

var UI = view.UI;

model.verify({
    success: function(res) {
        if (res.code == 0) {
            // console.log('judgeLogin', res);
            view.init({});
        } else {
            utils.alertMessage(res.msg)
        }
    },
    error: utils.handleFail
});


UI.on('needload', function(e, page) {
    // console.log('needload');
    model.getListData({
        param: {
            page: page
        },
        success: function(res) {
            if (res.code == 0) {
                view.insertData(res);
                // console.log('insertData', res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});