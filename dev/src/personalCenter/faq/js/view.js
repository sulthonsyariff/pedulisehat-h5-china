import mainTpl from '../tpl/faq.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'

/* translation */
import faq from 'faq'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(faq);
/* translation */

let [obj, $UI] = [{}, $('body')];


obj.UI = $UI;

obj.init = function(res) {
    res.JumpName = titleLang.faq;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '';

    commonNav.init(res);

    $('title').html(titleLang.faq);

    res.lang = lang;
    $UI.append(mainTpl(res));
    // commonFooter.init(res);
    // console.log('view:', res);
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);

    // sensorsActive.init();
    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};


export default obj;