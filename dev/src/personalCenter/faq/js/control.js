import view from './view'
import model from './model'
import 'loading'
import '../less/faq.less'
import 'freshchat.widget'

// 引入依赖
import utils from 'utils'
// import $ from 'jquery'


// window.fcWidget.init({
//     /* yc */
//     token: "2dc96525-57b9-40f7-8a43-7d2d9128c83e",
//     /* company */
//     token: "4125fbe8-bdd4-4f3d-ae59-aeed0cae24b4",
//     host: "https://wchat.freshchat.com",
//     faqTags : {
//         // Array of Tags
//         tags : ['donatur'],
//         //For articles, the below value should be article.
//         //For article category, the below value should be category.
//         filterType:'category' //Or filterType: 'article'
//       },
// });
// // console.log('faq',window.fcWidget.init)

// window.fcWidget.setFaqTags({
//     // For ex: ["public", "paid"]
//     tags : ['general'],
//     //For articles, the below value should be article.
//     //For article category, the below value should be category.
//     filterType:'category' //Or filterType: 'category'
//   },console.log('tags'));

// 隐藏loading
utils.hideLoading();
view.init({});

$("[property='og:url']").attr('content', '123');
$("[property='og:title']").attr('content', '123');
$("[property='og:description']").attr('content', '123');
$("[property='og:image']").attr('content', "https://static-qa.pedulisehat.id/js/homePage/../../img/homePage/banner_home_74fd6b27.png");