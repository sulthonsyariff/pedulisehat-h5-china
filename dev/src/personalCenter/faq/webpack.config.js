var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'faq',
    htmlFileURL: 'html/faq.html',
    appDir: 'js/faq',
    uglify: true,
    hash: '',
    mode: 'production'
})