var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'followed campaigns',
    htmlFileURL: 'html/followedCampaigns.html',
    appDir: 'js/followedCampaigns',
    uglify: true,
    hash: '',
    mode: 'development'
})