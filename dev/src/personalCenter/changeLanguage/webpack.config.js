var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'changeLanguage',
    htmlFileURL: 'html/changeLanguage.html',
    appDir: 'js/changeLanguage',
    uglify: true,
    hash: '',
    mode: 'production'
})