// 公共库
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import store from 'store'
import googleAnalytics from 'google.analytics'
import 'jq_cookie' //ajax cookie
// import sensorsActive from 'sensorsActive'

/* translation */
import changeLanguage from 'changeLanguage'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(changeLanguage);
/* translation */

let [obj, $UI] = [{}, $('body')];
let CACHED_KEY = 'switchLanguage';
let _lang = 'id';

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    res.JumpName = titleLang.changeLanguage;
    // res.commonNavGoToWhere = '/';
    commonNav.init(res);
    res.lang = lang;

    $UI.append(mainTpl(res)); //主模版

    // commonFooter.init(res);
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });

    // judge which language is right now
    let language = ['en', 'id'];
    for (let i = 0; i < language.length; i++) {
        if (store.get(CACHED_KEY) && store.get(CACHED_KEY).lang == language[i]) {
            $('.' + language[i]).addClass('choosed');
        } else if (!store.get(CACHED_KEY)) {
            $('.id').addClass('choosed');
        }
    }

    // indonesia / english
    $UI.on('click', 'p', function() {
        $('p').removeClass('choosed');
        $(this).addClass('choosed');

        _lang = $(this).hasClass('id') ? 'id' : 'en'

        store.set(CACHED_KEY, {
            lang: _lang
        });

        // GTRY-Pedulisehat-Id站点无法获取www.pedulisehate.id域名下localstorage中的语言的问题，估在cookie中存一份
        $.cookie(CACHED_KEY, _lang, {
            expires: 365,
            path: '/',
            domain: 'pedulisehat.id'
        });

        setTimeout(() => {
            location.reload();
        }, 100);

    });

};

export default obj;