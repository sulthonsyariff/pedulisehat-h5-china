var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'Language',
    htmlFileURL: 'html/changeLanguage.html',
    appDir: 'js/changeLanguage',
    uglify: true,
    hash: '',
    mode: 'development'
})