import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName'
import 'jq_cookie'

let obj = {};

// obj.getDonateMedal = function (o) {

//     let url = domainName.psuic + '/v1/ramadan/medal/donated';

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url,
//     }, o)
// }

// obj.getShareMedal = function (o) {

//     let url = domainName.psuic + '/v1/ramadan/medal/share';

//     ajaxProxy.ajax({
//         type: 'get',
//         url: url,
//     }, o)
// }
obj.getMedalList = function (o) {

    let url = domainName.psuic + '/v1/ramadan/medal';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}


export default obj;