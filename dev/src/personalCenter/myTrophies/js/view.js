import mainTpl from '../tpl/main.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'

import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'


import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'

/* translation */
import myTrophies from 'myTrophies'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(myTrophies);
/* translation */

let [obj, $UI] = [{}, $('body')];


obj.UI = $UI;

obj.init = function(res) {



    res.JumpName = lang.lang1;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '';

    // commonNav.init(res);

    $('title').html(lang.lang1);

    res.lang = lang;
    $UI.append(mainTpl(res));


    $('.donate-card-content').html(res.data.donated.requirements.replace(/\n|\r\n/g, "<br/>"))
    $('.share-card-content').html(res.data.share.requirements.replace(/\n|\r\n/g, "<br/>"))
    clickHandle(res);


    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();


};


function clickHandle(res) {

    // goback
    $UI.on('click', '.goback-icon', function(rs) {
        location.href = '/myProfile.html'
    })

    // donate box
    $UI.on('click', '.donate-wrap', function(rs) {
        if (res.data.donated.level != 'Green') {
            res.data.donated.isDonatedOrshare = 'list_donated'; // 'donated'
            res.data.donated.fromWhichPage = 'myTrophies.html'; //google analytics need distinguish the page name
            // console.log('alert-donate-box', res.data.donated)

            ramadanTrophiesAlertBox.init(res.data.donated);
            $('.common-trophies').css('display', 'block')
        }

    })

    // share box
    $UI.on('click', '.share-wrap', function(rs) {
        if (res.data.share.level != 'Green') {
            res.data.share.isDonatedOrshare = 'list_share'; // 'donated'
            res.data.share.fromWhichPage = 'myTrophies.html'; //google analytics need distinguish the page name
            // console.log('alert-share-box', res.data.share)

            ramadanTrophiesAlertBox.init(res.data.share);
            $('.common-trophies').css('display', 'block')
        }

    })

    // go to campaign list
    $UI.on('click', '.card-btn', function(rs) {
        location.href = '/campaignList.html?composite=1'
    })


}


export default obj;