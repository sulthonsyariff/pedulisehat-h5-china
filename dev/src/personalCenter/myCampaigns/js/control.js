import view from './view'
import model from './model'
import 'loading'
import '../less/list.less'
// 引入依赖
import utils from 'utils'
// import $ from 'jquery'

// 隐藏loading
utils.hideLoading();

var UI = view.UI;

model.verify({
    success: function(rs) {
        if (rs.code == 0) {
            // console.log('judgeLogin', rs);
            view.init({});
        } else {
            utils.alertMessage(rs.msg)
        }
    },
    error: utils.handleFail
});


UI.on('needload', function(e, page) {
    // console.log('needload');
    model.getListData({
        param: {
            page: page
        },
        success: function(data) {
            if (data.code == 0) {
                view.insertData(data);
                // console.log('insertData', data);
            } else {
                utils.alertMessage(data.msg)
            }
        },
        error: utils.handleFail
    })
});