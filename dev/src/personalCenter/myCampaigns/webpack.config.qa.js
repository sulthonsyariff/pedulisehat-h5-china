var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'my campaigns',
    htmlFileURL: 'html/myCampaigns.html',
    appDir: 'js/myCampaigns',
    uglify: true,
    hash: '',
    mode: 'development'
})