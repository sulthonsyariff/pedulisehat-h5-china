var baseConfig = require('../../../../base.config.js');

module.exports = baseConfig({
    htmlTitle: 'my campaigns',
    htmlFileURL: 'html/myCampaigns.html',
    appDir: 'js/myCampaigns',
    uglify: true,
    hash: '',
    mode: 'production'
})