var baseConfig = require('../../../../base.config.qa.js');

module.exports = baseConfig({
    htmlTitle: 'userLevel',
    htmlFileURL: 'html/userLevel.html',
    appDir: 'js/userLevel',
    uglify: true,
    hash: '',
    mode: 'development'
})