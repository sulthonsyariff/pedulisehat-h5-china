import mainTpl from '../tpl/userLevel.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import store from 'store'
// import sensorsActive from 'sensorsActive'

/* translation */
import userLevel from 'userLevel'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(userLevel);
/* translation */
let response;

let [obj, $UI] = [{}, $('body')];


obj.UI = $UI;

obj.init = function(res, type) {

    // GET USER INFO = 1
    if (type == 1) {
        response = res;
    }
    // GET LEVEL RULES = 2
    else if (type == 2) {
        response.dataRules = res.data;
    }
    // GET LEVEL POINT = 3
    else {
        response.dataLevel = res.data;
        user_points = res.data.total_points

        for (let i = 0; i < response.dataRules.length; i++) {
    
            if (user_points < response.dataRules[i].points) {
                response.level = i - 1
                response.title = response.dataRules[i - 1].name
                response.isHighest = false
                response.current_points = user_points,
                response.next_level_points = response.dataRules[i].points,
                appendToJuicer(response);
    
                return;
            } else if (user_points >= response.dataRules[response.dataRules.length - 1].points) {
                console.log('i', i)
                response.level = response.dataRules.length - 1
                response.title = response.dataRules[response.dataRules.length - 1].name
                response.isHighest = true
                response.current_points = user_points,
                response.next_level_points = 50000,
                appendToJuicer(response);
    
                return;
            }
        }
    }

    console.log('response ==',type, response);

};

function appendToJuicer(res) {
    res.progress = (res.current_points / res.next_level_points) * 100

    res.JumpName = lang.lang1;
    res.commonNavGoToWhere = utils.browserVersion.android ? 'native://close' : '';

    commonNav.init(res);

    $('title').html(titleLang.userLevel);

    res.lang = lang;
    $UI.append(mainTpl(res));
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
}


export default obj;