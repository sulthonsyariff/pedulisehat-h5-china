import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName'
import 'jq_cookie'

let obj = {};

obj.getLevelInstruction = function (o) {

    let url = domainName.psuic + '/v1/points_introduce';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}

obj.getUserLevelPoints = function(o) {
    let url = domainName.psuic + '/v1/user_level';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

obj.getLevelRules = function(o) {
    let url = domainName.psuic + '/v1/level_rules';
    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};

export default obj;