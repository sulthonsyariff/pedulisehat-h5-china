import view from './view'
import model from './model'
import 'loading'
import '../less/userLevel.less'
// 引入依赖
import utils from 'utils'
// import $ from 'jquery'

// 隐藏loading
utils.hideLoading();

// getLevelInstruction = 1
model.getLevelInstruction({
    success: function (rs) {
        if (rs.code == 0) {
            view.init(rs, 1);
            console.log('getLevelInstruction', rs)

            // getLevelRules = 2
            model.getLevelRules({
                success: function (rs) {
                    if (rs.code == 0) {
                        view.init(rs, 2);
                        console.log('getLevelRules', rs)

                        // getUserLevelPoints = 3
                        model.getUserLevelPoints({
                            success: function (rs) {
                                if (rs.code == 0) {
                                    view.init(rs, 3);
                                    console.log('getLevelPoint', rs)
                                } 
                            },
                            error: utils.handleFail
                        })
                        
                    } 
                },
                error: utils.handleFail
            })
            
        } 
    },
    error: utils.handleFail
})

