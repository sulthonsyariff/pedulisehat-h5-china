import view from './view'
import model from './model'
import insertData from './_insertData'
import insertDataSupporter from './_insertDataSupporter'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

// 隐藏loading
utils.hideLoading();

let UI = view.UI;


model.shareMark({
    param: {},
    success: function(rs) {
        if (rs.code == 0) {
            donatedCalendarPublic(rs);
        }
    },
    error: utils.handleFail
})


function donatedCalendarPublic(res) {
    model.donatedCalendarPublic({
        param: {},
        success: function(rs) {
            if (rs.code == 0) {
                rs.data.calendar = rs.data
                rs.data.share_items = res.data.share_items
                view.init(rs);
                insertData.init();
            }
        },
        error: utils.handleFail
    })
}

UI.on('resetTabs', function() {
    page = 1;
    model.getListData({
        param: {
            page: page
        },
        success: function(res) {
            if (res.code == 0) {
                insertData.insertData2(res);
                // console.log('insertData', res);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});


UI.on('needload', function(e, page) {
    if ($('.cont.history').hasClass('choosed')) {
        model.getListData({
            param: {
                page: page
            },
            success: function(res) {
                if (res.code == 0) {
                    insertData.insertData(res);
                    // console.log('insertData', res);
                } else {
                    utils.alertMessage(res.msg)
                }
            },
            error: utils.handleFail
        })
    }
});

UI.on('needloadSupporter', function(e, page) {
    console.log('ini page', page);
    console.log('need load supporter');
    model.getListDataSupporter({
        param: {
            page: page
        },
        success: function(res) {
            if (res.code == 0) {
                insertDataSupporter.combineAllData(res);
                // console.log('berapakali');
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
});
