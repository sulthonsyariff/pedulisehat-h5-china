// 公共库
import mainTpl from '../tpl/main.juicer'
import commonNav from 'commonNav'
// import commonFooter from 'commonFooter'
import utils from 'utils'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
// import sensorsActive from 'sensorsActive'
// import laydate from 'layui-laydate';
import domainName from 'domainName' // port domain name
import navBottom from 'navBottom'

/* translation */
import donateHistory from 'donateHistory'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateHistory);
/* translation */

import promoPopup from '../../../common/promoPopup/js/main'
import actionPromoPopup from '../../../common/promoPopup/js/_clickHandle'

let [obj, $UI] = [{}, $('body')];

let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
    // console.log('appVersionCode', appVersionCode)

let script = document.createElement("script");
script.type = "text/javascript";
script.src = 'https:' + domainName.static + '/laydate/laydate.js';

// var urlStr = location.host;
// if (urlStr.indexOf("pre.pedulisehat.id") != -1) {
//     script.src = 'http:' + domainName.static + '/laydate/laydate.js';
// } else {
//     script.src = 'https:' + domainName.static + '/laydate/laydate.js';
// }

document.getElementsByTagName('head')[0].appendChild(script);


obj.UI = $UI;
// 初始化
obj.init = function(res) {
    // donationDataHandle(res);

    res.JumpName = lang.lang_9;
    res.commonNavGoToWhere = '#';
    commonNav.init(res);

    $('title').html(lang.lang_9);

    res.lang = lang;
    res.getLanguage = utils.getLanguage();


    $UI.append(mainTpl(res));

    res.fromWhichPage = 'donationHistory.html';
    navBottom.init(res, 2); //nav bottom bar

    //计算当前月第一天的星期
    //  thisDate.setFullYear(dateTime.year, dateTime.month, 1);
    //  startWeek = (thisDate.getDay()+6)%7;


    laydate.render({
        elem: '#test-n1',
        position: 'static',
        lang: 'en',
        showBottom: false,
        // mark: {//标注重要日子--类型：Object，默认值：无
        //     '2020-4-24': '发布',
        //   }
        ready: function(date) { //控件在打开时触发，回调返回一个参数
            console.log(date); //得到初始的日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            bindData(res);

        },
        change: function(value, date, endDate) { //日期时间被切换后的回调
            console.log(value); //得到日期生成的值，如：2017-08-18
            console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
            console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
            bindData(res);

        },


    });

    minisizeCalendar(res);
    clickHandle(res);

    // VersionCode>146 或者 VersionName > '1.1.2' 以上的安卓版本 去掉title
    if (appVersionCode > 146) {
        $('.commonNav').css('display', 'none')
    }

    // commonFooter.init(res);
    fastclick.attach(document.body);
    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // sensorsActive.init();

    // control the height display correct when download link is closed
    if ($('.app-download').css('display') === 'block') {
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });

    promoPopup.init()


};

function clickHandle() {
    $('.layui-icon').click(function() {
        actionPromoPopup.click()
        bindData();
    })

    $('.expand').click(function() {
        actionPromoPopup.click()
        expandCalendar();
    })
    $('.minisize').click(function() {
        actionPromoPopup.click()
        minisizeCalendar();
    })

    $UI.on('click', '.tabs .tabs-item', function() {
        actionPromoPopup.click()
        if (!$(this).hasClass('choosed')) {
            $('.tabs .tabs-item').removeClass('choosed')
            $(this).addClass('choosed')

            $('.cont').removeClass('choosed');
            $('.cont.' + $(this).attr('data-tab')).addClass('choosed');

            if ($('.cont.history').hasClass('choosed')) {
                $UI.trigger('resetTabs');
                $UI.trigger('resetTabsSupporter');
            } else {
                $UI.trigger('needloadSupporter', [1]);
            }
        }
    })

    $UI.on('click', '#historyToDetail', function(e) {
        actionPromoPopup.click()
    });

    $UI.on('click', '#supportedToDetail', function(e) {
        actionPromoPopup.click()
    });
    
}

function minisizeCalendar() {
    $('.layui-laydate-header').addClass('hideHead').css('display', 'none')
    $('.expand').css('display', 'block')
    $('.minisize').css('display', 'none')
        // $('.layui-laydate-list').css('display','none')
    $('tbody>tr').each(function(i) { // 遍历 tr
        let thisWeek

        $(this).children('td').each(function(j) { // 遍历 tr 的各个 td
            if ($(this).hasClass('layui-this')) {
                thisWeek = i
            }
        });

        if (i != thisWeek) {
            $(this).css('display', 'none')
        }
    });
}

function expandCalendar() {

    $('.layui-laydate-header').css('display', 'block')
    $('.expand').css('display', 'none')
    $('.minisize').css('display', 'block')
    $('tbody>tr').css('display', 'table-row')

}

function bindData(rs) {

    let headM = $('.laydate-set-ym').children().eq(0).attr('lay-ym').split('-')[1]

    if (headM == 1) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang12)

    } else if (headM == 2) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang13)

    } else if (headM == 3) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang14)

    } else if (headM == 4) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang15)

    } else if (headM == 5) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang16)

    } else if (headM == 6) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang17)

    } else if (headM == 7) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang18)

    } else if (headM == 8) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang19)

    } else if (headM == 9) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang20)

    } else if (headM == 10) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang21)

    } else if (headM == 11) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang22)

    } else if (headM == 12) {
        $('.laydate-set-ym').children().eq(0).html(lang.lang23)

    }

    console.log('laydate-set-ym', headM)
        // console.log('rs', rs)

    let today = new Date(+new Date() + 8 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '')
    let todayY = today.split('-')[0]
    let todayM = today.split('-')[1].replace(/^0/, '')
    let todayD = today.split('-')[2].split(' ')[0]

    // console.log('today',todayY,todayM,todayD)



    $('thead>tr').children().eq(0).html(lang.lang_7)
    $('thead>tr').children().eq(1).html(lang.lang_1)
    $('thead>tr').children().eq(2).html(lang.lang_2)
    $('thead>tr').children().eq(3).html(lang.lang_3)
    $('thead>tr').children().eq(4).html(lang.lang_4)
    $('thead>tr').children().eq(5).html(lang.lang_5)
    $('thead>tr').children().eq(6).html(lang.lang_6)
    $('thead>tr').children().css('color', '#999')

    //展示斋月
    $('tr').each(function(i) { // 遍历 tr
        $(this).children('td').each(function(j) { // 遍历 tr 的各个 td

            let tdDate = $(this).attr('lay-ymd')
            let tdY = tdDate.split('-')[0]
            let tdM = tdDate.split('-')[1]
            let tdD = tdDate.split('-')[2]



            $(this).attr({
                    'tdm': tdM,
                    'tdd': tdD,
                    'tdy': tdY,
                })
                // console.log('this is today',todayD,tdD,$(this).attr('tdd') == todayD)

            if ($(this).attr('tdy') == todayY && $(this).attr('tdm') == todayM && $(this).attr('tdd') == todayD) {
                console.log('this is today', $(this))
                $(this).addClass('today')
            }

            // if($(this).attr('tdd') == todayD){
            //     console.log('remove',$(this))
            //     // $(this).removeClass('layui-this')
            // }

            if (($(this).attr('tdm') == 4 && $(this).attr('tdd') > 23) ||
                ($(this).attr('tdm') == 5 && $(this).attr('tdd') < 26)) {
                $(this).addClass('ramadanMark')
            }
        });
    });

    // 捐款次数绑定到每天中
    if (rs && rs.data && rs.data.calendar) {
        for (let i = 0; i < rs.data.calendar.length; i++) {
            // console.log('rs.data[i]=', i, rs.data.calendar[i])
            let _param = String(rs.data.calendar[i].created_at).split('T')[0];
            let _paramY = _param.split('-')[0]
            let _paramM = _param.split('-')[1].replace(/^0/, '')
            let _paramD = _param.split('-')[2].replace(/^0/, '')

            _param = _paramY + '-' + _paramM + '-' + _paramD

            console.log('_param', _param)
            $('tr').each(function(i) { // 遍历 tr
                $(this).children('td').each(function(j) { // 遍历 tr 的各个 td
                    if ($(this).attr('lay-ymd') == _param) {
                        $(this).addClass('donateMark')
                        console.log('_this', $(this))
                    }
                    // console.log("第" + (i + 1) + "行，第" + (j + 1) + "个td的值：" + $(this).text() + "。");
                    // console.log($(this).attr('lay-ymd'))
                });
            });
        }
    }


    // 分享次数绑定到每天中
    if (rs && rs.data && rs.data.share_items) {
        // console.log('----', rs.data.share_items)
        let shareArr = []
        let newShareArr = [];


        for (let i = 0; i < rs.data.share_items.length; i++) {

            let shareParam = String(rs.data.share_items[i].date).split('T')[0]

            let shareDateY = shareParam.split('-')[0]
            let shareDateM = shareParam.split('-')[1].replace(/^0/, '')
            let shareDateD = shareParam.split('-')[2].replace(/^0/, '')

            shareParam = shareDateY + '-' + shareDateM + '-' + shareDateD
            shareArr.push(shareParam)

        }

        for (var i = 0; i < shareArr.length; i++) {
            var items = shareArr[i];
            //判断元素是否存在于new_arr中，如果不存在则插入到new_ar中
            if ($.inArray(items, newShareArr) == -1) {
                newShareArr.push(items);
            }
        }

        for (var s = 0; s < newShareArr.length; s++) {
            $('tr').each(function(i) { // 遍历 tr
                $(this).children('td').each(function(j) { // 遍历 tr 的各个 td
                    if ($(this).attr('lay-ymd') == newShareArr[s]) {
                        $(this).addClass('shareMark')
                    }
                });
            });
        }
    }
}

export default obj;