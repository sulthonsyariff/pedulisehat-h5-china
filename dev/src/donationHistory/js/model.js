import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;


obj.shareMark = function(o) {

    let url = domainName.share + '/v1/share_item?begin=20180101&end=20250101';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};


obj.donatedCalendarPublic = function(o) {

    let url = domainName.psuic + '/v1/donated_calendar_public?begin=20180101&end=20250101';
    // let url = '../mock/donationCalendar.json';




    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

// 普通    12
// zakat 13
// bulk 14
// qurban 15
obj.getListData = function(o) {
    var url = domainName.trade + '/v1/my_support?page=' + o.param.page + '&category_id=12,13,14,15';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}

obj.getListDataSupporter = function(o) {
    var url = domainName.share + '/v1/share/list_supporter_per_user?page=' + o.param.page + '&start_date=2018-01-01T00:00:00%2B00:00&end_date=2025-01-01T00:00:00%2B00:00';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}


export default obj;