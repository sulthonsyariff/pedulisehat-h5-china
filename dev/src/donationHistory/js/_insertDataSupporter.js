// 公共库
import view from './view'
import utils from 'utils'
import qscScroll_timestamp from 'qscScroll_timestamp'
import listItemTpl from '../tpl/listItemSupporter.juicer'
import changeMoneyFormat from 'changeMoneyFormat'

import domainName from 'domainName' // port domain name

/* translation */
import donateHistory from 'donateHistory'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateHistory);
/* translation */

let UI = view.UI;
let is_first = true;
let page = 1;
let listSupporterRanking = {
    data: []
};
let scroll_list = new qscScroll_timestamp();

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;



// 初始化
obj.init = function (res) {
    //
};

obj.combineAllData = function (rs) {

    // push new data to existing data
    if (rs.data != null) {
        for (let index = 0; index < rs.data.supporters.length; index++) {
            listSupporterRanking.data.push(rs.data.supporters[index]);
        }

        console.log('listSupporterRanking', listSupporterRanking);

        page++;

        $UI.trigger('needloadSupporter', [page]);

    } else {
        insertData(listSupporterRanking)
    }
}

UI.on('resetTabsSupporter', function() {
    page = 1;
    listSupporterRanking = {
        data: []
    };    
});

function insertData (rs) {
    rs.domainName = domainName;
    rs.lang = lang;

    
    // console.log('apanih', rs.data);
    if (rs.data) {

        console.log('rs_data supporter', rs.data);

        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (let i = 0; i < rs.data.length; i++) {
            let rightUrl = utils.imageChoose(JSON.parse(rs.data[i].projectCover).image)
            rs.data[i].rightUrl = rightUrl
            // 金额
            rs.data[i].price = changeMoneyFormat.moneyFormat(rs.data[i].gatheredDonation);
        }

        console.log('rs supporter append',rs)

        $('.supported-list').html(listItemTpl(rs));
        $('.loading').hide();

        // scroll_list.run();

    } else if (is_first) {
        console.log('rs supporter --  null',rs)

        $('.loading').hide();
        $('.supported-list').html(listItemTpl(rs));
    } else if (rs.data.supporters === null) {
        console.log('rs supporter == null',rs)
        $('.loading').hide();
    }

    // ExploreBtn 点击事件 返回首页
    $UI.on('click', '.ExploreBtn', function(e) {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html';
    });
}

obj.userDate = function(created_at) {
    // console.log('before====:', created_at);

    let myDate = new Date(created_at);
    let year = myDate.getFullYear();
    let month = myDate.getMonth() + 1;
    let day = myDate.getDate();

    if (month < 10) {
        month = "0" + month;
    }

    if (day < 10) {
        day = "0" + day;
    }
    return day + '-' + month + '-' + year;
};


export default obj;