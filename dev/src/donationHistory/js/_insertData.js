// 公共库

import utils from 'utils'
import qscScroll_timestamp from 'qscScroll_timestamp'
import listItemTpl from '../tpl/listItem.juicer'
import changeMoneyFormat from 'changeMoneyFormat'

import domainName from 'domainName' // port domain name

/* translation */
import donateHistory from 'donateHistory'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(donateHistory);
/* translation */


let is_first = true;
let page = 0;
let scroll_list = new qscScroll_timestamp();

let [obj, $UI] = [{}, $('body')];
obj.UI = $UI;



// 初始化
obj.init = function (res) {
    initScorll();
};


function initScorll() {
    // console.log('scroll');
    scroll_list.config({
        wrapper: $UI,
        onNeedLoad: function() {
            $UI.trigger('needload', [++page])
        }
    });

    scroll_list.run();
};

obj.insertData = function(rs) {
    rs.domainName = domainName;
    rs.lang = lang;

    if (rs.data) {

        console.log('rs_data', rs.data);

        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (let i = 0; i < rs.data.length; i++) {
            //时间
            rs.data[i].created_at = obj.userDate(rs.data[i].created_at);
            //   图片
            rs.data[i].cover = JSON.parse(rs.data[i].cover);

            let rightUrl = utils.imageChoose(rs.data[i].cover)

            rs.data[i].rightUrl = rightUrl
            // 金额
            rs.data[i].price = changeMoneyFormat.moneyFormat(rs.data[i].price);
        }

        console.log('rsappend',rs)

        $('.project-list').append(listItemTpl(rs));

        scroll_list.run();

    } else if (is_first) {
        console.log('rs --  null',rs)

        $('.loading').hide();
        $('.project-list').append(listItemTpl(rs));
    } else if (rs.data === null) {
        console.log('rs==null',rs)
        $('.loading').hide();

    }

    // ExploreBtn 点击事件 返回首页
    $UI.on('click', '.ExploreBtn', function(e) {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html';
    });
}

// not append but replace html for reset tabs data
obj.insertData2 = function(rs) {
    page = 1;

    rs.domainName = domainName;
    rs.lang = lang;

    if (rs.data) {

        console.log('rs_data', rs.data);

        if (is_first) {
            rs.is_first = is_first;
        }
        is_first = false;

        for (let i = 0; i < rs.data.length; i++) {
            //时间
            rs.data[i].created_at = obj.userDate(rs.data[i].created_at);
            //   图片
            rs.data[i].cover = JSON.parse(rs.data[i].cover);

            let rightUrl = utils.imageChoose(rs.data[i].cover)

            rs.data[i].rightUrl = rightUrl
            // 金额
            rs.data[i].price = changeMoneyFormat.moneyFormat(rs.data[i].price);
        }

        console.log('rsappend',rs)

        $('.project-list').html(listItemTpl(rs));

        scroll_list.run();

    } else if (is_first) {
        console.log('rs --  null',rs)

        $('.loading').hide();
        $('.project-list').html(listItemTpl(rs));
    } else if (rs.data === null) {
        console.log('rs==null',rs)
        $('.loading').hide();

    }

    // ExploreBtn 点击事件 返回首页
    $UI.on('click', '.ExploreBtn', function(e) {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html';
    });
}

obj.userDate = function(created_at) {
    // console.log('before====:', created_at);

    let myDate = new Date(created_at);
    let year = myDate.getFullYear();
    let month = myDate.getMonth() + 1;
    let day = myDate.getDate();

    if (month < 10) {
        month = "0" + month;
    }

    if (day < 10) {
        day = "0" + day;
    }
    return day + '-' + month + '-' + year;
};


export default obj;