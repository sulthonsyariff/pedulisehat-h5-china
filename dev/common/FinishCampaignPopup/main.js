import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie
import utils from 'utils'
// import changeMoneyFormat from 'changeMoneyFormat'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

import "./less/main.less"
import tpl from "./tpl/main.juicer"

/****
 * 详情页推荐列表： 紧急项目专题里随机三个（ 有更多按钮 + 样式同紧急项目UI）
 * 1 代表详情页推荐规则， 2. 代表结束项目推荐规则
 */
obj.init = function(param) {
    let url = domainName.project + '/v1/projects/' + param.project_id + '/recommend?rule=2';

    if (isLocal) {
        url = '/mock/project_lists_' + 1 + '.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, {
        success: function(rs) {
            if (rs.code == 0 && rs.data) {
                // console.log('---FinishCampaignPopup---', rs.data);

                rs.lang = param.lang;
                rs.fromWhichPage = param.fromWhichPage

                // 详情列表字段返回不一样
                rs.data = rs.data.projects
                rs.data = rs.data[0];

                //跳转详情地址
                rs.data.detailHref = utils.browserVersion.android ?
                    'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.data.project_id + '&clear_top=true' :
                    '/' + rs.data.short_link;

                // 图片
                rs.data.cover = JSON.parse(rs.data.cover);
                rs.data.rightUrl = utils.imageChoose4_3(rs.data.cover)


                $('body').prepend(tpl(rs));



            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })

}

// close
$('body').on('click', '.FinishCampaignPopup .close', function(e) {
    $('.FinishCampaignPopup').css({
        'display': 'none'
    })
})


export default obj;