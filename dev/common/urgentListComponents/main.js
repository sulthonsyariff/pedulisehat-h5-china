import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

import "./less/main.less"
import tpl from "./tpl/main.juicer"

/* translation */
import home from 'home'
import qscLang from 'qscLang'
let lang = qscLang.init(home);
/* translation */

/****
 * 详情页推荐列表： 紧急项目专题里随机三个（ 有更多按钮 + 样式同紧急项目UI） rule  1 代表详情页推荐规则， 2. 代表结束项目推荐规则
 * 首页紧急推荐列表： 紧急项目专题里随机三个（ 有更多按钮 + 样式同紧急项目UI）
 */
obj.init = function(param) {
    let url;
    if (param && param.project_id) {
        url = domainName.project + '/v1/projects/' + param.project_id + '/recommend?rule=1&limit=3';
    } else {
        url = domainName.project + '/v1/group_projects?group_id=38&limit=3'
    }

    if (isLocal) {
        url = '/mock/project_lists_' + 1 + '.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, {
        success: function(rs) {
            if (rs.code == 0 && rs.data) {
                rs.fromWhichPage = param.fromWhichPage

                // 详情列表字段返回不一样
                if (param && param.project_id) {
                    rs.data = rs.data.projects
                }

                for (let i = 0; i < rs.data.length; i++) {
                    //时间
                    if (rs.data[i].closed_at) {
                        rs.data[i].timeLeft = utils.timeLeft(rs.data[i].closed_at);
                    }
                    // 进度条：设置基准的高度为1%，
                    // rs.data[i].progress = rs.data[i].current_amount / rs.data[i].target_amount * 100 + 1;

                    //   图片
                    rs.data[i].cover = JSON.parse(rs.data[i].cover);

                    rs.data[i].rightUrl = utils.imageChoose2_1(rs.data[i].cover)
                    rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar)

                    // 格式化金额
                    rs.data[i].target_amount = changeMoneyFormat.moneyFormat(rs.data[i].target_amount);
                    rs.data[i].current_amount = changeMoneyFormat.moneyFormat(rs.data[i].current_amount);

                    // 企业用户
                    rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';
                }

                rs.lang = lang;

                $('.urgent-campaigns_list').append(tpl(rs));


                // handle img UI
                $('.u-img').height($('.u-img').width() / 2);

                // handle user-name width
                $('.urgent-campaigns_list .user-name').each(function() {
                    let _parent = $(this).parents('.u-user');
                    let _parentW = $(_parent).width();
                    let _childrenW = $(_parent).find('.enterprise-wrap').width() +
                        $(_parent).find('.verified_wrap').width()

                    if ($(this).width() > _parentW - _childrenW - 14) {
                        $(this).css({
                            'width': _parentW - _childrenW - 14,
                            'overflow': 'hidden',
                            'white-space': 'nowrap',
                            'text-overflow': 'ellipsis',
                        })
                    }
                });

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })

}


export default obj;