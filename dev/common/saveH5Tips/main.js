import './main.less'
import tpl from './main.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import utils from 'utils'
import appDownload from 'appDownload'

/* translation */
import qscLang from 'qscLang'
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
// let inSafari;
// let inSafari =  /Safari/.test(navigator.userAgent) && !/Chrome/.test(navigator.userAgent);

function issafari() {
    // var ua = navigator.userAgent.toLocaleLowerCase();
    var ua = navigator.userAgent.toLowerCase();
    // if (/safari/.test(ua) && !/chrome/.test(ua)) {
    //     return inSafari = true
    // } else {
    //     return inSafari = false
    // }
    if (ua.indexOf('applewebkit') > -1 && ua.indexOf('mobile') > -1 && ua.indexOf('safari') > -1 &&
        ua.indexOf('linux') === -1 && ua.indexOf('android') === -1 && ua.indexOf('chrome') === -1 &&
        ua.indexOf('ios') === -1 && ua.indexOf('browser') === -1) {
        return true;
    } else {
        return false;
    }


}

fastclick.attach(document.body);

obj.init = function (rs) {

    console.log('inSafari', issafari());
    // var ua = navigator.userAgent.toLowerCase();
    // //判断是不是在iPhone的Safair浏览器打开的本页面
    // if (ua.indexOf('applewebkit') > -1 && ua.indexOf('mobile') > -1 && ua.indexOf('safari') > -1 &&
    //     ua.indexOf('linux') === -1 && ua.indexOf('android') === -1 && ua.indexOf('chrome') === -1 &&
    //     ua.indexOf('ios') === -1 && ua.indexOf('browser') === -1) {
    //     inSafari = true
    // }


    console.log('commonLang', commonLang);
    // rs.commonLang = commonLang;

    if (sessionStorage.getItem('close-save-tips') != 2 && issafari()) {
        $UI.prepend(tpl(commonLang)); // 带close的模版
    }
    // rs.domainName = domainName;
    setTimeout(() => {
        $('.save-wrapper').animate({
            //'padding-top': 'toggle',
            //'padding-bottom': 'toggle',
            opacity: "toggle",
            // height: "toggle" 
        }, 'slow');
    }, 5000)


    obj.event(rs);
}

obj.event = function (res) {


    $('body').on('click', '.close-save-tips', function () {
        $('.save-wrapper').css('display', 'none')

        sessionStorage.setItem('close-save-tips', 2)
    })
}

export default obj;