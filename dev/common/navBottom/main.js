import './main.less'
import tpl from './main.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import utils from 'utils'
import model from './model'

/* translation */
import qscLang from 'qscLang'
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');

let passport = $.cookie('passport') ? JSON.parse($.cookie('passport')) : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';

fastclick.attach(document.body);

obj.init = function(rs, whichNavBottom) {
    rs.whichNavBottom = whichNavBottom;
    rs.lang = commonLang

    $UI.append(tpl(rs));

    clickHandle(rs);

    console.log('==nav bottom===', rs);

    // 登录用户才调：
    if (rs && rs.data && rs.data.user_id) {
        getMsgCount();
    } else {
        // 有些调nav bottom的地方可能并没有调用user接口，不知道是否登录了，这时候需要判断
        judgeLogin();
    }
}

function judgeLogin() {
    model.getUserInfo({
        success: function(rs) {
            if (rs.code == 0) {
                if (rs && rs.data && rs.data.user_id) {
                    getMsgCount();
                }
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        unauthorizeTodo: function(rs) {
            console.log('unauthorizeTodo');
        },
        error: utils.handleFail,
    });
}


// messages
function getMsgCount() {
    model.getMsgCount({
        params: {
            accessToken: accessToken
        },
        success: function(res) {
            refreshMsgCount(JSON.parse(res));
        }
    });
}

function refreshMsgCount(res) {
    // if (res.count && res.count <= 0) {
    //     //
    // } else
    if (res.count && res.count > 0 && res.count <= 99) {
        $('.count').css('display', 'block')
        $('.count').html(res.count)
    } else if (res.count && res.count > 99) {
        $('.count').css({
            'display': 'block',
            'line-height': '11px'
        })
        $('.count').html('...')
    }
}

function clickHandle(rs) {
    // bottom-icons
    $('body').on('click', '.bottom-icon', function() {
        if (!$(this).hasClass('active')) {
            // change UI
            $('.bottom-icon').removeClass('active');
            $(this).addClass('active');

            //1: home
            if ($(this).hasClass('home')) {
                location.href = '/';
            }

            // 2:donation-history
            else if ($(this).hasClass('donation-history')) {
                jumpUrl(rs, '/donationHistory.html')
            }

            // 3:start-campaign
            else if ($(this).hasClass('start-campaign')) {
                let initiateJumpUrl = '/initiatePage.html'; //默认值

                if (rs && rs.data && rs.data.user_id) {
                    // zakat项目发起
                    if (rs.data.user_id == '12419737548001456892' || rs.data.user_id == '12419737548005254075') {
                        initiateJumpUrl = '/initiateChoose.html'
                    } else {
                        initiateJumpUrl = '/initiatePage.html'
                    }
                }

                jumpUrl(rs, initiateJumpUrl)
            }

            //4: messages
            else if ($(this).hasClass('messages')) {
                jumpUrl(rs, '/MessagesCategory.html');
            }

            // 5:my-profile
            else if ($(this).hasClass('my-profile')) {
                location.href = '/myProfile.html';
            }
        }
    })
}

function jumpUrl(rs, url) {
    // judge login
    if (rs.data) {
        location.href = url;
    } else {
        // location.href = '/login.html?qf_redirect=' + encodeURIComponent('/');
        location.href = '/login.html?qf_redirect=' + encodeURIComponent(location.href);
    }
}



export default obj;