import ajaxProxy from 'ajaxProxy'
import webSocket from 'webSocket'
import mnsName from 'mnsName'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    if (isLocal) {
        url = '../mock/userInfo.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};

obj.getMsgCount = function(o) {
    let wsUrl = mnsName.webSocket + "/v1/ws?token=" + o.params.accessToken
    webSocket.WS(wsUrl, o);
}
export default obj;