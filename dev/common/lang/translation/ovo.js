let ovoTranslation = {};

ovoTranslation.id = {
    lang1: 'Silahkan masukan no telepon yang benar',
    // lang2: 'No ponsel yang teregistrasi OVO',
    lang3: 'Jumlah',
    lang4: 'Order ID',
    lang6: 'Masukan no ponsel akun OVO anda',
    lang7: 'Bayar dengan OVO',
    lang8: 'Memuat...',
    lang9: 'Pastikan aplikasi OVO anda sudah siap/sudah berjalan dengan koneksi Internet yang baik',
    lang10: 'Bagaimana cara membayar?',
    lang11: '1. Buka aplikasi OVO.',
    lang12: '2. Cek notifikasi atau cari transaksi dari icon notifikasi pada OVO.',
    lang13: '3. Pilih metode pembayaran (OVO cash, OVO points, atau Split) lalu klik "Bayar".',
    lang14: '4. Selamat! Pembayaranmu sudah selesai.',


};
ovoTranslation.en = {
    lang1: 'Please enter valid phone number',
    // lang2: 'OVO registered phone number',
    lang3: 'Amount',
    lang4: 'Order ID',
    lang5: 'Pay Now with OVO',
    lang6: 'Please input your OVO registered phone number',
    lang7: 'Pay Now with OVO',
    lang8: 'Loading...',
    lang9: 'Please make sure your OVO application ready/already opened with good Internet connection',
    lang10: 'How to Pay?',
    lang11: '1. Please open OVO application.',
    lang12: '2. Check the notification or find the order from the notification icon inside OVO.',
    lang13: '3. Choose your payment method (OVO cash, OVO points, or Split) and click Pay.',
    lang14: '4. Congratulations your payment is done.',

};

export default ovoTranslation;