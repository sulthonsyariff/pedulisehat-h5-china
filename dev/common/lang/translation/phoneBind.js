let phoneBindTranslation = {};

phoneBindTranslation.id = {
    lang1: 'Silahkan masukan no telepon yang benar',
    lang2: 'Silahkan masukan kode yang benar',
    lang3: 'Nomor telepon',
    lang4: 'Kode Verifikasi',
    lang5: 'Kode WhatsApp',
    lang6: 'Masuk',
    lang7: 'Anda menyetujui dengan',
    lang8: 'Syarat & Ketentuan Penggunaan',
    lang9: 'Memuat....',
    lang10: 'Silahkan menyetujui semua kebijakan',
    lang11: 'dan',
    lang12: 'Kebijakan Privasi',
    lang13: 'Masukan nomor telepon anda',
    lang14: 'Lanjut',
    lang15: 'Silakan verifikasi nomor ponsel Kamu',
    lang16: 'Tidak punya WhatsApp? Kirim Kode ke SMS',
    lang24: 'Kode SMS telah dikirim',

};
phoneBindTranslation.en = {
    lang1: 'Please enter valid phone number',
    lang2: 'Please enter the correct  code ',
    lang3: 'Phone number',
    lang4: 'Verification Code',
    lang5: 'WhatsApp Code',
    lang6: 'Sign in',
    lang7: 'You are agreeing with',
    lang8: 'Terms & Conditions of Use',
    lang9: 'Loading....',
    lang10: 'Please agree with all policy',
    lang11: 'and',
    lang12: 'Privacy Policy',
    lang13: 'Please enter your phone number',
    lang14: 'Next',
    lang15: 'Please verify Your phone number',
    lang16: 'No WhatsApp？Send SMS code',
    lang24: 'SMS code has been sent',

};

export default phoneBindTranslation;