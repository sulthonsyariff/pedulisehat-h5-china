let myCampaignsTranslation = {};

myCampaignsTranslation.id = {
    lang1: 'Memuat',
    lang2: 'Aktif',
    lang3: 'Selesai',
    lang4: 'Belum ada konten',
    lang5: 'Jelajahi',
    lang6: 'Kampanye saya',
    lang7: 'Gagal',
    lang8: 'Sukses',
    lang9: 'Dibekukan',
};
myCampaignsTranslation.en = {
    lang1: 'Loading',
    lang2: 'Active',
    lang3: 'Finished',
    lang4: 'There is no content yet',
    lang5: 'Explore',
    lang6: 'My Campaigns',
    lang7: 'Failed',
    lang8: 'Succeed',
    lang9: 'Freezed',
};

export default myCampaignsTranslation;