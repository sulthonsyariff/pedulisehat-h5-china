let sinarmasTranslation = {};

sinarmasTranslation.id = {
    lang1: 'Detail Transaksi',
    lang2: 'Jumlah',
    lang3: 'No Virtual Account',
    lang4: 'Salin',
    lang5: 'Merchant',
    lang6: 'Transaksi ini akan berakhir pada',
    lang7: 'Simpan Sebagai Gambar',
    lang8: 'Kembali ke Halaman Kampanye',
    lang9: 'Tersalin ke clipboard',
    lang10: 'Memuat...',
    // lang11: 'Tekan lama pada gambar dan pilih "Download gambar"',
    lang11: 'Long press save image',
    lang12: 'Lihat Semua Kampanye',
    lang28: "Selesai",

    lang93: "Sisa Waktu",

    _lang3: 'Waktu Berlalu',
    _lang4: 'Hari',
    _lang5: 'Hari',
    _lang6: 'Terkumpul',
    _lang9: 'Target',

    lang_check: 'Cek Status Pesananku',

};
sinarmasTranslation.en = {
    lang1: 'Order Transaction Detail',
    lang2: 'Amount',
    lang3: 'Virtual Account Number',
    lang4: 'Copy',
    lang5: 'Merchant',
    lang6: 'This order will end at',
    lang7: 'Save as Photo',
    lang8: 'Back to Campaign Page',
    lang9: 'Copied to clipboard',
    lang10: 'Loading...',
    // lang11: 'Long press on image and choose "Download image" ',
    lang11: 'Long press save image',
    lang12: 'View All Campaigns',
    lang28: "Finished",

    lang93: "Time Left",

    _lang3: 'Time Past',
    _lang4: 'Day',
    _lang5: 'Days',
    _lang6: 'Funded',
    _lang9: 'Goal',

    lang_check: 'Check My Order Status',
};

export default sinarmasTranslation;