let goPayUnFinishTranslation = {};

goPayUnFinishTranslation.id = {
    lang1: 'Donasi Belum Selesai',
    lang2: 'Coba Bayar Lagi',
    lang3: 'Tinjau Kembali Kampanye',
};
goPayUnFinishTranslation.en = {
    lang1: 'Donation Unfinished!',
    lang2: 'Pay again',
    lang3: 'Review the Campaign',
};

export default goPayUnFinishTranslation;