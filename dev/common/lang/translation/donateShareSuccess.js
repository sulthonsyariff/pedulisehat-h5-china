let homeTranslation = {};

homeTranslation.id = {
    lang1: 'Kampanye Terkini',
    lang2: 'Memuat',
    lang3: 'Waktu Berlalu',
    lang4: 'Hari',
    lang5: 'Hari',
    lang6: 'Terkumpul',
    lang7: 'Tidak ada data',
    lang8: 'Terima kasih telah membagikan kampanye',
    lang9: 'Target',
    lang10: 'kampanye penggalangan dana lain juga butuh bantuan Anda. Ayo lihat juga disini!',
    lang12: 'Selengkapnya',
    lang13: 'Kampanye',
    lang14: 'Lihat Semua Kampanye',
    lang18: 'Copyright © 2019 Peduli Sehat Gotongroyong',
    lang28: "Selesai",
   
    lang30: 'Tinjau Kampanye >>',
    lang93: "Sisa Waktu"

};
homeTranslation.en = {
    lang1: 'Popular Campaigns',
    lang2: 'Loading',
    lang3: 'Time Past',
    lang4: 'Day',
    lang5: 'Days',
    lang6: 'Funded',
    lang7: 'no data',
    lang8: 'Thank you for sharing campaign',
    lang9: 'Goal',
    lang10: 'these fundraising campaigns also need your help, see more!',
    lang12: 'Details',
    lang13: 'Campaigns',
    lang14: 'View All Campaigns',
    lang18: 'Copyright © 2019 Peduli Sehat Gotongroyong',
    lang28: "Finished",
    
    lang30: 'Review the Campaign >>',
    lang93: "Time Left"

};

export default homeTranslation;