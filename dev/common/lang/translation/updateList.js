let updateListTranslation = {};

updateListTranslation.id = {
    lang1: 'Perbarui',
    lang2: 'Memuat',
    lang3: 'menit lalu',
    lang4: 'Belum ada konten',
    lang5: 'Jelajahi',
    lang6: 'Pembaruan saya',
};
updateListTranslation.en = {
    lang1: 'Updates',
    lang2: 'Loading',
    lang3: 'minutes ago',
    lang4: 'There is no content yet',
    lang5: 'Explore',
    lang6: 'My Updates',
};

export default updateListTranslation;