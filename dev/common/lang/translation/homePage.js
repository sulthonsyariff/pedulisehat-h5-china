let homePageTranslation = {};

homePageTranslation.id = {
    lang1: 'Kampanye Terkini',
    lang2: 'Memuat',
    lang3: 'Waktu Berlalu',
    lang4: 'Hari',
    lang5: 'Hari',
    lang6: 'Terkumpul',
    lang7: 'Tidak ada data',
    lang8: 'Blog',
    lang9: 'Target',
};
homePageTranslation.en = {
    lang1: 'Popular Campaigns',
    lang2: 'Loading',
    lang3: 'Time Past',
    lang4: 'Day',
    lang5: 'Days',
    lang6: 'Funded',
    lang7: 'no data',
    lang8: 'Blog',
    lang9: 'Goal',
};

export default homePageTranslation;