let settingTranslation = {};

settingTranslation.id = {
    lang1: 'Foto Profil',
    lang2: 'Nama Kamu',
    lang3: 'Nomor Telepon',
    lang4: 'Jilid akun',
    lang5: 'Sukses diubah',
    lang6: 'Pengaturan',
    lang7: 'Bahasa',
    lang8: 'Ubah Password',
    lang9: 'Keluar',
    lang10: 'Akun ku',
    lang11: 'E-mail',
    lang12: 'Tambah',
};
settingTranslation.en = {
    lang1: 'Profile Picture',
    lang2: 'Your name',
    lang3: 'Phone number',
    lang4: 'Binding account',
    lang5: 'Successful modify',
    lang6: 'Setting',
    lang7: 'Language',
    lang8: 'Modify Password',
    lang9: 'Sign Out',
    lang10: 'My Account',
    lang11: 'E-mail',
    lang12: 'Add',
};

export default settingTranslation;