let modifyGoalTranslation = {};

modifyGoalTranslation.id = {
    lang4: 'Belum ada konten',
    lang5: 'Notifikasi',
    lang6: 'Pembaruan',
};
modifyGoalTranslation.en = {
    lang4: 'There is no content yet',
    lang5: 'Notifications',
    lang6: 'Updates',
};

export default modifyGoalTranslation;
