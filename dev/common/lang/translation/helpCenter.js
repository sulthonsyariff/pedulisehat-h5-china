﻿let hospitalTranslation = {};

hospitalTranslation.id = {
    lang1: 'Pusat Bantuan',
    lang2: 'Apakah saya dapat menarik dana saya?',
    lang3: 'Anda dapat menarik dana anda setelah kampanye anda terverifikasi dan dipastikan bahwa itu telah berakhir.',
    lang4: 'Apakah ada batas waktu pada kampanye saya?',
    lang5: 'Tidak, Peduli Sehat tidak akan menetapkan batas waktu untuk kampanye Anda. Anda dapat mengakhirinya kapan pun Anda tidak membutuhkan donasi lagi. Namun, perbarui status kampanye Anda sesering mungkin, karena sebagian besar donatur ingin tahu tentang status terbaru dari kampanye Anda.',
    lang6: 'Perubahan apa yang dapat saya lakukan pada kampanye saya?',
    lang7: 'Anda dapat mengedit: memperbarui status, jumlah target, cerita kampanye, atau mengakhiri kampanye.',
    lang8: 'Bagaimana jika target donasi saya tidak tercapai?',
    lang9: 'Setelah kampanye Anda terverifikasi, Anda dapat menarik donasi kapan saja meskipun target belum tercapai.',
    lang10: 'Mengapa ada biaya platform sebesar 5%?',
    lang11: 'Biaya platform dikenakan untuk aktifitas operasional dan pengembangan teknologi, dengan tujuan menjaga kualitas pelayanan terbaik.',
    lang12: 'Dapatkah saya mendapatkan pengembalian dana, jika kampanye yang saya donasikan dibatalkan? ',
    lang13: 'Dalam hal demikian, donasi Anda akan secara acak disumbangkan ke kampanye lain yang memenuhi syarat.',
    lang14: 'Terlepas dari biaya platform sebesar 5%, apakah ada biaya tambahan dikurangi dari donasi?',
    lang15: 'Akan ada biaya administrasi transfer, yang dapat bervariasi sesuai dengan kebijakan setiap metode pembayaran.'
};
hospitalTranslation.en = {
    lang1: 'Help Center',
    lang2: 'How do I withdraw my fund?',
    lang3: 'You can withdraw your fund after your campaign passed the verification and make sure it has been ended.',
    lang4: 'Is there a time limit for my campaign?',
    lang5: 'No, Peduli Sehat will not set a time limit for your campaign. You can end it whenever you do not need donations any more. However, please update your campaign status frequently, because most donors would like to know about the recent status of your campaign.',
    lang6: 'What changes can I make to my campaigns?',
    lang7: 'You can edit: update status, target amount, campaign story, or end the campaign.',
    lang8: 'What if my donation target is not achieved?',
    lang9: 'After your campaign passed the verification, you can withdraw the donation at any time even if the target has not yet been reached. ',
    lang10: 'Why is 5% platform fee charged?',
    lang11: 'The platform fee is used for operational activities and technology development, in order to guaranteeing the up-to-date and quality services.',
    lang12: 'Can I get a refund, if the campaign I donated to is  cancelled?',
    lang13: 'In such case,  your donation will be randomly donated to other  qualified campaign.',
    lang14: 'Apart from 5% platform fee, is there any extra fee deducted from the donation?',
    lang15: 'There will be the charge of payment channel, which may vary according to the policy of each payment channel.'
};

export default hospitalTranslation;
