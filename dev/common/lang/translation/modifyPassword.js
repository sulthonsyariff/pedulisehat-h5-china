let modifyPassword = {};

modifyPassword.id = {
    lang1: 'Kode Verifikasi',
    lang2: 'Password Lama',
    lang3: 'Silahkan pilih salah satu opsi untuk melakukan identifikasi',
};
modifyPassword.en = {
    lang1: 'Verification Code',
    lang2: 'Old password',
    lang3: 'Please choose one to do the identification',
};

export default modifyPassword;
