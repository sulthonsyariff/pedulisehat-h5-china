let guidenceTranslation = {};

guidenceTranslation.id = {
    lang1: 'Persyaratan Untuk Pengajuan',
    lang2: 'Unpaid',
    lang3: 'Ajukan Bantuan',

    lang4: 'Pastikan anggota Gotongroyong telah memenuhi persyaratan',
    lang5: 'Syarat dan Ketentuan Gotongroyong Hadapi Penyakit Kritis',
    lang6: 'dan',
    lang7: 'Persyaratan Kesehatan',
    lang8: 'Jika anggota tidak memenuhi persyaratan, maka anggota tidak dapat mengajukan bantuan.',
    
    lang_4: 'Pastikan anggota Gotongroyong telah memenuhi persyaratan',
    lang_5: 'Syarat dan Ketentuan Gotongroyong Siaga Hadapi Kecelakaan',
    lang_6: 'dan',
    lang_7: 'Persyaratan Kesehatan dan Pekerjaan',
    lang_8: 'Jika anggota tidak memenuhi persyaratan, maka anggota tidak dapat mengajukan bantuan.',

    lang9: 'Pastikan anggota Gotongroyong punya seluruh dokumen relevan untuk membuktikan situasi, seperti KTP, kartu keluarga, sertifikat diagnosa rumah sakit, dan lain-lain.',
    
    lang10: 'Siapkan Dokumen',
    lang11: 'Siapkan KTP, kartu keluarga, sertifikat diagnosa dari rumah sakit, informasi rekening bank.',
    lang12: 'Ajukan untuk Bantuan Gotongroyong',
    lang13: 'Isi informasi sesuai yang tertera pada formulir.',
    lang14: 'Verifikasi',
    lang15: 'Tim Peduli Sehat akan memeriksa informasi anggota dan sertifikat.',
    lang16: 'Transparansi Publik',
    lang17: 'Apabila anggota lulus verifikasi, kami akan mengumumkan kepada seluruh anggota Gotongroyong selama 7 hari.',
    lang18: 'Transfer Bantuan Donasi Gotongroyong',
    lang19: 'Jika tidak ada penyanggahan saat publikasi, kami akan transfer bantuan donasi ke rekening bank anggota.',
    
    lang20: 'Lanjut',


    lang41: 'Setuju',

  
};
guidenceTranslation.en = {
    lang1: 'Requirements for Application',
    lang2: 'Unpaid',
    lang3: 'Apply for Aid',

    lang4: 'Make sure Gotongroyong member meets the requirements',
    lang5: 'Gotongroyong Facing Critical Diseases Terms and Agreements',
    lang6: 'and',
    lang7: 'Health Requirements',
    lang8: 'If the member doesn’t meet the requirements, then the  member can’t apply for aid.',
    
    lang_4: 'Make sure Gotongroyong member meets the requirements',
    lang_5: 'Gotongroyong Steady Facing Accidents Terms and Agreements',
    lang_6: '',
    lang_7: 'Health and Occupation Requirements',
    lang_8: ' If the member doesn’t meet the requirements, then the member can’t apply for aid.',

    lang9: 'Make sure the Gotongroyong member has all of the relevant documents to prove the situation, such as KTP card, family card, hospital diagnostic certificates, etc.',
    
    lang10: 'Prepare the documents',
    lang11: 'Prepare the KTP card, family card, hospital diagnostic certificates, bank account information.',
    lang12: 'Apply for Gotongroyong Aid',
    lang13: 'Fill in the information as required in the forms.',
    lang14: 'Verification',
    lang15: 'Peduli Sehat team will audit the member’s information and certificates.',
    lang16: 'Public Transparency',
    lang17: 'If the member passes the audition, we will inform it publicly it to all of Gotongroyong members for 7 days.',
    lang18: 'Transfer Gotongroyong Aid Donation',
    lang19: 'If there is no objection during publication, we will transfer the aid funds to the member’s bank account.',
    
    lang20: 'Next',

    lang41: 'Got it',


};

export default guidenceTranslation;