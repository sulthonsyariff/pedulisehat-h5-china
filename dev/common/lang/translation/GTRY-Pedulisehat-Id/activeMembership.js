let activeMembershipTranslation = {};

activeMembershipTranslation.id = {
    lang1: 'Aktifkan keanggotaanmu dengan melengkapi informasi verifikasi ini.',
    lang2: 'Ajukan Bantuan',
    lang3: 'hanya dapat diakses oleh anggota yang telah mengisi lengkap informasi verifikasi personal.',
    lang4: 'Kamu dapat menemukan nomor KTP anak (NIK) di kartu keluarga.',
    lang5: 'Nomor KTP',
    lang6: 'Nama sesuai KTP',
    lang7: 'Aktifkan Sekarang',

    lang8: 'Silakan isi nama sesuai KTP',
    lang9: 'Silakan masukan nomor KTP anda',
    lang10: 'Silakan isi no KTP yang benar',
    lang11: 'Silakan pilih jenis hubungan',
    lang12: 'Memuat...',
    lang13: 'Aktifkan Keanggotaan',

    lang21: 'Saya sendiri',
    lang22: 'Suami / Istri',
    lang23: 'Anak',
    lang24: 'Orang tua',
    lang25: 'Cucu',
    lang26: 'Kakek / Nenek',
    lang27: 'Kakak / Adik',
    lang28: 'Lainnya',
    lang29: 'Silakan pilih jenis hubungan',

};
activeMembershipTranslation.en = {
    lang1: 'Activate your membership by completing these verification fields.',
    lang2: 'Apply for Aid',
    lang3: 'can only be accessed by members who already done the personal information verification.',
    lang4: 'You can find kid’s KTP number (NIK) in the family card.',
    lang5: 'KTP Number',
    lang6: 'Name according to KTP',
    lang7: 'Activate Now',

    lang8: 'Please fill in the real name',
    lang9: 'Please enter your KTP number',
    lang10: 'Please fill in the correct ID card number',
    lang11: 'Please choose the relationship',
    lang12: 'Loading...',
    lang13: 'Activate Membership',

    lang21: 'Myself',
    lang22: 'Husband / Wife',
    lang23: 'Child',
    lang24: 'Parent',
    lang25: 'Grandchild',
    lang26: 'Grandparent',
    lang27: 'Brother / Sister',
    lang28: 'Other',
    lang29: 'Please choose the relationship',

};

export default activeMembershipTranslation;