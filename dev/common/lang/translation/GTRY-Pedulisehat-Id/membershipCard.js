let membershipCardTranslation = {};

membershipCardTranslation.id = {
    lang1: 'Gotongroyong Hadapi Penyakit Kritis',
    lang2: 'Belum Dibayar',
    lang3: 'Ajukan Bantuan',
    lang4: 'Periode Menunggu',
    lang5: 'Efektif',
    lang6: 'No KTP',
    lang7: 'Tanggal Bergabung',
    lang8: 'Tanggal Efektif',
    lang9: 'Teman yang Telah Ditolong',
    lang10: 'Transaksi',
    lang11: 'Cek',
    lang12: 'Memuat',
    lang13: 'Program GTRY ku',
    lang14: 'Saldo GTRY',
    lang15: 'Publikasi',
    lang16: 'Bonus Referral',
    // lang17: 'Ajak Keluarga Bergabung ke GTRY',
    lang17: 'Tambah Anggota Baru',

    lang18: 'Gotongroyong Siaga Hadapi Kecelakaan',
    lang19: 'Aktifkan Keanggotaan',
    lang20: 'Tidak Efektif',
    lang21: 'Data perlu dilengkapi',
    lang22: 'Tidak ada',

    lang23:'Gabung Lagi',
    lang24:'Berhenti',
    lang25:'Saldo Tidak Cukup',

};
membershipCardTranslation.en = {
    lang1: 'Gotongroyong Facing Critical Diseases',
    lang2: 'Unpaid',
    lang3: 'Apply for Aid',
    lang4: 'Waiting Period',
    lang5: 'Effective',
    lang6: 'ID Card',
    lang7: 'Joined Date',
    lang8: 'Effective Date',
    lang9: 'Helped People',
    lang10: 'Transactions',
    lang11: 'Check',
    lang12: 'Loading',
    lang13: 'My GTRY',
    lang14: 'GTRY Balance',
    lang15: 'Publication',
    lang16: 'Referral Bonus',
    // lang17: 'Join GTRY for My Family',
    lang17: 'Add a Member',

    lang18: 'Gotongroyong Steady Facing Accidents',
    lang19: 'Activate Membership',
    lang20: 'Not Effective',
    lang21: 'Data need to be completed',
    lang22: 'None',

    lang23:'Rejoin',
    lang24:'Quit',
    lang25:'Not enough balance',
};

export default membershipCardTranslation;