let transactionsTranslation = {};

transactionsTranslation.id = {
    lang1: 'Transaksi',
    lang2: 'Ronde ke 1',
    lang3: 'Bonus Referral',
    lang4: 'Donasi',
    lang5: '',
    lang6: '',
    lang7: '',
    lang8: '',
    lang9: '',
    lang10: '',
};
transactionsTranslation.en = {
    lang1: 'Transactions',
    lang2: '1nd Round',
    lang3: 'Referral Bonus',
    lang4: 'Apportion',
    lang5: '',
    lang6: '',
    lang7: '',
    lang8: '',
    lang9: '',
    lang10: '',
};

export default transactionsTranslation;