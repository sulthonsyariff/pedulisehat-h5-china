let gtryPublicityTranslation = {};

gtryPublicityTranslation.id = {
    lang1: 'KTP terverifikasi',
    lang2: 'Gotongroyong Hadapi Penyakit Kritis',
    lang3: 'Umur',
    lang4: 'Jenis Kelamin',
    lang5: 'Kota',
    lang6: 'Waktu Bergabung',

    lang7: 'Informasi Verifikasi',
    lang8: 'Sertifikat perawatan terverifikasi',
    lang9: 'Minta Bantuan Rp ',
    lang10: 'Mampu mengajukan permintaan bantuan',
    lang11: 'Program Terkait',

    lang12: 'Wanita',
    lang13: 'Pria',

    lang14: 'Laporan Gotongroyong',

    lang68: 'Gotongroyong Hadapi Penyakit Kritis',
    lang69: 'Perlindungan untuk 30 jenis penyakit kritis',
    lang70: 'Gabung Sekarang',
    lang71: 'Gotongroyong Siaga Hadapi Kecelakaan',
    lang72: 'Perlindungan untuk kecelakaan',
  
   
};
gtryPublicityTranslation.en = {
    lang1: 'ID card verified',
    lang2: 'Gotongroyong Facing Critical Diseases',
    lang3: 'Age',
    lang4: 'Gender',
    lang5: 'City',
    lang6: 'Join time',

    lang7: 'Verification Information',
    lang8: 'Treatment certification verified',
    lang9: 'Applied for Aid Rp ',
    lang10: 'Meet the apply for aid conditions',
    lang11: 'Program Related',

    lang12: 'Female',
    lang13: 'Male',
    lang14: 'Gotongroyong Publicity',

    lang68: 'Gotongroyong Facing Critical Diseases',
    lang69: 'Protection for 30 kinds of critical diseases',
    lang70: 'Join Now',
    lang71: 'Gotongroyong Steady Facing Accident',
    lang72: 'Protection for accident',
    
    lang99:'',
};

export default gtryPublicityTranslation;