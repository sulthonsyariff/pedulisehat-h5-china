let paySucceedTranslation = {};

paySucceedTranslation.id = {
    lang1: 'Pembayaran sukses',
    lang2: 'Cek Program GTRY ku',
    lang3: 'Bagikan',
    lang54: "Setiap kali ada tamu yang diundang untuk bergabung ke program GTRY dari share-mu, kami akan memberikan bonus masing-masing Rp 5.000 untukmu dan tamu yang bergabung ke saldo GTRY.",

};
paySucceedTranslation.en = {
    lang1: 'Payment succeed!',
    lang2: 'Check My GTRY',
    lang3: 'Share',
    lang54: "Every time a new user is invited to join the GTRY from your share, we will give Rp 5.000 bonus to you and the invitee both in form of GTRY balance.",

};

export default paySucceedTranslation;