let payFailedTranslation = {};

payFailedTranslation.id = {
    lang1: 'Pembayaran gagal',
    lang2: 'Coba Bayar Lagi',
};
payFailedTranslation.en = {
    lang1: 'Payment failed!',
    lang2: 'Pay again',
};

export default payFailedTranslation;