let diseaseInfoTranslation = {};

diseaseInfoTranslation.id = {
    lang1: 'Informasi Pasien',
    lang2: 'Informasi Penyakit',
    lang_2: 'Informasi Kecelakaan',
    lang3: 'Informasi Penerima Dana',

    lang4: 'Nama Penyakit',
    lang_4: 'Tipe Kecelakaan',
    lang5: 'Silakan masukan nama penyakit',
    lang_5: 'Silakan isi tipe kecelakaan',
    lang6: 'Nama Rumah Sakit',
    // lang7: 'Please choose the hospital',
    lang7: 'Silakan pilih kecamatan',

    lang8: 'Informasi Detil',
    lang9: 'Silakan deskripsikan secara detil alur dan jenis perawatan penyakit.',
    
    lang10: 'Sertifikat Diagnosa',
    lang11: 'Upload',

    lang12: 'Silakan sediakan foto atau hasil scan dari surat diagnosa dan',
    lang13: 'harus dicap oleh rumah sakit',
    lang14: '(laporan inspeksi, rekod medis, bukti perawatan, dan lain-lain).Kamu dapat upload lebih dari satu gambar.',
    
    lang15: 'Silakan masukan nama penyakit',
    lang_15: 'Silakan isi tipe kecelakaan',
    lang16: 'Silakan pilih kecamatan',
    lang_16: 'Silakan masukan nama rumah sakit',

    lang17: 'Silakan masukan informasi detil',
    lang18: 'Silakan upload sertifikat diagnosa',
    
    lang19: 'If there is no objection during publication, we will transfer the aid funds to the member’s bank account.',
    
    lang20: 'Lanjut',
    lang21: 'Memuat....',

    lang41: 'Setuju',
    lang42: 'Berhasil Diajukan',
    lang43: 'Mengerti',

    lang44: 'Provinsi',
    lang45: 'Kota',
    lang46: 'Kecamatan',

    lang47: "Provinsi",
    lang48: "Kota",
    lang49: "Nama Rumah Sakit",

    lang50:'Provinsi',
    lang51:'Kota',
    lang52:'Kecamatan',
    lang53:'Rumah Sakit',
};
diseaseInfoTranslation.en = {
    lang1: 'Patient Information',
    lang2: 'Disease Information',
    lang_2: 'Accident Information',
    lang3: 'Payee Information',

    lang4: 'Disease Name',
    lang_4: 'Accident Type',
    lang5: 'Please input the disease name',
    lang_5: 'Please input the accident type',
    lang6: 'Hospital Name',
    // lang7: 'Please choose the hospital',
    lang7: 'Please input the hospital',

    lang8: 'Detail Information',
    lang9: 'Please describe in detail the course and treatment for the disease. ',
    lang_9: 'Please describe in detail the course and treatment.',
    
    lang10: 'Diagnosis Certificate',
    lang11: 'Upload',

    lang12: 'Please provide photo or scan of diagnosis letters which',
    lang13: 'must be stamped by the hospital ',
    lang14: '(inspection report, medical history, proof of treatment, etc).You may upload more than one picture.',
    
    lang15: 'Please input the disease name',
    lang_15: 'Please input the accident type',
    lang16: 'Please choose the hospital',
    lang_16: 'Please input the hospital name',
    lang17: 'Please input the detail information',
    lang18: 'Please upload diagnosis certificate',
    
    lang19: 'If there is no objection during publication, we will transfer the aid funds to the member’s bank account.',
    
    lang20: 'Next',
    lang21: 'Loading....',

    lang41: 'Got it',
    lang42: 'Successfully Submitted ',
    lang43: 'Got it',


    lang44: 'Province',
    lang45: 'City',
    lang46: 'District',

    lang47: "Province",
    lang48: "City",
    lang49: "Hospital's name",

    lang50:'Province',
    lang51:'City',
    lang52:'District',
    lang53:'Hospital',
};

export default diseaseInfoTranslation;