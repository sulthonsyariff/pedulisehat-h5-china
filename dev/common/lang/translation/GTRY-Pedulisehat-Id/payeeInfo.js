let payeeInfoTranslation = {};

payeeInfoTranslation.id = {
    lang1: 'Informasi Pasien',
    lang2: 'Informasi Penyakit',
    lang_2: 'Informasi Kecelakaan',
    lang3: 'Informasi Penerima Dana',
 

    lang4: 'Hubungan Penerima Dana dengan Pasien',
    lang5: 'Pilih jenis hubungan',

    lang6: 'Pasien Sendiri',
    lang7: 'Hubungan Langsung (Keluarga)',
    lang8: 'Tutup',

    lang9: 'Informasi Transfer Bank',
    lang10: 'No Rekening Bank Penerima Dana',
    lang11: 'Silakan isi no rekening bank penerima dana',
    lang12: 'Nama Bank Penerima Dana',
    lang_12: 'Nama Bank Penerima Dana',
    lang13: 'Pilih bank',
    lang14: 'Nama Penerima Dana (Pemilik Akun Bank)',
    lang15: 'Nama lengkap penerima dana',
    lang16: 'Foto Halaman Depan Buku Bank',
    lang17: 'Upload',
    lang18: 'Klik disini unutk panduan halaman depan buku bank yang perlu diberikan.',
    lang19: 'Informasi Umum Penerima Dana',
    lang20: 'No KTP Penerima Dana (Pemilik Akun Bank)',
    lang21: 'Silakan masukan KTP penerima dana',
    lang22: 'No Telepon Penerima Dana (Pemilik Akun Bank)',
    lang23: 'Masukan no telepon penerima dana',
    lang24: 'Ajukan',
    lang25: 'Informasi Kartu Keluarga',
    lang26: 'Silakan upload foto kartu keluarga untuk membuktikan hubunganmu.',
    lang27: 'Klik disini untukpanduan bagaimana cara upload foto kartu keluarga.',
    
    
    lang28: "Silakan isi no rekening bank penerima dana",
    lang29: 'Silakan pilih nama bank',
    lang30: "Silakan isi nama lengkap penerima dana",
    lang31: 'Silakan upload foto buku bank',
    lang32: "Silakan masukan KTP penerima dana",
    lang33: 'Silakan isi no ponsel yang benar',
    lang34: 'Silakan upload foto kartu keluarga',

    lang35: 'Foto buku bank yang diupload harus jelas menunjukan informasi berikut:',
    lang36: '1. No rekening bank',
    lang37: '2. Pemilik akun bank',
    lang38: '3. Nama bank',
    lang39: '4. Cabang bank yang mengeluarkan buku bank tersebut',
    
    lang40: 'Foto kartu keluarga harus diupload jelas untuk menampilkan informasi berikut:',
    lang41: '1. Nomor kartu keluarga',
    lang42: '2. Nama anggota keluarga dan NIK',
    lang43: '3. Informasi hubungan dalam kartu keluarga',
    lang44: 'Mengerti',
    lang45: 'Memuat....',

  
};
payeeInfoTranslation.en = {
    lang1: 'Patient Information',
    lang2: 'Disease Information',
    lang_2: 'Accident Information',

    lang3: 'Payee Information',

    lang4: 'Payee relationship with the Patient',
    lang5: 'Choose a relationship',

    lang6: 'Patient Himself',
    lang7: 'Direct Relatives (Family)',
    lang8: 'Cancel',

    lang9: 'Bank Transfer Information',
    lang10: 'Payee Bank Account Number',
    lang11: 'Please fill in payee’s bank account number',
    lang12: 'Bank Name',
    lang_12: "Payee’s Bank",
    lang13: 'Choose bank',
    lang14: 'Payee’s Name (Bank Account Owner)',
    lang15: 'Fill in Payee’s full name',
    lang16: 'Bank Book Front Page Photo',
    lang17: 'Upload',
    lang18: 'Click here for guide which bank book front page to upload.',
    lang19: 'Payee General Information',
    lang20: 'Payee’s KTP Number (Bank Account Owner)',
    lang21: 'Please fill in payee’s KTP number',
    lang22: 'Payee’s Phone Number (Bank Account Owner)',
    lang23: 'Fill in payee’s phone number',
    lang24: 'Apply',
    lang25: 'Family Card Information',
    lang26: 'Please upload picture of your family card to prove your relationship.',
    lang27: 'Click here for guide on how to upload your family card photo.',
    
    
    lang28: "Please fill in payee's bank account number",
    lang29: 'Please choose bank name',
    lang30: "Please fill in payee's full name ",
    lang31: 'Please upload bank book photo',
    lang32: "Please fill in payee's KTP number",
    lang33: 'Please fill in the correct phone number',
    lang34: 'Please upload the family card photo',

    lang35: 'Bank book photo uploaded should clearly display these information:',
    lang36: '1. Bank account number',
    lang37: '2. Bank account owner',
    lang38: '3. Bank name',
    lang39: '4. The bank branch which released the bank book',
    
    lang40: 'Family card photo uploaded should clearly display these information:',
    lang41: '1. Family card number',
    lang42: '2. Family members name and NIK ',
    lang43: '3. The relationship information in family card',
    lang44: 'Got it',
    lang45: 'Loading....',
    
    // lang20: 'Next',

    // lang41: 'Got it',


};

export default payeeInfoTranslation;