let referralBonusTranslation = {};

referralBonusTranslation.id = {
    lang1: 'Undang Teman dan Keluargamu',
    lang2: 'Bagaimana Cara Mendapatkan Bonus Referral >>',
    lang3: 'Bonus yang Dihadiahkan',
    lang4: 'Catatan Referral',
    lang5: 'Mengundang',
    lang6: 'Bonus anggota baru yang diundang',
    lang7: 'Bagaimana Cara Mendapatkan Bonus Referral',
    lang8: 'Anggota GTRY harus membagi share (tautan) dari menu GTRY masing-masing.',
    lang9: 'Link share diakses oleh calon anggota kemudian mereka mendaftar di Peduli Sehat dan bergabung ke program GTRY.',
    lang10: 'Anggota GTRY yang mengundang dan anggota baru GTRY yang diundang melalui link share akan mendapatkan masing-masing Rp 5.000 bonus referral pada saldo GTRY mereka.',
    lang11: 'Hanya anggota pertama GTRY yang mengundang, akan mendapatkan bonus referral Rp 5.0000.',
    lang12: 'Apabila ada anggota baru yang diundang melalui link share dan mendaftarkan lebih dari satu orang, bonus referral hanya akan diberikan ke anggota yang pertama saja.',
    lang13: 'Mengerti',
    lang14: 'Bonus Referral',
    lang15: 'Selesai',
    lang54: 'Setiap kali ada tamu yang diundang untuk bergabung ke program GTRY dari share-mu, kami akan memberikan bonus masing-masing Rp 5.000 untukmu dan tamu yang bergabung ke saldo GTRY.'
};
referralBonusTranslation.en = {
    lang1: 'Invite Your Friends and Family',
    lang2: 'How to Get Referral Bonus >>',
    lang3: 'Awarded Bonuses',
    lang4: 'Referral Record',
    lang5: 'Invited',
    lang6: 'New Invitee Bonus',
    lang7: 'How to Get Referral Bonus',
    lang8: 'GTRY members must share share links from their GTRY menu.',
    lang9: 'The link share is accessed by prospective members then they register at Peduli Sehat and join GTRY.',
    lang10: 'GTRY inviter members and new GTRY invitee members who join through the share link will get a Rp 5,000 referral bonus.',
    lang11: 'Only GTRY inviter first members, will get Rp 5.000 referral bonus',
    lang12: 'If a new GTRY invitee who joins through the share link and add more than one person, then the referral bonus will only be given to the first new GTRY member.',
    lang13: 'Got it',
    lang14: 'Referral Bonus',
    lang15: 'Finished',
    lang54: 'Every time a new user is invited to join the GTRY from your share, we will give Rp 5.000 bonus to you and the invitee both in form of GTRY balance.'
};

export default referralBonusTranslation;