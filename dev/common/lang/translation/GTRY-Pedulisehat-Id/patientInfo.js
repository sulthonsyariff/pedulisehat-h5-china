let patientInfoTranslation = {};

patientInfoTranslation.id = {
    lang1: 'Informasi Pasien',
    lang2: 'Informasi Penyakit',
    lang_2: 'Informasi Kecelakaan',
    lang3: 'Informasi Penerima Dana',

    lang4: 'Nama Lengkap Pasien',
    lang5: 'Gotongroyong Facing Critical Diseases Terms and Agreements',
    
    lang6: 'Nomor KTP Pasien',
    lang7: 'Health Requirements',
    
    lang8: 'Status Pasien',
    
    

    lang9: 'Masih Hidup',
    lang10: 'Meninggal',

    lang11: 'Upload',
    lang12: 'Klik disini untuk panduan bagaimana cara mengambil foto KTP',
    lang13: 'Upload Foto KTP',

    lang14: 'Alamat Rumah Pasien',
    lang15: 'Please choose the address',
    lang16: 'Silakan masukan alamat lengkap',
    lang_16: 'Silahkan pilih alamat',

    lang17: 'Nomor Telepon Pasien',
    lang18: 'Silakan masukan nomor telepon pasien',

    
    lang20: 'Lanjut',

    lang21: 'Please upload KTP photo',
    lang22: 'Please input the detail address',
    lang23: 'Please fill in the correct phone number',
    
    lang26: 'Memuat....',

    lang27: 'Contoh foto KTP yang direkomendasi:',
    lang28: 'Foto KTP yang Baik',
    lang29: 'KTP tertutup jari tidak akan diterima',
    lang30: 'KTP buram tidak akan diterima',
    lang31: '1. Pastikan KTP yang Kamu foto tidak ada informasi yang tertutup/terhapus.',
    lang32: '2. Pastikan KTP mu masih berlaku.',
    lang33: '3. Pastikan foto KTP yang Kamu upload benar ada, dan bukan hasil dari scan atau fotokopi.',
 
    lang41: 'Mengerti',

  
};
patientInfoTranslation.en = {
    lang1: 'Patient Information',
    lang2: 'Disease Information',
    lang_2: 'Accident Information',
    lang3: 'Payee Information',

    lang4: 'Patient Full Name',
    lang5: 'Gotongroyong Facing Critical Diseases Terms and Agreements',
    
    lang6: 'Patient KTP Number',
    lang7: 'Health Requirements',
    
    lang8: 'Patient Status',
    
    

    lang9: 'Alive',
    lang10: 'Passed Away',

    lang11: 'Upload',
    lang12: 'Click here for guide on how to take photo of KTP Card',
    lang13: 'Upload KTP Photo',

    lang14: 'Patient’s Home Address',
    lang15: 'Please choose the address',
    lang16: 'Please input the detail address',
    lang_16: 'Please choose the address',

    lang17: 'Patient’s Phone Number',
    lang18: 'Please input patient’s phone number',

    
    lang20: 'Next',

    lang21: 'Please upload KTP photo',
    lang22: 'Please input the detail address',
    lang23: 'Please fill in the correct phone number',
    
    lang26: 'Loading....',

    lang27: 'Example of recommended KTP photo:',
    lang28: 'Acceptable KTP photo',
    lang29: 'KTP blocked by finger won’t be accepted',
    lang30: 'KTP blurred also not accepted',
    lang31: '1. Make sure the ID card that You photograph has no truncated part.',
    lang32: '2. Please make sure Your KTP stil valid.',
    lang33: '3. Please make photo of KTP You upload is real, and the card is not result of scanned or photocopy.',
 
    lang41: 'Got it',

    
};

export default patientInfoTranslation;