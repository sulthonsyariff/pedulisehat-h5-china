let goPayErrorTranslation = {};

goPayErrorTranslation.id = {
    lang1: 'Donasi Gagal',
    lang2: 'Coba Bayar Lagi',
    lang3: 'Tinjau Kembali Kampanye',
};
goPayErrorTranslation.en = {
    lang1: 'Donation failed！',
    lang2: 'Pay again',
    lang3: 'Review the Campaign',
};

export default goPayErrorTranslation;