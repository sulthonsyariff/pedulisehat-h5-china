let kindnessReportTranslation = {};

kindnessReportTranslation.id = {
    lang1: 'Peduli Sehat',
    lang2: 'Halo #insanpeduli',
    lang3: 'Kamu bergabung ke Peduli Sehat sejak',
    lang4: 'Sudah',
    lang5: 'hari sejak Kamu memulai',
    lang6: 'perjalanan kepedulian bersama kami',

    lang7: 'Pertama kali',
    lang8: 'Kamu berdonasi pada tanggal',
    lang9: 'Donasi pertamamu ditujukan untuk:',
    lang10: '',
    lang10_1: 'Kamu masih belum berdonasi, tapi kami sangat menghargai niatmu untuk peduli.',
    lang10_2: 'Kamu dapat mulai untuk menolong dirimu dan orang-orang lain mulai sekarang. Cek halaman berikutnya untuk info lebih lanjut.',

    lang11: 'Di tahun 2019',
    lang12: 'Kamu telah menunjukan',
    lang_13: 'kepedulianmu',
    lang13: 'kali',
    lang14: 'Kamu telah menolong',
    lang15: 'keluarga',
    lang16: 'Dengan total donasi sebesar',
    lang17: 'Rp',
    // lang17: 'Loading...',
    lang18: 'Kami sangat terkesan dengan niat Kamu untuk peduli',
    lang19: 'Karena kepedulian mu, Kami ingin menghadiahkanmu program Gotongroyong',
    lang20: 'Klik tombol dibawah untuk mulai melindungi Kamu dan keluargamu!',
    lang21: 'Bagikan',
    lang22: 'Lindungi Keluargaku',
    
    // lang23: 'No WhatsApp？Send SMS code',
    // lang24: 'SMS code has been sent',

};
kindnessReportTranslation.en = {
    lang1: 'Peduli Sehat',
    lang2: 'Hello #insanpeduli',
    lang3: 'You joined Peduli Sehat since',
    lang4: 'It has been',
    lang5: 'days',
    lang6: ' You started your kindness journey with us.',

    lang7: 'The first time',
    lang8: 'you donated Your kindness was',
    lang9: 'Your first donation',
    lang10: 'assistance provided to:',
    lang10_1: 'You have not donated yet , but we really appreciate your intention to do kindness.',
    lang10_2: 'You can start to help Yourself and other people from now on. Check out next page for more info. ',

    lang11: 'In 2019',
    lang12: 'You have showed your kindness',
    lang_13: '',
    lang13: 'times',
    lang14: 'You have saved',
    lang15: 'families',
    lang16: 'with the total donation amount',
    lang17: 'of Rp',
    // lang17: 'Loading...',
    lang18: 'We are really impressed with Your act of care',
    lang19: 'To award Your kindness act We would like to gift you the Gotongroyong program',
    lang20: 'Click the button to start to protect You and Your family!',
    lang21: 'Share',
    lang22: 'Protect My Family',

    // lang23: 'No WhatsApp？Send SMS code',
    // lang24: 'SMS code has been sent',

};

export default kindnessReportTranslation;