let modifyNameTranslation = {};

modifyNameTranslation.id = {
    lang1: 'Simpan',
    lang2: 'Nama anda',
    lang3: 'Silahkan masukan nama anda',
    lang4: 'Memuat...',
    lang5: 'Modifikasi sukses',

    // bing email
    lang6: 'Silakan isi alamat E-mail kamu',
    lang_submit: 'Kirim',
    lang7: 'Silakan isi dengan Alamat E-mail yang Benar',
    lang8: 'Pilih suffix E-mail',
    lang9: 'Isi suffix E-mail',
    lang10: 'Isi prefix E-mail Anda dan pilih suffix E-mail berdasarkan perusahaan tempat Anda bekerja',

};
modifyNameTranslation.en = {
    lang1: 'Save',
    lang2: 'Your name',
    lang3: 'Please enter your name',
    lang4: 'Loading...',
    lang5: 'Successful modify',

    // bing email
    lang6: 'Please fill in your E-mail address',
    lang_submit: 'Submit',
    lang7: 'Please fill in correct E-mail Address',
    lang8: 'Choose E-mail suffix',
    lang9: 'Fill in E-mail prefix',
    lang10: 'Fill in your E-mail prefix and choose your E-mail suffix based on the company You worked for',

};

export default modifyNameTranslation;