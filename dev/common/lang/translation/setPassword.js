let setPassword = {};

setPassword.id = {
    lang1: 'Tentukan password Kamu',
    lang2: 'Konfirmasi lagi password baru Kamu',
    lang3: 'Password hanya terdiri dari 6-16 digit, huruf atau simbol.',
    lang4: 'OK',
    lang5: 'Masukan password lama Kamu',
    lang6: 'Password Baru',
    lang7: 'Password lama',
    lang8: 'Syarat & Ketentuan Penggunaan',
    lang9: 'Memuat....',
    lang10: 'Silahkan menyetujui semua kebijakan',
    lang11: 'dan',
    lang12: 'Kebijakan Privasi',
    lang13: 'Masukan nomor telepon anda',
    lang14: 'Password baru dan Konfirmasi Password tidak cocok',
    lang15: 'Konfirmasi lagi password Kamu',
    lang16: 'Password yang ditentukan dan Konfirmasi Password tidak cocok',

};
setPassword.en = {
    lang1: 'Set Your new password',
    lang2: 'Confirm again Your new password',
    lang3: 'A password consists of 6-16 digits, letters, or symbols.',
    lang4: 'OK',
    lang5: 'Input Your old password',
    lang6: 'New Password',
    lang7: 'Old Password',
    lang8: 'Terms & Conditions of Use',
    lang9: 'Loading....',
    lang10: 'Please agree with all policy',
    lang11: 'and',
    lang12: 'Privacy Policy',
    lang13: 'Please enter your phone number',
    lang14: 'New Password and Confirm Password not matched',
    lang15: 'Confirm again Your password',
    lang16: 'Password set and Confirm Password not matched',
};

export default setPassword;