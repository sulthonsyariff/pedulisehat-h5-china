let changeLanguageTranslation = {};

changeLanguageTranslation.id = {
    lang1: 'Bahasa Indonesia (ID)',
    lang2: 'English (EN)',
};
changeLanguageTranslation.en = {
    lang1: 'Bahasa Indonesia (ID)',
    lang2: 'English (EN)',
};

export default changeLanguageTranslation;