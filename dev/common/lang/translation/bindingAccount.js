let bindingAccountTranslation = {};

bindingAccountTranslation.id = {
    lang1: 'Facebook',
    lang2: 'Google',
};
bindingAccountTranslation.en = {
    lang1: 'Facebook',
    lang2: 'Google',
};

export default bindingAccountTranslation;
