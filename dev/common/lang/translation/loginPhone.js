let loginPhoneTranslation = {};

loginPhoneTranslation.id = {
    lang1: 'Silahkan masukan no telepon yang benar',
    lang2: 'Silahkan masukan kode yang benar',
    lang3: 'Nomor telepon',
    lang4: 'Kode Verifikasi',
    lang5: 'Kirim kode',
    lang6: 'Masuk',
    lang7: 'Anda menyetujui dengan',
    lang8: 'Syarat & Ketentuan Penggunaan',
    lang9: 'Memuat....',
    lang10: 'Silahkan menyetujui semua kebijakan',
    lang11: 'dan',
    lang12: 'Kebijakan Privasi',
    lang13: 'Masukan nomor telepon anda',
    lang19: 'Masuk dengan Password',
    lang20: 'Masuk dengan kode verifikasi SMS',
    lang21: 'Lupa atau tidak ada password?',
    lang22: 'password',
    lang23: 'Nomor telepon',

};
loginPhoneTranslation.en = {
    lang1: 'Please enter valid phone number',
    lang2: 'Please enter the correct  code ',
    lang3: 'Phone number',
    lang4: 'Verification Code',
    lang5: 'Send code',
    lang6: 'Sign in',
    lang7: 'You are agreeing with',
    lang8: 'Terms & Conditions of Use',
    lang9: 'Loading....',
    lang10: 'Please agree with all policy',
    lang11: 'and',
    lang12: 'Privacy Policy',
    lang13: 'Please enter your phone number',
    lang19: 'Sign in with Password',
    lang20: 'Sign in with SMS Verification Code',
    lang21: 'No password or forgot password?',
    lang22: 'password',
    lang23: 'Phone number',

};

export default loginPhoneTranslation;