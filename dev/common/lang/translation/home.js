let homeTranslation = {};

homeTranslation.id = {
    lang1: 'Kampanye Terkini',
    lang2: 'Memuat',
    lang3: 'Waktu Berlalu',
    lang4: 'Hari',
    lang4_1: 'Beberapa Jam',
    lang4_2: 'Selesai',
    lang5: 'Hari',
    lang6: 'Terkumpul',
    lang7: 'Tidak ada data',
    lang8: 'Blog',
    lang9: 'Target',
    lang10: 'Tentang Kami',
    lang11: 'pedulisehat.id adalah platform donasi kesehatan online terkini di Indonesia. Sekarang kita bisa melakukan donasi kesehatan kapanpun, dimanapun dengan cepat dan transparan membantu pasien yang tepat.',
    lang12: 'Selengkapnya',
    lang13: 'Mulai Bantu Mereka Sekarang',
    lang14: 'Lihat Semua Kampanye',
    lang15: 'Galang Dana',
    lang16: '#PeduliRameRame untuk Indonesia',
    lang16_1: 'Kamu bisa tag, follow ataupun berkomunikasi langsung melalui social media Kami.',
    lang17: 'Kampanye yang Mendesak',
    lang18: 'Copyright © 2020 Peduli Sehat Gotongroyong',
    lang19: 'Cara Menggalang Dana di pedulisehat.id',
    lang20: 'Klik Galang Dana, ikuti instruksinya dan isi form dengan lengkap.',
    lang21: 'Bagikan halaman galang dana kamu melalui Facebook, WhatsApp maupun Twitter',
    lang22: 'Kamu memerlukan verifikasi jika ingin menarik dana yang telah terkumpul.',
    lang23: 'Galang Dana',
    lang24: 'Kontak Whatsapp',
    lang25: 'Dimonitor oleh',
    lang26: 'Donasi ke Semua',
    lang27: 'Bagikan',
    lang28: 'Sudah tahu belum kalau gotongroyong bisa dilakukan secara digital? ',
    lang29: 'Pelajari Lebih Lanjut disini >>',
    lang30: 'kampanye',
    lang31: 'dana terkumpul',
    lang32: "Selesai",
    lang33: "Sisa Waktu",
    lang34: "Donasi",
    lang35: "Zakat",
    // lang35: "Zakat & Kurban",
    lang36: "Gotongroyong",
    lang37: "Asuransi",
    lang37_1: "Data Covid-19",
    lang38: "Piala",
};
homeTranslation.en = {
    lang1: 'Popular Campaigns',
    lang2: 'Loading',
    lang3: 'Time Past',
    lang4: 'Day',
    lang4_1: 'Few Hours',
    lang4_2: 'Finish',
    lang5: 'Days',
    lang6: 'Funded',
    lang7: 'no data',
    lang8: 'Blog',
    lang9: 'Goal',
    lang10: 'About Us',
    lang11: 'pedulisehat.id is an online health donation platform in Indonesia. Now we are able to do health donation anytime, anywhere quickly and transparently to help the right patients.',
    lang12: 'Details',
    lang13: 'Start to Help Them Now',
    lang14: 'View All Campaigns',
    lang15: 'Start Campaign',
    lang16: '#PeduliRameRame for Indonesia',
    lang16_1: 'You can tag, follow direct communication through our social media.',
    lang17: 'Urgent Campaigns',
    lang18: 'Copyright © 2020 Peduli Sehat Gotongroyong',
    lang19: 'How to Start Campaign at pedulisehat.id',
    lang20: 'Click Start Campaign, follow the instructions and fill in the forms in detail.',
    lang21: 'Share your campaign page via Facebook, WhatsApp or Twitter',
    lang22: 'You need verification if you want to withdraw the fund which already raised.',
    lang23: 'Start Campaign',
    lang24: 'WhatsApp Contact',
    lang25: 'Monitored by',
    lang26: 'Donate to All',
    lang27: 'Share',
    lang28: 'Do you know yet that mutual help can be done digitally? ',
    lang29: 'Learn more here >>',
    lang30: 'campaigns',
    lang31: 'funded',
    lang32: "Finished",
    lang33: "Time Left", // 有其他列表页也有用到，
    lang34: "Donate",
    lang35: "Zakat",
    // lang35: "Zakat & Qurban",
    lang36: "Gotongroyong",
    lang37: "Insurance",
    lang37_1: "Covid-19 Data",
    lang38: "Trophies",
};

export default homeTranslation;