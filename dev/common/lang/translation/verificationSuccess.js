let verificationSuccessTranslation = {};

verificationSuccessTranslation.id = {
    lang1: 'Berhasil dikirim',
    lang2: 'Segera bagikan dengan kerabat dan teman anda, sehingga lebih banyak orang dapat melihat dan mendukung anda',
    lang3: 'Mengerti',
};
verificationSuccessTranslation.en = {
    lang1: 'Submitted successfully!',
    lang2: 'Share it to your friends quickly, so that more people can see and support you!',
    lang3: 'Got it',
};

export default verificationSuccessTranslation;