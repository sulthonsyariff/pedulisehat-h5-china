let withdrawTranslation = {};

withdrawTranslation.en = {
    lang1: 'Withdraw Amount',
    lang2: 'Withdraw All',
    lang3: 'Bank Account',
    lang4: 'Withdraw Purpose',
    lang5: 'Please fill in the purpose of the withdrawal, it will be showed in the campaign updates.',
    lang6: 'Submit',

    lang7: 'Minimum Rp 500.000',
    lang8: 'Maximum Rp',

    lang9: '1.Campaign verified.',
    lang10: '2. Donation will be received in 2 - 7 working days. ',
    lang11: '*If Your campaign still active',
    lang12: '3.	 You must wait 7 days for next withdrawal attempt. ',
    lang13: '4.	 Your campaign net balance ≥ Rp 500.000',
    lang14: '5. You can withdraw flexible amount that ≥ Rp 500.000 but no more than your net balance.',
    lang15: '*If Your campaign already finished',
    lang16: '3.	 You must wait 24 hours after your campaign finished.',
    lang17: '4. Your campaign net balance ≥ Rp 20.000',
    lang18: '5.	 Withdrawal amount = total amount of net balance.',
    lang19: 'Please select the bank account',
    lang23: 'Please fill in the purpose of the withdrawal',
    lang24: 'Please input the correct withdraw amount',

};
withdrawTranslation.id = {
    lang1: 'Jumlah Penarikan',
    lang2: 'Tarik Semua',
    lang3: 'Akun Bank',
    lang4: 'Alasan Penarikan',
    lang5: 'Silakan isi alasan penarikan, info ini akan muncul di pembaruan kampanye.',
    lang6: 'Ajukan',

    lang7: 'Minimum Rp 500.000',
    lang8: 'Maximum Rp',


    lang9: '1.Kampanye terverifikasi.',
    lang10: '2.Donasi akan diterima dalam waktu 2 - 7 hari kerja.',
    lang11: '*Jika kampanyemu masih aktif',
    lang12: '3. Kamu harus menunggu 7 hari untuk melakukan penarikan lagi.',
    lang13: '4. Saldo bersih kampanyemu ≥ Rp 500.000',
    lang14: '5. Kamu dapat menarik jumlah fleksibel yaitu ≥ Rp 500.000 tapi tidak lebih dari saldo bersihmu.',
    lang15: '*Jika kampanyemu telah selesai',
    lang16: '3.	 Kamu harus menunggu 24 jam setelah kampanyemu selesai/berakhir.',
    lang17: '4. Saldo bersih kampanyemu ≥ Rp 20.000',
    lang18: '5. Jumlah penarikan = jumlah total dari saldo bersih.',
    lang19: 'Silakan pilih akun bank',

    lang23: 'Silahkan mengisi tujuan penarikan',
    lang24: 'Silakan masukan jumlah penarikan yang benar',


};

export default withdrawTranslation;