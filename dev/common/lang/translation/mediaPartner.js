let demoTranslation = {};

demoTranslation.id = {
    lang1: 'kampanye',
    lang2: 'dana terkumpul',
    lang3: 'Kampanye Kami',
    lang4: '',
    lang5: '',
    lang6: '',
    lang7: '',
    lang8: '',
    lang9: '',
    lang10: '',
};
demoTranslation.en = {
    lang1: 'campaigns',
    lang2: 'funded',
    lang3: 'Our Campaigns',
    lang4: '',
    lang5: '',
    lang6: '',
    lang7: '',
    lang8: '',
    lang9: '',
    lang10: '',
};

export default demoTranslation;