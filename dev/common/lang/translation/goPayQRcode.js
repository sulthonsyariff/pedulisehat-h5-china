let followedCampaignsTranslation = {};

followedCampaignsTranslation.id = {
    lang1: 'Buka aplikasi GO-JEK pada ponsel anda dan scan kode QR dibawah ini',
    lang1_shopeePay: 'Buka aplikasi Shopee pada ponsel anda dan scan kode QR dibawah ini dengan ShopeePay',

    lang2: 'Silahkan selesaikan pembayaran anda secepatnya',
    lang2_shopeePay: 'Silakan selesaikan pembayaran anda secepatnya.',

    lang3: 'Bagaimana Cara Membayar?',
    lang4: 'Silahkan selesaikan pembayaran GO-PAY anda via aplikasi GO-JEK.',
    lang5: '1. Buka aplikasi GO-JEK pada ponsel anda',
    lang6: '2. Klik "Bayar"',
    lang7: '3. Arahkan kamera ponsel anda ke arah kode QR',
    lang8: '4. Cek detil pembayaran di aplikasi GO-JEK kemudian tekan Bayar',
    lang9: '5. Transaksi anda sudah selesai',
    lang10: 'Kembali ke Halaman Kampanye',
    lang11: 'Aplikasi belum diinstal',
    lang12_1: 'Kode QR',
    lang12_2: 'kedaluwarsa'
};
followedCampaignsTranslation.en = {
    lang1: 'Open GO-JEK app on your phone and scan QR code below',
    lang1_shopeePay: 'Open Shopee app on your phone annd scan QR code below',

    lang2: 'Please complete your payment as soon as possible',
    lang2_shopeePay: 'Please complete your payment as soon as possible.',

    lang3: 'How to Pay?',
    lang4: 'Please complete your GO-PAY payment via GO-JEK app.',
    lang5: '1. Open GO-JEK app on your phone.',
    lang6: '2. Click Pay.',
    lang7: '3. Point your camera to the QR Code.',
    lang8: '4. Check your payment details in the GO-JEK app and then tap Pay.',
    lang9: '5. Your transaction is done.',
    lang10: 'Back to Campaign Page',
    lang11: 'The app is not installed',
    lang12_1: 'The QR code',
    lang12_2: 'is expired'
};

export default followedCampaignsTranslation;