let userLevel = {};

userLevel.id = {
    lang1: 'Levelmu',
    lang2: 'Levelmu',
    lang3: 'Poin/Target',
    lang4: 'Bagaimana cara mendapatkan poin reward?',
    // lang5: 'Simpan',
    // lang6: 'Memuat',
    // lang7: 'Silahkan masukan cerita anda',
    // lang8: 'Sampul',
    // lang9: 'Oops, kami masih mengunggah arsip anda',
};
userLevel.en = {
    lang1: 'User Level',
    lang2: 'Current Level',
    lang3: 'Points/Target',
    lang4: 'How to get reward points?',
    // lang5: 'Save',
    // lang6: 'Loading',
    // lang7: 'Please enter your story',
    // lang8: 'Cover',
    // lang9: 'Oops, we are still uploading your files',
};

export default userLevel;