let sendCodeTranslation = {};

sendCodeTranslation.id = {
    lang1: 'Mohon masukan nomor telepon ada',
    lang2: 'Mohon masukan kode',
    lang3: 'Mohon masukan nomor telepon anda',
    lang4: 'Mohon masukan nomor telepon baru anda',
    lang5: 'Kirim kode',
    lang6: 'Menjilid',
};
sendCodeTranslation.en = {
    lang1: 'Please enter your phone number',
    lang2: 'Please enter the code',
    lang3: 'Please enter your phone number',
    lang4: 'Please enter your new phone number',
    lang5: 'Send code',
    lang6: 'Bind',
};

export default sendCodeTranslation;
