let updateTranslation = {};

updateTranslation.id = {
    lang1: 'Pembaruan',
    lang2: 'Tak kenal maka tak sayang. Kabar terbaru dari pasien akan membawa lebih banyak doa dan donasi.',
    lang3: 'Upload',
    lang4: 'Max. 8 foto',
    lang5: 'Simpan',
    lang6: 'Memuat',
    lang7: 'Silahkan masukan cerita anda',
    lang8: 'Sampul',
    lang9: 'Oops, kami masih mengunggah arsip anda',
};
updateTranslation.en = {
    lang1: 'Update',
    lang2: 'If you don’t know the person, you won’t love that person, The newest info about the patient will bring more prayer and donation.',
    lang3: 'Upload',
    lang4: 'Max. 8 photos',
    lang5: 'Save',
    lang6: 'Loading',
    lang7: 'Please enter your story',
    lang8: 'Cover',
    lang9: 'Oops, we are still uploading your files',
};

export default updateTranslation;