let searchResults = {};

searchResults.id = {
    lang1: 'Batal',
    lang2: 'Histori Pencarian',
    lang3: 'Populer',
    lang4: 'Cari apa yang ingin kamu bantu',
    // lang5: 'Kirim kode',
    // lang6: 'Menjilid',
};
searchResults.en = {
    lang1: 'Cancel',
    lang2: 'Search History',
    lang3: 'Popular',
    lang4: 'Find what do you want to help',
    // lang5: 'Send code',
    // lang6: 'Bind',
};

export default searchResults;
