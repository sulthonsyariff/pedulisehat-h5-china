let campaignListTranslation = {};

campaignListTranslation.id = {
    lang1: 'Laporan Real-time Covid-19 di Indonesia',
    lang2: '#SalingPeduli',
    lang3: 'Lawan Covid-19 (Corona)',
    lang4: 'Baru ',
    lang5: 'Terkonfirmasi',
    lang6: 'Dalam Perawatan',
    lang7: 'Sembuh',
    lang8: 'Meninggal',

    lang9: 'Data per Provinsi',
    lang10: 'Baru',
    lang11: 'Provinsi',
    lang12: 'Lihat lebih lanjut',
    lang13: 'Tutup',


    lang14: 'Bantu cegah penularan Covid-19, donasi disini!',
    lang15: 'Program Gotongroyong akan membantumu dari Covid-19. Gabung sekarang!',
    lang16: "Bagikan",
    lang17: "*Data Terkini diterima dari Laporan Kementerian Kesehatan Indonesia.",

    lang18: 'Buat yang WFH, saatnya berbagi berkah buat para ojol.',
    lang19: '',
    
    lang93: "Sisa Waktu"

};
campaignListTranslation.en = {
    lang1: 'Real-time Report of Covid-19 in Indonesia',
    lang2: '#SalingPeduli',
    lang3: 'Fight Covid-19 (Corona)',

    lang4: 'New ',
    lang5: 'Confirmed',
    lang6: 'In Treatment',
    lang7: 'Recovered',
    lang8: 'Deaths',

    lang9: 'Data per Province',
    lang10: 'New',
    lang11: 'Province',
    lang12: 'View More',
    lang13: 'Close',

    lang14: 'Assist prevention of Covid-19 spread, donate here!',
    lang15: 'Gotongroyong program will assist you from Covid-19. Join now!',
    lang16: 'Share',

    lang17: '*Real-time Data retrieved from Indonesian Ministry of Health Report.',
    lang18: 'For people doing WFH, now is the right time to share help to the online drivers.',
    lang19: '',
    lang28: "Finished",

    lang93: "Time Left"

};

export default campaignListTranslation;