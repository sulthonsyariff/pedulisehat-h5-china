let demoTranslation = {};

demoTranslation.id = {
  lang1: "Perpanjang Waktu Galang Dana",
  lang2: "Ubah",
  lang3: "Hari",
  lang4: "Kamu hanya dapat memperpanjang waktu penggalangan dana sekali",
  lang5:'Simpan',
  lang6: "Silakan pilih",
  lang7: 'Memuat',

};
demoTranslation.en = {
  lang1: "Extend Fundraising Time",
  lang2: "Change",
  lang3: "Days",
  lang4: "You can extend your campaign only once.",
  lang5: "Save",
  lang6:"Please Choose",
  lang7: 'Loading',

};

export default demoTranslation;
