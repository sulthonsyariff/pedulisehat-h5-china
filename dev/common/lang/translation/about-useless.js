﻿let aboutTranslation = {};

aboutTranslation.id = {
    lang1: 'Tentang Kami',
    lang2: 'Indonesia merupakan negara dengan populasi terbesar ke-4 dunia, dengan total penduduk mencapai dua ratus enam puluh lima juta jiwa.Dibandingkan dengan kondisi di negara maju, di Indonesia, 80 persen masyarakatnya jauh dari harapan hidup sehat. Banyak masyarakat kita akhirnya menderita penyakit kritis karena tak kunjung berobat, akibat kekurangan uang. Sementara itu, 20 persen masyarakat menengah ke atas uangnya habis untuk mengobati penyakit berat. World Health Organization (WHO) dan World Bank memperkirakan, 12 juta penduduk Indonesia didiagnosa menderita penyakit kritis tahun lalu. Sementara itu, tahun 2008, ada 36,1 juta orang meninggal dunia akibat penyakit kritis.',
    lang3: 'Peduli Sehat hadir sebagai wadah yang menawarkan layanan penggalangan dana kesehatan melalui media sosial. Berfokus pada pasien yang menderita penyakit kronis yang tidak memiliki cukup dana untuk perawatan, berobat juga bertahan hidup.Dikarenakan masih terdapat biaya lainnya (biaya obat, biaya susu khusus, alat medis penunjang kesehatan) yang tidak dapat dibiayai melalui program kartu BPJS (Badan Penyelenggara jaminan Sosial)/JKN (Jaminan Kesehatan Nasional). ',
    lang4: 'Visi',
    lang5: 'Memperlengkapi setiap keluarga dengan keberanian dan kekuatan dalam menghadapi berbagai penyakit.',
    lang6: 'Misi',
    lang7: 'Menjadi perusahaan berbasis teknologi dengan kesadaran tertinggi perihal tanggung jawab sosial.',
    lang8: 'Peduli Sehat tercatat di Kemenkumham,Kominfo dengan No 01204/DJAI.PSE/10/2018.'
};
aboutTranslation.en = {
    lang1: 'About Us',
    lang2: 'Indonesia currently has the fourth biggest population on the globe. Reaching the number two hundred sixty five million people, most of the population still suffering critical diseases. Inadequate number of medical practitioners and medical cost constraints, resulting in a swollen number of patients in every hospital in Indonesia.    ',
    lang3: 'Peduli Sehat is a platform that offers fund raising service and operates through the vast digital universe of social media. Focusing on those who suffers chronic diseases that are unable to have treatments due to the shortage amount of fund.',
    lang4: 'Vision',
    lang5: 'Provide every families with courage and strength in facing multiple diseases.',
    lang6: 'Mission',
    lang7: 'To become a company based on technology with the highest awareness in social responsibilities.',
    lang8: 'Peduli Sehat as a multifunction application provide chance to donate for our good brothers and sisters whose in need of treatment through our dedicated platform. This application also provide fund raising service to finance the treatment of our loved ones.'
};

export default aboutTranslation;
