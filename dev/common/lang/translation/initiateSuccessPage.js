let initiateSuccessPageTranslation = {};

initiateSuccessPageTranslation.id = {
    lang1: 'Sukses dipublikasikan',
    lang2: 'Lihat penggalang dana saya',
};
initiateSuccessPageTranslation.en = {
    lang1: 'Published successfully',
    lang2: 'View my campaign',
};

export default initiateSuccessPageTranslation;
