let modifyPhoneTranslation = {};

modifyPhoneTranslation.id = {
    lang1: 'Nomor telepon anda sekarang adalah',
    lang2: 'Ganti',
};
modifyPhoneTranslation.en = {
    lang1: 'Your current phone number is',
    lang2: 'Change',
};

export default modifyPhoneTranslation;
