let modifyStoryTranslation = {};

modifyStoryTranslation.id = {
    lang1: 'Apa cerita kamu',
    lang2: 'Ceritakan pada kami siapa anda, hal apa yang ingin anda lakukan, dan hal lain yang ingin anda sampaikan! ',
    lang3: 'Ubah Foto Kampanye',
    lang4: 'Upload',
    lang5: 'Max. 8 foto',
    lang6: 'Peringatan',
    lang7: ' Pengungkapan informasi termasuk namun tidak terbatas pada rekening bank, nomor telepon, kontak dan informasi pribadi lainnya dari tingkat yang sama sangat dilarang. Kampanye akan ditutup dan dana yang dikumpulkan akan dibekukan saat identifikasi ',
    lang8: 'Simpan',
    lang9: 'Perubahan diterima, silakan menunggu verifikasi',
    lang9_1: 'Kampanye diperbarui',
    lang10: 'Mohon masukan cerita anda',
    lang11: 'Mohon masukan maksimal 5000 karakter',
    lang12: 'Sampel',
    lang13: 'Cerita',
    lang14: 'Oops, kami masih mengunggah arsip anda',
    lang15: 'Silakan unggah minimal satu foto untuk konten galang danamu',
    lang16: 'Ubah Foto Utama',
    lang17: 'Silakan unggah foto utama yang jelas secara horizontal',
    lang18: 'Silakan unggah satu foto sampul galang dana'

};
modifyStoryTranslation.en = {
    lang1: 'What is Your Story',
    lang2: 'Tell us who you are, what goods you would like to do, and anything else that you want to say!',
    lang3: 'Edit Campaign Images',
    lang4: 'Upload',
    lang5: 'Max. 8 photos',
    lang6: 'Warning:',
    lang7: 'The revealing of information including but not limited to bank accounts, phone numbers, contacts and other personal information of the same level is strictly prohibited.The campaign will be closed and the funds raised will be frozen upon identification.',
    lang8: 'Save',
    lang9: 'Changes received, please wait for verification',
    lang9_1: 'Campaign updated',
    lang10: 'Please enter your story',
    lang11: 'Please input at most 5000 charaters',
    lang12: 'Cover',
    lang13: 'Story',
    lang14: 'Oops, we are still uploading your files',
    lang15: 'Please upload at least one photo for your campaign content',
    lang16: 'Edit Cover Image',
    lang17: 'Please upload a horizontal and clear cover image',
    lang18: 'Please upload one campaign cover photo'
};

export default modifyStoryTranslation;