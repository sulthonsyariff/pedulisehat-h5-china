let OVOTransactionTranslation = {};

OVOTransactionTranslation.id = {
    lang1: 'Tagihan sudah dikirim ke',
    lang2: 'Silahkan lakukan pembayaran anda dalam',
    lang3: 'detik',
    lang4: 'Bagaimana cara Membayar?',
    lang5: 'Silahkan selesaikan pembayaran OVO anda melalui',
    lang6: 'aplikasi OVO',
    lang7: 'Buka aplikasi OVO.',
    lang8: 'Cek notifikasi atau cari transaksi dari icon notifikasi pada OVO.',
    lang9: 'Pilih metode pembayaran (OVO cash, OVO points, atau Split) lalu klik "Bayar".',
    lang10: 'Selamat! Pembayaranmu sudah selesai.',
    lang11: 'Pastikan aplikasi OVO anda sudah siap/sudah berjalan dengan koneksi Internet yang baik',
};
OVOTransactionTranslation.en = {
    lang1: 'The bill already sent to',
    lang2: 'Please do your payment in',
    lang3: 'seconds',
    lang4: 'How to Pay?',
    lang5: 'Please complete your OVO payment via',
    lang6: 'OVO app',
    lang7: 'Please open OVO application.',
    lang8: 'Check the notification or find the order from the notification icon inside OVO.',
    lang9: 'Choose your payment method (OVO cash, OVO points, or Split) and click Pay.',
    lang10: 'Congratulations your payment is done.',
    lang11: 'Please make sure your OVO application ready/already opened with good Internet connection',
};

export default OVOTransactionTranslation;