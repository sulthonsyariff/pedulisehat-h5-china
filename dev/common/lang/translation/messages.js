let modifyGoalTranslation = {};

modifyGoalTranslation.id = {
    lang1: 'Notifikasi Donasi',
    lang2: 'Notifikasi Verifikasi',
    lang3: 'Kampanye yang Diikuti',
    lang4: 'Kampanye yang Didonasi',
    lang5: 'Notifikasi',
    lang6: 'Pembaruan',
};
modifyGoalTranslation.en = {
    lang1: 'Donation Notifications',
    lang2: 'Verification Notifications',
    lang3: 'Followed campaigns',
    lang4: 'Donated campaigns',
    lang5: 'Notifications',
    lang6: 'Updates',
};

export default modifyGoalTranslation;
