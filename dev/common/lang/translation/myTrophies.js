let campaignListTranslation = {};

campaignListTranslation.id = {
    lang1: 'Piala ku',
    lang2: 'Memuat',
    lang3: 'Raih Piala Ini',
    
};
campaignListTranslation.en = {
    lang1: 'My Trophies',
    lang2: 'Loading',
    lang3: 'Get This Trophy',

};

export default campaignListTranslation;