let demoTranslation = {};

demoTranslation.id = {
    lang1: 'Kampanye apa yang ingin Kamu mulai?',
    lang2: 'Zakat',
    lang3: 'Kampanye Biasa',
    lang4: '',
    lang5: '',
    lang6: '',
    lang7: '',
    lang8: '',
    lang9: '',
    lang10: '',
};
demoTranslation.en = {
    lang1: 'What campaign do you want to start?',
    lang2: 'Zakat Campaign',
    lang3: 'Regular Campaign',
    lang4: '',
    lang5: '',
    lang6: '',
    lang7: '',
    lang8: '',
    lang9: '',
    lang10: '',
};

export default demoTranslation;