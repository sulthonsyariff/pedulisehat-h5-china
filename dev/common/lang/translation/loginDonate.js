let loginDonateTranslation = {};

loginDonateTranslation.id = {
    lang1: 'Silahkan masukan no telepon yang benar',
    lang2: 'Silahkan masukan nama anda',
    lang3: 'Donasi sebagai Tamu',
    lang4: 'Nama',
    lang5: 'Silahkan isi nama lengkap anda',
    lang6: 'No telepon',
    lang7: 'Lanjut',
    lang8: 'Atau Masuk kemudian Donasi',
    lang9: 'Anda telah menyetujui dengan',
    lang10: 'Syarat & Ketentuan Penggunaan',
    lang11: 'dan',
    lang12: 'Kebijakan Privasi ',
    lang13: 'Silahkan menyetujui semua kebijakan',
    lang14: 'Memuat...',

};
loginDonateTranslation.en = {
    lang1: 'Please enter valid phone number',
    lang2: 'Please enter your name',
    lang3: 'Donate as Guest',
    lang4: 'Name',
    lang5: 'Please fill in your name',
    lang6: 'Phone Number',
    lang7: 'Next',
    lang8: 'Or Log in and Donate',
    lang9: 'You are agreeing with',
    lang10: 'Terms & Conditions of Use',
    lang11: 'and',
    lang12: 'Privacy Policy ',
    lang13: 'Please agree with all policy',
    lang14: 'Loading...',

};

export default loginDonateTranslation;