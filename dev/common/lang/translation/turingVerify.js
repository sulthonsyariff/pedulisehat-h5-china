let turingVerifyTranslation = {};

turingVerifyTranslation.en = {
    lang1: 'Please slide to verify',

    lang2: 'Slide button to the right',
    lang3: 'Validation Success',
    lang4: 'Validation Failed',
    lang8: 'Loading...',

    lang10: 'WhatsApp Code',
    lang24: 'SMS code has been sent',
    

};
turingVerifyTranslation.id = {
    lang1: 'Geser untuk verifikasi',
    lang2: 'Geser tombol ke kanan',
    lang3: 'Validasi Sukses',
    lang4: 'Validasi Gagal',
    lang8: 'Memuat...',

    lang10: 'Kode WhatsApp',
    lang24: 'Kode SMS telah dikirim',

   

};

export default turingVerifyTranslation;