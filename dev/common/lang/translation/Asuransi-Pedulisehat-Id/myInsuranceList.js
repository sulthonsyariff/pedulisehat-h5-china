let translation = {};

translation.id = {
    lang_wsapp: 'Kontak WhatsApp',
    lang_title: 'Asuransi ku',
    loading: 'Memuat',

    lang1: 'Daftar Asuransi ku',
    lang2: 'Polis',

    lang4: 'Belum ada konten',

    unpaid: 'Belum Bayar',
    paid: 'Paid',
    pay: 'Bayar',

    incomData: 'Data Belum Lengkap',
    inUndwr: 'Dalam Underwriting',
    undwFaid: 'Underwriting Gagal',
    undwSucs: 'Underwriting Sukses',
    effct: 'Effective',

    lang_r_1: 'Saya Sendiri',
    lang_r_2: 'Suami',
    lang_r_3: 'Istri',
    lang_r_4: 'Ayah',
    lang_r_5: 'Ibu',
    lang_r_6: 'Anak',
    lang_r_7: 'Adik / Kakak Kandung',

    lang_r_t_1: 'Tertanggung',
    lang_r_t_2: 'Pasangan',
    lang_r_t_3: 'Anak', 

    lang_r_b_1: 'Istri',
    lang_r_b_2: 'Suami',
    lang_r_b_3: 'Anak', 
    lang_r_b_4: 'Ayah', 
    lang_r_b_5: 'Ibu', 
    lang_r_b_6: 'Kakak', 
    lang_r_b_7: 'Adik', 

    lang9: 'Lengkapi Data',
    lang10: 'Cek',

    lang11: 'Tertanggung',
    lang12: 'Periode Perlindungan',

    lang13: 'Cetak Hardcopy',



    lang14: 'Belum dilengkapi',
    lang15: '',
    lang16: '',
    lang17: '',
    lang18: '',
    lang19: '',
    lang20: '',
    lang21: '',

};
translation.en = {
    lang_wsapp: 'WhatsApp Contact',
    lang_title: 'My Insurance',
    loading: 'Loading',

    lang1: 'My Insurance Order',
    lang2: 'My Policy',

    lang4: 'There is no content yet',

    unpaid: 'Unpaid',
    paid: 'Paid',
    pay: 'Pay',

    incomData: 'Incomplete Data',
    inUndwr: 'In Underwrting',
    undwFaid: 'Underwriting Failed',
    undwSucs: 'Underwriting Success',
    effct: 'Effective',

    lang_r_1: 'Myself',
    lang_r_2: 'Husband',
    lang_r_3: 'Wife',
    lang_r_4: 'Father',
    lang_r_5: 'Mother',
    lang_r_6: 'Child',
    lang_r_7: 'Siblings',

    lang_r_t_1: 'Insured',
    lang_r_t_2: 'Couple',
    lang_r_t_3: 'Child', 

    lang_r_b_1: 'Wife',
    lang_r_b_2: 'Husband',
    lang_r_b_3: 'Child',
    lang_r_b_4: 'Father',
    lang_r_b_5: 'Mother',
    lang_r_b_6: 'Brother',
    lang_r_b_7: 'Sister',

    lang9: 'Complete Data',
    lang10: 'Check',

    lang11: 'Insured',
    lang12: 'Protection Period',

    lang13: 'print',


    lang14: 'Unfilled',
    lang15: '',
    lang16: '',
    lang17: '',
    lang18: '',
    lang19: '',
    lang20: '',
    lang21: '',

};

export default translation;