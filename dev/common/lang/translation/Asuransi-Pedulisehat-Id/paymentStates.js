let translation = {};

translation.id = {
    JumpName: 'SiJi Corona Protection',
    lang1: 'Pembayaran Sukses!',
    lang2: 'Silakan melengkapi data polis dengan',
    lang3: 'klik tombol.',
    lang4: '',
    lang5: 'Lengkapi Data Polis',
    lang6: 'Setelah selesai mengisi informasi, layanan asuransi akan mulai berjalan.',
    lang7: 'Pembayaran Gagal !',
    lang8: 'Coba Lagi',

    lang9: 'Pengiriman Data Sukses !',
    lang10: 'Terima Kasih! Kami akan mengirim file polis via e-mail.',
    lang11: 'Cek Polis Ku',
    lang12: 'Pengiriman Data Gagal !',
    lang13: 'Coba Lagi',

};
translation.en = {
    JumpName: 'SiJi Corona Protection',
    lang1: 'Payment succeed!',
    lang2: 'Please ',
    lang3: 'click the button',
    lang4: ' to complete your policy data.',
    lang5: 'Complete My Policy Data',
    lang6: 'After completing the information, the insurance service will start.',
    lang7: 'Payment failed!',
    lang8: 'Try again',

    lang9: 'Data Submission Success!',
    lang10: 'Thank You! We will send your policy file via e-mail.',
    lang11: 'Check My Policy',
    lang12: 'Data Submission Failed!',
    lang13: 'Try Again',
};

export default translation;