let translation = {};

translation.id = {
    lang1: 'Mengundang',
    lang2: 'orang',
    lang3: 'Donasi Terkumpul',
    lang4: 'Apa itu peringkat suporter?',
    lang5: 'Kami menilai berdasarkan dari jumlah donasimu, ditambah dengan jumlah donasi dari teman lain yang kamu ajak ke kampanye galang dana.',
    lang6: 'Bagaimana meningkatkan peringkat:',
    lang7: 'Bantu penggalang dana untuk melakukan share (bagikan) halaman kampanye ke teman lain dan bantu meningkatkan jumlah donasinya.',
    lang8: 'Mengerti',
    lang9: 'Tingkatkan Peringkat Saya',
    lang10: '',
};
translation.en = {
    lang1: 'Invited',
    lang2: 'people',
    lang3: 'Donation Gathered',
    lang4: 'What is supporter ranking?',
    lang5: 'We rank according to the amount of your own donation amount, plus the sum of the donation amount from other friends brought by you to the campaign.',
    lang6: 'How to improve your ranking:',
    lang7: 'Help campaigner to share the campaign to more friends and raise more donations.',
    lang8: 'Got It',
    lang9: 'Improve My Ranking',
    lang10: '',
};

export default translation;