let modifyGoalTranslation = {};

modifyGoalTranslation.id = {
    lang1: 'Target',
    lang2: 'Masukan jumlah target baru',
    lang3: 'Simpan',
    lang4: 'Tujuan',
    lang5: 'Silahkan masukan nominal uang',
    lang6: 'Silahkan masukan bilangan bulat kelipatan dari Rp 10.000',
    lang7: 'Perubahan diterima, silakan menunggu verifikasi',
    lang8: 'Kampanye diperbarui'
};
modifyGoalTranslation.en = {
    lang1: 'Goal',
    lang2: 'Enter a new target amount',
    lang3: 'Save',
    lang4: 'Goal',
    lang5: 'please input money',
    lang6: 'Please enter an integer multiple of Rp 10.000',
    lang7: 'Changes received, please wait for verification',
    lang8: 'Campaign updated'
};

export default modifyGoalTranslation;
