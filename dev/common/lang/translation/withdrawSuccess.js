let withdrawSuccessTranslation = {};

withdrawSuccessTranslation.id = {
    lang1: 'Pengajuan telah dibuat',
    lang2: 'Mengerti',
};
withdrawSuccessTranslation.en = {
    lang1: 'Application is submitted',
    lang2: 'Got it',
};

export default withdrawSuccessTranslation;
