﻿let selectRelationshipTranslation = {};

selectRelationshipTranslation.id = {
  lang1: "Hubungan dengan pasien",
  lang2: "Pasien Sendiri",
  lang3: "Hubungan Keluarga",
  lang4: "Hubungan Pernikahan",
  lang5: "Rumah Sakit",
  lang6: "Organisasi Kesejahteraan Umum",
};
selectRelationshipTranslation.en = {
  lang1: "Relationship with patient",
  lang2: "Patient Himself",
  lang3: "Direct Relatives",
  lang4: "Conjugal Relation",
  lang5: "Hospital",
  lang6: "Commonweal Organization",
};

export default selectRelationshipTranslation;
