let translation = {};

translation.id = {
    lang1: 'Jumlah Total',
    lang2: 'Metode Pembayaran',
    lang3: 'Pilih Metode',
    lang4: 'Premi',
    lang5: 'Biaya Cetak Polis',
    lang6: 'Biaya Admin',
    lang7: 'Biaya Saluran Pembayaran',
    lang8: 'Bayar',
    lang9: '',
    lang10: '',
};
translation.en = {
    lang1: 'Total Amount',
    lang2: 'Payment Method',
    lang3: 'Choose Method',
    lang4: 'Premium',
    lang5: 'Policy Printing Fee',
    lang6: 'Admin Fee',
    lang7: 'Payment Channel Fee',
    lang8: 'Pay',
    lang9: '',
    lang10: '',
};

export default translation;