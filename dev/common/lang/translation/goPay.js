let goPayTranslation = {};

goPayTranslation.id = {
    lang1: 'Jumlah',
    lang2: 'Order ID',
    lang3: 'Bayar dengan GO-PAY',
};
goPayTranslation.en = {
    lang1: 'Amount',
    lang2: 'Order ID',
    lang3: 'Pay Now with GO-PAY',
};

export default goPayTranslation;