let followedCampaignsTranslation = {};

followedCampaignsTranslation.id = {
    lang1: 'Memuat',
    lang2: 'Aktif',
    lang3: 'Selesai',
    lang4: 'Belum ada konten',
    lang5: 'Jelajahi',
    lang6: 'Kampanye yang diikuti',
    lang7: 'gagal',
    lang8: 'Sukses',
    lang9: 'dibekukan',
};
followedCampaignsTranslation.en = {
    lang1: 'Loading',
    lang2: 'Active',
    lang3: 'Finished',
    lang4: 'There is no content yet',
    lang5: 'Explore',
    lang6: 'Campaigns Followed',
    lang7: 'Failed',
    lang8: 'Succeed',
    lang9: 'Freezed',
};

export default followedCampaignsTranslation;