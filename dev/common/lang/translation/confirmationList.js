let confirmationList = {};

confirmationList.id = {
    lang1: 'Konfirmasi dari User',
    lang2: 'menit yang lalu',
    lang3: 'Kerabat',
    lang4: 'Teman',
    lang5: 'Tetangga',
    lang6: 'Rekan',
    lang7: 'Guru',
    lang8: 'Teman sekolah',
    lang9: 'Sukarelawan',
    lang10: 'Lainnya',
    lang11: 'Bantu Konfirmasi',
    lang12: 'Undang Teman',
    lang13: 'Memuat...',
};
confirmationList.en = {
    lang1: 'People Confirmed',
    lang2: 'minutes ago',
    lang3: 'Relative',
    lang4: 'Friend',
    lang5: 'Neighbor',
    lang6: 'Colleague',
    lang7: 'Teacher',
    lang8: 'Schoolmate',
    lang9: 'Volunteer',
    lang10: 'Other',
    lang11: 'Help Verify',
    lang12: 'Invite Friends',
    lang13: 'Loading...',
};

export default confirmationList;