let systemTranslation = {};

systemTranslation.id = {
    lang1: 'Memuat...',
    lang2: 'Aplikasi belum diinstal',
    lang3: 'Pastikan kamu sudah memempunya aplikasi Shopee',
    lang4: '',
    lang5: '',
    lang6: '',
};
systemTranslation.en = {
    lang1: 'Loading...',
    lang2: 'The app is not installed',
    lang3: 'Please make sure you have installed Shopee app',
    lang4: '',
    lang5: '',
    lang6: '',
};

export default systemTranslation;