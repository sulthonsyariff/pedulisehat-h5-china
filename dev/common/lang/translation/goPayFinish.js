let goPayFinishTranslation = {};

goPayFinishTranslation.id = {
    lang1: 'Donasi Berhasil',
    lang2: 'Tinjau Kembali Kampanye',
};
goPayFinishTranslation.en = {
    lang1: 'Successfully Donated!',
    lang2: 'Review the Campaign',
};

export default goPayFinishTranslation;