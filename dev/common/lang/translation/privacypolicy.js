let privacyPolicyTranslation = {};

privacyPolicyTranslation.id = {
    lang1: 'Kebijakan Privasi',
    lang2: 'Id',
    lang3: 'Kebijakan Privasi',
    lang4: 'Kebijakan Privasi ini menetapkan kebijakan kami yang berkaitan dengan pengumpulan dan penggunaan Data Pribadi Pengguna Situs yang diberikan oleh para Pengguna Situs kepada Pengelola Situs dalam rangka pemanfaatan fasilitas, fitur, jasa, dan/atau layanan yang ditawarkan oleh Pengelola Situs melalui website www.pedulisehat.id maupun aplikasi mobile. Ketentuan-ketentuan menyangkut data mengikat seluruh Pengguna Situs tanpa terkecuali untuk tunduk dan patuh atas ketentuan-ketentuan yang telah ditetapkan oleh Pengelola Situs.',
    lang5: 'Peduli Sehat berkomitmen untuk melindungi privasi. Kami memahami bahwa pengunjung dan pengguna Platform Peduli Sehat memiliki kekhawatiran terkait privasi, serta kerahasiaan dan keamanan Data Pribadi yang diberikan.',
    lang6: 'Berikut beberapa ketentuan yang perlu diperhatikan:',
    lang7: 'Pengelola Situs atas data-data yang diberikan Para Pengguna Situs kepada Pengelola Situs sebagai pemenuhan syarat atas pemanfaatan fasilitas, fitur, jasa, dan/atau layanan yang ditawarkan oleh Pengelola Situs, berkewajiban untuk:',
    lang8: 'Menjaga kerahasiaan data-data para Pengguna yang tidak dapat ditampilkan dalam Situs dan/atau diberikan kepada pihak-pihak tertentu atas penggunaan jasa atau layanan Situs selama tidak ada perjanjian tertulis terlebih dahulu kepada Pengguna;',
    lang9: 'Kerahasiaan data Pengguna yang wajib dijaga oleh Pengelola Situs menjadi tidak berlaku apabila Pengelola Situs dipaksa oleh ketentuan hukum yang berlaku, perintah pengadilan, dan/atau perintah dari aparat/instansi yang berwenang, untuk membuka data-data milik Pengguna tersebut;',
    lang10: 'Pengelola Situs hanya bertanggung jawab atas data yang diberikan Pengguna Situs kepada Pengelola Situs sebagaimana yang telah ditentukan pada ketentuan sebelumnya;',
    lang11: 'Pengelola Situs tidak bertanggung jawab atas pemberian atau pertukaran data yang dilakukan sendiri antar Pengguna Situs;',
    lang12: 'Pengelola Situs berhak untuk mengubah ketentuan menyangkut data-data para Pengguna Situs tanpa pemberitahuan terlebih dahulu untuk mematuhi hukum yang berlaku dan operasi kami dengan tanpa mengabaikan hak para Pengguna Situs untuk dijaga kerahasiaan datanya sebagaimana yang telah ditentukan dalam butir (1).',
    lang13: 'Perubahan pada Kebijakan Privasi',
    lang14: 'Kami dapat mengubah Kebijakan Privasi ini dari waktu ke waktu. Kami tidak akan mengurangi hak Anda di dalam kebijakan privasi (privacy policy) tanpa pemberitahuan yang jelas dan eksplisit. Setiap perubahan akan ditandai dengan tanggal yang tertera di bagian atas Kebijakan Privasi ini.',
    lang15: 'Terakhir diubah: 15 October 2018',
    lang16: 'Copyright © 2019 Peduli Sehat Gotongroyong',
};

privacyPolicyTranslation.en = {
    lang1: 'Privacy Policy',
    lang2: 'Eng',
    lang3: 'Privacy Policy',
    lang4: 'This Privacy Policy sets out our policies relating to the collection and use of Personal Data of Site User provided by Site User to Site Manager in the context of utilizing facilities, features, services, and / or services offered by Site Manager through the website www.pedulisehat.id or mobile applications. The provisions regarding data are binding on all Site User without exception to being subject to and obeying the conditions set by the Site Manager.',
    lang5: 'Peduli Sehat is committed to protecting privacy. We understand that visitors and users of Peduli Sehat’s Platform have privacy concerns, as well as the confidentiality and security of the Personal Data provided.',
    lang6: 'Here are some provisions that need to be noticed:',
    lang7: 'Site Manager for the data provided by Site User to Site Manager as fulfilling the requirements for the utilization of facilities, features, services, and / or services offered by Site Manager, is obliged to:',
    lang8: 'Maintain the confidentiality of User data that cannot be displayed on the Site and / or given to certain parties for the use of Site services or services as long as there is no prior written agreement to the User;',
    lang9: 'The confidentiality of User data that must be maintained by the Site Manager becomes invalid if the Site Manager is forced by the applicable legal provisions, court orders, and / or orders from the competent authorities/agencies, to open data of the User;',
    lang10: 'Site Manager is only responsible for data provided by Site User to Site Manager as specified in the previous provisions;',
    lang11: 'Site Manager is not responsible for giving or exchanging data that is carried out alone between Site Users;',
    lang12: 'Site Manager have the right to change the provisions regarding the data of Site User without prior notice to comply with applicable laws and our operations without disregarding the rights of Site User to maintain the confidentiality of their data as specified in item (1).',
    lang13: 'Changes to the Privacy Policy',
    lang14: 'We may change this Privacy Policy from time to time. We will not reduce your rights in the privacy policy without clear and explicit notice. Each change will be marked with the date stated at the top of this Privacy Policy.',
    lang15: 'Last Modified: October 15, 2018',
    lang16: 'Copyright © 2019 Peduli Sehat Gotongroyong',
};

export default privacyPolicyTranslation;