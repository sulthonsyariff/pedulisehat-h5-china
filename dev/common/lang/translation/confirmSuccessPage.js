let confirmSuccessPageTranslation = {};

confirmSuccessPageTranslation.id = {
    lang1: 'Komentar berhasil diajukan',
    lang2: 'Tampilkan halaman kampanye',
};
confirmSuccessPageTranslation.en = {
    lang1: 'Submitted Successfully',
    lang2: 'view the campaign',
};

export default confirmSuccessPageTranslation;