﻿let myWalletTranslation = {};

myWalletTranslation.id = {
  lang1:
    "Kampanye anda sedang digalangkan. Anda bisa menarik dana setelah penggalangan selesai",
  lang2: "Mengerti",
  lang3: "Kampanyemu belum terverifikasi",
  lang4: "Batal",
  lang5: "Lakukan Verifikasi",
  lang6: "Saldo",
  lang7: "Tarik",
  lang8: "Transaksi",
  lang9: "Dikembalikan",
  lang10: "Dompet kampanye",
  lang11: "Memuat....",
  lang12: "Donasi",
  lang13: "Dikembalikan",
  lang14: "Diproses",
  lang15: "Gagal",
  lang16: "Belum ada transaksi",
  lang17: "Tidak valid",
  lang18:
    "Untuk pencocokan data, penarikan dana hanya dapat dilakukan 24 jam setelah kampanye berakhir.Terima kasih untuk kerja sama anda.",
  lang19:
    "Untuk proses rekonsiliasi data, penarikan hanya dapat dilakukan setelah 14 hari.",
  lang20: "hari",
  lang21: "hari",
  lang22: "Mohon masukan maksimal 500 karakter",
  lang23: "Silahkan mengisi tujuan penarikan",
  lang24: "Tutup",
  lang25: "Ajukan",
  lang26: "Tujuan Penarikan",
  lang27: "Jumlah Terdanai",
  lang28: "Biaya Layanan Platform",
  lang29: "Biaya Gerbang Pembayaran",
  lang30: "Donasi Bersih",
  lang31: "Donasi Bersih",
  lang32: "Saldo Bersih",
  lang33:
    "Maaf, saldo dompet kampanyemu kurang dari Rp 500.000 (persyaratan minimal)",
  lang34: "Cek Syarat",
  lang35:
    "Maaf, saldo dompet kampanyemu kurang dari Rp 20.000 (persyaratan minimal)",
  lang36: "Penarikan",
  lang37: "Biaya lain-lain",
};
myWalletTranslation.en = {
  lang1:
    "Your campaign is being fundraising.You can withdraw after your fundraising is over",
  lang2: "Got it",
  lang3: "Your campaign hasn’t been verified",
  lang4: "Cancel",
  lang5: "Go to Verify",
  lang6: "Balance",
  lang7: "Withdraw",
  lang8: "Transactions",
  lang9: "Refund",
  lang10: "Campaign wallet",
  lang11: "Loading...",
  lang12: "Donation",
  lang13: "Return",
  lang14: "Processing",
  lang15: "Failed",
  lang16: "No transactions yet",
  lang17: "Invalid",
  lang18:
    "For data reconcile process, withdrawal only can be done 24 hours after the campaign ended.Thank you for your cooperation.",
  lang19:
    "For data reconcile process, withdrawal only can be done after 14 days.",
  lang20: "day",
  lang21: "days",
  lang22: "Please input at most 500 charaters",
  lang23: "Please fill in the purpose of the withdrawal",
  lang24: "Close",
  lang25: "Submit",
  lang26: "Withdraw Purpose",
  lang27: "Funded Amount",
  lang28: "Platform Service Fee",
  lang29: "Payment Channel Fee",
  lang30: "Net Amount",
  lang31: "Net Donation",
  lang32: "Net Balance",
  lang33:
    "Sorry, your campaign wallet balance is less than the withdraw requirement Rp 500.000",
  lang34: "Check the Rules",
  lang35:
    "Sorry, your campaign wallet balance is less than the withdraw requirement Rp 20.000",
  lang36: "Withdraw",
  lang37: "Other expense",
};

export default myWalletTranslation;
