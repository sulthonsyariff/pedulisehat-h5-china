let noCaptchaTranslation = {};

noCaptchaTranslation.id = {
    lang1: 'Geser tombol ke kanan',
    lang2: 'Validasi Sukses',
    lang3: 'Validasi Gagal',
    lang4: 'Geser untuk verifikasi',
    lang8: 'Memuat...',

};
noCaptchaTranslation.en = {
    lang1: 'Slide button to the right',
    lang2: 'Validation Success',
    lang3: 'Validation Failed',
    lang4: 'Please slide to verify',
    lang8: 'Loading...',

    

};

export default noCaptchaTranslation;