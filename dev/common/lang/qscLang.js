import store from 'store'
import utils from 'utils';

let qscLang = {};
// let navLang;
let langCollection = {};
let langCodeArr = ['cn', 'tw', 'en', 'kr', 'jp', 'fr', 'de', 'it', 'ru', 'tl', 'id'];
// let CACHED_KEY = 'switchLanguage';

qscLang.init = function(translation) {
    for (let i = 0; i < langCodeArr.length; i++) {
        langCollection[langCodeArr[i]] = {};
        if (langCollection[langCodeArr[i]]) {
            langCollection[langCodeArr[i]] = translation[langCodeArr[i]];
        }
    };

    // 可通过获取本地缓存切换语言：switchLanguage:{lang:en}

    // 安卓客户端切换语言监听：印尼 in，英语 en
    // if (navigator.userAgent.indexOf('language') > -1) {
    //     if (navigator.userAgent.indexOf('language=in') > -1) {
    //         navLang = 'id'
    //     } else {
    //         navLang = 'en'
    //     }
    // } else {
    //     if (store.get(CACHED_KEY) && store.get(CACHED_KEY).lang) {
    //         navLang = store.get(CACHED_KEY).lang
    //     } else {
    //         navLang = 'id'
    //     }
    // }

    qscLang = langCollection[utils.getLanguage()];

    return qscLang;
}

export default qscLang;