
let obj = [{}, $('body')];
let CLICKABLE_COUNT = 'clickable_count';

obj.click = function(){
    let totalCount = sessionStorage.getItem(CLICKABLE_COUNT) ? sessionStorage.getItem(CLICKABLE_COUNT) : 0;
    const count = parseInt(totalCount) + 1
    sessionStorage.setItem(CLICKABLE_COUNT, count)
}

export default obj;