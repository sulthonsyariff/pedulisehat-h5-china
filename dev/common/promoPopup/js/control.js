import view from './main'
import model from './model'
import 'loading'

import utils from 'utils'
let UI = view.UI;

utils.showLoading();

UI.on('insertPopup', function() {
    console.log("INI INSERT");
    model.getPopupPromo({
        success: function(rs) {
            if (rs.code == 0) {
                view.insertPopup(rs);
                utils.hideLoading();
            }
        },
        error: utils.handleFail
    })
    
})
