import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

obj.getPopupPromo = function(o) {
    let url = domainName.project + '/v1/popup-insurance';

    if (isLocal) {
        url = '/mock/popup.json';
    }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
};


export default obj;