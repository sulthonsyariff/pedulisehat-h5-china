import promoPopup from '../../../common/promoPopup/tpl/main.juicer'
import model from './model'
import utils from 'utils'

let [obj, $UI] = [{}, $('body')];

let CLICKABLE_COUNT = 'clickable_count';
let CLICKABLE_TIME  = 'clickable_time';
let CLICKABLE_SESSION  = 'clickable_session';
let CLICKABLE_PROMO  = 'clickable_promo';
let clickable_count = sessionStorage.getItem(CLICKABLE_COUNT) ? sessionStorage.getItem(CLICKABLE_COUNT) : 0;
let clickable_time = sessionStorage.getItem(CLICKABLE_TIME) ? sessionStorage.getItem(CLICKABLE_TIME) : 0;
let clickable_session = localStorage.getItem(CLICKABLE_SESSION) ? localStorage.getItem(CLICKABLE_SESSION) : clickable_time;
let clickable_promo = localStorage.getItem(CLICKABLE_PROMO) ? localStorage.getItem(CLICKABLE_PROMO) : 0;

obj.UI = $UI;
obj.init = function(rs) {
    if(clickable_count == 0){
        clickable_time = new Date().getTime()
        sessionStorage.setItem(CLICKABLE_TIME, clickable_time)
    }

    if(clickable_count >= 10 || diffMinutes(clickable_time) >= 5){

        if(new Date().getTime() >= clickable_session){
            clickable_promo = 0
            localStorage.setItem(CLICKABLE_PROMO, clickable_promo)
        }

        if(clickable_promo == 0){
            localStorage.setItem(CLICKABLE_SESSION, timeNowAddHours(3))
            sessionStorage.setItem(CLICKABLE_TIME, new Date().getTime())
            sessionStorage.setItem(CLICKABLE_COUNT, 0)
            localStorage.setItem(CLICKABLE_PROMO, 1)

            model.getPopupPromo({
                success: function(rs) {
                    console.log("POPUP PROMO", rs);
                    if (rs.code == 0) {
                        var data = rs.data[0];
                        data.image = JSON.parse(data.image).image
                        $UI.append(promoPopup(data));
                    }
                },
                error: utils.handleFail
            })
        }
    }

    console.log("DATE TIME", new Date().getTime());
    console.log("IS_CLICKABLE_SESSION", new Date().getTime() >= clickable_session);
    console.log("CLICKABLE_COUNT", clickable_count);
    console.log("CLICKABLE_TIME", clickable_time);
    console.log("CLICKABLE_SESSION", clickable_session);
    console.log("CLICKABLE_PROMO", clickable_promo);

    // 弹窗： close toast
    $('body').on('click', '.close-pop,.mask', function() {
        // sensors.track('SearchBarClick')
        $('.promoPopup').css('display', 'none')
    })
}

const diffMinutes = (time) => {
    var diff =(new Date().getTime() - time) / 1000;
    diff /= 60;
    const minutes = Math.abs(Math.round(diff));
    console.log("CLICKABLE MINUTES :", minutes);
    return minutes
}

const timeNowAddHours = (h) => {
    return new Date().getTime() + (h*60*60*1000)
}

export default obj;