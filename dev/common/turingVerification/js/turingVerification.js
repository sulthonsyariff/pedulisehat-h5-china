/****
 */
import domainName from 'domainName' // port domain name
import fastclick from 'fastclick'
import mainTpl from '../tpl/turingVerification.juicer'
import utils from 'utils'
import '../less/main.less'
// import model from './model'
// import store from 'store'
import 'noCaptchaJs'

/* translation */
import turingVerify from 'turingVerify'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(turingVerify);
/* translation */

let reqObj = utils.getRequestParams();
let obj = {};
let $UI = $('body');

let nc;

fastclick.attach(document.body);

/**

 *
 */
obj.init = function(rs) {
    // appendShareTpl(rs)
    console.log('rs=====', rs);
    rs.lang = lang
    rs.domainName = domainName;
    rs.getLanguage = utils.getLanguage();
    $UI.prepend(mainTpl(rs)); // 带close的模版
    clickHandle(rs)

}


obj.nc_cb = function(sumbitData) {
    $('.common-verify').css('display', 'block');
    // nc.options.callback(sumbitData)
    // console.log('nc_cb',nc.options.callback());

    var nc_token = ["CF_APP_1", (new Date()).getTime(), Math.random()].join(':');
    nc = NoCaptcha.init({
        //声明滑动验证需要渲染的目标元素ID。
        renderTo: '#nc',
        //应用类型标识。它和使用场景标识（scene字段）一起决定了滑动验证的业务场景与后端对应使用的策略模型。您可以在人机验证控制台的配置管理页签找到对应的appkey字段值，请务必正确填写。
        appkey: 'FFFF0N000000000093BD',
        //使用场景标识。它和应用类型标识（appkey字段）一起决定了滑动验证的业务场景与后端对应使用的策略模型。您可以在人机验证控制台的配置管理页签找到对应的scene值，请务必正确填写。
        scene: 'nc_message_h5',
        //滑动验证码的主键，请勿将该字段定义为固定值。确保每个用户每次打开页面时，其token值都是不同的。系统默认的格式为：”您的appkey”+”时间戳”+”随机数”。
        token: nc_token,
        //业务键字段，可为空。为便于线上问题的排查，建议您按照线上问题定位文档中推荐的方法配置该字段值。
        trans: {
            "key1": "code0"
        },
        //语言，默认值为cn（中文）。HTML5应用类型默认支持简体中文、繁体中文、英文语言。
        language: "en",
        //内部网络请求的超时时间。一般情况建议保持默认值（10000ms）。
        timeout: 10000,
        //允许服务器超时重复次数，默认5次。
        retryTimes: 5,
        //验证通过后，验证码组件是否自动隐藏，默认不隐藏（false）。
        bannerHidden: false,
        //是否默认不渲染，默认值false。当设置为true时，不自动渲染，需要自行调用show方法进行渲染。
        initHidden: false,
        //前端滑动验证通过时会触发该回调参数。您可以在该回调参数中将请求标识（token）、会话ID（sessionid）、签名串（sig）字段记录下来，随业务请求一同发送至您的服务端调用验签。
        callback: function(data) {
            window.console && console.log(data)

            window.console && console.log(nc_token)
            window.console && console.log(data.csessionid)
            window.console && console.log(data.sig)
            setTimeout(() => {
                nc.reset();
                sumbitData.token = nc_token
                sumbitData.session_id = data.csessionid
                sumbitData.sig = data.sig
                $UI.trigger('sendCode', [sumbitData]);
                console.log('turing-send', [sumbitData]);
                $('.common-verify').css('display', 'none');
            }, 500);

        },
        error: function(s) {
            nc.reset();
        }
    });


    NoCaptcha.setEnabled(true);
    //请务必在此处调用一次reset()方法。
    //用于配置滑动验证的自定义文案。详细信息，请参见自定义文案与多语言文档。
    NoCaptcha.upLang('en', {
        //加载状态提示。
        'LOADING': lang.lang8,
        //等待滑动状态提示。
        'SLIDER_LABEL': lang.lang2,
        //验证通过状态提示。
        'CHECK_Y': lang.lang3,
        //验证失败触发拦截状态提示。
        'ERROR_TITLE': lang.lang4
    });
    nc.reset();


}

/*
 */
function clickHandle(param) {
    //close
    $('body').on('click', '.close', function() {
        $('.common-verify').css('display', 'none')
            // $('.common-verify').remove()
    });

}


//倒计时
obj.invokeWhatsAppSettime = function(obj) {
    invokeWhatsAppSettime("#sendCode");
}

obj.invokeSMSSettime = function(obj) {
    invokeSMSSettime(".SMSCode");
}

function invokeWhatsAppSettime(obj) {
    var countdown = 120;
    settime(obj);

    function settime(obj) {
        if (countdown == 0) {
            $(obj).attr("disabled", false).removeClass('active');
            $(obj).text(lang.lang10);
            // store.remove('countDown',countdown)
            countdown = 120;
            return;
        } else {
            $(obj).attr("disabled", true).addClass('active');
            $(obj).text("" + countdown + "s");
            countdown--;
            // store.set('countDown',countdown)
        }
        setTimeout(function() {
            settime(obj)
        }, 1000)
    }
}


function invokeSMSSettime(obj) {
    utils.alertMessage(lang.lang24);
    var countSMSdown = 120;
    settime(obj);

    function settime(obj) {
        if (countSMSdown == 0) {
            $(obj).attr("disabled", false).css({
                'pointer-events': 'auto',
                'color': '#999'
            });
            // $(obj).text(lang.lang23);
            $(obj).find('p ._numS').html('');

            countSMSdown = 120;
            return;
        } else {
            $(obj).attr("disabled", true).css({
                'pointer-events': 'none',
                'color': '#ccc'
            });

            $(obj).find('p ._numS').html("(" + countSMSdown + "s)");

            countSMSdown--;
        }
        setTimeout(function() {
            settime(obj)
        }, 1000)
    }
}


obj.changeBtn = function() {
    $('.verifyPass').css('display', 'block')
    $('.submitBtn').css('display', 'none')

    setTimeout(function() {
        $('.common-verify').css('display', 'none')
        $('.common-verify').remove()

    }, 2000)

}


export default obj;