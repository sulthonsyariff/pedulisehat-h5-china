import mainTpl from '../tpl/Qurban-bankList.juicer'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import model from '../js/_model'
import "../less/Qurban-bankList.less";

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let from = reqObj['from'];
let bankCode;

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {
    console.log('qurban-Banklist');

    // res.domainName = domainName;

    if (from == 'OVOXPeduliSehat') {
        bankCode = 4
    } else if (from == 'sinarmas') {
        bankCode = 2
    } else {
        bankCode = ''
    }
    getPayment(res);

    // $UI.trigger('calculationFormula'); //计算公式

    fastclick.attach(document.body);
    obj.event(res);
};

function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: bankCode, // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function(rs) {
            if (rs.code == 0) {
                rs.commonLang = commonLang;
                rs.isAndroid = utils.browserVersion.android;


                for (let i = 0; i < rs.data.length; i++) {
                    // handle image
                    if (rs.data[i].image) {
                        rs.data[i].image = JSON.parse(rs.data[i].image);
                    }
                }
                $UI.prepend(mainTpl(rs));

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}


obj.event = function(res) {
    // close
    // $('.alert-qurban-bank .close').on('click', function (rs) {
    //     $('.alert-qurban-bank').css('display', 'none')
    // })


    $('body').on('click', '.bank-mask', function(e) {
        $('.alert-qurban-bank').css('display', 'none')

    })
    $('body').on('click', '.close', function(e) {
        $('.alert-qurban-bank').css('display', 'none')

    })

    // choose payment method
    $('body').on('click', '.list-items', function(e) {
        $('.list-items').removeClass('choosed-bank');
        $('.alert-qurban-bank').attr({
            'data-bankcode': $(this).attr('data-bankcode'),
            'data-methodfee': $(this).attr('data-methodfee'),
            'data-expenseType': $(this).attr('data-expenseType'),

            'data-bankImg': $(this).attr('data-bankImg'),
            'data-bankName': $(this).attr('data-bankName'),
        });
        $(this).addClass('choosed-bank');

        setTimeout(() => {
            $('.alert-bank').css('display', 'none')
        }, 100)

        $('.channel').addClass('selected').html($(this).html()).attr({
            'data-bankcode': $(this).attr('data-bankcode'),
            'data-methodfee': $(this).attr('data-methodfee'),
            'data-expenseType': $(this).attr('data-expenseType'),
        });
        // store.set('chooseBank',{
        //     'bankcode': $(this).attr('data-bankcode'),
        //     'bankImg': $(this).attr('data-bankImg'),
        //     'bankName': $(this).attr('data-bankName'),

        //     'originPrice': '',
        //     'channelFee': '',
        // })
        $UI.trigger('chooseQurbanBankNext'); //计算公式


        $('.alert-qurban-bank').css('display', 'none')

    })
}

export default obj;