import mainTpl from '../tpl/common-bankList.juicer'
import fastclick from 'fastclick'
import store from 'store'
import domainName from 'domainName'; //port domain name
import model from './_model'
import "../less/common-bankList.less";
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];
let from = reqObj['from'];
let bankCode;
let result;
let gopayData = null;
let allResult = null;

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {
    console.log('qurban-Banklist');

    if (from == 'OVOXPeduliSehat') {
        bankCode = 4
    } else if (from == 'sinarmas') {
        bankCode = 2
    } else {
        bankCode = ''
    }
    // res.domainName = domainName;

    getUserInfo();

    fastclick.attach(document.body);
    obj.event(res);

    setTimeout(function() {
        getPayment(res);
    }, 300);
};

function getUserInfo() {
    model.getUserInfo({
        success: function(rs) {
            if (rs.code == 0) {
    
                getGopayBalance();
    
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        unauthorizeTodo: function(rs) {
            // 加载主模版
            console.log("unauthorizeTodo gais");
        },
        error: utils.handleFail,
    });
}

function getGopayBalance() {
    model.getGopayBalance({
        success: function(rs) {
            if (rs.code == 0) {
                console.log('response gopay balance', rs.data);
                gopayData = rs;
            }
        }
    })
}

function getPayment(res) {
    model.getPayment({
        param: {
            bank_code: bankCode, // OVO合作项目只需要展示OVO:bank_code=4
            // bank_code: (from == 'OVOXPeduliSehat' ? 4 : '') // OVO合作项目只需要展示OVO:bank_code=4
        },
        success: function(rs) {
            let tempData = [];
            if (rs.code == 0) {
                rs.commonLang = commonLang;
                rs.isAndroid = utils.browserVersion.android;
                rs.languange = utils.getLanguage();

                rs.from = from

                // filter data platform device
                for (let i = 0; i < rs.data.length; i++) {
                    if (utils.getBrowserDevice() == "Android-H5") {
                        if (rs.data[i].platform.mobile == true) {
                            tempData.push(rs.data[i]);
                        } else if (rs.data[i].platform.mobile == true && rs.data[i].platform.desktop == true) {
                            tempData.push(rs.data[i]);
                        }
                    } else {
                        if (rs.data[i].platform.desktop == true) {
                            tempData.push(rs.data[i]);
                        } else if (rs.data[i].platform.mobile == true && rs.data[i].platform.desktop == true) {
                            tempData.push(rs.data[i]);
                        }
                    }
                    // handle image
                    if (rs.data[i].image) {
                        rs.data[i].image = JSON.parse(rs.data[i].image);
                    }
                }
                rs.data = tempData;

                // result = rs;
                // not linking or not login
                if (gopayData == null || gopayData.data.GopayBalance == "") {
                    gopayData = null;
                    rs.gopayData = null;
                } else {
                    rs.gopayData = gopayData;
                    rs.gopayData.data.gopayBalanceDisplay = changeMoneyFormat.moneyFormat(gopayData.data.GopayBalance);
                }

                rs.gopayData = gopayData;
                
                console.log("get payment rs", rs);
                
                allResult = gopayData;
                $UI.prepend(mainTpl(rs));

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

obj.getGopayData = function () {
    return allResult;
}

obj.event = function(res, result) {
    // close
    // $('.alert-qurban-bank .close').on('click', function (rs) {
    //     $('.alert-qurban-bank').css('display', 'none')
    // })
    


    $('body').on('click', '.bank-mask', function(e) {
        $('.alert-common-bank').css('display', 'none')

    })
    $('body').on('click', '.close', function(e) {
        $('.alert-common-bank').css('display', 'none')

        if ($('.alert-common-bank').css('display') == 'block') {
            $('.wrap-bank-note').css("display", "block");
            $('.bank-note-gopay-linking').css("margin-top", "0");
        } else {
            $('.wrap-bank-note').css("display", "none");
            $('.bank-note-gopay-linking').css("margin-top", "-12px");
        }
    })

    // choose payment method
    $('body').on('click', '.list-items', function(e) {
        // if disabled
        if ($(this).hasClass('disabled')) {
            // 
        }
        // if not disabled
        else {
            $('.list-items'). removeClass('choosed-bank');
            $('.alert-common-bank').attr({
                'data-bankcode': $(this).attr('data-bankcode'),
                'data-methodfee': $(this).attr('data-methodfee'),
                'data-expenseType': $(this).attr('data-expenseType'),
    
                'data-bankImg': $(this).attr('data-bankImg'),
                'data-bankName': $(this).attr('data-bankName'),
            });
            $(this).addClass('choosed-bank');
    
            setTimeout(() => {
                $('.alert-bank').css('display', 'none')
            }, 100)

            $('.channel').addClass('selected').html($(this).html()).attr({
                'data-bankcode': $(this).attr('data-bankcode'),
                'data-methodfee': $(this).attr('data-methodfee'),
                'data-expenseType': $(this).attr('data-expenseType'),
            });            

            $('.channel .total-saldo').remove();
            
            // store.set('chooseBank',{
            //     'bankcode': $(this).attr('data-bankcode'),
            //     'bankImg': $(this).attr('data-bankImg'),
            //     'bankName': $(this).attr('data-bankName'),
    
            //     'originPrice': '',
            //     'channelFee': '',
            // })
            $UI.trigger('chooseCommonBankNext'); //计算公式
    
            $('.alert-common-bank').css('display', 'none')
            
            if ($('.alert-common-bank').css('display') == 'block') {
                $('.wrap-bank-note').css("display", "block");
                $('.bank-note-gopay-linking').css("margin-top", "0");
            } else {
                $('.wrap-bank-note').css("display", "none");
                $('.bank-note-gopay-linking').css("margin-top", "-12px");
            }    
        }
    })
}

export default obj;