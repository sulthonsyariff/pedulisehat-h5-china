import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import utils from 'utils'

// let reqObj = utils.getRequestParams();
// let showSP = reqObj['showSP'] || false; //是否展示shopeepay

let obj = {};

//get payment
obj.getPayment = function(o) {
    let url = domainName.trade + '/v2/pay_channel' +
        (o.param.bank_code ? ('?bank_code=' + o.param.bank_code) : '');
    //  +
    // (showSP ? '' : ('?bank_code_hidden=' + '6,7'));

    // if (isLocal) {
    //     url = 'mock/v2_pay_channel_donate.json'
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

// get gopay
obj.getGopayBalance = function(o) {
    let url = domainName.passport + '/v1/gopay/balance';

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

// get user info
obj.getUserInfo = function(o) {
    let url = domainName.passport + '/v1/user';

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o, 'unauthorizeTodo')
};
export default obj;