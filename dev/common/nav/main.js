import './main.less'
import tpl from './main.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import utils from 'utils'
import appDownload from 'appDownload'

/* translation */
import qscLang from 'qscLang'
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');

/* storage key */
let APP_DOWNLOAD_KEY = 'app_download';
let app_download = sessionStorage.getItem(APP_DOWNLOAD_KEY) ? sessionStorage.getItem(APP_DOWNLOAD_KEY) : {};

fastclick.attach(document.body);

if (utils.browserVersion.androids) {
    $('body').addClass('inAndroid');
}

obj.init = function(rs) {
    rs.commonLang = commonLang;
    rs.domainName = domainName;

    if (!utils.browserVersion.android) {
        $UI.prepend(tpl(rs)); // 带close的模版
        appDownload.init($('.app-download-wrapper')); //不在android内才加载
    }

    obj.event(rs);
}

obj.event = function(res) {
    // goBack
    $('body').on('click', '#goBack', function(e) {
        if (res.commonNavGoToWhere) {
            location.href = res.commonNavGoToWhere;
        } else {
            window.history.go(-1);
        }
    });
}

export default obj;