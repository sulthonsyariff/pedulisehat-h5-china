/****
 * 2020-5-6 戴莎莎
 * 分享弹窗组件：（互助分享组件）
 */
import domainName from 'domainName' // port domain name
import fastclick from 'fastclick'
import shareTpl from '../tpl/gtry.juicer'
import utils from 'utils'
import '../less/main.less'
import model from './_model'
/* translation */
import qscLang from 'qscLang'
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let reqObj = utils.getRequestParams();
let obj = {};
let $UI = $('body');
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

let shareCallBackName; //分享成功回调
let shareTplClassName; //share juicer 类名，控制展示隐藏点击绑定
let shareUrl; //普通项目分享地址
let shareWords = [];

fastclick.attach(document.body);

/**
 item_id y string 分享的产品ID
 item_type y int 产品类型
 item_short_link N string 众筹项目的短链接
 remark N `	string	标记（现阶段传勋章名字）

 item_id: '999999999' 勋章类型 互助首页, / "222222222" 新冠 / "111111111" 年度报告 /
 item_type: 0 互助 / 1 project / 2 专题 / 3 代表勋章
 满足这两个条件传remark字段
 *
 */
obj.init = function(param) {
    shareTplClassName = param.shareTplClassName; //分享弹窗名
    shareCallBackName = param.shareCallBackName; //分享成功回调名


    // 先获取分享地址 分享语
    // 优化：可以考虑把分享语分享地址存本地
    model.getShareInfo({
        param: {
            item_id: param.item_id,
            item_type: param.item_type,
            item_short_link: param.item_short_link || '',
            remark: param.remark || '' //标记（现阶段传勋章名字）,仅勋章分享需要
        },
        success: function(rs) {
            if (rs.code == 0) {
                shareUrl = rs.data.url //普通项目分享地址
                shareWords = rs.data.forwarding_desc

                if ($('.' + shareTplClassName).length) {
                    $('.' + shareTplClassName).css('display', 'block');
                } else {
                    rs.commonLang = commonLang;
                    appendShareTpl(param); //先获取地址，在弹窗
                }

                clickHandle();

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
}

export default obj;

/*****
 * 吊起安卓分享弹窗
 */
// 如果是安卓端： 吊起安卓分享弹窗
$UI.on('android-share', function(e, paramString, param) {
    console.log('===android-share===', paramString, param);

    model.getShareInfo({
        param: {
            item_id: param.item_id,
            item_type: param.item_type,
            item_short_link: param.item_short_link || '',
            remark: param.remark || '' //标记（现阶段传勋章名字）,仅勋章分享需要
        },
        success: function(rs) {
            if (rs.code == 0) {
                let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'));
                let androidOldUrlArr;
                let androidOldUrl;

                if (appVersionCode <= 190) {
                    androidOldUrlArr = rs.data.url.split("/")
                    androidOldUrl = androidOldUrlArr[0] + '/' + androidOldUrlArr[1] + '/' + androidOldUrlArr[2] + '/gtry/' + androidOldUrlArr[3]
                } else {
                    androidOldUrl = rs.data.url
                }

                // paramString :详情页的时候会多title/description/image等参数，此数据是在详情获取
                let android_share_url =
                    "native://share/webpage?" +
                    "url=" + encodeURIComponent(androidOldUrl) +
                    paramString || '' +
                    // "&actTitle=" + encodeURIComponent(actTitle) +
                    // "&actDesc=" + encodeURIComponent(actDesc) +
                    "&forwardingDesc=" + encodeURIComponent(rs.data.forwarding_default + ' %s') +
                    "&forwardingMap=" + encodeURIComponent(JSON.stringify(rs.data.forwarding_desc));

                console.log('===android_share_url===', android_share_url);

                location.href = android_share_url;

            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })
})

function appendShareTpl(rs) {
    rs.domainName = domainName;
    rs.commonLang = commonLang;
    rs.getLanguage = utils.getLanguage();

    $UI.prepend(shareTpl(rs)); // 带close的模版

    $('.' + shareTplClassName).css('display', 'block');
}

/*
     TargetTypefacebook = 1 // facebook
     TargetTypeWhatsApp = 2 // whatsapp
     TargetTypeTwitter = 3 // twitter
     TargetTypeLink = 4 // link
     TargetTypeLine = 5 // line
     TargetTypeInstagram = 6 // instagram
     TargetTypeYoutube = 7 // youtube

     注意： 需要获取最新的shareTplClassName / shareUrl / shareWords， 需要每次init的时候获取最新的， 所以没有放在外面
*/
function clickHandle() {
    // 先解绑，再绑定，以免每次init的时候重复绑定多个事件！！！
    $('.' + shareTplClassName + ' .Facebook,' +
        '.' + shareTplClassName + ' .Line,' +
        '.' + shareTplClassName + ' .Twitter,' +
        '.' + shareTplClassName + ' .WhatsApp').off('click').on('click',

        function(e) {
            e.preventDefault();

            if (shareUrl) {
                // console.log('shareUrl==', shareUrl, shareWords[1], shareCallBackName);

                if ($(this).hasClass('Facebook')) {

                    utils.fbShare(shareUrl + '_1', callback);
                    //分享计数成功后执行
                    function callback() {
                        $UI.trigger(shareCallBackName, 1);
                    }

                } else if ($(this).hasClass('Line')) {

                    utils.lineShare(shareWords[1], shareUrl + '_5', callback);
                    //分享计数成功后执行
                    function callback() {
                        $UI.trigger(shareCallBackName, 5);
                    }

                } else if ($(this).hasClass('Twitter')) {

                    utils.twitterShare(shareWords[1], shareUrl + '_3', callback);
                    //分享计数成功后执行
                    function callback() {
                        $UI.trigger(shareCallBackName, 3);
                    }

                } else if ($(this).hasClass('WhatsApp')) {

                    utils.whatsappShare(shareWords[1], shareUrl + '_2', callback);
                    //分享计数成功后执行
                    function callback() {
                        $UI.trigger(shareCallBackName, 2);
                    }

                }
            } else {
                console.log('!!!shareUrl==');

                utils.alertMessage(commonLang.lang43)
            }

            // 分享后关闭多渠道分享弹窗
            $('.' + shareTplClassName).css('display', 'none');

            return false;
        });

    // close 关闭对应分享弹窗
    $('.' + shareTplClassName + ' .close').off('click').on('click', function() {
        $('.' + shareTplClassName).css('display', 'none');
    });
}