import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};

//获取分享相关内容
obj.getTuringVerifyImg = function(o) {
    let url = domainName.passport + '/v1/image/url';
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
}


export default obj;