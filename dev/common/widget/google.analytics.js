 (function(i, s, o, g, r, a, m) {
     i['GoogleAnalyticsObject'] = r;
     i[r] = i[r] || function() {
         (i[r].q = i[r].q || []).push(arguments)
     }, i[r].l = 1 * new Date();
     a = s.createElement(o),
         m = s.getElementsByTagName(o)[0];
     a.async = 1;
     a.src = g;
     m.parentNode.insertBefore(a, m)
 })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

 ga('create', 'UA-127450137-1', 'auto'); // master
 ga('send', 'pageview');

 /**
  * 发送pageview网页跟踪数据
  * @param data
  * @param data.userId           用户id
  */
 function sendPageView(data) {
     for (var key in data) {
         ga('set', key, data[key]);
     }
     ga('send', 'pageview');
 }

 /**
  * 发送event事件跟踪数据 手动传参方式
  * @param data
  * @param data.eventCategory    事件类别,通常是用户与之互动的对象
  * @param data.eventAction      互动类型
  * @param data.eventLabel       用于对事件进行分类
  * @param data.eventValue       与事件相关的数值(int类型)
  * @param data.transport        信标传输(beacon)
  */
 function sendEvent(data) {
     ga('send', 'event', data);
 }

 /**
  * 事件跟中, 绑定事件方式
  */
 $('body').on('click', '.analytics', function() {
     var param = {
         eventCategory: $(this).data('category'), //事件类别
         eventAction: $(this).data('action'), //事件操作
         eventLabel: $(this).data('label'), //事件标签
         eventValue: parseInt($(this).data('value')),
         transport: 'beacon' //出站链接和表单可能会非常难于跟踪，因为大多数浏览器会在开始加载新网页时停止在当前网页上执行 JavaScript
     };
     sendEvent(param);
 });

 export default {
     sendPageView: sendPageView,
     sendEvent: sendEvent
 };