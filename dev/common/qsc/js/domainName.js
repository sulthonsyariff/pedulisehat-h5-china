/**
 * 不同环境不同域名
 * QA: -qa.pedulisehat.id
 * pre: -pre.pedulisehat.id
 * www: .pedulisehat.id
 *
heoproduct.pedulisehat.id
heoproduct-qa.pedulisehat.id
heoproduct-pre.pedulisehat.id

heouic.pedulisehat.id
heouic-qa.pedulisehat.id
heouic-pre.pedulisehat.id
 */
let obj = {};

var urlStr = location.host;

if (urlStr.indexOf("qa.pedulisehat.id") != -1
    // || urlStr.indexOf("gtry-qa.pedulisehat.id") != -1
) {
    // QA
    obj.passport = '//passport-qa.pedulisehat.id';
    obj.project = '//project-qa.pedulisehat.id';
    obj.psuic = '//psuic-qa.pedulisehat.id';
    obj.static = '//static-qa.pedulisehat.id';
    obj.trade = '//trade-qa.pedulisehat.id';
    obj.share = '//share-qa.pedulisehat.id';
    obj.heoproduct = '//heoproduct-qa.pedulisehat.id'; //互助产品
    obj.heouic = '//heouic-qa.pedulisehat.id'; //互助用户
    obj.base = '//base-qa.pedulisehat.id';
    obj.activity = '//activity-qa.pedulisehat.id';
    obj.insmd = '//insmd-qa.pedulisehat.id';
    obj.mns = '//mns-qa.pedulisehat.id';
    obj.zakat = '//base.pedulisehat.id';


} else if (urlStr.indexOf("dev.pedulisehat.id") != -1
    //  || urlStr.indexOf("gtry-pre.pedulisehat.id") != -1
) {
    // dev
    obj.passport = '//passport-dev.pedulisehat.id';
    obj.project = '//project-dev.pedulisehat.id';
    obj.psuic = '//psuic-dev.pedulisehat.id';
    obj.static = '//static-dev.pedulisehat.id';
    obj.trade = '//trade-dev.pedulisehat.id';
    obj.share = '//share-dev.pedulisehat.id';
    obj.heoproduct = '//heoproduct-dev.pedulisehat.id'; //互助产品
    obj.heouic = '//heouic-dev.pedulisehat.id'; //互助用户
    obj.base = '//base-dev.pedulisehat.id';
    obj.activity = '//activity-dev.pedulisehat.id';
    obj.insmd = '//insmd-dev.pedulisehat.id';
    obj.mns = '//mns-dev.pedulisehat.id';
    obj.zakat = '//base.pedulisehat.id';

} else if (urlStr.indexOf("pre.pedulisehat.id") != -1
    //  || urlStr.indexOf("gtry-pre.pedulisehat.id") != -1
) {
    // pre
    obj.passport = '//passport-pre.pedulisehat.id';
    obj.project = '//project-pre.pedulisehat.id';
    obj.psuic = '//psuic-pre.pedulisehat.id';
    obj.static = '//static-pre.pedulisehat.id';
    obj.trade = '//trade-pre.pedulisehat.id';
    obj.share = '//share-pre.pedulisehat.id';
    obj.heoproduct = '//heoproduct-pre.pedulisehat.id'; //互助产品
    obj.heouic = '//heouic-pre.pedulisehat.id'; //互助用户
    obj.base = '//base-pre.pedulisehat.id';
    obj.activity = '//activity-pre.pedulisehat.id';
    obj.insmd = '//insmd-pre.pedulisehat.id';
    obj.mns = '//mns-pre.pedulisehat.id';
    obj.zakat = '//base.pedulisehat.id';

} else if (urlStr.indexOf("www.pedulisehat.id") != -1 ||
    urlStr.indexOf("pedulisehat.id") != -1
    // || urlStr.indexOf("gtry.pedulisehat.id") != -1
) {
    // master
    obj.passport = '//passport.pedulisehat.id';
    obj.project = '//project.pedulisehat.id';
    obj.psuic = '//psuic.pedulisehat.id';
    obj.static = '//static.pedulisehat.id';
    obj.trade = '//trade.pedulisehat.id';
    obj.share = '//share.pedulisehat.id';
    obj.heoproduct = '//heoproduct.pedulisehat.id'; //互助产品
    obj.heouic = '//heouic.pedulisehat.id'; //互助用户
    obj.base = '//base.pedulisehat.id';
    obj.activity = '//activity.pedulisehat.id';
    obj.insmd = '//insmd.pedulisehat.id';
    obj.mns = '//mns.pedulisehat.id';
    obj.zakat = '//base.pedulisehat.id';

}

export default obj;
