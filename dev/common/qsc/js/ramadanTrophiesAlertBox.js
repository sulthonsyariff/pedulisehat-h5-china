import mainTpl from '../tpl/ramadanTrophies.juicer'
import utils from 'utils'
import '../less/ramadanTrophies.less'

import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import fastclick from 'fastclick'

import share from 'share'
import ramadanTrophiesAlertBoxLang from 'ramadanTrophiesAlertBoxLang'
import qscLang from 'qscLang'
let alertLang = qscLang.init(ramadanTrophiesAlertBoxLang);

/* translation */
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */


let reqObj = utils.getRequestParams();
let obj = {};
let $UI = $('body');
let _isDonatedOrshare;

// let forwarding_default;
// let forwarding_desc;
// let android_share_url;
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';

let shareUrl; //分享地址
let shareWords = [];

fastclick.attach(document.body);

/*****
 * !!!备注：（2020-5-1 勋章分享备注：）
 * 因为勋章分享弹窗和页面原有的分享弹窗存在混淆，
 * 虽然样式功能都一样，但是并不是所有都点击事件都是点击后再获取分享地址（项目详情的分享弹窗就不是），
 * 如果公用一个JS文件，分享地址无法独立，存在覆盖的问题，
 * 以及元素点击事件重复绑定，重复执行等原因，
 * 估没有公用一个JS文件，后期待优化！！！
 *
 */
obj.init = function(param) {
    console.log('====ramadanTrophies inits===');

    _isDonatedOrshare = param.isDonatedOrshare == 'donated' || param.isDonatedOrshare == 'list_donated' ?
        'donated' : 'share';

    ramadanDonatedOrShared(param);
};

function ramadanDonatedOrShared(param) {
    // 此处处理原因：
    // 1。例如分享第一次，弹窗1级勋章，不刷新，继续分享至第二级勋章时，还是旧的勋章；
    // 2。去掉已有的勋章弹窗，获取最新等级
    $('.common-trophies').remove();

    //勋章列表弹窗
    if (param.isDonatedOrshare == 'list_donated' || param.isDonatedOrshare == 'list_share') {
        // 列表勋章有两个，
        prependTpl(param)
    }
    // 分享 或者 捐款勋章弹窗
    else {
        getData(param);
    }
}

function prependTpl(data) {
    data._isDonatedOrshare = _isDonatedOrshare;
    data.alertLang = alertLang;

    $UI.prepend(mainTpl(data)); // 带close的模版
    $('.requirements').html(data.requirements.replace(/\n|\r\n/g, "<br/>"));

    // 决定是否显示
    if (data.popup || data._isDonatedOrshare == 'list_donated' || data._isDonatedOrshare == 'list_share') {
        $('.common-trophies').css('display', 'block');
    }

    bindClick(data); //remark获取后再绑定，要不然获取的为空
}

function getData(param) {
    let url = domainName.psuic + '/v1/ramadan/medal/' + param.isDonatedOrshare;
    let isLocal;
    if (isLocal) {
        url = '/mock/v1_ramadan_medal_donated.json';
    }
    let params = {
        url: url,
        type: 'GET'
    };

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, {
        success: function(res) {
            if (res.code == 0) {

                let _obj = JSON.parse(sessionStorage.getItem('ramadanTrophies')) || {};
                // 对比等级，等级更新了就弹窗
                if ((sessionStorage.getItem('ramadanTrophies') && _obj[param.isDonatedOrshare] && res.data.id > _obj[param.isDonatedOrshare]) ||
                    sessionStorage.getItem('ramadanTrophies') && !_obj[param.isDonatedOrshare] ||
                    !sessionStorage.getItem('ramadanTrophies')
                ) {
                    console.log(param.isDonatedOrshare, ' 等级改变了，可以展示勋章 !!!');
                    prependTpl(res.data); // 带close的模版

                    // 将等级存在本地，以便下次进行比对
                    _obj[param.isDonatedOrshare] = res.data.id;
                    sessionStorage.setItem('ramadanTrophies', JSON.stringify(_obj))
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        unauthorizeTodo: function(param) {

            console.log('unauthorizeTodo');

        },
        error: function(res) {
            utils.alertMessage(res.msg)
        }
    }, 'unauthorizeTodo')
}

/*
监听分享成功回调,写在各个业务页面内，因为成功回调各个业务线处理不一样，
*/
$UI.on('ramadanShareSuccess', function(e, target_type) {
    console.log('===ramadan Share Success===');

    shareActionCount(target_type);
});

/*****
target_type	Y	int	分享目标
item_id	Y	string	项目ID
item_type y int 产品类型 0 互助 /1 project /2 专题  /3 代表勋章
scene	N	int	分享场景	1:表示捐赠/付款前的分享 2:表示捐赠/付款成功后的分享 参数不传 默认为1
statistics_link	N	string	分享链接
terminal Y	string	终端类型
client_id	Y	string	客户端ID
 */
function shareActionCount(target_type) {
    let url = domainName.share + '/v1/share_action_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify({
            target_type: target_type, //分享目标
            item_id: '999999999', //项目ID
            item_type: 3, // 产品类型
            // scene: 1, //
            statistics_link: statistics_link,
            terminal: 'H5', //N	string	终端
            client_id: $.cookie('client_id') || '', //Y	string	客户端ID	长度不能超过45位
        }),
    }, {
        success: function(res) {
            //分享成功后
            if (res.code == 0) {
                console.log('ramadanTrophies Share success');
            } else {
                utils.alertMessage(res.msg);
            }
        },
        error: utils.handleFail
    })
}


function bindClick(data) {
    // 调起分享弹窗组件
    // 点击事件绑定注意解绑再绑定（ 因为每调一次勋章组件， 又会重新绑定一次）
    $('.common-trophies .share').off('click').on('click', function() {
        $('.common-trophies').css('display', 'none');

        let paramObj = {
            item_id: '999999999', //'999999999'代表勋章类型
            item_type: 3, //3 代表勋章 / 1 代表大病详情类型
            // item_short_link: '',
            remark: data.title, //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'ramadan-share-wrapper', // 分享弹窗名
            shareCallBackName: 'ramadanShareSuccess', //大病详情分享
            fromWhichPage: data.fromWhichPage || '' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // // 分享组件
            share.init(paramObj);
        }
    })
}

// go-to-campaign-list
$('body').on('click', '.common-trophies .btn.go-to-campaign-list', function() {
    location.href = '/campaignList.html?composite=1';
});

// close
$('body').on('click', '.common-trophies .close', function() {
    $('.common-trophies').css('display', 'none');
});

export default obj;