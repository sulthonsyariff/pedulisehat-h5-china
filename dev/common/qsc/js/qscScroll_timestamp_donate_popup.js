function QscScrollTime() {

    let $el, $elDonatePopup, $elContainer;
    let onNeedLoad;
    let intId;
    let jumpTime = 1000;
    let loading = false;

    this.config = function(o) {
        $elDonatePopup = o.elDonatePopup; //弹窗
        $el = o.wrapper; //监听的dom滚动元素
        $elContainer = o.elContainer; //父元素容器
        onNeedLoad = o.onNeedLoad;
    };

    this.run = function() {
        intId = setInterval(function() {
            if (loading) {
                return;
            }

            if ($elDonatePopup.css("display") == "none")
                return;

            let elH = $el[0].scrollHeight; //元素总高
            let elScrollingTop = $($el[0]).scrollTop(); //元素滚动高
            let elContainerH = $elContainer.height(); //父元素容器高
            let bottomDis = elH - elScrollingTop - elContainerH;

            // console.log(elH, elScrollingTop, elContainerH)
            // console.log(bottomDis)

            if (bottomDis < 300) {
                clearInterval(intId);
                loading = false;
                onNeedLoad();
            }

        }, jumpTime)
    };

    this.stop = function() {
        clearInterval(intId);
    }

}



export default QscScrollTime;