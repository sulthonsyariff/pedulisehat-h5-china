import utils from 'utils'
import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'

import commonDonateSuccessTpl from 'commonDonateSuccessTpl'
import commonDonateSuccessCalendarTpl from 'commonDonateSuccessCalendarTpl'
// import common_donate_success_listItem from '../tpl/common_donate_success_listItem.juicer'
import common_donate_success_toast from '../tpl/common_donate_success_toast.juicer'
import "../less/common_donate_success.less";
import share from 'share'

import shareAccessCount from 'shareAccessCount' //分享数据上报
import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'
import campaignListComponents from 'campaignListComponents'

/* translation */
import donateSuccess from 'donateSuccess'
let lang = qscLang.init(donateSuccess);
import common from 'common'
import qscLang from 'qscLang'
let commonLang = qscLang.init(common);
/* translation */

let isLocal;
let obj = {};
let $UI = $('body');
obj.UI = $UI;

// let shareUrl;
// let shareWords = [];
let tplName
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'] || '';
let order_id = reqObj['order_id'] || reqObj['bill_no'];

let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || ''; //专题

console.log("===sessionStorage.getItem('statistics_link')===", sessionStorage.getItem('statistics_link'));

if (sessionStorage.getItem('statistics_link')) {
    shareAccessCount.init(4, order_id); //分享数据上报
}

// obj.setShareInfo = function(rs) {
//     shareUrl = rs.data.url
//     shareWords = rs.data.forwarding_desc
// }


obj.init = function (res) {

    $('.finish-content').append(commonDonateSuccessTpl(res));
    clickHandle(res);
    getCalendar(res);
    toastCrtl(res)
    // toastAB(res);
};

function toastCrtl() {
    let url = domainName.project + '/v1/donated_success/popup?order_id=' + order_id;

    let param = {
        type: 'get',
        url: url,
    }
    ajaxProxy.ajax(param, {
        success: function (rs) {
            rs.lang = lang
            console.log('toastCtrl', rs)
            $UI.append(common_donate_success_toast(rs.data));
            toastClick(rs);
        },
        unauthorizeTodo: function () {
            let rs = {};
            rs.lang = lang
            $UI.append(common_donate_success_toast(rs.data));
            toastClick(rs);

        },
        error: function (rs) {
            utils.alertMessage(rs.msg)
        }
    }, 'unauthorizeTodo');
}

function toastClick(rs) {
    $('body').on('click', '.btn', function (e) {
       

        setTimeout(() => {
            location.href = rs.data.button_link
        }, 300);
    });

    $('body').on('click', '.close-icon', function(e) {
        $('.donation-toast').css('display', 'none')
        // rs.choose_sub_type = 'gtryClose'

        rs.isDonatedOrshare = 'donated'; // 'donated'
        rs.fromWhichPage = 'donateSuccess.html' //google analytics need distinguish the page name
        ramadanTrophiesAlertBox.init(rs);

    });
}

// function chooseRecord(rs) {
//     let url = domainName.base + '/v1/choose_record';
//     let data = {
//         template_name: rs.data.TemplateName,
//         choose_type: 1,
//         choose_sub_type: rs.choose_sub_type,
//     }
//     let param = {
//         type: 'post',
//         url: url,
//         data: JSON.stringify(data)
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {},
//         error: function (rs) {
//             // utils.alertMessage(rs.msg)
//         }
//     });
// }

// function abTest(tplName) {
//     let url = domainName.base + '/v1/ab/template_total/' + tplName;
//     let param = {
//         type: 'put',
//         url: url,
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {
//             console.log('abTest-success')
//         },
//         error: function (rs) {
//             utils.alertMessage(rs.msg)
//         }
//     });
// }

// function abFirstTest(tplName){
//     let url = domainName.base + '/v1/ab/template_total_random/' + tplName;

//     let param = {
//         type: 'put',
//         url: url,
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {
//             console.log('abFirstTest-success')
//         },
//         error: function (rs) {
//             utils.alertMessage(rs.msg)
//         }
//     });
// }


function getCalendar(res) {
    // get calendar
    let url = domainName.psuic + '/v1/donated_calendar';
    // if (1) {
    //     url = '/mock/v1_calendar.json';
    // }
    let param = {
        type: 'get',
        url: url,
    }
    ajaxProxy.ajax(param, {
        success: function (rs) {
            if (rs.code == 0 && rs.data && rs.data.now) {
                rs._data = res.data;
                rs.lang = lang;

                timeHandle(rs); //日历
            }
        },
        unauthorizeTodo: function (rs) {

        },
        error: function (rs) {
            utils.alertMessage(rs.msg)
        }
    }, 'unauthorizeTodo');
}

// 日历数据绑定
function timeHandle(rs) {

    let Nowdate = new Date(rs.data.now);
    let WeekFirstDay = Nowdate - ((Nowdate.getDay() || 7) - 1) * 86400000;
    let arr = [];
    // console.log('WeekFirstDay=', new Date(WeekFirstDay).getDate());

    // 定义一周的数组，每日数据绑定
    for (let i = 0; i < 7; i++) {
        arr.push({
            'date': new Date(WeekFirstDay + i * 86400000).getDate(),
            'day': new Date(WeekFirstDay + i * 86400000).getDay(),
            'today': (Nowdate.getDate() == new Date(WeekFirstDay + i * 86400000).getDate()) ? true : false,
            'donateCout': ''
        });
    }

    // 捐款次数绑定到每天中
    if (rs && rs.data && rs.data.calendar) {
        for (let i = 0; i < rs.data.calendar.length; i++) {
            // console.log('rs.data[i]=', i, rs.data.calendar[i])
            let _param = rs.data.calendar[i];

            for (let i = 0; i < arr.length; i++) {
                if (new Date(_param.created_at).getDate() == arr[i].date) {
                    arr[i].donateCout = _param.count
                }
            }
        }
    }

    rs.arr = arr;
    $('.calendar').append(commonDonateSuccessCalendarTpl(rs));
    $('.donation-word.noLogin').css('display', 'none');

}

function clickHandle(res) {
    $('body').on('click', '.review-this', function (e) {
        if (res.group_id) {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/special_projects?short_link=' + short_link : '/group/' + short_link
        } else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + res.project_id : '/' + short_link
        }
    });
    $('body').on('click', '.review-all', function (e) {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html'
    });

    // share
    $('body').on('click', '.share', function (e) {
        let paramObj = {
            item_id: project_id || group_id,
            item_type: project_id ? 1 : 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            item_short_link: short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'commonDonateSuccess.html' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // 分享组件
            share.init(paramObj);
        }
    });

    // open andriod share
    // $('body').on('click', '.andriod-share', function(e) {
    //     if (utils.browserVersion.android) {
    //         $UI.trigger('android-share2');
    //     }
    // });

    // // gtry entrance
    // $('body').on('click', '.gtry-entrance', function (e) {
    //     if (utils.judgeDomain() == 'qa') {
    //         location.href = 'https://gtry-qa.pedulisehat.id'

    //     } else if (utils.judgeDomain() == 'pre') {
    //         location.href = 'https://gtry-pre.pedulisehat.id'

    //     } else {
    //         location.href = 'https://gtry.pedulisehat.id'

    //     }

    //     // location.href = 'https://gtry-' + utils.judgeDomain() + '.pedulisehat.id'
    // });
}

obj.insertData = function(rs) {
    if (rs.data.length != 0) {
        rs.fromWhichPage = 'commonDonateSuccess.html'
        campaignListComponents.init(rs);
    }
}

// function raiseDay(created_at) {
//     let t1 = new Date(created_at);
//     let t2 = new Date();
//     let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
//     return raiseDay;
// }

export default obj;