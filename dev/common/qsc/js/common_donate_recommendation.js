import mainTpl from '../tpl/common_donate_recommendation.juicer'
import fastclick from 'fastclick'
import '../less/common_donate_recommendation.less'

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];

let reqSelect = location.href

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {
    console.log('联合支付：', res.getProjInfoData.data.category_id);

    res.commonLang = commonLang;

    $('.recommendationList').append(mainTpl(res));

    fastclick.attach(document.body);
    obj.event(res);
};

obj.event = function(res) {
    // console.log('res',res)

    // recommendation
    $UI.on('click', '.recommendation', function() {
        $(this).toggleClass('add');
        $UI.trigger('recommendationAdd', $(this));
    })
}

export default obj;