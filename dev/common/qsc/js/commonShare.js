import mainTpl from '../tpl/common_share.juicer'
import utils from 'utils'
import domainName from 'domainName'; //port domain name

// var ClipboardJS = require('clipboard');
// 复制粘贴
import ClipboardJS from 'clipboard'
new ClipboardJS('.share-item.link');

/* translation */
import common from 'common'
import qscLang from 'qscLang'
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

/**
 *
 $("[property='og:url']").attr('content', url);
 $("[property='og:title']").attr('content', title);
 $("[property='og:description']").attr('content', description);
 $("[property='og:image']").attr('content', image);

 （Facebook / Twitter / Whatsapp分享， 仅能抓取JS加载前meta标签内的内容，
 *故后端做‘ 分享链接’ 的模版渲染， 让Facebook / Twitter / Whatsapp爬虫抓取后端‘ 分享链接’ 中的meta详情内容）
 */
obj.init = function(rs) {
    // let shareUrl = $("[property='og:url']").attr('content');

    // 获取meta标签中的分享内容
    let meta_shareUrl = $("[property='og:url']").attr('content');
    let meta_shareTitle = $("[property='og:title']").attr('content');
    let meta_shareDescription = $("[property='og:description']").attr('content');
    let meta_shareImage = $("[property='og:image']").attr('content');

    // 如果有特定的分享链接，则分享特定的链接地址
    let shareUrl = meta_shareUrl;
    let shareTitle = meta_shareTitle;
    let shareDescription = meta_shareDescription;
    let shareImage = meta_shareImage;

    rs.domainName = domainName;
    rs.commonLang = commonLang;

    $UI.prepend(mainTpl(rs));

    // link
    $('.share-item.link').attr('data-clipboard-text', shareUrl);

    /*
    TargetTypefacebook = 1 // facebook
    TargetTypeWhatsApp = 2 // whatsapp
    TargetTypeTwitter = 3 // twitter
    TargetTypeLink = 4 // link
    TargetTypeLine = 5 // line
    TargetTypeInstagram = 6 // instagram
    TargetTypeYoutube = 7 // youtube
    */
    //facebook
    $('body').on('click', '.common-share .facebook', function(e) {
        e.preventDefault();
        utils.fbShare(shareUrl, callback);
        //分享计数成功后执行
        function callback() {
            // 分享后数字改变
            $UI.trigger('projectShare', 1);
            // console.log('分享后数字改变');
        }
        // 分享后关闭多渠道分享弹窗
        $('.common-share').css('display', 'none');

        return false;
    });

    //Link
    // $('body').on('click', '.common-share .link', function() {
    //     utils.copyLink('url-link', callback);
    //     //分享计数成功后执行
    //     function callback() {
    //         // 分享后数字改变
    //         $UI.trigger('projectShare', 4);
    //         // console.log('分享后数字改变');
    //     }

    //     // 分享后关闭多渠道分享弹窗
    //     $('.common-share').css('display', 'none');

    //     return false;
    // });

    //twitter
    $('body').on('click', '.common-share .twitter', function() {
        utils.twitterShare(shareTitle, shareUrl, callback);

        //分享计数成功后执行
        function callback() {
            // 分享后数字改变
            $UI.trigger('projectShare', 3);
            // console.log('分享后数字改变');
        }

        // 分享后关闭多渠道分享弹窗
        $('.common-share').css('display', 'none');
        return false;
    });

    //whatsapp
    $('body').on('click', '.common-share .wsapp', function() {
        utils.whatsappShare(shareTitle, shareUrl, callback);

        //分享计数成功后执行
        function callback() {
            // 分享后数字改变
            $UI.trigger('projectShare', 2);
            // console.log('分享后数字改变');
        }

        // 分享后关闭多渠道分享弹窗
        $('.common-share').css('display', 'none');
        return false;
    });

    // Line
    $('body').on('click', '.common-share .line', function() {
        utils.lineShare(shareTitle, shareUrl, callback);

        //分享计数成功后执行
        function callback() {
            // 分享后数字改变
            $UI.trigger('projectShare', 5);
            // console.log('分享后数字改变');
        }

        // 分享后关闭多渠道分享弹窗
        $('.common-share').css('display', 'none');

        return false;
    });
};

export default obj;