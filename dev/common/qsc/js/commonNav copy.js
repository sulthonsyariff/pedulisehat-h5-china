import mainTpl from '../tpl/common_nav.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import store from 'store'
import appDownload from 'appDownload'

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */
let obj = {};
let $UI = $('body');
obj.UI = $UI;


/* storage key */
// let APP_DOWNLOAD_KEY = 'app_download';
// let app_download = sessionStorage.getItem(APP_DOWNLOAD_KEY) ? sessionStorage.getItem(APP_DOWNLOAD_KEY) : {};

/**
 * @param {控制按钮文字} res.JumpName
 *  @param {控制返回链接} res.commonNavGoToWhere
 */
obj.init = function(res) {
    console.log('commonNav:', res)
    res.domainName = domainName;
    res.commonLang = commonLang;

    if (!utils.browserVersion.android) {
        $UI.prepend(mainTpl(res));
    }

    appDownload.init($('.app-download-wrapper'));

    fastclick.attach(document.body);
    obj.event(res);

    // // 设置avatar 头像
    // if (res && res.data && res.data.avatar) {
    //     if (res.data.avatar.indexOf('http') == 0) {
    //         $('.avatar-img').attr('src', utils.imageChoose(res.data.avatar));
    //     } else {
    //         $('.avatar-img').attr('src', domainName.static + '/img/avatar/' + res.data.avatar);
    //     }
    // }

    // // name overflow
    // $('.avatar-name .name').css('max-width', ($('.self-center-bg').width() - 85) * 0.95);

    // bottom-slide-wrapper
    if ($('.bottom-slide-wrapper .list').length == 3) {
        $('.bottom-slide-wrapper .list').css('width', '32%')
    }

};

obj.event = function(res) {
    // goBack
    $('body').on('click', '#goBack', function(e) {
        sessionStorage.setItem('OldHome', 6)

        if (res.commonNavGoToWhere) {
            location.href = res.commonNavGoToWhere;
        } else {
            window.history.go(-1);
        }
    });

    // search
    $('body').on('click', '.right', function(e) {
        // $UI.trigger('initiate');
        $('.search-wrap').css('display', 'block')
    });


    // // 个人中心：list
    // $('body').on('click', '.list', function(e) {

    //     $('.list').removeClass('choosed');
    //     $(this).addClass('choosed');

    //     // 页面跳转:页面中添加a标签跳转太快，高亮不显示
    //     setTimeout(() => {
    //         if ($(this).hasClass('my-insurance')) {
    //             if (utils.judgeDomain() == 'qa') {
    //                 location.href = 'https://asuransi-qa.pedulisehat.id/myInsurance.html';
    //             } else if (utils.judgeDomain() == 'pre') {
    //                 location.href = 'https://asuransi-pre.pedulisehat.id/myInsurance.html';
    //             } else {
    //                 location.href = 'https://asuransi.pedulisehat.id/myInsurance.html';
    //             }
    //         }
    //         if ($(this).hasClass('my-gtry')) {
    //             if (utils.judgeDomain() == 'qa') {
    //                 location.href = 'https://gtry-qa.pedulisehat.id/membershipCard.html';
    //             } else if (utils.judgeDomain() == 'pre') {
    //                 location.href = 'https://gtry-pre.pedulisehat.id/membershipCard.html';
    //             } else {
    //                 location.href = 'https://gtry.pedulisehat.id/membershipCard.html';
    //             }
    //         }
    //         if ($(this).hasClass('my-campaigns')) {
    //             location.href = '/myCampaigns.html';
    //         } else if ($(this).hasClass('supported-campaigns')) {
    //             // location.href = '/supportedCampaigns.html';
    //             location.href = '/donationHistory.html';
    //         } else if ($(this).hasClass('campaigns-followed')) {
    //             location.href = '/followedCampaigns.html';
    //         } else if ($(this).hasClass('messages')) {
    //             location.href = '/MessagesCategory.html';
    //         } else if ($(this).hasClass('setting')) {
    //             location.href = '/setting.html';
    //         } else if ($(this).hasClass('help-center')) {
    //             location.href = '/faq.html';
    //         } else if ($(this).hasClass('about')) {
    //             location.href = '/aboutUs.html';
    //         } else if ($(this).hasClass('language')) {
    //             location.href = '/changeLanguage.html';
    //         }
    //     }, 400);
    // });

    // // show left slide:home nav
    // $('body').on('click', '.home .left', function(e) {
    //     $('.body-mask').css({
    //         'display': 'block'
    //     });
    //     $('.slide-wrapper').css({
    //         'left': '0',
    //     })
    // });

    // // hide left slide:home nav
    // $('body').on('click', '.body-mask', function(e) {
    //     $('.slide-wrapper').css({
    //         'left': '-80%',
    //     });
    //     $('.body-mask').css({
    //         'display': 'none'
    //     })
    // });

    // $('body').on('click', '.sign-out', function(e) {
    //     $('.sign-out').removeClass('choosed');
    //     // console.log('signout===', $(this))
    //     $('.sign-out').addClass('choosed');

    // });

    // $('body').on('click', '.sign-in', function(e) {
    //     $('.sign-in').removeClass('choosed');
    //     // console.log('signout===', $(this))
    //     $('.sign-in').addClass('choosed');

    // });

    // //点击头像进入我的账户页面
    // $('body').on('click', '.avatar', function(e) {
    //     location.href = '/myAccount.html'

    // });
    // signOut
    // $('body').on('click', '#signOut', function(e) {
    //     // sessionStorage.setItem('OldHome', 5)
    //     e.preventDefault();
    //     var dbObj = JSON.stringify({
    //         accessToken: '',
    //         expiresIn: 0,
    //         refreshToken: '',
    //         tokenType: '',
    //         uid: ''
    //     });

    //     var isLocal = location.href.indexOf("pedulisehat.id") == -1;
    //     var tempDM = 'pedulisehat.id';
    //     if (isLocal) {
    //         tempDM = location.host;
    //     }

    //     $.cookie('passport', dbObj, {
    //         expires: 365,
    //         path: '/',
    //         domain: tempDM
    //     });

    //     location.href = '/';
    // })

    // // 详情页才有的点击的事件：
    // if ($('.bottom-slide-wrapper').length) {
    //     // edit:detail nav
    //     $('body').on('click', '#edit', function(e) {
    //         $('#edit span').html(commonLang.lang22);
    //         $('#edit').attr('id', 'close');

    //         $('.bottom-slide-wrapper').css({
    //             'display': 'block',
    //             'top': '57px',
    //             'z-index': '2001'
    //         })
    //         $('.body-mask').css('display', 'block')
    //         $('.navbar').css('z-index', '2001')
    //     });

    //     // detail close
    //     $('body').on('click', '#close,.body-mask', function(e) {
    //         // console.log('detail');
    //         $('#close span').html('Edit');
    //         $('#close').attr('id', 'edit');

    //         $('.bottom-slide-wrapper').css({
    //             'display': 'none',
    //             'top': '-35px',
    //             'z-index': '0'
    //         })
    //         $('.body-mask').css('display', 'none')
    //         $('.navbar').css('z-index', '2000')
    //     });
    // }


    // //  APP download topbar
    // if (app_download != 'false') {
    //     if (utils.browserVersion.androidEnd && !utils.browserVersion.android) {
    //         $('.app-download').css({
    //             'display': 'block'
    //         })
    //     }
    //     $('body').on('click', '.appDownload-close', function(e) {
    //         sessionStorage.setItem(APP_DOWNLOAD_KEY, 'false')

    //         $('.app-download').css({
    //             'display': 'none'
    //         })
    //     });
    // }

    // // APP download
    // $('body').on('click', '.right-wrapper', function(e) {
    //     location.href = 'https://github.com/walkermanx/pedulisehat_apk/releases/latest/download/PeduliSehat.apk';
    //     // location.href = 'market://details?id=com.qschou.pedulisehat.android'
    //     // location.href = 'http://oss.pgyer.com/725258b0a95a2b608a6d26093382e403.apk?auth_key=1586419762-36df84a32b0888c72c5a2be156c993e5-0-e5399805304cd2d9602b8be9b7789765&response-content-disposition=attachment%3B+filename%3D%255BGooglePlay%255DprodOnlineRelease-1.2.2%255B238%255D-0404124221.apk'
    //     // location.href = 'http://oss.pgyer.com/725258b0a95a2b608a6d26093382e403.apk?auth_key=1589862778-9a48f8502e9dd584b39548a59205edb9-0-0cb1b74429e5ebf18ed122904d0759f6&response-content-disposition=attachment%3B+filename%3D%255BGooglePlay%255DprodOnlineRelease-1.2.2%255B238%255D-0404124221.apk'
    // });

    // $('#fc_frame').click(function(){
    //     console.log('fc_frame')
    //     sensors.track('ContactCS', {
    //         contact_type: 'freshchat',
    //     })
    // });

    // $('body').on('click', '#fc_frame', function (e) {
    //     console.log('fc_frame')
    //     sensors.track('ContactCS', {
    //         contact_type: 'freshchat',
    //     })
    // });

}

export default obj;