import store from 'store'
import 'jq_cookie' //ajax cookie

let obj = {};
let failRecord = 0;

let $UI = $('body');
let CACHED_KEY = 'switchLanguage';
// let platform;

obj.UI = $UI;

// 安卓协议：
obj.jumpToLogin = function(fromUrl, nexturl, flag) {
    let appLink = 'qsc://app.pedulisehat/go/login?req_code=600'; // 这种情况是不需要给H5反馈code，登录完就可以了
    let h5Link = '/login.html?qf_redirect=' + encodeURIComponent(fromUrl);

    if (flag == 'login_1') {
        h5Link = '/login.html?loginFrom=sijicorona&qf_redirect=' + encodeURIComponent(fromUrl);
    }

    setTimeout(function() {
        location.href = obj.browserVersion.android ? appLink :
            'https://' + obj.judgeDomain() + '.pedulisehat.id' + h5Link
    }, 30)
}

/****
 * login jump
 */
// obj.jumpToLogin = function(fromUrl, nexturl) {
//     if (fromUrl) {
//         setTimeout(function() {
//             location.href = 'https://' + obj.judgeDomain() +
//                 '.pedulisehat.id/login.html?qf_redirect=' +
//                 encodeURIComponent(fromUrl)
//         }, 30)
//     }
// }

// qa / pre /www
obj.judgeDomain = function() {
    let obj = {};
    let urlStr = location.host;

    if (urlStr.indexOf("qa.pedulisehat.id") != -1 || urlStr.indexOf("gtry-qa.pedulisehat.id") != -1 || urlStr.indexOf("asuransi-qa.pedulisehat.id") != -1) {
        return 'qa';

    } else if (urlStr.indexOf("pre.pedulisehat.id") != -1 || urlStr.indexOf("gtry-pre.pedulisehat.id") != -1 || urlStr.indexOf("asuransi-pre.pedulisehat.id") != -1) {
        return 'pre';

    } else if (urlStr.indexOf("www.pedulisehat.id") != -1 || urlStr.indexOf("pedulisehat.id") != -1 || urlStr.indexOf("gtry.pedulisehat.id") != -1 || urlStr.indexOf("asuransi.pedulisehat.id") != -1) {
        return 'www';
    }
}

//  "https://gtry.pedulisehat.id"
obj.gtryLocationOrigin = function() {
    return 'https://gtry' + ((obj.judgeDomain() != 'www') ?
        ('-' + obj.judgeDomain()) :
        ''
    ) + '.pedulisehat.id'
}

//  "https://asuransi.pedulisehat.id"
obj.asuransiLocationOrigin = function() {
    return 'https://asuransi' + ((obj.judgeDomain() != 'www') ?
        ('-' + obj.judgeDomain()) :
        ''
    ) + '.pedulisehat.id'
}

obj.getBrowserDevice = function() {
    let result;
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      result = "Android-H5";
    } else {
      result = "H5";
    }
    return result;
}
  
obj.browserVersion = function() {
    let u = navigator.userAgent,
        app = navigator.appVersion;
    return {
        trident: u.indexOf('Trident') > -1, //IE内核
        presto: u.indexOf('Presto') > -1, //opera内核
        webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
        gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
        mobile: !!u.match(/AppleWebKit.*Mobile.*/) || u.indexOf('moslem') > -1, //是否为移动终端 (Moslem APP因为要解决app内Google登录的原因，除去了AppleWebKit.*Mobile这些标记)
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
        androidEnd: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端

        android: u.indexOf('language') > -1, //peduli APP应用
        moslemApp: u.indexOf('moslem') > -1, //Moslem APP应用
        androids: (u.indexOf('language') > -1) || (u.indexOf('moslem') > -1), // peduli APP应用 + Moslem APP应用

        iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
        iPad: u.indexOf('iPad') > -1, //是否iPad
        webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
        weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
        qq: u.match(/\sQQ/i) == " qq", //是否QQ
        UC: u.indexOf('UCBrowser') > -1, //UC浏览器
        isLine: u.indexOf('Line') > -1, //Line中
    };
}()

// 剩余时间
obj.timeLeft = function(closed_at) {
    let t1 = new Date(closed_at);
    let t2 = new Date();
    let subtract = t1.getTime() - t2.getTime();
    
    return Math.floor(subtract / 86400000);
};


obj.jsDateDiff = function(publishTime) {
    let timeNow = parseInt(new Date().getTime() / 1000);
    let diff, d_min, d_day, d_sec, d_mon, d_year, d_hour;
    diff = timeNow - publishTime;
    let tip = '';
    if (diff <= 0) {
        return '';
    } else {
        d_min = parseInt(diff / 60);
        d_hour = parseInt(diff / 3600);
        d_day = parseInt(diff / 86400);
        d_mon = parseInt(diff / 2592000);
        d_year = parseInt(diff / 31536000);
        if (diff < 60) {
            return tip + diff + '秒前';
        } else if (d_min < 60) {
            return tip + d_min + '分钟前';
        } else if (d_hour < 24) {
            return tip + d_hour + '小时前';
        } else if (d_day < 30) {
            return tip + d_day + '天前';
        } else if (d_mon < 12) {
            return tip + d_mon + '月前';
        } else {
            return tip + d_year + '年前';
        }
    }
};

obj.systemDateDiff = function(publishTime) {

    let lang = qscLang.systemLang;
    let timeNow = parseInt(new Date().getTime() / 1000);
    let diff, d_min, d_day, d_sec, d_mon, d_year, d_hour;
    diff = timeNow - publishTime;
    let tip = '';
    if (diff <= 0) {
        return '';
    } else {
        d_min = parseInt(diff / 60);
        d_hour = parseInt(diff / 3600);
        d_day = parseInt(diff / 86400);
        d_mon = parseInt(diff / 2592000);
        d_year = parseInt(diff / 31536000);
        if (diff < 60) {
            return lang.lang1.replace('{num}', tip + diff);
        } else if (d_min < 60) {
            return lang.lang2.replace('{num}', tip + d_min);
        } else if (d_hour < 24) {
            return lang.lang3.replace('{num}', tip + d_hour);
        } else if (d_day < 30) {
            return lang.lang4.replace('{num}', tip + d_day);
        } else if (d_mon < 12) {
            return lang.lang5.replace('{num}', tip + d_mon);
        } else {
            return lang.lang6.replace('{num}', tip + d_year);
        }
    }
};

/**
 * 根据 timestamp 返回本地时间字符串，格式如下：
 * 02/29/2000 23:59:59
 * @param {number} timestamp 以 UTC 为基准的时间戳，以秒为单位
 */
obj.dateFormat = function(timestamp) {

    let date = new Date(timestamp * 1000);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    let hour = date.getHours();
    let minute = date.getMinutes();
    let second = date.getSeconds();

    return [month, day, year].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':');

    function formatNumber(n) {
        n = n.toString();
        return n[1] ? n : '0' + n;
    }
};

// 10:57 23 Apr 2020
obj.msgTime = function(created_at) {
    let publishTime = new Date(created_at).getTime() / 1000;
    let timeNow = parseInt(new Date().getTime() / 1000);
    let dateNow = new Date().getDate();
    // console.log('dateNow',dateNow)

    let diff, d_hour;
    diff = timeNow - publishTime;

    let date = new Date(publishTime * 1000);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;

    if (month < 2) {
        month = "Jan";
    } else if (month < 3) {
        month = "Feb";
    } else if (month < 4) {
        month = "Mar";
    } else if (month < 5) {
        month = "Apr";
    } else if (month < 6) {
        month = "May";
    } else if (month < 7) {
        month = "Jun";
    } else if (month < 8) {
        month = "Jul";
    } else if (month < 9) {
        month = "Aug";
    } else if (month < 10) {
        month = "Sep";
    } else if (month < 11) {
        month = "Oct";
    } else if (month < 12) {
        month = "Nov";
    } else if (month < 13) {
        month = "Dec";
    }
    let day = date.getDate();
    // console.log('day',day)
    let hour = date.getHours();
    let minute = date.getMinutes();
    // let second = date.getSeconds();

    if (diff <= 0) {
        return '';
    } else {
        d_hour = parseInt(diff / 3600);
        if (d_hour < 24 && dateNow == day) {
            return [hour, minute].map(formatNumber).join(':');
        } else {
            return [hour, minute].map(formatNumber).join(':') +
                ' ' + [day, month].map(formatNumber).join(' ') +
                ' ' + [year].map(formatNumber);
        }
    }

    function formatNumber(n) {
        n = n.toString();
        return n[1] ? n : '0' + n;
    }

}

obj.ddmmyyyy = function(created_at) {
    let publishTime = new Date(created_at).getTime() / 1000;
    let timeNow = parseInt(new Date().getTime() / 1000);
    let dateNow = new Date().getDate();
    // console.log('dateNow',dateNow)

    let diff, d_hour;
    diff = timeNow - publishTime;

    let date = new Date(publishTime * 1000);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;

    if (month < 2) {
        month = "Jan";
    } else if (month < 3) {
        month = "Feb";
    } else if (month < 4) {
        month = "Mar";
    } else if (month < 5) {
        month = "Apr";
    } else if (month < 6) {
        month = "May";
    } else if (month < 7) {
        month = "Jun";
    } else if (month < 8) {
        month = "Jul";
    } else if (month < 9) {
        month = "Aug";
    } else if (month < 10) {
        month = "Sep";
    } else if (month < 11) {
        month = "Oct";
    } else if (month < 12) {
        month = "Nov";
    } else if (month < 13) {
        month = "Dec";
    }
    let day = date.getDate();
    // console.log('day',day)
    let hour = date.getHours();
    let minute = date.getMinutes();
    // let second = date.getSeconds();

    return [day, month].map(formatNumber).join(' ') + ' ' + [year].map(formatNumber);

    function formatNumber(n) {
        n = n.toString();
        return n[1] ? n : '0' + n;
    }
}

obj.loadingMessage = function(msg, display) {
    if (!msg) {
        msg = '数据加载中';
    }
    let dialog = '<div class="qsc-toast toast-loading" id="toast-loading" style="display: block;">' +
        '<div class="toast-backdrop"></div>' +
        '<div class="toast-dialog">' +
        '<div class="toast-content">' +
        '<span>' +
        '<div class="loading-icon">' +
        '<div class="loading-icon-leaf loading-icon-leaf_0"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_1"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_2"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_3"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_4"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_5"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_6"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_7"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_8"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_9"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_10"></div>' +
        '<div class="loading-icon-leaf loading-icon-leaf_11"></div>' +
        '</div>' +
        '<span class="loadingMsg">' + msg + '</span>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '</div>';

    if (display) {
        $("#toast-loading").remove();
        $('body').append(dialog);

    } else {
        $("#toast-loading").remove();
    }
}

obj.showLoading = function(msg) {
    if (!msg) {
        if (obj.getLanguage() == 'id') {
            msg = 'Memuat...';
        } else {
            msg = 'Loading...';
        }
    }
    let dialog = ['<div class="h5ui-toast h5ui-toast_loading" id="toast-loading" style="display:block">',
        '		<div class="h5ui-toast_backdrop"></div>',
        '		<div class="h5ui-toast_dialog">',
        '			<div class="h5ui-toast_content">',
        '				<span>',
        '					<div class="h5ui-toast_loading_icon">',
        '						<div class="loading-icon-leaf loading-icon-leaf_0"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_1"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_2"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_3"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_4"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_5"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_6"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_7"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_8"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_9"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_10"></div>',
        '						<div class="loading-icon-leaf loading-icon-leaf_11"></div>',
        '					</div>',
        '					<span class="loadingMsg">' + msg + '</span>',
        '				</span>',
        '			</div>',
        '		</div>',
        '	</div>',
        '	'
    ].join("");

    if ($("#toast-loading").length > 0) {
        $("#toast-loading").find('.loadingMsg').text(msg);
        $("#toast-loading").show();
    } else {
        $('body').append(dialog);
    }
}

obj.hideLoading = function() {
    if ($("#toast-loading").length > 0) {
        $("#toast-loading").hide();
    }
}

obj.is_weixin = function() {
    let ua = navigator.userAgent.toLowerCase();
    return ua.match(/MicroMessenger/i) == "micromessenger";
};


obj.alertMessage = function(msg, time) {
    if (!time) {
        time = 2000;
    }

    let dialog =
        '<div class="qsc-toast" id="toast-default" style="display:block;">' +
        '<div class="toast-backdrop"></div>' +
        '<div class="toast-dialog">' +
        '<div class="toast-content">' +
        '<span>' + msg + '</span>' +
        '</div>' +
        '</div>' +
        '</div>';
    if ($(".qsc-toast:visible").length) {
        $(".qsc-toast:visible").remove();
    }
    $('body').append(dialog);
    setTimeout(function() {
        $("#toast-default").remove();
    }, time);
};

obj.updateFancybox = function() {
    $('a.fancybox').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        prevEffect: 'none',
        nextEffect: 'none',
        padding: 0
    })
};

obj.getRequestParams = function() {
    let urlStr = location.search;
    let myRequest = new Object();
    if (urlStr.indexOf("?") != -1) {
        let tempStr = urlStr.substr(1);
        let strArr = tempStr.split("&");
        for (let i = 0; i < strArr.length; i++) {
            myRequest[strArr[i].split("=")[0]] = strArr[i].split("=")[1];
        }
    }
    return myRequest;
};

// 详情页短链
obj.isShortLink = function() {
    let path = location.pathname.split("/")[1];
    console.log('======path======', path)
    if (path.indexOf("detail.html") != -1) {
        return '';
    } else {
        return path;
    };
}

/**
 * （Facebook / Twitter / Whatsapp分享， 仅能抓取JS加载前meta标签内的内容，
 * 故后端做‘ 分享链接’ 的模版渲染， 让Facebook / Twitter / Whatsapp爬虫抓取后端‘ 分享链接’ 中的meta详情内容）
 * 分享网址到 facebook
 * 参考网址：
 * https://developers.facebook.com/docs/sharing/reference/share-dialog?locale=zh_CN
 */
obj.fbShare = function(url, callback) {
    console.log('url', url)

    try {
        FB.ui({
            method: 'share',
            href: url,
            hashtag: '#PeduliSehat'
                // mobile_iframe: true
        }, function(response) {
            if (response && !response.error_message) {
                console.log('Posting completed.');
                if (callback) {
                    callback();
                }
            } else {
                console.log('Error while posting.');
            }
        });
    } catch (e) {
        console.error("set up FB JavaScript SDK first");
    }
}

/**
 * facebook分享添加meta头部信息
 */
obj.changeFbHead = function(params) {
    $("[property='og:url']").attr('content', params.url);
    $("[property='og:title']").attr('content', params.title);
    $("[property='og:description']").attr('content', params.description);
    $("[property='og:image']").attr('content', params.image);
}

/**
 * （Facebook / Twitter / Whatsapp分享， 仅能抓取JS加载前meta标签内的内容， 故后端做‘ 分享链接’ 的模版渲染，
 * 让Facebook / Twitter / Whatsapp爬虫抓取后端‘ 分享链接’ 中的meta详情内容）
 * 分享网址到 twitter
 * 参考网址：
 * Tweet developer
 * https://dev.twitter.com
 */
obj.twitterShare = function(title, campaignURL, callback) {
    try {
        // Open the window.
        let tweetUrlBuilder = function(o) {
            return [
                // 'https://twitter.com/intent/tweet?text=' + o.title,
                'https://twitter.com/intent/tweet?text=' +
                encodeURIComponent('#PeduliSehat ' + o.title),
                '&url=', encodeURIComponent(o.url),
                '&via=', o.via
            ].join('');
        };

        // Create our custom url.
        let url = tweetUrlBuilder({
            title: title,
            url: '',
            via: 'PeduliSehat_ID' //艾特
        });


        if (obj.isPC()) {
            // platform = "PC"

            let myWindow = window.open(url, 'Tweet', 'width=800,height=800');
            if (window.focus) {
                myWindow.focus();
            }
        } else {
            // platform = "mobileH5Twitter"
            console.log('location.href =', url);
            location.href = url;
        }

        //跳转链接twitter
        if (callback) {
            callback();
        }
    } catch (e) {
        console.error("Twitter share error");
    }
};

/**
 * （Facebook / Twitter / Whatsapp分享， 仅能抓取JS加载前meta标签内的内容， 故后端做‘ 分享链接’ 的模版渲染，
 *  让Facebook / Twitter / Whatsapp爬虫抓取后端‘ 分享链接’ 中的meta详情内容）
 * 分享到whatsapp
 *
 * PC 分享时，调起PC分享界面；
 * mobile 分享时，用户没有安装应用时， 没有反应
 */
obj.whatsappShare = function(title, campaignURL, callback) {
    try {
        console.log('whatsappShare:', title, campaignURL);

        if (obj.isPC()) {
            // platform = "PC"
            let myWindow = window.open('https://wa.me/?text=' + encodeURIComponent(title),
                "WhatsApp", 'width=800,height=800');
            if (window.focus) {
                myWindow.focus();
            }
        } else {
            // platform = "mobileH5Whatsapp"

            let appSrc = 'whatsapp://send?text=' + encodeURIComponent(title);

            // 此方法在用户没有安装应用时，没有反应
            // moslem APP 没有自己的分享，需要加上跳转协议
            if (obj.browserVersion.androids) {
                // native://intent?url=<xxx>&errorInfo=<xxx>
                location.href = 'native://intent?url=' + encodeURIComponent(appSrc + '&errorInfo=Aplikasi belum diinstal');
            } else {
                location.href = appSrc;
            }


            // 此方法在安卓手机浏览器分享时，会报couldn't open link，
            // 此方法在用户没有安装应用时，会跳到应用商城，但产品方面觉得对我们并没有什么用处，用户应该对自己是否有安装应用自知
            // location.href = 'https://wa.me/?text=' + encodeURIComponent(
            //     title +
            //     ' ' +
            //     campaignURL);
        }


        //跳转链接twitter
        if (callback) {
            callback();
        }
    } catch (e) {
        console.error("What's app share error");
    }
}

/***
 * line分享
 *
 * https: //social-plugins.line.me/lineit/share?url= ？？？
 */
obj.lineShare = function(title, campaignURL, callback) {
    console.log('line', title)
    try {
        // line内部时，
        if (navigator.userAgent.indexOf('Line') > -1) {
            // platform = 'line';
            // platform = "insideLine"
            location.href = 'line://msg/text/' + encodeURIComponent(title)
        } else {
            // 安卓 moslem app 中
            if (obj.browserVersion.androids) {
                location.href = 'https://social-plugins.line.me/lineit/share?url=' + encodeURIComponent(campaignURL);

            } else {
                let myWindow = window.open('https://social-plugins.line.me/lineit/share?url=' + encodeURIComponent(campaignURL));
                if (window.focus) {
                    myWindow.focus();
                }
            }

        }

        //跳转链接twitter
        if (callback) {
            callback();
        }
    } catch (e) {
        console.error("line share error");
    }
}

obj.linkedinShare = function(title, campaignURL, callback) {
    try {
        console.log('linkedinShare:', title, campaignURL);

        let appSrc = 'https://www.linkedin.com/sharing/share-offsite/?url=' + encodeURIComponent(campaignURL);

        if (obj.isPC()) {
            // platform = "PC"
            let myWindow = window.open(appSrc
            ,"LinkedIn", 'width=800,height=800');
            if (window.focus) {
                myWindow.focus();
            }
        } else {
            // platform = "mobileH5Linkedin"

            if (obj.browserVersion.androids) {
                location.href = 'native://intent?url=' + encodeURIComponent(appSrc + '&errorInfo=Aplikasi belum diinstal');
            } else {
                location.href = appSrc;
            }


            // 此方法在安卓手机浏览器分享时，会报couldn't open link，
            // 此方法在用户没有安装应用时，会跳到应用商城，但产品方面觉得对我们并没有什么用处，用户应该对自己是否有安装应用自知
            // location.href = 'https://wa.me/?text=' + encodeURIComponent(
            //     title +
            //     ' ' +
            //     campaignURL);
        }


        //跳转链接linkedin
        if (callback) {
            callback();
        }
    } catch (e) {
        console.error("LinkedIn share error");
    }
}

obj.telegramShare = function(title, campaignURL, callback) {
    console.log('telegram', title)
    try {
        console.log('telegramShare:', title, campaignURL);

        // if (obj.isPC()) {
        //     // platform = "PC"
            let myWindow = window.open('https://t.me/share/url?url='+ encodeURIComponent(campaignURL) + '&text=' + title);
            if (window.focus) {
                myWindow.focus();
            }
        // } else {
        //     // platform = "mobileH5Telegram"
        //     let url = 'tg://msg_url?url='+ encodeURIComponent(campaignURL) + '&text=' + title;
        //     location.href = url;
        // }


        //跳转链接telegram
        if (callback) {
            callback();
        }
    } catch (e) {
        console.error("telegram share error");
    }
}

/**
 * copy Link
 * 该事件仅适用于<input type="text">和<textarea>文本框
 */
obj.copyLink = function(className, callback) {
    // let urlObj = document.getElementsByClassName(className)[0];
    // console.log('urlObj', urlObj)

    // urlObj.select();
    // console.log('urlObj-select', urlObj.select())
    // document.execCommand("Copy"); // 执行浏览器复制命令
    obj.alertMessage("Copied to clipboard");

    if (callback) {
        callback();
    }
}

// obj.browserVersion = function() {
//     let u = navigator.userAgent,
//         app = navigator.appVersion;
//     return {
//         trident: u.indexOf('Trident') > -1, //IE内核
//         presto: u.indexOf('Presto') > -1, //opera内核
//         webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
//         gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
//         mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
//         ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
//         androidEnd: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端

//         android: u.indexOf('language') > -1, //peduli APP应用
//         moslemApp: u.indexOf('moslem') > -1, //Moslem APP应用
//         androids: (u.indexOf('language') > -1) || (u.indexOf('moslem') > -1), // peduli APP应用 + Moslem APP应用

//         iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
//         iPad: u.indexOf('iPad') > -1, //是否iPad
//         webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
//         weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
//         qq: u.match(/\sQQ/i) == " qq", //是否QQ
//         UC: u.indexOf('UCBrowser') > -1, //UC浏览器
//         isLine: u.indexOf('Line') > -1, //Line中
//     };
// }()

//peduli APP：210 / Moslem APP ： 21 / H5 :10
obj.terminal = function() {
    return obj.browserVersion.moslemApp ? 210 :
        (obj.browserVersion.android ? 21 : 10)
}()

obj.isPC = function() {
    return !(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i.test(navigator.userAgent));
}

obj.handleFail = function(xhr) {
    if ($('#toast-loading').length) {
        $('#toast-loading').hide();
    }
    let error = 'Oops, something went wrong :(';

    if (xhr && xhr.msg) {
        error = xhr.msg;
    }
    obj.alertMessage(error)
}

obj.inputFourInOne = function(id) {
    $(id).val($(id).val().replace(/[^0-9]+/, '').replace(/^0+/, '').replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1   "));

    let item = document.querySelector(id);
    item.onkeyup = forceInt;
    item.onafterpaste = forceInt;

    function forceInt() {
        this.value = this.value.replace(/[^0-9]+/, '').replace(/^0+/, '').replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1   ");
    }
}

obj.change4in1 = function(param) {
    return param.replace(/[^0-9]+/, '').replace(/^0+/, '').replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, "$1-");
}

obj.getLanguage = function() {
    // 安卓客户端切换语言监听：印尼 in，英语 en
    if (navigator.userAgent.indexOf('language') > -1) {
        if (navigator.userAgent.indexOf('language=in') > -1) {
            return 'id'
        } else {
            return 'en'
        }
    } else {
        if ((store.get(CACHED_KEY) && store.get(CACHED_KEY).lang)) {
            return store.get(CACHED_KEY).lang

        } else if ($.cookie(CACHED_KEY)) {
            return $.cookie(CACHED_KEY)

        } else {
            return 'id'
        }
    }
}


obj.getAndroidArgsByNodeName = function(source, nodeName) {
    if (source == null || nodeName == null || source.indexOf(nodeName) <= -1) {
        return null
    }
    pattern = nodeName + "=([0-9_A-Z_a-b]|\\.)+"

    spice = source.match(pattern)

    if (spice == null) {
        return null
    }

    // console.log('==spice==', spice)

    arr = spice[0].split('=')
    return arr[1]
}

/****
 *
 * 图片处理器，H5浏览器不显示等等问题 https: //res.cloudinary.com/dqgl4hkkx/image/upload/v1578655008/avatar/wdnkau8slajwipc4pwhs.webp
 * 注意souceImg有两种格式!!!(一种是对象，一种是string)
 * souceImg: {
     "name": "",
     "image": "https://res.cloudinary.com/dqgl4hkkx/image/authenticated/s--akD0uPCI--/a_ignore,f_auto,q_auto/c_scale,f_auto,fl_relative,g_center,l_overlay_njgppi,w_0.7/v1589358218/ynqbk9ctya6nnm4kxnqv.webp",
     "thumb": "https://res.cloudinary.com/dqgl4hkkx/image/authenticated/s--USwJCgdP--/a_ignore,c_scale,f_auto,q_auto,w_300/v1589358218/ynqbk9ctya6nnm4kxnqv.webp"
 }
 或者“ https: //res.cloudinary.com/dqgl4hkkx/image/upload/v1589768934/avatar/gkwgpiaeqkvvhtyb7cpa.webp” 这种格式是安卓上传的头像地址，没有验证，

 */
obj.imageChoose = function(souceImg) {
    // endsWith是判断当前字符串是否以(".webp")作为结尾，但不幸的是，Javascript中没有自带这两个方法，需要的话只能自己写，如下,这样就可以使用.image.endsWith(".webp")
    if (typeof String.prototype.endsWith != 'function') {
        String.prototype.endsWith = function(str) {
            return this.slice(-str.length) == str;
        };
    }
    let imageEndWithWebp = (souceImg.image || souceImg).endsWith(".webp") //是否是webp图片
    let fauto = (souceImg.image || souceImg).indexOf("f_auto"); //添加上后可在移动端IOS内核正常展示webp图片
    let regResult = souceImg.image && (souceImg.image).match('/image/authenticated/s--[\\w\\-]{8}--/') //项目图片有验证，头像没有验证，这个适用于识别安卓项目图片地址

    if (regResult && regResult[0] && imageEndWithWebp && fauto == -1 || !souceImg.image && fauto == -1) {
        return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_faces:center,f_auto,ar_1:1/' + encodeURIComponent(souceImg.thumb || souceImg)
    } else {
        return souceImg.thumb || souceImg
    }
}

// 新项目列表
obj.imageChoose4_3 = function(souceImg) {
    // 原图为正方形缩略图，需要人脸裁剪
    return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_faces:center,f_auto,ar_4:3/' + encodeURIComponent(souceImg.thumb || souceImg)
}

// urgetnCampaigns image
obj.imageChoose2_1 = function(souceImg) {
    // 原图为正方形缩略图，需要人脸裁剪
    return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_faces:center,f_auto,ar_2:1/' + encodeURIComponent(souceImg.thumb || souceImg)
}

// detail banner img
obj.imageChoose16_9 = function(souceImg) {
    // 原图不知道什么比例，需要人脸裁剪
    return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_faces:center,f_auto,ar_16:9/' + encodeURIComponent(souceImg)
}


obj.addOverlay = function(o) {
    // 判断 旧项目拼接水印
    // let imageReg = '/image/authenticated/s--[\\w\\-]{8}--/';
    let regResult = o.image.match('/image/authenticated/s--[\\w\\-]{8}--/')
    let fauto = o.image.indexOf(",f_auto")

    // end with str function
    if (typeof String.prototype.endsWith != 'function') {
        String.prototype.endsWith = function(str) {
            return this.slice(-str.length) == str;
        };
    }
    let imageEndWithWebp = o.image.endsWith(".webp")

    if (regResult && regResult[0]) {
        if (imageEndWithWebp && fauto == -1) {
            return "https://res.cloudinary.com/dqgl4hkkx/image/fetch/f_auto/" + encodeURIComponent(o.image)
        }
        return o.image

    }
    return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/a_ignore,q_auto/c_scale,fl_relative,g_center,l_overlay_njgppi,w_0.7/' + encodeURIComponent(o.image)
}

/**
 * 原声JS获取cookie
 */
obj.getCookie = function(name) {
    //可以搜索RegExp和match进行学习
    let arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg)) {
        return JSON.parse(unescape(arr[2]));
    } else {
        return null;
    }
}


/*
 * 清空cookie
 */
obj.clearPassport = function() {
    let dbObj = JSON.stringify({
        accessToken: '',
        expiresIn: 0,
        refreshToken: '',
        tokenType: '',
        uid: ''
    });

    let isLocal = location.href.indexOf("pedulisehat.id") == -1;
    let tempDM = 'pedulisehat.id';
    if (isLocal) {
        tempDM = location.host;
    }

    $.cookie('passport', dbObj, {
        expires: 365,
        path: '/',
        domain: tempDM
    });
}


/****
 * 创造随机uuid密码
 */
obj.creatUuid = function() {
    let s = [];
    let hexDigits = "0123456789abcdef";
    for (let i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    let uuid = s.join("");
    return uuid;
}

/****
 * login.html 点击返回按钮XSS攻击问题
 *
 * https: //qa.pedulisehat.id/login.html?qf_redirect=https%3A%2F%2Fasuransi-qa.pedulisehat.id%2Fsijicorona
 * https: //qa.pedulisehat.id/login.html?qf_redirect=https%3A%2F%2Fqa.pedulisehat.id%2F20200408001
 * https: //qa.pedulisehat.id/login.html?qf_redirect=https%3A%2F%2Fqa.pedulisehat.id%2FmyAccount.html
 * https: //qa.pedulisehat.id/login.html?qf_redirect=%2F
 *
 *  '/' ||  'pedulisehat.id'
 */
obj.validXss = function(qf_redirect) {
    console.log(`qf_redirect=${qf_redirect},qf_redirect == '/', qf_redirect.indexOf('https') != -1 && qf_redirect.indexOf('pedulisehat.id') != -1`);

    if (qf_redirect == '/' ||
        (qf_redirect.indexOf('https') != -1 && qf_redirect.indexOf('pedulisehat.id') != -1)
    ) {
        return true
    } else {
        return false
    }
}

obj.generateDeviceId = function() {
    return Math.random().toString(36).substr(2) + Date.now().toString(36)
}

export default obj;