import mainTpl from '../tpl/bank-list.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name



/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];

var reqSelect = location.href
    /**
     * @param {返回按钮文字} res.JumpName
     */
obj.init = function(res) {

    res.domainName = domainName;
    res.commonLang = commonLang;

    $UI.prepend(mainTpl(res));

    fastclick.attach(document.body);
    obj.event(res);

};

obj.event = function(res) {
    // console.log('res',res)
    for (let i = 0; i < $('.list-items').length; i++) {
        $(".list-items").each(function() {
            // if($(this).attr('data-value') == '302'){
            //     // console.log('this',$(this))
            //     $(this).css('display', 'none')
            // }
            if ($(this).text() == res.data.bank_agency) {
                // console.log('$(this)')
                $(this).addClass('choosed-bank')
            }
        });
        
    }

    $('.close').on('click', function(rs) {
        $('.alert-bank').css('display', 'none')
    })

    $('body').on('click', '.list-items', function(e) {

        $('.list-items').removeClass('choosed-bank');
        $(this).addClass('choosed-bank');

        setTimeout(() => {
            $('.alert-bank').css('display', 'none')
        }, 100)

        $('.bank-name').css('color','#333').attr('data-bank_id',$(this).data('value')).addClass('selected').html($(this).html())
    })
}

export default obj;