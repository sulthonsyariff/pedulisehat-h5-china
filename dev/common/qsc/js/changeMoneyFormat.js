import {
    type
} from "os";

/*
 * 输入金额三位一个点
 */
let [obj, $UI] = [{}, $('body')];

obj.init = function ($input) {
    //输入框三位一个小数点
    $UI.on('input', $input, function (event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });

    $UI.on('blur', $input, function (event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });

    $UI.on('keyup', $input, function (event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });

    $UI.on('keydown', $input, function (event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });
}

obj.moneyFormat = function(price) {
    price = price + '';
    if (!price)
        return '';
    if (price === "0")
        return "0";
    if (price.length < 4)
        return price;
    var begin = price.length % 3;

    begin = begin === 0 ? 3 : begin;


    return price.substring(0, begin) + price.substring(begin).replace(/(\d{3,3})/g, ".$1");
}

obj.newMoneyFormat = function (moeny) {

    price = moeny.toString().split('.')[0]

    fraction = moeny.toString().split('.')[1]
    // console.log('----', price, fraction)
    priceCal();

    function priceCal(){
        price = price + '';
        if (!price) {
            return '';
        } else if (price === "0") {
            return "0";
        } else if (price.length < 4) {
            return price;
        }
    }
    // price = Number(price)
    // if(){

    // }
    var begin = price.length % 3;
    begin = begin === 0 ? 3 : begin;
    var integralPart = price.substring(0, begin) + price.substring(begin).replace(/(\d{3,3})/g, ".$1")
    console.log('integralPart', integralPart)


    if (fraction) {
        fractionCal();

        // var end = fraction.length % 3;
        // end = end === 0 ? 3 : end;
        // var fractionalPart = fraction.substring(0, end) + fraction.substring(end).replace(/(\d{3,3})/g, ".$1")
        var fractionalPart = fraction
        console.log('fractionalPart', fractionalPart)
    }

    function fractionCal() {
        fraction = fraction + '';
        if(fraction.length == 1){
            fraction = fraction + '0'
        }
        // if (!fraction) {
        //     return '';
        // } else if (fraction === "0") {
        //     return "0";
        // } else if (fraction.length < 4) {
        //     return fraction;
        // }
    }
    // if(fractionalPart && fractionalPart.length == 1){
    //     fractionalPart == fractionalPart + '0'
    // }

    return fractionalPart ? integralPart + ',' + fractionalPart : integralPart;
}

export default obj;