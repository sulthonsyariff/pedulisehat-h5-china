import mainTpl from '../tpl/search.juicer'
import store from 'store'


/* translation */
import searchResults from 'searchResults'
import qscLang from 'qscLang'
let lang = qscLang.init(searchResults);

/* translation */


let obj = {}
let $UI = $('body');
obj.UI = $UI

obj.init = function (res) {
    res.lang = lang
    console.log('search', res)
    $UI.prepend(mainTpl(res))

    clickHandle(res);


    $('.search-bar').bind('input propertychange', function () {
        if ($('.search-bar').val()) {
            $('.search-bar-delete').css('display', 'block');
        } else {
            $('.search-bar-delete').css('display', 'none');
        }
    });

    $('.search-bar').on('keypress', function (event) {

        if (event.keyCode == 13) {
            // sensors.track('SearchButtonClick',{
            //     search_word: $('.search-bar').val()
            // },location.href = '/searchResaults.html?search=' + $('.search-bar').val() + '&from=' + res.from)

            // location.href = '/searchResaults.html?search=' + $('.search-bar').val() + '&from=' + res.from
            store.set('search', $('.search-bar').val())
        }
    });
    if (res.searchHistory) {
        for (let i = 0; i < res.searchHistory.length; i++) {
            $('body').on('click', '.search-history-word_' + i, function () {
                store.set('search', res.searchHistory[i].search_key)
            })
        }
    }

    for (let i = 0; i < res.data.length; i++) {
        $('body').on('click', '.search-popular-word_' + i, function () {
            store.set('search', res.data[i].word)
        })
    }
}


function clickHandle(res) {
    $('body').on('click', '.cancel-bth', function (e) {
        $('.search-wrap').css('display', 'none')
    });
    $('body').on('click', '.empty-icon', function (e) {
        $('.search-history-wrap').css('display', 'none')
        $UI.trigger('deleteSearchHistory')

    })
    $('body').on('click', '.search-bar-delete', function () {
        $('.search-input').val('');
        $('.search-bar').val('');
        $('.search-bar-delete').css('display', 'none')
        store.remove('search');
    })
    // $('body').on('click', '.empty-icon', function () {
    //     // store.remove('search');
    //     $UI.trigger('deleteSearchHistory')
    // })
}

export default obj;