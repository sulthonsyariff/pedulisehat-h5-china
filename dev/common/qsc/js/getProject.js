import utils from 'utils'
import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let isLocal;
let obj = {};
let $UI = $('body');
let short_link;
let project_id;
obj.UI = $UI;

export default function(o) {
    let url = domainName.project + '/v1/project_unique?short_link=' + o.short_link + '&project_id=' + o.project_id;
    if (isLocal) {
        url = '/mock/v1_project_unique.json.json';
    }
    let params = {
        url: url,
        type: 'GET'
    };

    ajaxProxy.ajax(params, {
        success: function(res) {
            if (res.code == 0) {
                callback && callback(res.data);
            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: function(res) {
            utils.alertMessage(res.msg)
        }
    });

    function callback(o) {
        return {
            project_id: o.project_id,
            short_link: o.short_link
        }
    }
}