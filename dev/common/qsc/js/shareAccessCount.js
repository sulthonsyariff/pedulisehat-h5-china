/*****
 *
 ** 需统计

 1. 分享链接点击 PV, UV
 详情页：添加数据上报接口(/v1/share_access_count) source_type = 0 event_id = ‘’

  2. 点击分享链接下单、
 捐赠页：下单接口添加字段： statistics_link = ’XXXXX‘ client_id

 支付成功的人（ 用户 id 和游客设备 id） 和人数、 支付成功的金额
 支付成功页：添加数据上报接口(/v1/share_access_count) source_type = 4 event_id = ‘‘order_idXXXXX’

 3. 点击分享链接去注册的用户 id 和用户数
手机号登陆/注册页：登陆/注册接口(/v2/login/bymobile) 添加 statistics_link = ’XXXXX‘

 4. 点击分享链接后进行分享的用户 id 和用户数
所有分享页：分享成功回调 调接口(/v1/share_action_count) 添加： statistics_link = ’XXXXX‘
 *
 */

import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName'; //port domain name
import utils from 'utils'

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;
let reqObj = utils.getRequestParams();
let statistics_link = reqObj['statistics_link'] || sessionStorage.getItem('statistics_link') || '';
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let client_id = $.cookie('client_id') || getClient_id();

// 下单/注册 的时候会用到，
sessionStorage.setItem('statistics_link', statistics_link);

/****
    *
    *
  source_type:
    0 PV
    1 注册 user_id
    2 众筹捐款 order_id
    3 加入互助
    4 众筹捐款并且支付成功 order_id
    *
    */
obj.init = function(source_type, event_id) {
    shareAccessCount({
        param: {
            statistics_link: statistics_link,
            visitor_id: user_id, //n	int	访问用户ID
            terminal: 'H5', //N	string	终端
            client_id: client_id, //Y	string	客户端ID	长度不能超过45位

            source_type: parseInt(source_type), //	y	int	来源类型
            event_id: event_id || '' //n	sting	事件ID	详情点击链接来源类型
        },
        success: function(rs) {
            if (rs.code == 0) {
                console.log('=====submit share count====')
            } else {
                utils.alertMessage(rs.msg)
            }
        },
    })
}

//获取设备ID
function getClient_id() {
    let clientId = utils.creatUuid();
    $.cookie('client_id', clientId, {
        expires: 365,
        path: '/',
        domain: 'pedulisehat.id'
    });

    return clientId;
}

//分享数据上报
function shareAccessCount(o) {
    let url = domainName.share + '/v1/share_access_count';
    ajaxProxy.ajax({
        type: 'post',
        url: url,
        data: JSON.stringify(o.param),
    }, o)
}

export default obj;