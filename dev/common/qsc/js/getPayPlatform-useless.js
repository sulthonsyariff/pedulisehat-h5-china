/*****
 * *
 *
 * 可以使用这个公共的方法， 避免新增一个渠道都去更改
 *
  faspay:   pay_platform = 1, bank_code = **
  gopay:    pay_platform = 2, bank_code = 1 *
  sinamas:  pay_platform = 3, bank_code = 2 *
  OVO:      pay_platform = 4, bank_code = 4 *
  dana:     pay_platform = 5, bank_code = 5 *
  ShopeePay pay_platform = 6, bank_code = 6 / 7(APP)
 */
let pay_platform;

export default function(bank_code) {
    // gopay
    if (bank_code == '1') {
        pay_platform = 2;
    }
    // sinamas
    else if (bank_code == '2') {
        pay_platform = 3;
    }
    // ovo
    else if (bank_code == '4') {
        pay_platform = 4;
    }
    // dana
    else if (bank_code == '5') {
        pay_platform = 5;
    }
    // shopee
    else if (bank_code == '6' || bank_code == '7') {
        pay_platform = 6;
    }
    // faspay
    else {
        pay_platform = 1;
    }

    return pay_platform;
}