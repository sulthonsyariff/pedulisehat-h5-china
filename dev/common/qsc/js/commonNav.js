import mainTpl from '../tpl/common_nav.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import store from 'store'
import appDownload from 'appDownload'

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */
let obj = {};
let $UI = $('body');
obj.UI = $UI;


/* storage key */
// let APP_DOWNLOAD_KEY = 'app_download';
// let app_download = sessionStorage.getItem(APP_DOWNLOAD_KEY) ? sessionStorage.getItem(APP_DOWNLOAD_KEY) : {};

if (utils.browserVersion.androids) {
    $('body').addClass('inAndroid');
}

/**
 * @param {控制按钮文字} res.JumpName
 *  @param {控制返回链接} res.commonNavGoToWhere
 */
obj.init = function(res) {
    console.log('commonNav:', res)
    res.domainName = domainName;
    res.commonLang = commonLang;

    if (!utils.browserVersion.androids) {
        $UI.prepend(mainTpl(res));
        appDownload.init($('.app-download-wrapper')); //不在android内才加载

        fastclick.attach(document.body);
        obj.event(res);
    }
};

obj.event = function(res) {
    // goBack
    $('body').on('click', '#goBack', function(e) {
        sessionStorage.setItem('OldHome', 6)

        if (res.commonNavGoToWhere == '#') {
            // location.href = res.commonNavGoToWhere;
        } else if (res.commonNavGoToWhere) {
            location.href = res.commonNavGoToWhere;
        } else {
            window.history.go(-1);
        }
    });

    // search
    $('body').on('click', '.right', function(e) {
        // $UI.trigger('initiate');
        $('.search-wrap').css('display', 'block')
    });
}

export default obj;