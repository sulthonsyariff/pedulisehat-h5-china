import mainTpl from "../tpl/common_limit_time.juicer";
import utils from "utils";
import domainName from "domainName"; //port domain name
import "../less/common_limit_time.less";

/* translation */
import common from "common";
import qscLang from "qscLang";
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $("body");
obj.UI = $UI;

obj.init = function(rs) {
  console.log("===commonTimeLimit===", rs);

  rs.lang = commonLang;
  $UI.prepend(mainTpl(rs));

  // cancel
  $("body").on("click", ".cancel", function() {
    $(".common_limit_time").css({
      display: "none"
    });

    $UI.trigger("choose_cancel");
  });

  // choose
  $("body").on("click", ".border p", function() {
    $(".common_limit_time").css({
      display: "none"
    });

    // 修改页面显示天数
    $(".select-time-limit .num").html(
      $(this)
        .find(".num")
        .html()
    );

    $UI.trigger(
      "choose_time",
      $(this)
        .find(".num")
        .html()
    );
  });
};

export default obj;
