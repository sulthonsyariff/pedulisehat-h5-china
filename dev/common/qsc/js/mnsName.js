/**
 * 不同环境不同域名
 * QA: -qa.pedulisehat.id
 * pre: -pre.pedulisehat.id
 * www: .pedulisehat.id
 */
let obj = {};

var urlStr = location.host;

if (urlStr.indexOf("qa.pedulisehat.id") != -1) {

    // QA
    obj.webSocket = 'wss://mns-qa.pedulisehat.id';
    obj.mnsApi = '//mns-qa.pedulisehat.id';
    
} else if (urlStr.indexOf("pre.pedulisehat.id") != -1) {

    // 预生产
    obj.webSocket = 'wss://mns-pre.pedulisehat.id';
    obj.mnsApi = '//mns-pre.pedulisehat.id';
    

} else if (urlStr.indexOf("www.pedulisehat.id") != -1 || urlStr.indexOf("pedulisehat.id") != -1) {

    // 线上
    obj.webSocket = 'wss://mns.pedulisehat.id';
    obj.mnsApi = '//mns.pedulisehat.id';
    
}

export default obj;