import utils from 'utils'
import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'

import commonDonateFailureGopayTpl from 'commonDonateFailureGopayTpl'
import commonDonateSuccessCalendarTpl from 'commonDonateSuccessCalendarTpl'
// import common_donate_success_listItem from '../tpl/common_donate_success_listItem.juicer'
import common_donate_success_toast from '../tpl/common_donate_success_toast.juicer'
import "../less/common_donate_failure_gopay.less";
import share from 'share'

import shareAccessCount from 'shareAccessCount' //分享数据上报
import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'
import campaignListComponents from 'campaignListComponents'

/* translation */
import donateSuccess from 'donateSuccess'
let lang = qscLang.init(donateSuccess);
import common from 'common'
import qscLang from 'qscLang'
let commonLang = qscLang.init(common);
/* translation */

let isLocal;
let obj = {};
let $UI = $('body');
obj.UI = $UI;

// let shareUrl;
// let shareWords = [];
let tplName
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'] || '';
let order_id = reqObj['order_id'] || reqObj['bill_no'];

let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || ''; //专题

console.log("===sessionStorage.getItem('statistics_link')===", sessionStorage.getItem('statistics_link'));

if (sessionStorage.getItem('statistics_link')) {
    shareAccessCount.init(4, order_id); //分享数据上报
}

// obj.setShareInfo = function(rs) {
//     shareUrl = rs.data.url
//     shareWords = rs.data.forwarding_desc
// }


obj.init = function (res) {

    $('.finish-content').append(commonDonateFailureGopayTpl(res));
    clickHandle(res);
    // toastAB(res);
};
// function chooseRecord(rs) {
//     let url = domainName.base + '/v1/choose_record';
//     let data = {
//         template_name: rs.data.TemplateName,
//         choose_type: 1,
//         choose_sub_type: rs.choose_sub_type,
//     }
//     let param = {
//         type: 'post',
//         url: url,
//         data: JSON.stringify(data)
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {},
//         error: function (rs) {
//             // utils.alertMessage(rs.msg)
//         }
//     });
// }

// function abTest(tplName) {
//     let url = domainName.base + '/v1/ab/template_total/' + tplName;
//     let param = {
//         type: 'put',
//         url: url,
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {
//             console.log('abTest-success')
//         },
//         error: function (rs) {
//             utils.alertMessage(rs.msg)
//         }
//     });
// }

// function abFirstTest(tplName){
//     let url = domainName.base + '/v1/ab/template_total_random/' + tplName;

//     let param = {
//         type: 'put',
//         url: url,
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {
//             console.log('abFirstTest-success')
//         },
//         error: function (rs) {
//             utils.alertMessage(rs.msg)
//         }
//     });
// }


// 日历数据绑定
function timeHandle(rs) {

    let Nowdate = new Date(rs.data.now);
    let WeekFirstDay = Nowdate - ((Nowdate.getDay() || 7) - 1) * 86400000;
    let arr = [];
    // console.log('WeekFirstDay=', new Date(WeekFirstDay).getDate());

    // 定义一周的数组，每日数据绑定
    for (let i = 0; i < 7; i++) {
        arr.push({
            'date': new Date(WeekFirstDay + i * 86400000).getDate(),
            'day': new Date(WeekFirstDay + i * 86400000).getDay(),
            'today': (Nowdate.getDate() == new Date(WeekFirstDay + i * 86400000).getDate()) ? true : false,
            'donateCout': ''
        });
    }

    // 捐款次数绑定到每天中
    if (rs && rs.data && rs.data.calendar) {
        for (let i = 0; i < rs.data.calendar.length; i++) {
            // console.log('rs.data[i]=', i, rs.data.calendar[i])
            let _param = rs.data.calendar[i];

            for (let i = 0; i < arr.length; i++) {
                if (new Date(_param.created_at).getDate() == arr[i].date) {
                    arr[i].donateCout = _param.count
                }
            }
        }
    }

    rs.arr = arr;
    $('.calendar').append(commonDonateSuccessCalendarTpl(rs));
    $('.donation-word.noLogin').css('display', 'none');

}

function clickHandle(res) {
    $('body').on('click', '.review-this', function (e) {
        if (res.group_id) {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/special_projects?short_link=' + short_link : '/group/' + short_link
        } else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + res.project_id : '/' + short_link
        }
    });
    $('body').on('click', '.review-all', function (e) {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html'
    });

}

obj.insertData = function(rs) {
    if (rs.data.length != 0) {
        rs.fromWhichPage = 'commonDonateSuccess.html'
        campaignListComponents.init(rs);
    }
}

export default obj;