import mainTpl from '../tpl/common_footer.juicer'
import domainName from 'domainName'; //port domain name
import store from 'store'

let obj = {};
let $UI = $('body');
obj.UI = $UI;

obj.init = function(res) {
    res.domainName = domainName;
    $UI.append(mainTpl(res));


    // close APP download
    if ($('.app-download').css('display') === 'block') {
        console.log('app-download')
        $('.page-inner').css('padding-top', '102px')
    }
    $('body').on('click', '.appDownload-close', function(e) {
        // store.set('app_download', 'false')

        $('.app-download').css({
            'display': 'none'
        })
        $('.page-inner').css('padding-top', '56px')

    });
};

export default obj;