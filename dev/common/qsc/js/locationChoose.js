import mainTpl from '../tpl/locationChoose.juicer'
import listTpl from '../tpl/region_list.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import "../less/locationChoose.less";
import utils from 'utils';


import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie





/* translation */
import gtryDisease from 'gtryDisease'
import qscLang from 'qscLang'
import commonTitle from 'commonTitle'
let titleLang = qscLang.init(commonTitle);
let lang = qscLang.init(gtryDisease);

// import common from 'common'
// import qscLang from 'qscLang'
// let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

var reqObj = utils.getRequestParams();
var project_id = reqObj['project_id'];

//保险只有2级联动
let asuransiPolicy_step;

//互助：前两级为地区选择 第三极分为地区选择和医院选择
let locationChooseType;

var reqSelect = location.href
/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function (res) {

    res.domainName = domainName;
    res.lang = lang;
    console.log('location-res', res);
    locationChooseType = res.locationChooseType
    asuransiPolicy_step = res.asuransiPolicy_step
    $UI.prepend(mainTpl(res));

    fastclick.attach(document.body);
    obj.event(res);

};
obj.renderList = function (res) {
    res.locationChooseType = locationChooseType
    console.log('renderList', res);
    $('.province-list').prepend(listTpl(res));
}

obj.event = function (res) {

    // choose location
    $('body').on('click', '.address-content', function () {
        $('.location-list-wrapper').css('display', 'block');
    })
    // close location
    $('body').on('click', '.close', function () {
        $('.location-list-wrapper').css('display', 'none')
    })
    $('body').on('click', '.mask', function () {
        $('.location-list-wrapper').css('display', 'none')
    })

    /*
    点击列表title交互
    */

    //1级
    $('.level-1').on('click', function (rs) {

        $('.province-list').removeClass('third-region-list').removeClass('second-region-list').addClass('first-region-list');
        $('.first-region-list').empty();
        // $UI.trigger('reloadRegionList', 0);
        reloadRegionList(0);
        $(this).siblings().removeClass('choosed')
        $(this).addClass('choosed').attr('province')
        $('.level-2').html(lang.lang51)

        // $('.region-choose-left').addClass('left-1').removeClass('left-3');
        // $('.level-1').siblings().css('display','none').removeClass('choosed').removeClass('level-2').removeClass('level-3')
        // $('.level-1').css('display', 'block').addClass('choosed')


        if (locationChooseType == 'regionChoose') {
            $('.province-title').text(lang.lang44)
            $('.level-3').html(lang.lang52)

        } else {
            $('.province-title').text(lang.lang47)
            $('.level-3').html(lang.lang53)

        }
    })

    //2级
    $('.level-2').on('click', function (rs) {

        $('.province-list').removeClass('first-region-list').removeClass('third-region-list').addClass('second-region-list');
        $('.second-region-list').empty();
        reloadRegionList($('.region-choose-wrap').attr('province-id'));
        // $UI.trigger('reloadRegionList', $('.region-choose-wrap').attr('province-id'));
        $(this).siblings().removeClass('choosed')
        $(this).addClass('choosed')

        if (locationChooseType == 'regionChoose') {
            $('.province-title').html(lang.lang45)
            $('.level-3').html(lang.lang52)

        } else {
            $('.province-title').text(lang.lang48)
            $('.level-3').html(lang.lang53)

        }

    })

    //3级
    $('.level-3').on('click', function (rs) {
        $('.province-list').removeClass('first-region-list').removeClass('second-region-list').addClass('third-region-list');
        $('.third-region-list').empty();

        //区分第三级选择
        if (locationChooseType == 'regionChoose') {
            $('.province-title').html(lang.lang46)

            reloadRegionList($('.region-choose-wrap').attr('city-id'));
            // $UI.trigger('reloadRegionList', $('.region-choose-wrap').attr('city-id'));
        } else {
            $('.province-title').html(lang.lang49)

            reloadHospitalList($('.region-choose-wrap').attr('province-name'), $('.region-choose-wrap').attr('city-name'));
        }
        $(this).siblings().removeClass('choosed')
        $(this).addClass('choosed')
    })



    //1级选取
    $('body').on('click', '.first-region-list .list-items', function (e) {


        if ($(this).data('level') == 1) {
            if (locationChooseType == 'regionChoose') {
                $('.province-title').html(lang.lang45)
            } else {
                $('.province-title').html(lang.lang48)

            }

            console.log('======level===1=====')
            //获取2级列表
            reloadRegionList($(this).data('id'));

            // $UI.trigger('reloadRegionList', $(this).data('id'));

            $('.region-choose-left').removeClass('left-1').addClass('left-2');
            $('.list-items').removeClass('choosed-region');
            $('.province-list').removeClass('first-region-list').addClass('second-region-list');

            $(this).addClass('choosed-region');

            $('.level-2').siblings().removeClass('choosed')
            $('.level-2').css('display', 'block').addClass('choosed')

            $('.region-choose-wrap').attr('province-id', $(this).data('id')).attr('province-name', $(this).children().html())
            $('.level-1').attr('data-id', $(this).data('id')).html($(this).children().html());

            // $('.address-content').html($('.region-choose-wrap').attr('province-name')).attr('province-id', $(this).data('id')).attr('province-name', $(this).children().html())

        }
    })

    //2级选取
    $('body').on('click', '.second-region-list .list-items', function (e) {

        if ($(this).data('level') == 2) {

            if (asuransiPolicy_step) {

                $('.location-list-wrapper').css('display', 'none')
                $('.region-choose-wrap').attr('city-id', $(this).data('id')).attr('city-name', $(this).children().html())
                $('.level-2').attr('data-id', $(this).data('id')).html($(this).children().html());
                $('.address-content').attr('city-id', $(this).data('id')).attr('city-name', $(this).children().html())
                $('.address-content').html($('.region-choose-wrap').attr('province-name') + ',' + $('.region-choose-wrap').attr('city-name')).css('color', '#333')

            } else {

                if (locationChooseType == 'regionChoose') {
                    $('.province-title').html(lang.lang46)

                    reloadRegionList($(this).data('id'));
                } else {
                    $('.province-title').html(lang.lang49)

                    reloadHospitalList($('.region-choose-wrap').attr('province-name'), $(this).data('name'));
                }
                $('.region-choose-left').removeClass('left-2').addClass('left-3');
                $('.list-items').removeClass('choosed-region');
                $('.province-list').removeClass('second-region-list').addClass('third-region-list');
                $('.level-3').siblings().removeClass('choosed')
                $('.level-3').css('display', 'block').addClass('choosed')

                $('.region-choose-wrap').attr('city-id', $(this).data('id')).attr('city-name', $(this).children().html())
                $('.level-2').attr('data-id', $(this).data('id')).html($(this).children().html());

            }
            // $('.address-content').html($('.region-choose-wrap').attr('province-name') + ',' + $('.region-choose-wrap').attr('city-name')).css('color', '#333')
        }
    })


    //3级选取
    $('body').on('click', '.third-region-list .list-items', function (e) {
        if ($(this).data('level') == 3) {
            if (locationChooseType == 'regionChoose') {
                $('.region-choose-wrap').attr('district-id', $(this).data('id')).attr('district-name', $(this).children().html())
                reloadRegionList($(this).data('id'));
                $('.address-content').html($('.region-choose-wrap').attr('province-name') + ',' + $('.region-choose-wrap').attr('city-name') + ',' + $('.region-choose-wrap').attr('district-name')).css('color', '#333')

            } else {

                if ($(this).data('id') == 0) {
                    $('.patient-hospital').css('display', 'block')
                } else {
                    $('.patient-hospital').css('display', 'none')
                }
                $('.region-choose-wrap').attr('hospital-id', $(this).data('id')).attr('hospital-name', $(this).children().html())
                $('.address-content').attr('hospital-id', $(this).data('id')).html($('.region-choose-wrap').attr('province-name') + ',' + $('.region-choose-wrap').attr('city-name') + ',' + $('.region-choose-wrap').attr('hospital-name')).css('color', '#333')

            }

            $('.location-list-wrapper').css('display', 'none')
            $('.level-3').attr('data-id', $(this).data('id')).html($(this).children().html());
            $('.list-items').removeClass('choosed-region');
            $(this).addClass('choosed-region');
        }

    })


}



function reloadRegionList(region_level) {

    let url = domainName.base + '/v1/regions?pid=' + region_level;
    ajaxProxy.ajax({
        type: 'get',
        url: url,

    }, {
        success: function (rs) {
            if (rs.code == 0) {
                // sort ascending name
                let locationTemp;
                locationTemp = rs.data.sort((a, b) => {
                    if (a.name < b.name)
                      return -1;
                    if (a.name > b.name)
                      return 1;
                    return 0;
                });
                rs.data = locationTemp;
                
                obj.renderList(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })

}


function reloadHospitalList(procinveName, cityName) {
    let url = domainName.base + '/v1/hospitals?province=' + encodeURIComponent(procinveName) + '&city=' + encodeURIComponent(cityName);
    ajaxProxy.ajax({
        type: 'get',
        url: url,

    }, {
        success: function (rs) {
            if (rs.code == 0) {
                rs.hospitalRender = true
                obj.renderList(rs);
            } else {
                utils.alertMessage(rs.msg)
            }
        },
        error: utils.handleFail
    })

}


export default obj;