import mainTpl from '../tpl/Asuransi-bankList.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name

import "../less/Asuransi-bankList.less";

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let reqObj = utils.getRequestParams();
let project_id = reqObj['project_id'];

/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {

    res.domainName = domainName;
    res.commonLang = commonLang;
    res.isAndroid = utils.browserVersion.android;

    $UI.prepend(mainTpl(res));

    // $UI.trigger('calculationFormula'); //计算公式

    fastclick.attach(document.body);
    obj.event(res);
};

obj.event = function(res) {
    // close
    $('.alert-bank .close').on('click', function(rs) {
        $('.alert-bank').css('display', 'none')
    })

    // choose payment method
    $('body').on('click', '.list-items', function(e) {
        $('.list-items').removeClass('choosed-bank');
        $(this).addClass('choosed-bank');

        setTimeout(() => {
            $('.alert-bank').css('display', 'none')
        }, 100)

        $('.channel').addClass('selected').html($(this).html());

        $UI.trigger('calculationFormula'); //计算公式
    })
}

export default obj;