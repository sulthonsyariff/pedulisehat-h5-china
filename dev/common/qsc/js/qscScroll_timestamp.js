function QscScrollTime() {

    var $wrapper;
    var onNeedLoad;


    var intId;

    var jumpTime = 0;

    var loading = false;

    this.config = function(o) {
        $wrapper = o.wrapper;
        onNeedLoad = o.onNeedLoad;
    };

    this.run = function() {
        intId = setInterval(function() {
            if (loading) {
                return;
            }
            
            if ($wrapper.css("display") == "none")
                return;

            // var scrollTop = $wrapper.scrollTop();
            // scrollTop在FireFox与Chrome浏览器间的兼容问题
            var scrollTop = $(document).scrollTop();
            var dmtHeight = $wrapper[0].scrollHeight;

            var windowHeight = $(window).height();
            var bottomDis = dmtHeight - scrollTop - windowHeight;
            // console.log(bottomDis,22,dmtHeight,scrollTop,windowHeight)
            if (bottomDis < 300) {
                clearInterval(intId);
                loading = false;
                onNeedLoad();

            }
            if (jumpTime == 0) {
                jumpTime = 1000;
            }
            // console.log(jumpTime,Math.random())
        }, jumpTime)
    };

    this.stop = function() {
        clearInterval(intId);
    }

}



export default QscScrollTime;