let obj = new Object();



let ws; //websocket实例
let lockReconnect = false; //避免重复连接
// var wsUrl = "ws://" + 'xxxxxxx';

obj.WS = function (wsUrl, cbObj) {

    function createWebSocket(url) {
        try {
            if ('WebSocket' in window) {
                ws = new WebSocket(url);
            } else if ('MozWebSocket' in window) {
                ws = new MozWebSocket(url);
            }
            // else {
            //     url = "http://" + 'xxxxxxx';
            //     ws = new SockJS(url);
            // }
            console.log('new Websocket')
            initEventHandle();
        } catch (e) {
            reconnect(url);
        }
    }

    function initEventHandle() {
        ws.onclose = function (event) {
            // let date = new Date();
            console.log('websocket服务关闭了');
            reconnect(wsUrl);
        };
        ws.onerror = function (event) {
            console.log('websocket服务出错了');
            reconnect(wsUrl);
        };
        ws.onopen = function (event) {
            //心跳检测重置
            checkIsAlive.reset().start();
        };
        ws.onmessage = function (event) {
            //如果获取到消息，心跳检测重置
            //拿到任何消息都说明当前连接是正常的
            // console.log('websocket服务获得数据了', event.data);

            //接受消息后的UI变化
            // doWithMsg(event.data);

            cbObj.success && cbObj.success(event.data);
            // console.log('cbObj',cbObj)

            if (cbObj.sendMSG) {
                ws.send(JSON.stringify(cbObj.sendMSG))
            }

            // if (callback) {
            //     callback(event.data);
            // }

            checkIsAlive.reset().start();
        }

        //收到消息推送
        // function doWithMsg(msg) {
        //     // let msg = JSON.parse(msg)
        //     console.log('msg',JSON.parse(msg).count);

        //     // return msg;
        // }

    }

    function reconnect(url) {
        if (lockReconnect) return;
        lockReconnect = true;
        //没连接上会一直重连，设置延迟避免请求过多
        setTimeout(function () {
            createWebSocket(url);
            lockReconnect = false;
        }, 2000);
    }

    //心跳检测
    var checkIsAlive = {
        timeout: 60000, //60秒
        timeoutObj: null,
        serverTimeoutObj: null,
        reset: function () {
            clearTimeout(this.timeoutObj);
            // console.log('timeoutObj', this.timeoutObj)
            clearTimeout(this.serverTimeoutObj);
            // console.log('serverTimeoutObj', this.serverTimeoutObj)
            return this;
        },
        start: function () {
            var self = this;
            // console.log('self', self)
            this.timeoutObj = setTimeout(function () {
                //这里发送一个心跳，后端收到后，返回一个心跳消息，
                //onmessage拿到返回的心跳就说明连接正常
                ws.send("HeartBeat");
                self.serverTimeoutObj = setTimeout(function () { //如果超过一定时间还没重置，说明后端主动断开了
                    ws.close(); //如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
                }, self.timeout)
            }, this.timeout)
        }
    }

    //初始化websocket
    createWebSocket(wsUrl);

}


export default obj;
