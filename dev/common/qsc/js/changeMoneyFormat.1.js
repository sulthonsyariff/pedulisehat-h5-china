/*
 * 输入金额三位一个点
 */
let [obj, $UI] = [{}, $('body')];

obj.init = function($input) {
    //输入框三位一个小数点
    $UI.on('input', $input, function(event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });

    $UI.on('blur', $input, function(event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });

    $UI.on('keyup', $input, function(event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });

    $UI.on('keydown', $input, function(event) {
        var value = this.value.replace(/[\D]/g, "");
        value = value === '' ? '' : Number(value) + '';
        $(this).val(obj.moneyFormat(value));
    });
}

obj.moneyFormat = function(price) {
    price = price + '';
    if (!price)
        return '';
    if (price === "0")
        return "0";
    if (price.length < 4)
        return price;
    var begin = price.length % 3;

    begin = begin === 0 ? 3 : begin;


    return price.substring(0, begin) + price.substring(begin).replace(/(\d{3,3})/g, ".$1");
}

export default obj;