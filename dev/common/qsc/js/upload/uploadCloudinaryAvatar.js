/**
 * uploadCloudinaryMore.js
 * 该文件是对 createUploaderMore 的包装，简化了图片上传的相关操作
 */
import qscUpload from 'createUploaderAvatar'
import 'store'

/**
 * 设置图片上传组件参数
 * @param {string} btnId upload btn的ID，不包括`#`
 * @param {string} imgList 包含上传图片的容器
 * @param {string} imgKey 上传图片组标识，当同一页面有多个上传板块时区分栏目。
 * @param {number} numLimit 上传图片的最大张数
 * @param {function} callback 上传成功后和删除图片的回调函数
 * @param {string} ossURL OSS数据接口地址
 */
function create(btnId, imgList, imgKey, numLimit, callback, ossURL) {
    qscUpload.createUploader(
        btnId,
        imgList,
        imgKey,
        numLimit,
        callback,
        ossURL
    );

    addDeleteHandler(imgList, callback);
}


/**
 * 获取已经成功上传的图片信息
 * @param {string} imgList 包含上传的容器
 * @param {string} imgKey 上传图片组标识
 */
function getImageList(imgList, imgKey) {
    var cover = $(imgList).find('.addAvatar').find("img").attr("src");
    //     thumbs = [],
    //     origins = [];
    // var $list = $(imgList);

    console.log('getImageList:', $(imgList).find('.addAvatar').find("img").attr("src"));

    return cover;
}

/**
 * 使用 datas 包含的数据填充 imgList 的，主要用于从本地缓存读取数据
 */
function setImageList(imgList, imgKey, cover) {
    var $list = $(imgList);
    var dom = '';

    //如果没有缓存，则退出
    if (!cover) {
        return;
    }

    //cover 格式为 [{"thumb": ".png","image": ".png","cutting": ".png"},{"thumb": ".png","image": ".png","cutting": ".png"}]
    cover.forEach(function(item, idx) {
        var img = item.image;
        var thumb; // 缩略图
        var image; // 原始图，用于 fancybox 弹出显示

        image = img; // 原始图
        thumb = img.replace('image/upload/', 'image/upload/c_fill,g_face,h_72,w_72/'); // 缩略图

        //创建模版
        var template = '<div class="file-item thumbnail" id = ' + fileId + '>' +
            '<span class="addAvatar">' +
            '<img>' +
            '</span>' +
            '</div>';

        dom += template;
    });
    $list.html(dom);
}

/**
 * 删除图片时的回调函数
 * @param {string} imgList 包含上传的容器
 * @param {function} callback 删除后的回调函数
 */
function addDeleteHandler(imgList, callback) {
    $(imgList).on('click', '.file-panel', function(event) {
        event.stopPropagation();
        event.preventDefault();

        var block = $(this);
        block.parents('.file-item').off().find('.file-panel').off().end().remove();

        if (callback) {
            callback();
        }
    });
}

/**
 * 更新图片显示的文字
 * @param {string} imgList 包含上传图片的容器
 */
function refreshPictureNumber(imgList) {
    console.log('refresh picture number: ' + imgList);
    $(imgList).find('.file-item').each(function(i, ele) {
        if (i == 0) {
            $(ele).find('.info').html('cover');
        } else {
            $(ele).find('.info').html('pic ' + (i + 1));
        }
    });
}

export default {
    create: create,
    refreshPictureNumber: refreshPictureNumber,
    getImageList: getImageList,
    setImageList: setImageList
}