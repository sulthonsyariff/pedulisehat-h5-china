/**
 * qscUploadCloudinary.js
 * 用于向 Cloudinary 上传图片。
 *
 * 上传流程
 * 首先， 向 https://project.pedulisehat.id/publish/cloudinary 请求 public_id, timestamp, api_key, signature 等参数，然后向 Cloudinary 发送数据
 *
 * 参考文档：http://cloudinary.com/documentation/jquery_integration#getting_started_guide
 */
import 'cloudinary'
import 'fileupload'
import 'fancybox'
import utils from 'utils'
import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let qscUpload = {};

/**
 * 生成上传图片功能
 * @param {string} upBtnWrapperStr 上传图片按钮
 * @param {string} listWrapperStr 上传图片列表容器
 * @param {string} fileKey 上传图片的键值
 * @param {string} fileNumLimit 上传的最大图片数
 * @param {function} cb 上传成功之后的回调函数
 * @param {string} ossURL oss后台参数获取地址
 */
qscUpload.createUploader = function(upBtnWrapperStr, listWrapperStr, _fileKey, _fileNumLimit, cb, ossURL) {
    let $list = $(listWrapperStr);
    let cloud_name = '';
    let config = {};

    let $upBtn;
    let listWrapper;
    let fileKey;
    let fileNumLimit;
    let callback;

    $upBtn = $('#' + upBtnWrapperStr);
    fileKey = _fileKey;
    callback = cb;
    fileNumLimit = _fileNumLimit;

    let cover = [],
        img;
    let thumb; //缩略图
    let image; //原图

    let isLocal = location.href.indexOf("pedulisehat.id") == -1;

    getConfig(configHandler);

    /**
     * 从后台获取 Cloudinary 配置信息
     * 配置信息包括：
     * api_key,cloud_name, signature, timestamp
     */
    function getConfig(callback) {
        console.log('get config');
        // let url = domainName.project + '/v1/cloudinary';
        let url = domainName.project + '/v1/cloudinary/signature?upload_preset=avatar_upload_presets&tags=H5,Avatar';

        if (isLocal) {
            // 假数据
            url = '/mock/cloudinary.json';
        }

        let params = {
            url: url,
            type: 'GET'
        };

        ajaxProxy.ajax(params, {
            success: function(res) {
                console.log('success in read cloudinary signature');
                if (res.code != 0) {
                    utils.alertMessage(res.msg);
                    return;
                }

                callback && callback(res.data);
            },
            error: function(res) {
                console.log('error in get cloudinary signature');
            }
        });
    }

    /**
     * 读取服务器配置成功后的回调函数
     * @param {object} data - 服务器提供的配置参数
     */
    function configHandler(data) {
        config = data;
        console.log('show config: ');
        console.log(config);

        setupCloudinary(config);
    }

    /**
     * 配置 Cloudianry
     * @param {object} config - Cloudianry 配置参数
     * @param {string} config.cloud_name - 存储地址
     * @param {string} config.api_key - 公钥
     * @param {string} config.public_id - 文件名称
     * @param {string} config.timestamp - 时间戳
     * @param {string} config.callback - 旧浏览器兼容地址 "http://www.example.com/cloudinary_cors.html"
     * @param {string} config.signature - 签名
     */
    function setupCloudinary(config) {

        console.log('setup cloudinary:cloud_name api_key');

        $.cloudinary.config({
            cloud_name: config.CloudName,
            api_key: config.ApiKey
        });

        let input = createInput(config);
        $upBtn.parent().append(input); //cloudinary选择文件按钮

        // init cloudinary lib
        if ($.fn.cloudinary_fileupload != undefined) {
            $('input.cloudinary-fileupload[type=file]').cloudinary_fileupload();
        }

        // 开始图片上传操作
        startUploadCloudinaryImage(config);
    }

    /**
     * 根据配置参数生成上传表单
     * @param {object} config - 详情见 setupCloudianry 函数注释
     */
    function createInput(config) {

        console.log('create input in html');

        let el = document.createElement('input');
        el.setAttribute('name', 'file');
        el.setAttribute('type', 'file');
        // el.setAttribute('multiple', 'multiple'); //支持选择多张图片
        el.setAttribute('accept', 'image/jpeg,image/jpg,image/png,image/bmp'); //支持格式
        el.classList.add('cloudinary-fileupload');
        el.setAttribute('data-cloudinary-field', config.public_id);


        let tp = Number(config.Timestamp)
        // let sign = config.Signature
        let key = Number(config.ApiKey)

        let formData = {
            timestamp: tp,
            signature: config.Signature,
            api_key: key,
            upload_preset: 'avatar_upload_presets',
            tags: 'H5,Avatar'
            // timestamp: config.Timestamp,
            // callback: config.callback || 'http://pedulisehat.id/cloudinary_cors.html',
            // signature: config.Signature,
            // api_key: config.ApiKey,
            // eager: 'c_fill,g_face,h_600,w_600'
                // public_id: config.public_id
        };


        el.setAttribute('data-form-data', JSON.stringify(formData));

        return el;
    }

    /**
     * 图片上传操作
     * fileuploadsend 当有文件添加进队列时触发
     * fileuploadprogress 上传过程中操作
     * cloudinarydone 上传云端完毕
     */
    function startUploadCloudinaryImage(config) {

        let count = 0;

        // 当有文件添加进队列时触发，创建模版
        $('.cloudinary-fileupload').bind('fileuploadsend', function(e, data) {

            //设置计数器，为每个上传的图片设置独立的id 例如：fileId_1
            count = count + 1;

            let fileId = "fileId_" + count;

            //创建模版
            let $li = '<div class="file-item thumbnail" id = ' + fileId + '>' +
                '<span class="addAvatar">' +
                '<img>' +
                '</span>' +
                '</div>';

            utils.showLoading('loading');
            $list.html("");
            $list.append($li);
            data.count = count;

        });

        // 上传过程中操作,主要是使本页面的提交按钮禁用,还有进度条实时显示
        $('.cloudinary-fileupload').bind('fileuploadprogress', function(e, data) {

            let $li = $list.find('#fileId_' + data.count);
            let $percent = $li.find('.progress span');

            // 避免重复创建
            if (!$percent.length) {
                $percent = $('<p class="progress"><span></span></p>')
                    .appendTo($li)
                    .find('span');
            }
            $percent.css('width', Math.round((data.loaded * 100.0) / data.total) + '%');

        });

        //监听'上传云端完毕'事件,将图片添加进队列
        $('.cloudinary-fileupload').bind('cloudinarydone', function(e, data) {

            // 获取cloudinary 上传图片
            // img = $.cloudinary.image(data.result.public_id, {
            //     format: data.result.format,
            //     version: data.result.version,
            //     secure: true,
            //     quality: "auto"
            // });
            //原图
            // image = img[0].src;

            // console.log('=====获取cloudinary 上传图片====:', img);

            //缩略图
            thumb = 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_72,h_72/' + data.result.secure_url;
            // thumb = image.replace('image/upload/', 'image/upload/g_face,h_72,w_72,c_thumb,r_max/');

            let $li = $list.find('#fileId_' + data.count);
            // $li.find('.fancybox').attr('href', image);
            $li.find('.addAvatar').find("img").attr("src", thumb);
            // $li.find('.fancybox').fancybox();

            //cover 格式为 [{"thumb": ".png","image": ".png"},{"thumb": ".png","image": ".png"}]
            cover.push({
                // image: image,
                thumb: thumb
            });


            // //把绿色进度条删除掉
            // $list.find('.progress').remove();
            // // 将之前禁用的按钮解除禁用
            // $("a.btn:visible, input.btn:visible, span.btn:visible, button.btn:visible", $("form")).each(function(index, el) {
            //     $(el).removeClass("disabled");
            // });

            if (callback && (typeof callback == 'function')) {
                callback();
            }

        });

        //监听'上传失败'事件，
        $('.cloudinary-fileupload').bind('fileuploadfail', function(e, data) {
            utils.alertMessage('Upload Error, please try again later!');
        });

    }

}

export default qscUpload;