import utils from 'utils'

/* translation */
import system from 'system'
import qscLang from 'qscLang'
let lang = qscLang.init(system);
/* translation */

let obj = {};

obj.AppHasInstalled = function(appSrc, errorInfo) {
    console.log('native://intent?url=' +
        encodeURIComponent(appSrc) +
        '&errorInfo=' +
        encodeURIComponent(errorInfo ? errorInfo : lang.lang2));

    // if in mobile，check have gojek app / shopee
    if (utils.browserVersion.mobile) {

        // in our android app:安卓内需要加上协议才能打开外部协议
        if (utils.browserVersion.androids) {
            // native://intent?url=<gojet url>&errorInfo=<在此加上协议解析失败的提示 比如提示用户未安装app之类的>
            // console.log('===window.location===', 'native://intent?url=' + encodeURIComponent(appSrc) + '&errorInfo=Scan the QR code to pay');
            console.log('===window.location===', 'native://intent?url=' + encodeURIComponent(appSrc) + '&errorInfo=Scan the QR code to pay');

            window.location = 'native://intent?url=' +
                encodeURIComponent(appSrc) +
                '&errorInfo=' +
                encodeURIComponent(errorInfo ? errorInfo : lang.lang2);

        } else {
            console.log('===window.location===', appSrc);

            // console.log('===window.location===', appSrc);
            window.location = appSrc; //打开某手机上的某个app应用

            setTimeout(function() {
                console.log('cancle open app');
                //如果超时就取消
                window.close();
            }, 500);
        }
    }
}

export default obj;