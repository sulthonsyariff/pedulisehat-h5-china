import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};

model.getFreshID({
    success: function (res) {
        if (res.code == 0) {
            store.set('freshID', {
                externalID: res.data.userId,
                restoreID: res.data.restoredId
            })
            console.log('getFreshID', store.get('freshID'))
        }
    }
})


//fetch freshchat id
obj.getFreshID = function(o) {
    var url = domainName.project + '/v1/freshchat';
    
    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};
//update freshchat id
obj.getFreshID = function(o) {
    var url = domainName.project + '/v1/freshchat';
    
    ajaxProxy.ajax({
        type: 'put',
        url: url,
        data: JSON.stringify(o.param)
    }, o)
};

export default obj;