import utils from 'utils'
import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name
import changeMoneyFormat from 'changeMoneyFormat'
import store from 'store'

import commonDonateSuccessTpl from 'commonDonateSuccessTpl'
import commonDonateSuccessCalendarTpl from 'commonDonateSuccessCalendarTpl'
import common_donate_success_listItem from '../tpl/common_donate_success_listItem.juicer'
import common_donate_success_toast from '../tpl/common_donate_success_toast.juicer'
import "../less/common_donate_success.less";
import share from 'share'

import shareAccessCount from 'shareAccessCount' //分享数据上报
import ramadanTrophiesAlertBox from 'ramadanTrophiesAlertBox'
/* translation */
import donateSuccess from 'donateSuccess'
let lang = qscLang.init(donateSuccess);
import common from 'common'
import qscLang from 'qscLang'
let commonLang = qscLang.init(common);
/* translation */

let isLocal;
let obj = {};
let $UI = $('body');
obj.UI = $UI;

// let shareUrl;
// let shareWords = [];
let tplName
let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';

let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'] || '';
let order_id = reqObj['order_id'] || reqObj['bill_no'];

let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || ''; //专题

console.log("===sessionStorage.getItem('statistics_link')===", sessionStorage.getItem('statistics_link'));

if (sessionStorage.getItem('statistics_link')) {
    shareAccessCount.init(4, order_id); //分享数据上报
}

// obj.setShareInfo = function(rs) {
//     shareUrl = rs.data.url
//     shareWords = rs.data.forwarding_desc
// }


obj.init = function(res) {

    $('.finish-content').append(commonDonateSuccessTpl(res));
    clickHandle(res);
    getCalendar(res);
    toastAB(res);
};

function toastAB(res) {
    // let isAB;

    if (store.get('paySuccessToast-AB')) {
        tplName = store.get('paySuccessToast-AB');
        if (tplName == 'GTRY-guide-B1' || tplName == 'GTRY-guide-B2' || tplName == 'GTRY-guide-B3') {

            toastCrtl();

            if (!user_id) {
                abTest(tplName);
            }

            // } else if (tplName == 'GTRY-guide-A') {
            //     abTest(tplName);

            //     // gtry entrance
            //     $('body').on('click', '.gtry-entrance', function (e) {
            //         let rs = {};
            //         rs.data = {};
            //         rs.data.TemplateName = 'GTRY-guide-A'
            //         rs.choose_sub_type = 'gtryLink'
            //         console.log('rs', rs)
            //         chooseRecord(rs);

            //         setTimeout(() => {
            //             if (utils.judgeDomain() == 'qa') {
            //                 location.href = 'https://gtry-qa.pedulisehat.id'
            //             } else if (utils.judgeDomain() == 'pre') {
            //                 location.href = 'https://gtry-pre.pedulisehat.id'
            //             } else {
            //                 location.href = 'https://gtry.pedulisehat.id'
            //             }
            //         }, 300);

            //     });
        }

    } else {
        // if (Math.floor(Math.random() * 10 + 1) < 6) {
        //     console.log('==========a==========')
        //     tplName = "GTRY-guide-A"
        //     store.set('paySuccessToast-AB', tplName);

        //     abTest(tplName);

        //     // gtry entrance
        //     $('body').on('click', '.gtry-entrance', function (e) {
        //         let rs = {};
        //         rs.data = {};
        //         rs.data.TemplateName = 'GTRY-guide-A'
        //         rs.choose_sub_type = 'gtryLink'
        //         console.log('rs', rs)
        //         chooseRecord(rs);

        //         setTimeout(() => {
        //             if (utils.judgeDomain() == 'qa') {
        //                 location.href = 'https://gtry-qa.pedulisehat.id'
        //             } else if (utils.judgeDomain() == 'pre') {
        //                 location.href = 'https://gtry-pre.pedulisehat.id'
        //             } else {
        //                 location.href = 'https://gtry.pedulisehat.id'
        //             }
        //         }, 300);

        //     });

        // } else {
        console.log('==========b==========')

        // tplName = "b"
        if (!user_id) {
            abTest(tplName);
        }
        toastCrtl();
        // };

        // abTest(tplName);
        // abFirstTest(tplName);

    }
    return tplName;
}

function toastCrtl() {
    let url = domainName.heouic + '/v1/pop_guide';

    let param = {
        type: 'get',
        url: url,
    }
    ajaxProxy.ajax(param, {
        success: function(rs) {
            if (rs.data.TemplateName == 'GTRY-guide-B1') {
                tplName = 'GTRY-guide-B1'
                store.set('paySuccessToast-AB', tplName);

            } else if (rs.data.TemplateName == 'GTRY-guide-B2') {
                tplName = 'GTRY-guide-B2'
                store.set('paySuccessToast-AB', tplName);

            } else if (rs.data.TemplateName == 'GTRY-guide-B3') {
                tplName = 'GTRY-guide-B3'
                store.set('paySuccessToast-AB', tplName);

            }
            rs.lang = lang
            console.log('toastCtrl', rs)
            $UI.append(common_donate_success_toast(rs));
            toastClick(rs);
        },
        unauthorizeTodo: function() {
            let rs = {};
            rs.lang = lang
            rs.data = {};
            tplName = 'GTRY-guide-B1'
            rs.data.TemplateName = 'GTRY-guide-B1'
            store.set('paySuccessToast-AB', tplName);
            $UI.append(common_donate_success_toast(rs));
            toastClick(rs);

        },
        error: function(rs) {
            utils.alertMessage(rs.msg)
        }
    }, 'unauthorizeTodo');
}

function toastClick(rs) {
    $('body').on('click', '.gtryHomeBtn', function(e) {
        rs.choose_sub_type = 'gtryClick'
        chooseRecord(rs);

        setTimeout(() => {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id'
            } else {
                location.href = 'https://gtry.pedulisehat.id'
            }
        }, 300);
    });

    $('body').on('click', '.topUpBtn', function(e) {
        rs.choose_sub_type = 'gtryClick'
        chooseRecord(rs);

        setTimeout(() => {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id/topUp.html'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id/topUp.html'
            } else {
                location.href = 'https://gtry.pedulisehat.id/topUp.html'
            }
        }, 300);
    });

    $('body').on('click', '.activeBtn', function(e) {
        rs.choose_sub_type = 'gtryClick'
        chooseRecord(rs);
        setTimeout(() => {

            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id/membershipCard.html'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id/membershipCard.html'
            } else {
                location.href = 'https://gtry.pedulisehat.id/membershipCard.html'
            }
        }, 300);
    });

    // gtry entrance
    $('body').on('click', '.gtry-entrance', function(e) {
        rs.choose_sub_type = 'gtryLink'
        chooseRecord(rs);

        setTimeout(() => {
            if (utils.judgeDomain() == 'qa') {
                location.href = 'https://gtry-qa.pedulisehat.id'
            } else if (utils.judgeDomain() == 'pre') {
                location.href = 'https://gtry-pre.pedulisehat.id'
            } else {
                location.href = 'https://gtry.pedulisehat.id'
            }
        }, 300);

    });

    $('body').on('click', '.close-icon', function(e) {
        $('.donation-toast').css('display', 'none')
        rs.choose_sub_type = 'gtryClose'
        chooseRecord(rs);

        rs.isDonatedOrshare = 'donated'; // 'donated'
        rs.fromWhichPage = 'donateSuccess.html' //google analytics need distinguish the page name
        ramadanTrophiesAlertBox.init(rs);

    });
}

function chooseRecord(rs) {
    let url = domainName.base + '/v1/choose_record';
    let data = {
        template_name: rs.data.TemplateName,
        choose_type: 1,
        choose_sub_type: rs.choose_sub_type,
    }
    let param = {
        type: 'post',
        url: url,
        data: JSON.stringify(data)
    }
    ajaxProxy.ajax(param, {
        success: function(rs) {},
        error: function(rs) {
            // utils.alertMessage(rs.msg)
        }
    });
}

function abTest(tplName) {
    let url = domainName.base + '/v1/ab/template_total/' + tplName;
    let param = {
        type: 'put',
        url: url,
    }
    ajaxProxy.ajax(param, {
        success: function(rs) {
            console.log('abTest-success')
        },
        error: function(rs) {
            utils.alertMessage(rs.msg)
        }
    });
}

// function abFirstTest(tplName){
//     let url = domainName.base + '/v1/ab/template_total_random/' + tplName;

//     let param = {
//         type: 'put',
//         url: url,
//     }
//     ajaxProxy.ajax(param, {
//         success: function (rs) {
//             console.log('abFirstTest-success')
//         },
//         error: function (rs) {
//             utils.alertMessage(rs.msg)
//         }
//     });
// }


function getCalendar(res) {
    // get calendar
    let url = domainName.psuic + '/v1/donated_calendar';
    // if (1) {
    //     url = '/mock/v1_calendar.json';
    // }
    let param = {
        type: 'get',
        url: url,
    }
    ajaxProxy.ajax(param, {
        success: function(rs) {
            if (rs.code == 0 && rs.data && rs.data.now) {
                rs._data = res.data;
                rs.lang = lang;

                timeHandle(rs); //日历
            }
        },
        unauthorizeTodo: function(rs) {

        },
        error: function(rs) {
            utils.alertMessage(rs.msg)
        }
    }, 'unauthorizeTodo');
}

// 日历数据绑定
function timeHandle(rs) {

    let Nowdate = new Date(rs.data.now);
    let WeekFirstDay = Nowdate - ((Nowdate.getDay() || 7) - 1) * 86400000;
    let arr = [];
    // console.log('WeekFirstDay=', new Date(WeekFirstDay).getDate());

    // 定义一周的数组，每日数据绑定
    for (let i = 0; i < 7; i++) {
        arr.push({
            'date': new Date(WeekFirstDay + i * 86400000).getDate(),
            'day': new Date(WeekFirstDay + i * 86400000).getDay(),
            'today': (Nowdate.getDate() == new Date(WeekFirstDay + i * 86400000).getDate()) ? true : false,
            'donateCout': ''
        });
    }

    // 捐款次数绑定到每天中
    if (rs && rs.data && rs.data.calendar) {
        for (let i = 0; i < rs.data.calendar.length; i++) {
            // console.log('rs.data[i]=', i, rs.data.calendar[i])
            let _param = rs.data.calendar[i];

            for (let i = 0; i < arr.length; i++) {
                if (new Date(_param.created_at).getDate() == arr[i].date) {
                    arr[i].donateCout = _param.count
                }
            }
        }
    }

    rs.arr = arr;
    $('.calendar').append(commonDonateSuccessCalendarTpl(rs));
    $('.donation-word.noLogin').css('display', 'none');

}

function clickHandle(res) {
    $('body').on('click', '.review-this', function(e) {
        if (res.group_id) {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/special_projects?short_link=' + short_link : '/group/' + short_link
        } else {
            location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/campaign_details?project_id=' + res.project_id : '/' + short_link
        }
    });
    $('body').on('click', '.review-all', function(e) {
        location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/main?index=0' : '/campaignList.html'
    });

    // share
    $('body').on('click', '.share', function(e) {
        let paramObj = {
            item_id: project_id || group_id,
            item_type: project_id ? 1 : 2, //0 互助 / 1 project / 2 专题 / 3 代表勋章
            item_short_link: short_link,
            // remark: '' //标记（现阶段传勋章名字）,仅勋章分享需要
            shareTplClassName: 'detail-share-wrapper', // 分享弹窗名
            shareCallBackName: 'projectShareSuccess', //大病详情分享
            fromWhichPage: 'commonDonateSuccess.html' //google analytics need distinguish the page name
        }

        if (utils.browserVersion.android) {
            $UI.trigger('android-share', ['', paramObj]);
        } else {
            // 分享组件
            share.init(paramObj);
        }
    });

    // open andriod share
    // $('body').on('click', '.andriod-share', function(e) {
    //     if (utils.browserVersion.android) {
    //         $UI.trigger('android-share2');
    //     }
    // });

    // // gtry entrance
    // $('body').on('click', '.gtry-entrance', function (e) {
    //     if (utils.judgeDomain() == 'qa') {
    //         location.href = 'https://gtry-qa.pedulisehat.id'

    //     } else if (utils.judgeDomain() == 'pre') {
    //         location.href = 'https://gtry-pre.pedulisehat.id'

    //     } else {
    //         location.href = 'https://gtry.pedulisehat.id'

    //     }

    //     // location.href = 'https://gtry-' + utils.judgeDomain() + '.pedulisehat.id'
    // });

    //facebook
    // $('body').on('click', '.fb', function(e) {
    //     e.preventDefault();
    //     utils.fbShare(shareUrl + '_1', callback);
    //     //分享计数成功后执行
    //     function callback() {
    //         let params = {
    //                 // platform: platform,
    //                 target_type: 1
    //             }
    //             // 分享后数字改变
    //         $UI.trigger('projectShare', params);
    //     }
    //     // 分享后关闭多渠道分享弹窗
    //     $('.common-share').css('display', 'none');

    //     return false;
    // });

    // //whatsapp
    // $('body').on('click', '.whatsApp', function() {
    //     utils.whatsappShare(shareWords[1], shareUrl + '_2', callback);
    //     //分享计数成功后执行
    //     function callback(platform) {
    //         let params = {
    //                 platform: platform,
    //                 target_type: 2
    //             }
    //             // 分享后数字改变
    //         $UI.trigger('projectShare', params);
    //         // console.log('分享后数字改变');
    //     }
    //     // 分享后关闭多渠道分享弹窗
    //     $('.common-share').css('display', 'none');
    //     return false;
    // });

    // //twitter
    // $('body').on('click', '.twitter', function() {
    //     utils.twitterShare(shareWords[1], shareUrl + '_3', callback);
    //     //分享计数成功后执行
    //     function callback(platform) {
    //         // 分享后数字改变
    //         let params = {
    //             platform: platform,
    //             target_type: 3
    //         }
    //         $UI.trigger('projectShare', params);
    //     }
    //     // 分享后关闭多渠道分享弹窗
    //     $('.common-share').css('display', 'none');
    //     return false;
    // });

    // // Line
    // $('body').on('click', '.line', function() {
    //     utils.lineShare(shareWords[1], shareUrl + '_5', callback);
    //     //分享计数成功后执行
    //     function callback(platform) {
    //         // 分享后数字改变
    //         let params = {
    //             platform: platform,
    //             target_type: 5
    //         }
    //         $UI.trigger('projectShare', params);
    //     }

    //     // 分享后关闭多渠道分享弹窗
    //     $('.common-share').css('display', 'none');

    //     return false;
    // });
}

obj.insertData = function(rs) {

    rs.domainName = domainName;
    let arr = [];

    if (rs.data.length != 0) {
        rs.hotCampaigns = rs.data
        rs._short_link = short_link;

        for (let i = 0; i < rs.data.length; i++) {
            if (!rs.data[i].stopped) {
                arr.push(rs.data[i])
            }
        }

        for (let i = 0; i < rs.data.length; i++) {
            rs.hotCampaigns = rs.data
                // console.log('rs.hotCampaigns[i]', rs.hotCampaigns[i].created_at)
                //时间
                // rs.hotCampaigns[i].created_at = raiseDay(rs.hotCampaigns[i].created_at);

            //时间
            if (rs.hotCampaigns[i].closed_at) {
                rs.hotCampaigns[i].timeLeft = utils.timeLeft(rs.hotCampaigns[i].closed_at);
            }

            rs.hotCampaigns[i].detailHref = utils.browserVersion.android ?
                'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.hotCampaigns[i].project_id + '&clear_top=true' :
                '/' + rs.hotCampaigns[i].short_link;

            // 进度条：设置基准的高度为1%，
            rs.hotCampaigns[i].progress = rs.hotCampaigns[i].current_amount / rs.hotCampaigns[i].target_amount * 100 + 1;

            //   图片
            rs.hotCampaigns[i].cover = JSON.parse(rs.hotCampaigns[i].cover);
            rs.hotCampaigns[i].rightUrl = utils.imageChoose(rs.hotCampaigns[i].cover)

            rs.hotCampaigns[i]._avatar = utils.imageChoose(rs.hotCampaigns[i].avatar)

            // function imageChoose(souceImg) {
            //     var fauto = souceImg.image.indexOf(",f_auto")
            //         // var imageReg = '/image/authenticated/s--[\\w\\-]{8}--/';
            //     var regResult = souceImg.image.match('/image/authenticated/s--[\\w\\-]{8}--/')

            //     if (typeof String.prototype.endsWith != 'function') {
            //         String.prototype.endsWith = function(str) {
            //             return this.slice(-str.length) == str;
            //         };
            //     }
            //     var imageEndWithWebp = souceImg.image.endsWith(".webp")

            //     if (regResult && regResult[0]) {
            //         if (imageEndWithWebp && fauto == -1) {
            //             return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120,f_auto/' + souceImg.thumb
            //         }
            //         return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120/' + souceImg.thumb
            //     }
            //     return 'https://res.cloudinary.com/dqgl4hkkx/image/fetch/c_fill,g_face,w_120,h_120/' + souceImg.image
            // }

            // 格式化金额
            rs.hotCampaigns[i].target_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].target_amount);
            rs.hotCampaigns[i].current_amount = changeMoneyFormat.moneyFormat(rs.hotCampaigns[i].current_amount);

            // 企业用户
            rs.hotCampaigns[i].corner_mark = rs.hotCampaigns[i].corner_mark ? JSON.parse(rs.hotCampaigns[i].corner_mark) : '';
        }
        rs.lang = lang;

        // console.log(rs)

        $('.campaigns_list').append(common_donate_success_listItem(rs));

        // if row's number more than 2,show the overflow class
        for (let i = 0; i < rs.data.length; i++) {
            if ($(".inner_name" + '_' + i).height() > 32) {
                $('.campaign_name' + '_' + i).addClass('overflow')
            }
        }

        let windowWidth = $(window).width() / 2
        $(".user-name").each(function() {
            console.log('windowWidth', windowWidth)
            if ($(this).width() > windowWidth) {
                $(this).css({
                    'width': windowWidth,
                    'overflow': 'hidden',
                    'white-space': 'nowrap',
                    'text-overflow': 'ellipsis',
                })
            }
        });
        // 如果超过四个项目，则隐藏
        // if ($('.campaign_box').length > 3) {
        //     $($('.campaign_box')[3]).css('display', 'none');
        // }

    }
    rs.lang = lang;
}

function raiseDay(created_at) {

    let t1 = new Date(created_at);
    let t2 = new Date();
    let raiseDay = Math.floor((t2.getTime() - t1.getTime()) / 86400000);
    return raiseDay;
}

export default obj;