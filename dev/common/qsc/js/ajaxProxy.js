 import store from 'store';
 import domainName from 'domainName'
 import utils from 'utils'
 import qscLang from 'qscLang'
 import 'jq_cookie' //ajax cookie

 let obj = new Object();
 //   let isLocal = location.href.indexOf("pedulisehat.id") == -1;
 let navLang;
 let CACHED_KEY = 'switchLanguage';

 let synLock = false; //同步锁

 //  let ua = 'Mozilla/5.0 (Linux; Android 9; MI 6 Build/PKQ1.190118.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/75.0.3770.89 Mobile Safari/537.36 ;language=zh;appVersionName=1.1.3;appVersionCode=150';

 let appVersionName = utils.browserVersion.android ? utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionName') : '';
 let appVersionCode = utils.browserVersion.android ? utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode') : '';

 //  console.log('appVersionName =', appVersionName);
 //  console.log('appVersionCode =', appVersionCode);

 // 安卓客户端切换语言监听：印尼 id-ID，英语 en-US
 //  watch language change in android
 if (navigator.userAgent.indexOf('language') > -1) {
     if (navigator.userAgent.indexOf('language=in') > -1) {
         navLang = 'id-ID'
     } else {
         navLang = 'en-US'
     }
 } else {
     if (store.get(CACHED_KEY) && store.get(CACHED_KEY).lang) {
         navLang = store.get(CACHED_KEY).lang

     } else if ($.cookie(CACHED_KEY)) {
         navLang = $.cookie(CACHED_KEY)

     } else {
         navLang = 'id-ID'
     }
 }

 console.log('Accept-Language=', navLang);

 /**
  *  需要登陆才能执行的操作，未登楼调用，正常程序会进入error报错（http Status Code: 401 Unauthorized）
  * 但不想让其报错，使页面正常加载, 故添加 cbObj.unauthorizeTodo 回调函数，给这些不需要登陆也可正常访问的页面处理使用
  * 适用：首页nav：/v1/user
  *
  * 如果unauthorizeParam存在， 说明此处为游客可执行的操作， 调用cbObj.unauthorizeTodo 回调函数即可
  * 如果unauthorizeParam不存在， 说明此处为登陆用户才可执行的操作， 故先跳转登陆页
  *
  * @param {*} ajaxObj
  * @param {*} cbObj
  * @param {*} unauthorizeParam
  *
  */
 obj.ajax = function(ajaxObj, cbObj, unauthorizeParam) {
     judgeServerTimeStamp(); //判断是否过期

     //  判断是否过期，如果过期刷新token，否则正常调接口
     function judgeServerTimeStamp() {
         let passport = $.cookie('passport') ? JSON.parse($.cookie('passport')) : '';

         let nowTime = (new Date()).getTime();
         let oldTime = passport.serverTimestamp; // 登陆时种下
         let expiresInTime = passport.expiresIn * 1000;

         //  console.log(nowTime, oldTime, expiresInTime, nowTime - oldTime >= expiresInTime);

         if (oldTime && expiresInTime && (nowTime - oldTime >= expiresInTime)) {

             // 无同步执行接口
             if (!synLock && !utils.browserVersion.android) {
                 refreshToken();
             } else if (!synLock && utils.browserVersion.android) {
                 // 告知客户端 客户端会根据请求码做不同的事情 400代表刷新token 以后其他的代码 可扩展其他的操作
                 //  客户端token H5可用，反之不可
                 location.href = 'native://do.something/secret?req_code=400'; // 客户端刷新token
             }
             // 有同步执行接口时，请等待
             else {
                 //  console.log('===wait 500===');

                 let judgeIfSynLockFalse = setInterval(() => {
                     //  console.log('===try again===');

                     if (!synLock) {
                         judgeServerTimeStamp();
                         clearInterval(judgeIfSynLockFalse);

                         //  console.log('==clearInterval judgeIfSynLockFalse==');
                     }

                 }, 500);

             }

         } else {
             tryAjax();
         }
     }

     function refreshToken() {
         //  console.log('===refreshToken===');
         let passport = $.cookie('passport') ? JSON.parse($.cookie('passport')) : '';

         synLock = true; //锁住

         $.ajax({
             url: domainName.passport + '/v1/oauth2/token?grantType=refresh_token&refreshToken=' + passport.refreshToken,
             type: 'get',
             headers: {
                 'Qsc-Peduli-Token': passport.accessToken,
                 'Authorization': (passport.uid ? passport.uid : '') + ':' + (passport.signature ? passport.signature : ''), //uid:signature
                 'ExpiresIn': passport.expiresIn, //登录后,token的在多少秒之后失效
                 'TokenExpires': passport.tokenExpires, //token过期时间，服务端会用到
                 'Platform': utils.browserVersion.android ? 'Android-H5' : 'H5', //安卓登陆H5展示
                 'Accept-Language': navLang,
                 "Content-Type": "application/json; charset=utf-8",
                 'Version': appVersionName
             },
             success: function(res) {
                 if (res.code == 0) {
                     //存localstorage
                     $.removeCookie('passport', { path: '/' });
                     $.cookie('passport', JSON.stringify({
                         accessToken: res.data.accessToken,
                         expiresIn: res.data.expiresIn,
                         refreshToken: res.data.refreshToken,
                         tokenExpires: res.data.tokenExpires,
                         uid: res.data.uid,
                         signature: res.data.signature,
                         serverTimestamp: (new Date()).getTime() // 获取登陆成功时当前时间，判断token是否过期时使用
                     }), {
                         expires: 365,
                         path: '/',
                         domain: 'pedulisehat.id'
                     });

                     synLock = false; //释放
                     //  console.log('===synLock===', synLock);

                     tryAjax(); // 重试

                 } else if (res.code == 401) {
                     synLock = false; //释放

                     setTimeout(() => {
                         location.href = utils.browserVersion.android ?
                             'qsc://app.pedulisehat/go/login' :
                             ('https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?qf_redirect=' + encodeURIComponent(location.href));
                     }, 200);
                 } else {
                     synLock = false; //释放

                     utils.alertMessage(res.msg)
                 }
             },
             //  error: utils.handleFail

             error: function(res) {
                 console.log('ajax == error:', res);
                 console.log('ajaxObj = ', ajaxObj);

                 if (res && res.responseJSON && res.responseJSON.code == 401) {
                     utils.clearPassport(); //清空cookie

                     setTimeout(() => {
                         location.href = utils.browserVersion.android ?
                             'qsc://app.pedulisehat/go/login' :
                             ('https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?qf_redirect=' + encodeURIComponent(location.href));

                         //  location.href = utils.browserVersion.android ? 'qsc://app.pedulisehat/go/login' : '/login.html?qf_redirect=' + encodeURIComponent(location.href);
                     }, 200);
                 }
                 //  报错
                 else {
                     utils.handleFail
                 }
             }
         })
     }


     function tryAjax() {
        //  console.log('==tryAjax==');
        let passport = $.cookie('passport') ? JSON.parse($.cookie('passport')) : ''; // 获取最新cookie
        let deviceId = localStorage.getItem("deviceId") || null;

        // generate deviceId
        if (!deviceId) {
            localStorage.setItem("deviceId", utils.generateDeviceId());
            deviceId = localStorage.getItem("deviceId");
        }

         ajaxObj.headers = {
             'Qsc-Peduli-Token': passport.accessToken,
             'Authorization': (passport.uid ? passport.uid : '') + ':' + (passport.signature ? passport.signature : ''), //uid:signature
             'ExpiresIn': passport.expiresIn, //登录后,token的在多少秒之后失效
             'TokenExpires': passport.tokenExpires, //token过期时间，服务端会用到
             'Platform': utils.browserVersion.android ? 'Android-H5' : 'H5', //安卓登陆H5展示
             'Accept-Language': navLang,
             "Content-Type": "application/json; charset=utf-8",
             'Version': appVersionName,
             'DeviceID': deviceId
         };

         ajaxObj.xhrFields = {
             withCredentials: true
         };

         ajaxObj.crossDomain = true;

         ajaxObj.cache = false;

         ajaxObj.success = function(res) {
             //  console.log('ajax success = ', res);
             cbObj.success && cbObj.success(res);
         }


         ajaxObj.error = function(res) {
             //  console.log('ajax == error:', res);
             //  console.log('ajaxObj = ', ajaxObj);

             //  游客可执行的操作
             //  如果unauthorizeParam存在，说明此处为游客可执行的操作，调用cbObj.unauthorizeTodo 回调函数即可
             if (res && res.responseJSON && res.responseJSON.code == 401 && unauthorizeParam) {
                 cbObj.unauthorizeTodo && cbObj.unauthorizeTodo(res.responseJSON);
             }

             //  需要登陆才能执行的操作
             //  如果unauthorizeParam不存在, code == 401, 说明此处为登陆用户才可执行的操作，故先跳转登陆页
             else if (res && res.responseJSON && res.responseJSON.code == 401) {
                 utils.clearPassport(); //清空cookie

                 setTimeout(() => {
                     location.href = utils.browserVersion.android ?
                         'qsc://app.pedulisehat/go/login' :
                         ('https://' + utils.judgeDomain() + '.pedulisehat.id/login.html?qf_redirect=' + encodeURIComponent(location.href));
                 }, 200);
             }

             //  http=400 & code=4011,调用刷新token接口
             else if (res && res.responseJSON && res.responseJSON.code == 4011 && !utils.browserVersion.android) {
                 //  console.log('/v1/oauth2/token');
                 refreshToken(); //刷新token
             }

             //  http=400 & code=4011,调用刷新token接口
             else if (res && res.responseJSON && res.responseJSON.code == 4011 && utils.browserVersion.android) {
                 //  console.log('native://do.something/secret?req_code=400');
                 // 告知客户端 客户端会根据请求码做不同的事情 400代表刷新token 以后其他的代码 可扩展其他的操作
                 //  客户端token H5可用，反之不可
                 location.href = 'native://do.something/secret?req_code=400'; // 客户端刷新token
             }
             //  报错
             else {
                 cbObj.error && cbObj.error(res.responseJSON); // 正常报错
             }
         }

         $.ajax(ajaxObj);
     }

     /*
        android:
        req_code = 100: android login success and jump to donate
        req_code = 200: android login success and reload detail (是否主客态 / 是否关注， 故要刷新)
        req_code = 300 android login success and jump to confirmPage
        req_code = 400: android refresh token success
    */
     let flag = false;
     // token过期时，如果在安卓内部，则通知安卓刷新token，得到反馈后重试
     if (!flag) {
         window.refreshTokenHandle = function(req_code) {
             console.log('== window.refreshTokenHandle ==');

             if (req_code == 400) {
                 console.log('== window.refreshTokenHandle, code : 400 ==');

                 tryAjax();
             }
         }
     }
 }

 obj.ajaxFreshdesk = function(params, callback) {

    function make_base_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return "Basic " + hash;
    }

    $.ajax
    ({
      type: "GET",
      url: "https://pedulisehat.freshdesk.com/api/v2/solutions/articles/" + params.id,
      dataType: 'json',
      beforeSend: function (xhr){ 
        xhr.setRequestHeader('Authorization', make_base_auth('cjeane@pedulisehat.id', 'januari20')); 
      },
      success: function (res){
        callback(res)
      }
    });
 }

 export default obj;