import mainTpl from "../tpl/select-relationship.juicer";
import fastclick from "fastclick";
import domainName from "domainName"; //port domain name
import utils from "utils";
let obj = {};
let $UI = $("body");
obj.UI = $UI;
/* translation */
import selectRelationship from "selectRelationship";
import qscLang from "qscLang";
let selectLang = qscLang.init(selectRelationship);
/* translation */
let reqObj = utils.getRequestParams();
let project_id = reqObj["project_id"];
let short_link = reqObj["short_link"];
let from = reqObj["from"];
let form2 = reqObj["form2"]; // 第二个表单

var reqSelect = location.href;
/**
 * @param {返回按钮文字} res.JumpName
 */
obj.init = function(res) {
  res.selectLang = selectLang;
  // console.log('select:', res)
  res.domainName = domainName;

  $UI.prepend(mainTpl(res));

  fastclick.attach(document.body);

  obj.event(res);
};

obj.event = function(res) {
  // select bar
  if (reqSelect.indexOf("commonwealOrganization") != -1) {
    $(".btn.active").removeClass("active");
    $(".CommonwealOrganization.btn").addClass("active");
  } else if (reqSelect.indexOf("hospital") != -1) {
    $(".btn.active").removeClass("active");
    $(".Hospital.btn").addClass("active");
  }
  //  else if (reqSelect.indexOf('conjugalRelation') != -1) {
  //     $('.btn.active').removeClass('active')
  //     $('.ConjugalRelation.btn').addClass('active')
  // }
  else if (reqSelect.indexOf("directRelatives") != -1) {
    $(".btn.active").removeClass("active");
    $(".DirectRelatives.btn").addClass("active");
  } else if (reqSelect.indexOf("patientHimself") != -1) {
    $(".btn.active").removeClass("active");
    $(".PatientHimself.btn").addClass("active");
  }

  $("body").on("click", ".select", function(e) {
    $(".mask").css({
      display: "block"
    });
    $(".contentBox").css({
      bottom: "0"
    });
  });

  $("body").on("click", ".close", function(e) {
    $(".contentBox").css({
      bottom: "-444px"
    });
    $(".mask").css({
      display: "none"
    });
  });

  // click btn to link
  $("body").on("click", ".btn", function(e) {
    $(".btn").removeClass("active");
    $(this).addClass("active");

    setTimeout(() => {
      if ($(this).hasClass("PatientHimself")) {
        location.href =
          "/patientHimself.html?project_id=" +
          project_id +
          "&short_link=" +
          short_link +
          "&from=" +
          from +
          (form2 ? "&form2=" + form2 : "");
      }
      if ($(this).hasClass("DirectRelatives")) {
        location.href =
          "/directRelatives.html?project_id=" +
          project_id +
          "&short_link=" +
          short_link +
          "&from=" +
          from +
          (form2 ? "&form2=" + form2 : "");
      }
      // if ($(this).hasClass('ConjugalRelation')) {
      //     location.href = '/conjugalRelation.html?project_id=' + project_id
      // }
      if ($(this).hasClass("Hospital")) {
        location.href =
          "/hospital.html?project_id=" +
          project_id +
          "&short_link=" +
          short_link +
          "&from=" +
          from +
          (form2 ? "&form2=" + form2 : "");
      }
      if ($(this).hasClass("CommonwealOrganization")) {
        location.href =
          "/commonwealOrganization.html?project_id=" +
          project_id +
          "&short_link=" +
          short_link +
          "&from=" +
          from +
          (form2 ? "&form2=" + form2 : "");
      }
    }, 100);
  });
};

export default obj;
