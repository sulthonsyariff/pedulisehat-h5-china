import utils from 'utils'
import store from 'store'
import domainName from 'domainName' // port domain name
import AppHasInstalled from 'AppHasInstalled'

let generateQRcode;
let deeplink_redirect;
let reqObj = utils.getRequestParams();

let project_id = reqObj['project_id'] || '';
let group_id = reqObj['group_id'] || ''; //专题ID
let short_link = reqObj['short_link'] || ''; //专题短链 / 项目短链

let fromLogin = reqObj['fromLogin'] || '';
let from = reqObj['from'] || '';
let joinFrom = reqObj['joinFrom'] || '';

let obj = {};
let $UI = $('body');
obj.UI = $UI;

let user_id = $.cookie('passport') ? JSON.parse($.cookie('passport')).uid : '';
let accessToken = $.cookie('passport') ? JSON.parse($.cookie('passport')).accessToken : '';
let gopayToken = reqObj['gopayToken'] || '';

/*****
 * 众筹 + 互助：yay账户（基金会账户） ，保险：pt公司账户
 *
gopay: pay_platform = 2, bank_code = 1 *
sinamas: pay_platform = 3, bank_code = 2 *
faspay: pay_platform = 1, bank_code = **
OVO: pay_platform = 4, bank_code = 4 *
dana: pay_platform = 5, bank_code = 5 *
ShopeePay pay_platform = 6, bank_code = 6 / 7(APP)

 * 众筹(zakat计算器支付)： 成功 / commonDonateSuccess.html 失败 / commonDonateFailure.html
 * 互助：                成功 / paymentSucceed.html 失败 / paymentFailed.html
 *
 */

// 跳转链接
let OrderTransactionDetailUrl, goPayQRcodeUrl, OVOUrl, shopeePayQRcodeUrl, gtryPaySucs;

// 判断是 gtry  / asuransi / www
let _locationHostNameJudge = location.hostname.indexOf('gtry') != -1 ? 'gtry' :
    (location.hostname.indexOf('asuransi') != -1 ? 'asuransi' :
        '');

//utils.gtryLocationOrigin()  https://gtry-qa.pedulisehat.id
// 获取域名gtry  asuransi
let _locationOrigin = (_locationHostNameJudge == 'gtry') ? utils.gtryLocationOrigin() :
    ((_locationHostNameJudge == 'asuransi') ? utils.asuransiLocationOrigin() : '');


export default function(objData, res) {
    utils.showLoading();

    // ！！！注意res返回的数据不同的业务返回的数据不一样，注意获取不到报错情况，故放在业务条件语句中
    // gtry 支付提交跳转
    if (_locationHostNameJudge == 'gtry') {
        // gopay
        goPayQRcodeUrl = _locationOrigin +
            '/goPay.html?trade_id=' + res.data.trade_id +
            '&amount=' + res.data.money +
            '&gtryFrom=' + res.gtryFrom +
            (joinFrom ? ('&joinFrom=' + joinFrom) : ''); //joinFrom:pay 页面传入

        // sinamas / faspay
        OrderTransactionDetailUrl = _locationOrigin +
            '/OrderTransactionDetail.html?channel=' + objData.payment_channel +
            '&gtryFrom=' + res.gtryFrom +
            (joinFrom ? ('&joinFrom=' + joinFrom) : ''); //joinFrom:pay 页面传入

        // OVO
        OVOUrl = _locationOrigin +
            '/OVO.html?order_id=' + res.data.order_id +
            '&amount=' + res.data.amount +
            '&gtryFrom=' + res.gtryFrom +
            '&trade_id=' + res.data.trade_id +
            (joinFrom ? ('&joinFrom=' + joinFrom) : ''); //joinFrom:pay 页面传入

        // shopeePay QR
        shopeePayQRcodeUrl = _locationOrigin +
            '/shopeePayQRcode.html?order_id=' + res.data.order_id +
            '&trade_id=' + res.data.trade_id +
            '&gtryFrom=' + res.gtryFrom +
            (joinFrom ? ('&joinFrom=' + joinFrom) : '');

        // 特殊情况！！！：gtry 互助支付成功页:活的金额为0的时候会有这种跳转成功页的情况
        gtryPaySucs = _locationOrigin +
            '/paymentSucceed.html?order_id=' + res.data.order_id +
            (joinFrom ? ('&joinFrom=' + joinFrom) : ''); //joinFrom:pay 页面传入
    }
    // asuransi 支付提交跳转
    else if (_locationHostNameJudge == 'asuransi') {
        res.data = res && res.data && res.data.third_platform_data && JSON.parse(res.data.third_platform_data);
        objData.payment_channel = objData.payment_channel_id;

        console.log('****res.data***', res.data)
        console.log('***res.data.order_id***', res.data.order_id)

        // {request_id: "1596772469524464176", debug_msg: "success", store_name: "Peduli Sehat", qr_content: "00020101021226580016ID.CO.SHOPEE.WWW01189360091800…51234562310520142216330533696277890703Web6304694B", qr_url: "https:/ / api.uat.wallet.airpay.co.id / v3 / merchant - ho… oad ? qr = NzYSk52jDe9XlbjgQxYrWWO5ykNJcBlh85EJ1VKCFt "}

        // gopay ！！！asuransi账号有问题没上线，代码没测试后台不能直接打开！！！
        goPayQRcodeUrl = _locationOrigin +
            '/goPay.html?trade_id=' + res.data.trade_id +
            '&order_id=' + res.data.order_id +
            '&amount=' + res.data.money;

        // sinamas ！！！asuransi没有账号，代码没测试后台不能直接打开！！！
        // faspay
        OrderTransactionDetailUrl = _locationOrigin +
            '/OrderTransactionDetail.html?channel=' + objData.payment_channel +
            '&order_id=' + res.data.order_id +
            '&trade_id=' + res.data.trade_id;

        // OVO！！！asuransi没有对账单没上线，代码没测试后台不能直接打开！！！
        OVOUrl = _locationOrigin +
            '/OVO.html?order_id=' + res.data.order_id +
            '&amount=' + res.data.amount +
            '&trade_id=' + res.data.trade_id;

        // dana ！！！asuransi没有账号，代码没测试后台不能直接打开！！！

        // shopeePay QR
        shopeePayQRcodeUrl = _locationOrigin +
            '/shopeePayQRcode.html?order_id=' + res.data.order_id +
            '&trade_id=' + res.data.trade_id;

    }
    // 众筹 支付提交跳转 ：注意众筹还有专题支付方式 group_id !!!
    else {
        //zakat计算器需要,zakat计算器获取项目ID时特殊处理
        if (res.fromCalculator) {
            project_id = objData.project_id;
            short_link = objData.short_link;
        }

        // gopay
        goPayQRcodeUrl = _locationOrigin +
            '/goPayQRcode.html?' +
            (group_id ? ('group_id=' + group_id) : ('project_id=' + project_id)) +
            '&short_link=' + short_link +
            '&order_id=' + res.data.order_id +
            '&amount=' + res.data.gross_amount +
            (res.fromCalculator ? '&fromCalculator=zakatCalculator' : '') // 页面判断返回zakat计算器页面时需要

        // sinamas / faspay
        OrderTransactionDetailUrl = _locationOrigin +
            '/OrderTransactionDetail.html?' +
            (group_id ? ('group_id=' + group_id) : ('project_id=' + project_id)) +
            '&short_link=' + short_link +
            '&channel=' + objData.payment_channel +
            (res.fromCalculator ? '&fromCalculator=zakatCalculator' : '') // 页面判断返回zakat计算器页面时需要？

        // OVO
        OVOUrl = _locationOrigin +
            '/OVO.html?' +
            (group_id ? ('group_id=' + group_id) : ('project_id=' + project_id)) +
            '&short_link=' + short_link +
            '&order_id=' + res.data.order_id +
            '&amount=' + (objData.total_money || objData.money) +
            '&phone=' + objData.phone +
            (from == 'OVOXPeduliSehat' ? '&from=OVOXPeduliSehat' : '') + //OVO 合作需要 （应该不要了吧）
            (res.fromCalculator ? '&fromCalculator=zakatCalculator' : '') // 页面判断返回zakat计算器页面时需要

        // shopeePay QR
        shopeePayQRcodeUrl = _locationOrigin +
            '/shopeePayQRcode.html?' +
            (group_id ? ('group_id=' + group_id) : ('project_id=' + project_id)) +
            '&short_link=' + short_link +
            '&order_id=' + res.data.order_id +
            (res.fromCalculator ? '&fromCalculator=zakatCalculator' : '') // 页面判断返回zakat计算器页面时需要
    }



    // gopay: bank_code = 1
    if (objData.payment_channel == '1') {
        storeGopay(res);
        setTimeout(() => {
            // gopay linking
            if (gopayToken == 1) {
                // pending
                if (res.data.status_code == "201") {
                    location.href = res.data.actions[1].url;
                } else {
                    location.href = '/commonDonateSuccess.html?order_id=' + res.data.order_id +
                    '&short_link=' + short_link +
                    // '&terminal=' + res.data.terminal +
                    (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                    (group_id ? ('&group_id=' + group_id) :
                        ('&project_id=' + project_id));
                }
            } else {
                location.href = goPayQRcodeUrl;
            }
        }, 300);
    }
    // sinamas: bank_code = 2
    else if (objData.payment_channel == '2') {
        storeSinamas(res);
        setTimeout(() => {
            location.href = OrderTransactionDetailUrl
        }, 300);
    }
    // OVO:bank_code = 4
    else if (objData.payment_channel == '4') {
        console.log('ovo = ', res, res.data.order_id, objData);
        setTimeout(() => {
            location.href = OVOUrl;
        }, 300);
    }
    // dana:bank_code = 5 ！！！asuransi没有账号，代码没测试后台不能直接打开！！
    else if (objData.payment_channel == '5') {
        // storeDana(res);
        console.log('dana = ', res);
        if (user_id) {
            setTimeout(() => {
                location.href = domainName.trade +
                    '/v1/dana/oauth?Qsc-Peduli-Token=' + accessToken +
                    '&checkout_url=' + encodeURI(res.data.checkout_url);
            }, 300);
        } else {
            setTimeout(() => {
                location.href = res.data.checkout_url
            }, 300);
        }
    }
    /****
     * ShopeePay QR: bank_code = 6
     "data": {
         "request_id": "1596435591600568953",
        "debug_msg": "success",
        "store_name": "Peduli Sehat",
         qr_content: "" //二维码的字符串
         qr_url: "" //二维码地址
        "order_id": "14221633053369627081",
        "trade_id": "14221633053369627081"
    },
        *
        */
    else if (objData.payment_channel == '6') {
        storeShopee(res);
        setTimeout(() => {
            location.href = shopeePayQRcodeUrl
        }, 300);
    }
    /*****
     * ShopeePay App: bank_code = 7
    "data": {
        "request_id": "1596433893054854741",
        "debug_msg": "success",
        "redirect_url_app": "shopeeid://main?apprl=%2Frn%2FT",//app跳转地址，暂时只用了这个
        "redirect_url_http": "https://wsa.uat.wallet.a"
    },
    */
    else if (objData.payment_channel == '7') {
        console.log('ShopeePay APP = ', res);
        // 跳转APP支付
        setTimeout(() => {
            utils.hideLoading();
            // location.href = res.data.redirect_url_app;
            AppHasInstalled.AppHasInstalled(res.data.redirect_url_http);
        }, 300);
    }

    // faspay:bank_code = *
    else if (objData.payment_channel == '707' ||
        objData.payment_channel == '801' ||
        objData.payment_channel == '708' ||
        objData.payment_channel == '802' ||
        objData.payment_channel == '408' ||
        objData.payment_channel == '402' ||
        objData.payment_channel == '702' ||
        objData.payment_channel == '800') {
        // console.log('VAorderPage', objData.payment_channel)
        // console.log('VAorderPage===', JSON.stringify(res.data))
        storeFaspay(res);
        setTimeout(() => {
            location.href = OrderTransactionDetailUrl
        }, 300);
    }
    // 金额为零：互助做活动的时候会有，直接跳转成功页（
    else if (_locationHostNameJudge == 'gtry' && !objData.paid_up_money && res.data.order_id) {
        setTimeout(() => {
            location.href = gtryPaySucs;
        }, 300);
    }
    // 其他银行
    else {
        location.href = res.data.redirect_url;
    }




}

function storeGopay(res) {
    if (gopayToken != 1) {
        generateQRcode = res.data.actions[0].url
        deeplink_redirect = res.data.actions[1].url
        store.set('goPay', {
            generate_qr_code: generateQRcode,
            deeplink_redirect: deeplink_redirect
        });
    }
}

function storeSinamas(res) {
    SourceID = res.data.SourceID
    IssueDate = res.data.IssueDate
    NoRef = res.data.NoRef
    VirtualAccountNumber = res.data.VirtualAccountNumber
    Amount = res.data.Amount
    trade_id = res.data.trade_id || ''
    order_id = res.data.order_id || ''
    let VAorder = {
        SourceID: SourceID,
        IssueDate: IssueDate,
        NoRef: NoRef,
        VirtualAccountNumber: VirtualAccountNumber,
        Amount: Amount,
        trade_id: trade_id,
        order_id: order_id,
        from: 'sinarmas'
    }
    sessionStorage.setItem('VA', JSON.stringify(VAorder));
}

function storeFaspay(res) {
    SourceID = res.data.merchant
    IssueDate = res.data.pay_expired
    NoRef = res.data.bill_no
    VirtualAccountNumber = res.data.trx_id
    Amount = res.data.money
    trade_id = res.data.trade_id || ''
    order_id = res.data.order_id || ''
    let VAorder = {
        SourceID: SourceID,
        IssueDate: IssueDate,
        NoRef: NoRef,
        VirtualAccountNumber: VirtualAccountNumber,
        Amount: Amount,
        trade_id: trade_id,
        order_id: order_id,
        from: 'faspay'
    }
    sessionStorage.setItem('VA', JSON.stringify(VAorder));
}

/**
 debug_msg: "success"
 order_id: "14221633053369627081"
 qr_content: "" //二维码的字符串
 qr_url: "" //二维码地址
 request_id: "1596435591600568953"
 store_name: "Peduli Sehat"
 trade_id: "14221633053369627081"
 */
function storeShopee(res) {
    store.set('storeShopee', {
        qr_content: res.data.qr_content,
        qr_url: res.data.qr_url,
    });
}

/*
 android: visit login or login
{“
    user_name:"123"
    phone:'123'
}
当俩个字段对应的数值均为空的时候 表示当前用户登录成功;
当俩个字段均不为空的时候 表示其为游客 对应的字段就是游客信息;
req_code = 500 监听app登陆成功或者游客模式下， 返回捐赠页， H5自动跳转到相应支付页面（ gopay / fastpay） req_code = 500

 1:group logined pay
 2:logined pay
 3:group unlogin pay
 4:unlogin pay
*/
window.appHandle = function(requestCode, jsonBean) {
    utils.showLoading();

    let objData = store.get('donateMoney');

    // 众筹项目/专题捐款：分为游客捐款和登录用户捐款
    // 互助和保险不会进入这个逻辑里面，因为下单未登录时会先去登录
    // visit login:
    if (_locationHostNameJudge == '' && requestCode == 500 && jsonBean.user_name && jsonBean.phone) {
        objData.user_name = jsonBean.user_name;
        objData.phone = jsonBean.phone;

        group_id ?
            $UI.trigger('appHandle_createPayment', [objData, 3]) : // 3:group unlogin pay
            $UI.trigger('appHandle_createPayment', [objData, 4]) // 4:unlogin pay
    }
    // login
    else if (requestCode == 500 && !jsonBean.user_name && !jsonBean.phone) {
        // 众筹项目/专题捐款
        if (_locationHostNameJudge == '') {
            group_id ?
                $UI.trigger('appHandle_createPayment', [objData, 1]) : // 1:group logined pay
                $UI.trigger('appHandle_createPayment', [objData, 2]) // 2:logined pay
        }
        // gtry / asuransi
        else {
            $UI.trigger('appHandle_createPayment', objData)
        }


    } else if (requestCode == 600) {
        location.reload();
    }
}