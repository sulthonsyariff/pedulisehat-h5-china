let obj = {};
let $UI = $('body');
obj.UI = $UI;

/**
 * 修改计算公式中的Fee + 修改银行渠道下的tips费率提示
 * paramObj:{}
 *
 * paramObj.bankChannelFee
 * paramObj.bankExpenseType
 * paramObj.bankCode
 *
 * paramObj.toalPaymentChannelFee //总平台费
 * paramObj.preTotal 预支付金额
 * paramObj.total //支付金额
 *
 * expense_type =2 意思是按比例
 * expense_type = 1 是按单笔
 */
obj.init = function(paramObj) {
    // GO-Pay：GO-PAY按照总金额*2%计算 expense_type =2
    // OVO：  OVO 按照总金额*1.5%计算 expense_type =2
    if (paramObj.bankExpenseType == 2) {
        paramObj.toalPaymentChannelFee = Math.ceil(paramObj.preTotal / (1 - paramObj.bankChannelFee)) - paramObj.preTotal;
        paramObj.total = Math.ceil(paramObj.preTotal / (1 - paramObj.bankChannelFee));

    }
    // VA和online bank:按照每笔订单计算
    else if (paramObj.bankExpenseType == 1) {
        paramObj.toalPaymentChannelFee = paramObj.bankChannelFee;
        paramObj.total = paramObj.preTotal + parseInt(paramObj.bankChannelFee);
    }

    return paramObj;
}

export default obj;