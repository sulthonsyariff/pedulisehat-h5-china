import ajaxProxy from 'ajaxProxy'
import 'jq_cookie' //ajax cookie
import domainName from 'domainName' // port domain name

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/******
 * 互助和保险:trade_id
 * 众筹: order_id / trade_id (shopeepay: reference_id)
 */
//  shopeepay 订单查询接口
obj.getOrderState = function(o) {
    var url = domainName.trade + '/v1/shopee/query?reference_id=' + o.param.order_id;

    ajaxProxy.ajax({
        type: 'get',
        url: url,
    }, o)
};

export default obj;