// 公共库
import commonNav from 'commonNav'
import commonFooter from 'commonFooter'
import mainTpl from '../tpl/main.juicer'
import fastclick from 'fastclick'
import googleAnalytics from 'google.analytics'
import utils from 'utils'
import store from 'store'
// // import sensorsActive from 'sensorsActive'

/* translation */
import goPayQRcode from 'goPayQRcode'
import qscLang from 'qscLang'
let lang = qscLang.init(goPayQRcode);
/* translation */

let [obj, $UI] = [{}, $('body')];
let project_id = utils.getRequestParams().project_id;
let group_id = utils.getRequestParams().group_id;
let order_id = utils.getRequestParams().order_id;
var short_link = utils.getRequestParams().short_link;

let CACHED_KEY = 'storeShopee';
let generate_qr_code = store.get(CACHED_KEY).qr_content;
let qr_url = store.get(CACHED_KEY).qr_url;

obj.UI = $UI;
// 初始化
obj.init = function(res) {
    res.JumpName = 'ShopeePay QR';

    if (!utils.browserVersion.android) {
        // res.JumpName = lang.JumpName;
        commonNav.init(res);
    } else {
        location.href = 'native://do.something/setTitle?title=' + 'ShopeePay QR'
    }



    res.lang = lang;
    res.getLanguage = utils.getLanguage();
    res.order_id = order_id;
    // res.generate_qr_code = generate_qr_code;
    res.qr_url = qr_url;
    $UI.append(mainTpl(res)); //主模版
    commonFooter.init(res);

    utils.hideLoading();
    fastclick.attach(document.body);

    // google anaytics
    let param = {};
    googleAnalytics.sendPageView(param);
    // // sensorsActive.init();

};

export default obj;