import view from './view'
import model from './model'
import 'loading'
import '../less/main.less'
// 引入依赖
import utils from 'utils'

let order_id = utils.getRequestParams().order_id || '';
let trade_id = utils.getRequestParams().trade_id || '';
let group_id = utils.getRequestParams().group_id || '';
let short_link = utils.getRequestParams().short_link || '';
let project_id = utils.getRequestParams().project_id || '';
let appVersionCode = parseInt(utils.getAndroidArgsByNodeName(navigator.userAgent, 'appVersionCode'))
let joinFrom = utils.getRequestParams().joinFrom || '';

judgeOrderState();

let timer = setInterval(() => {
    judgeOrderState();
}, 3000);

view.init({});

/*
 * judge order state
 PaymentSuccessful = 1 支付成功
PaymentProcessing = 5 进行中
PaymentFailed = 6 失败

 PaymentNotFound = 2 未找到订单（ 一般是超时未支付）
 PaymentRefunded = 3 退款
 PaymentVoid = 4 反转
 410 二维码过期

 */
function judgeOrderState() {
    model.getOrderState({
        param: {
            //!!!参数reference_id(众筹的order_id 其他业务是trade_id)
            order_id: (location.hostname.indexOf('gtry') == -1 && location.hostname.indexOf('asuransi') == -1) ?
                order_id : trade_id
        },
        success: function(res) {
            if (res.code == 0) {

                // 1成功 3 4 6 失败
                if (res.data && res.data.payment_status == 1) {
                    // gtry
                    if (location.hostname.indexOf('gtry') != -1) {
                        location.href = '/paymentSucceed.html' +
                            (joinFrom ? ('?joinFrom=' + joinFrom) : '');
                    }
                    // asuransi
                    else if (location.hostname.indexOf('asuransi') != -1) {
                        location.href = '/paymentSucceed.html?order_id=' + order_id;
                    }
                    // 众筹（捐款安卓有自己的支付成功页）
                    else {
                        // 安卓项目
                        if (appVersionCode > 128 && project_id) {
                            location.href = 'qsc://app.pedulisehat/go/donate_success?project_id=' +
                                encodeURIComponent(project_id) +
                                '&order_id=' + encodeURIComponent(order_id);

                        }
                        // 安卓专题
                        else if (appVersionCode >= 202 && group_id && short_link) {
                            location.href = 'qsc://app.pedulisehat/go/donate_success?order_id=' +
                                encodeURIComponent(order_id) +
                                '&short_link=' + encodeURIComponent(short_link);

                        }
                        // H5项目+专题
                        else {
                            location.href = '/commonDonateSuccess.html?order_id=' + order_id +
                                '&short_link=' + short_link +
                                // '&terminal=' + res.data.terminal +
                                (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                                (group_id ? ('&group_id=' + group_id) :
                                    ('&project_id=' + project_id))
                        }
                    }

                }
                // 1成功 3 4 6 失败
                else if (res.data && res.data.payment_status == 3 ||
                    res.data.payment_status == 4 ||
                    res.data.payment_status == 6) {

                    // gtry
                    if (location.hostname.indexOf('gtry') != -1) {
                        location.href = '/paymentFailed.html?order_id=' + order_id +
                            (joinFrom ? ('&joinFrom=' + joinFrom) : '');

                    }
                    // asuransi
                    else if (location.hostname.indexOf('asuransi') != -1) {
                        location.href = '/paymentFailed.html?order_id=' + order_id +
                            '&trade_id=' + trade_id;
                    }
                    // 众筹
                    else {
                        location.href = '/commonDonateFailure.html?order_id=' + order_id +
                            '&short_link=' + short_link +
                            // '&terminal=' + res.data.terminal +
                            (res.data.terminal ? '&terminal=' + res.data.terminal : '') + //不知道这个干嘛的？
                            (group_id ? ('&group_id=' + group_id) :
                                ('&project_id=' + project_id))
                    }
                }
                // 410 二维码过期
                else if (res.data && res.data.payment_status == 410) {
                    clearInterval(timer);

                    $('.expired').css({
                        'display': 'block'
                    })
                }

            } else {
                utils.alertMessage(res.msg)
            }
        },
        error: utils.handleFail
    })
}