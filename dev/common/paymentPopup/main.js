import fastclick from 'fastclick'
import tpl from './main.juicer'
import './main.less'
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat' //divide the amount by decimal point

/* translation */
import paymentPopup from 'paymentPopupLang'
import qscLang from 'qscLang'
let paymentPopupLang = qscLang.init(paymentPopup);
/* translation */
let resAll;
let [obj, $UI] = [{}, $('body')];

fastclick.attach(document.body);

obj.init = function(rs) {
    console.log('===paymentPopup rs===', rs);
    resAll = rs;

    $('.paymentPopup-wrapper').remove();
    rs.paymentPopupLang = paymentPopupLang;
    $UI.append(tpl(rs));
}

$UI.on('click', '.paymentPopup-wrapper .close', function() {
    $('.paymentPopup-wrapper').css('display', 'none');
})

// payment channel
$UI.on('click', '.payment_channel', function() {
    $('.alert-bank').css('display', 'block');
})

$UI.on('click', '.pay', function() {
    if (resAll.data._access == 'asuransi') {
        utils.showLoading();
        $UI.trigger('payIns');
    } else {
        if ($('.channel.selected').length) {
            utils.showLoading();
    
            $UI.trigger('payIns');
        }
    }
})


export default obj;