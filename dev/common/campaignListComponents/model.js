import ajaxProxy from 'ajaxProxy'
import domainName from 'domainName' // port domain name
import 'jq_cookie' //ajax cookie

let obj = {};
let isLocal = location.href.indexOf("pedulisehat.id") == -1;

/****
首页列表： 验证通过的 + 筹资中 + 所有类型
搜索结果页： 验证通过的 + 筹资中 + 所有类型
项目列表页: 验证通过的 + 筹资中 + 所有类型
Donate列表页: 验证通过的 + 筹资中 + 一般项目 & bulk项目
Zakat + Qurban列表: 验证通过的 + 筹资中 + zakat项目 & Qurban项目
VA推荐列表： 验证通过的 + 筹资中 + 所有类型 + 除本项目以外的10个筹款中项目 + （按最新排序）
捐款成功推荐列表： 验证通过的 + 筹资中 + 所有类型 + 除本项目以外的10个筹款中项目 + （按最新排序）
分享成功推荐列表: 验证通过的 + 筹资中 + 所有类型 + 除本项目以外的10个筹款中项目 + （按最新排序）
媒体合作伙伴页： 后台设置（ 筹款中 & 已结束的、 验证通过 & 未验证[通过]）
专题页： 后台设置（ 筹款中 & 已结束的、 验证通过 & 未验证[通过]、 默认zakat项目不能加入专题页（ 但是在qa上以前添加过））

state: 2 隐藏   8 失败   16 冻结   512 成功   8192 筹资中
(结束： 512 / 8 筹款成功或者失败)
*/


let v1_public = domainName.project + '/v1/public?page=';
let v1_project_ovo_group = domainName.project + '/v1/project_ovo_group?page=';
let v1_partner_projects = domainName.project + '/v1/partner_projects';
let v1_group_projects = domainName.project + '/v1/group_projects';

let allCategoryId = '12,13,14'; //所有类型 12 regular   13 zakat   14 bulk   15 qurban
// let allCategoryId = '12,13,14,15'; //所有类型 12 regular   13 zakat   14 bulk   15 qurban

// home： 验证通过的+筹资中+所有类型
obj.getListData_home = function(o) {
    let url = v1_public + 1 + '&state=8192&category_id=' + allCategoryId + '&limit=3';
    ajaxF(url, o);
};

// searchResaults :Reconmmend 验证通过的+筹资中+所有类型
obj.getListReconmmendData_searchResaults = function(o) {
    let url = v1_public + o.param.page + '&state=8192&category_id=' + allCategoryId;
    ajaxF(url, o);
};

// searchResaults: 验证通过的+筹资中+所有类型
obj.getListData_searchResaults = function(o) {
    let url = v1_public + o.param.page + '&state=8192&category_id=' + allCategoryId + '&search=' + o.param.search;
    ajaxF(url, o);
};

// OrderTransactionDetail VA推荐列表：验证通过的+筹资中+所有类型+除本项目以外的10个筹款中项目
obj.getListData_OrderTransactionDetail = function(o) {
    let url = v1_public + 1 + '&state=8192&category_id=' + allCategoryId + '&limit=10&newest=1&project_id=' + o.param.project_id; // project_id N string 项目ID 该项目不返回
    ajaxF(url, o);
};

// donate success :捐款成功推荐列表：验证通过的+筹资中+所有类型+除本项目以外的10个筹款中项目
obj.getListData_commonDonateSuccess = function(o) {
    let url = v1_public + 1 + '&state=8192&category_id=' + allCategoryId + '&limit=10&newest=1&project_id=' + o.param.project_id; // project_id N string 项目ID 该项目不返回
    ajaxF(url, o);
};

// donate share success:分享成功推荐列表: 验证通过的+筹资中+所有类型+除本项目以外的10个筹款中项目
obj.getListData_donateShareSuccess = function(o) {
    let url = v1_public + 1 + '&state=8192&category_id=' + allCategoryId + '&limit=10&newest=1&short_link=' + o.param.short_link; // short_link N string 项目ID 该项目不返回
    ajaxF(url, o);
};

// donate share success : OVO分享成功推荐列表: 验证通过的+筹资中+所有类型+除本项目以外的10个筹款中项目
obj.getListData_donateShareSuccess_OVO = function(o) {
    let url = v1_project_ovo_group + 1 + '&state=8192&category_id=' + allCategoryId + '&limit=10&short_link=' + o.param.short_link; // short_link N string 项目ID 该项目不返回
    ajaxF(url, o);
};

/****
 * 项目列表页:          验证通过的 + 筹资中 + 所有类型
 * Donate列表页:       验证通过的 + 筹资中 + 一般项目 & bulk项目
 * Zakat + Qurban列表: 验证通过的 + 筹资中 + zakat项目 & Qurban项目
 */
obj.getListData_donateList_campaignList_zakatList = function(o) {
    let isCampaignList = location.href.indexOf('campaignList') != -1;
    let isZakatList = location.href.indexOf('zakatList') != -1;
    let isDonateList = location.href.indexOf('donateList') != -1;
    let url;

    let category_ids = isCampaignList && allCategoryId ||
        isZakatList && "13,15" ||
        isDonateList && "12,14";

    url = v1_public +
        o.param.page + '&state=8192&category_id=' + category_ids +
        (o.param.campaigner ? '&usertype=' + o.param.campaigner : '') +
        (o.param.newest ? ('&newest=' + o.param.newest) :
        (o.param.hottest ? '&hottest=' + o.param.hottest : '')
        )

    // if (o.param.newest) {
    //     url = v1_public + o.param.page + '&state=8192&category_id=' + category_ids + '&newest=' + o.param.newest;
    // } else if (o.param.hottest) {
    //     url = v1_public + o.param.page + '&state=8192&category_id=' + category_ids + '&hottest=' + o.param.hottest;
    // } else {
    //     url = v1_public + o.param.page + '&state=8192&category_id=' + category_ids;
    // }

    ajaxF(url, o);
};

// mediaPartner媒体伙伴页：后台设置筛选条件，前端不做处理
obj.getListData_mediaPartner = function(o) {
    let url = v1_partner_projects + '?user_id=' + o.param.user_id + '&page=' + o.param.page + '&category_id=12,13';
    ajaxF(url, o);
};

// 专题：后台设置筛选条件，前端不做处理 https://qa.pedulisehat.id/group/gopay
obj.getListData_specialSubject = function(o) {
    let url = v1_group_projects + '?short_link=' + o.params.short_link;
    ajaxF(url, o);
};

function ajaxF(url, o) {
    // if (isLocal) {
    //     url = '/mock/project_lists_' + o.param.page + '.json';
    //     url = '/mock/v1_group_projects.json'
    //     url = '/mock/project_lists_' + 1 + '.json';
    // }

    ajaxProxy.ajax({
        type: 'get',
        url: url
    }, o)
}

export default obj;