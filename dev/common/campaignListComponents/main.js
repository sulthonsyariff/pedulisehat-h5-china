import "./less/main.less"
import tpl from "./tpl/main.juicer"
import utils from 'utils'
import changeMoneyFormat from 'changeMoneyFormat'
import domainName from 'domainName' // port domain name

/* translation */
import home from 'home'
import qscLang from 'qscLang'
let lang = qscLang.init(home);
/* translation */

let obj = {};
let reqObj = utils.getRequestParams();
let short_link = reqObj['short_link'] || '';

/****
state　
2 隐藏
 8 失败
 16 冻结
 512 成功
 8192 筹资中

 结束： 512 / 8 筹款成功或者失败
 */
obj.init = function(rs) {
    rs.lang = lang;
    rs.domainName = domainName;
    rs._short_link = short_link;

    for (let i = 0; i < rs.data.length; i++) {
        //跳转详情地址
        rs.data[i].detailHref = utils.browserVersion.android ?
            'qsc://app.pedulisehat/go/campaign_details?project_id=' + rs.data[i].project_id + '&clear_top=true' :
            '/' + rs.data[i].short_link;

        //时间
        if (rs.data[i].closed_at) {
            rs.data[i].timeLeft = utils.timeLeft(rs.data[i].closed_at);
        }

        //   图片
        rs.data[i].cover = JSON.parse(rs.data[i].cover);

        rs.data[i].rightUrl = utils.imageChoose4_3(rs.data[i].cover)
        rs.data[i].avatar = utils.imageChoose(rs.data[i].avatar)

        // 格式化金额
        rs.data[i].target_amount = changeMoneyFormat.moneyFormat(rs.data[i].target_amount);
        rs.data[i].current_amount = changeMoneyFormat.moneyFormat(rs.data[i].current_amount);

        // 企业用户
        rs.data[i].corner_mark = rs.data[i].corner_mark ? JSON.parse(rs.data[i].corner_mark) : '';

        // 添加结束印戳
        rs.data[i].getLanguage = utils.getLanguage();
    }

    if (rs.$dom) {
        $(rs.$dom).append(tpl(rs));
    } else {
        $('.Campaigns_List').append(tpl(rs));
    }

    // if row's number more than 2,show the overflow class
    // for (let i = 0; i < rs.data.length; i++) {
    //     if ($(".inner_name" + '_' + i).height() > 32) {
    //         $('.campaign_name' + '_' + i).addClass('overflow')
    //     }
    // }

    $(".Campaigns_List .user-name").each(function() {
        let _parent = $(this).parents('.avatar-name');
        let _parentW = $(_parent).width();
        let _childrenW = $(_parent).find('.enterprise-wrap').width() +
            $(_parent).find('.verified_wrap').width() +
            $(_parent).find('.avatar-wrap').width();

        if ($(this).width() > _parentW - _childrenW - 16) {
            $(this).css({
                'width': _parentW - _childrenW - 16,
                'overflow': 'hidden',
                'white-space': 'nowrap',
                'text-overflow': 'ellipsis',
            })
        }
    });
}

export default obj;