import mainTpl from './main.juicer'
import './main.less'
import fastclick from 'fastclick'
import domainName from 'domainName'; //port domain name
import store from 'store'
import appDownload from 'appDownload'

/* translation */
import common from 'common'
import qscLang from 'qscLang'
import utils from 'utils';
let commonLang = qscLang.init(common);
/* translation */
let obj = {};
let $UI = $('body');
obj.UI = $UI;


/* storage key */
// let APP_DOWNLOAD_KEY = 'app_download';
// let app_download = sessionStorage.getItem(APP_DOWNLOAD_KEY) ? sessionStorage.getItem(APP_DOWNLOAD_KEY) : {};

/**
 * @param {控制按钮文字} res.JumpName
 *  @param {控制返回链接} res.commonNavGoToWhere
 */
obj.init = function(res) {
    console.log('commonNav:', res)
    res.domainName = domainName;
    res.commonLang = commonLang;

    if (!utils.browserVersion.android) {
        $UI.prepend(mainTpl(res));
    }

    appDownload.init($('.app-download-wrapper'));

    fastclick.attach(document.body);
    obj.event(res);

    // bottom-slide-wrapper
    if ($('.bottom-slide-wrapper .list').length == 3) {
        $('.bottom-slide-wrapper .list').css('width', '32%')
    }
};

obj.event = function(res) {
    // goBack
    $('body').on('click', '#goBack', function(e) {
        sessionStorage.setItem('OldHome', 6)

        if (res.commonNavGoToWhere) {
            location.href = res.commonNavGoToWhere;
        } else {
            window.history.go(-1);
        }
    });
}

export default obj;