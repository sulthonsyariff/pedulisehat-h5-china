import './main.less'
import tpl from './main.juicer'
import fastclick from 'fastclick'
import domainName from 'domainName' // port domain name
import utils from 'utils'

/* translation */
import qscLang from 'qscLang'
import common from 'common'
let commonLang = qscLang.init(common);
/* translation */

let obj = {};
let $UI = $('body');

/* storage key */
let APP_DOWNLOAD_KEY = 'app_download';
let app_download = sessionStorage.getItem(APP_DOWNLOAD_KEY) ? sessionStorage.getItem(APP_DOWNLOAD_KEY) : {};

fastclick.attach(document.body);

obj.init = function(element) {
    let rs = {};
    rs.commonLang = commonLang;
    rs.domainName = domainName;

    $(element).append(tpl(rs)); // 带close的模版

    //  APP download topbar
    if (app_download != 'false') {
        // 非安卓APP + 安卓手机 : 展示下载UI
        if (utils.browserVersion.androidEnd && !utils.browserVersion.androids) {
            $('.app-download').css({
                'display': 'block'
            })
        }

        $('body').on('click', '.appDownload-close', function(e) {
            sessionStorage.setItem(APP_DOWNLOAD_KEY, 'false')

            $('.app-download').css({
                'display': 'none'
            })
        });
    }

    // 注意顺序问题，$('.page-inner')还没加载完的话无法使用css控制样式，故采取添加classname的方式！！！
    if ($('.app-download').css('display') === 'block') {
        $('body').addClass('add-app-download')
    }
}

export default obj;

// APP download
$UI.on('click', '.right-wrapper', function(e) {
    location.href = 'https://play.google.com/store/apps/details?id=com.qschou.pedulisehat.android';
    // location.href = 'https://github.com/walkermanx/pedulisehat_apk/releases/latest/download/PeduliSehat.apk';
    // location.href = 'market://details?id=com.qschou.pedulisehat.android'
    // location.href = 'http://oss.pgyer.com/725258b0a95a2b608a6d26093382e403.apk?auth_key=1586419762-36df84a32b0888c72c5a2be156c993e5-0-e5399805304cd2d9602b8be9b7789765&response-content-disposition=attachment%3B+filename%3D%255BGooglePlay%255DprodOnlineRelease-1.2.2%255B238%255D-0404124221.apk'
    // location.href = 'http://oss.pgyer.com/725258b0a95a2b608a6d26093382e403.apk?auth_key=1589862778-9a48f8502e9dd584b39548a59205edb9-0-0cb1b74429e5ebf18ed122904d0759f6&response-content-disposition=attachment%3B+filename%3D%255BGooglePlay%255DprodOnlineRelease-1.2.2%255B238%255D-0404124221.apk'
});

$UI.on('click', '.appDownload-close', function(e) {
    // store.set('app_download', 'false')
    $('.app-download').css({
        'display': 'none'
    })
    $('body').removeClass('add-app-download')
});