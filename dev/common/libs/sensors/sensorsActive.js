// const sensors = require('sa-sdk-javascript/sensorsdata.min.js');
// sensors.init({
//   server_url: '...',
//   heatmap: {
//      //是否开启点击图，默认 default 表示开启，自动采集 $WebClick 事件，可以设置 'not_collect' 表示关闭
//      //需要 JSSDK 版本号大于 1.7
//      clickmap:'default',
//      //是否开启触达注意力图，默认 default 表示开启，自动采集 $WebStay 事件，可以设置 'not_collect' 表示关闭
//      //需要 JSSDK 版本号大于 1.9.1
//      scroll_notice_map:'not_collect'
//   }
// //   .......
// });
// sensors.login(user_id);
// sensors.quick('autoTrack');
import domainName from 'domainName' // port domain name
import utils from 'utils'

let obj = {};


let _server_url;
if (utils.judgeDomain() == 'qa') {
    _server_url = 'https://sensors.pedulisehat.id/sa?project=default'
} else if (utils.judgeDomain() == 'pre') {
    _server_url = 'https://sensors.pedulisehat.id/sa?project=default'
} else {
    _server_url = 'https://sensors.pedulisehat.id/sa?project=production'
}


obj.init = function() {
    (function(para) {
        var p = para.sdk_url,
            n = para.name,
            w = window,
            d = document,
            s = 'script',
            x = null,
            y = null;

        if (typeof(w['sensorsDataAnalytic201505']) !== 'undefined') {
            return false;
        }
        w['sensorsDataAnalytic201505'] = n;
        // console.log('sensors-active', p, w['sensorsDataAnalytic201505'])

        w[n] = w[n] || function(a) {
            return function() {
                (w[n]._q = w[n]._q || []).push([a, arguments]);
            }
        };
        var ifs = ['track', 'quick', 'register', 'registerPage', 'registerOnce', 'trackSignup', 'trackAbtest',
            'setProfile', 'setOnceProfile', 'appendProfile', 'incrementProfile', 'deleteProfile',
            'unsetProfile', 'identify', 'login', 'logout', 'trackLink', 'clearAllRegister', 'getAppStatus'
        ];
        for (var i = 0; i < ifs.length; i++) {
            w[n][ifs[i]] = w[n].call(null, ifs[i]);
        }
        if (!w[n]._t) {
            x = d.createElement(s), y = d.getElementsByTagName(s)[0];
            x.async = 1;
            x.src = p;
            x.setAttribute('charset', 'UTF-8');
            w[n].para = para;
            y.parentNode.insertBefore(x, y);
        }
    })({
        sdk_url: domainName.static + '/sensors/sensorsdata.min.js',
        heatmap_url: domainName.static + '/sensors/heatmap.min.js',
        // sdk_url: '../sensors/sensorsdata.min.js',
        // heatmap_url: '../sensors/heatmap.min.js',
        name: 'sensors',
        //配置打通 App 与 H5 的参数
        use_app_track: true,
        server_url: _server_url,
        heatmap: {
            clickmap: 'default',
        },
        cross_subdomain: true,
    });
    sensors.quick('autoTrack');
}


export default obj;