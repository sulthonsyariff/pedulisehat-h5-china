#!/bin/bash
rootPath=$PWD

# GTRY-Pedulisehat-Id
# index
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/home && ${rootPath}/node_modules/.bin/webpack
# accident
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/accident && ${rootPath}/node_modules/.bin/webpack
# criticalDiseases
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/criticalDiseases && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/membershipCard && ${rootPath}/node_modules/.bin/webpack
# cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/referralBonus && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/activeMembership && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/applyForAidGuidence && ${rootPath}/node_modules/.bin/webpack

#--- apply for aid ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/applyForAid/accidentInfo && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/applyForAid/diseaseInfo && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/applyForAid/patientInfo && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/applyForAid/payeeInfo && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/applyForAid/applyFinish && ${rootPath}/node_modules/.bin/webpack

cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/transactions && ${rootPath}/node_modules/.bin/webpack

#--- apply for aid ---#
#--- publicity ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/publicity/publicityDetail && ${rootPath}/node_modules/.bin/webpack
#--- publicity ---#
#--- go-pay ---#
#1
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/GO-PAY/goPay && ${rootPath}/node_modules/.bin/webpack
#2
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/GO-PAY/goPayQRcode && ${rootPath}/node_modules/.bin/webpack
#--- go-pay ---#

#--- OVO ---#
#1
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/OVO/OVO && ${rootPath}/node_modules/.bin/webpack
#2
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/OVO/OVOTransaction && ${rootPath}/node_modules/.bin/webpack
#--- OVO ---#

#--- pay ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/pay && ${rootPath}/node_modules/.bin/webpack
#--- pay ---#

#--- payment-state ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/paymentFailed && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/paymentSucceed && ${rootPath}/node_modules/.bin/webpack
#--- payment-state ---#

#--- top-up ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/topUp && ${rootPath}/node_modules/.bin/webpack
#--- top-up ---#

#--- VA ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/VirtualAccount/OrderTransactionDetail && ${rootPath}/node_modules/.bin/webpack
#--- VA ---#
cd ${rootPath}/dev/src/GTRY-Pedulisehat-Id/join/shopeePay/QRcode && ${rootPath}/node_modules/.bin/webpack

