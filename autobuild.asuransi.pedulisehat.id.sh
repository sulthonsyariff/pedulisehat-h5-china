#!/bin/bash
rootPath=$PWD

cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/home && ${rootPath}/node_modules/.bin/webpack

cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/sijicorona/detail && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/sijicorona/benifitInfo && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/sijicorona/holderInfo && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/sijicorona/insuredInfo && ${rootPath}/node_modules/.bin/webpack

cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/submitSuccess && webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/submitFailed && webpack

cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/GO-PAY/goPay && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/GO-PAY/goPayQRcode && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/OVO/OVO && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/OVO/OVOTransaction && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/paymentFailed && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/paymentSucceed && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/VirtualAccount/OrderTransactionDetail && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/join/shopeePay/QRcode && ${rootPath}/node_modules/.bin/webpack

cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/myInsuranceList && ${rootPath}/node_modules/.bin/webpack
cd ${rootPath}/dev/src/Asuransi-Pedulisehat-Id/policyData && ${rootPath}/node_modules/.bin/webpack
