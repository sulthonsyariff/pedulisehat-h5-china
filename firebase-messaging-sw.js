importScripts('https://www.gstatic.com/firebasejs/8.7.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.7.0/firebase-messaging.js');

var firebaseConfig = {
  apiKey: "AIzaSyD4dOacjSeBSsfWRyN9j8BwJ5q9BzmNRGA",
  authDomain: "pedulisehat-h5.firebaseapp.com",
  projectId: "pedulisehat-h5",
  storageBucket: "pedulisehat-h5.appspot.com",
  messagingSenderId: "1050487659451",
  appId: "1:1050487659451:web:72e644f0bbc3d20a69c29e",
  measurementId: "G-QFF5TCYQRP"
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

var click_action;
messaging.setBackgroundMessageHandler(function (payload) {
    console.log(payload);

    click_action = payload.data.click_action;

    var notificationOption = {
        body: payload.data.body,
        image: payload.data.icon,
        icon: payload.data.icon,
        data: {
          click_action: payload.data.click_action
        }
    };
    return self.registration.showNotification(payload.data.title, notificationOption);
});

self.addEventListener('notificationclick', function(event) {
  event.notification.close();

  event.waitUntil(
    clients.openWindow(click_action)
  );
});