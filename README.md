`印尼版`

```
npm install --save-dev
npm install webpack -g
npm install webpack-cli -g
```

```
webpack打包方式：

1.单页面打包：
执行：webpack / webpack -w

2.所有页面打包：

QA（测试环境）：
执行：./autobuild.qa.sh
或者：npm run autobuildQA

prodeuction（生产环境）：
执行：./autobuild.sh
或者：npm run autobuild

3.webpack.dll.config.js 第三方依赖库单独打包注意：一般没有更改，不用重新打包，若有更改，则后续需执行所有页面文件的打包）
执行：webpack --config webpack.dll.config.js
（
```

```
全局打包
./autobuilod.sh
npm run autobuild
```

```
注意：

html中src，使用绝对路径
less中URL，可以使用相对或者绝对路径

```

```
rs.JumpName = titleLang.detail;
rs.commonNavGoToWhere = '';
```

```
 * payee_type:
 1 患者验证 patientHimself :1
 2 直系亲属验证 directRelatives: 2
 3 夫妻验证 conjugalRelation: 3
 4 公益机构验证 commonwealOrganization: 4
 5 医院验证 hospital: 5
```

```
Status Code: 401 Unauthorized 接口error，不需要跳转login登陆页，
1.用户信息接口（首页nav）：/v1/user

Status Code: 401 Unauthorized 接口error，需要跳转login登陆页
1.用户信息接口（首页nav）：/v1/user
2.判断是否登陆接口 (发起)：/v1/login/verify
3.所有需要登陆才能访问的接口（登陆访问页），包括token失效；
```

```
payee_type:
1 患者验证
2 直系亲属验证
3 夫妻验证
4 工艺机构验证
5 医院验证

```

```
platform: 'h5|android|ios',
auth_type: 101 facebook
auth_type: 100 message
```

```
项目STATE状态:
state 8192 筹资中 active(绿色字体)
state 8 失败 failed
state 512 成功 succeed
state 16 冻结 freezed

结束： 512 / 8 筹款成功或者失败
```

```
我捐款的列表页state状态：
state 1 未支付
state 2 已支付    Rp120.000
state 3 提现
state 4 退款      Refunded
state 5 返还      Returned
state 100 取消订单
```

```
stopped:0 项目没有关闭
stopped:1 项目已关闭
```

```
TargetTypefacebook  = 1 // facebook
TargetTypeWhatsApp  = 2 // whatsapp
TargetTypeTwitter   = 3 // twitter
TargetTypeLink      = 4 // link
TargetTypeLine      = 5 // line
TargetTypeInstagram = 6 // instagram
TargetTypeYoutube   = 7 // youtube
```

```
钱包页交易流水相关：
交易类型：
trade_type : 1 Income
trade_type : 2 Withdraw
trade_type : 3 Refund
trade_type : 4 Return
交易状态：
trade_state : 1 Preparing
trade_state : 2 Processing
trade_state : 3 Finished
trade_state : 4 Failed
```

```
Status Code
0 Unprocessed
1 In Process
2 Payment Success
4 Payment Reserval
5 No bills found
8 Payment Cancelled
9 Unknown
```

```
project confirm
 1 Relative
 2 Friend
 3 Neighbor
 4 Clleague
 5 Teacher
 6 Schoolmate
 7 Volunteer
 8 Other
```

```
安卓协议：

*****应用市场：market://details?id=
*****关闭：native://close
*****修改安卓头部标题：native://do.something/setTitle?title=
*****返回首页：qsc://app.pedulisehat/go/main?index=0
*****返回带第三方登陆的页面：qsc://app.pedulisehat/go/login?req_code=100
***** native://intent?url=xxx&errorInfo=XXX app内调用第三方app,注意参数encode处理
***** qsc://app.pedulisehat/go/donate_prompt 跳转安卓捐赠登陆页
***** qsc://app.pedulisehat/go/campaign_details?project_id=' + project_id +'&clear_top=true 跳转安卓详情页
*****分享："native://share/webpage" +
    "?url=" + encodeURIComponent('') +
    "&title=" + encodeURIComponent('') +
    "&description" + encodeURIComponent('') +
    "&image=" + encodeURIComponent('') +
    "&hashtag=" + encodeURIComponent('') +
    "&quote=" + encodeURIComponent('');

req_code=100 监听app登陆成功后回到详情页,H5自动跳转捐赠页 req_code = 100
req_code=200 监听app登陆成功后回到详情页,H5自动刷新详情页(是否主客态/是否关注，故要刷新) req_code = 200
req_code=300 监听app登陆成功后回到详情页,H5自动跳转证实表单页 req_code = 300
req_code=400 监听app登陆成功后回到证实列表页,H5自动跳转证实表单页 req_code = 400
req_code=500 监听app登陆成功或者游客模式下，返回捐赠页，H5自动跳转到相应支付页面（gopay/fastpay） req_code = 500
<!-- 注意 req_code!=30-->

blankforapp.html为安卓H5调试页面，非业务页面
window.appHandle(requestCode,jsonBean);
webview 中 UA 字段会追加一个字段 <oldUA>;language=[ in | en |  xx ]
```

```
后端详情页分享模版：
（Facebook/Twitter/Whatsapp分享，仅能抓取JS加载前meta标签内的内容，故后端做‘分享链接’的模版渲染，让Facebook/Twitter/Whatsapp爬虫抓取后端‘分享链接’中的meta详情内容）
```

```
线上
https://project.pedulisehat.id/short_link
```

```
涉及到手机号位数及+62的页面(6):

登陆页
手机号绑定页

提款人验证：（4）
Patient himself
Direct relatives
Conjugal relationship
Commonweal organization

OVO支付
```

```
 * gopay:    pay_platform = 2, bank_code = 1
 * sinamas:  pay_platform = 3, bank_code = 2
 * faspay:   pay_platform = 1, bank_code = *
 * OVO:      pay_platform = 4, bank_code = 4
 * dana:     pay_platform = 5, bank_code = 5
 * ShopeePay pay_platform = 6, bank_code = 6 / 7(APP)
```
