cd $PWD/dev/src/Asuransi-Pedulisehat-Id/sijicorona/detail && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/sijicorona/benifitInfo && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/sijicorona/holderInfo && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/sijicorona/insuredInfo && webpack --config webpack.config.qa.js

cd $PWD/dev/src/Asuransi-Pedulisehat-Id/submitSuccess && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/submitFailed && webpack --config webpack.config.qa.js

cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/GO-PAY/goPay && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/GO-PAY/goPayQRcode && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/OVO/OVO && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/OVO/OVOTransaction && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/paymentFailed && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/paymentSucceed && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/VirtualAccount/OrderTransactionDetail && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/join/shopeePay/QRcode && webpack --config webpack.config.qa.js

cd $PWD/dev/src/Asuransi-Pedulisehat-Id/myInsuranceList && webpack --config webpack.config.qa.js
cd $PWD/dev/src/Asuransi-Pedulisehat-Id/policyData && webpack --config webpack.config.qa.js