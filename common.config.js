module.exports = function(paramObj, domain) {
    const webpack = require('webpack');
    const path = require('path');
    const HtmlWebpackPlugin = require('html-webpack-plugin');
    const CleanWebpackPlugin = require('clean-webpack-plugin');
    const ExtractTextPlugin = require('extract-text-webpack-plugin'); //separate .css files
    const HtmlIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');
    const alias = require("./config/alias"); //别名
    const TerserPlugin = require('terser-webpack-plugin'); //用terser-webpack-plugin替换掉uglifyjs-webpack-plugin
    const VueLoaderPlugin = require("vue-loader/lib/plugin");

    // const cssnano = require('cssnano');
    // const MiniCssExtractPlugin = require('mini-css-extract-plugin');
    // const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

    // 为了浏览器的兼容性，有时候我们必须加入-webkit,-ms,-o,-moz这些前缀
    let postcssConfigPath = path.resolve(__dirname, './postcss.config.js');
    let postcssLoader = {
        loader: 'postcss-loader',
        options: {
            config: {
                path: postcssConfigPath
            }
        }
    };

    let commonDir = path.resolve(__dirname);
    let htmlTitle = paramObj.htmlTitle;

    //区分环境，决定哪些插件加不加载
    let env = (
        domain == '//static-qa.pedulisehat.id/' ?
        'qa' :
        (domain == '//static-dev.pedulisehat.id/' ?
            'dev' :
            (domain == '//static-pre.pedulisehat.id/' ?
                'pre' :
                'www')
        )
    );

    // share
    let htmlOgUrl = paramObj.htmlOgUrl || 'https://www.pedulisehat.id/';
    let htmlOgTitle = paramObj.htmlOgTitle || 'pedulisehat.id - Platform Donasi Kesehatan no. 1 Indonesia'; //pedulisehat.id - Platform Donasi Kesehatan no. 1 Indonesia
    let htmlOgDescription = paramObj.htmlOgDescription || 'Sekarang kita bisa melakukan donasi kesehatan kapanpun dimanapun, dengan cepat dan transparan membantu pasien yang tepat'; //Sekarang kita bisa melakukan donasi kesehatan kapanpun dimanapun, dengan cepat dan transparan membantu pasien yang tepat
    let htmlOgImage = paramObj.htmlOgImage || (domain + 'img/ico/logo.png');

    let htmlFileURL = paramObj.htmlFileURL;

    let uglify = paramObj.uglify;
    let hash = paramObj.hash;
    let htmlTpl = paramObj.htmlTpl || 'baseTpl.html'; //默认是baseTpl， VUE是baseTplVue.html
    let appDir = paramObj.appDir;
    let cssDir = paramObj.appDir.replace(/^js/, 'css');
    let imgDir = paramObj.appDir.replace(/^js/, 'img');
    // let mockDir = paramObj.appDir.replace(/^js/, 'mock');
    let baseTplUrl = path.join(commonDir, htmlTpl);
    let staticFileDir = path.resolve(commonDir, './release');
    let hashFileName = hash ? '[name]_' + hash : '[name]_[hash:8]';
    let isVue = paramObj.isVue;

    let obj = {};

    // ie9和一些低版本的高级浏览器对es6新语法并不支持:babel-polyfill解决
    // obj.entry = ["babel-polyfill", "./js/control"];
    if (isVue) {
        obj.entry = ["babel-polyfill", "./index"]; // 设置入口文件：VUE项目的入口文件为index.js
    } else {
        obj.entry = ["babel-polyfill", "./js/control"];
    }

    obj.output = {
        filename: hashFileName + '.min.js',
        path: path.join(staticFileDir, appDir),
        publicPath: domain + path.join(appDir, '/')
    };

    obj.mode = paramObj.mode; // 设置mode

    obj.stats = 'minimal'; //'errors-warnings'/'minimal'/'errors-only'

    obj.resolve = {
        // 当你reuire时，不需要加上以下扩展名
        extensions: ['.js', '.juicer', ".vue", '.json'],
        // resolve.alias 可以配置 webpack 模块解析的别名，对于比较深的解析路径，可以对其配置 alias. 可以提升 webpack 的构建速度。
        // 第三方依赖,alias别名配置:(也可以在给定对象的键后的末尾添加 $，以表示精准匹配)
        alias: alias
    };

    obj.module = {
        rules: [{
            test: /\.juicer$/,
            loader: ['html-withimg-loader', 'juicer-loader'],
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    'css-loader',
                    postcssLoader
                ]
            })
        }, {
            test: /\.less$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    'css-loader',
                    postcssLoader,
                    'less-loader'
                ]
            })
        }, {
            test: /\.(png|jpe?g|gif|svg|ttf)$/,
            // loader: 'url?name=[path][name].[ext]',
            use: {
                loader: 'url-loader',
                query: {
                    limit: 10000, //是把小于1000B的文件打成Base64的格式，写入JS
                    name: path.join(path.relative(appDir, imgDir), hashFileName + '.[ext]')
                }
            }
        }, {
            test: /\.(js|es|es6)$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['es2015', {
                            modules: false,
                            loose: true
                        }]
                    ],
                    cacheDirectory: true
                }
            }],
            exclude: /node_modules/
        }, {
            test: /\.vue$/,
            use: [{
                loader: "vue-loader",
            }, ],
        }]
    };

    // js压缩优化 用terser-webpack-plugin替换掉uglifyjs-webpack-plugin解决uglifyjs不支持es6语法问题
    obj.optimization = {
        'minimize': true,
        minimizer: [
            // 允许你通过提供一个或多个定制过的 TerserPlugin 实例， 覆盖默认压缩工具(minimizer)。
            // js压缩优化 用terser-webpack-plugin替换掉uglifyjs-webpack-plugin解决uglifyjs不支持es6语法问题
            new TerserPlugin({
                cache: true, // 开启缓存
                parallel: true, // 支持多进程

                extractComments: true, //启用/禁用提取注释。//如果原始文件名为foo.js，则注释将存储到foo.js.LICENSE.txt中。
                test: /\.js(\?.*)?$/i,

                // 本地不可以，发布线上就可以: if it is prod env then you can remove all these
                // sourceMap: true, // Must be set to true if using source-maps in production
                terserOptions: {
                    compress: {
                        drop_console: true,
                    },
                },
            }),
        ],
    };

    let pluginsArr = [
        new VueLoaderPlugin(),

        new CleanWebpackPlugin([appDir, cssDir], {
            // new CleanWebpackPlugin([appDir, cssDir, mockDir], {
            root: staticFileDir,
            verbose: true
        }),

        //separate html files
        new HtmlWebpackPlugin({
            title: htmlTitle,
            linkFavicon: 'https:' + domain + 'img/ico/favicon.ico', //favicon icon
            linkManifest: 'https:' + domain + 'pwa/manifest.json', //manifest.json
            linkManifestIcon: 'https:' + domain + 'pwa/logo-lite.png', //manifest logo
            // 分享
            htmlOgUrl: htmlOgUrl,
            htmlOgTitle: htmlOgTitle,
            htmlOgDescription: htmlOgDescription,
            htmlOgImage: htmlOgImage,

            filename: path.join(staticFileDir, htmlFileURL),
            template: baseTplUrl,
            minify: {
                collapseWhitespace: (env == 'qa' || env == 'pre' || env == 'dev') ? false : true, //去除空格
            },
            cache: true, //只有在内容变化时才会生成新的html,开启 Cache 选项，有利用提高构建性能。
            removeComments: true, //是否压缩时 去除注释
            uglify: true,
            hash: true,
            isVue: isVue, //是否是VUE项目

        }),

        //separate .css files
        // extract-text-webpack-plugin目前版本不支持webpack4,使用extract-text-webpack-plugin的最新的beta版:npm install extract-text-webpack-plugin@next
        new ExtractTextPlugin({
            filename: path.join(path.relative(appDir, cssDir), hashFileName + '.min.css')
        }),

        // 自动加载模块，而不必到处 import 或 require？
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),

        // 打包第三方依赖（webpack.dll.config.js文件中配置打包的文件为第三方依赖，不会变更）
        // 此处引入后，项目文件中若引入第三方库不会重复打包，而是使用这里打包的文件
        new webpack.DllReferencePlugin({
            context: __dirname,
            manifest: require('./release/lib/libs/vendors-manifest.json')
        }),

        // 将新的（例如.js）资源文件添加到html中 扩展html插件的功能
        new HtmlIncludeAssetsPlugin({
            assets: ['../../lib/libs/vendors.dll_V20180820.js'], // 添加的资源相对html的路径
            append: false // false 在其他资源的之前添加 true 在其他资源之后添加
        }),

        // js压缩优化 用terser-webpack-plugin替换掉uglifyjs-webpack-plugin解决uglifyjs不支持es6语法问题
        // new TerserPlugin({
        //     cache: true, // 开启缓存
        //     // parallel: true, // 支持多进程

        //     extractComments: true, //启用/禁用提取注释。//如果原始文件名为foo.js，则注释将存储到foo.js.LICENSE.txt中。
        //     test: /\.js(\?.*)?$/i,

        //     // sourceMap: true, // Must be set to true if using source-maps in production
        //     terserOptions: {
        //         compress: {
        //             drop_console: true,
        //         },
        //     },
        // }),

        //压缩提取出的css，并解决ExtractTextPlugin分离出的js重复问题(多个文件引入同一css文件)
        // new OptimizeCSSPlugin(),
        // new OptimizeCSSAssetsPlugin({
        //     cssProcessor: cssnano,
        //     // cssProcessorOptions: options, //?
        //     canPrint: false,
        // }),
    ];

    obj.plugins = pluginsArr;

    // 文件过大报错提示，提高报错门槛
    obj.performance = {
        hints: 'warning',
        //入口起点的最大体积 整数类型（以字节为单位）
        maxEntrypointSize: 50000000,
        //生成文件的最大体积 整数类型（以字节为单位 300k）
        maxAssetSize: 30000000,
        //只给出 js 文件的性能提示
        assetFilter: function(assetFilename) {
            return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
        }
    }

    return obj;
};