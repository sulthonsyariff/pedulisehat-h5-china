let domain = '//static-pre.pedulisehat.id/'; //qa
let webpackConfig = require("./common.config");

module.exports = function(paramObj) {
    return webpackConfig(paramObj, domain);
}