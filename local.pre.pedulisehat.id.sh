#-----  首页 -----#
cd $PWD/dev/src/home && webpack --config webpack.config.qa.js
#-----  首页 -----#


#-----  专题页 -----#
cd $PWD/dev/src/specialSubject && webpack --config webpack.config.qa.js
#-----  专题页 -----#

#-----  covidCount -----#
cd $PWD/dev/src/covidCount && webpack --config webpack.config.qa.js
#-----  covidCount -----#

#-----  noCaptchaForApp -----#
cd $PWD/dev/src/noCaptchaForApp && webpack --config webpack.config.qa.js
#-----  noCaptchaForApp -----#

# 列表页
cd $PWD/dev/src/campaignList && webpack --config webpack.config.qa.campaignList.js
cd $PWD/dev/src/campaignList && webpack --config webpack.config.qa.donateList.js
cd $PWD/dev/src/campaignList && webpack --config webpack.config.qa.zakatList.js

cd $PWD/dev/src/OVOXPeduliSehat && webpack --config webpack.config.qa.js

# 搜索结果页
cd $PWD/dev/src/searchResaults && webpack --config webpack.config.qa.js

#-----  new landing page -----#
# cd $PWD/dev/src/newHp2 && webpack --config webpack.config.qa.js
#-----  new landing page -----#
#-----  首页:banner -----#
# cd $PWD/dev/src/homeBanner/NGO && webpack --config webpack.config.qa.js
#-----  首页:banner -----#

#----- 个人中心 -----#
# aboutUs
cd $PWD/dev/src/personalCenter/aboutUs && webpack --config webpack.config.qa.js
# faq
cd $PWD/dev/src/personalCenter/faq && webpack --config webpack.config.qa.js

# 1.我关注的列表
cd $PWD/dev/src/personalCenter/followedCampaigns && webpack --config webpack.config.qa.js
# 2.我支持的列表
# cd $PWD/dev/src/personalCenter/supportedCampaigns && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donationHistory && webpack --config webpack.config.qa.js
# 3.我发起的列表
cd $PWD/dev/src/personalCenter/myCampaigns && webpack --config webpack.config.qa.js
# 勋章页
cd $PWD/dev/src/personalCenter/myTrophies && webpack --config webpack.config.qa.js

# bindEmail
cd $PWD/dev/src/personalCenter/setting/bindEmail && webpack --config webpack.config.qa.js

cd $PWD/dev/src/personalCenter/setting/myAccount && webpack --config webpack.config.qa.js
# 4.设置页--修改昵称
cd $PWD/dev/src/personalCenter/setting/modifyName && webpack --config webpack.config.qa.js
# 5.设置页--修改头像
cd $PWD/dev/src/personalCenter/setting/setting && webpack --config webpack.config.qa.js
# 6.设置页--绑定
# cd $PWD/dev/src/personalCenter/setting/bindingAccount && webpack --config webpack.config.qa.js
# 7.设置页--确认重新绑定手机号
# cd $PWD/dev/src/personalCenter/setting/modifyPhone && webpack --config webpack.config.qa.js
# 8.设置页--绑定手机号
# cd $PWD/dev/src/personalCenter/setting/sendCode && webpack --config webpack.config.qa.js
# 9.设置页--切换语言
cd $PWD/dev/src/personalCenter/changeLanguage && webpack --config webpack.config.qa.js
# 10.用户等级
cd $PWD/dev/src/personalCenter/userLevel && webpack --config webpack.config.qa.js

#----- 设置页--修改密码 -----#
# 1.找回密码
cd $PWD/dev/src/personalCenter/setting/modifyPassword/findBackPassword && webpack --config webpack.config.qa.js
# 2.修改密码
cd $PWD/dev/src/personalCenter/setting/modifyPassword/modifyPassword && webpack --config webpack.config.qa.js
# 3.短信验证
cd $PWD/dev/src/personalCenter/setting/modifyPassword/phoneNumVerification && webpack --config webpack.config.qa.js
# 4.重置密码
cd $PWD/dev/src/personalCenter/setting/modifyPassword/resetPassword && webpack --config webpack.config.qa.js
# 5.设置密码
cd $PWD/dev/src/personalCenter/setting/modifyPassword/setPassword && webpack --config webpack.config.qa.js
#----- 设置页--修改密码 -----#

#----- 消息 -----#
# 1.消息聚合列表
cd $PWD/dev/src/personalCenter/messages/MessagesCategory && webpack --config webpack.config.qa.js
# 2.消息设置
cd $PWD/dev/src/personalCenter/messages/MessagesSetting && webpack --config webpack.config.qa.js
# 3.通知类型消息详情
cd $PWD/dev/src/personalCenter/messages/NotificationMsgDetail && webpack --config webpack.config.qa.js
# 4.通知类型消息列表
cd $PWD/dev/src/personalCenter/messages/NotificationsMsg && webpack --config webpack.config.qa.js
# 5.动态更新消息详情
cd $PWD/dev/src/personalCenter/messages/UpdatesMsg && webpack --config webpack.config.qa.js
# 6.动态更新消息列表
cd $PWD/dev/src/personalCenter/messages/UpdatesMsgDetail && webpack --config webpack.config.qa.js
#----- 消息 -----#

#----- 个人中心 -----#


#-----  登陆 -----#
cd $PWD/dev/src/login && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/loginDonate && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/loginPhone && webpack --config webpack.config.qa.js
#-----  登陆 -----#


#-----  发起 -----#
# 发起页
cd $PWD/dev/src/initiate/initiatePage && webpack --config webpack.config.qa.js
cd $PWD/dev/src/initiate/initiateChoose && webpack --config webpack.config.qa.js
cd $PWD/dev/src/initiate/initiateZakat && webpack --config webpack.config.qa.js
# 发起成功页
cd $PWD/dev/src/initiate/initiateSuccessPage && webpack --config webpack.config.qa.js
#-----  发起 -----#


#-----  详情 -----#
# 详情页
cd $PWD/dev/src/detail && webpack --config webpack.config.qa.js
#-----  详情 -----#


#-----  详情页相关 -----#
# 1.修改金额
cd $PWD/dev/src/detailRelated/modifyGoal && webpack --config webpack.config.qa.js
# 2.修改故事
cd $PWD/dev/src/detailRelated/modifyStory && webpack --config webpack.config.qa.js
# 3.项目更新
cd $PWD/dev/src/detailRelated/update && webpack --config webpack.config.qa.js
# 4.项目更新列表
cd $PWD/dev/src/detailRelated/updateList && webpack --config webpack.config.qa.js
# 5.患者验证
cd $PWD/dev/src/detailRelated/verification/patientVerification && webpack --config webpack.config.qa.js
# 6.增加项目结束时间
cd $PWD/dev/src/detailRelated/extension && webpack --config webpack.config.qa.js
# 7.支持者排行
cd $PWD/dev/src/detailRelated/supporterRanking && webpack --config webpack.config.qa.js

#-- 提款人相关  --#
# 6.患者本人提款验证
cd $PWD/dev/src/detailRelated/verification/payeeVerification/patientHimself && webpack --config webpack.config.qa.js
# 7.直系亲属提款验证
cd $PWD/dev/src/detailRelated/verification/payeeVerification/directRelatives && webpack --config webpack.config.qa.js
# 8.夫妻关系验证
# cd $PWD/dev/src/detailRelated/verification/payeeVerification/conjugalRelation && webpack --config webpack.config.qa.js
# 9.医院验证
cd $PWD/dev/src/detailRelated/verification/payeeVerification/hospital && webpack --config webpack.config.qa.js
# 10.公益组织验证
cd $PWD/dev/src/detailRelated/verification/payeeVerification/commonwealOrganization && webpack --config webpack.config.qa.js
#-- 提款人相关  --#

# 11.验证成功
cd $PWD/dev/src/detailRelated/verification/verificationSuccess && webpack --config webpack.config.qa.js

#--  钱包相关  --#
# 12.我的钱包
cd $PWD/dev/src/detailRelated/wallet/myWallet && webpack --config webpack.config.qa.js
# 13.提现
cd $PWD/dev/src/detailRelated/wallet/withdraw && webpack --config webpack.config.qa.js
# 14.提现成功
cd $PWD/dev/src/detailRelated/wallet/withdrawSuccess && webpack --config webpack.config.qa.js
#--  钱包相关  --#

# 15.confirmation list
cd $PWD/dev/src/detailRelated/confirmationList && webpack --config webpack.config.qa.js
# 16.confirm page
cd $PWD/dev/src/detailRelated/confirm/confirmPage && webpack --config webpack.config.qa.js
# 17.confirma success page
cd $PWD/dev/src/detailRelated/confirm/confirmSuccessPage && webpack --config webpack.config.qa.js

#-----  详情页相关 -----#



#-----  捐赠 -----#
# 1.捐赠--捐赠页
cd $PWD/dev/src/donate/donateMoney && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/donateMoneyTest && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/confirmOrder && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/confirmOrderTest && webpack --config webpack.config.qa.js
# 2.捐赠--捐赠状态页
# cd $PWD/dev/src/donate/donateSuccess && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/commonDonateStatus/commonDonateFailure && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/commonDonateStatus/commonDonateFailureGopay && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/commonDonateStatus/commonDonateSuccess && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/donate/donateSuccess && webpack --config webpack.config.qa.jsm
# cd $PWD/dev/src/donate/donateSuccess && webpack --config webpack.config.qa.js
# 3.捐赠--捐赠test
# cd $PWD/dev/src/donate/donateTest && webpack --config webpack.config.qa.js
# 4.捐赠--捐赠分享成功页
cd $PWD/dev/src/donate/donateShareSuccess && webpack --config webpack.config.qa.js
# 5.网上银行捐赠结果页
# cd $PWD/dev/src/donate/onlineBankPayResults && webpack --config webpack.config.qa.js
#----- GO PAY -----#
# 1.GO-PAY--go pay
# cd $PWD/dev/src/donate/goPay/goPay && webpack --config webpack.config.qa.js

# 2.GO-PAY--go pay error
# cd $PWD/dev/src/donate/goPay/goPayError && webpack --config webpack.config.qa.js
# 3.GO-PAY--go pay finish
# cd $PWD/dev/src/donate/goPay/goPayFinish && webpack --config webpack.config.qa.js
# 4.GO-PAY--go pay QR code
cd $PWD/dev/src/donate/goPay/goPayQRcode && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/shopeePay/QRcode && webpack --config webpack.config.qa.js
# 5.GO-PAY--go pay unfinish
# cd $PWD/dev/src/donate/goPay/goPayUnFinish && webpack --config webpack.config.qa.js
#----- GO PAY -----#

#----- sinar mas -----#
cd $PWD/dev/src/donate/VirtualAccount/OrderTransactionDetail && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/donate/VirtualAccount/VAFinish && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/donate/VirtualAccount/VAUnFinish && webpack --config webpack.config.qa.js
#----- sinar mas -----#

#----- OVO -----#
cd $PWD/dev/src/donate/OVO/OVO && webpack --config webpack.config.qa.js
cd $PWD/dev/src/donate/OVO/OVOTransaction && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/donate/OVO/OVOError && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/donate/OVO/OVOFinish && webpack --config webpack.config.qa.js
#----- OVO -----#

#----- group donate -----#
# !!!!!!! 老版本安卓还需要，暂时留着，没有变更不再重新打包 2020-8-5
# cd $PWD/dev/src/donate/groupDonate && webpack --config webpack.config.qa.js
#----- group donate -----#

#-----  捐赠 -----#

#-----  电话注册或绑定  -----#
cd $PWD/dev/src/phoneBind && webpack --config webpack.config.qa.js
#-----  电话注册或绑定  -----#

#-----  安卓页 -----#
cd $PWD/dev/src/blankforapp && webpack --config webpack.config.qa.js
#-----  安卓页 -----#

# privacyPolicy
cd $PWD/dev/src/privacyPolicy && webpack --config webpack.config.qa.js
cd $PWD/dev/src/termCondition && webpack --config webpack.config.qa.js

# 运营活动:复仇者联盟
# cd $PWD/dev/src/activity/the-avengers/home && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/activity/the-avengers/info && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/activity/the-avengers/questions && webpack --config webpack.config.qa.js

# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.1.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.2.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.3.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.4.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.5.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.6.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.7.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.8.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.9.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.10.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.11.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.12.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.13.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.14.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.15.js
# cd $PWD/dev/src/activity/the-avengers/result && webpack --config webpack.config.qa.16.js
# 运营活动:复仇者联盟

# mediaPartner
cd $PWD/dev/src/mediaPartner && webpack --config webpack.config.qa.js

# kindnessReport
cd $PWD/dev/src/kindnessReport && webpack --config webpack.config.qa.js

# myProfile
cd $PWD/dev/src/myProfile && webpack --config webpack.config.qa.js

cd $PWD/dev/src/zakatCalculator && webpack --config webpack.config.qa.js