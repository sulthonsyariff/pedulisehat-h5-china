# GTRY-Pedulisehat-Id
# index
cd $PWD/dev/src/GTRY-Pedulisehat-Id/home && webpack --config webpack.config.qa.js
# accident
cd $PWD/dev/src/GTRY-Pedulisehat-Id/accident && webpack --config webpack.config.qa.js
# criticalDiseases
cd $PWD/dev/src/GTRY-Pedulisehat-Id/criticalDiseases && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/membershipCard && webpack --config webpack.config.qa.js
# cd $PWD/dev/src/GTRY-Pedulisehat-Id/referralBonus && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/activeMembership && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/applyForAidGuidence && webpack --config webpack.config.qa.js

#--- apply for aid ---#
cd $PWD/dev/src/GTRY-Pedulisehat-Id/applyForAid/accidentInfo && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/applyForAid/diseaseInfo && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/applyForAid/patientInfo && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/applyForAid/payeeInfo && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/applyForAid/applyFinish && webpack --config webpack.config.qa.js

cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/transactions && webpack --config webpack.config.qa.js

#--- apply for aid ---#


#--- publicity ---#
cd $PWD/dev/src/GTRY-Pedulisehat-Id/publicity/publicityDetail && webpack --config webpack.config.qa.js
#--- publicity ---#



#--- go-pay ---#
#1
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/GO-PAY/goPay && webpack --config webpack.config.qa.js
#2
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/GO-PAY/goPayQRcode && webpack --config webpack.config.qa.js
#--- go-pay ---#

#--- OVO ---#
#1
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/OVO/OVO && webpack --config webpack.config.qa.js
#2
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/OVO/OVOTransaction && webpack --config webpack.config.qa.js
#--- OVO ---#

#--- pay ---#
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/pay && webpack --config webpack.config.qa.js
#--- pay ---#

#--- payment-state ---#
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/paymentFailed && webpack --config webpack.config.qa.js
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/paymentSucceed && webpack --config webpack.config.qa.js
#--- payment-state ---#

#--- top-up ---#
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/topUp && webpack --config webpack.config.qa.js
#--- top-up ---#

#--- VA ---#
cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/VirtualAccount/OrderTransactionDetail && webpack --config webpack.config.qa.js
#--- VA ---#

cd $PWD/dev/src/GTRY-Pedulisehat-Id/join/shopeePay/QRcode && webpack --config webpack.config.qa.js